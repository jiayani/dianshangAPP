#!/bin/bash

# LiDong
# Function: upload symbol file(*.app.dSYM) to OneAPM.com automatically
#
# usage:
# invoke this script during an XCode build.
# 
# 1. In XCode, select your project in Project navigator, then select the target;
# 2. Select the Build Phases tab in the settings editor, then click in the white space below;
# 3. In Menu Bar, Select "Editor -> Add Build Phase -> Add Run Script Build Phase";
# 4. Copy and paste the following lines of code to the new phase,
#    remove the '#' at the start of each line and replace APPLICATION_TOKEN with your own
#    application token from your oneapm.com;
#
#SCRIPT=`/usr/bin/find "${SRCROOT}" -name oneapm_postbuild.sh | head -n 1`
#/bin/sh "${SCRIPT}" "APPLICATION_TOKEN"
#

xcode_env_check() {
    echo "OneAPM: $0 must be run from Xcode"
	exit -2
}


if [ ! $1 ]; then
    echo "usage: $0 <APPLICATION_TOKEN>"
	exit -1
fi

if [ ! "$DWARF_DSYM_FOLDER_PATH" -o ! "$DWARF_DSYM_FILE_NAME" -o ! "$INFOPLIST_FILE" ]; then
	xcode_env_check
fi

# upload url
if [ ! "${DSYM_UPLOAD_URL}" ]; then
    DSYM_UPLOAD_URL="https://mobile.oneapm.com/mobile/ios/uploadDsym"
fi


# application token
TOKEN_OF_APPLICATION=$1
DSYM_SRC="${DWARF_DSYM_FOLDER_PATH}/${DWARF_DSYM_FILE_NAME}"

# zip
DSYM_ARCHIVE_PATH="/tmp/$DWARF_DSYM_FILE_NAME.zip"


if [ "$EFFECTIVE_PLATFORM_NAME" == "-iphonesimulator" -a ! "$ENABLE_SIMULATOR_DSYM_UPLOAD" ]; then
	echo "OneAPM: Skipping automatic upload of simulator build symbols"
	exit 0
fi


echo "OneAPM: Archiving \"${DSYM_SRC}\" to \"${DSYM_ARCHIVE_PATH}\""
/usr/bin/zip --recurse-paths --quiet "${DSYM_ARCHIVE_PATH}" "${DSYM_SRC}"

if [ ! -f "$DSYM_ARCHIVE_PATH" ]; then
	echo "OneAPM: Failed to archive \"${DSYM_SRC}\" to \"${DSYM_ARCHIVE_PATH}\""
	exit -3
fi

# md5
MD5_VALUE=$(/sbin/md5 -q ${DSYM_ARCHIVE_PATH})
echo "OneAPM: md5 value is $MD5_VALUE"


echo "curl --write-out %{http_code} --silent --output /dev/null -F file=@\"${DSYM_ARCHIVE_PATH}\" -F md5=\"${MD5_VALUE}\" -F appVersionId=\"$(CURRENT_PROJECT_VERSION)\" -H \"X-App-License-Key: ${TOKEN_OF_APPLICATION}\" -H \"X-BlueWare-Connect-Time: 5000\"  \"${DSYM_UPLOAD_URL}\""
SERVER_RESPONSE=$(curl --write-out %{http_code} --silent --output /dev/null -F file=@"${DSYM_ARCHIVE_PATH}" -F md5="${MD5_VALUE}" -F appVersionId="63" -H "X-App-License-Key: ${TOKEN_OF_APPLICATION}" -H "X-BlueWare-Connect-Time: 5000" "${DSYM_UPLOAD_URL}")

if [ $SERVER_RESPONSE -eq 200 ]; then
    echo "OneAPM: Successfully uploaded debug symbols"
else
    if [ $SERVER_RESPONSE -eq 409 ]; then
        echo "OneAPM: dSYM \"${DSYM_UUIDS}\" already uploaded"
    else
        echo "OneAPM: ERROR \"${SERVER_RESPONSE}\" while uploading \"${DSYM_ARCHIVE_PATH}\" to \"${DSYM_ARCHIVE_PATH}\""
        exit -4
    fi
fi

/bin/rm -f "${DSYM_ARCHIVE_PATH}"


exit 0

