//
//  WXTabBar.h
//  TianLvApp
//
//  Created by 霞 王 on 13-6-21.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WXTabBarDelegate;

@interface WXTabBar : UIView
{
	UIImageView *_backgroundView;
	id<WXTabBarDelegate> _delegate;
	NSMutableArray *_buttons;
    UIColor *_fontColorOfBar;
    UIColor *_fontColorOfSelectedBar;

}
@property (nonatomic, retain) UIImageView *backgroundView;
@property (nonatomic, assign) id<WXTabBarDelegate> delegate;
@property (nonatomic, retain) NSMutableArray *buttons;
@property (nonatomic, retain) UIColor *fontColorOfBar;
@property (nonatomic, retain) UIColor *fontColorOfSelectedBar;


- (id)initWithFrame:(CGRect)frame buttonImages:(NSArray *)imageArray titleArray:(NSArray *)titleArray fontStyleOfMenu:(NSArray *)fontStyleOfMenu;
- (void)selectTabAtIndex:(NSInteger)index;
- (void)removeTabAtIndex:(NSInteger)index;
- (void)insertTabWithImageDic:(NSDictionary *)dict atIndex:(NSUInteger)index;
- (void)setBackgroundImage:(UIImage *)img;
- (void)tabBarButtonClicked:(id)sender;
@end

@protocol WXTabBarDelegate<NSObject>
@optional
- (void)tabBar:(WXTabBar *)tabBar didSelectIndex:(NSInteger)index;
@end


