//
//  WXTabBarControllerViewController.h
//  TianLvApp
//
//  Created by 霞 王 on 13-6-21.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WXTabBar.h"
@class UITabBarController;
@protocol WXTabBarControllerDelegate;
@interface WXTabBarViewController : UIViewController <WXTabBarDelegate>
{
	WXTabBar *_tabBar;
	UIView      *_containerView;
	UIView		*_transitionView;
	id<WXTabBarControllerDelegate> _delegate;
	NSMutableArray *_viewControllers;
	NSUInteger _selectedIndex;
	
	BOOL _tabBarTransparent;
	BOOL _tabBarHidden;
    
    NSInteger animateDriect;
}

@property(nonatomic, retain) NSMutableArray *viewControllers;

@property(nonatomic, readonly) UIViewController *selectedViewController;
@property(nonatomic) NSUInteger selectedIndex;

// Apple is readonly
@property (nonatomic, readonly) WXTabBar *tabBar;
@property(nonatomic,assign) id<WXTabBarControllerDelegate> delegate;


// Default is NO, if set to YES, content will under tabbar
@property (nonatomic) BOOL tabBarTransparent;
@property (nonatomic) BOOL tabBarHidden;

//应用显示的范围， >=7.0 带状态栏 否则不带状态栏
@property (readwrite) CGRect screenRect;

@property(nonatomic,assign) NSInteger animateDriect;

- (id)initWithViewControllers:(NSArray *)vcs imageArray:(NSArray *)arr titleArray:(NSArray *)titleArray fontStyleOfMenu:(NSArray *)fontStyleOfMenu;

- (void)hidesTabBar:(BOOL)yesOrNO animated:(BOOL)animated;
- (void)hidesTabBar:(BOOL)yesOrNO animated:(BOOL)animated driect:(NSInteger)driect;

// Remove the viewcontroller at index of viewControllers.
- (void)removeViewControllerAtIndex:(NSUInteger)index;

// Insert an viewcontroller at index of viewControllers.
- (void)insertViewController:(UIViewController *)vc withImageDic:(NSDictionary *)dict atIndex:(NSUInteger)index;

@end


@protocol WXTabBarControllerDelegate <NSObject>
@optional
- (BOOL)tabBarController:(WXTabBarViewController *)tabBarController shouldSelectViewController:(UIViewController *)viewController;
- (void)tabBarController:(WXTabBarViewController *)tabBarController didSelectViewController:(UIViewController *)viewController;
@end

@interface UIViewController (WXTabBarControllerSupport)
@property(nonatomic, readonly) WXTabBarViewController *wxTabBarController;
@end

