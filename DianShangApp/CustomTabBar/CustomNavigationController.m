//
//  CustomNavigationController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-16.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import "CustomNavigationController.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "WXUserLoginViewController.h"
#import "XYOrderPayViewController.h"
#import "XYPayWebViewController.h"
#import "XYPaySuccessViewController.h"
#import "WXUserLoginViewController.h"
#import "WXCommonSingletonClass.h"
#import "XYGoodsInforViewController.h"
#import "XYGiftInforViewController.h"
#import "WXGroupBuyingDetailViewController.h"
#import "FYGiftSuccessViewController.h"
#import "WXFindPwd1ViewController.h"
#import "JYNUserRegisterViewController.h"
#import "WXUserRegisterProtocolViewController.h"

#define KEY_WINDOW  [[UIApplication sharedApplication]keyWindow]
@interface CustomNavigationController (){
     CGPoint startTouch;
}

@end

@implementation CustomNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil 
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //设置状态栏的字体颜色为白色
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        NSString *imageStr ;
        if (iOS7) {
            imageStr = @"navigationBarBackgrounImage64.png";
            
        }else{
            imageStr = @"navigationBarBackgrounImage44.png";
            self.navigationBar.translucent = NO;
        }
        [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [AppDelegate delegate].titleFontColorOfNav, UITextAttributeTextColor,[AppDelegate delegate].titleFontSizeOfNav, NSFontAttributeName, nil]];
        
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:imageStr] forBarMetrics:UIBarMetricsDefault];

        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (iOS7) {
        //开启iOS7的滑动返回效果
        self.interactivePopGestureRecognizer.enabled = YES;
        self.interactivePopGestureRecognizer.delegate = self;
        /*
        self.interactivePopGestureRecognizer.enabled = YES;
        if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.interactivePopGestureRecognizer.delegate = nil;
        }
         */
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dealloc
{
  
}

// override the push method
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (iOS7) {
        if ([viewController isKindOfClass:[XYOrderPayViewController class]] || [viewController isKindOfClass:[XYPaySuccessViewController class]] || [viewController isKindOfClass:[XYPayWebViewController class]] || [viewController isKindOfClass:[WXUserLoginViewController class]] || [viewController isKindOfClass:[FYGiftSuccessViewController class]]) {
            
            self.interactivePopGestureRecognizer.enabled = NO;
        }else if([viewController isKindOfClass:[XYGoodsInforViewController class]] || [viewController isKindOfClass:[XYGiftInforViewController class]] ||  [viewController isKindOfClass:[WXGroupBuyingDetailViewController class]] || [viewController isKindOfClass:[WXFindPwd1ViewController class]] || [viewController isKindOfClass:[JYNUserRegisterViewController class]] || [viewController isKindOfClass:[WXUserRegisterProtocolViewController class]]){
            
             self.interactivePopGestureRecognizer.enabled = NO;
            [self addPanGestureRecognizer:viewController.view];
        }else{
            //开启iOS7的滑动返回效果
            self.interactivePopGestureRecognizer.enabled = YES;
            self.interactivePopGestureRecognizer.delegate = self;
            /*
            self.interactivePopGestureRecognizer.enabled = YES;
            if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                self.interactivePopGestureRecognizer.delegate = nil;
            }
             */
        }
    }
    [super pushViewController:viewController animated:animated];
}


- (void)addPanGestureRecognizer:(UIView *)myView{
    UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(paningGestureReceive:)];
    [recognizer delaysTouchesBegan];
    [myView addGestureRecognizer:recognizer];
}


#pragma mark - Gesture Recognizer -

- (void)paningGestureReceive:(UIPanGestureRecognizer *)recoginzer
{
    // we get the touch position by the window's coordinate
    CGPoint touchPoint = [recoginzer locationInView:KEY_WINDOW];

    if (recoginzer.state == UIGestureRecognizerStateBegan) {
        startTouch = touchPoint;
        
    }else if (recoginzer.state == UIGestureRecognizerStateEnded){
        
        if (startTouch.x <= 50 && touchPoint.x - startTouch.x > 50)
        {
            [UIView animateWithDuration:0.5 animations:^{
            } completion:^(BOOL finished) {
                
                [self popViewControllerAnimated:NO];
                CGRect frame = self.view.frame;
                frame.origin.x = 0;
                self.view.frame = frame;
                
            }];
        }

    }

}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer;
{
    [self popViewControllerAnimated:YES];
    return YES;
}
@end

