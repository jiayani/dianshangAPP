//
//  WXTabBar.m
//  TianLvApp
//
//  Created by 霞 王 on 13-6-21.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import "WXTabBar.h"
#define kTabBarHeight 49.0f

@implementation WXTabBar

@synthesize backgroundView = _backgroundView;
@synthesize delegate = _delegate;
@synthesize buttons = _buttons;
@synthesize fontColorOfBar = _fontColorOfBar;
@synthesize fontColorOfSelectedBar = _fontColorOfSelectedBar;

- (id)initWithFrame:(CGRect)frame buttonImages:(NSArray *)imageArray titleArray:(NSArray *)titleArray fontStyleOfMenu:(NSArray *)fontStyleOfMenu
{
    self = [super initWithFrame:frame];
    if (self)
	{
		self.backgroundColor = [UIColor clearColor];
		_backgroundView = [[UIImageView alloc] initWithFrame:self.bounds];
		[self addSubview:_backgroundView];
		
		self.buttons = [NSMutableArray arrayWithCapacity:[imageArray count]];
		UIButton *btn;
		CGFloat width = 320.0f / [imageArray count];
		for (int i = 0; i < [imageArray count]; i++)
		{
			btn = [UIButton buttonWithType:UIButtonTypeCustom];
			btn.showsTouchWhenHighlighted = NO;
			btn.tag = i;
            CGFloat pointY = 0;
            CGFloat btnHeight = kTabBarHeight;
            
			btn.frame = CGRectMake(width * i, pointY, width, btnHeight);
			[btn setBackgroundImage:[[imageArray objectAtIndex:i] objectForKey:@"Default"] forState:UIControlStateNormal];
			[btn setBackgroundImage:[[imageArray objectAtIndex:i] objectForKey:@"Highlighted"] forState:UIControlStateHighlighted];
			[btn setBackgroundImage:[[imageArray objectAtIndex:i] objectForKey:@"Seleted"] forState:UIControlStateSelected];
            [btn setTitle:[titleArray objectAtIndex:i]  forState:UIControlStateNormal];
            btn.titleLabel.textAlignment = NSTextAlignmentCenter;
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
            btn.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
            UIEdgeInsets edgetInsets = UIEdgeInsetsMake(0, 0, 6.0, 0);
            [btn setTitleEdgeInsets:edgetInsets];
            

            
            //设置菜单字体大小
            CGFloat fontSizeOfMenu = [[fontStyleOfMenu objectAtIndex:0]floatValue];
            btn.titleLabel.font = [UIFont systemFontOfSize:fontSizeOfMenu];
            
            //设置菜单字体颜色
            
            UIColor *fontColorOfMenu = [fontStyleOfMenu objectAtIndex:1];
            [btn setTitleColor:fontColorOfMenu forState:UIControlStateNormal];
            _fontColorOfBar = fontColorOfMenu;
            _fontColorOfSelectedBar = [fontStyleOfMenu objectAtIndex:2];
            
			[btn addTarget:self action:@selector(tabBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
			[self.buttons addObject:btn];
			[self addSubview:btn];
		}
    }
    return self;
}

- (void)setBackgroundImage:(UIImage *)img
{
	[_backgroundView setImage:img];
}

- (void)tabBarButtonClicked:(id)sender
{
	UIButton *btn = sender;
	[self selectTabAtIndex:btn.tag];
    
    if ([_delegate respondsToSelector:@selector(tabBar:didSelectIndex:)])
    {
        [_delegate tabBar:self didSelectIndex:btn.tag];
    }
}

- (void)selectTabAtIndex:(NSInteger)index
{
	for (int i = 0; i < [self.buttons count]; i++)
	{
		UIButton *b = [self.buttons objectAtIndex:i];
		b.selected = NO;
		b.userInteractionEnabled = YES;
        
        [b setTitleColor:_fontColorOfBar forState:UIControlStateNormal];
	}
	UIButton *btn = [self.buttons objectAtIndex:index];
	btn.selected = YES;
    //设置菜单字体颜色
    [btn setTitleColor:_fontColorOfSelectedBar forState:UIControlStateNormal];
	btn.userInteractionEnabled = NO;
}

- (void)removeTabAtIndex:(NSInteger)index
{
    // Remove button
    [(UIButton *)[self.buttons objectAtIndex:index] removeFromSuperview];
    [self.buttons removeObjectAtIndex:index];
    
    // Re-index the buttons
    CGFloat width = 320.0f / [self.buttons count];
    for (UIButton *btn in self.buttons)
    {
        if (btn.tag > index)
        {
            btn.tag --;
        }
        btn.frame = CGRectMake(width * btn.tag, 0, width, self.frame.size.height);
    }
}
- (void)insertTabWithImageDic:(NSDictionary *)dict atIndex:(NSUInteger)index
{
    // Re-index the buttons
    CGFloat width = 320.0f / ([self.buttons count] + 1);
    for (UIButton *b in self.buttons)
    {
        if (b.tag >= index)
        {
            b.tag ++;
        }
        b.frame = CGRectMake(width * b.tag, 0, width, self.frame.size.height);
    }
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //去掉高亮显示
    btn.showsTouchWhenHighlighted = NO;
    btn.tag = index;
    btn.frame = CGRectMake(width * index, 0, width, self.frame.size.height);
    [btn setImage:[dict objectForKey:@"Default"] forState:UIControlStateNormal];
    [btn setImage:[dict objectForKey:@"Highlighted"] forState:UIControlStateHighlighted];
    [btn setImage:[dict objectForKey:@"Seleted"] forState:UIControlStateSelected];
    [btn addTarget:self action:@selector(tabBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttons insertObject:btn atIndex:index];
    [self addSubview:btn];
}
- (void)dealloc
{
    [_backgroundView release];
    [_buttons release];
    [super dealloc];
}

@end
