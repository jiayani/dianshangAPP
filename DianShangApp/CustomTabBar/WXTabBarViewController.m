//
//  WXTabBarControllerViewController.m
//  TianLvApp
//
//  Created by 霞 王 on 13-6-21.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//


#import "WXTabBar.h"
#import "WXTabBarViewController.h"
#import "CustomNavigationController.h"
#import "WXUserLoginViewController.h"
#import "WXShoppingCartViewController.h"

#define kTabBarHeight 49.0f

static WXTabBarViewController *wxTabBarController;


@implementation UIViewController (WXTabBarControllerSupport)

- (WXTabBarViewController *)wxTabBarController
{
	return wxTabBarController;
}

@end

@interface WXTabBarViewController (private)
- (void)displayViewAtIndex:(NSUInteger)index;
@end

@implementation WXTabBarViewController

@synthesize delegate = _delegate;
@synthesize selectedViewController = _selectedViewController;
@synthesize viewControllers = _viewControllers;
@synthesize selectedIndex = _selectedIndex;
@synthesize tabBarHidden = _tabBarHidden;
@synthesize animateDriect;

#pragma mark -
#pragma mark lifecycle
- (id)initWithViewControllers:(NSArray *)vcs imageArray:(NSArray *)arr titleArray:(NSArray *)titleArray fontStyleOfMenu:(NSArray *)fontStyleOfMenu
{
	self = [super init];
	if (self != nil)
	{
        if (iOS7) {
            self.screenRect = [[UIScreen mainScreen] bounds];
        }else{
            self.screenRect = [[UIScreen mainScreen] applicationFrame];
        }
		_viewControllers = [[NSMutableArray arrayWithArray:vcs] retain];
		
		_containerView = [[UIView alloc] initWithFrame:self.screenRect];
		_transitionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320.0f, _containerView.frame.size.height - kTabBarHeight)];
		_transitionView.backgroundColor =  [UIColor groupTableViewBackgroundColor];
		
		_tabBar = [[WXTabBar alloc] initWithFrame:CGRectMake(0, _containerView.frame.size.height - kTabBarHeight, 320.0f, kTabBarHeight) buttonImages:arr titleArray:titleArray fontStyleOfMenu:fontStyleOfMenu];
		_tabBar.delegate = self;
		
        wxTabBarController = self;
        animateDriect = 1;
	}
	return self;
}

- (void)loadView
{
	[super loadView];
	
	[_containerView addSubview:_transitionView];
	[_containerView addSubview:_tabBar];
	self.view = _containerView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.selectedIndex = 0;
}

- (void)viewDidUnload
{
	[super viewDidUnload];
	
	_tabBar = nil;
	_viewControllers = nil;
}

- (void)dealloc
{
    _tabBar.delegate = nil;
	[_tabBar release];
    [_containerView release];
    [_transitionView release];
	[_viewControllers release];
    [super dealloc];
}

#pragma mark - instant methods

- (WXTabBar *)tabBar
{
	return _tabBar;
}

- (BOOL)tabBarTransparent
{
	return _tabBarTransparent;
}

- (void)setTabBarTransparent:(BOOL)yesOrNo
{
	if (yesOrNo == YES)
	{
		_transitionView.frame = _containerView.bounds;
	}
	else
	{
		_transitionView.frame = CGRectMake(0, 0, 320.0f, _containerView.frame.size.height - kTabBarHeight);
	}
}

/*
 - (void)hidesTabBar:(BOOL)yesOrNO animated:(BOOL)animated
 {
 if (yesOrNO == YES)
 {
 if (self.tabBar.frame.origin.y == self.view.frame.size.height)
 {
 return;
 }
 }
 else
 {
 if (self.tabBar.frame.origin.y == self.view.frame.size.height - kTabBarHeight)
 {
 return;
 }
 }
 
 if (animated == YES)
 {
 [UIView beginAnimations:nil context:NULL];
 [UIView setAnimationDuration:0.3f];
 if (yesOrNO == YES)
 {
 self.tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, self.tabBar.frame.origin.y + kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
 }
 else
 {
 self.tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, self.tabBar.frame.origin.y - kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
 }
 [UIView commitAnimations];
 }
 else
 {
 if (yesOrNO == YES)
 {
 self.tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, self.tabBar.frame.origin.y + kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
 }
 else
 {
 self.tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, self.tabBar.frame.origin.y - kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
 }
 }
 }
 */

- (void)hidesTabBar:(BOOL)yesOrNO animated:(BOOL)animated
{
    [self hidesTabBar:yesOrNO animated:animated driect:animateDriect];
}

- (void)hidesTabBar:(BOOL)yesOrNO animated:(BOOL)animated driect:(NSInteger)driect
{
    // driect: 0 -- 上下  1 -- 左右
    
    NSInteger kTabBarWidth = self.screenRect.size.width;
    NSInteger screenHeight = self.screenRect.size.height;
    
	if (yesOrNO == YES)
	{
        if (driect == 0)
        {
            if (self.tabBar.frame.origin.y == self.view.frame.size.height)
            {
                return;
            }
        }
        else
        {
            if (self.tabBar.frame.origin.x == 0 - kTabBarWidth)
            {
                return;
            }
        }
	}
	else
	{
        if (driect == 0)
        {
            if (self.tabBar.frame.origin.y == self.view.frame.size.height - kTabBarHeight)
            {
                return;
            }
        }
        else
        {
            if (self.tabBar.frame.origin.x == 0)
            {
                return;
            }
        }
	}
	
	if (animated == YES)
	{
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.2f];
		if (yesOrNO == YES)
		{
            if (driect == 0)
            {
//                self.tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, self.tabBar.frame.origin.y + kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
                self.tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, screenHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
            }
            else
            {
                self.tabBar.frame = CGRectMake(0 - kTabBarWidth, self.tabBar.frame.origin.y, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
            }
		}
		else
		{
            if (driect == 0)
            {
//                self.tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, self.tabBar.frame.origin.y - kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
                
                self.tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, screenHeight - kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
            }
            else
            {
                self.tabBar.frame = CGRectMake(0, self.tabBar.frame.origin.y, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
            }
		}
		[UIView commitAnimations];
	}
	else
	{
		if (yesOrNO == YES)
		{
            if (driect == 0)
            {
//                self.tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, self.tabBar.frame.origin.y + kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
                self.tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, screenHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
            }
            else
            {
                self.tabBar.frame = CGRectMake(0 - kTabBarWidth, self.tabBar.frame.origin.y, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
            }
		}
		else
		{
            if (driect == 0)
            {
//                self.tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, self.tabBar.frame.origin.y - kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
                self.tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, screenHeight - kTabBarHeight, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
            }
            else
            {
                self.tabBar.frame = CGRectMake(0, self.tabBar.frame.origin.y, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
            }
		}
	}
}

- (NSUInteger)selectedIndex
{
	return _selectedIndex;
}
- (CustomNavigationController *)selectedViewController
{
    return [_viewControllers objectAtIndex:_selectedIndex];
}

-(void)setSelectedIndex:(NSUInteger)index
{
    [self displayViewAtIndex:index];
    [_tabBar selectTabAtIndex:index];
}

- (void)removeViewControllerAtIndex:(NSUInteger)index
{
    if (index >= [_viewControllers count])
    {
        return;
    }
    // Remove view from superview.
    [[(CustomNavigationController *)[_viewControllers objectAtIndex:index] view] removeFromSuperview];
    // Remove viewcontroller in array.
    [_viewControllers removeObjectAtIndex:index];
    // Remove tab from tabbar.
    [_tabBar removeTabAtIndex:index];
}

- (void)insertViewController:(CustomNavigationController *)vc withImageDic:(NSDictionary *)dict atIndex:(NSUInteger)index
{
    [_viewControllers insertObject:vc atIndex:index];
    [_tabBar insertTabWithImageDic:dict atIndex:index];
}


#pragma mark - Private methods
- (void)displayViewAtIndex:(NSUInteger)index
{
    // Before change index, ask the delegate should change the index.
    if ([(id)_delegate respondsToSelector:@selector(tabBarController:shouldSelectViewController:)])
    {
        if (![(id)_delegate tabBarController:(UITabBarController *)self shouldSelectViewController:[self.viewControllers objectAtIndex:index]])
        {
            return;
        }
    }
    // If target index if equal to current index, do nothing.
    if (_selectedIndex == index && [[_transitionView subviews] count] != 0)
    {
        return;
    }
   
    _selectedIndex = index;
    
	CustomNavigationController *selectedVC = [self.viewControllers objectAtIndex:index];
	
	selectedVC.view.frame = _transitionView.frame;
	if ([selectedVC.view isDescendantOfView:_transitionView])
	{
		[_transitionView bringSubviewToFront:selectedVC.view];
	}
	else
	{
		[_transitionView addSubview:selectedVC.view];
	}
    
    // Notify the delegate, the viewcontroller has been changed.
    if ([(id)_delegate respondsToSelector:@selector(tabBarController:didSelectViewController:)])
    {
        [(id)_delegate tabBarController:(UITabBarController *)self didSelectViewController:selectedVC];
    }
    
}

#pragma mark -
#pragma mark tabBar delegates
- (void)tabBar:(WXTabBar *)tabBar didSelectIndex:(NSInteger)index
{
    CustomNavigationController *nav = (CustomNavigationController *)[_viewControllers objectAtIndex:index];
//    if ([nav.viewControllers isKindOfClass:[WXShoppingCartViewController class]]) {
//        WXShoppingCartViewController *shoppingCartVC = (WXShoppingCartViewController *)nav.viewControllers;
//        shoppingCartVC.needLoad = YES;
//    }
    
    [nav.visibleViewController viewWillAppear:NO];
    
	[self displayViewAtIndex:index];
    
}
@end
