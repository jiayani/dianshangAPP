//
//  WXGroupBuyingDetailViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-6-13.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
@class WXStrikeThroughLabel;
@class WXCommonPicScrollView;
@class XYLoadingView;

@interface WXGroupBuyingDetailViewController : UIViewController<UIWebViewDelegate,UIScrollViewDelegate,MBProgressHUDDelegate>{
    NSDictionary *myDataDic;
    int maxCount;
    XYLoadingView *loadingView;
    int shopCount;
    MBProgressHUD *mbProgressHUD;
}

@property (strong,nonatomic) NSString *keyStr;
@property (strong, nonatomic) IBOutlet WXCommonPicScrollView *picScrollView;
@property (weak, nonatomic) IBOutlet UILabel *groupbuyTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *groupbuyPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *originPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *remainCountLabel;
@property (weak, nonatomic) IBOutlet UITextField *inputCountTxt;
@property (strong, nonatomic) IBOutlet UIView *myView;
@property (weak, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (weak, nonatomic) IBOutlet UIWebView *infoWebView;
@property (weak, nonatomic) IBOutlet UIButton *addShopCart;

- (IBAction)backBtnPressed:(UIButton *)sender;
//单击添加按钮事件
- (IBAction)addBtnPressed:(UIButton *)btn;
//单击减少按钮事件
- (IBAction)decreaseBtnPressed:(UIButton *)btn;
- (IBAction)addShopCartBtnPressed:(id)sender;

@end
