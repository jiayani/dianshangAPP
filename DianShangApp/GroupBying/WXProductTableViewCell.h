//
//  WXProductTableViewCell.h
//  DianShangApp
//
//  Created by 霞 王 on 14-6-13.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AsynImageView;
@class WXStrikeThroughLabel;

@interface WXProductTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet AsynImageView *promotionPicImageView;
@property (weak, nonatomic) IBOutlet UILabel *productTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountPriceLabel;
@property (weak, nonatomic) IBOutlet WXStrikeThroughLabel *originPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *lineLabel;

@end
