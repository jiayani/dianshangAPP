//
//  WXProductTableViewCell.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-13.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXProductTableViewCell.h"
#import "WXConfigDataControll.h"

@implementation WXProductTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    self.discountPriceLabel.textColor = [WXConfigDataControll getFontColorOfPrice];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
