//
//  WXGroupByingListViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-6-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomSeg.h"
#import "WX_Cloud_Delegate.h"
#import "EGORefreshTableFooterView.h"
#import "XYLoadingView.h"
@class XYNoDataView;



@interface WXGroupByingListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,CustomSegDelegate,WX_Cloud_Delegate,EGORefreshTableDelegate>{
    
    CustomSeg *customSeg;
    NSMutableArray *favorableActivityArray;
    NSMutableArray *groupBuyingArray;
    BOOL isHaveMoreOfGroupBuy;
    BOOL isHaveMoreOfActivity;
    EGORefreshTableFooterView *refreshFooterView;
    BOOL reloading;
    NSMutableArray *timerArray;
    int groupBuyPage,favorablePage;
    XYLoadingView *loadingView;
    XYNoDataView *noDataView;

}

@property (readwrite) int flagInt;//1:优惠活动 2:团购

@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (readwrite) BOOL isHave;
@property (strong,nonatomic) NSString *productId;
-(void)requestData;

@end
