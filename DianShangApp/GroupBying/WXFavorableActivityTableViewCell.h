//
//  WXFavorableActivityTableViewCell.h
//  DianShangApp
//
//  Created by 霞 王 on 14-6-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AsynImageView;

@interface WXFavorableActivityTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet AsynImageView *imgImageView;
@property (weak, nonatomic) IBOutlet UILabel *promotionsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *stateImageView;

@end
