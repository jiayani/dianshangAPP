//
//  WXFavorableActivityDetailViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-13.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXFavorableActivityDetailViewController.h"
#import "WXCommonViewClass.h"
#import "WXProductTableViewCell.h"
#import "WXFavorableActivityTableViewCell.h"
#import "WXCommonDateClass.h"
#import "AsynImageView.h"
#import "WXStrikeThroughLabel.h"
#import "WX_Cloud_FavorableActivityDetail.h"
#import "WXUserLoginViewController.h"
#import "AppDelegate.h"
#import "AsynImageView.h"
#import "XYLoadingView.h"
#import "MBProgressHUD.h"
#import "WXConfigDataControll.h"
#import "FYShareViewController.h"
#import "WeiboSDK.h"
#import "XYGoodsInforViewController.h"
#import "WXApi.h"

@interface WXFavorableActivityDetailViewController ()
{
    UIButton *shareBtn;
}
@end

@implementation WXFavorableActivityDetailViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"优惠详情", @"优惠详情标题");
        [WXCommonViewClass hideTabBar:self isHiden:YES]; 
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //微信、新浪注册
    [self register];
    [self initAllControllers];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    secong3Height = 0;
    //同步下载数据
    [self syncGetActivityDetail];
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[WX_Cloud_FavorableActivityDetail share] setDelegate:nil];
    mbProgressHUD.delegate = nil;
    mbProgressHUD = nil;
    if (loadingView != nil) {
        [loadingView removeFromSuperview];
        loadingView = nil;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - All My Methods

//微信、新浪注册
- (void)register{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if(singletonClass.isRegisterSina == NO){//没有注册
        //注册新浪微博
        [WeiboSDK enableDebugMode:YES];
        if([WeiboSDK registerApp:[WXConfigDataControll getSinaAppKey]]){
            singletonClass.isRegisterSina = YES;
        }
        
    }
    //微信注册
    if (singletonClass.isRegisterWeiXin == NO) {
        [WXApi registerApp:[WXConfigDataControll getWeiXinAppKey] withDescription:@"DianShangApp"];
        singletonClass.isRegisterWeiXin = YES;
    }
    
}
- (void)initAllControllers{
    /*begin:设置导航返回按钮 返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    //end
    //分享按钮
    shareBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:NSLocalizedString(@"分享", @"优惠活动/分享")];
    [shareBtn addTarget:self action:@selector(shareButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:shareBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    //end
    
    [self.view addSubview:self.shareView];
    self.shareView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, self.shareView.frame.size.width, self.shareView.frame.size.height);
    
}
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)syncGetActivityDetail{
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
    [[WX_Cloud_FavorableActivityDetail share] setDelegate:(id)self];
    [[WX_Cloud_FavorableActivityDetail share] syncFavorableDetail:self.keyStr];
}
- (void)shareButtonPressed:(id)sender{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if (singletonClass.currentUserId == nil) {
        //用户没有登录
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"每天分享一次可以获得积分，请先登录您的账户吧。" delegate:self cancelButtonTitle:nil otherButtonTitles:@"登录", @"不登录",nil];
        [alert show];
    }
    else
    {
        UIBarButtonItem *btn = (UIBarButtonItem *)sender;
        btn.enabled = NO;
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        view.backgroundColor = [UIColor darkGrayColor];
        view.alpha = 0.5;
        view.tag = 100;
        [self.view addSubview:view];
        [self.view bringSubviewToFront:self.shareView];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2];
        self.shareView.frame = CGRectMake(0, self.view.frame.size.height - self.shareView.frame.size.height, self.shareView.frame.size.width, self.shareView.frame.size.height);
        [UIView commitAnimations];
    }
}
//返回字符串的高度
- (CGFloat)getHeightOfLabel:(NSString *)str fontSize:(CGFloat)fontSize labelWidth:(CGFloat)labelWidth {
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    //设置一个行高上限
    CGSize size = CGSizeMake(labelWidth,2000);
    //计算实际frame大小，并将label的frame变成实际大小
    CGSize labelsize = [str sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
    return labelsize.height;
}
//得到一行字符的宽度
- (float)getWidthOfStr:(NSString *)str fontSize:(UIFont *)font{
    
    //设置一个行高上限
    CGSize size = CGSizeMake(115,16);
    //计算实际frame大小，并将label的frame变成实际大小
    CGSize labelsize = [str sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByTruncatingMiddle];
    return labelsize.width;
}
//修改优惠活动状态
- (void)setFavorableState{
    //1: 默认 在活动中  2: 活动未开始 3: 活动已结束
    int status = [[myDataDic objectForKey:@"status"]intValue];
    switch (status) {
        case 1://默认 在活动中 不显示
            self.stateImageView.hidden = YES;
            break;
        case 2://活动未开始
        {
            self.stateImageView.hidden = NO;
            self.stateImageView.image = [UIImage imageNamed:@"favorableStart.png"];
            break;
        }
            
        case 3://活动已结束
        {
            self.stateImageView.hidden = NO;
            self.stateImageView.image = [UIImage imageNamed:@"favorableEnd.png"];
            break;
        }
            
            
        default:
            break;
    }
    
}
- (void)getHeaderView{
    [self setFavorableState];
    NSString *promotionsTilte = [myDataDic  objectForKey:@"promotionsTitle"];
    self.promotionsTitleLabel.text = promotionsTilte;
    self.promotionsTitleLabel.numberOfLines = 0;
    long startLong = [[myDataDic  objectForKey:@"startDate"]longValue];
    long endLong = [[myDataDic  objectForKey:@"endDate"]longValue];
    NSDate *starDate = [NSDate dateWithTimeIntervalSince1970:startLong];
    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:endLong];
    NSString *starDateStr = [WXCommonDateClass getDateStringOfFormat:@"yyyy-MM-dd" fromDate:starDate];
    NSString *endDateStr = [WXCommonDateClass getDateStringOfFormat:@"yyyy-MM-dd" fromDate:endDate];
    self.dateLabel.text = [NSString stringWithFormat:@"活动日期：%@至%@",starDateStr,endDateStr];
    //begin:设置异步加载图片
    NSString *picStr = [myDataDic objectForKey:@"promotionsPic"];
    self.imgImageView.type = 2;
    self.imgImageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    self.imgImageView.imageURL = picStr;
    //end
    //begin:计算标题的高度，根据标题高度来改变日期的位置和这个cell的高度
    CGRect frame = self.promotionsTitleLabel.frame;
    float myHeight = [self getHeightOfLabel:promotionsTilte  fontSize:12.0 labelWidth:frame.size.width];
    float addHeight = myHeight - frame.size.height;
    CGRect dateFrame = self.dateLabel.frame;
    float cellAddHeight = 0;
    if (addHeight > 0) {
        frame.size.height = myHeight;
        self.promotionsTitleLabel.frame = frame;
        //cell的高度
        CGRect cellFrame = self.headerView.frame;
        cellFrame.size.height = self.headerView.frame.size.height + addHeight;
        self.headerView.frame = cellFrame;
        //日期位置
        dateFrame.origin.y = dateFrame.origin.y + addHeight;
        self.dateLabel.frame = dateFrame;
        cellAddHeight = addHeight;
    }
    //end
    
    //begin:如果有具体的商品的时候，把“优惠商品标题显示出来”
    NSArray *productArray = [myDataDic objectForKey:@"dishOfPromotions"];
    if (productArray != nil && productArray.count > 0 && self.youHuiXuZhiView.superview == nil) {
        [self.headerView addSubview:self.youHuiXuZhiView];
        CGRect youHuiXuzhiFrame = self.youHuiXuZhiView.frame;
        youHuiXuzhiFrame.origin.y = dateFrame.origin.y + dateFrame.size.height;
        self.youHuiXuZhiView.frame = youHuiXuzhiFrame;
        cellAddHeight += youHuiXuzhiFrame.size.height;
    }
    //end
    CGRect cellFrame = self.headerView.frame;
    cellFrame.size.height += cellAddHeight;
    self.headerView.frame = cellFrame;
    secong3Height = self.headerView.frame.size.height;

   
}
- (void)getFooterView{
    self.promotionsContentTextView.text = [myDataDic objectForKey:@"promotionsContent"];
    float height;
    if (iOS7) {
        
        CGRect textFrame=[[self.promotionsContentTextView layoutManager]usedRectForTextContainer:[self.promotionsContentTextView textContainer]];
        height = textFrame.size.height + 16;
        
    }else {
        
        height = self.promotionsContentTextView.contentSize.height;
    }
    CGRect tempframe = self.promotionsContentTextView.frame;
    tempframe.size.height = height;
    self.promotionsContentTextView.frame = tempframe;
    CGRect promotionViewFrame = self.promotionsContentView.frame;
    promotionViewFrame.size.height = tempframe.origin.y + height + 10;
    self.promotionsContentView.frame = promotionViewFrame;
    
}
//更新完数据，刷新页面
- (void)reloadUI{
    //header View
    [self getHeaderView];
    //footer View
    [self getFooterView];
 
    [self.myTableView reloadData];
}
#pragma mark - UIAlertViewDelegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 200) {
        return;
    }
    if (alertView.tag == 101) {
        if (buttonIndex == 1) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[WXApi getWXAppInstallUrl]]];
        }
    }
    if (buttonIndex == 0) {
        [WXCommonViewClass goToLogin:self.navigationController];
        
    }else if (buttonIndex == 1){
        //不登录
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        shareBtn.enabled = NO;
        view.backgroundColor = [UIColor darkGrayColor];
        view.alpha = 0.5;
        view.tag = 100;
        [self.view addSubview:view];
        [self.view bringSubviewToFront:self.shareView];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2];
        self.shareView.frame = CGRectMake(0, self.view.frame.size.height - self.shareView.frame.size.height, self.shareView.frame.size.width, self.shareView.frame.size.height);
        [UIView commitAnimations];
    }
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 3;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{ 
    // Return the number of rows in the section.
    switch (section) {
        case 0:
        case 2:
            return 1;
            break;
        case 1:{
            NSArray *productArray = [myDataDic objectForKey:@"dishOfPromotions"];
            if (productArray != nil) {
                return productArray.count;
            }else{
                return 0;
            }
        }
            break;
        default:
            return 0;
            break;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d%d", [indexPath section], [indexPath row]];//以indexPath来唯一确定cell
    UITableViewCell *cell;
    int secton = indexPath.section;
    if (cell == nil) {
        if (secton == 0){
            cell = [[UITableViewCell alloc]init];
            [cell addSubview:self.headerView];
        }else if (secton == 1) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"WXProductTableViewCell" owner:self options:nil];
            cell = (WXProductTableViewCell *)[nib objectAtIndex:0];
            [self setFavorableActivityTableView:tableView cellForRowAtIndexPath:indexPath cell:(WXProductTableViewCell *)cell];
        }else if (secton == 2){
            cell = [[UITableViewCell alloc]init];
            [cell addSubview:self.promotionsContentView];
        }
        
        
    }
    
    //设置选中时没有背景色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)setFavorableActivityTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath cell:(WXProductTableViewCell *)cell{
    int row = indexPath.row;
    NSArray *productArray = [myDataDic objectForKey:@"dishOfPromotions"];
    NSDictionary *productDic = [productArray objectAtIndex:row];
    //最后一个没有线
    if (row == (productArray.count - 1)) {
        cell.lineLabel.hidden = YES;
    }else{
        cell.lineLabel.hidden = NO;
    }
    cell.productTitleLabel.text = [productDic objectForKey:@"productTitle"];
    //begin:设置异步加载图片
    cell.promotionPicImageView.type = 2;
    cell.promotionPicImageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    cell.promotionPicImageView.imageURL = [productDic objectForKey:@"img"];
    
    //end
    //begin:如果折扣价格不为0.则显示折扣价格，否则显示原始价格
    NSString *showPriceStr;
    BOOL isHiddenPrice;
    if ([[productDic objectForKey: @"discountPrice"]floatValue] == 0.0f) {
        showPriceStr =[NSString stringWithFormat:@"¥ %@",[productDic objectForKey: @"originPrice"]] ;
        isHiddenPrice = YES;
    }else{
        showPriceStr = [NSString stringWithFormat:@"¥ %@",[productDic objectForKey: @"discountPrice"]];
        isHiddenPrice = NO;
    }
    
    NSString *discountPriceStr = showPriceStr;
    CGRect discountPriceFrame = cell.discountPriceLabel.frame;
    discountPriceFrame.size.width = [self getWidthOfStr:discountPriceStr fontSize:cell.discountPriceLabel.font];
    cell.discountPriceLabel.frame = discountPriceFrame;
    cell.discountPriceLabel.text = discountPriceStr ;
    
    if (isHiddenPrice == NO) {
        NSString *originPriceStr = [NSString stringWithFormat:@"¥ %@",[productDic objectForKey:@"originPrice"]];
        CGRect originPriceFrame = cell.originPriceLabel.frame;
        originPriceFrame.size.width = [self getWidthOfStr:originPriceStr fontSize:cell.originPriceLabel.font] + 8;
        originPriceFrame.origin.x = discountPriceFrame.origin.x + discountPriceFrame.size.width + 5;
        cell.originPriceLabel.frame = originPriceFrame;
        cell.originPriceLabel.text = originPriceStr;
        cell.originPriceLabel.hidden = NO;
    }else{
        cell.originPriceLabel.hidden = YES;
    }
    
    //end
    
    
    int isHotsell = [[productDic objectForKey:@"isHotsell"]intValue];//热销
    int isPopularity = [[productDic objectForKey:@"isPopularity"]intValue];//人气
    
    int isfreeFreight = [[productDic objectForKey:@"isfreeFreight"]intValue];//包邮
    CGRect priceFrame = cell.discountPriceLabel.frame;
    UIImageView *rexiaoImageView,*renqiImageView,*baoyouImageView;
    CGSize size = CGSizeMake(30.0f, 13.0f);
    CGFloat pointX = priceFrame.origin.x;
    CGFloat pointY = priceFrame.size.height + priceFrame.origin.y + 8;
    float evaluateValue = [[productDic objectForKey:@"evaluateValue"]floatValue];
    UIView *starView = [WXCommonViewClass getStarView:evaluateValue :pointX :pointY];
    [cell addSubview:starView];
    pointX += starView.frame.size.width/2 + 8;
    if (isHotsell == 1) {
        
        pointX += size.width + 2;
        rexiaoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(pointX, pointY, size.width, size.height)];
        rexiaoImageView.image = [UIImage imageNamed:@"rexiao.png"];
        [cell addSubview:rexiaoImageView];
        
    }else if (isPopularity == 1){
        pointX += size.width + 2;
        renqiImageView = [[UIImageView alloc]initWithFrame:CGRectMake(pointX, pointY, size.width, size.height)];
        renqiImageView.image = [UIImage imageNamed:@"renqi.png"];
        [cell addSubview:renqiImageView];
        
    }else if (isfreeFreight == 1){
        pointX += size.width + 2;
        baoyouImageView = [[UIImageView alloc]initWithFrame:CGRectMake(pointX, pointY, size.width, size.height)];
        baoyouImageView.image = [UIImage imageNamed:@"baoyou.png"];
        [cell addSubview:baoyouImageView];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        XYGoodsInforViewController *goodsInforViewController = [[XYGoodsInforViewController alloc]initWithNibName:@"XYGoodsInforViewController" bundle:nil];
        
        NSArray *productArray = [myDataDic objectForKey:@"dishOfPromotions"];
        NSDictionary *productDic = [productArray objectAtIndex:indexPath.row];
        goodsInforViewController.productId = [productDic objectForKey:@"productID"];
        [self.navigationController pushViewController:goodsInforViewController animated:YES];
    }
    
}

#pragma mark - Table view delegate
- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
            return secong3Height;
            break;
        case 1:
            return 100.0f;
            break;
        case 2:
            return self.promotionsContentView.frame.size.height;
            break;
        default:
            return 0;
            break;
    }
    
}
#pragma mark － MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
    if (loadingView != nil) {
        [loadingView removeFromSuperview];
        loadingView = nil;
    }
	[self.navigationController popViewControllerAnimated:YES];
    
}
#pragma mark - WX_Cloud_Delegate Methods
//无网络和意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    //从服务器端取得最新数据
    [[WX_Cloud_FavorableActivityDetail share] setDelegate:nil];
    mbProgressHUD = [WXCommonViewClass showHudInButtonOfView:self.view title:errorStr closeInSecond:3];
    mbProgressHUD.delegate = self;
    
    
}
#pragma mark GroupBuyingDelegate Methods

-(void)syncGetFavorableDetailSuccess:(NSDictionary *)dataDic{
    if (loadingView != nil) {
        [loadingView removeFromSuperview];
        loadingView = nil;
    }
    [[WX_Cloud_FavorableActivityDetail share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil View:self.view];
    //刷新界面
    myDataDic = dataDic;
    [self reloadUI];

}
-(void)syncGetFavorableDetailFailed:(NSString*)errCode{
    //从服务器端取得最新数据
    [[WX_Cloud_FavorableActivityDetail share] setDelegate:nil];
    mbProgressHUD = [WXCommonViewClass showHudInButtonOfView:self.view title:errCode closeInSecond:3];
    mbProgressHUD.delegate = self;
    
}

#pragma mark - share 分享

- (IBAction)shareCancelBtnAct:(id)sender
{
    UIView *view = [self.view viewWithTag:100];
    [view removeFromSuperview];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    shareBtn.enabled = YES;    
    self.shareView.frame = CGRectMake(0, self.view.frame.size.height , self.shareView.frame.size.width, self.shareView.frame.size.height);
    [UIView commitAnimations];
}
//分享腾讯微博
- (IBAction)btn_shareQQ:(id)sender
{
    if ([WXConfigDataControll getQQAppKey] == nil || [[WXConfigDataControll getQQAppKey] isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"此功能暂未开通" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alert.tag = 200;
        [alert show];
        return;
    }
    FYShareViewController *shareVC = [[FYShareViewController alloc] initWithNibName:@"FYShareViewController" bundle:nil];
    UIView *view = [self.view viewWithTag:100];
    [view removeFromSuperview];
    self.shareView.frame = CGRectMake(0, self.view.frame.size.height , self.shareView.frame.size.width, self.shareView.frame.size.height);
    shareBtn.enabled = YES;
    shareVC.pushVC = self;
    shareVC.titleText = [myDataDic objectForKey:@"promotionsTitle"];
    [self.navigationController pushViewController:shareVC animated:YES];
    
}
//分享微信好友
- (IBAction)btn_weixinGoodF:(id)sender
{
    if ([WXConfigDataControll getWeiXinAppKey] == nil || [[WXConfigDataControll getWeiXinAppKey] isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"此功能暂未开通" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alert.tag = 200;
        [alert show];
        return;
    }
    if ([WXApi isWXAppInstalled]) {
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    UIView *view = [self.view viewWithTag:100];
    [view removeFromSuperview];
    self.shareView.frame = CGRectMake(0, self.view.frame.size.height , self.shareView.frame.size.width, self.shareView.frame.size.height);
    shareBtn.enabled = YES;
    singletonClass.shareStr = @"weixin";
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.text = [NSString stringWithFormat:@"我在%@手机客户端看到了%@介绍,大家也来看看吧,下载链接是 http://apps.kstapp.com/downloads/%@.html",[WXConfigDataControll getAppName] ,[myDataDic objectForKey:@"promotionsTitle"], singletonClass.shopid];
    req.bText = YES;
    [WXApi sendReq:req];
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"微信版本过低或未安装微信,现在去下载微信?" delegate:self cancelButtonTitle:@"暂不下载" otherButtonTitles:@"前往下载", nil];
        alert.tag = 101;
        [alert show];
    }
}
//分享朋友圈
- (IBAction)btn_weixinFquan:(id)sender
{
    if ([WXConfigDataControll getWeiXinAppKey] == nil || [[WXConfigDataControll getWeiXinAppKey] isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"此功能暂未开通" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alert.tag = 200;
        [alert show];
        return;
    }
    if ([WXApi isWXAppInstalled]) {
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    UIView *view = [self.view viewWithTag:100];
    [view removeFromSuperview];
    self.shareView.frame = CGRectMake(0, self.view.frame.size.height , self.shareView.frame.size.width, self.shareView.frame.size.height);
    shareBtn.enabled = YES;
    singletonClass.shareStr = @"weixin";
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    req.text = [NSString stringWithFormat:@"我在%@手机客户端看到了%@介绍,大家也来看看吧,下载链接是 http://apps.kstapp.com/downloads/%@.html",[WXConfigDataControll getAppName] ,[myDataDic objectForKey:@"promotionsTitle"], singletonClass.shopid];
    req.bText = YES;
    req.scene = WXSceneTimeline;
    
    [WXApi sendReq:req];
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"微信版本过低或未安装微信,现在去下载微信?" delegate:self cancelButtonTitle:@"暂不下载" otherButtonTitles:@"前往下载", nil];
        alert.tag = 101;
        [alert show];
    }

}
//分享新浪微博
- (IBAction)btn_shareSina:(id)sender
{
    if ([WXConfigDataControll getSinaAppKey] == nil || [[WXConfigDataControll getSinaAppKey] isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"此功能暂未开通" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alert.tag = 200;
        [alert show];
        return;
    }
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    UIView *view = [self.view viewWithTag:100];
    [view removeFromSuperview];
    self.shareView.frame = CGRectMake(0, self.view.frame.size.height , self.shareView.frame.size.width, self.shareView.frame.size.height);
    singletonClass.shareStr = @"sina";
    shareBtn.enabled = YES;
    WBSendMessageToWeiboRequest *request = [WBSendMessageToWeiboRequest requestWithMessage:[self messageToShare]];
    request.userInfo = @{@"ShareMessageFrom": @"SendMessageToWeiboViewController",
                         @"Other_Info_1": [NSNumber numberWithInt:123],
                         @"Other_Info_2": @[@"obj1", @"obj2"],
                         @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
    [WeiboSDK sendRequest:request];
}

//处理新浪微博数据
- (WBMessageObject *)messageToShare
{
    WBMessageObject *message = [WBMessageObject message];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    message.text = [NSString stringWithFormat:@"我在%@手机客户端看到了%@介绍,大家也来看看吧,下载链接是 http://apps.kstapp.com/downloads/%@.html",[WXConfigDataControll getAppName] ,[myDataDic objectForKey:@"promotionsTitle"], singletonClass.shopid];
    return message;
}
//微信回调
-(void) onReq:(BaseReq*)req
{
    UIView *view = [self.view viewWithTag:100];
    [view removeFromSuperview];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    self.shareView.frame = CGRectMake(0, self.view.frame.size.height , self.shareView.frame.size.width, self.shareView.frame.size.height);
    [UIView commitAnimations];
}

-(void) onResp:(BaseResp*)resp
{
    
    if([resp isKindOfClass:[SendMessageToWXResp class]])
    {
        //  NSString *strTitle = [NSString stringWithFormat:@"发送媒体消息结果"];
        NSString *mesStr;
        switch (resp.errCode) {
            case 0:
                mesStr = @"分享成功";
                break;
            case 1:
                mesStr = @"意外错误";
                break;
            case 2:
                mesStr = @"用户取消操作";
                break;
            default:
                break;
        }
        [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:mesStr];
        
    }
}

@end
