//
//  WXFavorableActivityDetailViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-6-13.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "WXApi.h"
@class AsynImageView;
@class XYLoadingView;


@interface WXFavorableActivityDetailViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MBProgressHUDDelegate, WXApiDelegate>{
    NSDictionary *myDataDic;
    float secong3Height;
    XYLoadingView *loadingView;
    MBProgressHUD *mbProgressHUD;
    
}
@property (strong,nonatomic) NSString *keyStr;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (strong, nonatomic) IBOutlet UIView *promotionsContentView;
@property (strong, nonatomic) IBOutlet UITextView *promotionsContentTextView;
@property (strong, nonatomic) IBOutlet UIView *youHuiXuZhiView;
@property (strong, nonatomic) IBOutlet AsynImageView *imgImageView;
@property (weak, nonatomic) IBOutlet UILabel *promotionsTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIView *shareView;
@property (weak, nonatomic) IBOutlet UIImageView *stateImageView;

- (IBAction)btn_weixinGoodF:(id)sender;
- (IBAction)btn_weixinFquan:(id)sender;
- (IBAction)btn_shareSina:(id)sender;
- (IBAction)shareCancelBtnAct:(id)sender;
- (IBAction)btn_shareQQ:(id)sender;

@end
