//
//  WXGroupBuyingTableViewCell.h
//  DianShangApp
//
//  Created by 霞 王 on 14-6-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AsynImageView;

@interface WXGroupBuyingTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet AsynImageView *imgImageView;
@property (weak, nonatomic) IBOutlet UILabel *groupBuyTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *groupbuyPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *marketPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *endTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (weak, nonatomic) IBOutlet UILabel *freefreightLabel;

@end
