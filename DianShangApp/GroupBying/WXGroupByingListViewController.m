//
//  WXGroupByingListViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXGroupByingListViewController.h"
#import "WXCommonViewClass.h"
#import "WXGroupBuyingTableViewCell.h"
#import "WXFavorableActivityTableViewCell.h"
#import "AsynImageView.h"
#import "WXCommonDateClass.h"
#import "WXCommonDataClass.h"
#import "WX_Cloud_FavorableActivityList.h"
#import "WX_Cloud_GroupBuyingList.h"
#import "WX_Cloud_IsConnection.h"
#import "WXFavorableActivityDetailViewController.h"
#import "WXGroupBuyingDetailViewController.h"
#import "AsynImageView.h"
#import "XYNoDataView.h"



#define maxCountOfOnePage 8;
@interface WXGroupByingListViewController ()

@end

@implementation WXGroupByingListViewController
@synthesize flagInt;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (flagInt == 1) {
        self.title = NSLocalizedString(@"优惠活动", @"优惠活动");
    }else{
        self.title = NSLocalizedString(@"团购", @"团购");
    }

    //bgin:添加礼品购物车和商品购物车的切换按钮
    customSeg = [[[NSBundle mainBundle]loadNibNamed:@"CustomSeg" owner:self options:nil]objectAtIndex:0];
    customSeg.typeSeg = 0;
    customSeg.delegate = self;
    [customSeg setBtn1Title:@"优惠活动" andBtn2Title:@"团购" andBtn1Image:nil andBtn2Image:nil];
    [self.view addSubview:customSeg];
    
    
    //end
    [self initAllControllers];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    //在这去服务端请求数据
    //下载团购信息
    groupBuyPage = 1;
    favorablePage = 1;
    [customSeg segClick:flagInt];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //frame应在表格加载完数据源之后再设置
    [self setRefreshViewFrame];
    
    
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    favorableActivityArray = nil;
    groupBuyingArray = nil;
    [[WX_Cloud_FavorableActivityList share] setDelegate:nil];
    [[WX_Cloud_GroupBuyingList share] setDelegate:nil];
    if (loadingView != nil) {
        [loadingView removeFromSuperview];
    }
    for (NSTimer *tempTimer in timerArray) {
        [tempTimer invalidate];//去掉定时器
    }
    timerArray = nil;
    
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - All My Methods
- (void)initAllControllers{
    [self initRefreshFooterView];
    /*设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    //定义3个计数器
    timerArray = [[NSMutableArray alloc]init];
//    for (int i = 0; i < 3; i++) {
//        NSTimer *tempTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(refreshCountDown:) userInfo:nil repeats:YES];
//        [[NSRunLoop currentRunLoop]addTimer:tempTimer forMode:NSDefaultRunLoopMode];
//        [timerArray insertObject:tempTimer atIndex:i];
//    }
    
//    NSTimer *tempTimer2 = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(refreshCountDown:) userInfo:nil repeats:YES];
//    NSTimer *tempTimer3 = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(refreshCountDown:) userInfo:nil repeats:YES];
//    timerArray = [[NSMutableArray alloc]initWithObjects:tempTimer1,tempTimer2,tempTimer3, nil];
//    [[NSRunLoop currentRunLoop]addTimer:tempTimer1 forMode:NSDefaultRunLoopMode];
//    [[NSRunLoop currentRunLoop]addTimer:tempTimer2 forMode:NSDefaultRunLoopMode];
//    [[NSRunLoop currentRunLoop]addTimer:tempTimer1 forMode:NSDefaultRunLoopMode];

    
}
//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)initRefreshFooterView{
    int height = MAX(self.myTableView.bounds.size.height, self.myTableView.contentSize.height);
    refreshFooterView = [[EGORefreshTableFooterView alloc]  initWithFrame:CGRectZero];
    refreshFooterView.frame =CGRectMake(0.0f, height, self.view.frame.size.width, self.myTableView.bounds.size.height);
    //如果contentsize的高度比表的高度小，那么就需要把刷新视图放在表的bounds的下面
    
    refreshFooterView.delegate = self;
    //下拉刷新的控件添加在tableView上
    
    [self.myTableView addSubview:refreshFooterView];
    reloading = NO;
    
}
- (void)syncDataOfGroupBuying{
    if (self.productId == nil) {
        self.productId = @"";
    }
    [[WX_Cloud_GroupBuyingList share] setDelegate:(id)self];
    [[WX_Cloud_GroupBuyingList share] syncGroupBuyingByPage:self.productId page:groupBuyPage];
}
- (void)syncDataOfFavorable{
    //下载优惠活动信息
    if (self.productId == nil) {
        self.productId = @"";
    }
    [[WX_Cloud_FavorableActivityList share] setDelegate:(id)self];
    [[WX_Cloud_FavorableActivityList share] syncFavorableActivityByPage:self.productId  page:favorablePage];
}
//添加优惠活动无数据页面
- (void)addFavorableNoDataView{
    if (noDataView == nil) {
        noDataView = [[XYNoDataView alloc]initWithFrame:CGRectMake(0, 47, self.view.frame.size.width, self.view.frame.size.height)];
        noDataView.megUpLabel.text = @"暂时没有数据";
        noDataView.megDownLabel.text = @"先去其他页面看看吧";
        [self.view addSubview:noDataView];
    }
    
}
//添加团购无数据页面
- (void)addGroupBuyNoDataView{
    if (noDataView == nil) {
        noDataView = [[XYNoDataView alloc]initWithFrame:CGRectMake(0, 47, self.view.frame.size.width, self.view.frame.size.height)];
        noDataView.megUpLabel.text = @"暂时没有数据";
        noDataView.megDownLabel.text = @"先去其他页面看看吧";
        [self.view addSubview:noDataView];
    }
    
}
//移除正在加载的视图
- (void)removeLoadingView{
    if (loadingView != nil) {
        [loadingView removeFromSuperview];
        loadingView = nil;
    }
}
- (void)groupBuyingNodataView{
    if (groupBuyingArray.count > 0) {
        if (noDataView != nil) {
            [noDataView removeFromSuperview];
            noDataView = nil;
        }
        
    }else{
        [self addGroupBuyNoDataView];
    }
}
- (void)favorableNodataView{
    if (favorableActivityArray.count > 0) {
        if (noDataView != nil) {
            [noDataView removeFromSuperview];
            noDataView = nil;
        }
        
    }else{
        [self addFavorableNoDataView];
    }

}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    if (flagInt == 1) {
        //优惠
        return favorableActivityArray.count;
    }else{
        //团购
        return groupBuyingArray.count;
    }

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return 1;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d%d", [indexPath section], [indexPath row]];//以indexPath来唯一确定cell
    UITableViewCell *cell;
    
    if (cell == nil) {
        if (flagInt == 1) {//优惠
            cell = (WXFavorableActivityTableViewCell *)[[WXFavorableActivityTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                                                             reuseIdentifier: CellIdentifier];
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"WXFavorableActivityTableViewCell" owner:self options:nil];
            cell = (WXFavorableActivityTableViewCell *)[nib objectAtIndex:0];
            [self setFavorableActivityTableView:tableView cellForRowAtIndexPath:indexPath cell:(WXFavorableActivityTableViewCell *)cell];
        }else{//团购
            cell = (WXGroupBuyingTableViewCell *)[[WXGroupBuyingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault                                 reuseIdentifier: CellIdentifier];
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"WXGroupBuyingTableViewCell" owner:self options:nil];
            cell = (WXGroupBuyingTableViewCell *)[nib objectAtIndex:0];
            [self setGroupBuyingTableView:tableView cellForRowAtIndexPath:indexPath cell:(WXGroupBuyingTableViewCell *)cell];
        }
    }
    //设置选中时没有背景色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)setGroupBuyingTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  cell:(WXGroupBuyingTableViewCell *)cell{
    int row = indexPath.section;
    NSDictionary *dic = [groupBuyingArray objectAtIndex:row];
    cell.groupBuyTitleLabel.text = [dic objectForKey:@"groupbuyTitle"];
    float priceF = [[dic objectForKey:@"groupbuyPrice"]floatValue];
    float marketPriceF = [[dic objectForKey:@"marketPrice"]floatValue];
    NSString *priceStr = [NSString stringWithFormat:@"¥%.2f",priceF];
    cell.groupbuyPriceLabel.text = priceStr;
    cell.marketPriceLabel.text = [NSString stringWithFormat:@"¥%.2f",marketPriceF];
    long endTimeLong = [[dic objectForKey:@"endTime"]longValue];
    NSDate *endTimeDate = [NSDate dateWithTimeIntervalSince1970:endTimeLong];
    //begin:日期区域化
    NSTimeZone *endZone = [NSTimeZone  timeZoneWithName:@"Asia/Shanghai"];
    NSInteger endInterval = [endZone secondsFromGMTForDate:endTimeDate];
    endTimeDate = [endTimeDate  dateByAddingTimeInterval: endInterval];
    //end
    NSArray *tempArray = [[NSArray alloc]initWithObjects:cell.endTimeLabel,endTimeDate, nil];

    //begin:每一秒执行一次（重复性）
    if (timerArray != nil) {
        if (timerArray.count <= 4) {
            NSTimer *tempTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(refreshCountDown:) userInfo:tempArray repeats:YES];
            [[NSRunLoop currentRunLoop]addTimer:tempTimer forMode:NSDefaultRunLoopMode];
            [timerArray addObject:tempTimer];
        }else{
            NSTimer *tempTimer = (NSTimer *)[timerArray objectAtIndex:row%4];
            [tempTimer invalidate];//去掉定时器
            tempTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(refreshCountDown:) userInfo:tempArray repeats:YES];
            [[NSRunLoop currentRunLoop]addTimer:tempTimer forMode:NSDefaultRunLoopMode];
        }
    }
   


    //end
    //begin:根据剩余数量来判断是否卖光了
    long remainCount = [[dic objectForKey:@"remainCount"]longValue];
    UIColor *borderColor;
    if (remainCount > 0) {
        [cell.buyButton setTitle:@"立即抢购" forState:UIControlStateNormal];
        cell.buyButton.enabled = YES;
        borderColor = [UIColor colorWithRed:253.0f/255.0f green:75.0f/255.0f blue:77.0f/255.0f alpha:1.0f];
       
    }else{
        [cell.buyButton setTitle:@"卖光了" forState:UIControlStateNormal];
        cell.buyButton.enabled = NO;
        borderColor = [UIColor colorWithRed:178.0f/255.0f green:178.0f/255.0f blue:178.0f/255.0f alpha:1.0f];
        
    }
    cell.buyButton.layer.borderWidth = 1.0f;
    cell.buyButton.layer.borderColor = borderColor.CGColor;
    [cell.buyButton setTitleColor:borderColor forState:UIControlStateNormal];
    //end
    //begin:根据isFreefreight 判断是否显示包邮图标
    int isFreeFreight = [[dic objectForKey:@"isFreefreight"]intValue];
    UIFont *myFont = cell.groupbuyPriceLabel.font;
    if (isFreeFreight == 0) {
        
        cell.freefreightLabel.hidden = YES;
    }else{
        //begin:让包邮的标签紧贴着购买价格
        CGSize textSize = [priceStr sizeWithFont:myFont];
        CGFloat strikeWidth = textSize.width;
        CGRect frame = cell.groupbuyPriceLabel.frame;
        frame.size.width = strikeWidth;
        cell.groupbuyPriceLabel.frame = frame;
        CGRect freeFreightFrame = cell.freefreightLabel.frame;
        freeFreightFrame.origin.x = frame.origin.x + strikeWidth + 5.0f;
        cell.freefreightLabel.frame = freeFreightFrame;
        //end
        cell.freefreightLabel.hidden = NO;
    }
    //end
    cell.imgImageView.type = 1;
    cell.imgImageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    cell.imgImageView.imageURL = [dic objectForKey:@"img"];
    
    
}
//每秒刷新一次倒计时
- (void)refreshCountDown:(NSTimer *)myTimer{
    
    NSArray *tempArray = (NSArray *)myTimer.userInfo;
    if (myTimer.userInfo != nil) {
        UILabel *label = (UILabel *)[tempArray objectAtIndex:0];
        NSDate *endTimeDate = [tempArray objectAtIndex:1];
        
        NSString *endTimeString = [NSString stringWithFormat:@"剩余时间：%@",[WXCommonDateClass getCountdownStr:endTimeDate ]];
        label.text = endTimeString;
    }
}
- (void)setFavorableActivityTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath cell:(WXFavorableActivityTableViewCell *)cell{
    int row = indexPath.section;
    NSDictionary *dic = [favorableActivityArray objectAtIndex:row];
    //1: 默认 在活动中  2: 活动未开始 3: 活动已结束
    int status = [[dic objectForKey:@"status"]intValue];
    switch (status) {
        case 1://默认 在活动中 不显示
            cell.stateImageView.hidden = YES;
            break;
        case 2://活动未开始
            {
                cell.stateImageView.hidden = NO;
                cell.stateImageView.image = [UIImage imageNamed:@"favorableStart.png"];
                break;
            }
            
        case 3://活动已结束
            {
                cell.stateImageView.hidden = NO;
                cell.stateImageView.image = [UIImage imageNamed:@"favorableEnd.png"];
                break;
            }

            
        default:
            break;
    }
    cell.promotionsTitleLabel.text = [dic  objectForKey:@"promotionsTitle"];
    long startLong = [[dic  objectForKey:@"startDate"]longValue];
    long endLong = [[dic  objectForKey:@"endDate"]longValue];
    NSDate *starDate = [NSDate dateWithTimeIntervalSince1970:startLong];
    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:endLong];
    NSString *starDateStr = [WXCommonDateClass getDateStringOfFormat:@"yyyy-MM-dd" fromDate:starDate];
    NSString *endDateStr = [WXCommonDateClass getDateStringOfFormat:@"yyyy-MM-dd" fromDate:endDate];
    cell.dateLabel.text = [NSString stringWithFormat:@"活动日期：%@至%@",starDateStr,endDateStr];
     //设置异步加载图片
    cell.imgImageView.type = 1;
    cell.imgImageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    cell.imgImageView.imageURL = [dic objectForKey:@"img"];
    

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    int row = indexPath.section;
    
    if (flagInt == 1) {
        NSDictionary *dic = [favorableActivityArray objectAtIndex:row];
        WXFavorableActivityDetailViewController *favorableActivityDetailViewController = [[WXFavorableActivityDetailViewController alloc]initWithNibName:@"WXFavorableActivityDetailViewController" bundle:nil];
        favorableActivityDetailViewController.keyStr = [dic objectForKey:@"promotionsID"];
        [self.navigationController pushViewController:favorableActivityDetailViewController animated:YES];
      //优惠
    }else{
      //团购
        NSDictionary *dic = [groupBuyingArray objectAtIndex:row];
        WXGroupBuyingDetailViewController *groupBuyingDetailViewController = [[WXGroupBuyingDetailViewController alloc]initWithNibName:@"WXGroupBuyingDetailViewController" bundle:nil];
        groupBuyingDetailViewController.keyStr = [dic objectForKey:@"groupbuyID"];
        [self.navigationController pushViewController:groupBuyingDetailViewController animated:YES];
    }
}

#pragma mark - Table view delegate

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (flagInt == 1) {
        //优惠活动
        return 215;
    }else{
        //团购
        return 170;
    }
    
}

- (float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* myView = [[UIView alloc] init];
    myView.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0f];
    myView.layer.borderWidth = 0;
    return myView;
}
#pragma mark - 下拉刷新数据方法
//请求数据
-(void)requestData
{
    reloading = YES;
    BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
    if (isConnection) {//有网络情况
        
        //每次进入该页面的时候，重新从服务器端读取数据
        if(refreshFooterView.hasMoreData)
        {
            if (flagInt == 1) {
                int myCount = favorableActivityArray.count;
                int pageCount = myCount/maxCountOfOnePage;
                favorablePage = pageCount + 1;
                //下载优惠活动信息
                [self syncDataOfFavorable];
            }else{
                int myCount = groupBuyingArray.count;
                int pageCount = myCount/maxCountOfOnePage;
                groupBuyPage = pageCount + 1;
                //下载团购信息
                [self syncDataOfGroupBuying];
            }
        }
    }else{//无网络情况
        [WXCommonViewClass showHudInButtonOfView:self.view title:NoHaveNetwork closeInSecond:2];
    }

}

-(void)reloadUI
{
    reloading = NO;
    //停止下拉的动作,恢复表格
	[refreshFooterView egoRefreshScrollViewDataSourceDidFinishedLoading:self.myTableView];
    //更新界面
    [self.myTableView reloadData];
    [self setRefreshViewFrame];

}

-(void)setRefreshViewFrame
{
    //如果contentsize的高度比表的高度小，那么就需要把刷新视图放在表的bounds的下面
    int height = MAX(self.myTableView.bounds.size.height, self.myTableView.contentSize.height);
    refreshFooterView.frame =CGRectMake(0.0f, height, self.view.frame.size.width, self.myTableView.bounds.size.height);
}

#pragma mark - EGORefreshTableFooterDelegate
//出发下拉刷新动作，开始拉取数据
- (void)egoRefreshTableDidTriggerRefresh:(EGORefreshPos)aRefreshPos
{
    [self requestData];
}


//返回当前刷新状态：是否在刷新
- (BOOL)egoRefreshTableDataSourceIsLoading:(UIView*)view{
	
	return reloading; // should return if data source model is reloading
	
}
//返回刷新时间
// if we don't realize this method, it won't display the refresh timestamp
- (NSDate*)egoRefreshTableDataSourceLastUpdated:(UIView*)view
{
	
	return [NSDate date]; // should return date data source was last changed
	
}

#pragma mark - UIScrollView

//此代理在scrollview滚动时就会调用
//在下拉一段距离到提示松开和松开后提示都应该有变化，变化可以在这里实现
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (flagInt == 1) {
        refreshFooterView.hasMoreData = isHaveMoreOfActivity;
    }else{
         refreshFooterView.hasMoreData = isHaveMoreOfGroupBuy;
    }
    
    [refreshFooterView egoRefreshScrollViewDidScroll:scrollView];
}
//松开后判断表格是否在刷新，若在刷新则表格位置偏移，且状态说明文字变化为loading...
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (flagInt == 1) {
        refreshFooterView.hasMoreData = isHaveMoreOfActivity;
    }else{
        refreshFooterView.hasMoreData = isHaveMoreOfGroupBuy;
    }
    [refreshFooterView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark - CustomSegDelegate Methods

- (void)segChange:(int)index andIsFirst:(int)firstTag{

    flagInt = index;
    //无数据时显示无数据页面
    
    if (self.isHave == NO) {
        if (loadingView == nil) {
            loadingView = [[XYLoadingView alloc] initWithType:2];
            [self.view addSubview:loadingView];
        }

        if (flagInt == 1 ) {
            favorableActivityArray = nil;
            favorablePage = 1;
            [self syncDataOfFavorable];
        }else if (flagInt == 2){
            groupBuyingArray = nil;
            groupBuyPage = 1;
            [self syncDataOfGroupBuying];
            
        }
    }else{
        if (flagInt == 1 ){
            favorableActivityArray = nil;
            [self favorableNodataView];
        }else if (flagInt == 2){
            groupBuyingArray = nil;
            [self groupBuyingNodataView];
        }
    }
    

}
#pragma mark - WX_Cloud_Delegate Methods
//无网络和意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    //从服务器端取得最新数据
    [[WX_Cloud_FavorableActivityList share] setDelegate:nil];
    [[WX_Cloud_GroupBuyingList share] setDelegate:nil];
    [self removeLoadingView];
    if (flagInt == 1) {
        [self favorableNodataView];
    }else{
        [self groupBuyingNodataView];
    }
    [WXCommonViewClass showHudInButtonOfView:self.view title:errorStr closeInSecond:2];
}
#pragma mark GroupBuyingDelegate Methods
//获得团购列表信息
-(void)syncGetGroupBuyingSuccess:(NSDictionary *)dic{
    [self removeLoadingView];
    NSArray *dataArray = [dic objectForKey:@"data"];
    int  hasMore = [[dic objectForKey:@"hasMore"]intValue];
    [[WX_Cloud_GroupBuyingList share] setDelegate:nil];
    
    if (groupBuyingArray == nil || groupBuyingArray.count == 0) {
        groupBuyingArray = [[NSMutableArray alloc]initWithArray:dataArray];
    }else{
        [groupBuyingArray addObjectsFromArray:dataArray] ;
 
    }
 
    if (hasMore == 1) {
        isHaveMoreOfGroupBuy = YES;
    }else{
        isHaveMoreOfGroupBuy = NO;
    }
    
    //刷新UI
    [self groupBuyingNodataView];
    [self reloadUI];
}
-(void)syncGetGroupBuyingFailed:(NSString*)errCode{
    [self removeLoadingView];
    [[WX_Cloud_GroupBuyingList share] setDelegate:nil];
    //刷新UI
    [self groupBuyingNodataView];
    [self reloadUI];
    [WXCommonViewClass showHudInButtonOfView:self.view title:errCode closeInSecond:2];
}

#pragma mark FavorableActivityListDelegate Methods
//获得优惠活动列表信息
-(void)syncGetFavorableActivitySuccess:(NSDictionary *)dataDic{
    [self removeLoadingView];
    [[WX_Cloud_FavorableActivityList share] setDelegate:nil];
    NSDictionary *myDataDic = [dataDic objectForKey:@"data"];
    if (favorableActivityArray == nil || favorableActivityArray.count == 0) {
        favorableActivityArray = [[NSMutableArray alloc]initWithArray:[myDataDic objectForKey:@"items"]];
    }else{
        [favorableActivityArray addObjectsFromArray:[myDataDic objectForKey:@"items"]] ;
    }
    [WXCommonDataClass setPreferInforTime:[myDataDic objectForKey:@"time"]];
    int hasMore = [[dataDic objectForKey:@"hasMore"]intValue];
    if (hasMore == 1) {
        isHaveMoreOfActivity = YES;
    }else{
        isHaveMoreOfActivity = NO;
    }
    //刷新UI
    [self favorableNodataView];
    [self reloadUI];
}
-(void)syncGetFavorableActivityFailed:(NSString*)errCode{
    [self removeLoadingView];
    [[WX_Cloud_FavorableActivityList share] setDelegate:nil];
    [self favorableNodataView];
    [WXCommonViewClass showHudInButtonOfView:self.view title:errCode closeInSecond:2];
}
@end
