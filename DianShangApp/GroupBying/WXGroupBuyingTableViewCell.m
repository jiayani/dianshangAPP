//
//  WXGroupBuyingTableViewCell.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXGroupBuyingTableViewCell.h"
#import "WXConfigDataControll.h"

@implementation WXGroupBuyingTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    self.freefreightLabel.layer.cornerRadius = 1.0f;
    self.buyButton.layer.cornerRadius = 4;
    self.buyButton.layer.borderWidth = 1;
    self.groupbuyPriceLabel.textColor = [WXConfigDataControll getFontColorOfPrice];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
