//
//  WXGroupBuyingDetailViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-13.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXGroupBuyingDetailViewController.h"
#import "WXCommonViewClass.h"
#import "WX_Cloud_GroupBuyingDetail.h"
#import "WXCommonDateClass.h"
#import "WXCommonPicScrollView.h"
#import "FYPayViewController.h"
#import "XYLoadingView.h"
#import "WXConfigDataControll.h"
#import "ImageEnlarge.h"
#import "AsynImageView.h"
#import "WXUserLoginViewController.h"

@interface WXGroupBuyingDetailViewController ()

@end

@implementation WXGroupBuyingDetailViewController
@synthesize picScrollView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initAllControllers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    WXCommonSingletonClass *singletionClass = [WXCommonSingletonClass share];
    if (singletionClass.isNotFistLoad == NO) {
        //同步下载数据
        [self syncGroupBuyingDetail];
    }
    
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[WX_Cloud_GroupBuyingDetail share] setDelegate:nil];
    mbProgressHUD.delegate = nil;
    mbProgressHUD = nil;
    
}
#pragma mark - All My Methods
- (void)initAllControllers{
    shopCount = 1;
    self.groupbuyPriceLabel.textColor = [WXConfigDataControll getFontColorOfPrice];
}
- (IBAction)backBtnPressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)syncGroupBuyingDetail{
    
    [[WX_Cloud_GroupBuyingDetail share] setDelegate:(id)self];
    [[WX_Cloud_GroupBuyingDetail share] syncGroupBuyingDetail:self.keyStr];
}
- (void)reloadUI{
    //begin:图片
    NSArray *picArray = [myDataDic objectForKey:@"groupbuyPics"];
    self.picScrollView = [[WXCommonPicScrollView alloc]initWithFrame:CGRectMake(0, 0, 320.0f, 320.0f)];
    [self.picScrollView initAllControllers:picArray newNav:self.navigationController];
    [self.myScrollView addSubview:self.picScrollView];

    //end
    NSString *groupByTitleStr = [myDataDic objectForKey:@"groupbuyTitle"];
    self.groupbuyTitleLabel.text = groupByTitleStr;
    //begin:团购价
    double priceFloat = [[myDataDic objectForKey:@"groupbuyPrice"]doubleValue];
    NSString *groupPriceStr = [self getPriceString:priceFloat];
    CGRect groupPriceFrame = self.groupbuyPriceLabel.frame;
    groupPriceFrame.size.width = [self getWidthOfStr:groupPriceStr fontSize:self.groupbuyPriceLabel.font];
    self.groupbuyPriceLabel.frame = groupPriceFrame;
    self.groupbuyPriceLabel.text = groupPriceStr;
    //end
    //begin:原价
    double originFloat = [[myDataDic objectForKey:@"originPrice"]doubleValue];
    NSString *orignPriceStr = [self getPriceString:originFloat];
    CGRect orignPriceFrame = self.originPriceLabel.frame;
    orignPriceFrame.origin.x = groupPriceFrame.origin.x + groupPriceFrame.size.width + 5;
    orignPriceFrame.size.width = [self getWidthOfStr:orignPriceStr fontSize:self.originPriceLabel.font];
    self.originPriceLabel.frame = orignPriceFrame;
    self.originPriceLabel.text = orignPriceStr;
    //end
    //begin:包邮
    CGSize size = CGSizeMake(30.0f, 13.0f);
    CGFloat pointX = orignPriceFrame.origin.x + orignPriceFrame.size.width + 8;
    CGFloat pointY = orignPriceFrame.origin.y + 2;
    int isfreeFreight = [[myDataDic objectForKey:@"isFreefreight"]intValue];//包邮
    if (isfreeFreight == 1){
        UIImageView *baoyouImageView = [[UIImageView alloc]initWithFrame:CGRectMake(pointX, pointY, size.width, size.height)];
        baoyouImageView.image = [UIImage imageNamed:@"baoyou.png"];
        [self.myView addSubview:baoyouImageView];
    }
    //end
    self.remainCountLabel.text = [NSString stringWithFormat:@"剩余团购：%@",[myDataDic objectForKey:@"remainCount"]];
    long startLong = [[myDataDic  objectForKey:@"startTime"]longValue];
    long endLong = [[myDataDic  objectForKey:@"endTime"]longValue];
    NSDate *starDate = [NSDate dateWithTimeIntervalSince1970:startLong];
    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:endLong];
    NSString *starDateStr = [WXCommonDateClass getDateStringOfFormat:@"MM-dd" fromDate:starDate];
    NSString *endDateStr = [WXCommonDateClass getDateStringOfFormat:@"MM-dd" fromDate:endDate];
    self.dateLabel.text = [NSString stringWithFormat:@"活动时间：%@至%@",starDateStr,endDateStr];
    maxCount = [[myDataDic objectForKey:@"remainCount"]intValue];
    NSString *addShopImageStr;
    if (maxCount == 0) {
        addShopImageStr = @"group_buy_uncl.png";
        self.addShopCart.enabled = NO;
    }else{
        addShopImageStr = @"group_buy_n.png";
        self.addShopCart.enabled = YES;
    }
    [self.addShopCart setBackgroundImage:[UIImage imageNamed:addShopImageStr] forState:UIControlStateNormal];
    //begin:根据团购标题是显示一行还是2行 来调试UI的位置
    CGRect frame = self.groupbuyTitleLabel.frame;
    float myHeight = [self getHeightOfLabel:groupByTitleStr  fontSize:14.0 labelWidth:frame.size.width];
    float addHeight = myHeight - frame.size.height;
    if (addHeight > 0) {
        frame.size.height = myHeight;
        self.groupbuyTitleLabel.frame = frame;
    }
    CGRect frameOfMyView = self.myView.frame;
    frameOfMyView.origin.y = frame.origin.y + frame.size.height;
    self.myView.frame = frameOfMyView;
    [self.myScrollView addSubview:self.myView];
    
    //begin:web
    [self.infoWebView loadHTMLString:[NSString stringWithFormat:@"%@",[myDataDic objectForKey:@"groupbuyContent"]] baseURL:nil];
    //end
    
    
}
- (NSString *)getPriceString:(double)price{
    return [NSString stringWithFormat:@"¥ %.2f",price];
}
//返回字符串的高度
- (CGFloat)getHeightOfLabel:(NSString *)str fontSize:(CGFloat)fontSize labelWidth:(CGFloat)labelWidth {
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    //设置一个行高上限
    CGSize size = CGSizeMake(labelWidth,2000);
    //计算实际frame大小，并将label的frame变成实际大小
    CGSize labelsize = [str sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
    return labelsize.height;
}
//单击添加按钮事件
- (void)addBtnPressed:(UIButton *)btn{
    int count = [self.inputCountTxt.text intValue];
    if (count < maxCount) {
        self.inputCountTxt.text = [NSString stringWithFormat:@"%d",++count];
    }
    shopCount = count;
}

//单击减少按钮事件
- (void)decreaseBtnPressed:(UIButton *)btn{
    int count = [self.inputCountTxt.text intValue];
    if (count > 1) {
        self.inputCountTxt.text = [NSString stringWithFormat:@"%d",--count];
    }
    shopCount = count;
    
}

- (IBAction)addShopCartBtnPressed:(id)sender {
    if (maxCount > 0) {
        //判断是否登陆，登陆的话进入订单页面；否则进入登录页面
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        if (singletonClass.currentUserId == nil) {
            //无登录
            [WXCommonViewClass goToLogin:self.navigationController];
        }else{
            //有登录，进入订单
            FYPayViewController *payViewController  = [[FYPayViewController alloc]initWithNibName:@"FYPayViewController" bundle:nil];
            payViewController.isTuan = YES;
            payViewController.productArray = [[NSArray alloc]initWithObjects:myDataDic, nil];
            payViewController.count = shopCount;
            payViewController.money = [[myDataDic objectForKey:@"groupbuyPrice"]doubleValue] * shopCount;
            [self.navigationController pushViewController:payViewController animated:YES];
        }

    }

}
//得到一行字符的宽度
- (float)getWidthOfStr:(NSString *)str fontSize:(UIFont *)font{
    
    //设置一个行高上限
    CGSize size = CGSizeMake(115,16);
    //计算实际frame大小，并将label的frame变成实际大小
    CGSize labelsize = [str sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByTruncatingMiddle];
    return labelsize.width;
}
//点击小图查看大图
//-(void)singleTap:(UITapGestureRecognizer*)recognizer{
//
//    self.navigationController.view.backgroundColor = [UIColor blackColor];
//    AsynImageView *imageView = (AsynImageView *)[picScrollView viewWithTag:_customPageController.currentPage];
//    ImageEnlarge *pictureVC = [[ImageEnlarge alloc]init];
//    
//    
//    if(imageView.image != nil){
//        pictureVC.images = imageView.image;
//        
//        float height = imageView.image.size.height * 320.0 / imageView.image.size.width;
//        pictureVC.imageView.frame = CGRectMake(0,0, 320,height);
//        pictureVC.view.backgroundColor = [UIColor blackColor];
//        pictureVC.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
//        [UIView beginAnimations:nil context:nil];
//        [UIView setAnimationDuration:0.3f];
//        
//        pictureVC.view.transform = CGAffineTransformMakeScale(1, 1);
//        
//        
//        [self.navigationController pushViewController:pictureVC animated:NO];
//        [UIView commitAnimations];
//    }else{
//        //弹出提示语
//        [WXCommonViewClass showHudInView:self.view title:@"未发现本地图片"];
//    }
//}

#pragma mark -  WebviewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    //webview 自适应高度
    CGRect webFrame = webView.frame;

    float productContentHight = webView.scrollView.contentSize.height + 108;
    float scrollHeith = webFrame.origin.y + productContentHight ;
    if ((scrollHeith -  self.view.frame.size.height) < 350) {
        scrollHeith = scrollHeith + 350;
    }else{
        scrollHeith = productContentHight + 130;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [webView stringByEvaluatingJavaScriptFromString:@"var imgs = document.getElementsByTagName('img');"
         " for(var i= 0;i<imgs.length;i++){"
         "  imgs[i].width = 300;"
         "}"];
        
        webView.frame = CGRectMake(webFrame.origin.x, webFrame.origin.y,webFrame.size.width,productContentHight);
        CGRect myFrame = self.myScrollView.frame;
        self.myScrollView.contentSize = CGSizeMake(myFrame.size.width, scrollHeith);
    });

}
#pragma mark － MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
    if (loadingView != nil) {
        [loadingView removeFromSuperview];
        loadingView = nil;
    }
	// Remove HUD from screen when the HUD was hidded
	[self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - WX_Cloud_Delegate Methods
//无网络和意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    //从服务器端取得最新数据
    [[WX_Cloud_GroupBuyingDetail share] setDelegate:nil];
    mbProgressHUD = [WXCommonViewClass showHudInButtonOfView:self.view title:errorStr closeInSecond:3];
    mbProgressHUD.delegate = self;
   

}

#pragma mark GroupBuyingDelegate Methods

-(void)syncGetGroupBuyDetailSuccess:(NSDictionary *)dataDic{
    [[WX_Cloud_GroupBuyingDetail share] setDelegate:nil];
    //刷新界面
    myDataDic = dataDic;
    [self reloadUI];
}
-(void)syncGetGroupBuyDetailFailed:(NSString*)errCode{
    
    [[WX_Cloud_GroupBuyingDetail share] setDelegate:nil];
    mbProgressHUD = [WXCommonViewClass showHudInButtonOfView:self.view title:errCode closeInSecond:3];
    mbProgressHUD.delegate = self;

    
}
@end
