//
//  ProductAds.h
//  DianShangApp
//
//  Created by wa on 14-7-16.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ProductAds : NSManagedObject

@property (nonatomic, retain) NSNumber * ad_orderBy;
@property (nonatomic, retain) NSNumber * ad_type;
@property (nonatomic, retain) NSString * ad_url;
@property (nonatomic, retain) NSNumber * adid;
@property (nonatomic, retain) NSString * pic_url;
@property (nonatomic, retain) NSString * title;

@end
