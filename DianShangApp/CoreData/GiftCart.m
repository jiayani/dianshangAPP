//
//  GiftCart.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "GiftCart.h"


@implementation GiftCart

@dynamic count;
@dynamic giftCartOrderBy;
@dynamic giftid;
@dynamic integral;
@dynamic isOnline;
@dynamic mode;
@dynamic pic_url;
@dynamic stockCount;
@dynamic title;
@dynamic type;

@end
