//
//  GuideAnimation.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface GuideAnimation : NSManagedObject

@property (nonatomic, retain) NSString * appStartPic;
@property (nonatomic, retain) NSString * guideAnimationID;
@property (nonatomic, retain) id guideAnimationPic;
@property (nonatomic, retain) NSDate * updateDate;

@end
