//
//  ProductSecondClass.m
//  DianShangApp
//
//  Created by 霞 王 on 14-10-24.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "ProductSecondClass.h"
#import "ProductFirstClass.h"


@implementation ProductSecondClass

@dynamic orderBy;
@dynamic pic_url;
@dynamic secondclassid;
@dynamic title;
@dynamic type;
@dynamic productFirstClass;

@end
