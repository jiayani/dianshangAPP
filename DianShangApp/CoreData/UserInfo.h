//
//  UserInfo.h
//  DianShangApp
//
//  Created by wa on 14-10-17.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class UserextroInfo;

@interface UserInfo : NSManagedObject

@property (nonatomic, retain) NSString * bindQQID;
@property (nonatomic, retain) NSDate * birthday;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * education;
@property (nonatomic, retain) NSString * hobby;
@property (nonatomic, retain) NSNumber * integral;
@property (nonatomic, retain) NSNumber * isBindWoo;
@property (nonatomic, retain) NSString * marriage;
@property (nonatomic, retain) NSString * nickName;
@property (nonatomic, retain) NSString * phoneNumber;
@property (nonatomic, retain) NSString * profession;
@property (nonatomic, retain) NSNumber * sex;
@property (nonatomic, retain) NSString * signature;
@property (nonatomic, retain) NSString * userEmail;
@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSString * userPassword;
@property (nonatomic, retain) NSNumber * wooIntegral;
@property (nonatomic, retain) NSString * wooPassword;
@property (nonatomic, retain) NSString * wooUserName;
@property (nonatomic, retain) NSString * userPhone;
@property (nonatomic, retain) UserextroInfo *userextroInfo;

@end
