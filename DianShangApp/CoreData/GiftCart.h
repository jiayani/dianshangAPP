//
//  GiftCart.h
//  DianShangApp
//
//  Created by 霞 王 on 14-6-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface GiftCart : NSManagedObject

@property (nonatomic, retain) NSNumber * count;
@property (nonatomic, retain) NSNumber * giftCartOrderBy;
@property (nonatomic, retain) NSString * giftid;
@property (nonatomic, retain) NSNumber * integral;
@property (nonatomic, retain) NSNumber * isOnline;
@property (nonatomic, retain) id mode;
@property (nonatomic, retain) NSString * pic_url;
@property (nonatomic, retain) NSNumber * stockCount;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * type;

@end
