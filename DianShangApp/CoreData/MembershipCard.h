//
//  MembershipCard.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MembershipCard : NSManagedObject

@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSString * pic_url;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * istopLevel;
@property (nonatomic, retain) NSNumber * cardLevel;

@end
