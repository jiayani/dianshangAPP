//
//  ProductFirstClass.m
//  DianShangApp
//
//  Created by 霞 王 on 14-10-24.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "ProductFirstClass.h"
#import "ProductSecondClass.h"


@implementation ProductFirstClass

@dynamic firstclassid;
@dynamic orderBy;
@dynamic pic_url;
@dynamic title;
@dynamic type;
@dynamic productSecondClass;

@end
