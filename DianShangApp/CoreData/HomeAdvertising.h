//
//  HomeAdvertising.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface HomeAdvertising : NSManagedObject

@property (nonatomic, retain) NSString * advertisingID;
@property (nonatomic, retain) NSString * advertisingPic;
@property (nonatomic, retain) NSString * advertisingTitle;
@property (nonatomic, retain) NSString * linkInfo;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSDate * updateDate;

@end
