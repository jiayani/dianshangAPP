//
//  ShopCart.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "ShopCart.h"


@implementation ShopCart

@dynamic count;
@dynamic isOnline;
@dynamic mode;
@dynamic pic_url;
@dynamic price;
@dynamic productid;
@dynamic stockCount;
@dynamic title;
@dynamic addDate;

@end
