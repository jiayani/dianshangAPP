//
//  HomeAdvertising.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "HomeAdvertising.h"


@implementation HomeAdvertising

@dynamic advertisingID;
@dynamic advertisingPic;
@dynamic advertisingTitle;
@dynamic linkInfo;
@dynamic type;
@dynamic updateDate;

@end
