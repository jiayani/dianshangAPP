//
//  BindSinaweiboInfo.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BindSinaweiboInfo : NSManagedObject

@property (nonatomic, retain) NSString * sinaexprisein;
@property (nonatomic, retain) NSString * sinatoken;
@property (nonatomic, retain) NSNumber * useremail;
@property (nonatomic, retain) NSNumber * userid;

@end
