//
//  BindQQweiboInfo.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "BindQQweiboInfo.h"


@implementation BindQQweiboInfo

@dynamic useremail;
@dynamic qqnonce;
@dynamic qqsignaturemethod;
@dynamic qqopenid;
@dynamic userid;
@dynamic qqtoken;
@dynamic qqveritier;
@dynamic qqimestap;

@end
