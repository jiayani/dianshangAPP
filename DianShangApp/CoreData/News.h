//
//  News.h
//  DianShangApp
//
//  Created by 霞 王 on 14-6-24.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface News : NSManagedObject

@property (nonatomic, retain) NSNumber * isread;
@property (nonatomic, retain) NSString * newsContent;
@property (nonatomic, retain) id newsData;
@property (nonatomic, retain) NSDate * newsDate;
@property (nonatomic, retain) NSString * newsID;
@property (nonatomic, retain) NSString * newsTitle;
@property (nonatomic, retain) NSNumber * type;

@end
