//
//  ProductAds.m
//  DianShangApp
//
//  Created by wa on 14-7-16.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "ProductAds.h"


@implementation ProductAds

@dynamic ad_orderBy;
@dynamic ad_type;
@dynamic ad_url;
@dynamic adid;
@dynamic pic_url;
@dynamic title;

@end
