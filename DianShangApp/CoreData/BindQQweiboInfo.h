//
//  BindQQweiboInfo.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BindQQweiboInfo : NSManagedObject

@property (nonatomic, retain) NSString * useremail;
@property (nonatomic, retain) NSString * qqnonce;
@property (nonatomic, retain) NSString * qqsignaturemethod;
@property (nonatomic, retain) NSString * qqopenid;
@property (nonatomic, retain) NSNumber * userid;
@property (nonatomic, retain) NSString * qqtoken;
@property (nonatomic, retain) NSString * qqveritier;
@property (nonatomic, retain) NSString * qqimestap;

@end
