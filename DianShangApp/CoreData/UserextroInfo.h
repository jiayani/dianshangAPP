//
//  UserextroInfo.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-19.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class UserInfo;

@interface UserextroInfo : NSManagedObject

@property (nonatomic, retain) NSNumber * cardLevel;
@property (nonatomic, retain) NSString * cardNumber;
@property (nonatomic, retain) NSNumber * giftWaitDeliver;
@property (nonatomic, retain) NSNumber * giftWaitGet;
@property (nonatomic, retain) NSNumber * giftWaitReturn;
@property (nonatomic, retain) NSNumber * integral;
@property (nonatomic, retain) NSString * oldcardNumber;
@property (nonatomic, retain) NSNumber * proWaitDeliver;
@property (nonatomic, retain) NSNumber * proWaitEvaluate;
@property (nonatomic, retain) NSNumber * proWaitGet;
@property (nonatomic, retain) NSNumber * proWaitPay;
@property (nonatomic, retain) NSNumber * totalintegral;
@property (nonatomic, retain) UserInfo *userInfo;

@end
