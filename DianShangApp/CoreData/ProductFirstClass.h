//
//  ProductFirstClass.h
//  DianShangApp
//
//  Created by 霞 王 on 14-10-24.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ProductSecondClass;

@interface ProductFirstClass : NSManagedObject

@property (nonatomic, retain) NSString * firstclassid;
@property (nonatomic, retain) NSNumber * orderBy;
@property (nonatomic, retain) NSString * pic_url;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) NSSet *productSecondClass;
@end

@interface ProductFirstClass (CoreDataGeneratedAccessors)

- (void)addProductSecondClassObject:(ProductSecondClass *)value;
- (void)removeProductSecondClassObject:(ProductSecondClass *)value;
- (void)addProductSecondClass:(NSSet *)values;
- (void)removeProductSecondClass:(NSSet *)values;

@end
