//
//  ProductSecondClass.h
//  DianShangApp
//
//  Created by 霞 王 on 14-10-24.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ProductFirstClass;

@interface ProductSecondClass : NSManagedObject

@property (nonatomic, retain) NSNumber * orderBy;
@property (nonatomic, retain) NSString * pic_url;
@property (nonatomic, retain) NSString * secondclassid;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) ProductFirstClass *productFirstClass;

@end
