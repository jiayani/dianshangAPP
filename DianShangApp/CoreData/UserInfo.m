//
//  UserInfo.m
//  DianShangApp
//
//  Created by wa on 14-10-17.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "UserInfo.h"
#import "UserextroInfo.h"


@implementation UserInfo

@dynamic bindQQID;
@dynamic birthday;
@dynamic city;
@dynamic education;
@dynamic hobby;
@dynamic integral;
@dynamic isBindWoo;
@dynamic marriage;
@dynamic nickName;
@dynamic phoneNumber;
@dynamic profession;
@dynamic sex;
@dynamic signature;
@dynamic userEmail;
@dynamic userId;
@dynamic userPassword;
@dynamic wooIntegral;
@dynamic wooPassword;
@dynamic wooUserName;
@dynamic userPhone;
@dynamic userextroInfo;

@end
