//
//  News.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-24.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "News.h"


@implementation News

@dynamic isread;
@dynamic newsContent;
@dynamic newsData;
@dynamic newsDate;
@dynamic newsID;
@dynamic newsTitle;
@dynamic type;

@end
