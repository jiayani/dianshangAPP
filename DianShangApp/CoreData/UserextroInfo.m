//
//  UserextroInfo.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-19.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "UserextroInfo.h"
#import "UserInfo.h"


@implementation UserextroInfo

@dynamic cardLevel;
@dynamic cardNumber;
@dynamic giftWaitDeliver;
@dynamic giftWaitGet;
@dynamic giftWaitReturn;
@dynamic integral;
@dynamic oldcardNumber;
@dynamic proWaitDeliver;
@dynamic proWaitEvaluate;
@dynamic proWaitGet;
@dynamic proWaitPay;
@dynamic totalintegral;
@dynamic userInfo;

@end
