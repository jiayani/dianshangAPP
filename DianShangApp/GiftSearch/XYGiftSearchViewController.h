//
//  XYGiftSearchViewController.h
//  DianShangApp
//
//  Created by wa on 14-6-11.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableFooterView.h"
#import "XYLoadingView.h"
#import "XYNoDataView.h"
@interface XYGiftSearchViewController : UIViewController<UISearchBarDelegate,EGORefreshTableDelegate>
{
    UISearchBar *serchBar;
    BOOL isHaveMore;
    BOOL reloading;
    EGORefreshTableFooterView *refreshFooterView;
    BOOL setFlag;
    XYLoadingView * loadingView;
    XYNoDataView * noDataView;
    UIButton *cancleBtn;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong,nonatomic) NSString * keyString;
@property(strong,nonatomic) NSString * score;
@property(strong,nonatomic) NSString * tranScore;
@property(assign,nonatomic) int page;
@property(assign,nonatomic) int flag;

@property (nonatomic, retain) NSMutableArray *gifeArray;
- (IBAction)changeGiftBtnAct:(id)sender;
@end
