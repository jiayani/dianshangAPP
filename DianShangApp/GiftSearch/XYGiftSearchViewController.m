//
//  XYGiftSearchViewController.m
//  DianShangApp
//
//  Created by wa on 14-6-11.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XYGiftSearchViewController.h"
#import "WXCommonViewClass.h"
#import "XY_Cloud_getGiftSearchResult.h"
#import "XYGiftInforViewController.h"
#import "FYGifeListTableViewCell.h"
#import "WX_Cloud_IsConnection.h"
#import "AsynImageView.h"
#import "WXConfigDataControll.h"

#define maxCountOfOnePage 8;
@interface XYGiftSearchViewController ()

@end

@implementation XYGiftSearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    /*设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[XY_Cloud_getGiftSearchResult share] setDelegate:nil];
}
- (void)initRefreshFooterView{
    
    int height = MAX(self.tableView.bounds.size.height, self.tableView.contentSize.height);
    refreshFooterView = [[EGORefreshTableFooterView alloc]  initWithFrame:CGRectZero];
    refreshFooterView.frame =CGRectMake(0.0f, height, self.view.frame.size.width, self.tableView.bounds.size.height);
    //如果contentsize的高度比表的高度小，那么就需要把刷新视图放在表的bounds的下面
    
    refreshFooterView.delegate = self;
    //下拉刷新的控件添加在tableView上
    
    [self.tableView addSubview:refreshFooterView];
    reloading = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self createSearchBar];
    [self initRefreshFooterView];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationItem.hidesBackButton = YES;
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)requestGiftList{
    [noDataView removeFromSuperview];
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
    [[XY_Cloud_getGiftSearchResult share] setDelegate:(id)self];
    [[XY_Cloud_getGiftSearchResult share] productListSync:self.keyString :self.score :self.page :self.flag];
}

- (void) createSearchBar {
    serchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0 , -5, 190.0f, 50.0f)];
    [serchBar setPlaceholder:@"请输入搜索关键字                          "];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        [[[[serchBar.subviews objectAtIndex:0]subviews]objectAtIndex:0]removeFromSuperview];
    }
    else {
        for(id img in serchBar.subviews)
        {
            if([img isKindOfClass:NSClassFromString(@"UISearchBarBackground")])
            {
                [img removeFromSuperview];
            }
        }
        serchBar.frame = CGRectMake(0 , -5, 190.0f, 50.0f);
        [serchBar setPlaceholder:@"请输入搜索关键字"];
    }
    serchBar.backgroundColor = [UIColor clearColor];
    [serchBar setContentMode:UIViewContentModeLeft];
    //    [searchBar searchTextPositionAdjustment:UIOffsetMake(20.0, 30.0)];
    serchBar.delegate = self;
    
    cancleBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:NSLocalizedString(@"取消", @"搜索/取消")];
    [cancleBtn addTarget:self action:@selector(cancleBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    cancleBtn.frame =  CGRectMake(190.0f, 5, 49.0, 29.0);
    cancleBtn.hidden = YES;
    
    //将搜索条放在一个UIView上
    UIView *searchView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 245, 44)];
    searchView.backgroundColor = [UIColor clearColor];
    [searchView addSubview:serchBar];
    [searchView addSubview:cancleBtn];
    
    self.navigationItem.titleView = searchView;
    
    [serchBar becomeFirstResponder];
    [self searchBarTextDidBeginEditing:serchBar];
    
}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    serchBar.frame = CGRectMake(0 , -5, 190.0f, 50.0f);
    cancleBtn.hidden = NO;
    
    
}

-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.gifeArray = [[NSMutableArray alloc]init];
    cancleBtn.hidden = YES;
    serchBar.frame = CGRectMake(0 , -5, 190.0f, 50.0f);
    [serchBar resignFirstResponder];
    self.page = 1;
    self.keyString = serchBar.text;
    self.flag = 0;
    self.score = @"-1";
    setFlag = !setFlag;
    
    [self requestGiftList];
}

- (IBAction)cancleBtnPressed:(id)sender {
    cancleBtn.hidden = YES;
    serchBar.text = @"";
    serchBar.frame = CGRectMake(0 , -5, 240.0f, 50.0f);
    [serchBar resignFirstResponder];
}

-(void)getGiftSearchResultSuccess:(NSDictionary *)data
{
    [loadingView removeFromSuperview];
    
    if (self.gifeArray.count == 0) {
        self.gifeArray = [data objectForKey:@"data"];
    }else{
        [self.gifeArray addObjectsFromArray:[data objectForKey:@"data"]];
    }
    NSArray * currentArr = [NSArray arrayWithArray:[data objectForKey:@"data"]];
//    int hasMore = [[data objectForKey:@"hasMore"]intValue];
    if (currentArr.count == 8) {
        isHaveMore = YES;
        int count = 0;
        for (int i = 0; i<self.gifeArray.count; i++) {
            if ([[[self.gifeArray objectAtIndex:i]objectForKey:@"giftType"] intValue] == 1) {
                count += 1;
            }
        }
        self.flag = count;
    }else{
        isHaveMore = NO;
    }
    //刷新UI
    [self reloadUI];
}
-(void)getGiftSearchResultFailed:(NSString*)errMsg
{
    [loadingView removeFromSuperview];
    if (self.page == 1) {
        noDataView =[[XYNoDataView alloc]initWithFrame:self.view.frame];
        noDataView.megUpLabel.text = @"暂时没有数据";
        noDataView.megDownLabel.text = @"先去其他页面看看吧";
        [self.view addSubview:noDataView];
    }
    
     [[XY_Cloud_getGiftSearchResult share] setDelegate:nil];
    isHaveMore = NO;
    [self reloadUI];
    [loadingView removeFromSuperview];
}

#pragma mark  tableView DataSourse

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.gifeArray == nil || self.gifeArray.count == 0){
        return 0;
    }
    return self.gifeArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *str = @"Cell";
    FYGifeListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
    if (cell == nil){
        cell = (FYGifeListTableViewCell *)[[[NSBundle  mainBundle]  loadNibNamed:@"FYGifeListTableViewCell" owner:self options:nil]  lastObject];
        
    }
    // cell = [tableView dequeueReusableCellWithIdentifier:str];
    cell.lbl_gifeName.text = [NSString stringWithFormat:@"%@",[[self.gifeArray objectAtIndex:indexPath.row] objectForKey:@"giftName"]];
    cell.lbl_gifeScore.text = [NSString stringWithFormat:@"%@积分", [[self.gifeArray objectAtIndex:indexPath.row] objectForKey:@"giftIntegral"]];
    cell.lbl_gifeScore.textColor = [WXConfigDataControll getFontColorOfIntegral];
    cell.gifeImageView.type = 1;
    if ([[[self.gifeArray objectAtIndex:indexPath.row] objectForKey:@"giftType"] intValue] == 1) {
        cell.gifeImageView.isFromWooGift = YES;
    }
    cell.gifeImageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    cell.gifeImageView.imageURL = [[[[self.gifeArray objectAtIndex:indexPath.row] objectForKey:@"giftPic"] objectAtIndex:0] objectForKey:@"img"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    XYGiftInforViewController *XYGiftVC = [[XYGiftInforViewController alloc] initWithNibName:@"XYGiftInforViewController" bundle:nil];
    XYGiftVC.giftType = [[[self.gifeArray objectAtIndex:indexPath.row] objectForKey:@"giftType"] intValue];
    XYGiftVC.giftID = [[[self.gifeArray objectAtIndex:indexPath.row] objectForKey:@"giftID"] intValue];
    [self.navigationController pushViewController:XYGiftVC animated:YES];
    
}

#pragma mark - 下拉刷新数据方法
//请求数据
-(void)requestData
{
    reloading = YES;
    BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
    if (isConnection) {//有网络情况
        
        //每次进入该页面的时候，重新从服务器端读取数据
        if(refreshFooterView.hasMoreData)
        {
            
            int myCount = self.gifeArray.count;
            int pageCount = myCount/maxCountOfOnePage;
            self.page = pageCount + 1;
            //下载团购信息
            [self requestGiftList];
            
        }
    }else{//无网络情况
        [WXCommonViewClass showHudInButtonOfView:self.view title:NoHaveNetwork closeInSecond:2];
    }
    
}

-(void)reloadUI
{
    reloading = NO;
    //停止下拉的动作,恢复表格的便宜
	[refreshFooterView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    //更新界面
    [self.tableView reloadData];
    
    if (setFlag) {
        self.tableView.contentOffset = CGPointMake(0, 0);
        setFlag = !setFlag;
    }
    
    [self setRefreshViewFrame];
    
}

-(void)setRefreshViewFrame
{
    //如果contentsize的高度比表的高度小，那么就需要把刷新视图放在表的bounds的下面
    int height = MAX(self.tableView.bounds.size.height, self.tableView.contentSize.height);
    refreshFooterView.frame =CGRectMake(0.0f, height, self.view.frame.size.width, self.tableView.bounds.size.height);
}

#pragma mark - EGORefreshTableFooterDelegate
//出发下拉刷新动作，开始拉取数据
- (void)egoRefreshTableDidTriggerRefresh:(EGORefreshPos)aRefreshPos
{
    [self requestData];
}


//返回当前刷新状态：是否在刷新
- (BOOL)egoRefreshTableDataSourceIsLoading:(UIView*)view{
	
	return reloading; // should return if data source model is reloading
	
}
//返回刷新时间
// if we don't realize this method, it won't display the refresh timestamp
- (NSDate*)egoRefreshTableDataSourceLastUpdated:(UIView*)view
{
	
	return [NSDate date]; // should return date data source was last changed
	
}

#pragma mark - UIScrollView

//此代理在scrollview滚动时就会调用
//在下拉一段距离到提示松开和松开后提示都应该有变化，变化可以在这里实现
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    refreshFooterView.hasMoreData = isHaveMore;
    [refreshFooterView egoRefreshScrollViewDidScroll:scrollView];
}
//松开后判断表格是否在刷新，若在刷新则表格位置偏移，且状态说明文字变化为loading...
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    refreshFooterView.hasMoreData = isHaveMore;
    [refreshFooterView egoRefreshScrollViewDidEndDragging:scrollView];
}



- (IBAction)changeGiftBtnAct:(id)sender {
    WXCommonSingletonClass *  singletonClass  = [WXCommonSingletonClass share];
    if (self.tranScore == NULL || self.tranScore == nil || singletonClass.currentUserId == nil) {
        [WXCommonViewClass showHudInView:self.view title:@"您还没有登录哦！"];
        return;
    }
    self.gifeArray = [[NSMutableArray alloc]init];
    cancleBtn.hidden = YES;
    serchBar.frame = CGRectMake(0 , -5, 240.0f, 50.0f);
    [serchBar resignFirstResponder];
    self.page = 1;
    self.keyString = serchBar.text;
    if (self.keyString == NULL) {
        self.keyString = @"";
    }
    self.flag = 0;
    self.score = self.tranScore;
    setFlag = !setFlag;
    [self requestGiftList];
}
@end
