//
//  WXUnionShopTableViewCell.h
//  TianLvApp
//
//  Created by 霞 王 on 14-8-6.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AsynImageView;
@class WXLabel;

@interface WXUnionShopTableViewCell : UITableViewCell
@property (unsafe_unretained, nonatomic) IBOutlet AsynImageView *shopImageView;
@property (weak, nonatomic) IBOutlet WXLabel *shopDiscribeLabel;
@property (weak, nonatomic) IBOutlet UIButton *downBtn;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIImageView *xiangKuangImageView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *shopNameLabel;
@end
