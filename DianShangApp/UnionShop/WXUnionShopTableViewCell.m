//
//  WXUnionShopTableViewCell.m
//  TianLvApp
//
//  Created by 霞 王 on 14-8-6.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXUnionShopTableViewCell.h"
#import "WXLabel.h"

@implementation WXUnionShopTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.shopDiscribeLabel.verticalAlignment = VerticalAlignmentTop;

    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
