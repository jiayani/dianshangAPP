//
//  WXUnionShopListViewController.m
//  TianLvApp
//
//  Created by 霞 王 on 14-7-7.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXUnionShopListViewController.h"
#import "WXCommonViewClass.h"
#import "WX_Cloud_GetUnionShopList.h"
#import "WXUnionShopTableViewCell.h"
#import "AsynImageView.h"
#import "WX_Cloud_IsConnection.h"
#import "AppDelegate.h"
#import "WXLabel.h"
#import "WXTabBarViewController.h"
#import  "XYLoadingView.h"
#import  "XYNoDataView.h"

@interface WXUnionShopListViewController ()

@end
static NSString *wxCollectionViewCellIdentifier = @"CellId";
static NSString *wxCollectionViewHeaderIdentifier = @"HeaderId";

@implementation WXUnionShopListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"积分联盟", @"积分联盟");
        [WXCommonViewClass hideTabBar:self isHiden:YES];   
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.myTableview.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self initAllControllers];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self syncGetUnionShopList];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[WX_Cloud_GetUnionShopList share] setDelegate:nil];
    [self removeLoading];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - All My Methods
- (void)initAllControllers{
    //begin:返回按钮
    UIButton * backBtn = [WXCommonViewClass getBackButton];
    [backBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftBtnItem;
    //end
    myDataArray = [[NSArray alloc]init];

}
- (void)goBack
{
    if (self.isFromeStarPage) {
        [self.navigationController popViewControllerAnimated:NO];
        WXTabBarViewController *wxBarController = [[AppDelegate delegate] getWXBarViewController];
        UIButton *tempBtn = [wxBarController.tabBar.subviews objectAtIndex:3];
        [wxBarController.tabBar tabBarButtonClicked:tempBtn];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}
- (void)syncGetUnionShopList{
    //本地判断是否连接网络
    BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
    if (isConnection) {//有网络情况
        loadingView = [[XYLoadingView alloc] initWithType:2];
        [self.view addSubview:loadingView];
        //每次进入该页面的时候，重新从服务器端读取数据
        [[WX_Cloud_GetUnionShopList share] setDelegate:(id)self];
        [[WX_Cloud_GetUnionShopList share] getUnionShopList];
    }else{//无网络情况
        [WXCommonViewClass showHudInButtonOfView:self.view title:NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用") closeInSecond:3];
        
    }
    
}
- (void)removeLoading{
    if (loadingView != nil) {
        [loadingView removeFromSuperview];
        loadingView = nil;
    }
}
- (IBAction)wooDownbtnPressed:(UIButton *)downBtn{
    NSDictionary *dic = [myDataArray objectAtIndex:myDataArray.count - 1];
    NSString *urlStr = [dic objectForKey:@"apkUrl"];
    if (urlStr != nil && urlStr.length > 0) {
        NSURL *url = [[NSURL alloc]initWithString:urlStr];
        [[UIApplication sharedApplication] openURL:url];
    }
}
//下载事件
- (void)downbtnPressed:(int)myIndex{
    NSDictionary *dic = [myDataArray objectAtIndex:myIndex];
    NSString *urlStr = [dic objectForKey:@"apkUrl"];
    if (urlStr != nil && urlStr.length > 0) {
        NSURL *url = [[NSURL alloc]initWithString:urlStr];
        [[UIApplication sharedApplication] openURL:url];
    }
}

//去看看，跳转到对应的商家的微网站
- (void)gotoWeiWangZhan:(UIButton *)downBtn{
    NSDictionary *dic = [myDataArray objectAtIndex:downBtn.tag];
    NSString *urlStr = [dic objectForKey:@"wnetUrl"];
    if (urlStr != nil && urlStr.length > 0) {
        NSURL *url = [[NSURL alloc]initWithString:urlStr];
        [[UIApplication sharedApplication] openURL:url];
    }
}
#pragma mark - UITableViewDataSource Mehtods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else{
        if (myDataArray.count > 1) {
            return myDataArray.count - 1;
        }else{
            return  0;
        }
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (cell == nil) {
        if (indexPath.section == 0) {
            cell = [[UITableViewCell alloc]init];
            
            
            self.wooShopImage = [[AsynImageView alloc]initWithFrame:self.xiangKuangImageView.frame];
            [self.headView addSubview:self.wooShopImage];
            self.wooShopImage.type = 0;
            self.wooShopImage.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
            if (myDataArray.count > 0) {
                NSDictionary *dic = [myDataArray objectAtIndex:myDataArray.count - 1];
                //图片
                self.wooShopImage.imageURL = [dic objectForKey:@"shopImageUrl"];
            }
            [cell addSubview:self.headView];

        }else{
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"WXUnionShopTableViewCell" owner:self options:nil];
            cell = (WXUnionShopTableViewCell *)[nib objectAtIndex:0];
            [self setUnionShopTableView:tableView cellForRowAtIndexPath:indexPath cell:(WXUnionShopTableViewCell *)cell];
        }
    }
    //设置选中时没有背景色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)setUnionShopTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath cell:(WXUnionShopTableViewCell *)cell{
    int myRow = indexPath.row;
    int count = myDataArray.count;
    
    if (count > 1) {
        NSDictionary *dic = [myDataArray objectAtIndex:myRow];
        cell.shopNameLabel.text = [dic objectForKey:@"shopName"];
        cell.shopDiscribeLabel.text = [dic objectForKeyedSubscript:@"shopDiscribe"];
        //图片
        cell.shopImageView.type = 0;
        cell.shopImageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
        cell.shopImageView.imageURL = [dic objectForKey:@"shopImageUrl"];
        cell.downBtn.tag = myRow;
        [cell.downBtn addTarget:self action:@selector(gotoWeiWangZhan:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //下载该商家的App
    if (indexPath.section == 0) {
//        [self wooDownbtnPressed:nil];
    }else{
        [self downbtnPressed:indexPath.row];
    }
}

#pragma mark - UITableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return self.headView.frame.size.height;
    }else{
        return 82.0f;
    }
}

#pragma mark - WX_Cloud_Delegate Methods
//无网络和意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    //从服务器端取得最新数据
    [[WX_Cloud_GetUnionShopList share] setDelegate:nil];
       [WXCommonViewClass showHudInButtonOfView:self.view title:errorStr closeInSecond:3];
}
//得到联盟商家的列表 wangxia
- (void)syncGetUnionShopListSuccess:(NSArray *)dataArray{
    [[WX_Cloud_GetUnionShopList share] setDelegate:nil];
    [self removeLoading];
    myDataArray = dataArray;
    [self.myTableview reloadData];
}
- (void)syncGetUnionShopListFailed:(NSString *)errCode{
    [[WX_Cloud_GetUnionShopList share] setDelegate:nil];
    [self removeLoading];
    [WXCommonViewClass showHudInButtonOfView:self.view title:errCode closeInSecond:3];
    
}


@end
