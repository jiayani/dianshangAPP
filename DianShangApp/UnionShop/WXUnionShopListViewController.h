//
//  WXUnionShopListViewController.h
//  TianLvApp
//
//  Created by 霞 王 on 14-7-7.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"
@class XYLoadingView;
@class XYNoDataView;
@class AsynImageView;

@interface WXUnionShopListViewController : UIViewController<WX_Cloud_Delegate,UITableViewDataSource,UITableViewDelegate>{
    NSArray *myDataArray;
    XYLoadingView *loadingView;

}

@property (weak, nonatomic) IBOutlet UITableView *myTableview;
@property (readwrite) BOOL isFromeStarPage;
@property (strong, nonatomic) IBOutlet AsynImageView *wooShopImage;
@property (retain, nonatomic) IBOutlet UIView *headView;
@property (weak, nonatomic) IBOutlet UIImageView *xiangKuangImageView;

@end
