//
//  XY_ProductDatabaseClass.h
//  DianShangApp
//
//  Created by wa on 14-5-21.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductFirstClass.h"
#import "ProductSecondClass.h"
#import "AppDelegate.h"

@interface XY_ProductDatabaseClass : NSObject
/*获取所有一级分类信息集合*/
+ (NSArray *)getAllProductOfLevel1;
@end
