//
//  XY_ProductDatabaseClass.m
//  DianShangApp
//
//  Created by wa on 14-5-21.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XY_ProductDatabaseClass.h"

@implementation XY_ProductDatabaseClass
/*获取所有一级分类信息集合*/
+ (NSArray *)getAllProductOfLevel1{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    NSEntityDescription *entityDescription=[NSEntityDescription entityForName:@"ProductFirstClass" inManagedObjectContext:context];
    NSFetchRequest *request=[[NSFetchRequest alloc]init];
    [request setEntity:entityDescription];
    //按照类型（属性类别、商品类别）、时间倒序
    NSSortDescriptor *sortType = [[NSSortDescriptor alloc] initWithKey:@"type" ascending:YES];
    NSSortDescriptor *sortCollectDate = [[NSSortDescriptor alloc] initWithKey:@"orderBy" ascending:NO];
    NSArray *sortArrary = [NSArray arrayWithObjects:sortType,sortCollectDate, nil];
    [request setSortDescriptors:sortArrary];
    
    
    NSError *error;
    
    NSMutableArray *objects=[NSMutableArray arrayWithArray:[context executeFetchRequest:request error:&error]];
    
    return objects;
}
@end
