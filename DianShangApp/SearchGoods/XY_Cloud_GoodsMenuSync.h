//
//  XY_GoodsMenuSync.h
//  DianShangApp
//
//  Created by wa on 14-5-21.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"
@interface XY_Cloud_GoodsMenuSync : NSObject
{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;
    WXCommonSingletonClass * singletonClass;
}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)syncGoodsAttributeCatalog:(int)flag;
@end
