//
//  XYSearchGoodsViewController.m
//  DianShangApp
//
//  Created by wa on 14-5-21.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XYSearchGoodsViewController.h"
#import "XY_FirstMenuCell.h"
#import "XY_Cloud_GoodsMenuSync.h"
#import "ProductFirstClass.h"
#import "ProductSecondClass.h"
#import "XY_ProductDatabaseClass.h"
#import "WXCommonViewClass.h"
#import "WXCommonSingletonClass.h"
#import "XYGoodsDisplayViewController.h"
#import "WXProductAttributeViewController.h"

/*1级菜单是否选中标示*/
/*被选中的1级菜单,未选中为-1,默认未选中*/
static NSInteger sectionSelected = -1;

@interface XYSearchGoodsViewController ()

@end

@implementation XYSearchGoodsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //设置导航右侧按钮
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"商品服务";
    // Do any additional setup after loading the view from its nib.
    UIButton *rightBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:@"搜索"];
    [rightBtn addTarget:self action:@selector(rightButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[XY_Cloud_GoodsMenuSync share]setDelegate:nil];
}

/*加载数据*/
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self createSearchBar];
    
    [[XY_Cloud_GoodsMenuSync share]setDelegate:(id)self];
    [[XY_Cloud_GoodsMenuSync share]syncGoodsAttributeCatalog:0];

    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)rightButtonPressed
{
    WXProductAttributeViewController *productAttributeVC = [[WXProductAttributeViewController alloc]initWithNibName:@"WXProductAttributeViewController" bundle:nil];
    [self.navigationController pushViewController:productAttributeVC animated:YES];
}

- (IBAction)cancleBtnPressed:(id)sender {
    self.navigationItem.rightBarButtonItem = nil;
    serchBar.text = @"";
    if (iOS7) {
        serchBar.frame = CGRectMake(0 , -5, 300.0f, 50.0f);
    }
    else{
    serchBar.frame = CGRectMake(-22 , -5, 300.0f, 50.0f);
    }
    [serchBar resignFirstResponder];
    
}

- (void) createSearchBar {
    serchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0 , -5, 300.0f, 50.0f)];
    [serchBar setPlaceholder:@"请输入搜索关键字                             "];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        [[[[serchBar.subviews objectAtIndex:0]subviews]objectAtIndex:0]removeFromSuperview];
    }
    else {
    for(id img in serchBar.subviews)
    {
        if([img isKindOfClass:NSClassFromString(@"UISearchBarBackground")])
        {
            [img removeFromSuperview];
        }
    }
        serchBar.frame = CGRectMake(0 , -4, 300.0f, 50.0f);
         [serchBar setPlaceholder:@"请输入搜索关键字"];
}
    serchBar.backgroundColor = [UIColor clearColor];
    [serchBar setContentMode:UIViewContentModeLeft];
//    [searchBar searchTextPositionAdjustment:UIOffsetMake(20.0, 30.0)];
    serchBar.delegate = self;
    
    
    //将搜索条放在一个UIView上
    UIView *searchView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 44)];
    searchView.backgroundColor = [UIColor clearColor];
    [searchView addSubview:serchBar];
    
    self.navigationItem.titleView = searchView;
    
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    serchBar.frame = CGRectMake(0 , -5, 270.0f, 50.0f);
    UIButton * cancleBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:NSLocalizedString(@"取消", @"搜索/取消")];
    [cancleBtn addTarget:self action:@selector(cancleBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:cancleBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}

-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.navigationItem.rightBarButtonItem = nil;
    
    serchBar.frame = CGRectMake(0 , -5, 300.0f, 50.0f);
    XYGoodsDisplayViewController * xyGoodsDisplayVC= [[XYGoodsDisplayViewController alloc] initWithNibName:@"XYGoodsDisplayViewController" bundle:nil];
    xyGoodsDisplayVC.keyword = serchBar.text;
    serchBar.text = @"";
    serchBar.frame = CGRectMake(0 , -5, 280.0f, 50.0f);
    [serchBar resignFirstResponder];
    xyGoodsDisplayVC.type = 0;
    xyGoodsDisplayVC.sortType = 2;
    xyGoodsDisplayVC.page = 1;
    xyGoodsDisplayVC.searchFlag = @"searchFlag";
    [self.navigationController pushViewController:xyGoodsDisplayVC animated:YES];
}


//初始化数据
- (void)initData{

    //加载商品一级分类信息
    _firstMenuArray = [NSMutableArray arrayWithArray:[XY_ProductDatabaseClass getAllProductOfLevel1]];
    //    //使用商品一级分类信息进行占位
    //    _headImageViewArray = [NSMutableArray arrayWithArray:_firstMenuArray];
    
    //加载商品二级分类信息
    _secondMenuArray = [NSMutableArray array];
    for(ProductFirstClass *firstMenu in _firstMenuArray){
        NSSet *set = firstMenu.productSecondClass;
        NSArray *array = [set allObjects];
        NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"orderBy" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sorter];
        NSArray *sortedArray = [array sortedArrayUsingDescriptors:sortDescriptors];
        
        
        [_secondMenuArray addObject:sortedArray];
    }
    [_productTableView reloadData];
}

- (void)addNodataView{
    if (noDataView != nil) {
        [noDataView removeFromSuperview];
        noDataView = nil;
    }
    if (_firstMenuArray == nil || _firstMenuArray.count <= 0) {
        noDataView = [[XYNoDataView alloc] initWithFrame:self.view.frame];
        noDataView.megUpLabel.text = @"暂时没有数据";
        noDataView.megDownLabel.text = @"先去其他页面看看吧";
        [self.view addSubview:noDataView];
    }

}
-(void)syncSuccess:(NSDictionary*)adDic{
    [loadingView removeFromSuperview];
    [self initData];
    [self addNodataView];
}
-(void)syncFailed:(NSString*)errCode{
    [loadingView removeFromSuperview];
    [self addNodataView];
}

#pragma mark - tableviewdelegatge
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//        productListViewController = nil;
//        productListViewController = [[ZJProductListViewController alloc]init];
//        //获取二级菜单集合
//        NSArray *secondMenuArray =[_secondMenuArray objectAtIndex:indexPath.section-1];
//        //获取当前二级菜单
//        SecondMenu *secondMenu = [secondMenuArray objectAtIndex:indexPath.row];
//        FirstMenu *firstMenu = [_firstMenuArray objectAtIndex:indexPath.section -1];
//        productListViewController.firstMenuId = firstMenu.firstMenuID;
//        productListViewController.secondMenuId = secondMenu.secondMenuID;
//        productListViewController.title = secondMenu.secondMenuName;
//        productListViewController.pageIndex = 1;
//        _needLoad = NO;
//        [self.navigationController pushViewController:productListViewController animated:YES];
    
    XYGoodsDisplayViewController * xyGoodsDisplayVC= [[XYGoodsDisplayViewController alloc] initWithNibName:@"XYGoodsDisplayViewController" bundle:nil];
     NSArray *secondMenuArray =[_secondMenuArray objectAtIndex:indexPath.section];
    ProductSecondClass *secondMenu= [secondMenuArray objectAtIndex:indexPath.row];
    ProductFirstClass *firstMenu = [_firstMenuArray objectAtIndex:indexPath.section];
    if ([firstMenu.type intValue] == 1) {
        xyGoodsDisplayVC.att1_id = firstMenu.firstclassid ;
        xyGoodsDisplayVC.att2_id = secondMenu.secondclassid ;
        xyGoodsDisplayVC.type = 6;  //属性商品
    }else{
        xyGoodsDisplayVC.class1_id = [firstMenu.firstclassid intValue];
        xyGoodsDisplayVC.class2_id = [secondMenu.secondclassid intValue];
        xyGoodsDisplayVC.type = 3;
    }
    
    xyGoodsDisplayVC.sortType = 2;
    xyGoodsDisplayVC.page = 1;
    xyGoodsDisplayVC.searchClassStr = secondMenu.title;
    xyGoodsDisplayVC.isKeyWord = NO;
    [self.navigationController pushViewController:xyGoodsDisplayVC animated:YES];

    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

        return 83;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(sectionSelected == -1 && indexPath.section != 0){
        return 0;
    }else{
        //被选中的一级菜单下的二级菜单
        if(indexPath.section == sectionSelected){
            return 55;
        
        }
        else{
            return 0;
        }
    }
}
//自定义section
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

        XY_FirstMenuCell *firstMenuCell;
        firstMenuCell = [[XY_FirstMenuCell alloc]init];
        firstMenuCell.frame = CGRectMake(0, 0, 320, 83);
        if(section == 1){
       
            firstMenuCell.headerLabel.frame = CGRectMake(95,31,177,21);
            firstMenuCell.arrowImageView.frame = CGRectMake(294,36,8,12);
            
       }
    
        firstMenuCell.cleanImageView.layer.masksToBounds = YES;
        firstMenuCell.cleanImageView.layer.cornerRadius = 3;
        if(section == sectionSelected){
            firstMenuCell.arrowImageView.frame = CGRectMake(294, 36, 12, 8);
            firstMenuCell.arrowImageView.image = [UIImage imageNamed:@"UpArrow.png"];
        }else{
            //一级菜单下直接挂商品显示向右的箭头
             firstMenuCell.arrowImageView.frame = CGRectMake(294, 36, 8, 12);
            NSSet *set = [_secondMenuArray objectAtIndex:section];

            if(set == nil || set.count == 0){
                firstMenuCell.arrowImageView.image = [UIImage imageNamed:@"RightArrow.png"];
            }else{
                firstMenuCell.arrowImageView.image = [UIImage imageNamed:@"RightArrow.png"];
            }
            
            
        }
        
        
        
        //        UIColor *bgColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"backgroundImageOfPage1.png"]];
        //        firstMenuCell.backgroundColor = bgColor;
        //        firstMenuCell.headerLabel.font = [UIFont boldSystemFontOfSize:17.0];
        //        firstMenuCell.headerLabel.textColor = [UIColor blackColor];
    
        firstMenuCell.headerLabel.text = [(ProductFirstClass *)[_firstMenuArray objectAtIndex:section ]title];
        AsynImageView * imageView = [[AsynImageView alloc] init];
        imageView.frame = CGRectMake(0, 0, 60, 60);
        
        imageView.type = 1;
        imageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];

        imageView.imageURL = [(ProductFirstClass *)[_firstMenuArray objectAtIndex:section ]pic_url];
        
        [firstMenuCell.headerView addSubview:imageView];
    
        firstMenuCell.tag = section;
        //绑定1级菜单的单击动作
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleViewTap:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [firstMenuCell addGestureRecognizer:tapGestureRecognizer];
        
        return firstMenuCell;
    
}

//绑定section的单击动作
-(void)singleViewTap:(UITapGestureRecognizer*)recognizer
{
    NSInteger sectionClick = recognizer.view.tag;
    NSArray *array = [_secondMenuArray objectAtIndex:sectionClick];
    //当前选中的1级分类下不存在2级分类
    if(array == nil || array.count == 0){
        
        //存在已展开的1级分类
        if(sectionSelected != -1){
            
            NSIndexSet *temp = [NSIndexSet indexSetWithIndex:sectionSelected];
            //已展开1级分类复位
            sectionSelected = -1;
            //关闭已展开1级分类
            [_productTableView reloadSections:temp withRowAnimation:UITableViewRowAnimationNone];
            
        }
        /*
        productListViewController = nil;
        productListViewController = [[ZJProductListViewController alloc]init];
        FirstMenu *firstMenu = [_firstMenuArray objectAtIndex:sectionClick - 1];
        productListViewController.firstMenuId = firstMenu.firstMenuID;
        productListViewController.secondMenuId = nil;
        productListViewController.title = firstMenu.fitstMenuName;
        productListViewController.pageIndex = 1;
        _needLoad = NO;
        [self.navigationController pushViewController:productListViewController animated:YES];
         */
        XYGoodsDisplayViewController * xyGoodsDisplayVC= [[XYGoodsDisplayViewController alloc] initWithNibName:@"XYGoodsDisplayViewController" bundle:nil];
        ProductFirstClass *firstMenu = [_firstMenuArray objectAtIndex:sectionClick];
        if ([firstMenu.type intValue] == 1) {
            xyGoodsDisplayVC.att1_id = firstMenu.firstclassid ;
            xyGoodsDisplayVC.att2_id = @"";
            xyGoodsDisplayVC.type = 6;  //属性商品
        }else{
            xyGoodsDisplayVC.class1_id = [firstMenu.firstclassid intValue];
            xyGoodsDisplayVC.class2_id = 0;
            xyGoodsDisplayVC.type = 3;  //属性商品
        }
        
        xyGoodsDisplayVC.sortType = 2;
        xyGoodsDisplayVC.page = 1;
        xyGoodsDisplayVC.searchClassStr = firstMenu.title;
        xyGoodsDisplayVC.isKeyWord = NO;
        [self.navigationController pushViewController:xyGoodsDisplayVC animated:YES];
        //        [WXCommonViewClass showHudInButtonOfView:self.view title:NSLocalizedString(@"无二级菜单",@"商品及服务/无二级菜单") closeInSecond:3];
    }else{
        
        //当前点击的1级菜单已展开
        if(sectionSelected == sectionClick){
            sectionSelected = -1;
        }else{
            //存在非当前点击的菜单的其他已展开菜单
            if(sectionSelected != -1){
                NSIndexSet *temp = [NSIndexSet indexSetWithIndex:sectionSelected];
                sectionSelected = sectionClick;
                [_productTableView reloadSectionIndexTitles];
                //重置之前打开的箭头状态
                [_productTableView reloadSections:temp withRowAnimation:UITableViewRowAnimationNone];
            }else{
                sectionSelected = sectionClick;
            }
            
        }
        
        [self.productTableView reloadSections:[NSIndexSet indexSetWithIndex:sectionClick] withRowAnimation:UITableViewRowAnimationNone];
        if(sectionSelected != -1){
            //判断该一级菜单下的二级菜单是否超过当前屏幕
            NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:sectionClick];
            [_productTableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        
    }
    
    
}
#pragma mark -tableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    //一级分类的数量
    return _firstMenuArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //二级分类的数量

        NSSet *set = [_secondMenuArray objectAtIndex:(section)];
        if(set == nil){
            return 0;
        }else{
            return [set count];
        }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
     NSString *CellIdentifier = [NSString stringWithFormat:@"Cell"];//以indexPath来唯一确定cell
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                                 CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier: CellIdentifier];
        }

    for (UIView * subview in cell.contentView.subviews) {
        [subview removeFromSuperview];
    }
        NSArray *array = [[_secondMenuArray objectAtIndex:indexPath.section]allObjects];  
        ProductSecondClass *secondMenu = (ProductSecondClass *)[array objectAtIndex:indexPath.row];
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
        if(imageView){
            [imageView removeFromSuperview];
        }

    
        UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(25, 54, 295, 1)];
        lineView.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:239.0/255.0 alpha:1.0];
    
        if(indexPath.row == array.count - 1){
            lineView.frame = CGRectMake(14, 54, 310, 1);
        }
    
        [cell.contentView addSubview:lineView];
        
        
        cell.textLabel.text =[NSString stringWithFormat:@"  %@",secondMenu.title];
        CGRect frame = cell.textLabel.frame;
        cell.textLabel.frame = CGRectMake(20,frame.origin.y,frame.size.width,frame.size.height);
        [cell.textLabel setFont:[UIFont systemFontOfSize:14]];
        cell.textLabel.textColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.backgroundColor = [UIColor clearColor];
        
//        UIView *backgroundView = [[UIView alloc] initWithFrame:cell.frame];
//        backgroundView.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1];
//        cell.backgroundView=backgroundView;
        cell.backgroundColor = [UIColor whiteColor];
        
        return cell;
    }

//无网络连接和意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    //去掉Loading页面
    [loadingView removeFromSuperview];
    [[XY_Cloud_GoodsMenuSync share] setDelegate:nil];
    [self addNodataView];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
    
}
@end
