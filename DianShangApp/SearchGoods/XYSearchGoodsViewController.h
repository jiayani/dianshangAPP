//
//  XYSearchGoodsViewController.h
//  DianShangApp
//
//  Created by wa on 14-5-21.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMQuiltViewCell.h"
#import "TMQuiltView.h"
#import "XYLoadingView.h"
#import "XYNoDataView.h"
#import "WX_Cloud_Delegate.h"
@interface XYSearchGoodsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,WX_Cloud_Delegate>

{
    UISearchBar *serchBar;
    XYLoadingView * loadingView;
    XYNoDataView *noDataView;
}
@property (weak, nonatomic) IBOutlet UITableView *productTableView;

/*一级分类集合*/
@property(strong,nonatomic)NSMutableArray *firstMenuArray;
/*二级分类集合*/
@property(strong,nonatomic)NSMutableArray *secondMenuArray;

@end
