//
//  WXProductAttributeViewController.h
//  TianLvApp
//
//  Created by 霞 王 on 14-8-28.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"
@class XYLoadingView;
@class XYNoDataView;

@interface WXProductAttributeViewController : UIViewController<WX_Cloud_Delegate,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>{
    NSArray *myDataArray;
    XYLoadingView *loadingView;
    XYNoDataView *nodataView;
}

@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@end
