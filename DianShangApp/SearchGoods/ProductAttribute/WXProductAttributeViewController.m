//
//  WXProductAttributeViewController.m
//  TianLvApp
//
//  Created by 霞 王 on 14-8-28.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXProductAttributeViewController.h"
#import "WX_Cloud_GoodsAttributeCatalog.h"
#import "XYLoadingView.h"
#import "WXCommonViewClass.h"
#import "XYGoodsDisplayViewController.h"
#import "XYNoDataView.h"

/*1级菜单是否选中标示*/
/*被选中的1级菜单,未选中为-1,默认未选中*/
static NSInteger sectionSelected = -1;
@interface WXProductAttributeViewController ()

@end

@implementation WXProductAttributeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"搜索";
        //隐藏底部菜单
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initAllControllers];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getProductAttribute];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[WX_Cloud_GoodsAttributeCatalog share]setDelegate:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - All My Methods

- (void)initAllControllers{
    [self addBackbtn];
    self.cancelBtn.hidden = YES;
    self.searchBar.placeholder = @"在全部分类下搜索";
    [self.searchBar setContentMode:UIViewContentModeLeft];
    self.searchBar.tintColor =  [UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1.0];
    if (iOS7) {
        [[[[self.searchBar.subviews objectAtIndex:0]subviews]objectAtIndex:0]removeFromSuperview];
    }else {
        for(id img in self.searchBar.subviews)
        {
            if([img isKindOfClass:NSClassFromString(@"UISearchBarBackground")])
            {
                [img removeFromSuperview];
            }
        }
    }
    
}

- (void)addNoDataView{
    if (nodataView != nil) {
        [nodataView removeFromSuperview];
        nodataView = nil;
    }
    if (myDataArray == nil || myDataArray.count <= 0) {
        nodataView = [[XYNoDataView alloc] init];
        nodataView.megUpLabel.text = @"这里空空如也";
        nodataView.megDownLabel.text = @"请过会来看看吧";
        nodataView.frame =  CGRectMake(0, self.myTableView.frame.origin.y - 2, self.view.frame.size.width, self.myTableView.frame.size.height );
        [self.view addSubview:nodataView];
    }
    
}

- (void)addBackbtn{
    //begin:返回按钮
    UIButton * backBtn = [WXCommonViewClass getBackButton];
    [backBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftBtnItem;
    //end

}
- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)getProductAttribute{
    //向服务器请求一级二级数据
    [[WX_Cloud_GoodsAttributeCatalog share] setDelegate:(id)self];
    [[WX_Cloud_GoodsAttributeCatalog share] syncGoodsAttributeCatalog:1];
}
//一级菜单的单击事件
-(void)firstMenuSingleTap:(UITapGestureRecognizer*)recognizer
{
    NSInteger sectionClick = recognizer.view.tag;
    NSDictionary *tempDic = [myDataArray objectAtIndex:sectionClick];
    NSArray *tempArray = [tempDic objectForKey:@"children"];
    //当前选中的1级分类下不存在2级分类
    if(tempArray == nil || tempArray.count == 0){
        
        //存在已展开的1级分类
        if(sectionSelected != -1){
            NSIndexSet *temp = [NSIndexSet indexSetWithIndex:sectionSelected];
            //已展开1级分类复位
            sectionSelected = -1;
            //关闭已展开1级分类
            [self.myTableView reloadSections:temp withRowAnimation:UITableViewRowAnimationNone];
            
        }
        
        //begin:跳转到下一页面
        [self gotoProductListByClickFirstMenu:sectionClick];
        //en
    }else{
        
        //当前点击的1级菜单已展开
        if(sectionSelected == sectionClick){
            sectionSelected = -1;
        }else{
            //存在非当前点击的菜单的其他已展开菜单
            if(sectionSelected != -1){
                NSIndexSet *temp = [NSIndexSet indexSetWithIndex:sectionSelected];
                sectionSelected = sectionClick;
                [self.myTableView reloadSectionIndexTitles];
                //重置之前打开的箭头状态
                [self.myTableView reloadSections:temp withRowAnimation:UITableViewRowAnimationNone];
            }else{
                sectionSelected = sectionClick;
            }
            
        }
        
        [self.myTableView reloadSections:[NSIndexSet indexSetWithIndex:sectionClick] withRowAnimation:UITableViewRowAnimationNone];
        if(sectionSelected != -1){
            //判断该一级菜单下的二级菜单是否超过当前屏幕
            NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:sectionClick];
            [self.myTableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        
    }


}
- (void)gotoProductListByClickFirstMenu:(int)section{
    NSDictionary *firstDic = [myDataArray objectAtIndex:section];
    XYGoodsDisplayViewController *productListVC = [[XYGoodsDisplayViewController alloc]initWithNibName:@"XYGoodsDisplayViewController" bundle:nil];
    productListVC.type = 0;
    productListVC.sortType = 2; //默认销量降序
    productListVC.page = 1;
    productListVC.class2_id = 0;  //分类ID，属性时无效传0
    productListVC.class1_id = 0;
    productListVC.att1_id = [firstDic objectForKey:@"firstMenuID"] ; //属性ID
    productListVC.att2_id = @"";
    productListVC.searchClassStr = [firstDic objectForKey:@"firstMenuName"];
    productListVC.isKeyWord = NO;
    [self.navigationController pushViewController:productListVC animated:YES];
}
- (IBAction)cancleBtnPressed:(id)sender {
    self.searchBar.text = @"";
    [self.searchBar resignFirstResponder];
    CGRect searchBarFrame = self.searchBar.frame;
    searchBarFrame.size.width = 308;
    self.searchBar.frame = searchBarFrame;
    self.cancelBtn.hidden = YES;
    
    
}
- (void)gotoProductList:(NSString *)keyWords{
    if (keyWords != nil && keyWords.length > 0) {
        XYGoodsDisplayViewController *productListVC = [[XYGoodsDisplayViewController alloc]initWithNibName:@"XYGoodsDisplayViewController" bundle:nil];
        productListVC.keyword = keyWords;
        productListVC.type = 0;
        productListVC.sortType = 2; //默认销量降序
        productListVC.page = 1;
        productListVC.class2_id = 0;  //分类ID，属性时无效传0
        productListVC.class1_id = 0;
        productListVC.att1_id = @"0" ; //属性ID
        productListVC.att2_id = @"0";
        productListVC.searchClassStr = keyWords;
        productListVC.isKeyWord = YES;
        [self.navigationController pushViewController:productListVC animated:YES];
    }
    
}

#pragma mark - UISearchBarDelegate
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    CGRect searchBarFrame = self.searchBar.frame;
    searchBarFrame.size.width = 275;
    self.searchBar.frame = searchBarFrame;
    self.cancelBtn.hidden = NO;
    
}

-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    CGRect searchBarFrame = self.searchBar.frame;
    searchBarFrame.size.width = 308;
    self.searchBar.frame = searchBarFrame;
    self.cancelBtn.hidden = YES;
    [self.searchBar resignFirstResponder];
    //搜索后的操作
    [self gotoProductList:searchBar.text];
}


#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    //一级属性个数
    return myDataArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //二级属性个数
    NSDictionary *tempDic = [myDataArray objectAtIndex:section];
    NSArray *tempArray = [tempDic objectForKey:@"children"];
    if (tempArray) {
        return tempArray.count;
    }else{
        return 0;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault
                                     reuseIdentifier:CellIdentifier];
    }
    cell.contentView.backgroundColor = [UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1.0];
    long section = indexPath.section;
    long row = indexPath.row;
    
    NSDictionary *firstDic = [myDataArray objectAtIndex:section];
    NSArray *secondArray = [firstDic objectForKey:@"children"];

    if(secondArray){
        NSDictionary *secondDic = [secondArray objectAtIndex:row];
        
        cell.textLabel.text = [NSString stringWithFormat:@"  %@", [secondDic objectForKey:@"secondMenuName"]];
        cell.textLabel.textColor = [UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1.0];
        cell.textLabel.font = [UIFont systemFontOfSize:15.0];
    }
    
   
    //设置选中时没有背景色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    //添加一级属性菜单
    CGFloat tableViewWidth = tableView.frame.size.width;
    UIView *firstMenuView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableViewWidth , 44.0f)];
    firstMenuView.tag = section;
    firstMenuView.backgroundColor = [UIColor whiteColor];
    
    //firstMenuName Label
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, tableViewWidth - 30 , 44.0f)];
    titleLabel.font = [UIFont systemFontOfSize:15.0];
    titleLabel.textColor = [UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1.0];
    [firstMenuView addSubview:titleLabel];
    NSDictionary *firstDic = [myDataArray objectAtIndex:section];
    titleLabel.text = [firstDic objectForKey:@"firstMenuName"];
    
    //画线
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 43, tableViewWidth , 1)];
    lineView.backgroundColor = [UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0];
    [firstMenuView addSubview:lineView];
    
    //arrowImageView
    UIImageView *arrowImageView = [[UIImageView alloc]initWithFrame:CGRectMake(tableViewWidth - 30, 19, 10, 7)];
    [firstMenuView addSubview:arrowImageView];
    NSString *imageNameStr ;
    if(section == sectionSelected){
        
        imageNameStr = @"UpArrow.png";
        lineView.hidden = YES;
    }else{
        lineView.hidden = NO;
        //一级菜单下直接挂商品显示向右的箭头
        NSDictionary *tempDic = [myDataArray objectAtIndex:section];
        NSArray *tempArray = [tempDic objectForKey:@"children"];
        if(tempArray == nil || tempArray.count == 0){
            imageNameStr = @"RightArrow.png";
            arrowImageView.frame = CGRectMake(tableViewWidth - 30, 17, 7, 10);
        }else{
            imageNameStr = @"myDownArrow.png";
        }
        
    }
    arrowImageView.image = [UIImage imageNamed:imageNameStr];
    
    
    
    //一级菜单的单击动作
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(firstMenuSingleTap:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [firstMenuView addGestureRecognizer:tapGestureRecognizer];
    
    return firstMenuView;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *firstDic = [myDataArray objectAtIndex:indexPath.section];
    NSArray *secondArray = [firstDic objectForKey:@"children"];
    NSDictionary *secondDic = [secondArray objectAtIndex:indexPath.row];
    XYGoodsDisplayViewController *productListVC = [[XYGoodsDisplayViewController alloc]initWithNibName:@"XYGoodsDisplayViewController" bundle:nil];
    productListVC.type = 6;  //属性商品
    productListVC.sortType = 2; //默认销量降序
    productListVC.page = 1;
    productListVC.class2_id = 0;  //分类ID，属性时无效传0
    productListVC.class1_id = 0;
    productListVC.att1_id = [firstDic objectForKey:@"firstMenuID"] ; //属性ID
    productListVC.att2_id = [secondDic objectForKey:@"secondMenuID"];
    productListVC.searchClassStr = [secondDic objectForKey:@"secondMenuName"];
    productListVC.isKeyWord = NO;
    [self.navigationController pushViewController:productListVC animated:YES];

    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == sectionSelected) {
        return 44.0f;
    }else{
        return 0;
    }
    
}
#pragma mark - WX_Cloud_Delegate Methods
//无网络和意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    //从服务器端取得最新数据
    [[WX_Cloud_GoodsAttributeCatalog share] setDelegate:nil];
    [WXCommonViewClass showHudInButtonOfView:self.view title:errorStr closeInSecond:3];
    [self addNoDataView];
}
//请求成功
- (void)syncGetGoodsAttributeCatalogSuccess:(NSArray *)dataArray{
    [[WX_Cloud_GoodsAttributeCatalog share] setDelegate:nil];
    [loadingView removeFromSuperview];
    myDataArray = dataArray;
    [self.myTableView reloadData];
    [self addNoDataView];
}
//请求失败
- (void)syncGetGoodsAttributeCatalogFailed:(NSString *)errCode{
    [[WX_Cloud_GoodsAttributeCatalog share] setDelegate:nil];
    [loadingView removeFromSuperview];
    if (errCode != nil && errCode.length > 0) {
        [WXCommonViewClass showHudInButtonOfView:self.view title:errCode closeInSecond:3];
    }
    [self addNoDataView];

}

@end
