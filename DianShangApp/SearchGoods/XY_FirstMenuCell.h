//
//  XY_FirstMenuCell.h
//  DianShangApp
//
//  Created by wa on 14-5-22.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsynImageView.h"
@interface XY_FirstMenuCell : UIView

@property(strong,nonatomic)IBOutlet UIImageView *cleanImageView;
@property(strong,nonatomic)IBOutlet UILabel *headerLabel;
//箭头
@property(strong,nonatomic)IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@end
