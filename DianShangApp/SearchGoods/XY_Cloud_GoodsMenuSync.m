//
//  XY_GoodsMenuSync.m
//  DianShangApp
//
//  Created by wa on 14-5-21.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XY_Cloud_GoodsMenuSync.h"
#import "SBJson.h"
#import "AppDelegate.h"
#import "ProductFirstClass.h"
#import "ProductSecondClass.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"

static XY_Cloud_GoodsMenuSync * goodsMenu = nil;
@implementation XY_Cloud_GoodsMenuSync
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!goodsMenu)
    {
        goodsMenu = [[XY_Cloud_GoodsMenuSync alloc] init];
    }
    return goodsMenu;
}

-(void)syncGoodsAttributeCatalog:(int)flag;
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        singletonClass  = [WXCommonSingletonClass share];
        /*发送接口请求*/
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_goodsAttributeCatalog?shopid=%@&flag=%d",singletonClass.shopid,flag]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSArray * array = [dic objectForKey:@"data"];
        AppDelegate * appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        NSManagedObjectContext * context = appDelegate.managedObjectContext;
        
        if (array.count > 0 && array != nil)
        {
            NSEntityDescription * des = [NSEntityDescription entityForName:@"ProductFirstClass" inManagedObjectContext:context];
            NSFetchRequest * request = [[NSFetchRequest alloc] init];
            [request setEntity:des];
            NSArray * allLocalData = [context executeFetchRequest:request error:nil];
            for (int i=0; i<allLocalData.count; i++)
            {
                [context deleteObject:[allLocalData objectAtIndex:i]];
                [context save:nil];
            }
        }
        
        
        
        for (int i=0; i<[array count]; i++)
        {
            
            NSDictionary * menu = [array objectAtIndex:i];
            NSString * firstMenuID = [menu objectForKey:@"firstMenuID"];
            NSString * firstMenuName = [menu objectForKey:@"firstMenuName"];
            NSDictionary * firstMenuPicDic = [menu objectForKey:@"firstMenuPic"];
            NSString * firstMenuPicURLPath = [firstMenuPicDic objectForKey:@"img"];
            if ([firstMenuPicDic objectForKey:@"img"] == [NSNull null])
            {
                firstMenuPicURLPath = @"";
            }
//            NSString * firstImgDate = [firstMenuPicDic objectForKey:@"imgDate"];
            NSString * sortTime = [menu objectForKey:@"orderBy"];
            if ([menu objectForKey:@"orderBy"] == [NSNull null])
            {
                sortTime = @"-1";
            }
            
            
            NSEntityDescription * entity = [NSEntityDescription entityForName:@"ProductFirstClass" inManagedObjectContext:context];
            NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
            NSPredicate * pred = [NSPredicate predicateWithFormat:@"firstclassid=%@",firstMenuID];
            [fetch setEntity:entity];
            [fetch setPredicate:pred];
            NSArray * tempArr = [context executeFetchRequest:fetch error:nil];
            ProductFirstClass * firstMenuObject;
            if ([tempArr count] == 0)
            {
                firstMenuObject = [NSEntityDescription insertNewObjectForEntityForName:@"ProductFirstClass" inManagedObjectContext:context];
            }
            else
            {
                firstMenuObject = [tempArr objectAtIndex:0];
            }
            firstMenuObject.firstclassid = firstMenuID;
            firstMenuObject.title = firstMenuName;
            firstMenuObject.orderBy = [NSNumber numberWithInt:[sortTime intValue]];
            firstMenuObject.type = [menu objectForKey:@"type"];
            firstMenuObject.pic_url = [NSString stringWithFormat:@"%@",firstMenuPicURLPath];
            [context save:nil];
            
            
            NSArray * children = [menu objectForKey:@"children"];
            NSMutableSet * set = [NSMutableSet set];
            for (int j=0; j<[children count]; j++)
            {
                NSDictionary * secMenu = [children objectAtIndex:j];
                NSString * secondMenuID = [secMenu objectForKey:@"secondMenuID"];
                NSString * secondMenuName = [secMenu objectForKey:@"secondMenuName"];
                NSString * sortTime2 = [secMenu objectForKey:@"orderBy"];
                if ([secMenu objectForKey:@"orderBy"] == [NSNull null])
                {
                    sortTime2 = @"-1";
                }
                NSDictionary * secondMenuPicDic = [secMenu objectForKey:@"secondMenuPic"];
                NSString * secondMenuPicURLPath = [secondMenuPicDic objectForKey:@"img"];
                if ([secondMenuPicDic objectForKey:@"img"] == [NSNull null])
                {
                    secondMenuPicURLPath = @"";
                }
//                NSString * secondImgDate = [secondMenuPicDic objectForKey:@"imgDate"];
                
                
                NSEntityDescription * entity = [NSEntityDescription entityForName:@"ProductSecondClass" inManagedObjectContext:context];
                NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
                NSPredicate * pred = [NSPredicate predicateWithFormat:@"secondclassid=%@",secondMenuID];
                [fetch setEntity:entity];
                [fetch setPredicate:pred];
                NSArray * tempArray = [context executeFetchRequest:fetch error:nil];
                ProductSecondClass * secondMenuObject;
                if ([tempArray count] == 0)
                {
                    secondMenuObject = [NSEntityDescription insertNewObjectForEntityForName:@"ProductSecondClass" inManagedObjectContext:context];
                }
                else
                {
                    secondMenuObject = [tempArray objectAtIndex:0];
                }
                secondMenuObject.secondclassid = secondMenuID;
                secondMenuObject.title = secondMenuName;
                secondMenuObject.orderBy = [NSNumber numberWithInt:[sortTime2 intValue]];
                secondMenuObject.type = [secMenu objectForKey:@"type"];
                secondMenuObject.pic_url = [NSString stringWithFormat:@"%@",secondMenuPicURLPath];
                
                [set addObject:secondMenuObject];
                [context save:nil];
            }
            firstMenuObject.productSecondClass = set;
        }
        [delegate syncSuccess:dic];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncFailed:@"请求失败"];
        }
        else
        {
            [delegate syncFailed:[dic objectForKey:@"message"]];
        }
    }
}




@end
