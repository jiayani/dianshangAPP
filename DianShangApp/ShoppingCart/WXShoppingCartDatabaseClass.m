//
//  WXShoppingCartDatabaseClass.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-4.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXShoppingCartDatabaseClass.h"
#import "ShopCart.h"
#import "GiftCart.h"
#import "AppDelegate.h"
#import "WXCommonDataClass.h"

@implementation WXShoppingCartDatabaseClass

/*添加购物车 Yes购物车中已经有弹出提示*/
+ (BOOL)addShopCart:(NSMutableDictionary *)dic{
    
    //添加购物车 如果商品已经存在购物车 则修改数量，不存在添加到购物车中
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[appDelegate managedObjectContext];
    
    NSError *error;
    NSEntityDescription *entityDescription=[NSEntityDescription entityForName:@"ShopCart" inManagedObjectContext:context];
    ShopCart *theObject;
    [context lock];
    //查询本地数据库中是否有该数据 有：进行修改  无：添加一条数据
    NSFetchRequest *request=[[NSFetchRequest alloc]init];
    NSPredicate *pred=[NSPredicate predicateWithFormat:@"(productid=%@)",[dic objectForKey:@"productId"]];
    [request setPredicate:pred];
    [request setEntity:entityDescription];
    
    NSArray *objects=[context executeFetchRequest:request error:&error];
    if (objects!=nil && objects.count>0) {
        for (ShopCart *tempShopCart in objects) {
            if([(NSDictionary *)tempShopCart.mode isEqualToDictionary:[dic objectForKey:@"mode"]]){
                return YES;
            }
        }
        
    }
    theObject=(ShopCart *)[[NSManagedObject alloc]initWithEntity:entityDescription insertIntoManagedObjectContext:context];
    theObject.addDate = [NSDate date];
    
    theObject.productid = [dic objectForKey:@"productId"];
    if ([[dic objectForKey: @"discountPrice"]floatValue] == 0.0f) {
        theObject.price = [self numberWithPrice:[dic objectForKey:@"originPrice"]];

    }else{
        theObject.price = [self numberWithPrice:[dic objectForKey:@"discountPrice"]];;
        
    }
    
    theObject.count = [dic objectForKey:@"count"];
    theObject.title = [dic objectForKey:@"productTitle"];
    theObject.pic_url =[dic objectForKey:@"pic_url"];
    theObject.mode = [dic objectForKey:@"mode"];
    theObject.stockCount = [dic objectForKey:@"stockCount"];
    if ([context save:&error]) {
        [context unlock];
        return NO;
    }else{
        [context unlock];
        return NO;
    }
}
//根据商品的上架状态处理本地数据 flag 1:下架  2:真删除  3:修改
+ (void)addShopCartObjects:(NSArray *)array{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[appDelegate managedObjectContext];
    NSError *error;
    NSEntityDescription *entityDescription=[NSEntityDescription entityForName:@"ShopCart" inManagedObjectContext:context];
    ShopCart *theObject;
    
    [context lock];
    //查询本地数据库中是否有该数据 有：进行修改  无：添加一条数据
    NSFetchRequest *request=[[NSFetchRequest alloc]init];
    for (NSDictionary *dic in array){

        int flag = [[dic objectForKey:@"flag"]intValue];
        NSPredicate *pred=[NSPredicate predicateWithFormat:@"(productid=%@ )",[dic objectForKey:@"dishID"]];
        [request setPredicate:pred];
        [request setEntity:entityDescription];
        NSArray *objects=[context executeFetchRequest:request error:&error];
        if (objects!=nil && objects.count>0) {
            for (ShopCart *tempShopCart in objects) {
                if([(NSDictionary *)tempShopCart.mode isEqualToDictionary:[dic objectForKey:@"dish"]]){
                    theObject=(ShopCart *)[objects objectAtIndex:0];
                    if (flag == 3) {
                        theObject.productid = [dic objectForKey:@"dishID"];
                        theObject.title = [dic objectForKey:@"dishName"];
                        theObject.pic_url =[dic objectForKey:@"dishPic"];
                        theObject.price = [self numberWithPrice:[dic objectForKey:@"dishPrice"]];
                        theObject.isOnline = [dic objectForKey:@"flag"];
                        theObject.stockCount = [dic objectForKey:@"stock"];
                        //如果购买数量大于库存的时候，把数量改为库存
                        if ([theObject.count intValue] > [theObject.stockCount intValue]) {
                            theObject.count = theObject.stockCount;
                        }
                    }else{
                        [context deleteObject:theObject];
                    }
                }
            }
        }
    }
    if ([context save:&error]) {
        [context unlock];
    }else{
        [context unlock];
    }
}

+ (NSNumber *)numberWithPrice:(NSString *)price
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    
    [numberFormatter setPositiveFormat:@"0.00"];
    
    return  [numberFormatter numberFromString:price];
}

//根据礼品的上架状态处理本地数据 state 1:正常上架状态  0:下架
+ (void)addGiftCartObjects:(NSArray *)array{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[appDelegate managedObjectContext];
    NSError *error;
    NSEntityDescription *entityDescription=[NSEntityDescription entityForName:@"GiftCart" inManagedObjectContext:context];
    GiftCart *theObject;
    
    [context lock];
    //查询本地数据库中是否有该数据 有：进行修改  无：添加一条数据
    NSFetchRequest *request=[[NSFetchRequest alloc]init];
    for (NSDictionary *dic in array){
        
        int state = [[dic objectForKey:@"state"]intValue];
        NSPredicate *pred=[NSPredicate predicateWithFormat:@"(giftid=%@)",[dic objectForKey:@"giftId"]];
        [request setPredicate:pred];
        [request setEntity:entityDescription];
        NSArray *objects=[context executeFetchRequest:request error:&error];
        if (objects!=nil && objects.count>0) {
            theObject=(GiftCart *)[objects objectAtIndex:0];
            if (state == 1) {
                theObject.giftid = [dic objectForKey:@"giftId"];
                theObject.title = [dic objectForKey:@"giftName"];
                theObject.type = [NSNumber numberWithInt:[[dic objectForKey:@"giftType"]intValue]];
                int integral = [[dic objectForKey:@"giftIntegral"]intValue];
                theObject.integral = [NSNumber numberWithInt:integral];
                theObject.pic_url =[dic objectForKey:@"giftImg"];
                theObject.stockCount = [NSNumber numberWithInt:[[dic objectForKey:@"stock"]intValue]];
                theObject.isOnline = [NSNumber numberWithInt:[[dic objectForKey:@"state"]intValue]];
                //如果购买数量大于库存的时候，把数量改为库存
                if ([theObject.count intValue] > [theObject.stockCount intValue]) {
                    theObject.count = theObject.stockCount;
                }
            }else{
                [context deleteObject:theObject];
            }
        }
    }
    if ([context save:&error]) {
        [context unlock];
    }else{
        [context unlock];
    }
}

/*获取购物车中的所有商品*/
+ (NSMutableArray *)getAllShoppingCart{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    NSEntityDescription *entityDescription=[NSEntityDescription entityForName:@"ShopCart" inManagedObjectContext:context];
    NSFetchRequest *request=[[NSFetchRequest alloc]init];
    [request setEntity:entityDescription];
    //按照收藏时间倒序
    NSSortDescriptor *sortCollectDate = [[NSSortDescriptor alloc] initWithKey:@"addDate" ascending:NO];
    NSArray *sortArrary = [NSArray arrayWithObjects:sortCollectDate, nil];
    [request setSortDescriptors:sortArrary];
    
    NSError *error;
    [context lock];
    NSMutableArray *objects=[[context executeFetchRequest:request error:&error]mutableCopy];
    [context unlock];
    return objects;
}
/*获取购物车中的所有商品数量*/
+ (int)getAllShoppingCartCount{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    NSEntityDescription *entityDescription=[NSEntityDescription entityForName:@"ShopCart" inManagedObjectContext:context];
    NSFetchRequest *request=[[NSFetchRequest alloc]init];
    [request setEntity:entityDescription];
    NSError *error;
    [context lock];
    NSMutableArray *objects=[[context executeFetchRequest:request error:&error]mutableCopy];
    [context unlock];
    if (objects != nil) {
        return (int)objects.count;
    }else{
        return 0;
    }
}
/*获取购物车中的礼品*/
+ (NSMutableArray *)getAllGiftCart{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    NSEntityDescription *entityDescription=[NSEntityDescription entityForName:@"GiftCart" inManagedObjectContext:context];
    NSFetchRequest *request=[[NSFetchRequest alloc]init];
    [request setEntity:entityDescription];
    //按照收藏时间倒序
    NSSortDescriptor *sortCollectDate = [[NSSortDescriptor alloc] initWithKey:@"giftCartOrderBy" ascending:NO];
    NSArray *sortArrary = [NSArray arrayWithObjects:sortCollectDate, nil];
    [request setSortDescriptors:sortArrary];
    
    NSError *error;
    [context lock];
    NSMutableArray *objects=[[context executeFetchRequest:request error:&error]mutableCopy];
    [context unlock];
    return objects;
}

//删除购物车中的一个商品
+ (BOOL)deleteOneProductCart:(ShopCart *)oneObject{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[appDelegate managedObjectContext];
    NSError *error;
    [context lock];
    [context deleteObject:oneObject];//删除该对象
    
    if ([context save:&error]) {
        [context unlock];
        return YES;
    }else{
        [context unlock];
        return NO;
    }
    
}

//删除购物车中的一个礼品
+ (BOOL)deleteOneGiftCart:(GiftCart *)oneObject{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[appDelegate managedObjectContext];
    NSError *error;
    [context lock];
    [context deleteObject:oneObject];//删除该对象
    
    if ([context save:&error]) {
        [context unlock];
        return YES;
    }else{
        [context unlock];
        return NO;
    }
    
}
@end
