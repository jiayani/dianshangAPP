//
//  WXShoppingCartViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-4-24.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXShoppingCartViewController.h"
#import "WXShoppingCartTableViewCell.h"
#import "CustomSeg.h"
#import "WXCommonViewClass.h"
#import "WXGiftShoppingCartTableViewCell.h"
#import "ShopCart.h"
#import "GiftCart.h"
#import "WXShoppingCartDatabaseClass.h"
#import "WXCommonDataClass.h"
#import "AsynImageView.h"
#import "WX_Cloud_CheckGoodsExist.h"
#import "WXCommonDataClass.h"
#import "WX_Cloud_getGiftCart.h"
#import "FYPayViewController.h"
#import "WXUserLoginViewController.h"
#import "WXUserDatabaseClass.h"
#import "FYGiftPayViewController.h"
#import "UserInfo.h"
#import "WXCommonSingletonClass.h"
#import "CustomNavigationController.h"
#import "XYLoadingView.h"
#import "XYNoDataView.h"
#import "XYSelectedGoodsViewController.h"
#import "AppDelegate.h"
#import "WXTabBarViewController.h"
#import "WX_Cloud_MyCard.h"

@interface WXShoppingCartViewController ()

@end

@implementation WXShoppingCartViewController
@synthesize isGiftShoppingCart;
@synthesize productCartArray;
@synthesize giftCartArray;
@synthesize checkFlagOfGiftArray;
@synthesize checkFlagOfProductArray;
@synthesize dataOfCheckedArray;
@synthesize countOfGiftCartArray;
@synthesize countOfProductCartArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"购物车", @"购物车页面/购物车");
        [WXCommonViewClass hideTabBar:self isHiden:NO];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //bgin:添加礼品购物车和商品购物车的切换按钮
    customSeg = [[[NSBundle mainBundle]loadNibNamed:@"CustomSeg" owner:self options:nil]objectAtIndex:0];
    customSeg.typeSeg = 0;
    customSeg.delegate = self;
    [customSeg setBtn1Title:@"商品购物车" andBtn2Title:@"礼品购物车" andBtn1Image:nil andBtn2Image:nil];
    [self.view addSubview:customSeg];
    self.navigationItem.hidesBackButton = YES;
    
    //end
    //begin:添加导航右侧按钮
    [self addRightRightBarButton];
    //end
    [self initAllControllers];
    
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    isGiftShoppingCart = singletonClass.isGifstShopCard;

    int flagInt = isGiftShoppingCart == YES ? 2 : 1;
    [customSeg segClick:flagInt];
    
    [self initDataIntoByType:flagInt];

    isNotFirstLoad = NO;
    isGiftSetValue = YES;
    isProductSetValue = YES;
    [self.navigationController setNavigationBarHidden:NO animated:NO];

    [self syncGetMyCard];
    [self syncGetGiftData];
    [self syncGetProductData];

}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[WX_Cloud_CheckGoodsExist share] setDelegate:nil];
    [[WX_Cloud_getGiftCart share] setDelegate:nil];
    [[WX_Cloud_MyCard share] setDelegate:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - All My Methods
- (void)initAllControllers{
    
    CGPoint myPoint = self.settleAcountsView.frame.origin;
    CGSize mySize = self.settleAcountsView.frame.size;
    CGFloat pointY;
    if (self.isHaveBack) {
        pointY = self.view.frame.size.height - mySize.height;
    }else{
        pointY = self.view.frame.size.height - 48.0f - mySize.height;
    }
    
        
    self.settleAcountsView.frame = CGRectMake(myPoint.x, pointY, mySize.width, mySize.height);
    isAnimation = YES;
    
}
- (void)initGiftInfo{
    self.giftCartArray = [WXShoppingCartDatabaseClass getAllGiftCart];
    isGiftSetValue = NO;
    self.checkFlagOfGiftArray = [NSMutableArray arrayWithCapacity:self.giftCartArray.count];
    for (int i = 0; i < self.giftCartArray.count; i++) {
        [self.checkFlagOfGiftArray insertObject:[NSNumber numberWithBool:NO] atIndex:i];
    }
}
- (void)initProductInfo{
    self.productCartArray = [WXShoppingCartDatabaseClass getAllShoppingCart];
    isProductSetValue = NO;
    self.checkFlagOfProductArray = [NSMutableArray arrayWithCapacity:self.productCartArray.count];
    for (int i = 0; i < self.productCartArray.count; i++) {
        [self.checkFlagOfProductArray insertObject:[NSNumber numberWithBool:NO] atIndex:i];
    }
}
- (void)reloadData{
    //begin:重新加载数据
    if (isGiftShoppingCart) {
        [self initGiftInfo];
    }else{
        
        [self initProductInfo];
    }
    [self countOfChecked];
    [self.myTableView reloadData];
    if (isGiftShoppingCart) {
        [self giftNodataView];
    }else{
        [self productNodataView];
    }
    [self isHideRightBtn];
}
//同步商品购物车数据
- (void)syncGetProductData{
    [self reloadData];
    if (productCartArray != nil && productCartArray.count > 0) {
        if (loadingView == nil) {
            loadingView = [[XYLoadingView alloc] initWithType:2];
            [self.view addSubview:loadingView];
        }
        NSMutableArray *array = [[NSMutableArray alloc]init];
        for (ShopCart *shopCart in self.productCartArray) {
            [array addObject:shopCart.mode];
        }
        NSString *jsonStr = [WXCommonDataClass toJsonStr:array];
        [[WX_Cloud_CheckGoodsExist share] setDelegate:(id)self];
        [[WX_Cloud_CheckGoodsExist share]checkGoods:jsonStr];

    }
    
}
//同步礼品购物车的数据
- (void)syncGetGiftData{
    [self reloadData];
    if (giftCartArray != nil && giftCartArray.count > 0) {
        if (loadingView == nil) {
            loadingView = [[XYLoadingView alloc] initWithType:2];
            [self.view addSubview:loadingView];
        }
        NSMutableArray *array = [[NSMutableArray alloc]init];
        for (GiftCart *giftCart in self.giftCartArray) {
            [array addObject:giftCart.mode];
        }
        NSString *jsonStr = [WXCommonDataClass toJsonStr:array];
        [[WX_Cloud_getGiftCart share] setDelegate:(id)self];
        [[WX_Cloud_getGiftCart share]syncGiftCart:jsonStr];
    }
    
}
//同步会员卡信息
- (void)syncGetMyCard{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    //从服务器端取得最新数据
    [[WX_Cloud_MyCard share] setDelegate:(id)self];
    [[WX_Cloud_MyCard share] syncGetMyCard:singletonClass.currentUserId];
}
//添加导航上编辑按钮
- (void)addRightRightBarButton{
    //设置导航右侧按钮
    self.rightBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:@"编辑"];
    [self.rightBtn addTarget:self action:@selector(editBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.rightBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
}
//编辑按钮事件
- (void)editBtnPressed{
    isNotFirstLoad = YES;
    isAnimation = YES;
    NSString *btnNameStr,*btnImageNameStr;
    
    if (self.isEditing) {
        btnNameStr = @"编辑";
        btnImageNameStr = @"settlement_bt_cl.png";
        self.isEditing = NO;
        self.settleAcountsBtn.enabled = YES;
        self.selectAllBtn.enabled = YES;
        //begin:单击完成的时候保存数据库
        AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *context=[appDelegate managedObjectContext];
        [context lock];
        [context save:nil];
        [context unlock];
        //end

    }else{
        btnNameStr = @"完成";
        btnImageNameStr = @"settlement_bt_uncl.png";
        self.isEditing = YES;
        self.settleAcountsBtn.enabled = NO;
        self.selectAllBtn.enabled = NO;
        
    }
    [self setBtnEnable];
    [self.rightBtn setTitle:btnNameStr forState:UIControlStateNormal];
    [self.settleAcountsBtn setBackgroundImage:[UIImage imageNamed:btnImageNameStr] forState:UIControlStateNormal];
    [self.myTableView reloadData];
    
}
//在编辑状态下，礼品购物车和商品购物车不能切换
- (void)setBtnEnable{
    if (self.isEditing) {
        customSeg.btn1.enabled = NO;
        customSeg.btn2.enabled = NO;
    }else{
        customSeg.btn1.enabled = YES;
        customSeg.btn2.enabled = YES;
    }
    

}
//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}
//去结算事件
- (IBAction)settleAcountsBtnPressed:(id)sender {
    if (countOfChecked == 0) {
        if (isGiftShoppingCart) {
            
            [WXCommonViewClass showHudInView:self.view title:@"请选择要兑换的礼品"];
        }else{
            [WXCommonViewClass showHudInView:self.view title:@"请选择要结算的商品"];
        }
        
    }else{
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        if (singletonClass.currentUserId == nil) {
            [WXCommonViewClass goToLogin:self.navigationController];
            
        }else{
            if (isGiftShoppingCart) {//礼品进入结算
                [self giftToPay];
                
            }else{//商品结算
                [self productToPay];
            }
        }
        
    }
}
//商品进入结算页面
- (void)productToPay{
    if (isStockCountZero) {
        [WXCommonViewClass showHudInButtonOfView:self.view title:@"商品的数量不能0" closeInSecond:3];
    }else {
        //进入结算页面
        NSMutableArray *tempArray = [[NSMutableArray alloc]init];
        for (int i = 0; i < self.checkFlagOfProductArray.count; i++) {
            BOOL isChecked = [[self.checkFlagOfProductArray objectAtIndex:i]boolValue];
            if (isChecked) {
                [tempArray addObject:[productCartArray objectAtIndex:i] ];
            }
        }
        FYPayViewController *payViewController  = [[FYPayViewController alloc]initWithNibName:@"FYPayViewController" bundle:nil];
        payViewController.productArray = [[NSArray alloc]initWithArray:tempArray];
        payViewController.count = productCoud;
        payViewController.money = totalMoney;
        payViewController.isTuan = NO;
        [self.navigationController pushViewController:payViewController animated:YES];
    }
}

//礼品进入结算页面
- (void)giftToPay{
    if (isStockCountZero) {
        [WXCommonViewClass showHudInButtonOfView:self.view title:@"礼品的数量不能0" closeInSecond:3];
    }else{
        if (myIntegral >= totalIntegration) {
            //进入结算页面
            NSMutableArray *tempArray = [[NSMutableArray alloc]init];
            for (int i = 0; i < self.checkFlagOfGiftArray.count; i++) {
                BOOL isChecked = [[self.checkFlagOfGiftArray objectAtIndex:i]boolValue];
                if (isChecked) {
                    [tempArray addObject:[giftCartArray objectAtIndex:i] ];
                }
            }
            FYGiftPayViewController *giftPayViewController  = [[FYGiftPayViewController alloc]initWithNibName:@"FYGiftPayViewController" bundle:nil];
            giftPayViewController.count = productCoud;
            giftPayViewController.money = totalIntegration;
            giftPayViewController.giftArray = [[NSArray alloc]initWithArray:tempArray];
            [self.navigationController pushViewController:giftPayViewController animated:YES];
        }else{
            [WXCommonViewClass showHudInButtonOfView:self.view title:@"用户积分不足" closeInSecond:3];
        }
        
    }
}

//选中所有的商品
- (IBAction)selectAllBtnPressed:(UIButton *)sender {
    BOOL isChecked;
    NSMutableArray *tempDataArray;
    if (isGiftShoppingCart) {
        tempDataArray = self.giftCartArray;
        if (countOfChecked < self.giftCartArray.count) {
            isChecked = YES;
        }else{
            isChecked = NO;
        }
        for (int i = 0; i < tempDataArray.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:i];
            WXGiftShoppingCartTableViewCell *giftCell = (WXGiftShoppingCartTableViewCell *)[self.myTableView cellForRowAtIndexPath:indexPath];
            [self.checkFlagOfGiftArray replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:isChecked]];
            [self initCheckBtn:giftCell.selectedOneItemBtn isChecked:isChecked];
        }
    }else{
        if (countOfChecked < self.productCartArray.count) {
            isChecked = YES;
        }else{
            isChecked = NO;
        }
        tempDataArray = self.productCartArray;
        for (int i = 0; i < tempDataArray.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:i];
            WXShoppingCartTableViewCell *productCell = (WXShoppingCartTableViewCell *)[self.myTableView cellForRowAtIndexPath:indexPath];
            [self.checkFlagOfProductArray replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:isChecked]];
            [self initCheckBtn:productCell.selectedOneItemBtn isChecked:isChecked];
            
        }
    }
    
    [self countOfChecked];
    
    
}

//切换复选框
- (void)initCheckBtn:(UIButton *)btn isChecked:(BOOL)isChecked{
    NSString *imageNameStr;
    if (isChecked) {
        imageNameStr = @"addDefault.png";
        
    }else{
        imageNameStr = @"notSelect.png";
    }
    [btn setImage:[UIImage imageNamed:imageNameStr] forState:UIControlStateNormal];
}
//选中一个商品
- (void)selectedOneItemBtnPressed:(UIButton *)btn{
    int row = 0;
    BOOL isChecked;
    if (isGiftShoppingCart) {
        
        WXGiftShoppingCartTableViewCell *tempCell;
        if (iOS7) {
            tempCell = (WXGiftShoppingCartTableViewCell *)[[[[[btn superview]superview]superview]superview]superview];
        }else{
            tempCell = (WXGiftShoppingCartTableViewCell *)[[[[btn superview]superview]superview]superview];

        }
        row = [self.myTableView indexPathForCell:tempCell].section;
        isChecked = [[self.checkFlagOfGiftArray objectAtIndex:row] boolValue];
        isChecked = !isChecked;
        [self.checkFlagOfGiftArray replaceObjectAtIndex:row withObject:[NSNumber numberWithBool:isChecked]];
    }else{
        WXShoppingCartTableViewCell *tempCell;
        if (iOS7) {
            tempCell = (WXShoppingCartTableViewCell *)[[[[[btn superview]superview]superview]superview]superview];
        }else{
            tempCell = (WXShoppingCartTableViewCell *)[[[[btn superview]superview]superview]superview];
        }
        row = [self.myTableView indexPathForCell:tempCell].section;
        isChecked = [[self.checkFlagOfProductArray objectAtIndex:row] boolValue];
        isChecked = !isChecked;
        [self.checkFlagOfProductArray replaceObjectAtIndex:row withObject:[NSNumber numberWithBool:isChecked]];
    }
    [self initCheckBtn:btn isChecked:isChecked];
    [self countOfChecked];
    
}
//统计选中商品
- (void)countOfChecked{
    isStockCountZero = NO;
    countOfChecked = 0;
    NSMutableArray *tempArray;
    GiftCart *giftCart ;
    ShopCart *shopCart;
    productCoud = 0;//购买的商品数量
    totalMoney = 0;//总价格
    totalIntegration = 0;//总积分
    int allCount;
    if (isGiftShoppingCart) {
        
        allCount = self.giftCartArray.count;
        tempArray = self.checkFlagOfGiftArray;
        for (int i = 0; i < tempArray.count; i++) {
            BOOL isChecked = [[tempArray objectAtIndex:i]boolValue];
            if (isChecked) {
                countOfChecked++ ;
                giftCart = [self.giftCartArray objectAtIndex:i];
                totalIntegration += [giftCart.integral intValue] * [giftCart.count intValue];
                productCoud += [giftCart.count intValue];
                if ([giftCart.stockCount intValue] == 0) {
                    isStockCountZero = YES;
                }
            }
        }
        self.totalAmountLabel.text = [NSString stringWithFormat:@"%ld",totalIntegration];
    }else{
        allCount = self.productCartArray.count;
        tempArray = self.checkFlagOfProductArray;
        for (int i = 0; i < tempArray.count; i++) {
            BOOL isChecked = [[tempArray objectAtIndex:i]boolValue];
            if (isChecked) {
                countOfChecked++ ;
                shopCart = [self.productCartArray objectAtIndex:i];
                totalMoney += [shopCart.price doubleValue] * [shopCart.count intValue];
                productCoud += [shopCart.count intValue];
                if ([shopCart.stockCount intValue] == 0) {
                    isStockCountZero = YES;
                }
            }
        }
        self.totalAmountLabel.text = [NSString stringWithFormat:@"¥%.2f",totalMoney];
        
    }
    
    self.countLabel.text = [NSString stringWithFormat:@"已选择%d件",countOfChecked];
    if (countOfChecked == allCount && allCount != 0) {
        [self initCheckBtn:self.selectAllBtn isChecked:YES];
    }else{
        [self initCheckBtn:self.selectAllBtn isChecked:NO];
    }
}
//单击添加按钮事件
- (void)addBtnPressed:(UIButton *)btn{
    int row = btn.tag;
    int count;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:row];
    if (isGiftShoppingCart) {
        WXGiftShoppingCartTableViewCell *giftCell = (WXGiftShoppingCartTableViewCell *)[self.myTableView cellForRowAtIndexPath:indexPath];
        count = [giftCell.inputCountTxt.text intValue];
        if (count < giftCell.inputCountTxt.tag) {
            giftCell.inputCountTxt.text = [NSString stringWithFormat:@"%d",++count];
        }
        
    }else{
        WXShoppingCartTableViewCell *producCell = (WXShoppingCartTableViewCell *)[self.myTableView cellForRowAtIndexPath:indexPath];
        count = [producCell.inputCountTxt.text intValue];
        if (count < producCell.inputCountTxt.tag) {
            producCell.inputCountTxt.text = [NSString stringWithFormat:@"%d",++count];
        }
        
    }
    [self updateDatabaseCount:row myNewCount:count];
    
}

//单击减少按钮事件
- (void)decreaseBtnPressed:(UIButton *)btn{
    int row = btn.tag;
    int count;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:row];
    if (isGiftShoppingCart) {
        WXGiftShoppingCartTableViewCell *giftCell = (WXGiftShoppingCartTableViewCell *)[self.myTableView cellForRowAtIndexPath:indexPath];
        count = [giftCell.inputCountTxt.text intValue];
        if (count > 1) {
            giftCell.inputCountTxt.text = [NSString stringWithFormat:@"%d",--count];
        }
        
        
    }else{
        WXShoppingCartTableViewCell *producCell = (WXShoppingCartTableViewCell *)[self.myTableView cellForRowAtIndexPath:indexPath];
        count = [producCell.inputCountTxt.text intValue];
        if (count > 1) {
            producCell.inputCountTxt.text = [NSString stringWithFormat:@"%d",--count];
        }
    }
    [self updateDatabaseCount:row myNewCount:count];
}

//从购物车中删除一件商品
- (void)deleteBtnPressed:(UIButton *)btn{
    isNotFirstLoad = NO;
    int row = btn.tag;
    if (isGiftShoppingCart) {
        GiftCart *oneObject = (GiftCart *)[self.giftCartArray objectAtIndex:row];
        BOOL myBool = [WXShoppingCartDatabaseClass deleteOneGiftCart:oneObject];
        if (myBool) {
            [self.giftCartArray removeObjectAtIndex:row];
            [self.checkFlagOfGiftArray removeObjectAtIndex:row];
            [self.myTableView reloadData];
            if (self.isEditing == YES && self.giftCartArray.count == 0) {
                [self editBtnPressed];
            }
        }
    }else{
        ShopCart *oneObejct = (ShopCart *)[self.productCartArray objectAtIndex:row];
        BOOL myBool = [WXShoppingCartDatabaseClass deleteOneProductCart:oneObejct];
        if (myBool) {
            [self.productCartArray removeObjectAtIndex:row];
            [self.checkFlagOfProductArray removeObjectAtIndex:row];
            [self.myTableView reloadData];
            if (self.isEditing == YES && self.productCartArray.count == 0) {
                [self editBtnPressed];
            }
        }
    }
    [self isHideRightBtn];
    
}

//添加礼品无数据页面
- (void)addGiftNoDataView{
    if (noDataView == nil) {
        noDataView = [[XYNoDataView alloc]initWithFrame:CGRectMake(0, 47, self.view.frame.size.width, self.view.frame.size.height)];
        [self.view addSubview:noDataView];
    }
    noDataView.megUpLabel.text = @"这里空空如也";
    noDataView.megDownLabel.text = @"快去挑选心仪的礼品吧！";
    
}
//添加商品无数据页面
- (void)addProductNoDataView{
    if (noDataView == nil) {
        noDataView = [[XYNoDataView alloc]initWithFrame:CGRectMake(0, 47, self.view.frame.size.width, self.view.frame.size.height)];
        [self.view addSubview:noDataView];
    }
    noDataView.megUpLabel.text = @"这里空空如也";
    noDataView.megDownLabel.text = @"购物车木有东东呀，赶紧去选购您中意的商品或服务吧";

    
}
//移除正在加载的视图
- (void)removeLoadingView{
    if (loadingView != nil) {
        [loadingView removeFromSuperview];
        loadingView = nil;
    }
}
- (void)productNodataView{
    if (productCartArray.count > 0) {
        if (noDataView != nil) {
            [noDataView removeFromSuperview];
            noDataView = nil;
        }
        
    }else{
        [self addProductNoDataView];
    }
}
- (void)giftNodataView{
    if (giftCartArray.count > 0) {
        if (noDataView != nil) {
            [noDataView removeFromSuperview];
            noDataView = nil;
        }
        
    }else{
        [self addGiftNoDataView];
    }
    
}
- (void)updateDatabaseCount:(int)row myNewCount:(int)myNewCount{
    if (isGiftShoppingCart) {
        GiftCart *giftCartObject = [self.giftCartArray objectAtIndex:row];
        giftCartObject.count = [NSNumber numberWithInteger:myNewCount];
    }else{
        ShopCart *productCartObject = [self.productCartArray objectAtIndex:row];
        productCartObject.count = [NSNumber numberWithInteger:myNewCount];
    }
    
}
/*判断右侧编辑按钮是否显示；
 *如果购物车中无东西，
 *则隐藏编辑按钮，否则显示编辑按钮
 */
- (void)isHideRightBtn{
    NSMutableArray *tempArray;
    if (isGiftShoppingCart) {
        tempArray = giftCartArray;
    }else{
        tempArray = productCartArray;
    }
    if (tempArray == nil || tempArray.count <= 0) {
        self.rightBtn.hidden = YES;
    }else{
        self.rightBtn.hidden = NO;
    }
}
//根据是礼品还是商品来初始化页面信息
- (void)initDataIntoByType:(int)index{
    if (index == 2) {
        isGiftShoppingCart = YES;
        [self initGiftInfo];
         self.totalInformationLabel.text = @"总积分:";
         self.totalAmountLabel.text = [NSString stringWithFormat:@"%ld",totalIntegration];
        [self.settleAcountsBtn setTitle:@"兑换" forState:UIControlStateNormal];
        [self giftNodataView];
    }else{
        isGiftShoppingCart = NO;
        [self initProductInfo];
        self.totalInformationLabel.text = @"总金额:";
         self.totalAmountLabel.text = [NSString stringWithFormat:@"¥%.2f",totalMoney];
        [self.settleAcountsBtn setTitle:@"去结算" forState:UIControlStateNormal];
        [self productNodataView];
    }

}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    if (isGiftShoppingCart) {
        return self.giftCartArray.count;
    }else{
        return self.productCartArray.count;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return 1;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d%d", [indexPath row],[indexPath section]];//以indexPath来唯一确定cell
    UITableViewCell *cell;
    
    if (cell == nil) {
        if (isGiftShoppingCart) {
            cell = (WXGiftShoppingCartTableViewCell *)[[WXGiftShoppingCartTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                                                             reuseIdentifier: CellIdentifier];
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"WXGiftShoppingCartTableViewCell" owner:self options:nil];
            cell = (WXGiftShoppingCartTableViewCell *)[nib objectAtIndex:0];
            [self setGiftTableView:tableView cellForRowAtIndexPath:indexPath cell:(WXGiftShoppingCartTableViewCell *)cell];
        }else{
            cell = (WXGiftShoppingCartTableViewCell *)[[WXGiftShoppingCartTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault                                 reuseIdentifier: CellIdentifier];
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"WXShoppingCartTableViewCell" owner:self options:nil];
            cell = (WXShoppingCartTableViewCell *)[nib objectAtIndex:0];
            [self setProductTableView:tableView cellForRowAtIndexPath:indexPath cell:(WXShoppingCartTableViewCell *)cell];
        }
    }
    //为了解决左滑有时候删除按钮不出现的问题
    cell.backgroundView.backgroundColor = [UIColor clearColor];
    //设置选中时没有背景色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)setGiftTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  cell:(WXGiftShoppingCartTableViewCell *)cell{
    [[cell.myView layer] setBorderWidth:1.0f];
    UIColor *borderColor = [UIColor colorWithRed:217.0f/255.0f green:217.0f/255.0f blue:217.0f/255.0f alpha:1.0f];
    [[cell.myView layer] setBorderColor:borderColor.CGColor];
    float sizeW = self.view.frame.size.width;
    CGSize tempSize = cell.editView.frame.size;
    [cell addSubview:cell.editView];
    cell.editView.frame = CGRectMake(sizeW + 1, 0, tempSize.width, tempSize.height);
    if (self.isEditing) {
        if (isNotFirstLoad && isAnimation) {
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.2f];
            cell.dataView.frame = CGRectMake(-tempSize.width, 0, sizeW, tempSize.height);
            cell.editView.frame = CGRectMake(sizeW - tempSize.width, 0, tempSize.width, tempSize.height);
            [UIView commitAnimations];
            
        }else{
            cell.dataView.frame = CGRectMake(-tempSize.width, 0, sizeW, tempSize.height);
            cell.editView.frame = CGRectMake(sizeW - tempSize.width, 0,tempSize.width, tempSize.height);
        }
        
    }else{
        //begin：第一次加载无动画效果
        if (isNotFirstLoad && isAnimation) {
            cell.dataView.frame = CGRectMake(-tempSize.width, 0, sizeW, tempSize.height);
            cell.editView.frame = CGRectMake(sizeW - tempSize.width, 0, tempSize.width, tempSize.height);
            [UIView animateWithDuration:0.2f animations:^(){
                cell.dataView.frame = CGRectMake(0, 0, sizeW, tempSize.height);
                cell.editView.frame = CGRectMake(sizeW + 1, 0,  tempSize.width, tempSize.height);
                
            } completion:^(BOOL finished){
                if (finished) {
                    [cell.editView removeFromSuperview];
                }
                
            }];
            
        }else{
            cell.dataView.frame = CGRectMake(0, 0, sizeW, tempSize.height);
            cell.editView.frame = CGRectMake(sizeW + 1, 0,  tempSize.width, tempSize.height);
            [cell.editView removeFromSuperview];
        }
        //end
    }
    NSInteger row = indexPath.section;
    cell.selectedOneItemBtn.tag = row;
    cell.addBtn.tag = row;
    cell.decreaseBtn.tag = row;
    cell.deleteBtn.tag = row;
    
    //begin:初始化默认复选框状态
    BOOL isChecked = [[self.checkFlagOfGiftArray objectAtIndex:row] boolValue];
    [self initCheckBtn:cell.selectedOneItemBtn isChecked:isChecked];
    //end
    [cell.selectedOneItemBtn addTarget:self action:@selector(selectedOneItemBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    GiftCart *giftCartObject = [self.giftCartArray objectAtIndex:row];
    [cell.addBtn addTarget:self action:@selector(addBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.decreaseBtn addTarget:self action:@selector(decreaseBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.deleteBtn addTarget:self action:@selector(deleteBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    cell.inputCountTxt.text = [NSString stringWithFormat:@"%@", giftCartObject.count];
    cell.inputCountTxt.tag = [giftCartObject.stockCount intValue];
    cell.giftNameLabel.text = giftCartObject.title;
    cell.giftIntegrationLabel.text = [NSString stringWithFormat:@"%@积分   库存数：%@",giftCartObject.integral,giftCartObject.stockCount];
    cell.gifttCountLabel.text = [NSString stringWithFormat:@"数量：%@",giftCartObject.count];
    
    cell.giftImageview.isFromWooGift = YES;
    cell.giftImageview.type = 1;
    cell.giftImageview.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    cell.giftImageview.imageURL = giftCartObject.pic_url;
    
    
}
- (void)setProductTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath cell:(WXShoppingCartTableViewCell *)cell{
    [[cell.myView layer] setBorderWidth:1.0f];
    UIColor *borderColor = [UIColor colorWithRed:217.0f/255.0f green:217.0f/255.0f blue:217.0f/255.0f alpha:1.0f];
    [[cell.myView layer] setBorderColor:borderColor.CGColor];
    //begin:动画效果
    float sizeW = self.view.frame.size.width;
    CGSize tempSize = cell.editView.frame.size;
    [cell addSubview:cell.editView];
    cell.editView.frame = CGRectMake(sizeW + 1, 0, tempSize.width, tempSize.height);
    if (self.isEditing) {
        
        if (isNotFirstLoad && isAnimation) {
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.2f];
            cell.dataView.frame = CGRectMake(-tempSize.width, 0, sizeW, tempSize.height);
            cell.editView.frame = CGRectMake(sizeW -tempSize.width, 0,  tempSize.width, tempSize.height);
            [UIView commitAnimations];
        }else{
            
            cell.dataView.frame = CGRectMake(-tempSize.width, 0, sizeW, tempSize.height);
            cell.editView.frame = CGRectMake(sizeW - tempSize.width, 0, tempSize.width, tempSize.height);
        }
        
    }else{
        //begin：第一次加载无动画效果
        if (isNotFirstLoad && isAnimation) {
            cell.dataView.frame = CGRectMake(-tempSize.width, 0, sizeW, tempSize.height);
            cell.editView.frame = CGRectMake(sizeW - tempSize.width, 0, tempSize.width, tempSize.height);
            [UIView animateWithDuration:0.2f animations:^(){
                cell.dataView.frame = CGRectMake(0, 0, sizeW, tempSize.height);
                cell.editView.frame = CGRectMake(sizeW + 1, 0, tempSize.width, tempSize.height);
                
            } completion:^(BOOL finished){
                if (finished) {
                    [cell.editView removeFromSuperview];
                }
                
            }];
        }else{
            
            cell.dataView.frame = CGRectMake(0, 0, sizeW, tempSize.height);
            cell.editView.frame = CGRectMake(sizeW + 1, 0,  tempSize.width, tempSize.height);
            [cell.editView removeFromSuperview];
        }
        
        //end
    }
    //end
    
    NSInteger row = indexPath.section;
    cell.selectedOneItemBtn.tag = row;
    cell.addBtn.tag = row;
    cell.decreaseBtn.tag = row;
    cell.deleteBtn.tag = row;
    //begin:初始化默认复选框状态
    BOOL isChecked = [[self.checkFlagOfProductArray objectAtIndex:row] boolValue];
    [self initCheckBtn:cell.selectedOneItemBtn isChecked:isChecked];
    //end
    
    [cell.selectedOneItemBtn addTarget:self action:@selector(selectedOneItemBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.addBtn addTarget:self action:@selector(addBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.decreaseBtn addTarget:self action:@selector(decreaseBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.deleteBtn addTarget:self action:@selector(deleteBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    ShopCart *shopCartObject = [self.productCartArray objectAtIndex:row];
    
    cell.productNameLabel.text = shopCartObject.title;
    cell.productPriceLabel.text = [NSString stringWithFormat:@"¥%@    库存数：%@",[WXCommonDataClass getPriceString:shopCartObject.price],shopCartObject.stockCount];
    cell.productCountLabel.text = [NSString stringWithFormat:@"数量: %@",shopCartObject.count];
    NSMutableDictionary *dic = (NSMutableDictionary *)shopCartObject.mode;
    cell.inputCountTxt.text = [NSString stringWithFormat:@"%@", shopCartObject.count];
    cell.inputCountTxt.tag = [shopCartObject.stockCount intValue];
    NSString *modeStr = @"";
    int i = 0;
    for (NSString *str in [dic allKeys]) {
        if ([str hasPrefix:@"value"]) {
            if (i == 0) {
                modeStr = [dic objectForKey:str];
            }else{
                modeStr = [modeStr stringByAppendingFormat:@",%@",[dic objectForKey:str]];
            }
            i++;
            
        }
    }
    cell.productSpecificationLabel.text = [NSString stringWithFormat:@"规格: %@",modeStr];
    //设置异步加载图片
    cell.productImageview.type = 1;
    cell.productImageview.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    cell.productImageview.imageURL = shopCartObject.pic_url;
    
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    if (self.isEditing) {
        return NO;
    }else{
        return YES;
    }
    
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    int row = indexPath.section;
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        if (isGiftShoppingCart) {
            GiftCart *oneObject = (GiftCart *)[self.giftCartArray objectAtIndex:row];
            BOOL myBool = [WXShoppingCartDatabaseClass deleteOneGiftCart:oneObject];
            if (myBool) {
                [self.giftCartArray removeObjectAtIndex:row];
                BOOL isChecked = [[self.checkFlagOfGiftArray objectAtIndex:row]boolValue];
                if (isChecked) {
                    WXShoppingCartTableViewCell *shoppingCartTableViewCell = (WXShoppingCartTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
                    [self selectedOneItemBtnPressed:shoppingCartTableViewCell.selectedOneItemBtn];
                }
                
                [self.checkFlagOfGiftArray removeObjectAtIndex:row];
                if (self.isEditing == YES && self.giftCartArray.count == 0) {
                    [self editBtnPressed];
                }
            }
        }else{
            ShopCart *oneObejct = (ShopCart *)[self.productCartArray objectAtIndex:row];
            BOOL myBool = [WXShoppingCartDatabaseClass deleteOneProductCart:oneObejct];
            if (myBool) {
                 BOOL isChecked = [[self.checkFlagOfProductArray objectAtIndex:row]boolValue];
                if (isChecked) {
                    WXGiftShoppingCartTableViewCell *giftShoppingCartTableViewCell = (WXGiftShoppingCartTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
                    [self selectedOneItemBtnPressed:giftShoppingCartTableViewCell.selectedOneItemBtn];
                }
                [self.productCartArray removeObjectAtIndex:row];
                [self.checkFlagOfProductArray removeObjectAtIndex:row];
                if (self.isEditing == YES && self.productCartArray.count == 0) {
                    [self editBtnPressed];
                }
            }
            
        }
        NSIndexSet *indexSet = [[NSIndexSet alloc]initWithIndex:row];
        [tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationBottom];
        [self isHideRightBtn];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!self.isEditing) {
        if (isGiftShoppingCart) {
            WXGiftShoppingCartTableViewCell *giftCart = (WXGiftShoppingCartTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            [self selectedOneItemBtnPressed:giftCart.selectedOneItemBtn];
        }else{
            WXShoppingCartTableViewCell *shopCart = (WXShoppingCartTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            [self selectedOneItemBtnPressed:shopCart.selectedOneItemBtn];
        }
    }
    
    
}

#pragma mark - Table view delegate
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.isEditing) {
        return NO;
    }else{
        return YES;
    }
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.isEditing) {
        return UITableViewCellEditingStyleNone;
    }else{
        return UITableViewCellEditingStyleDelete;
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"删除";
}
- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (isGiftShoppingCart) {
        return 88.0f;
    }else{
        return 108.0f;
    }
    
}
- (float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0.0f;
    }else{
        return 10.0f;
    }
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 10.0f)];
    view.backgroundColor = [UIColor colorWithRed:245.0f/255.0 green:245.0f/255.0 blue:245.0f/255.0 alpha:1.0];
    view.layer.borderWidth = 0;
    return view;
}
#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    isAnimation = NO;
    
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    isAnimation = YES;
}

#pragma mark - CustomSegDelegate Methods

- (void)segChange:(int)index andIsFirst:(int)firstTag{
    BOOL myBool = NO;
    if (index == 2) {
        myBool = YES;
    }
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.isGifstShopCard = myBool;
    if ((isGiftShoppingCart == NO && index == 1) || ((isGiftShoppingCart == YES && index == 2))) {
        isNotFirstLoad = YES;
        [self isHideRightBtn];
        return;
    }else{
        
        if (isGiftShoppingCart) {
            countOfChecked = giftCartArray.count;
        }else{
            countOfChecked = productCartArray.count;
        }
        [self selectAllBtnPressed:nil];
        [self initDataIntoByType:index];
        isNotFirstLoad = NO;
        [self.myTableView reloadData];
        
    }
    [self isHideRightBtn];


}
#pragma mark - WX_Cloud_Delegate Methods
//无网络和意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[WX_Cloud_CheckGoodsExist share] setDelegate:nil];
    [[WX_Cloud_getGiftCart share] setDelegate:nil];
    [[WX_Cloud_MyCard share] setDelegate:nil];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
    [self removeLoadingView];
    if (isGiftShoppingCart) {
        [self giftNodataView];
    }else{
        [self productNodataView];
    }
    [self isHideRightBtn];
}

-(void)checkGoodsExist:(NSArray *)dataArray{

    [[WX_Cloud_CheckGoodsExist share] setDelegate:nil];
    [self removeLoadingView];
    
    [WXShoppingCartDatabaseClass addShopCartObjects:dataArray];
    [self reloadData];

    
}
-(void)checkGoodsExistFailed:(NSString*)errCode{
    [[WX_Cloud_CheckGoodsExist share] setDelegate:nil];
    [self removeLoadingView];
    [self productNodataView];
    [self isHideRightBtn];

}
#pragma mark - GiftCartDelegate
//同步礼品购物车
-(void)syncGetGiftCartScuccess:(NSArray *)dataArray{
    [[WX_Cloud_getGiftCart share] setDelegate:nil];
    [self removeLoadingView];

    [WXShoppingCartDatabaseClass addGiftCartObjects:dataArray];
    [self reloadData];

}
-(void)syncGetGiftCartFailed:(NSString*)errCode{
    [[WX_Cloud_getGiftCart share] setDelegate:nil];
    [self removeLoadingView];
    [self giftNodataView];
    [self isHideRightBtn];
    
}
#pragma mark -   获得会员卡信息
-(void)syncGetMyCardSuccess:(NSDictionary *)dataDic{
    [[WX_Cloud_MyCard share] setDelegate:nil];
    myIntegral = [[dataDic objectForKey:@"cardIntegral"]intValue];
}

-(void)syncGetMyCardFailed:(NSString *)errCode{
    [[WX_Cloud_MyCard share] setDelegate:nil];
    
}
@end
