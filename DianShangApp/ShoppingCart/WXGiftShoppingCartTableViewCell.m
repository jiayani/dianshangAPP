//
//  WXGiftShoppingCartTableViewCell.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-30.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXGiftShoppingCartTableViewCell.h"

@implementation WXGiftShoppingCartTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    self.deleteBtn .titleLabel.numberOfLines=0;
    self.deleteBtn.titleLabel.text = @"删\n除";
    [self.deleteBtn setTitle:@"删\n除" forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
