//
//  WXShoppingCartViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-4-24.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomSeg.h"
#import "WX_Cloud_Delegate.h"
@class XYLoadingView;
@class XYNoDataView;

@interface WXShoppingCartViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,CustomSegDelegate,WX_Cloud_Delegate>{
    BOOL isNotFirstLoad;//是否该页面是第一次加载
    
    int pageCount;
    float pageWidth;
    CustomSeg *customSeg;
    BOOL isAnimation;
    CGFloat scrollMoveY;
    int countOfChecked;
    int productCoud;
    double totalMoney ;//总价格
    long totalIntegration ;//总积分
    XYLoadingView *loadingView;
    XYNoDataView *noDataView;
    BOOL isGiftSetValue;
    BOOL isProductSetValue;
    BOOL isStockCountZero;//判断选择的商品库存是否为0
    int myIntegral;//从服务器端得到最新的积分
    

}
@property (readwrite) BOOL needLoad;
@property (readwrite) BOOL isEditing;
@property (readwrite) BOOL isHaveBack;
@property (readwrite) BOOL isGiftShoppingCart;//是否是显示的是礼品购物车
//绑定数据源
@property (strong,nonatomic) NSMutableArray *productCartArray;
@property (strong,nonatomic) NSMutableArray *giftCartArray;
//显示被选中的状态的
@property (strong,nonatomic) NSMutableArray *checkFlagOfGiftArray;
@property (strong,nonatomic) NSMutableArray *checkFlagOfProductArray;
//礼品,商品的数量
@property (strong,nonatomic) NSMutableArray *countOfGiftCartArray;
@property (strong,nonatomic) NSMutableArray *countOfProductCartArray;

//选中的商品
@property (strong,nonatomic) NSMutableArray *dataOfCheckedArray;

@property (weak,nonatomic) IBOutlet UIButton *rightBtn;

@property (weak, nonatomic) IBOutlet UITableView *myTableView;
//begin：结算view上的控件
@property (weak, nonatomic) IBOutlet UILabel *totalInformationLabel;

@property (weak, nonatomic) IBOutlet UIView *settleAcountsView;
@property (weak, nonatomic) IBOutlet UIButton *selectAllBtn;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;
@property (weak, nonatomic) IBOutlet UIButton *settleAcountsBtn;
//end


//去结算事件
- (IBAction)settleAcountsBtnPressed:(id)sender;

//选中所有的商品
- (IBAction)selectAllBtnPressed:(UIButton *)sender;

//选中一个商品
- (void)selectedOneItemBtnPressed:(UIButton *)btn;

//单击添加按钮事件
- (void)addBtnPressed:(UIButton *)btn;

//单击减少按钮事件
- (void)decreaseBtnPressed:(UIButton *)btn;

//从购物车中删除一件商品
- (void)deleteBtnPressed:(UIButton *)btn;

//统计选中商品
- (void)countOfChecked;

//切换复选框
- (void)initCheckBtn:(UIButton *)btn isChecked:(BOOL)isChecked;
@end
