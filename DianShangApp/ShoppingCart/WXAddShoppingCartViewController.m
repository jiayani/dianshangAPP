//
//  WXAddShoppingCartViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-6.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXAddShoppingCartViewController.h"
#import "WXCommonViewClass.h"
#import "WX_Cloud_UpdateAttributeOfProduct.h"
#import "WXShoppingCartDatabaseClass.h"
#import "WXShoppingCartViewController.h"
#import "XYNoDataView.h"
#import "AppDelegate.h"
#import "XYSelectedGoodsViewController.h"
#import "WXConfigDataControll.h"

@interface WXAddShoppingCartViewController ()

@end


@implementation WXAddShoppingCartViewController
@synthesize dataDic;
@synthesize productDic;
@synthesize attributeView;
@synthesize labelArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"加入购物车", @"购物车页面/加入购物车");
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initProductInfo];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //显示导航
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    //添加无数据视图
    if (dataDic == nil) {
        if (nodataView == nil) {
            nodataView = [[XYNoDataView alloc]initWithFrame:self.view.frame];
            [self.view addSubview:nodataView];
        }
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - MyAllMethods

//初始化商品的基本信息：图片、价格、名称
- (void)initProductInfo{
    //记录这个属性的最大宽度
    viewWidth = self.myScrollView.frame.size.width - 10;
    labelPointY = 0.0f;
    btnPointY = 0.0f;
    buyCount = 1;
    self.priceLabel.textColor = [WXConfigDataControll getFontColorOfPrice];
    /*begin设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    //end
    selectedAttibuteDic = [[NSMutableDictionary alloc]init];
    [selectedAttibuteDic setValue:[self.productDic objectForKey:@"productId"] forKey:@"productId"];
    self.productNameLabel.text = [self.productDic objectForKey:@"productTitle"];
    
    if ([[self.productDic objectForKey: @"discountPrice"]floatValue] == 0.0f) {
        self.priceLabel.text =[NSString stringWithFormat:@"¥%@",[self.productDic objectForKey: @"originPrice"]] ;
    }else{
        self.priceLabel.text = [NSString stringWithFormat:@"¥%@",[self.productDic objectForKey: @"discountPrice"]];
    }
    
    NSArray *picArray = [self.productDic objectForKey:@"productPics"];
    self.productImage.type = 1;
    self.productImage.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    if (picArray != nil) {
        NSDictionary *picDic = [picArray objectAtIndex:0];
        self.productImage.imageURL = [picDic objectForKey:@"img"];
        [self.productDic setValue:[picDic objectForKey:@"img"] forKey:@"pic_url"];
    }else{
        self.productImage.imageURL = @"";
        [self.productDic setValue:@"" forKey:@"pic_url"];
    }
    
    //动态的创建属性
    [self createAttributes];

}
//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}
//动态创建属性
- (void)createAttributes{
    
    clickedBtnDic = [[NSMutableDictionary alloc]init];
    formatsArray = [dataDic objectForKey:@"formats"];
    stockArray = [dataDic objectForKey:@"stock"];
    self.labelArray = [NSMutableArray arrayWithCapacity:formatsArray.count];
    buttonDic = [[NSMutableDictionary alloc]init];//初始化属性按钮字典
    for (int i = 0; i < formatsArray.count; i++) {
        NSString *tempKeyStr = [NSString stringWithFormat:@"%d",i];

        NSDictionary *tempDic = [formatsArray objectAtIndex:i];
        NSString *labeTitleStr = [tempDic objectForKey:@"formatTypeName"];
        //画Label
        UILabel *myLabel = [self createAttributeLabels:labeTitleStr count:i tag:i];
        btnPointY = labelPointY + myLabel.frame.size.height + 10.0f;
        //画Button
        NSArray *buttonTitleArray = [tempDic objectForKey:@"formatValues"];
        NSMutableArray *tempButtonArray = [[NSMutableArray alloc]init];
        int j = 0;
        CGFloat lastHeightOfBtn;
        savePointX = 0;
        isEnter = YES;
        for (NSString *tempStr in buttonTitleArray) {
            BOOL isHaveStock = YES;
            if (i == 0) {
                //如果是第一类属性需要判断有没有库存
                isHaveStock = [self isHaveStockByInitBtn:i second:j];
                
            }
            UIButton *button = [self createAttributeButtons:tempStr isHave:isHaveStock  tag:i * 20 + j];
            lastHeightOfBtn = button.frame.size.height;
            [tempButtonArray addObject:button];
            j++;
        }
        labelPointY = btnPointY + lastHeightOfBtn + 10.0f ;
        //先把Button保存下来，为了以后单选的时候改变按钮样式
        [buttonDic setValue:tempButtonArray forKey:tempKeyStr];
        
    }
    
    //创建完属性后，把countView添加到后面
    CGRect frame = self.countView.frame;
    frame.origin.y = labelPointY ;
    self.countView.frame = frame;
    [self.myScrollView addSubview:self.countView];
    //改变UIScrollView的contentSize
    self.myScrollView.contentSize = CGSizeMake(viewWidth, frame.size.height + frame.origin.y + 50);
    
    //计算库存总数
    maxCount = 0;
    for (NSString *str in stockArray) {
        maxCount += [str intValue];
    }
     self.stockCountLabel.text = [NSString stringWithFormat:@"库存: %ld",maxCount];
}
/*找出属性中最长的属性
  根据最长的属性计算出属性按钮的width,height
 */
- (void)getMaxLengthStr:(NSArray *)buttonTitleArray{
    int maxLenth = 0;
    for (NSString *tempStr in buttonTitleArray) {
        int tempLenth = tempStr.length;
        if (tempLenth > maxLenth) {
            maxLenth = tempLenth;
        }else{
            continue;
        }
    }
    
}
- (UIButton *)createAttributeButtons:(NSString *)titleStr  isHave:(BOOL)isHave tag:(int)tag {

    CGFloat tempviewWidth;
    CGFloat widthF = 90.0f;
    
    int myCount = titleStr.length - 5 ;
    if (myCount > 0) {
        widthF = 90.0f + (myCount * 15);
    }
    tempviewWidth = widthF > viewWidth ? viewWidth : widthF;
    if (savePointX == 0) {
        savePointX = 10;
    }
    CGFloat tempPointX = savePointX;
    tempPointX += 10 + widthF;
    if (tempPointX > viewWidth) {
        savePointX = 10;
        if (proBtn != nil) {
            isEnter = YES;
        }
    }else{
        if (proBtn != nil) {
            isEnter = NO;
        }
        
    }
    if (isEnter) {
        if (proBtn != nil) {
            btnPointY += proBtn.frame.size.height + 10.0f;
        }
        
    }
    
    UIButton *myButton = [UIButton buttonWithType:UIButtonTypeCustom];
    myButton.tag = tag;//用tag来标记所单击的按钮属于第几类属性下的属性
    myButton.layer.cornerRadius = 4.0f;
    myButton.layer.borderWidth = 1.0f;
    myButton.showsTouchWhenHighlighted = NO;
    
    //让按钮高度自适应
    myButton.titleLabel.numberOfLines = 0;
    myButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [myButton setTitle:titleStr forState:UIControlStateNormal];
    myButton.layer.borderColor = [UIColor whiteColor].CGColor;
    myButton.backgroundColor = [UIColor whiteColor];
    UIFont *font = [UIFont systemFontOfSize:14.0f];
    //设置一个行高上限
    CGSize size = CGSizeMake(tempviewWidth,2000);
    //计算实际frame大小，并将label的frame变成实际大小
    CGSize labelsize = [titleStr sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
    if (labelsize.width < 90) {
        labelsize.width = 90;
    }
    if (labelsize.height < 36) {
        labelsize.height = 36;
    }
    myButton.frame = CGRectMake(savePointX, btnPointY, labelsize.width, labelsize.height);
    savePointX += labelsize.width + 10;

    myButton.titleLabel.font = font;
    [myButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self updateButtonStyle:myButton isHave:isHave];
    [self.myScrollView addSubview:myButton];
    
    NSString *keyStr = [NSString stringWithFormat:@"%d",tag];
    NSArray *btnArray = [buttonDic objectForKey:keyStr];
    if (btnArray == nil || btnArray.count == 0) {
        proBtn = myButton;
    }
    return myButton;
    
}
//根据库存来变更按钮的样式
- (void)updateButtonStyle:(UIButton *)myButton isHave:(BOOL)isHave{
    UIColor *color;
    if (isHave) {
        //有库存
        color = [UIColor colorWithRed:43.0f/255.0f green:43.0f/255.0f blue:43.0f/255.0f alpha:1.0];
        myButton.enabled = YES;
    }else{
        //无库存
        color = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0];
        myButton.enabled = NO;
    }
     [myButton setTitleColor:color forState:UIControlStateNormal];

}
//动态的创建Label
- (UILabel *)createAttributeLabels:(NSString *)titleStr count:(int)count tag:(int)tag{
    //初始化label
    UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0,0,0)];
    myLabel.backgroundColor = [UIColor clearColor];
    myLabel.tag = tag;
    [myLabel setNumberOfLines:0];
    myLabel.lineBreakMode = NSLineBreakByWordWrapping;
    myLabel.text = titleStr;
    UIFont *font = [UIFont systemFontOfSize:14.0f];
    //设置一个行高上限
    CGSize size = CGSizeMake(viewWidth,2000);
    //计算实际frame大小，并将label的frame变成实际大小
    CGSize labelsize = [titleStr sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
    myLabel.frame = CGRectMake(10.0f,labelPointY, labelsize.width, labelsize.height);
    myLabel.textColor = [UIColor colorWithRed:43.0f/255.0f green:43.0f/255.0f blue:43.0f/255.0f alpha:1.0];
    myLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.myScrollView addSubview:myLabel];
    [self.labelArray addObject:myLabel];
    return myLabel;
    
}
/*属性按钮的单击事件
 */
- (void)buttonPressed:(UIButton *)button{
    int tag = button.tag;
    int firstIndex = tag / 20;
    int secondInde = tag % 20;
    NSString *keyStr = [NSString stringWithFormat:@"%d",firstIndex];
    
    //取出前一个选中的按钮,把该按钮改为不选中的状态
    UIButton *preButton = [clickedBtnDic objectForKey:keyStr];
    if (preButton != nil) {
        preButton.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    //修改当前选中的按钮的样式
    button.layer.borderColor = [UIColor colorWithRed:253.0f/255.0f green:75.0f/255.0f blue:77.0f/255.0f alpha:1.0].CGColor;
    [clickedBtnDic setObject:button forKey:keyStr];
    if (formatsArray.count == 2) {
        [self setSecondBtnEnable:firstIndex secondIndex:secondInde];
    }
    
    //选中所有属性时才显示库存数量
    if (clickedBtnDic.count == formatsArray.count) {
        [self isHaveStock];
        //购买数量超出库存数量的时候，显示库存数量
        if ([self.inputCountTxt.text intValue] > maxCount) {
            self.inputCountTxt.text = [NSString stringWithFormat:@"%ld",maxCount];
        }
        
    }
    //把对应的属性内容添加到字典中
    NSDictionary *tempDic = [formatsArray objectAtIndex:firstIndex];
    NSString *labeTitleStr = [tempDic objectForKey:@"formatTypeName"];
    NSString *type = [NSString stringWithFormat:@"type%d",firstIndex + 1];
    NSString *value = [NSString stringWithFormat:@"value%d",firstIndex + 1];
    [selectedAttibuteDic setObject:labeTitleStr forKey:type];
    [selectedAttibuteDic setObject:button.titleLabel.text forKey:value];
}
/*第一类属性初始化的时候判断有没有库存
 *有没有库存
 */
- (BOOL)isHaveStockByInitBtn:(int)firstIndex  second:(int)secondIndex {
    int tempCount = formatsArray.count;
    int stock = 0 ;
    if (tempCount == 1) {
    //只有一类属性时
        stock = [[stockArray objectAtIndex:secondIndex]intValue];
    }else if (tempCount == 2){
    //有2类属性
        NSDictionary *tempDic = [formatsArray objectAtIndex:1];
        NSArray *tempArray = [tempDic objectForKey:@"formatValues"];
        int secondCount = tempArray.count;
        if (firstIndex == 0) {//第一类属性
            
            int index = secondIndex * secondCount;
            for (int i = 0; i < secondCount; i++) {
                stock += [[stockArray objectAtIndex:index++]intValue];
            }
        }else if (firstIndex == 1){
            //第二类属性
            NSDictionary *tempDic = [formatsArray objectAtIndex:0];
            NSArray *tempArray = [tempDic objectForKey:@"formatValues"];
            int firstCount = tempArray.count;
            int index = secondIndex;
            for (int i = secondIndex; i < firstCount; i += secondCount) {
                stock += [[stockArray objectAtIndex:index++]intValue];
            }
        }
    }
    if (stock > 0) {
        return YES;
    }else{
        return NO;
    }
}
/*根据选择的属性来得到最大库存量
 *有没有库存
 */
- (BOOL)isHaveStock{
    maxCount = 0;
    //计算该属性对应库存的索引
    int tempCount = formatsArray.count;
    int indext = 0;
    for (int i = 0; i < tempCount - 1; i++) {
         UIButton *tempBtn = [clickedBtnDic objectForKey:[NSString stringWithFormat:@"%d",i]];
         indext = tempBtn.tag % 20;
         for (int j = i + 1; j < tempCount; j++) {
            NSDictionary *tempDic = [formatsArray objectAtIndex:j];
            NSArray *tempArray = [tempDic objectForKey:@"formatValues"];
            int b = 0;
            if (tempArray != nil) {
                b = tempArray.count;
            }
            indext *= b ;
        }
    }
    UIButton *tempBtn = [clickedBtnDic objectForKey:[NSString stringWithFormat:@"%d",tempCount - 1]];
    indext += tempBtn.tag % 20 + 1;
    maxCount += [[stockArray objectAtIndex:indext - 1]intValue];
    self.stockCountLabel.text = [NSString stringWithFormat:@"库存: %ld",maxCount];
    if (maxCount > 0) {
        return YES;
    }else{
        return NO;
    }
}
//如果是2个属性 选中第一个时，判断第二个是不是有库存，来判断他能不能选择
- (void)setSecondBtnEnable:(int)firstIndex secondIndex:(int)secondIndex{
    NSDictionary *tempDic = [formatsArray objectAtIndex:1];
    NSArray *tempArray = [tempDic objectForKey:@"formatValues"];
    int secondCount = tempArray.count;
    NSString *keyStr;
    long stock = 0;
    BOOL isHaveStock;
    if (firstIndex == 0) {//第一类属性
        int index = secondIndex * secondCount;
        keyStr = [NSString stringWithFormat:@"1"];
        for (int i = 0; i < secondCount; i++) {
            stock = [[stockArray objectAtIndex:index++]intValue];
            if (stock == 0) {
                isHaveStock = NO;
            }else{
                isHaveStock = YES;
            }
            NSArray *secondBtnArray = [buttonDic objectForKey:keyStr];
            UIButton *tempBtn = [secondBtnArray objectAtIndex:i];
            [self updateButtonStyle:tempBtn isHave:isHaveStock];
        }
    }else if (firstIndex == 1){
        //第二类属性
        NSDictionary *tempDic = [formatsArray objectAtIndex:0];
        NSArray *tempArray = [tempDic objectForKey:@"formatValues"];
        int firstCount = tempArray.count;
        int index = secondIndex;
        keyStr = [NSString stringWithFormat:@"0"];
        for (int i = 0; i < firstCount; i ++) {
            stock = [[stockArray objectAtIndex:index]intValue];
            index += secondCount;
            if (stock == 0) {
                isHaveStock = NO;
            }else{
                isHaveStock = YES;
            }
            NSArray *secondBtnArray = [buttonDic objectForKey:keyStr];
            UIButton *tempBtn = [secondBtnArray objectAtIndex:i];
            [self updateButtonStyle:tempBtn isHave:isHaveStock];
        }

    }
}
//单击添加按钮事件
- (void)addBtnPressed:(UIButton *)btn{
    int count = [self.inputCountTxt.text intValue];
    if (count < maxCount) {
        self.inputCountTxt.text = [NSString stringWithFormat:@"%d",++count];
    }
    buyCount = count;
}

//单击减少按钮事件
- (void)decreaseBtnPressed:(UIButton *)btn{
    int count = [self.inputCountTxt.text intValue];
    if (count > 1) {
        self.inputCountTxt.text = [NSString stringWithFormat:@"%d",--count];
    }
    buyCount = count;

}
- (IBAction)AddCartBtnPressed:(id)sender {
    //如果属性没有选择，提示需要选择那一个属性，3秒自动消失
    if (clickedBtnDic.count < formatsArray.count) {
        for (int i = 0; i < formatsArray.count; i++) {
            NSString *keyStr = [NSString stringWithFormat:@"%d",i];
            NSArray *keyArray = [clickedBtnDic allKeys];
            if (![keyArray containsObject:keyStr]) {
                UILabel *tempLabel = [self.labelArray objectAtIndex:i];
                NSString *alertStr = [NSString stringWithFormat:@"请选择%@",tempLabel.text];
                [WXCommonViewClass showHudInView:self.view title:alertStr];
                return;
            }
        }
    }
    if(selectedAttibuteDic.allKeys.count <= 3){
        [selectedAttibuteDic setValue:@"" forKey:@"type2"];
        [selectedAttibuteDic setValue:@"" forKey:@"value2"];
    }
    [productDic setValue:[NSNumber numberWithInteger:buyCount] forKey:@"count"];
    [productDic setValue:[NSNumber numberWithInteger:maxCount] forKey:@"stockCount"];
    [productDic setValue:selectedAttibuteDic forKey:@"mode"];

    BOOL isAddSuccess =  [WXShoppingCartDatabaseClass addShopCart:productDic];
    if (isAddSuccess) {
        //购物车已经存在
        [WXCommonViewClass showHudInButtonOfView:self.view title:@"此商品在购物车中已存在" closeInSecond:2];
        
    }else{
        //添加成功后
        if (iOS8) {
            //系统ios8的处理
            NSString *title = @"";
            NSString *message = @"该商品成功加入购物车";
            NSString *cancelButtonTitle = NSLocalizedString(@"继续购物", nil);
            NSString *otherButtonTitle = NSLocalizedString(@"去结算", nil);
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
            // Create the actions.
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                //单击的是继续购物
                [WXCommonViewClass goToTabItem:self.navigationController tabItem:1];
            }];
            
            UIAlertAction *otherAction = [UIAlertAction actionWithTitle:otherButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //单击的是“去结算”
                WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
                singletonClass.isGifstShopCard = NO;
                [WXCommonViewClass goToTabItem:self.navigationController tabItem:4];
            }];
            
            // Add the actions.
            [alertController addAction:cancelAction];
            [alertController addAction:otherAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
            return;
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"该商品成功加入购物车" delegate:self cancelButtonTitle:nil otherButtonTitles:@"继续购物",@"去结算", nil];
            alert.delegate = self;
            [alert show];
        }
    }
    
}

#pragma mark - UIAlertViewDelegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        //单击的是继续购物
        [WXCommonViewClass goToTabItem:self.navigationController tabItem:1];
    }else if (buttonIndex == 1){
        //单击的是“去结算”
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        singletonClass.isGifstShopCard = NO;
        [WXCommonViewClass goToTabItem:self.navigationController tabItem:4];
       
    }
}
@end
