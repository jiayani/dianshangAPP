//
//  WXShoppingCartDatabaseClass.h
//  DianShangApp
//
//  Created by 霞 王 on 14-6-4.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
@class GiftCart;
@class ShopCart;

@interface WXShoppingCartDatabaseClass : NSObject

/*添加购物车 Yes购物车中已经有弹出提示*/
+ (BOOL)addShopCart:(NSMutableDictionary *)dic;

//批量添加购物车
+ (void)addShopCartObjects:(NSArray *)array;

//根据礼品的上架状态处理本地数据 state 1:正常上架状态  0:下架
+ (void)addGiftCartObjects:(NSArray *)array;

/*获取购物车中的所有商品*/
+ (NSMutableArray *)getAllShoppingCart;

/*获取购物车中的所有商品数量*/
+ (int)getAllShoppingCartCount;

/*获取购物车中的礼品*/
+ (NSMutableArray *)getAllGiftCart;

//删除购物车中的一个礼品
+ (BOOL)deleteOneGiftCart:(GiftCart *)oneObject;

//删除购物车中的一个商品
+ (BOOL)deleteOneProductCart:(ShopCart *)oneObject;
@end
