//
//  WXShoppingCartTableViewCell.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-28.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsynImageView.h"

@interface WXShoppingCartTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *myView;

//begin:非编辑状态下显示的控件
@property (weak, nonatomic) IBOutlet UIView *dataView;
@property (weak, nonatomic) IBOutlet UIButton *selectedOneItemBtn;
@property (weak, nonatomic) IBOutlet AsynImageView *productImageview;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *productCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *productSpecificationLabel;
//end

//begin:编辑状态下的控件
@property (strong, nonatomic) IBOutlet UIView *editView;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UITextField *inputCountTxt;
@property (weak, nonatomic) IBOutlet UIButton *decreaseBtn;
//end
@end
