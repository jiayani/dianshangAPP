//
//  WXAddShoppingCartViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-6-6.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsynImageView.h"
@class XYNoDataView;

@interface WXAddShoppingCartViewController : UIViewController<UIAlertViewDelegate>{
    long maxCount;//最大库存数量
    CGFloat btnPointY;//上一个按钮的高度
    CGFloat labelPointY;//保存Label的高度
    CGFloat viewWidth;//属性按钮的最大宽度
    NSArray *formatsArray;//保存属性的值
    NSArray *stockArray;//保存库存数量的
    NSMutableDictionary *clickedBtnDic;//保存属性索引，用来计算对应的库存所在数组的下标
    NSMutableDictionary *formatDic;//保存选中的属性内容
    NSMutableDictionary *buttonDic;//用来保存属性按钮
    NSMutableDictionary *selectedAttibuteDic;//选中属性的内容，用来保存到数据库中的
    int buyCount;
    CGFloat savePointX;
    BOOL isEnter;
    XYNoDataView *nodataView;
    UIButton *proBtn;
    NSString *showPrice;


}

@property (strong,nonatomic) NSDictionary *dataDic;//从前一个页面传过来属性的内容
@property (strong,nonatomic) NSMutableDictionary *productDic;//从前一个页面传过来商品信息的内容
@property (strong, nonatomic) NSMutableArray *labelArray;
@property (weak, nonatomic) IBOutlet AsynImageView *productImage;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIView *attributeView;
@property (weak, nonatomic) IBOutlet UITextField *inputCountTxt;
@property (weak, nonatomic) IBOutlet UILabel *stockCountLabel;
@property (strong, nonatomic) IBOutlet UIView *countView;
@property (weak, nonatomic) IBOutlet UIScrollView *myScrollView;

- (IBAction)AddCartBtnPressed:(id)sender;
//动态创建属性
- (void)createAttributes;
//动态的创建Label
- (UILabel *)createAttributeLabels:(NSString *)titleStr count:(int)count tag:(int)tag;

//单击添加按钮事件
- (IBAction)addBtnPressed:(UIButton *)btn;
//单击减少按钮事件
- (IBAction)decreaseBtnPressed:(UIButton *)btn;
@end
