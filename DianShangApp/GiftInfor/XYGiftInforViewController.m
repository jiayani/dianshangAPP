//
//  XYGiftInforViewController.m
//  DianShangApp
//
//  Created by wa on 14-5-30.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XYGiftInforViewController.h"
#import "AsynImageView.h"
#import "XY_Cloud_Advertising.h"
#import "WXCommonViewClass.h"
#import "XY_Cloud_GiftData.h"
#import "GiftCart.h"
#import "AppDelegate.h"
#import "WXShoppingCartViewController.h"
#import "WXConfigDataControll.h"
#import "ImageEnlarge.h"
#import "WXCommonViewClass.h"
#import "WXUnionShopListViewController.h"
static NSInteger maxPageControllerIndex = 0;
static CustomPageControl *customPageControl = nil;
static NSInteger num = 1;
//产品详情高度
static float productContentHight = 0.0;

@interface XYGiftInforViewController ()

@end

@implementation XYGiftInforViewController
@synthesize inforArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (iPhone5) {
        self.mainScrollView.contentSize = CGSizeMake(320, self.mainScrollView.frame.size.height);
        
    }else{
        self.mainScrollView.contentSize = CGSizeMake(320, self.mainScrollView.frame.size.height);
    }
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
    
    [[XY_Cloud_GiftData share]setDelegate:(id)self];
    [[XY_Cloud_GiftData share]giftDetailSync:self.giftID giftType:self.giftType];
    //wangxia add
    if (self.giftType == 2) {
        self.unionShopView.hidden = NO;
        
    }else{
        self.unionShopView.hidden = YES;
        CGRect giftInfoFrame = self.giftInfoView.frame;
        giftInfoFrame.origin.y = self.unionShopView.frame.origin.y;
        self.giftInfoView.frame = giftInfoFrame;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
    num = 1;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[XY_Cloud_GiftData share]setDelegate:nil];
    progressHUD = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)giftInforSyncSuccess:(NSDictionary *)data
{
    [loadingView removeFromSuperview];
    giftId = [data objectForKey:@"giftID"];
    self.giftNameLabel.text = [data objectForKey:@"giftName"];
    self.IntegralLabel.text = [NSString stringWithFormat:@"%@积分",[data objectForKey:@"giftIntegral"]];
    self.priceLabel.text = [NSString stringWithFormat:@"市场价￥%.2f",[[data objectForKey:@"giftPrice"] floatValue]];
    self.brandLabel.text = [NSString stringWithFormat:@"礼品品牌：%@",[data objectForKey:@"brands"]];
    self.stockLabel.text = [NSString stringWithFormat:@"库存：%@",[data objectForKey:@"stock"]];
    stockCount = [[data objectForKey:@"stock"] intValue];
    
    self.IntegralLabel.textColor = [WXConfigDataControll getFontColorOfIntegral];
    
    CGSize textSize = [self.IntegralLabel.text sizeWithFont:self.IntegralLabel.font];
    CGFloat strikeWidth = textSize.width;
    CGRect frame = self.IntegralLabel.frame;
    frame.size.width = strikeWidth;
    self.IntegralLabel.frame = frame;
    CGRect oldPriceFrame = self.priceLabel.frame;
    oldPriceFrame.origin.x = frame.origin.x + strikeWidth + 5.0f;
    self.priceLabel.frame = oldPriceFrame;
    
    self.typeOfSendLabel.text = @"配送方式：商家配送";
    
    
    if (self.giftType == 0) {
        self.inforLabel.hidden = NO;
        self.inforWebview.hidden = YES;
        self.inforLabel.text = [data objectForKey:@"giftContent"];
        CGSize size = [self.inforLabel.text sizeWithFont:self.inforLabel.font
                      constrainedToSize:CGSizeMake(self.inforLabel.frame.size.width, 10000)];

        productContentHight = size.height;
        CGRect rect = self.inforLabel.frame;
        rect.size.height = +productContentHight;
        [self.inforLabel setFrame:rect];

        CGRect giftInfoFrame = self.giftInfoView.frame;
        giftInfoFrame.size.height = rect.size.height + rect.origin.y;
        self.giftInfoView.frame = giftInfoFrame;
        
        self.mainScrollView.contentSize = CGSizeMake(320, self.giftInfoView.frame.origin.y + self.giftInfoView.frame.size.height + 100);
    }else if (self.giftType ==1) {
        self.inforLabel.hidden = YES;
        self.inforWebview.hidden = NO;
        [self.inforWebview loadHTMLString:[NSString stringWithFormat:@"%@",[data objectForKey:@"giftContent"]] baseURL:nil];
    }else if (self.giftType == 2){
        [self initUnionView:data];
        self.inforLabel.hidden = NO;
        self.inforWebview.hidden = YES;
    }
    
    inforArray = [data objectForKey:@"giftPic"];
    maxPageControllerIndex = inforArray.count + 1;
    self.inforScrollView.contentSize = CGSizeMake(320*[inforArray count], 320);
    self.customPageControl.numberOfPages = [inforArray count];
    customPageControl = self.customPageControl;
    pic_url = [[inforArray objectAtIndex:0]objectForKey:@"img"];
    if (inforArray.count <= 1) {
        self.inforScrollView.scrollEnabled = NO;
    }
    else {
        self.inforScrollView.scrollEnabled = YES;
    }
    
    for (int i=0; i< [inforArray count]; i++)
    {
        AsynImageView * imageView = [[AsynImageView alloc] init];
        imageView.frame = CGRectMake(i*320, 0, 320, 320);
        imageView.tag = i;
        if (self.giftType ==1) {
            imageView.isFromWooGift = YES;
        }
        imageView.type = 2;
        imageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
        NSDictionary * object = [inforArray objectAtIndex:i];
        NSString * image = [object objectForKey:@"img"];
        imageView.imageURL = [NSString stringWithFormat:@"%@",image];
        //绑定单击事件
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(inforSingleTap:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [imageView addGestureRecognizer:tapGestureRecognizer];
        imageView.userInteractionEnabled = YES;
        [self.inforScrollView addSubview:imageView];
        
    }
    //在第一个位置添加第一张广告图片
//    [self loadAdHolderWithAdIndex:0];
    
    //在最后一个位置添加第一张广告图片
//    [self loadAdHolderWithAdIndex:inforArray.count-1];
    
    self.inforScrollView.decelerationRate = 0;
    self.inforScrollView.frame = CGRectMake(0, 0, 320, 320);
    self.inforScrollView.showsHorizontalScrollIndicator = NO;
    self.inforScrollView.showsVerticalScrollIndicator = NO;
    [self.inforScrollView scrollRectToVisible:CGRectMake(0,0, 320, 320) animated:NO];
    self.customPageControl.currentPage = 0;
}
-(void)giftInforSyncFailed:(NSString*)errMsg
{
    [loadingView removeFromSuperview];
}
/*加载广告占位 第一张和最后一张*/
//- (void)loadAdHolderWithAdIndex:(NSInteger)index{
//    
//    
//    AsynImageView *imageView = [[AsynImageView alloc]init];
//    NSDictionary * object;
//    //第一张广告过渡
//    if(index == 0){
//        if (inforArray.count != 0) {
//            object = [inforArray objectAtIndex:inforArray.count - 1];
//        }
//        
//        //第一张广告过渡添加在0的位置
//        imageView.frame = CGRectMake(0, 0, 320, 320);
//        self.firstAdImageView = imageView;
//        //最后一张广告过渡
//    }else{
//        if (inforArray.count != 0) {
//        object = [inforArray objectAtIndex:0];
//        }
//        //最后一张广告过渡添加在最后一张广告过渡的后面
//        imageView.frame = CGRectMake((index+2)*320, 0, 320, 320);
//        self.lastAdImageView = imageView;
//    }
//    if (self.giftType ==1) {
//        imageView.isFromWooGift = YES;
//    }
//    imageView.type = 2;
//    imageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
//    
//    NSString * image = [object objectForKey:@"img"];
//    
//    imageView.imageURL = [NSString stringWithFormat:@"%@",image];
//    
//    [self.inforScrollView addSubview:imageView];
//}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.inforScrollView) {
        
        int cur = scrollView.contentOffset.x / scrollView.frame.size.width ;
        self.customPageControl.currentPage = cur;
    }
}

- (void)inforSingleTap:(UITapGestureRecognizer*)recognizer{
    self.navigationController.view.backgroundColor = [UIColor blackColor];
    AsynImageView *imageView = (AsynImageView *)[self.inforScrollView viewWithTag:self.customPageControl.currentPage];
    ImageEnlarge *pictureVC = [[ImageEnlarge alloc]init];
    
    
    if(imageView.image != nil){
        pictureVC.images = imageView.image;
        
        float height = imageView.image.size.height * 320.0 / imageView.image.size.width;
        pictureVC.imageView.frame = CGRectMake(0,0, 320,height);
        pictureVC.view.backgroundColor = [UIColor blackColor];
        pictureVC.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f];
        
        pictureVC.view.transform = CGAffineTransformMakeScale(1, 1);
        
        
        [self.navigationController pushViewController:pictureVC animated:NO];
        [UIView commitAnimations];
    }else{
        //弹出提示语
        [WXCommonViewClass showHudInView:self.view title:@"未发现本地图片"];
    }

}
#pragma WebviewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView { //webview 自适应高度
    
    NSString *height = [webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"];
    productContentHight = [height integerValue] +20;
    //    productContentHight = 500;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [webView stringByEvaluatingJavaScriptFromString:@"var imgs = document.getElementsByTagName('img');"
         " for(var i= 0;i<imgs.length;i++){"
         "  imgs[i].width = 305;"
         "}"];
        CGRect webFrame = webView.frame;
        
        webView.frame = CGRectMake(webFrame.origin.x, webFrame.origin.y,webFrame.size.width,productContentHight);
    
        CGRect giftInfoFrame = self.giftInfoView.frame;
        giftInfoFrame.size.height = webView.frame.size.height + webView.frame.origin.y;
        self.giftInfoView.frame = giftInfoFrame;
        
        self.mainScrollView.contentSize = CGSizeMake(320, self.giftInfoView.frame.origin.y + self.giftInfoView.frame.size.height + 100);
    });
}

- (GiftCart *)getShoppingCartByDishId:(NSString *)gift{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    NSEntityDescription *entityDescription=[NSEntityDescription entityForName:@"GiftCart" inManagedObjectContext:context];
    NSFetchRequest *request=[[NSFetchRequest alloc]init];
    [request setEntity:entityDescription];
    
    NSPredicate *predicateTemplate = [NSPredicate predicateWithFormat:@"giftid = %@",gift];
    [request setPredicate: predicateTemplate];
    
    NSError *error;
    [context unlock];
    
    NSArray *objects=[context executeFetchRequest:request error:&error];
    
    if(objects == nil || objects.count == 0){
        return nil;
    }else {
        return (GiftCart *)[objects objectAtIndex:0];
    }
}

- (IBAction)joinShopCart:(id)sender {
    
    if (stockCount == 0) {
        [WXCommonViewClass showHudInView:self.view title:@"库存不足！"];
        return;
    }
    
    GiftCart *shoppingCart = [self getShoppingCartByDishId:giftId];
    if(shoppingCart != nil){
        [WXCommonViewClass showHudInView:self.view title:@"此商品已经在购物车中"];
    }
    else
    {
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[appDelegate managedObjectContext];
    
    NSError *error;
    NSEntityDescription *entityDescription=[NSEntityDescription entityForName:@"GiftCart" inManagedObjectContext:context];
    GiftCart *theObject;
    //查询本地数据库中是否有该数据 有：进行修改  无：添加一条数据
    NSFetchRequest *request=[[NSFetchRequest alloc]init];
    NSPredicate *pred=[NSPredicate predicateWithFormat:@"(giftid=%@)",giftId];
    [request setPredicate:pred];
    [request setEntity:entityDescription];
    NSArray *objects=[context executeFetchRequest:request error:&error];
    if (objects!=nil && objects.count>0) {
        
        theObject=(GiftCart *)[objects objectAtIndex:0];
        
    }else{
        theObject=(GiftCart *)[[NSManagedObject alloc]initWithEntity:entityDescription insertIntoManagedObjectContext:context];
    }
        theObject.isOnline = [NSNumber numberWithBool:NO];
        theObject.count = [NSNumber numberWithInt:[self.numTextField.text intValue]];
        theObject.pic_url = pic_url;
        theObject.integral = [NSNumber numberWithInt:[self.IntegralLabel.text intValue]];
        theObject.title = self.giftNameLabel.text;
        theObject.giftid = giftId;
        theObject.stockCount = [NSNumber numberWithInt:stockCount];
        NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:giftId,@"giftId",[NSString stringWithFormat:@"%i",self.giftType],@"giftType",nil];
        theObject.mode = dic;
        theObject.type = [NSNumber numberWithInt:self.giftType];
        NSDate *datenow = [NSDate date];
        NSTimeZone *zone = [NSTimeZone  timeZoneWithName:@"Asia/Shanghai"];
        NSInteger interval = [zone secondsFromGMTForDate:datenow];
        NSDate *localeDate = [datenow  dateByAddingTimeInterval: interval];
        long  timSp = (long)[localeDate timeIntervalSince1970];
        theObject.giftCartOrderBy = [NSNumber numberWithLong:timSp];
    if ([context save:nil]) {
       
    }
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"提示"
                                                         message:@"该商品已加入购物车。"
                                                        delegate:self
                                               cancelButtonTitle:@"继续兑换"
                                               otherButtonTitles:@"去购物车",nil];
    alertView.tag = 1010;
    [alertView show];
    }
}
#pragma mark - 联盟商家信息 wangxia add
- (void)initUnionView:(NSDictionary *)data{
    //礼品所属的商家名称
    NSString *giftFromShopNameStr = [data objectForKey:@"giftFromShopName"];
    self.fromShopLabel.text = giftFromShopNameStr;


    self.inforLabel.text = [data objectForKey:@"giftContent"];
    CGSize size = [self.inforLabel.text sizeWithFont:self.inforLabel.font
                                   constrainedToSize:CGSizeMake(self.inforLabel.frame.size.width, 10000)];
    
    productContentHight = size.height;
    CGRect rect = self.inforLabel.frame;
    rect.size.height = +productContentHight;
    [self.inforLabel setFrame:rect];
    
    CGRect giftInfoFrame = self.giftInfoView.frame;
    giftInfoFrame.size.height = rect.size.height + rect.origin.y;
    self.giftInfoView.frame = giftInfoFrame;
    
    self.mainScrollView.contentSize = CGSizeMake(320, self.giftInfoView.frame.origin.y + self.giftInfoView.frame.size.height + 100);

}

//wangxia  单击联盟商家跳转到联盟商家列表页面
- (IBAction)unionShopBtnPressed:(id)sender {
    WXUnionShopListViewController *unionShopListViewController = [[WXUnionShopListViewController alloc]initWithNibName:@"WXUnionShopListViewController" bundle:nil];
    [self.navigationController pushViewController:unionShopListViewController animated:YES];
}
#pragma mark -UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 1010){
        if (buttonIndex == 0){
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else if (buttonIndex == 1){
            WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
            singletonClass.isGifstShopCard = YES;
             [WXCommonViewClass goToTabItem:self.navigationController tabItem:4];
            
        }
    }
}

- (IBAction)reduceBtnAct:(id)sender {
    if (![self.numTextField.text isEqualToString:@"1"]) {
        num -=1;
        self.numTextField.text = [NSString stringWithFormat:@"%i",num];
    }
}

- (IBAction)backBtnAct:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addBtnAct:(id)sender {
    if (![self.numTextField.text isEqualToString:[NSString stringWithFormat:@"%i",stockCount]]) {
        num += 1;
        self.numTextField.text = [NSString stringWithFormat:@"%i",num];
    }
    
}
//无网络连接和意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[XY_Cloud_GiftData share] setDelegate:nil];
    //小圈停止运动
    progressHUD =[WXCommonViewClass showHudInView:self.view title:errorStr];
    progressHUD.delegate = self;
    [loadingView removeFromSuperview];
}

#pragma mark - MBProgressHUDDelegate
- (void)hudWasHidden:(MBProgressHUD *)hud{
    progressHUD = nil;
    [self.navigationController popViewControllerAnimated:YES];
}
@end
