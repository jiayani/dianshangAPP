//
//  XYGiftInforViewController.h
//  DianShangApp
//
//  Created by wa on 14-5-30.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomPageControl.h"
#import "XYLoadingView.h"
#import "MBProgressHUD.h"
#import "WX_Cloud_Delegate.h"

@interface XYGiftInforViewController : UIViewController<UIScrollViewDelegate,UIWebViewDelegate,UIAlertViewDelegate,MBProgressHUDDelegate,WX_Cloud_Delegate>
{
    NSString * giftId;
    NSString * pic_url;
    int stockCount;
    XYLoadingView * loadingView;
    MBProgressHUD *progressHUD;
}
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *inforScrollView;
@property (weak, nonatomic) IBOutlet CustomPageControl *customPageControl;
@property (weak, nonatomic) IBOutlet UILabel *stockLabel;
@property (weak, nonatomic) IBOutlet UIWebView *inforWebview;
@property (weak, nonatomic) IBOutlet UILabel *typeOfSendLabel;
@property (strong,nonatomic)NSArray *inforArray;
@property (retain,nonatomic)UIImageView *firstAdImageView;
@property (assign,nonatomic) int giftType;
@property (assign,nonatomic) int giftID;
@property (weak, nonatomic) IBOutlet UILabel *brandLabel;
@property (weak, nonatomic) IBOutlet UITextField *numTextField;
@property(retain,nonatomic)UIImageView *lastAdImageView;
@property (weak, nonatomic) IBOutlet UILabel *inforLabel;
@property (weak, nonatomic) IBOutlet UILabel *IntegralLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *giftNameLabel;
//wangxia 联盟
@property (weak, nonatomic) IBOutlet UIView *giftInfoView;
@property (weak, nonatomic) IBOutlet UIView *unionShopView;
@property (weak, nonatomic) IBOutlet UILabel *fromShopLabel;
- (IBAction)joinShopCart:(id)sender;

- (IBAction)reduceBtnAct:(id)sender;
- (IBAction)backBtnAct:(id)sender;
- (IBAction)addBtnAct:(id)sender;

//wangxia 联盟
- (IBAction)unionShopBtnPressed:(id)sender;
@end
