//
//  XY_GiftData.m
//  DianShangApp
//
//  Created by wa on 14-6-4.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XY_Cloud_GiftData.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"


static XY_Cloud_GiftData * giftData = nil;
@implementation XY_Cloud_GiftData
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!giftData)
    {
        giftData = [[XY_Cloud_GiftData alloc] init];
    }
    return giftData;
}
-(void)giftDetailSync:(int)giftID giftType:(int)type
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr;
        WXCommonSingletonClass *  singletonClass  = [WXCommonSingletonClass share];
        urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/giftDetailByID_GetImp?giftID=%i&giftType=%i&shopid=%@&version=%@", giftID, type, singletonClass.shopid, singletonClass.version]];
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
         [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSDictionary * dataDic = [dic objectForKey:@"data"];
        [delegate giftInforSyncSuccess:dataDic];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate giftInforSyncFailed:@"请求失败"];
        }
        else
        {
        NSString * message = [dic objectForKey:@"message"];
        [delegate giftInforSyncFailed:message];
        }
    }
}

@end
