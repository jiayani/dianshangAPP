//
//  YJNemailUserTionViewController.h
//  TianLvApp
//
//  Created by 霞 王 on 14-9-8.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"

@interface YJNemailUserTionViewController :  UIViewController<UITextFieldDelegate,WX_Cloud_Delegate>
{
    int countdownInt;//倒计时计数
    int valid;//有效期
    int sendCount;//记录发送验证码的次数
    NSDate *startDate;
    BOOL isMove;//是否需要将view上移
    CGFloat moveFloat;//上移动多少
}
@property (strong,nonatomic) NSString *emailStr;
@property (readwrite) BOOL isFromBindPhone;
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UIView *myAnimationView;

@property (strong, nonatomic) IBOutlet UITextField *verifyVodeText;
@property (weak, nonatomic) IBOutlet UIButton *submitVerifyCodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *resendVerifyCodeBtn;
@property (weak, nonatomic) IBOutlet UILabel *resendVerifyCodeLabel;

- (IBAction)textFieldChanged:(id)sender;
- (IBAction)submitVerityCodeBtnPressed:(id)sender;
- (IBAction)resendVerifyCodeBtnPressed:(id)sender;

@end
