//
//  JYNemailUserAction1ViewController.h
//  TianLvApp
//
//  Created by 霞 王 on 14-9-8.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"

@class UserInfo;

@interface JYNemailUserAction1ViewController : UIViewController<UITextFieldDelegate,WX_Cloud_Delegate>

@property (strong, nonatomic)  UserInfo *currentUser;
@property (readwrite)  BOOL isFromBindemail;
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdLabel;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (strong, nonatomic) IBOutlet UITextField *phoneText;
@property (weak, nonatomic) IBOutlet UIButton *getVerifyCodeBtn;
@property (weak, nonatomic) IBOutlet UILabel *userPhoneLabel;

@property (strong, nonatomic) IBOutlet UITextView *directionsTextView;

- (IBAction)getVerifyBtnPressed:(id)sender;
- (IBAction)textFieldChanged:(id)sender;

@end

