//
//  JYNemailUserAction1ViewController.m
//  TianLvApp
//
//  Created by 霞 王 on 14-9-8.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "JYNemailUserAction1ViewController.h"
#import "YJNemailUserTionViewController.h"
#import "WXCommonViewClass.h"
#import "WXUserDatabaseClass.h"
#import "UserInfo.h"
#import "WXVerifyMethodsClass.h"
#import "AppDelegate.h"
#import "JYNGetKeyeords.h"
#import "WX_Cloud_IsConnection.h"

@interface JYNemailUserAction1ViewController ()

@end

@implementation JYNemailUserAction1ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    self.view.backgroundColor = RGBA(220.0f, 220.0f, 220.0f, 1.0);
    if (iPhone5) {
        self = [super initWithNibName:@"WXUserActivation1ViewController" bundle:nibBundleOrNil];
    }else{
        if ([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
            self = [super initWithNibName:@"WXUserActivation1ViewController" bundle:nibBundleOrNil];
        }else{
            self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
        }
    }
    if (self) {
        
        // Custom initialization
        
        [WXCommonViewClass hideTabBar:self isHiden:YES];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //ios7 导航适配
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    if (self.isFromBindemail) {
        self.title = NSLocalizedString(@"绑定邮箱", @"绑定邮箱");
    }else{
        self.title = NSLocalizedString(@"绑定邮箱", @"绑定邮箱1页面标题");
    }
    UIButton * backBtn = [WXCommonViewClass getBackButton];
    [backBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftBtnItem;
    [self initAllControllers];
    self.phoneText.keyboardType = UIKeyboardTypeEmailAddress;
    
}

-(void)goBack
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[JYNGetKeyeords share] setDelegate:nil];
}
- (void)viewDidUnload {
    [self setFirstLabel:nil];
    [self setSecondLabel:nil];
    [self setThirdLabel:nil];
    [self setErrorLabel:nil];
    [self setPhoneText:nil];
    [self setGetVerifyCodeBtn:nil];
    [self setDirectionsTextView:nil];
    [self setUserPhoneLabel:nil];
    [super viewDidUnload];
}
#pragma mark - UITextFieldDelegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)textFieldChanged:(id)sender {
    if (self.phoneText.text.length <= 0 ) {
        self.getVerifyCodeBtn.enabled = NO;
        self.errorLabel.hidden = NO;
        self.errorLabel.text = NSLocalizedString(@"邮箱不能为空", @"会员激活1/邮箱不能为空");
        
    }else{
        self.getVerifyCodeBtn.enabled = YES;
        
    }
}
#pragma mark - All My Methods
/*初始化所有页面显示内容*/
- (void)initAllControllers{
    
    self.firstLabel.text = NSLocalizedString(@"填写邮箱号", @"会员激活1/填写注册邮箱");
    self.secondLabel.text = NSLocalizedString(@"验证身份", @"会员激活1/验证身份");
    self.thirdLabel.text = NSLocalizedString(@"绑定成功", @"会员激活1/绑定成功");
    [self.getVerifyCodeBtn setTitle:NSLocalizedString(@"获取验证码", @"会员激活1/获取验证码") forState:UIControlStateNormal];
    self.phoneText.placeholder = NSLocalizedString(@"请输入您的邮箱", @"会员激活1/请输入您的邮箱");
    self.directionsTextView.text = NSLocalizedString(@"说明：\n1、绑定您常用的邮箱。\n2、绑定邮箱可以享受帐号保护、积分返点、商家优惠等服务。", @"会员激活1/会员激活的说明");
    self.directionsTextView.editable = NO;
    self.phoneText.keyboardType = UIKeyboardTypeNumberPad;
    self.errorLabel.hidden = YES;
    self.currentUser = [WXUserDatabaseClass getCurrentUser];
    if (self.currentUser != nil) {
        if (self.currentUser.userEmail != nil && self.currentUser.userEmail.length > 0) {
            self.userPhoneLabel.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"您绑定的邮箱", @"会员激活1/您绑定的邮箱"),self.currentUser.userEmail];
        }else{
            self.userPhoneLabel.text = [NSString stringWithFormat:@"%@",NSLocalizedString(@"您尚未绑定邮箱", @"会员激活1/您尚未绑定邮箱")];
            
        }
    }
    self.getVerifyCodeBtn.enabled = NO;
    
    
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    if ([touch view] == self.view) {
        
        [self.phoneText resignFirstResponder];
    }
}
- (IBAction)getVerifyBtnPressed:(id)sender {
    
    WXCommonSingletonClass *single = [WXCommonSingletonClass share];
    if (single.myLastLoginName ==self.currentUser.userEmail)//就说明是用邮箱登录进来的
    {
        
        single.isSameLoginName =YES;
    }
    else //用其他方式进来的
    {
        single.isSameLoginName =NO;
    }
    NSString *phoneStr = [self.phoneText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    BOOL isValidatePhone = [WXVerifyMethodsClass isValidateEmail:self.phoneText.text];
    if (!isValidatePhone) {
        NSString *alertStr = NSLocalizedString(@"请输入正确的邮箱", @"会员激活1/请输入正确的邮箱");
        self.errorLabel.hidden = NO;
        self.errorLabel.text = alertStr;
        
    }else{
        self.errorLabel.hidden = YES;
        //本地判断是否连接网络
        BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
        if (!isConnection) {//无网络情况
            NSString *errCode = NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用");
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:errCode delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"找回密码1/确定") otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            //小圈转动等待验证
            [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"等待验证...", @"会员激活1/等待验证...") nav:self.navigationController];
            //调用后台方法进行服务器断验证
            [[JYNGetKeyeords share] setDelegate:(id)self];
            //0 邮箱  1:邮箱
            [[JYNGetKeyeords share] getKeywordsWithNumber:phoneStr];
        }
        
    }
    
    
}
#pragma mark - WYDCloudDelegate Methods
//无网络连接
-(void)haveNoNetwork{
    [[JYNGetKeyeords share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    NSString *errorStr = NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用");
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:errorStr delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"确定") otherButtonTitles:nil];
    [alert show];
    //    [WXCommonViewClass showHudInButtonOfView:self.view title:NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用") closeInSecond:3];
}
//意外错误
-(void)accidentError:(NSError*)error{
    [[JYNGetKeyeords share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    NSString *errorStr = NSLocalizedString(@"网络异常", @"网络异常");
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:errorStr delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"确定") otherButtonTitles:nil];
    [alert show];
    
    //    [WXCommonViewClass showHudInButtonOfView:self.view title:errorStr closeInSecond:3];
}
//验证码验证
-(void)verifyPhoneOrEmailSuccess:(NSString*)keyWordStr{
    [[JYNGetKeyeords share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    YJNemailUserTionViewController *activation2VC = [[YJNemailUserTionViewController alloc]initWithNibName:@"YJNemailUserTionViewController" bundle:nil];
    activation2VC.isFromBindPhone = self.isFromBindemail;
    activation2VC.emailStr = self.phoneText.text;
    [self.navigationController pushViewController:activation2VC animated:YES];
}
- (void)verifyPhoneOrEmailFailed:(NSString*)keyWordStr errCode:(NSString*)errCode{
    [[JYNGetKeyeords share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:errCode delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"确定") otherButtonTitles:nil];
    [alert show];
}
- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [WXCommonViewClass showHudInView:self.view title:errorCode];
}
@end
