//
//  YJNemailUserTionViewController.m
//  TianLvApp
//
//  Created by 霞 王 on 14-9-8.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "YJNemailUserTionViewController.h"
#import "WXUserActivation3ViewController.h"
#import "WXCommonViewClass.h"
#import "WXUserDatabaseClass.h"
#import "AppDelegate.h"
#import "WX_Cloud_IsConnection.h"
#import "JYNsendKeywords.h"
#import "JYNGetKeyWords.h"
//监听键盘的宏定义
#define _UIKeyboardFrameEndUserInfoKey (&UIKeyboardFrameEndUserInfoKey != NULL ? UIKeyboardFrameEndUserInfoKey : @"UIKeyboardBoundsUserInfoKey")

@interface YJNemailUserTionViewController ()

@end

@implementation YJNemailUserTionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (iPhone5) {
        self = [super initWithNibName:@"YJNemailUserTionViewController" bundle:nibBundleOrNil];
    }else{
        if ([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
            self = [super initWithNibName:@"YJNemailUserTionViewController" bundle:nibBundleOrNil];
        }else{
            self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
        }
    }
    if (self) {
        
        [WXCommonViewClass hideTabBar:self isHiden:YES];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //ios7 导航适配
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    // Do any additional setup after loading the view from its nib.
    if (self.isFromBindPhone) {
    }else{
        self.title = NSLocalizedString(@"绑定邮箱", @"会员激活2页面标题");
    }
    [self initAllControllers];
    UIButton * backBtn = [WXCommonViewClass getBackButton];
    [backBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftBtnItem;
}
- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!iPhone5) {
        //键盘的监听事件，获取高度
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[JYNSendKeywords share] setDelegate:nil];
    [[JYNSendKeywords share] setDelegate:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}
- (void)viewDidUnload {
    [self setFirstLabel:nil];
    [self setSecondLabel:nil];
    [self setThirdLabel:nil];
    [self setPhoneLabel:nil];
    [self setVerifyVodeText:nil];
    [self setSubmitVerifyCodeBtn:nil];
    [self setResendVerifyCodeBtn:nil];
    [self setErrorLabel:nil];
    [self setMyAnimationView:nil];
    [super viewDidUnload];
}
#pragma mark - UITextFieldDelegate Methods
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self ViewMoveToZero];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)textFieldChanged:(id)sender {
    if (self.verifyVodeText.text.length <= 0 ) {
        self.submitVerifyCodeBtn.enabled = NO;
        self.errorLabel.hidden = NO;
        self.errorLabel.text = NSLocalizedString(@"验证码不能为空", @"会员激活2/验证码不能为空");
        
    }else{
        self.submitVerifyCodeBtn.enabled = YES;
        
    }
}

#pragma mark - All My Methods
/*初始化所有页面显示内容*/
- (void)initAllControllers{
    self.myAnimationView.hidden = YES;
    self.firstLabel.text = NSLocalizedString(@"填写注册手机号", @"会员激活2/填写注册手机号");
    self.secondLabel.text = NSLocalizedString(@"验证身份", @"会员激活2/验证身份");
    self.thirdLabel.text = NSLocalizedString(@"绑定成功", @"会员激活2/绑定成功");
    NSString *tempStr = NSLocalizedString(@"您的邮箱是", @"会员激活2/您的手机号是");
    NSString *str = [NSString stringWithFormat:@"%@:%@",tempStr,self.emailStr];
    self.phoneLabel.text = str;
    [self.submitVerifyCodeBtn setTitle:NSLocalizedString(@"提交验证码", @"会员激活2/提交验证码") forState:UIControlStateNormal];
    self.verifyVodeText.placeholder = NSLocalizedString(@"请输入验证码", @"会员激活2/请输入验证码");
    self.errorLabel.hidden = YES;
    self.submitVerifyCodeBtn.enabled = NO;
    countdownInt = 60;
    valid = 600;
    sendCount = 0;
    startDate = [NSDate date];
    self.resendVerifyCodeBtn.enabled = NO;
    NSString *resendStr = NSLocalizedString(@"重发验证码", @"找回密码2/重发验证码");
    NSString *tempStr2 = [NSString stringWithFormat:@"%@(%d\")",resendStr,countdownInt];
    self.resendVerifyCodeLabel.text = tempStr2;
    //每秒钟刷新一次倒计时显示
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(refreshCountdown) userInfo:nil repeats:YES];
    
    
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    if ([touch view] == self.view) {
        [self.verifyVodeText resignFirstResponder];
    }
}

//键盘事件
- (void)keyboardWillShow:(NSNotification*)notification{
    CGRect keyboardRect = [[[notification userInfo] objectForKey:_UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat txtHeight = self.verifyVodeText.frame.origin.y + self.verifyVodeText.frame.size.height;
    CGFloat keyY = self.view.frame.size.height - keyboardRect.size.height;
    if (keyY < txtHeight) {
        isMove = YES;
        moveFloat = txtHeight - keyY;
    }else{
        isMove = NO;
        moveFloat = 0;
    }
    if (isMove) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.2];
        
        self.view.frame = CGRectMake(0, -(moveFloat + 20), self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }else{
        [self ViewMoveToZero];
    }
    
}
/*恢复view刚开始的位置*/
- (void)ViewMoveToZero{
    if (self.view.frame.origin.y != 0) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.2];
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }
}
- (void)refreshCountdown{
    countdownInt--;
    NSString *resendStr = NSLocalizedString(@"重新获取验证码", @"会员激活2/重新获取验证码")
    ;
    NSString *tempStr;
    
    tempStr = [NSString stringWithFormat:@"%@(%d\")",NSLocalizedString(@"重发验证码", @"找回密码2/重发验证码"),countdownInt];
    
    if (countdownInt <= 0) {
        self.resendVerifyCodeBtn.enabled = YES;
        self.resendVerifyCodeLabel.enabled = YES;
        self.resendVerifyCodeLabel.text = resendStr;
    }else{
        self.resendVerifyCodeBtn.enabled = NO;
        self.resendVerifyCodeLabel.enabled = NO;
        self.resendVerifyCodeLabel.text = tempStr;
        
    }
    
}

- (IBAction)submitVerityCodeBtnPressed:(id)sender {
    [self.verifyVodeText resignFirstResponder];
    NSString *verifyCodeStr = [self.verifyVodeText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //输入的验证码是否为空
    if (verifyCodeStr.length <= 0) {
        return;
    }else{
        //把验证码发给后台服务器端进行验证
        sendCount++;
        //验证成功后调用的代码
        self.errorLabel.hidden = YES;
        //本地判断是否连接网络
        BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
        if (!isConnection) {//无网络情况
            NSString *errCode = NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用");
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:errCode delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"找回密码1/确定") otherButtonTitles:nil];
            [alert show];
        }else{
            //小圈转动等待验证
            self.myAnimationView.hidden = NO;
            [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"等待验证...", @"会员激活2/等待验证...") View:self.myAnimationView];
            //调用后台方法进行服务器断验证
            
            [[JYNSendKeywords share] setDelegate:(id)self];
            
          //  [[JYNSendKeywords share] getPasswordWithKeywords:verifyCodeStr flag:sendCount];
            [[JYNSendKeywords share]getPasswordWithKeywords:verifyCodeStr andemail:self.emailStr flag:sendCount];
        }
        
        /*
         //判断验证码的超过有效期没有
         if ((-[startDate timeIntervalSinceNow]) > valid) {
         self.errorLabel.text = NSLocalizedString(@"您的验证码已经过期", @"会员激活2/您的验证码已经过期");
         self.errorLabel.hidden = NO;
         }else{
         self.errorLabel.hidden = YES;
         //本地判断是否连接网络
         BOOL isConnection = [[[WYDIsConnection alloc]init]isConnectionAvailable];
         if (!isConnection) {//无网络情况
         NSString *errCode = NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用");
         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:errCode delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"找回密码1/确定") otherButtonTitles:nil];
         [alert show];
         }else{
         //小圈转动等待验证
         [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"等待验证...", @"会员激活2/等待验证...") nav:self.navigationController];
         //调用后台方法进行服务器断验证
         
         [[WYDSendKeywords share] setDelegate:self];
         
         [[WYDSendKeywords share] getPasswordWithKeywords:verifyCodeStr flag:sendCount];
         }
         
         }
         */
    }
}
- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:errorCode];
}

- (IBAction)resendVerifyCodeBtnPressed:(id)sender {
    [self.verifyVodeText resignFirstResponder];
    countdownInt = 60;
    startDate = [NSDate date];
    self.resendVerifyCodeBtn.enabled = NO;
    NSString *resendStr = NSLocalizedString(@"重发验证码", @"找回密码2/重发验证码");
    NSString *tempStr = [NSString stringWithFormat:@"%@(%d\")",resendStr,countdownInt];
    self.resendVerifyCodeLabel.text = tempStr;
    
    //调用后台方法进行服务器断验证
    [[JYNGetKeyWords share] setDelegate:(id)self];
    [[JYNGetKeyWords share] getKeywordsWithNumber:self.emailStr];
}

#pragma mark - WYDCloudDelegate Methods
//无网络连接
-(void)haveNoNetwork{
    [[JYNSendKeywords share] setDelegate:nil];
    
    //小圈停止运动
    self.myAnimationView.hidden = YES;
    [WXCommonViewClass setActivityWX:NO content:nil View:self.myAnimationView];
    NSString *errorStr = NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用");
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:errorStr delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"找回密码2/确定") otherButtonTitles:nil];
    [alert show];
    //    [WXCommonViewClass showHudInButtonOfView:self.view title:NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用") closeInSecond:3];
}
//意外错误
-(void)accidentError:(NSError*)error{
    [[JYNSendKeywords share] setDelegate:nil];
    //小圈停止运动
    self.myAnimationView.hidden = YES;
    [WXCommonViewClass setActivityWX:NO content:nil View:self.myAnimationView];
    NSString *errorStr = NSLocalizedString(@"网络异常", @"网络异常");
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:errorStr delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"找回密码2/确定") otherButtonTitles:nil];
    [alert show];
    //    [WXCommonViewClass showHudInButtonOfView:self.view title:errorStr closeInSecond:3];
}
//验证码验证
-(void)gotoNextStep2:(NSString*)keywordsID{
    [[JYNSendKeywords share] setDelegate:nil];
    
    //小圈停止运动
    self.myAnimationView.hidden = YES;
    [WXCommonViewClass setActivityWX:NO content:nil View:self.myAnimationView];
    UserInfo *currentUser = [WXUserDatabaseClass getCurrentUser];
    [WXUserDatabaseClass updateEmailOfUser:currentUser emailStr:self.emailStr];

    //[WXUserDatabaseClass updatePhoneOfUser:currentUser phoneStr:self.emailStr];
    WXUserActivation3ViewController *actiovation3VC = [[WXUserActivation3ViewController alloc]initWithNibName:@"WXUserActivation3ViewController" bundle:nil];
    actiovation3VC.isFromBindPhone = self.isFromBindPhone;
    actiovation3VC.phoneStr = self.emailStr;
    [self.navigationController pushViewController:actiovation3VC animated:YES];
    
}
-(void)keywordsError2:(NSString*)keywordsID andErrorMessage:(NSString*)errCode{
    [[JYNSendKeywords share] setDelegate:nil];
    
    //小圈停止运动
    self.myAnimationView.hidden = YES;
    [WXCommonViewClass setActivityWX:NO content:nil View:self.myAnimationView];
    //        NSString *alertStr = NSLocalizedString(@"证码输入错误", @"找回密码2/证码输入错误");
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:errCode delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"找回密码2/确定") otherButtonTitles:nil];
    [alert show];
    
}
#pragma mark - WYDCloudDelegate 获取验证码方法
//验证码验证
-(void)gotoNextStep1:(NSString*)keywordsID{
    [[JYNGetKeyWords share] setDelegate:nil];
    //小圈停止运动
    //    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    
}
-(void)keywordsError1:(NSString*)keywordsID andErrorMessage:(NSString*)errCode{
    [[JYNGetKeyWords share] setDelegate:nil];
    //小圈停止运动
    //    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
}
@end
