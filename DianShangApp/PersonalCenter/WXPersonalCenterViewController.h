//
//  WXPersonalCenterViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-4-22.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"
#import "MBProgressHUD.h"
@class UserInfo;
@class MKNumberBadgeView;

@interface WXPersonalCenterViewController : UIViewController<WX_Cloud_Delegate,MBProgressHUDDelegate,UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray *titleArray;
    NSMutableArray *backgroundImageBtnArray;
    MBProgressHUD *progressHUD;
}
@property (strong, nonatomic) UserInfo *currentUser;
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UIView *headView;

@property (weak, nonatomic) IBOutlet UIButton *signinBtn;
@property (weak, nonatomic) IBOutlet UIButton *nickNameBtn;
@property (weak, nonatomic) IBOutlet UILabel *integarLabel;
@property (weak, nonatomic) IBOutlet UIButton *cardLeveNameBtn;

@property (weak, nonatomic) IBOutlet UIButton *integralChangeBtn;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *productOrderBtn;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *giftOrderBtn;
@property (weak, nonatomic) IBOutlet UIView *productView;
@property (weak, nonatomic) IBOutlet UIView *giftView;
@property (strong, nonatomic)  UIView *cancelLoginBtnView;

@property (weak, nonatomic) IBOutlet UIButton *productOrOrderItemBtn;
@property (weak, nonatomic) IBOutlet UIImageView *productAndOrderItemImageView;

@property (readwrite) BOOL isProductOrderItemSelected;
@property (weak, nonatomic) IBOutlet MKNumberBadgeView *waitPayOfProductView;
@property (weak, nonatomic) IBOutlet MKNumberBadgeView *waitSendOfProductView;
@property (weak, nonatomic) IBOutlet MKNumberBadgeView *waitReciveOfProduct;
@property (weak, nonatomic) IBOutlet MKNumberBadgeView *waitCommetnOfProduct;
@property (weak, nonatomic) IBOutlet MKNumberBadgeView *waitSendOfGiftView;
@property (weak, nonatomic) IBOutlet MKNumberBadgeView *waitReciveOfGiftView;
@property (strong, nonatomic) IBOutlet UIView *noLoginView;

- (IBAction)userInfoBtnPressed:(id)sender;

- (IBAction)productOrderItemBtnPressed:(id)sender;
- (IBAction)giftOrderItemPressed:(id)sender;

- (IBAction)productOrderBtnPressed:(id)sender;
- (IBAction)giftOrderBtnPressed:(id)sender;


- (IBAction)allOrderBtnPressed:(id)sender;
- (IBAction)integralBtnPressed:(id)sender;
- (IBAction)signBtnPressed:(id)sender;

- (IBAction)integralChangeBtnPressed:(id)sender;
- (IBAction)accountSafeBtnPressed:(id)sender;
- (IBAction)myFavoriteBtnPressed:(id)sender;
- (IBAction)leaveMessageAndNewsBtnPressed:(id)sender;
- (IBAction)addressMangeBtnPressed:(id)sender;
- (IBAction)cancelLoginBtnPressed:(id)sender;
- (IBAction)cardLevelBtnPressed:(id)sender;
//跳转到登陆页面
- (IBAction)goToLoginVC:(id)sender;


@end
