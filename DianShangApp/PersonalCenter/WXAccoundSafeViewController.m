//
//  WXAccoundSafeViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-23.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXAccoundSafeViewController.h"
#import "WXUserActivation1ViewController.h"
#import "WXCommonViewClass.h"
#import "WXUpdatePwdViewController.h"
#import "WXBindWooViewController.h"
#import "FYBindWeiBoViewController.h"
#import "FY_Cloud_getWeiBo.h"
#import "JYNemailUserAction1ViewController.h"
@interface WXAccoundSafeViewController ()

@end

@implementation WXAccoundSafeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"账户设置", @"个人中心/账户设置");
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initAllControllers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
//付岩-添加微博绑定信息获取方法
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   // [self getBindInfo:nil];
    [[FY_Cloud_getWeiBo share] setDelegate:(id)self];
    [[FY_Cloud_getWeiBo share] getWeiBo];
}
#pragma mark - 绑定微博回调
-(void)getWeiBoSuccess:(NSDictionary *)data
{
    if ([data objectForKey:@"hasBindQQ"]) {
        self.bindWeiBoLabel.text = @"已绑定";
    }
    else{
        self.bindWeiBoLabel.text = @"未绑定";
    }
}

-(void)getWeiBoFailed:(NSString*)errMsg
{
    self.bindWeiBoLabel.text = @"未绑定";
}
//------------------------

#pragma mark - All My Methods
- (void)initAllControllers{
    /*设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    NSArray *section0Array = [[NSArray alloc]initWithObjects:
                              NSLocalizedString(@"修改密码", @"账户安全/修改密码"),
                              nil];
    NSArray *section1Array = [[NSArray alloc]initWithObjects:
                              NSLocalizedString(@"绑定手机号", @"账户安全/绑定手机号"),
                              NSLocalizedString(@"绑定邮箱", @"账户安全/绑定微博"),
                              NSLocalizedString(@"绑定哇点账号", @"账户安全/绑定哇点账号"),
                              NSLocalizedString(@"绑定其他账号", @"账户安全/绑定其他账号"),
                              nil];
    dataArray  = [[NSArray alloc]initWithObjects:section0Array,section1Array, nil];
    
        [self.myTableVew reloadData];
}
- (NSString *)getBindInfo:(long)row{
    //初始化绑定
    NSString *str;
    if (self.currentUser != nil) {
        if (row == 0) {//绑定手机号
            NSString *phoneStr = self.currentUser.userPhone;
            if (phoneStr == nil || phoneStr.length <= 0) {
                str = @"未绑定";
            }else{
                str = phoneStr;
            }
        }else if (row == 1){//绑定邮箱
            NSString *emailStr = self.currentUser.userEmail;
            if (emailStr == nil || emailStr.length <= 0) {
                str = @"未绑定";
            }else{
                str = emailStr;
            }
        }else if (row == 2){//绑定哇点账号
            if ([self.currentUser.isBindWoo boolValue]) {
                str = @"已绑定";
            }else{
                str = @"未绑定";
            }
        }
    }else{
        str = @"未绑定";
    }
    return str;
}


//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - UITableView Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArray.count;;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *titleArray = [dataArray objectAtIndex:section];
    return titleArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *CellIdentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    //设置选中时没有背景色
    long section = indexPath.section;
    long row = indexPath.row;
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSArray *titleArray = [dataArray objectAtIndex:section];
    
    UIView *myView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 45)];
    myView.backgroundColor = [UIColor colorWithRed:230.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:1.0f];
    //背景图
    UIImageView *backgroudImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 45)];
        backgroudImageView.image = [UIImage imageNamed:@"oneRowInput"];
    
    
    //文字设置
    UIButton *myBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    myBtn.frame = myView.frame;
    myBtn.titleLabel.font = [UIFont systemFontOfSize:15.0f];
    [myBtn setTitleColor:[UIColor colorWithRed:43.0f/255.0f green:43.0f/255.0f blue:43.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    NSString *titleStr = [titleArray objectAtIndex:row];
    [myBtn setTitle:titleStr forState:UIControlStateNormal];
    myBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    UIEdgeInsets edgetInsets = UIEdgeInsetsMake(0, 16, 0, 0);
    [myBtn setTitleEdgeInsets:edgetInsets];
    [myView setUserInteractionEnabled:NO];
    [myBtn setUserInteractionEnabled:NO];
    [myView addSubview:backgroudImageView];
    [myView addSubview:myBtn];
    if (section == 1) {
        if (row < 3) {
            NSString *bindInfo = [self getBindInfo:row];
            UILabel *bindLabel = [[UILabel alloc]initWithFrame:CGRectMake(140, 13, 150, 18)];
            bindLabel.font = [UIFont systemFontOfSize:12];
            bindLabel.textColor = [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1.0];
            bindLabel.textAlignment = NSTextAlignmentRight;
            bindLabel.text = bindInfo;
            [myView addSubview:bindLabel];
        }
        
        
    }
    
    [cell addSubview:myView];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        
        [self updateBtnPressed:nil];
        
    }else if (indexPath.section == 1) {
        switch (indexPath.row) {
            case 0:
                [self bindPhoneBtnPressed:nil];
                
                break;
            case 1:
                [self bindWeiBoBtnPressed:nil];
                break;
            case 2:
                [self bindWooBtnPressed:nil];
                break;
            case 3:
                [self bindOtherPressed:nil];
                
                break;
            default:
                break;
        }
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 45.0f;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 25.0f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* myView = [[UIView alloc] init];
    myView.backgroundColor = [UIColor colorWithRed:230.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:1.0f];
    myView.layer.borderWidth = 0;
    return myView;
}

//绑定手机
- (IBAction)bindPhoneBtnPressed:(id)sender {
    WXUserActivation1ViewController *activationVC = [[WXUserActivation1ViewController alloc]initWithNibName:@"WXUserActivation1ViewController" bundle:nil];
    activationVC.isFromBindPhone = YES;
    [self.navigationController pushViewController:activationVC animated:YES];
}

//修改密码
- (IBAction)updateBtnPressed:(id)sender {
    WXUpdatePwdViewController *updatePwdVC = [[WXUpdatePwdViewController alloc]initWithNibName:@"WXUpdatePwdViewController" bundle:nil];
    [self.navigationController pushViewController:updatePwdVC animated:YES];
}

//绑定其他账号
- (IBAction)bindOtherPressed:(id)sender
{
    FYBindWeiBoViewController *bindWeiBoVC = [[FYBindWeiBoViewController alloc]initWithNibName:@"FYBindWeiBoViewController" bundle:nil];
    [self.navigationController pushViewController:bindWeiBoVC animated:YES];
}

//绑定邮箱 2014 10 14 付岩电商1.1优化更改
- (IBAction)bindWeiBoBtnPressed:(id)sender
{
    JYNemailUserAction1ViewController *emailVC = [[JYNemailUserAction1ViewController alloc]init];
    emailVC.isFromBindemail=YES;
    [self.navigationController pushViewController:emailVC animated:YES];
}

//绑定哇点账号
- (IBAction)bindWooBtnPressed:(id)sender {
    WXBindWooViewController *bindWooVC = [[WXBindWooViewController alloc]initWithNibName:@"WXBindWooViewController" bundle:nil];
    [self.navigationController pushViewController:bindWooVC animated:YES];

}

@end
