//
//  FYjifenInfoViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-6-3.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"

@interface FYjifenInfoViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,WX_Cloud_Delegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
