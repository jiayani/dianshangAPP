//
//  FYjifenInfoViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-6-3.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYjifenInfoViewController.h"
#import "FYjifenInfoCell.h"
#import "WXCommonViewClass.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
#import "WX_Cloud_Delegate.h"
#import "FY_Cloud_jifenInfo.h"
@interface FYjifenInfoViewController ()
{
    NSArray *jifenArray;
}
@end

@implementation FYjifenInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"积分明细", @"积分明细标题");
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    if(![[WX_Cloud_IsConnection share]isConnectionAvailable]){
        [WXCommonViewClass showHudInButtonOfView:self.view title:NSLocalizedString(@"目前网络连接不可用",@"目前网络连接不可用") closeInSecond:3];
    }else{
        [[FY_Cloud_jifenInfo share] setDelegate:(id)self];
        [[FY_Cloud_jifenInfo share] getData];
    }
    self.view.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)syncjifenSuccess:(id)database
{
    jifenArray = (NSArray *)database;
    [self.tableView reloadData];
}


-(void)syncjifenFailed:(NSString*)errCode
{
    [WXCommonViewClass showHudInView:self.view title:errCode];
}

- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [WXCommonViewClass showHudInView:self.view title:errorCode];
}


#pragma mark tabelView dataSourse

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 50)];
        view.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 320, 40)];
        label.font = [UIFont systemFontOfSize:16];
        long score = [[[jifenArray objectAtIndex:0]  objectForKey:@"allScore"] intValue] + [[[jifenArray objectAtIndex:0]  objectForKey:@"score"] intValue];
        label.text = [NSString stringWithFormat:@"    您当前的积分总数: %ld", score];
        label.textColor = [UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1.0];
        [view addSubview:label];
        return view;
    }
    return nil;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(jifenArray == nil || jifenArray.count == 0){
        return 0;
    }
    return jifenArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *str = @"Cell";
    FYjifenInfoCell *cell = (FYjifenInfoCell *)[tableView dequeueReusableCellWithIdentifier:str];
    if (cell == nil) {
        cell = (FYjifenInfoCell *)[[[NSBundle  mainBundle]  loadNibNamed:@"FYjifenInfoCell" owner:self options:nil]  lastObject];
    }
    cell.lbl_info.text = [[jifenArray objectAtIndex: indexPath.row] objectForKey:@"reason"];
    NSString *string = [NSString stringWithFormat:@"%@",[[jifenArray objectAtIndex:indexPath.row] objectForKey:@"date"]];
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate * date = [formatter dateFromString:string];
    cell.lbl_time.text = [WXCommonDateClass getYYYYMMDDString:date WithIden:@"-" ];
    UIColor *color;
    NSString *jifenStr;
    int creditsStr = [[NSString stringWithFormat:@"%@", [[jifenArray objectAtIndex:indexPath.row] objectForKey:@"score"]] intValue];
    if (creditsStr >= 0) {
        color = [UIColor colorWithRed:51.0/255.0 green:204.0/255.0 blue:0.0/255.0 alpha:1.0];
        jifenStr = [NSString stringWithFormat:@"+%d",creditsStr];
    }else{
        color = [UIColor colorWithRed:204.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
        jifenStr = [NSString stringWithFormat:@"%d",creditsStr];
    }
    cell.lbl_jifen.textColor = color;
    cell.lbl_jifen.text = jifenStr;
        return cell;
}

@end
