//
//  FYjifenChangeViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-6-4.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYjifenChangeViewController.h"
#import "WXCommonViewClass.h"
#import "WX_Cloud_IsConnection.h"
#import "WX_Cloud_Delegate.h"
#import "UserInfo.h"
#import "WXUserDatabaseClass.h"
#import "FY_Cloud_jifenChange.h"
#import "WXBindWooViewController.h"
#import "FY_Cloud_getWooJiFen.h"
#import "XYLoadingView.h"
#import "WXCommonSingletonClass.h"
#import "WXConfigDataControll.h"
@interface FYjifenChangeViewController ()
{
    int btnIndex;
    XYLoadingView *loadingView;
    int point;  //商家积分数
    int wooPoint; //哇点数
}
@end

@implementation FYjifenChangeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"积分互换", @"积分互换标题");
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    [self initAllControllers];
    btnIndex = 1;
    type = 11;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

//加载界面
- (void)initAllControllers
{
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    //自定义滑动seg
    CustomSeg *customSeg = [[[NSBundle mainBundle] loadNibNamed:@"CustomSeg" owner:self options:nil] objectAtIndex:0];
    customSeg.typeSeg = 0;
    customSeg.tag = 99;
    customSeg.delegate = self;
    [customSeg setBtn1Title:@"商家积分转哇点" andBtn2Title:@"哇点转商家积分" andBtn1Image:nil andBtn2Image:nil];
    [self.view addSubview:customSeg];
    UserInfo *user = [WXUserDatabaseClass getCurrentUser];
    self.wooName = user.wooUserName;
    WXCommonSingletonClass *singleClass = [WXCommonSingletonClass share];
    NSString *appName = [WXConfigDataControll getAppName];
    float money = (float)[[singleClass.changeDic objectForKey:@"p_woo"] intValue] / 10;
    self.lbl_guize.text = [NSString stringWithFormat:@"哇点互换规则为100积分兑换%d个哇点。",[[singleClass.changeDic objectForKey:@"p_woo"] intValue]];
    self.textView.text = [NSString stringWithFormat:@"1、什么是积分互换?\n\t积分互换是%@推出的积分之间互相转换的功能,可以将%@积分转换为积分管家的哇点积分,也可以将积分管家的哇点积分换入，转换为%@积分。\n2、积分管家的哇点积分可以用来做什么?\n\t积分管家的哇点积分可以在积分管家APP和哇点网上兑换上万种礼品，还可以用于商家抵现消费；100%@积分=%d哇点=%.1f元。", appName, appName, appName, appName, [[singleClass.changeDic objectForKey:@"p_woo"] intValue], money];
}

- (void)viewWillAppear:(BOOL)animated
{
    UserInfo *user = [WXUserDatabaseClass getCurrentUser];

    if ([user.isBindWoo boolValue] == NO) {
        [self.btn_bangding setHidden:NO];
    }else
    {
        self.lbl_info.text = [NSString stringWithFormat:@""];
        [self.btn_bangding setHidden:YES];
    }
    if ([user.isBindWoo boolValue] == YES) {
        [self getWoo];
    }
}

- (void)getWoo
{
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
    [[FY_Cloud_getWooJiFen share] setDelegate:(id)self];
    [[FY_Cloud_getWooJiFen share] getWooJiFen];
}

#pragma mark 网络回调
//绑定后获取积分成功回调
-(void)syncGetWooSuccess:(NSDictionary *)data
{
    [loadingView removeFromSuperview];
    NSString *str1 = [NSString stringWithFormat:@"%@", [data objectForKey:@"credits"]];
    NSString *str2 = [NSString stringWithFormat:@"%@", [data objectForKey:@"woo"]];
    point = [str1 intValue];
    wooPoint = [str2 intValue];
    NSMutableAttributedString *jifenStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"您现在有商家积分%@个,哇点%@个", str1,str2]];
    [jifenStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(8,str1.length)];
    [jifenStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(8 + str1.length + 4,str2.length)];
    self.lbl_info.attributedText = jifenStr;
    
}

-(void)syncGetWooFailed:(NSString*)errMsg
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:@"获取积分信息失败"];
}

//积分互换成功回调
-(void)syncSetJifenChangeSuccess:(NSDictionary *)data
{
    [loadingView removeFromSuperview];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"积分互换成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
    self.textfield.text = @"";
    self.lbl_jifen.text = @"";
    self.btn_change.enabled = NO;
    [self getWoo];
}

-(void)syncSetJifenChangeFailed:(NSString*)errMsg
{
    self.textfield.text = @"";
    self.lbl_jifen.text = @"";
    self.btn_change.enabled = NO;
    [WXCommonViewClass showHudInView:self.view title:errMsg];
    [loadingView removeFromSuperview];
}
//无网络连接或意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}

//双按钮点击事件
- (void)segChange:(int)index andIsFirst:(int)firstTag
{
    [self.textfield resignFirstResponder];
    self.textfield.text = @"";
    self.lbl_jifen.text = @"";
    if (index == 2) {
        self.textfield.placeholder = @"需转换的哇点数:";
        btnIndex = 2;
        type = 10;
    }else
    {
        self.textfield.placeholder = @"需转换的商家积分数:";
        btnIndex = 1;
        type = 11;
    }
}

- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark textField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *new = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSInteger res = 6-[new length];
    if (res == 6) {
        self.btn_change.enabled = NO;
    }
    else{
        self.btn_change.enabled = YES;
    }
    if(res >= 0){
        
        return YES;
    }
    else{
        NSRange rg = {0,[string length]+res};
        if (rg.length>0) {
        }
        return NO;
    }
}

- (IBAction)touchMe:(id)sender
{
    [self.textfield resignFirstResponder];
}
//计算兑换积分数
- (IBAction)textChange:(id)sender
{
    UITextField *textField = (UITextField *)sender;
    WXCommonSingletonClass *singleClass = [WXCommonSingletonClass share];
    float number = [[singleClass.changeDic objectForKey:@"p_woo"] floatValue] / 100;
    float number2 = 100 / [[singleClass.changeDic objectForKey:@"p_woo"] floatValue];
    if (btnIndex == 1) {
        self.lbl_jifen.text = [NSString stringWithFormat:@"%d", (int)([textField.text intValue] * number)];
    }else
    {
        self.lbl_jifen.text = [NSString stringWithFormat:@"%d", (int)([textField.text intValue] * 0.9 * number2)];
    }
    if ([textField.text isEqual:@""]) {
        self.lbl_jifen.text = @"";
    }
    
    
}

- (IBAction)btn_changeClick:(id)sender
{
    [self.textfield resignFirstResponder];
    UserInfo *user = [WXUserDatabaseClass getCurrentUser];
    if ([user.isBindWoo boolValue] == NO) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"您必须先绑定哇点账号" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        if (type == 11 && [self.textfield.text intValue] > point) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"您输入的积分数不正确，请重新输入" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
        }else if (type == 10 && [self.textfield.text intValue] > wooPoint)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"您输入的积分数不正确，请重新输入" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            
            [alert show];
        }else{
        [[FY_Cloud_jifenChange share] setDelegate:(id)self];
        [[FY_Cloud_jifenChange share] setJifenChange:[self.textfield.text intValue] andType:type];
        loadingView = [[XYLoadingView alloc] initWithType:2];
        [self.view addSubview:loadingView];
        }
    }
    
}

- (IBAction)btn_bangdingClick:(id)sender
{
    WXBindWooViewController *bindVC = [[WXBindWooViewController alloc] initWithNibName:@"WXBindWooViewController" bundle:nil];
    [self.navigationController pushViewController:bindVC animated:YES];
}


@end
