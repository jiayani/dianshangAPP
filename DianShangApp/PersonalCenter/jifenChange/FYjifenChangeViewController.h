//
//  FYjifenChangeViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-6-4.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomSeg.h"
#import "WX_Cloud_Delegate.h"
@interface FYjifenChangeViewController : UIViewController< CustomSegDelegate, UITextFieldDelegate,WX_Cloud_Delegate>
{
    int type;
}
@property (weak, nonatomic) IBOutlet UILabel *lbl_jifen;
@property (weak, nonatomic) IBOutlet UITextField *textfield;
@property (weak, nonatomic) IBOutlet UILabel *lbl_info;
@property (weak, nonatomic) IBOutlet UIButton *btn_bangding;
@property (weak, nonatomic) IBOutlet UIButton *btn_change;
@property (weak, nonatomic) IBOutlet UILabel *lbl_guize;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak,nonatomic) NSString *wooName;
- (IBAction)touchMe:(id)sender;
- (IBAction)textChange:(id)sender;
- (IBAction)btn_changeClick:(id)sender;
- (IBAction)btn_bangdingClick:(id)sender;


@end
