//
//  FYOrderTableViewCell.h
//  DianShangApp
//
//  Created by Fuy on 14-5-27.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AsynImageView;
@interface FYOrderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet AsynImageView *imageView_product;
@property (weak, nonatomic) IBOutlet UILabel *lbl_orderID;
@property (weak, nonatomic) IBOutlet UILabel *lbl_orderCount;
@property (weak, nonatomic) IBOutlet UILabel *lbl_orderMoney;
@property (weak, nonatomic) IBOutlet UILabel *lbl_orderStyel;
@property (weak, nonatomic) IBOutlet UIButton *btn_right;
@property (weak, nonatomic) IBOutlet UIButton *btn_left;
@property (nonatomic) int rightBtnType;
@property (nonatomic) bool isTuan;


@end
