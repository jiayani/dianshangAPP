//
//  CustomSeg.h
//  BookReader2
//
//  Created by 付岩 on 14-2-21.
//  Copyright (c) 2014年 付岩. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CustomSegDelegate <NSObject>
//点击代理，按钮为1或2，isfirst为是否第一次点击某按钮
- (void)segChange:(int)index andIsFirst:(int)firstTag;

@end

@interface CustomSeg : UIView
{
    int firstTag;
}

@property (weak, nonatomic) IBOutlet UIImageView *slider;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (assign,nonatomic) int typeSeg;//控件风格， 0为文字，1为图片，初始化时选择，默认为0
@property (nonatomic, retain) id<CustomSegDelegate>delegate;


- (IBAction)btn_Click:(id)sender;
//初始化方法， 无图片传nil
- (void)setBtn1Title:(NSString *)btn1Title andBtn2Title:(NSString *)btn2Title andBtn1Image:(UIImage *)image1 andBtn2Image:(UIImage *)image2;
//button点击时间，其他类中使用，传值1或2
- (void)segClick:(int)index;


@end
