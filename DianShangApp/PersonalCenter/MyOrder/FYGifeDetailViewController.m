//
//  FYGifeDetailViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-5-28.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYGifeDetailViewController.h"
#import "addressView.h"
#import "listTableViewCell.h"
#import "FYLeaveView.h"
#import "FYDetailTableViewCell.h"
#import "WXCommonViewClass.h"
#import "AsynImageView.h"
#import "WXConfigDataControll.h"
#import "FY_Cloud_giftCancel.h"
#import "FY_Cloud_giftReturn.h"
#import "XYLoadingView.h"
static NSInteger sectionSelected3 = 1;
@interface FYGifeDetailViewController ()
{
    NSDictionary *dic2;
    BOOL isfirst;
    UIButton *rightBtn;
    XYLoadingView *loadingView;
}
@end

@implementation FYGifeDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"礼品订单详情", @"礼品订单详情标题");
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    dic2 = [[NSDictionary alloc] initWithDictionary:self.gifeDic];
    self.lbl_orderStyle.text = self.style;
    self.lbl_score.text = [NSString stringWithFormat:@"%@积分",[dic2 objectForKey:@"totalScore"]];
    self.lbl_score.textColor = [WXConfigDataControll getFontColorOfIntegral];
    isfirst = YES;
    [self initController];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[FY_Cloud_giftCancel share] setDelegate:nil];
    [[FY_Cloud_giftReturn share] setDelegate:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)initController
{
    if (([[dic2 objectForKey:@"redeemState"] intValue] < 5 && [[dic2 objectForKey:@"giftType"] intValue] == 0) || ([[dic2 objectForKey:@"redeemState"] intValue] < 6 && [[dic2 objectForKey:@"giftType"] intValue] == 1)) {
        rightBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:NSLocalizedString(@"取消订单", @"取消订单/取消")];
        [rightBtn addTarget:self action:@selector(rightButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        rightBtn.tag = 31;
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
        self.navigationItem.rightBarButtonItem = rightBarButtonItem;
        return;
    }
    if (([[dic2 objectForKey:@"redeemState"] intValue] == 8 && [[dic2 objectForKey:@"giftType"] intValue] == 0) || ([[dic2 objectForKey:@"redeemState"] intValue] == 7 && [[dic2 objectForKey:@"giftType"] intValue] == 1)) {
        rightBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:NSLocalizedString(@"退货", @"退货/退货")];
        [rightBtn addTarget:self action:@selector(rightButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        rightBtn.tag = 32;
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
        self.navigationItem.rightBarButtonItem = rightBarButtonItem;
        return;
    }
    
}

- (void)rightButtonPressed
{
    switch (rightBtn.tag) {
        case 31:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"您确定要取消订单吗?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alert show];
        }
            break;
        case 32:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"您确定要退货吗?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alert show];
        }
            break;
        default:
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    loadingView = [[XYLoadingView alloc] initWithType:2];
    
    if (buttonIndex == 1 && rightBtn.tag == 31) {
        [self.view addSubview:loadingView];
        [[FY_Cloud_giftCancel share] setDelegate:(id)self];
        [[FY_Cloud_giftCancel share] cancelOrderWithOrderID:[dic2 objectForKey:@"orderNumber"] andGiftType:[dic2 objectForKey:@"giftType"]];
        return;
    }
    if (buttonIndex == 1 && rightBtn.tag == 32) {
        [self.view addSubview:loadingView];
        [[FY_Cloud_giftReturn share] setDelegate:(id)self];
        [[FY_Cloud_giftReturn share] orderReturnWithOrderID:[dic2 objectForKey:@"orderNumber"] andGiftType:[dic2 objectForKey:@"giftType"]];
    }
}
#pragma mark 网络回调
//取消礼品订单
-(void)syncGiftCancelOrderSuccess:(NSString*)EvaString
{
    [loadingView removeFromSuperview];
    self.lbl_orderStyle.text = @"已取消";
    self.navigationItem.rightBarButtonItem = nil;
    [WXCommonViewClass showHudInView:self.view title:EvaString];
    
}
-(void)syncGiftCancelOrderFailed:(NSString*)errCode
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errCode];
}

//礼品退货
-(void)syncGiftOrderReturnSuccess:(NSString*)EvaString
{
    [loadingView removeFromSuperview];
    self.lbl_orderStyle.text = @"退货中";
    self.navigationItem.rightBarButtonItem = nil;
    [WXCommonViewClass showHudInView:self.view title:EvaString];
}

-(void)syncGiftOrderReturnFailed:(NSString*)errCode
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errCode];
}

- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}
#pragma mark 单击展开列表
- (void)singleTap:(UITapGestureRecognizer*)recognizer
{
    if(sectionSelected3 == 1){
        sectionSelected3 = -1;
        isfirst = NO;
    }else{
        sectionSelected3 = 1;
        isfirst = YES;
    }
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
    if(sectionSelected3 != -1){
        //判断该一级菜单下的二级菜单是否超过当前屏幕
        NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:1];
        [self.tableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
   
    [self.tableView reloadData];
    
}


#pragma mark tableView dataSourse

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        addressView *aView = [[addressView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
        aView.lbl_name.text = [NSString stringWithFormat:@"%@", [dic2 objectForKey:@"personName"]];
        aView.lbl_phone.text = [NSString stringWithFormat:@"%@", [dic2 objectForKey:@"phone"]];
        aView.lbl_city.text = [NSString stringWithFormat:@"%@", [dic2 objectForKey:@"address"]];
        aView.imageView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"no-arrow_add_bg" ofType:@"png"]];
        return aView;
        
    }else if(section == 1)
    {
        listTableViewCell *listCell = [[listTableViewCell alloc] init];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        //[listCell xiaorongaddGestureRecognizer:tapGestureRecognizer];
        listCell.lbl_count.hidden = YES;
        listCell.lbl_zongji.hidden = YES;
        listCell.lbl_name.text = @"礼品清单";
        if (isfirst) {
            listCell.imageView_1.image = [UIImage imageNamed:@"quanYiUpArrows.png"];
        }else
        {
            listCell.imageView_1.image = [UIImage imageNamed:@"yuanYiDownArrows.png"];
        }
        return listCell;
        
    }else if (section == 2)
    {
        FYLeaveView *leaveView = [[FYLeaveView alloc] init];
        leaveView.lbl_info.text = @"备注信息";
        leaveView.textView.text = [NSString stringWithFormat:@"%@", [dic2 objectForKey:@"remark"]];
        leaveView.textView.editable = NO;
        
        return leaveView;
        
    }
    return nil;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //二级分类的数量
    if(section != 1){
        return 0;
    }else{
        
        if(sectionSelected3 == -1){
            return 0;
        }else{
            return 1;
        }
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    FYDetailTableViewCell *cell = (FYDetailTableViewCell *)[[[NSBundle  mainBundle]  loadNibNamed:@"FYDetailTableViewCell" owner:self options:nil]  lastObject];
    if (cell == nil){
        cell = [[FYDetailTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lbl_name.text = [NSString stringWithFormat:@"%@", [dic2 objectForKey:@"name"]];
    cell.lbl_price.text = [NSString stringWithFormat:@"%d积分", [[dic2 objectForKey:@"totalScore"] intValue] / [[dic2 objectForKey:@"giftNumber"] intValue]];
    cell.lbl_count.text = [NSString stringWithFormat:@"数量:%@", [dic2 objectForKey:@"giftNumber"]];
    cell.imageView_product.type = 1;
    if ([[dic2 objectForKey:@"giftType"] intValue] == 1) {
        cell.imageView_product.isFromWooGift = YES;
    }
    cell.imageView_product.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    cell.imageView_product.imageURL = [[dic2 objectForKey:@"giftPic"] objectForKey:@"img"];
    return cell;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 100;
    }
    if (section == 1) {
        return 60;
    }
    if (section == 2) {
        return 125;
    }
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(sectionSelected3 == -1 ){
        return 0;
    }else{
        //被选中的一级菜单下的二级菜单
        if(indexPath.section == sectionSelected3){
            return 80;
            //广告位置固定为152
        }else{
            return 0;
        }
    }
}
@end
