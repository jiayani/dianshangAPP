//
//  FYOrderDetailViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-5-27.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"

@interface FYOrderDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate,WX_Cloud_Delegate>
@property (weak, nonatomic) IBOutlet UILabel *lbl_style;
@property (weak, nonatomic) IBOutlet UILabel *lbl_money;
@property (weak, nonatomic) IBOutlet UIButton *btn_pay;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) UIViewController *pushView;
- (IBAction)btn_pay:(id)sender;
@property (strong, nonatomic) NSString *orderID;
@property (strong, nonatomic) NSDictionary* productDic;
-(void)getNewDataFromServer;
- (void)initData;
- (void)gotoPaySuccess;
@end
