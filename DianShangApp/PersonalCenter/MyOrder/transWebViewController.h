//
//  transWebViewController.h
//  DianShangApp
//
//  Created by wa on 14-7-1.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface transWebViewController : UIViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *mianWebView;
@property(retain,nonatomic)NSString *transUrl;
@end
