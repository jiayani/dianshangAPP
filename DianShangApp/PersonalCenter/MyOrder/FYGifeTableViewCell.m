//
//  FYGifeTableViewCell.m
//  DianShangApp
//
//  Created by Fuy on 14-5-28.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYGifeTableViewCell.h"
#import "WXConfigDataControll.h"
@implementation FYGifeTableViewCell

- (void)awakeFromNib
{
    self.lbl_count.textColor = [WXConfigDataControll getFontColorOfIntegral];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
