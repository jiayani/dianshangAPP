//
//  FYOrderTableViewCell.m
//  DianShangApp
//
//  Created by Fuy on 14-5-27.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYOrderTableViewCell.h"
#import "WXConfigDataControll.h"
@implementation FYOrderTableViewCell

- (void)awakeFromNib
{
    self.lbl_orderMoney.textColor = [WXConfigDataControll getFontColorOfIntegral];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
