//
//  CustomSeg.m
//  BookReader2
//
//  Created by 付岩 on 14-2-21.
//  Copyright (c) 2014年 付岩. All rights reserved.
//

#import "CustomSeg.h"

@implementation CustomSeg

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    self.frame = CGRectMake(0, 0, 320, 45);
}


- (IBAction)btn_Click:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    [self.delegate segChange:btn.tag andIsFirst:firstTag];
    for (int i = 1; i < 3; i++) {
        UIButton *btn = (UIButton *)[self viewWithTag:i];
        [btn setTitleColor:[UIColor colorWithRed:120.0/255.0 green:120.0/255.0 blue:120.0/255.0 alpha:1] forState:UIControlStateNormal];
    }
    [btn setTitleColor:[UIColor colorWithRed:1.0/255.0 green:1.0/255.0 blue:1.0/255.0 alpha:1] forState:UIControlStateNormal];
    [UIView animateWithDuration:0.2 animations:^{
        self.slider.frame = CGRectMake(btn.frame.origin.x + 10, 44, self.slider.frame.size.width, self.slider.frame.size.height);
        
    }];
    
    if (btn.tag == 2 && self.typeSeg == 1 && firstTag == 0) {
        firstTag = 1;
        [self.btn2 setBackgroundImage:[UIImage imageNamed:@"price2.png"] forState:UIControlStateNormal];
    }
    else if (btn.tag == 2 && self.typeSeg == 1 && firstTag ==1) {
        firstTag = 0;
        [self.btn2 setBackgroundImage:[UIImage imageNamed:@"price3.png"] forState:UIControlStateNormal];
    }
    if (btn.tag == 1 && self.typeSeg == 1) {
        firstTag = 0;
        [self.btn2 setBackgroundImage:[UIImage imageNamed:@"price1.png"] forState:UIControlStateNormal];
    }

}

- (void)setBtn1Title:(NSString *)btn1Title andBtn2Title:(NSString *)btn2Title andBtn1Image:(UIImage *)image1 andBtn2Image:(UIImage *)image2
{
    [self.btn1 setTitle:btn1Title forState:UIControlStateNormal];
    [self.btn2 setTitle:btn2Title forState:UIControlStateNormal];
    [self.btn1 setImage:image1 forState:UIControlStateNormal];
    [self.btn2 setBackgroundImage:image2 forState:UIControlStateNormal];
    
}

- (void)segClick:(int)index
{
    UIButton *btn = (UIButton *)[self viewWithTag:index];
    [self btn_Click:btn];
    
}

@end
