//
//  FYMyOrderViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-5-27.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYMyOrderViewController.h"
#import "WXCommonViewClass.h"
#import "FYOrderDetailViewController.h"
#import "FYOrderTableViewCell.h"
#import "FYGifeTableViewCell.h"
#import "FYGifeDetailViewController.h"
#import "WX_Cloud_Delegate.h"
#import "FY_Cloud_getOrderList.h"
#import "WX_Cloud_IsConnection.h"
#import "XYOrderPayViewController.h"
#import "XY_Cloud_OrderPaymentInfo.h"
#import "AppDelegate.h"
#import "AlixPay.h"
#import "XYPayWebViewController.h"
#import "FY_Cloud_confirm.h"
#import "FYEvaluateViewController.h"
#import "FY_Cloud_getGiftOrder.h"
#import "WXCommonDateClass.h"
#import "XYNoDataView.h"
#import "AsynImageView.h"
#import "XYLoadingView.h"
#import "XYPaySuccessViewController.h"
#import "transWebViewController.h"
#define maxCountOfOnePage 8;
@interface FYMyOrderViewController ()
{
    NSMutableArray *productArray;
    NSMutableArray *giftArray;
    int btnStyle;
    int rightBtnStyle;
    int index2;//确认收货传值
    int page;
    BOOL isHaveMore;
    BOOL reloading;
    EGORefreshTableFooterView *refreshFooterView;
    XYNoDataView *noData;
    XYLoadingView *loadingView;
    int wooCount;
}
@end

@implementation FYMyOrderViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    switch (self.status) {
        case -1:
            if (self.style == 1) {
                self.title = NSLocalizedString(@"全部商品订单", @"订单列表标题");
            }else
            {
                self.title = NSLocalizedString(@"全部礼品订单", @"订单列表标题");
            }
            break;
        case 1:
            if (self.style == 1) {
                self.title = NSLocalizedString(@"商品-待付款", @"订单列表标题");
            }else
            {
                self.title = NSLocalizedString(@"礼品-待发货", @"订单列表标题");
            }
            break;
        case 2:
            if (self.style == 1) {
                self.title = NSLocalizedString(@"商品-待发货", @"订单列表标题");
            }else
            {
                self.title = NSLocalizedString(@"礼品-待收货", @"订单列表标题");
            }
            break;
        case 3:
            if (self.style == 1) {
                self.title = NSLocalizedString(@"商品-待收货", @"订单列表标题");
            }else
            {
                self.title = NSLocalizedString(@"礼品-待收货", @"订单列表标题");
            }
            break;
        case 4:
            self.title = NSLocalizedString(@"商品-待评价", @"订单列表标题");
            break;

        default:
            break;
    }
    btnStyle = self.style;
    page = 1;
    wooCount = 0;
    productArray = [[NSMutableArray alloc] init];
    giftArray = [[NSMutableArray alloc] init];
    [self initController];
    [self initRefreshFooterView];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    
    [[FY_Cloud_getOrderList share] setDelegate:nil];
    [[FY_Cloud_getGiftOrder share] setDelegate:nil];
    [noData removeFromSuperview];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    page = 1;
    wooCount = 0;
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
    productArray = [[NSMutableArray alloc] init];
    giftArray = [[NSMutableArray alloc] init];
    if (btnStyle == 1) {
        [[FY_Cloud_getOrderList share] setDelegate:(id)self];
        [[FY_Cloud_getOrderList share] getOrderWithStatus:self.status andPage:page];
    }else
    {
        [[FY_Cloud_getGiftOrder share] setDelegate:(id)self];
        [[FY_Cloud_getGiftOrder share] getGiftOrderListWithPage:page andState:self.status andWooGiftsCount:wooCount];
    }
    
    
}

- (void)initRefreshFooterView{
    
    int height = MAX(self.tableView1.bounds.size.height, self.tableView1.contentSize.height);
    refreshFooterView = [[EGORefreshTableFooterView alloc]  initWithFrame:CGRectZero];
    refreshFooterView.frame =CGRectMake(0.0f, height, self.view.frame.size.width, self.tableView1.bounds.size.height);
    //如果contentsize的高度比表的高度小，那么就需要把刷新视图放在表的bounds的下面
    
    refreshFooterView.delegate = self;
    //下拉刷新的控件添加在tableView上
    
    [self.tableView1 addSubview:refreshFooterView];
    reloading = NO;
    
}
- (void)initController
{
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    //自定义滑动seg
    if (self.status == -1) {
        CustomSeg *customSeg = [[[NSBundle mainBundle] loadNibNamed:@"CustomSeg" owner:self options:nil] objectAtIndex:0];
        customSeg.typeSeg = 0;
        customSeg.tag = 99;
        customSeg.delegate = self;
        [customSeg setBtn1Title:@"商品订单" andBtn2Title:@"礼品订单" andBtn1Image:nil andBtn2Image:nil];
        [self.view addSubview:customSeg];
        self.tableView1.frame = CGRectMake(0, 49, self.tableView1.frame.size.width, self.view.frame.size.height - 49);
    }else
    {
        self.tableView1.frame = CGRectMake(0, 4, self.tableView1.frame.size.width, self.view.frame.size.height - 4);
    }
    self.tableView1.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView1.delegate = self;
    self.tableView1.dataSource = self;
    self.tableView1.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}
//获取商品订单列表成功回调
-(void)syncGetOrderListSuccess:(NSArray *)data
{
    // productArray = [NSArray arrayWithArray:data];
    [loadingView removeFromSuperview];
    if (productArray.count == 0) {
        productArray = [NSMutableArray arrayWithArray:data];
    }else{
        [productArray addObjectsFromArray:data] ;
    }
    if (data.count == 8) {
        isHaveMore = YES;
    }else{
        isHaveMore = NO;
    }
    [self reloadUI];
}

-(void)syncGetOrderListFailed:(NSString*)errMsg
{
    [loadingView removeFromSuperview];
    if (self.status != -1) {
        noData = [[XYNoDataView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }else
    {
        noData = [[XYNoDataView alloc] initWithFrame:CGRectMake(0, 46, self.view.frame.size.width, self.view.frame.size.height - 46)];
    }
    noData.megUpLabel.text = @"这里空空如也";
    noData.megDownLabel.text = @"赶快去下订单吧";
    if (page == 1) {
        [self.view addSubview:noData];
        isHaveMore = NO;
        
    }else
    {
        isHaveMore = NO;
        [self reloadUI];
    }
    

}

//获取礼品订单列表成功回调
-(void)syncGetGiftOrderListSuccess:(NSArray *)data
{
    [loadingView removeFromSuperview];
    if (giftArray.count == 0) {
        giftArray = [NSMutableArray arrayWithArray:data];
    }else{
        [giftArray addObjectsFromArray:data] ;
    }
    if (data.count == 8) {
        isHaveMore = YES;
        for (int i = 0; i< data.count; i++) {
            if ([[[data objectAtIndex:i] objectForKey:@"giftType"] intValue] == 1) {
                wooCount ++;
            }
        }
    }else{
        isHaveMore = NO;
    }
    [self reloadUI];
}
-(void)syncGetGiftOrderListFailed:(NSString*)errMsg
{
    [loadingView removeFromSuperview];
    if (self.status != -1) {
        noData = [[XYNoDataView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }else
    {
    noData = [[XYNoDataView alloc] initWithFrame:CGRectMake(0, 46, self.view.frame.size.width, self.view.frame.size.height - 46)];
    }
    noData.megUpLabel.text = @"这里空空如也";
    noData.megDownLabel.text = @"赶快去下订单吧";
    if (page == 1) {
        [self.view addSubview:noData];
        isHaveMore = NO;
    }else
    {
        isHaveMore = NO;
        [self reloadUI];
    }

}

- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [WXCommonViewClass showHudInView:self.view title:errorCode];
}

#pragma mark ScrollView Delegate

-(void)segChange:(int)index andIsFirst:(int)firstTag
{
    btnStyle = index;
    [noData removeFromSuperview];
    page = 1;
    giftArray = [[NSMutableArray alloc] init];
    productArray = [[NSMutableArray alloc] init];
    loadingView = [[XYLoadingView alloc] initWithType:1];
    [self.view addSubview:loadingView];
    if (index == 2) {
        [[FY_Cloud_getGiftOrder share] setDelegate:(id)self];
        [[FY_Cloud_getGiftOrder share] getGiftOrderListWithPage:page andState:self.status andWooGiftsCount:wooCount];
    }else
    {
        [[FY_Cloud_getOrderList share] setDelegate:(id)self];
        [[FY_Cloud_getOrderList share] getOrderWithStatus:self.status andPage:page];
    }
    if (btnStyle == 1) {
        self.title = NSLocalizedString(@"全部商品订单", @"订单列表标题");
    }else
    {
        self.title = NSLocalizedString(@"全部礼品订单", @"订单列表标题");
    }
    //[tableView1 reloadData];
}

#pragma mark tableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (btnStyle == 1)
    {
        return productArray.count;
    }else
    {
        return giftArray.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (btnStyle == 1) {
        NSDictionary *dic = [[NSDictionary alloc] initWithDictionary:[productArray objectAtIndex:indexPath.row]];
        static NSString *CellIdentifier = @"Cell";
        FYOrderTableViewCell *cell = (FYOrderTableViewCell *)[[[NSBundle  mainBundle]  loadNibNamed:@"FYOrderTableViewCell" owner:self options:nil]  lastObject];
        if (cell == nil){
            cell = [[FYOrderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];;
        }
        cell.lbl_orderID.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"orderID"]];
        cell.lbl_orderCount.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"count"]];
        cell.lbl_orderMoney.text = [NSString stringWithFormat:@"￥%@", [dic objectForKey:@"price"]];
        cell.imageView_product.type = 1;
        cell.imageView_product.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
        if ([[dic objectForKey:@"images"] count] != 0) {
            cell.imageView_product.imageURL = [[[dic objectForKey:@"images"] objectAtIndex:0] objectForKey:@"url"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        switch ([[dic objectForKey:@"status"] intValue]) {
            case 1:
                cell.lbl_orderStyel.text = @"待付款";
                [cell.btn_right setTitle:@"付款" forState:UIControlStateNormal];
                cell.btn_right.tag = indexPath.row;
                cell.rightBtnType = 1;
                [cell.btn_right addTarget:self action:@selector(payClick:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btn_left setHidden:YES];
                [cell.btn_right setHidden:NO];
                break;
            case 2:
                cell.lbl_orderStyel.text = @"待发货";
                [cell.btn_left setHidden:YES];
                [cell.btn_right setHidden:YES];
                break;
            case 3:
                cell.lbl_orderStyel.text = @"待收货";
                [cell.btn_left setHidden:NO];
                [cell.btn_right setHidden:NO];
                [cell.btn_right setTitle:@"确认收货" forState:UIControlStateNormal];
                cell.btn_right.tag = indexPath.row;
                cell.btn_left.tag = indexPath.row;
                cell.rightBtnType = 1;
                [cell.btn_right addTarget:self action:@selector(confirmClick:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btn_left addTarget:self action:@selector(seeWhere:) forControlEvents:UIControlEventTouchUpInside];
                break;
            case 4:
                cell.lbl_orderStyel.text = @"待评价";
                [cell.btn_right setTitle:@"评价" forState:UIControlStateNormal];
                [cell.btn_left setHidden:NO];
                [cell.btn_right setHidden:NO];
                cell.btn_right.tag = indexPath.row;
                cell.btn_left.tag = indexPath.row;
                rightBtnStyle = 4;
                [cell.btn_right addTarget:self action:@selector(evaluation:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btn_left addTarget:self action:@selector(seeWhere:) forControlEvents:UIControlEventTouchUpInside];
                break;
            case 5:
                cell.lbl_orderStyel.text = @"已完成";
                [cell.btn_left setHidden:YES];
                [cell.btn_right setHidden:YES];
                break;
            case 6:
                cell.lbl_orderStyel.text = @"退货中";
                [cell.btn_left setHidden:YES];
                [cell.btn_right setHidden:YES];
                break;
            case 7:
                cell.lbl_orderStyel.text = @"已退货";
                [cell.btn_left setHidden:YES];
                [cell.btn_right setHidden:YES];
                break;
            case 8:
                cell.lbl_orderStyel.text = @"已取消";
                [cell.btn_left setHidden:YES];
                [cell.btn_right setHidden:YES];
                break;
            default:
                break;
        }
        if ([[[productArray objectAtIndex:indexPath.row] objectForKey:@"consumeType"] intValue] == 2 || [[[productArray objectAtIndex:indexPath.row] objectForKey:@"consumeType"] intValue] == 3) {
            [cell.btn_left setHidden:YES];
        }
        return cell;
    }
    else
    {
        //礼品列表---------------------------
        NSDictionary *dic2 = [[NSDictionary alloc] initWithDictionary:[giftArray objectAtIndex:indexPath.row]];
        static NSString *CellIdentifier = @"Cell2";
        FYGifeTableViewCell *cell = (FYGifeTableViewCell *)[[[NSBundle  mainBundle]  loadNibNamed:@"FYGifeTableViewCell" owner:self options:nil]  lastObject];
        if (cell == nil){
            cell = [[FYGifeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.lbl_orderID.text = [NSString stringWithFormat:@"%@", [dic2 objectForKey:@"orderNumber"]];
        cell.imageView_gift.type = 1;
        if ([[dic2 objectForKey:@"giftType"] intValue] == 1) {
            cell.imageView_gift.isFromWooGift = YES;
        }
        cell.imageView_gift.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
        cell.imageView_gift.imageURL = [[dic2 objectForKey:@"giftPic"] objectForKey:@"img"];
        cell.lbl_count.text = [dic2 objectForKey:@"totalScore"] ;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        switch ([[dic2 objectForKey:@"redeemState"] intValue]) {
            case 1:
                cell.lbl_orderStyle.text = @"未确认";
                break;
            case 2:
                cell.lbl_orderStyle.text = @"处理中";
                break;
            case 3:
                if (![[dic2 objectForKey:@"giftType"] intValue]) {
                    cell.lbl_orderStyle.text = @"处理中";
                }else
                {
                    cell.lbl_orderStyle.text = @"已确认";
                }
                break;
            case 4:
                if (![[dic2 objectForKey:@"giftType"] intValue]) {
                    cell.lbl_orderStyle.text = @"已确认";
                }else
                {
                    cell.lbl_orderStyle.text = @"处理中";
                }
                break;
            case 5:
                if (![[dic2 objectForKey:@"giftType"] intValue]) {
                    cell.lbl_orderStyle.text = @"已完成";
                }else
                {
                    cell.lbl_orderStyle.text = @"已确认";
                }
                break;
            case 6:
                if (![[dic2 objectForKey:@"giftType"] intValue]) {
                    cell.lbl_orderStyle.text = @"已取消";
                }else
                {
                    cell.lbl_orderStyle.text = @"配送中";
                }
                break;
            case 7:
                if (![[dic2 objectForKey:@"giftType"] intValue]) {
                    cell.lbl_orderStyle.text = @"配送中";
                }else
                {
                    cell.lbl_orderStyle.text = @"已完成";
                }
                break;
            case 8:
                cell.lbl_orderStyle.text = @"已完成";
                break;
            case 9:
                cell.lbl_orderStyle.text = @"已完成";
                break;
            case 10:
                if (![[dic2 objectForKey:@"giftType"] intValue]) {
                    cell.lbl_orderStyle.text = @"已完成";
                }else
                {
                    cell.lbl_orderStyle.text = @"已取消";
                }
                break;
            default:
                break;
        }
        NSDateFormatter  *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        //[formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Etc/UTC"]];
        NSMutableString *timeString = [NSMutableString stringWithFormat:@"%@",[dic2 objectForKey:@"orderDate"]];
        cell.lbl_date.text = timeString;
        return cell;
    }
    
}

- (CGFloat) tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 135;
}

#pragma mark tableView deleagte
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (btnStyle == 1) {
        FYOrderDetailViewController *detailVC = [[FYOrderDetailViewController alloc] initWithNibName:@"FYOrderDetailViewController" bundle:nil];
        detailVC.orderID = [NSString stringWithFormat:@"%@", [[productArray objectAtIndex:indexPath.row] objectForKey:@"orderID"]];
        [self.navigationController pushViewController:detailVC animated:YES];
    }else
    {
        FYGifeDetailViewController *detailVC = [[FYGifeDetailViewController alloc] initWithNibName:@"FYGifeDetailViewController" bundle:nil];
        detailVC.gifeDic = [giftArray objectAtIndex:indexPath.row];
        detailVC.style =((FYGifeTableViewCell *)[tableView cellForRowAtIndexPath:indexPath]).lbl_orderStyle.text;
        [self.navigationController pushViewController:detailVC animated:YES];
    }
}

#pragma mark - cellBtnClick
- (void)payClick:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    NSDictionary *dic = [NSDictionary dictionaryWithDictionary:[productArray objectAtIndex:btn.tag]];
    self.payDic = [[NSDictionary alloc] initWithDictionary:[productArray objectAtIndex:btn.tag]];
    payMent = [[[productArray objectAtIndex:btn.tag]objectForKey:@"payment"] intValue];
    [[XY_Cloud_OrderPaymentInfo share]setDelegate:(id)self];
    [[XY_Cloud_OrderPaymentInfo share]readyToPay:[NSString stringWithFormat:@"%@", [dic objectForKey:@"orderID"]]];
    
}

- (void)seeWhere:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    transWebViewController *trans = [[transWebViewController alloc] initWithNibName:@"transWebViewController" bundle:nil];
    trans.transUrl = [NSString stringWithFormat:@"http://m.kuaidi100.com/index_all.html?type=%@&postid=%@", [[productArray objectAtIndex:btn.tag] objectForKey:@"expressCompanyNameForSearch"],[[productArray objectAtIndex:btn.tag] objectForKey:@"deliveryNo"]];
    [self.navigationController pushViewController:trans animated:YES];
}

- (void)confirmClick:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    if(![[WX_Cloud_IsConnection share]isConnectionAvailable]){
        [WXCommonViewClass showHudInButtonOfView:self.view title:NSLocalizedString(@"目前网络连接不可用",@"目前网络连接不可用") closeInSecond:3];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"确认收货" message:@"是否确认收货" delegate:self cancelButtonTitle:@"暂不确认" otherButtonTitles:@"确认收货", nil];
        [alert show];
        index2 = btn.tag;
        self.payDic = [[NSDictionary alloc] initWithDictionary:[productArray objectAtIndex:btn.tag]];
    }
}
#pragma mark - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 777) {
            NSString * URLString = @"http://itunes.apple.com/cn/app/id535715926?mt=8";
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URLString]];
    }
   
    if (alertView.tag == 100) {
        if (buttonIndex == 1)
        {
            FYEvaluateViewController *evaluationVC = [[FYEvaluateViewController alloc] initWithNibName:@"FYEvaluateViewController" bundle:nil];
            // self.payDic = [[NSDictionary alloc] initWithDictionary:[productArray objectAtIndex:btn.tag]];
            evaluationVC.orderID = [[self.payDic objectForKey:@"orderID"] intValue];
            [self.navigationController pushViewController:evaluationVC animated:YES];
            return;
        }
    }
    if (buttonIndex == 1) {
        [[FY_Cloud_confirm share] setDelegate:(id)self];
        [[FY_Cloud_confirm share] comfirmWithOrderID:[[productArray objectAtIndex:index2]objectForKey:@"orderID"] andRemark:[[productArray objectAtIndex:index2]objectForKey:@"remark"]];
    }
}

- (void)evaluation:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    FYEvaluateViewController *evaluationVC = [[FYEvaluateViewController alloc] initWithNibName:@"FYEvaluateViewController" bundle:nil];
    self.payDic = [[NSDictionary alloc] initWithDictionary:[productArray objectAtIndex:btn.tag]];
    evaluationVC.orderID = [[[productArray objectAtIndex:btn.tag]objectForKey:@"orderID"] intValue];
    [self.navigationController pushViewController:evaluationVC animated:YES];
    
}

//支付宝相关
-(void)ready:(NSString*)RSAString
{
    [[XY_Cloud_OrderPaymentInfo share] setDelegate:nil];
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    appDelegate.myOrderVC = self;
    if(payMent == 1){
        [self startAliPay:RSAString];
    }else if(payMent == 2){
        
        XYPayWebViewController *payWebVC = [[XYPayWebViewController alloc]init];
        payWebVC.payUrl = RSAString;
        /*
         payWebVC.orderId = orderform.orderFormID;
         payWebVC.totalMoney = [orderform.totalAmount floatValue];
         payWebVC.orderFormDetailVC = self;
         payWebVC.payFlag = [orderform.consumeType integerValue];
         */
        if ([[[self.payDic objectForKey:@"consumeType"] substringWithRange:NSMakeRange(0, 1)]isEqualToString:@"3"]) {
            payWebVC.selfTakeFlag = YES;
        }else
        {
            payWebVC.selfTakeFlag = NO;
        }
        [self.navigationController pushViewController:payWebVC animated:YES];
        
    }
}
-(void)getNewDataFromServer
{

    loadingView = [[XYLoadingView alloc] initWithType:2];
    [[UIApplication sharedApplication].keyWindow addSubview:loadingView];
    [[FY_Cloud_getOrderList share] setDelegate:(id)self];
    [[FY_Cloud_getOrderList share] getOrderWithStatus:self.status andPage:page];
    
}
-(void)notReady:(NSString*)errCode
{
    [[XY_Cloud_OrderPaymentInfo share] setDelegate:nil];
}

/*客户端支付*/
- (void)startAliPay:(NSString*)str{
	NSString *appScheme = url_schemes_alixPay;
	
    //获取快捷支付单例并调用快捷支付接口
    AlixPay * alixpay = [AlixPay shared];
    int ret = [alixpay pay:str applicationScheme:appScheme];
    
    if (ret == kSPErrorAlipayClientNotInstalled) {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"您还没有安装支付宝快捷支付，请先安装。" delegate:self cancelButtonTitle:@"确定"otherButtonTitles:nil];
        [alertView setTag:777];
        [alertView show];
    }
}
#pragma mark 确认收货回调
//确认收货成功回调
-(void)syncConfirmSuccess:(NSString*)RSAString
{
    page = 1;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"确认收货成功，现在是否要对商品进行评价" delegate:self cancelButtonTitle:@"下次再评" otherButtonTitles:@"去评价", nil];
    alert.tag = 100;
    [alert show];
    giftArray = [[NSMutableArray alloc] init];
    productArray = [[NSMutableArray alloc] init];
    [[FY_Cloud_getOrderList share] setDelegate:(id)self];
    [[FY_Cloud_getOrderList share] getOrderWithStatus:self.status andPage:page];
}

-(void)syncConfirmFailed:(NSString*)errCode
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errCode];
}

-(void)requestProductList{
    if (btnStyle == 1) {
        [[FY_Cloud_getOrderList share] setDelegate:(id)self];
        [[FY_Cloud_getOrderList share] getOrderWithStatus:self.status andPage:page];
    }else
    {
        [[FY_Cloud_getGiftOrder share] setDelegate:(id)self];
        [[FY_Cloud_getGiftOrder share] getGiftOrderListWithPage:page andState:self.status andWooGiftsCount:wooCount];
    }
}

#pragma mark - 下拉刷新数据方法
//请求数据
-(void)requestData
{
    reloading = YES;
    BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
    if (isConnection) {//有网络情况
        
        //每次进入该页面的时候，重新从服务器端读取数据
        if(refreshFooterView.hasMoreData)
        {
            if (btnStyle == 1) {
                page = 1;
                int myCount = productArray.count;
                int pageCount = myCount/maxCountOfOnePage;
                page = pageCount +1;
                //下载团购信息
                [self requestProductList];
            }
            else
            {
                page = 1;
                int myCount = giftArray.count;
                int pageCount = myCount/maxCountOfOnePage;
                page = pageCount +1;
                //下载团购信息
                [self requestProductList];
            }
        }
    }else{//无网络情况
        [WXCommonViewClass showHudInButtonOfView:self.view title:NoHaveNetwork closeInSecond:2];
    }
    
}

-(void)reloadUI
{
    reloading = NO;
    //停止下拉的动作,恢复表格的便宜
	[refreshFooterView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView1];
    //更新界面
    [self.tableView1 reloadData];
    
    [self setRefreshViewFrame];
    
}

-(void)setRefreshViewFrame
{
    //如果contentsize的高度比表的高度小，那么就需要把刷新视图放在表的bounds的下面
    int height = MAX(self.tableView1.bounds.size.height, self.tableView1.contentSize.height);
    refreshFooterView.frame =CGRectMake(0.0f, height, self.view.frame.size.width, self.tableView1.bounds.size.height);
}

#pragma mark - EGORefreshTableFooterDelegate
//出发下拉刷新动作，开始拉取数据
- (void)egoRefreshTableDidTriggerRefresh:(EGORefreshPos)aRefreshPos
{
    [self requestData];
}


//返回当前刷新状态：是否在刷新
- (BOOL)egoRefreshTableDataSourceIsLoading:(UIView*)view{
	
	return reloading; // should return if data source model is reloading
	
}
//返回刷新时间
// if we don't realize this method, it won't display the refresh timestamp
- (NSDate*)egoRefreshTableDataSourceLastUpdated:(UIView*)view
{
	
	return [NSDate date]; // should return date data source was last changed
	
}

#pragma mark - UIScrollView

//此代理在scrollview滚动时就会调用
//在下拉一段距离到提示松开和松开后提示都应该有变化，变化可以在这里实现
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    refreshFooterView.hasMoreData = isHaveMore;
    [refreshFooterView egoRefreshScrollViewDidScroll:scrollView];
}
//松开后判断表格是否在刷新，若在刷新则表格位置偏移，且状态说明文字变化为loading...
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    refreshFooterView.hasMoreData = isHaveMore;
    [refreshFooterView egoRefreshScrollViewDidEndDragging:scrollView];
}
- (void)gotoPaySuccess{
    XYPaySuccessViewController *paySuccessVC = [[XYPaySuccessViewController alloc]init];
    if ([[self.payDic objectForKey:@"isTuan"] intValue]) {
        paySuccessVC.isTuan = YES;
    }else
    {
        paySuccessVC.isTuan = NO;
    }
    if ([[[self.payDic objectForKey:@"consumeType"] substringWithRange:NSMakeRange(0, 1)]isEqualToString:@"3"]) {
        paySuccessVC.selfTakeFlag = YES;
    }else
    {
        paySuccessVC.selfTakeFlag = NO;
    }
    paySuccessVC.orderID = [self.payDic objectForKey:@"orderID"];
    paySuccessVC.money = [self.payDic objectForKey:@"price"];
    NSString *str;
    switch ([[self.payDic objectForKey:@"payment"] intValue]) {
        case 1:
            str = @"支付宝客户端支付";
            break;
        case 2:
            str = @"支付宝网页支付";
            break;
        case 3:
            str = @"货到付款";
            break;
        case 4:
            str = @"到店支付";
            break;
        default:
            break;
    }
    paySuccessVC.payStyleText = str;
    [self.navigationController pushViewController:paySuccessVC animated:YES];
    
}

@end
