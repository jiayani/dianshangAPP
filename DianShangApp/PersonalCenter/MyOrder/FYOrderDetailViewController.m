//
//  FYOrderDetailViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-5-27.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYOrderDetailViewController.h"
#import "addressView.h"
#import "listTableViewCell.h"
#import "FYLeaveView.h"
#import "WXCommonViewClass.h"
#import "FYDetailTableViewCell.h"
#import "FY_Cloud_getInfoOfProductDeatil.h"
#import "AsynImageView.h"
#import "XY_Cloud_OrderPaymentInfo.h"
#import "AppDelegate.h"
#import "AlixPay.h"
#import "XYPayWebViewController.h"
#import "XYPaySuccessViewController.h"
#import "XYOrderPayViewController.h"
#import "FY_Cloud_cancelOrder.h"
#import "FY_Cloud_orderReturn.h"
#import "transWebViewController.h"
#import "WXConfigDataControll.h"
#import "XYLoadingView.h"
#import "FY_Cloud_confirm.h"
#import "FY_Cloud_cancelReturn.h"
#import "FYEvaluateViewController.h"
static NSInteger sectionSelected2 = 1;
@interface FYOrderDetailViewController ()
{
    NSDictionary *productDic;
    int btnStyle;
    int orderStyle;
    NSArray *proArray;
    BOOL isfirst;
    NSString *ID;
    XYLoadingView *loadingView;
    UIButton *rightBtn;
}
@end

@implementation FYOrderDetailViewController
@synthesize productDic;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"订单详情", @"订单详情标题");
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    self.lbl_money.textColor = [WXConfigDataControll getFontColorOfPrice];
    isfirst = YES;
    ID = self.orderID;
    [self initData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)backBtnPressed{
    if ([self.pushView isKindOfClass:[XYOrderPayViewController class]] || [self.pushView isKindOfClass:[XYPaySuccessViewController class]])
    {
        [WXCommonViewClass goToTabItem:self.navigationController tabItem:4];
        return;
    }else {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}

-(void)getNewDataFromServer
{
    [[FY_Cloud_getInfoOfProductDeatil share] setDelegate:(id)self];
    [[FY_Cloud_getInfoOfProductDeatil share] getGiftOrderListWithOrderID:self.orderID];
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [[FY_Cloud_getInfoOfProductDeatil share] setDelegate:nil];
    [[FY_Cloud_cancelOrder share] setDelegate:(id)self];
    [[FY_Cloud_orderReturn share] setDelegate:(id)self];
}
- (void)initData
{
    
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
    [[FY_Cloud_getInfoOfProductDeatil share] setDelegate:(id)self];
    [[FY_Cloud_getInfoOfProductDeatil share] getGiftOrderListWithOrderID:self.orderID];
}
#pragma mark  网络回调、右侧barbutton点击事件
//获取详情成功回调
-(void)syncGetMyProductSuccess:(NSDictionary *)data
{
    [loadingView removeFromSuperview];
    productDic = [[NSDictionary alloc] initWithDictionary:data];
    proArray = [NSArray arrayWithArray:[data objectForKey:@"goods"]];
    orderStyle = [[data objectForKey:@"status"] intValue];
    NSString *str;
    if (orderStyle < 4) {
        str = @"取消订单";
    }else if(orderStyle < 6)
    {
        str = @"退货";
    }else if(orderStyle < 7)
    {
        str = @"取消退货";
    }
    rightBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:NSLocalizedString(str, @"取消订单/取消")];
    [rightBtn addTarget:self action:@selector(rightButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    switch (orderStyle) {
        case 1:
            self.lbl_style.text = @"待付款";
            if ([[productDic objectForKey:@"payment"] intValue] == 4) {
                self.btn_pay.hidden = YES;
            }
            break;
        case 2:
            self.lbl_style.text = @"待发货";
            if ([[productDic objectForKey:@"payment"] intValue] == 1 || [[productDic objectForKey:@"payment"] intValue] == 2)
            {
                self.navigationItem.rightBarButtonItem = nil;
            }
            self.btn_pay.hidden = YES;
            break;
        case 3:
            self.lbl_style.text = @"待收货";
            [self.btn_pay setTitle:@"确认收货" forState:UIControlStateNormal];
            
            if ([[productDic objectForKey:@"payment"] intValue] == 1 || [[productDic objectForKey:@"payment"] intValue] == 2)
            {
                self.navigationItem.rightBarButtonItem = nil;
            }
            break;
        case 4:
            self.lbl_style.text = @"待评论";
            [self.btn_pay setTitle:@"评价商品" forState:UIControlStateNormal];
           // self.navigationItem.rightBarButtonItem = nil;
            break;
        case 5:
            self.lbl_style.text = @"已完成";
            self.btn_pay.hidden = YES;
            break;
        case 6:
            self.lbl_style.text = @"退货中";
            self.btn_pay.hidden = YES;
            break;
        case 7:
            self.lbl_style.text = @"已退货";
            self.btn_pay.hidden = YES;
            break;
        case 8:
            self.lbl_style.text = @"已取消";
            self.btn_pay.hidden = YES;
            self.navigationItem.rightBarButtonItem = nil;
            break;
        default:
            break;
    }
    self.lbl_money.text = [NSString stringWithFormat:@"￥%@", [productDic objectForKey:@"price"]];
    if ([[productDic objectForKey:@"allowRefund"] intValue] == 0 && orderStyle >= 4) {
        if (orderStyle < 6) {
            self.navigationItem.rightBarButtonItem = nil;
        }
    }
    [self.tableView reloadData];
}

- (void)rightButtonPressed
{
    
    if (orderStyle < 4) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"确定取消订单吗" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alert show];
    }else if (orderStyle < 6)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"确定退货吗" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alert show];
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"确定取消退货吗" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 777) {
            NSString * URLString = @"http://itunes.apple.com/cn/app/id535715926?mt=8";
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URLString]];
        return;
        }
    if (alertView.tag == 10) {
        if (buttonIndex == 1)
        {
            //确认收货
            [[FY_Cloud_confirm share] setDelegate:(id)self];
            [[FY_Cloud_confirm share] comfirmWithOrderID:self.orderID andRemark:@""];
            return;
        }
    }
    if (alertView.tag == 11) {
        if (buttonIndex == 1)
        {
            //评论
            FYEvaluateViewController *evaluationVC = [[FYEvaluateViewController alloc] initWithNibName:@"FYEvaluateViewController" bundle:nil];
            evaluationVC.orderID = [self.orderID intValue];
            [self.navigationController pushViewController:evaluationVC animated:YES];
            return;
        }
    }
    if (buttonIndex == 1) {
        if (orderStyle < 4) {
            loadingView = [[XYLoadingView alloc] initWithType:2];
            [self.view addSubview:loadingView];
            [[FY_Cloud_cancelOrder share] setDelegate:(id)self];
            [[FY_Cloud_cancelOrder share] cancelOrderWithOrderID:ID];
            
        }else if (orderStyle < 6)
        {
            loadingView = [[XYLoadingView alloc] initWithType:2];
            [self.view addSubview:loadingView];
            [[FY_Cloud_orderReturn share] setDelegate:(id)self];
            [[FY_Cloud_orderReturn share] orderReturnWithOrderID:ID];
        }else
        {
            loadingView = [[XYLoadingView alloc] initWithType:2];
            [self.view addSubview:loadingView];
            [[FY_Cloud_cancelReturn share] setDelegate:(id)self];
            [[FY_Cloud_cancelReturn share] cancelReturnWithOrderID:ID];
        }
    }
}

-(void)syncGetMyProductFailed:(NSString*)errMsg
{
    [WXCommonViewClass showHudInView:self.view title:errMsg];
}

//取消订单
-(void)syncCancelOrderSuccess:(NSString*)EvaString;
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:EvaString];
    self.navigationItem.rightBarButtonItem = nil;
    [WXCommonViewClass goToTabItem:self.navigationController tabItem:4];
}

-(void)syncCancelOrderFailed:(NSString*)errCode;
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errCode];
}

//申请退货
-(void)syncOrderReturnSuccess:(NSString*)EvaString
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:EvaString];
    [self initData];
}

-(void)syncOrderReturnFailed:(NSString*)errCode;
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errCode];
}
//取消退货回调
-(void)syncCancelReturnSuccess:(NSString*)EvaString
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:EvaString];
    [self initData];
}

-(void)syncCancelReturnFailed:(NSString*)errCode
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errCode];
}

//无网络连接
- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}

- (void)singleTap:(UITapGestureRecognizer*)recognizer
{
    if(sectionSelected2 == 1){
        sectionSelected2 = -1;
        isfirst = NO;
    }else{
        sectionSelected2 = 1;
        isfirst = YES;
    }
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
    if(sectionSelected2 != -1){
        //判断该一级菜单下的二级菜单是否超过当前屏幕
        NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:1];
        [self.tableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    [self.tableView reloadData];
    
}

- (void)seeWuLiu:(id)sender
{
    transWebViewController *trans = [[transWebViewController alloc] initWithNibName:@"transWebViewController" bundle:nil];
    
    trans.transUrl = [NSString stringWithFormat:@"http://m.kuaidi100.com/index_all.html?type=%@&postid=%@", [productDic objectForKey:@"expressCompanyNameForSearch"],[productDic objectForKey:@"deliveryNo"]];
    [self.navigationController pushViewController:trans animated:YES];
}

#pragma mark tableView dataSourse

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        addressView *aView = [[addressView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
        aView.lbl_city.text = [productDic objectForKey:@"sendAddress"];
        aView.lbl_phone.text = [productDic objectForKey:@"phone"];
        aView.lbl_name.text = [productDic objectForKey:@"contactName"];
        aView.imageView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"no-arrow_add_bg" ofType:@"png"]];
        return aView;
        
    }else if(section == 1)
    {
        listTableViewCell *listCell = [[listTableViewCell alloc] init];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [listCell addGestureRecognizer:tapGestureRecognizer];
        listCell.lbl_count.text = [NSString stringWithFormat:@"共%@件", [productDic objectForKey:@"count"]];
        listCell.lbl_money.text = [NSString stringWithFormat:@"%@", [productDic objectForKey:@"goodTotPrice"]];
        if (isfirst) {
            listCell.imageView_1.image = [UIImage imageNamed:@"quanYiUpArrows.png"];
        }else
        {
            listCell.imageView_1.image = [UIImage imageNamed:@"yuanYiDownArrows.png"];
        }
        return listCell;
        
    }else if (section == 2)
    {
        UIView *peisongView = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, 320, 50)];
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 59, 320, 1)];
        lineView.backgroundColor = [UIColor colorWithRed:224.0/255.0 green:224.0/255.0 blue:224.0/255.0 alpha:1.0];
        [peisongView addSubview:lineView];
        UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(12, 14, 80, 20)];
        lable.text = @"配送方式:";
        lable.backgroundColor = [UIColor clearColor];
        lable.font = [UIFont systemFontOfSize:15];
        UILabel *lable2 = [[UILabel alloc]initWithFrame:CGRectMake(80, 14, 120, 20)];
        
        lable2.text = [NSString stringWithFormat:@"%@",[productDic objectForKey:@"consumeTypeDesc"]];
        lable2.backgroundColor = [UIColor clearColor];
        lable2.textColor = [UIColor grayColor];
        lable2.font = [UIFont systemFontOfSize:14];
        UILabel *lable3 = [[UILabel alloc]initWithFrame:CGRectMake(12, 36, 80, 20)];
        lable3.text = @"货运单号:";
        lable3.backgroundColor = [UIColor clearColor];
        lable3.font = [UIFont systemFontOfSize:15];
        UILabel *lable4 = [[UILabel alloc]initWithFrame:CGRectMake(80, 36, 120, 20)];
        lable4.text = @"配送方式";
        lable4.backgroundColor = [UIColor clearColor];
        lable4.font = [UIFont systemFontOfSize:14];
        lable4.text = [NSString stringWithFormat:@"%@",[productDic objectForKey:@"deliveryNo"]];
        lable4.textColor = [UIColor grayColor];
        [peisongView addSubview:lable];
        [peisongView addSubview:lable2];
        [peisongView addSubview:lable3];
        [peisongView addSubview:lable4];
        if (orderStyle == 1 || [[productDic objectForKey:@"payment"] intValue] == 3 || orderStyle == 8) {
            lable3.hidden = YES;
            lable4.hidden = YES;
        }

        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(240, 16, 70, 30)];
        [button setTitle:@"查看物流" forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"getVerifyImage.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(seeWuLiu:) forControlEvents:UIControlEventTouchUpInside];
        if ([[productDic objectForKey:@"status"] intValue] == 2 || [[productDic objectForKey:@"status"] intValue] == 1) {
            lable4.text = @"待发货";
        }else if ([[productDic objectForKey:@"status"] intValue] != 8 && [[productDic objectForKey:@"payment"] intValue] != 4)
        {
           [peisongView addSubview:button];
        }
        if ([[[productDic objectForKey:@"consumeType"] substringWithRange:NSMakeRange(0, 1)]isEqualToString:@"2"] || [[[productDic objectForKey:@"consumeType"] substringWithRange:NSMakeRange(0, 1)]isEqualToString:@"3"]) {
            lable3.hidden = YES;
            lable4.hidden = YES;
            [button setHidden:YES];
        }
        if ([[productDic objectForKey:@"payment"] intValue] == 4 && [[productDic objectForKey:@"status"] intValue] == 3) {
            lable4.text = @"已发货";
        }
        peisongView.backgroundColor = [UIColor whiteColor];
        return peisongView;
        
    }else if (section == 3)
    {
        UIView *peisongView = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, 320, 50)];
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 49, 320, 1)];
        lineView.backgroundColor = [UIColor colorWithRed:224.0/255.0 green:224.0/255.0 blue:224.0/255.0 alpha:1.0];
        [peisongView addSubview:lineView];
        UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(12, 20, 80, 20)];
        lable.text = @"付款方式";
        lable.backgroundColor = [UIColor clearColor];
        lable.font = [UIFont systemFontOfSize:15];
        [peisongView addSubview:lable];
        UILabel *lable2 = [[UILabel alloc]initWithFrame:CGRectMake(80, 20, 120, 20)];
        switch ([[productDic objectForKey:@"payment"] intValue]) {
            case 1:
                lable2.text = @"支付宝客户端支付";
                btnStyle = 1;
                break;
            case 2:
                lable2.text = @"支付宝网页支付";
                btnStyle = 2;
                break;
            case 4:
                lable2.text = @"货到付款";
                break;
            case 3:
                lable2.text = @"到店支付";
                break;
            default:
                break;
        }
        
        lable2.backgroundColor = [UIColor clearColor];
        lable2.font = [UIFont systemFontOfSize:14];
        [peisongView addSubview:lable2];
        lable2.textColor = [UIColor grayColor];
        peisongView.backgroundColor = [UIColor whiteColor];
        return peisongView;
        
    }else if (section == 4)
    {
        UIView *peisongView = [[UIView alloc ] initWithFrame:CGRectMake(0, 0, 320, 50)];
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 59, 320, 1)];
        lineView.backgroundColor = [UIColor colorWithRed:224.0/255.0 green:224.0/255.0 blue:224.0/255.0 alpha:1.0];
        [peisongView addSubview:lineView];
        UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(12, 14, 80, 20)];
        lable.text = @"订单信息";
        lable.backgroundColor = [UIColor clearColor];
        lable.font = [UIFont systemFontOfSize:15];
        [peisongView addSubview:lable];
        UILabel *lable2 = [[UILabel alloc]initWithFrame:CGRectMake(80, 14, 200, 20)];
        lable2.text = [NSString stringWithFormat:@"订单编号:%@",[productDic objectForKey:@"orderID"]];
        lable2.backgroundColor = [UIColor clearColor];
        lable2.textColor = [UIColor grayColor];
        lable2.font = [UIFont systemFontOfSize:13];
        [peisongView addSubview:lable2];
        UILabel *lable3 = [[UILabel alloc]initWithFrame:CGRectMake(80, 32, 200, 20)];
        lable3.text = [NSString stringWithFormat:@"下单时间:%@", [productDic objectForKey:@"date"]];
        lable3.backgroundColor = [UIColor clearColor];
        lable3.textColor = [UIColor grayColor];
        lable3.font = [UIFont systemFontOfSize:13];
        [peisongView addSubview:lable3];
        peisongView.backgroundColor = [UIColor whiteColor];
        return peisongView;
        
    }else
    {
        FYLeaveView *leaveView = [[FYLeaveView alloc] init];
        leaveView.lbl_info.text = @"备注信息";
        leaveView.textView.editable = NO;
        leaveView.textView.text = [productDic objectForKey:@"remark"];
        return leaveView;
    }
    
    return nil;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //二级分类的数量
    if(section != 1){
        return 0;
    }else{
        
        if(sectionSelected2 == -1){
            return 0;
        }else{
            return proArray.count;
        }
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    FYDetailTableViewCell *cell = (FYDetailTableViewCell *)[[[NSBundle  mainBundle]  loadNibNamed:@"FYDetailTableViewCell" owner:self options:nil]  lastObject];
    if (cell == nil){
        cell = [[FYDetailTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lbl_name.text = [[proArray objectAtIndex:indexPath.row] objectForKey:@"goodsName"];
    cell.lbl_price.text = [NSString stringWithFormat:@"￥%@", [[proArray objectAtIndex:indexPath.row] objectForKey:@"goodsPrice"]];
    cell.lbl_count.text = [NSString stringWithFormat:@"数量:%@", [[proArray objectAtIndex:indexPath.row] objectForKey:@"goodsCount"]];
    cell.lbl_size.text = [NSString stringWithFormat:@"%@",  [[proArray objectAtIndex:indexPath.row] objectForKey:@"type_desc"]];
    cell.imageView_product.type = 1;
    cell.imageView_product.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    cell.imageView_product.imageURL = [[[[proArray objectAtIndex:indexPath.row] objectForKey:@"goodsImages"] objectAtIndex:0] objectForKey:@"url"];
    
    return cell;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 6;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 100;
    }
    if (section == 1) {
        return 60;
    }
    if (section == 2) {
        return 60;
    }
    if (section == 3) {
        return 50;
    }
    if (section == 4) {
        return 60;
    }
    if (section == 5) {
        return 125;
    }
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(sectionSelected2 == -1 ){
        return 0;
    }else{
        //被选中的一级菜单下的二级菜单
        if(indexPath.section == sectionSelected2){
            return 80;
            //广告位置固定为152
        }else{
            return 0;
        }
    }
}

//付款、评价、确认收货按钮点击事件
- (IBAction)btn_pay:(id)sender
{
    
    
    if (orderStyle == 1) {
        [[XY_Cloud_OrderPaymentInfo share]setDelegate:(id)self];
        [[XY_Cloud_OrderPaymentInfo share]readyToPay:self.orderID];
    }else if (orderStyle == 3)
    {
        //确认收货
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"确定要确认收货吗?" delegate:self cancelButtonTitle:@"暂不确认" otherButtonTitles:@"确定收货", nil];
        alert.tag = 10;
        [alert show];
        
    }else
    {
        //评价商品
        FYEvaluateViewController *evaluationVC = [[FYEvaluateViewController alloc] initWithNibName:@"FYEvaluateViewController" bundle:nil];
        evaluationVC.orderID = [self.orderID intValue];
        [self.navigationController pushViewController:evaluationVC animated:YES];
        return;
        
    }
}
//确认收货成功回调
-(void)syncConfirmSuccess:(NSString*)RSAString
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"确认收货成功，现在是否要对商品进行评价?" delegate:self cancelButtonTitle:@"下次再评" otherButtonTitles:@"去评价", nil];
    alert.tag = 11;
    [alert show];
    [self initData];
}

-(void)syncConfirmFailed:(NSString*)errCode
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errCode];
}
#pragma mark 支付宝相关
/*客户端支付*/
- (void)startAliPay:(NSString*)str{
	NSString *appScheme = url_schemes_alixPay;
    //获取快捷支付单例并调用快捷支付接口
    AlixPay * alixpay = [AlixPay shared];
    int ret = [alixpay pay:str applicationScheme:appScheme];
    
    if (ret == kSPErrorAlipayClientNotInstalled) {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"您还没有安装支付宝快捷支付，请先安装。" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertView setTag:777];
        [alertView show];
    }
}
//支付宝相关
-(void)ready:(NSString*)RSAString
{
    [[XY_Cloud_OrderPaymentInfo share] setDelegate:nil];
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    appDelegate.orderDetailVC = self;
    if( btnStyle == 1 ){
        [self startAliPay:RSAString];
    }else if( btnStyle  == 2 ){
        
        XYPayWebViewController *payWebVC = [[XYPayWebViewController alloc]init];
        payWebVC.payUrl = RSAString;
        /*
         payWebVC.orderId = orderform.orderFormID;
         payWebVC.totalMoney = [orderform.totalAmount floatValue];
         payWebVC.orderFormDetailVC = self;
         payWebVC.payFlag = [orderform.consumeType integerValue];
         */
        if ([[[productDic objectForKey:@"consumeType"] substringWithRange:NSMakeRange(0, 1)]isEqualToString:@"3"]) {
            payWebVC.selfTakeFlag = YES;
        }else
        {
            payWebVC.selfTakeFlag = NO;
        }
        [self.navigationController pushViewController:payWebVC animated:YES];
        
    }
}
-(void)notReady:(NSString*)errCode
{
    [[XY_Cloud_OrderPaymentInfo share] setDelegate:nil];
}
- (void)gotoPaySuccess{
    XYPaySuccessViewController *paySuccessVC = [[XYPaySuccessViewController alloc]init];
    if ([[productDic objectForKey:@"isTuan"] intValue]) {
        paySuccessVC.isTuan = YES;
    }else
    {
        paySuccessVC.isTuan = NO;
    }
    paySuccessVC.orderID = self.orderID;
    paySuccessVC.money = self.lbl_money.text;
    NSString *str;
    if ([[[productDic objectForKey:@"consumeType"] substringWithRange:NSMakeRange(0, 1)]isEqualToString:@"3"]) {
        paySuccessVC.selfTakeFlag = YES;
    }else
    {
        paySuccessVC.selfTakeFlag = NO;
    }
    switch ([[productDic objectForKey:@"payment"] intValue]) {
        case 1:
            str = @"支付宝客户端支付";
            break;
        case 2:
            str = @"支付宝网页支付";
            break;
        case 3:
            str = @"货到付款";
            break;
        case 4:
            str = @"到店支付";
            break;
        default:
            break;
    }
    paySuccessVC.payStyleText = str;
    [self.navigationController pushViewController:paySuccessVC animated:YES];
    
}


@end
