//
//  transWebViewController.m
//  DianShangApp
//
//  Created by wa on 14-7-1.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "transWebViewController.h"
#import "WXCommonViewClass.h"
@interface transWebViewController ()

@end

@implementation transWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"查看物流", @"查看物流/查看物流");
        /*设置导航返回按钮*/
        UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
        [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
        self.navigationItem.leftBarButtonItem = leftBarButtonItem;
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"正在加载…", @"登录/正在加载…") nav:self.navigationController];
    NSURL *url = [NSURL URLWithString:self.transUrl];
    [self.mianWebView loadRequest:[NSURLRequest requestWithURL:url]];
}

//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    
}
@end
