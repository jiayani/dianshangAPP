//
//  FYMyOrderViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-5-27.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomSeg.h"
#import "EGORefreshTableFooterView.h"
#import "WX_Cloud_Delegate.h"
@interface FYMyOrderViewController : UIViewController <CustomSegDelegate, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, EGORefreshTableDelegate,WX_Cloud_Delegate>
{
    int payMent;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView1;
@property (nonatomic) int status;
@property (nonatomic) int style;
@property (strong ,nonatomic) NSDictionary *payDic;
-(void)getNewDataFromServer;
- (void)gotoPaySuccess;
@end
