//
//  FYGifeDetailViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-5-28.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"

@interface FYGifeDetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate,WX_Cloud_Delegate>
@property (weak, nonatomic) IBOutlet UILabel *lbl_score;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_orderStyle;
@property (weak, nonatomic) NSDictionary *gifeDic;
@property (weak, nonatomic) NSString *style;
@end
