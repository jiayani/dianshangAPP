//
//  FYGifeTableViewCell.h
//  DianShangApp
//
//  Created by Fuy on 14-5-28.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AsynImageView;
@interface FYGifeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet AsynImageView *imageView_gift;
@property (weak, nonatomic) IBOutlet UILabel *lbl_orderID;
@property (weak, nonatomic) IBOutlet UILabel *lbl_count;
@property (weak, nonatomic) IBOutlet UILabel *lbl_orderStyle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;

@end
