//
//  FYCollectCell.h
//  DianShangApp
//
//  Created by Fuy on 14-6-5.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AsynImageView;
@interface FYCollectCell : UITableViewCell

@property (weak, nonatomic) IBOutlet  AsynImageView *imageview_collect;
@property (weak, nonatomic) IBOutlet UILabel *lbl_info;
@property (weak, nonatomic) IBOutlet UILabel *lbl_money;
@property (weak, nonatomic) IBOutlet UILabel *lbl_mans;

@end
