//
//  FYCollectViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-6-5.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYCollectViewController.h"
#import "FYCollectCell.h"
#import "WXCommonViewClass.h"
#import "FY_Cloud_getCollect.h"
#import "WX_Cloud_Delegate.h"
#import "WX_Cloud_IsConnection.h"
#import "AsynImageView.h"
#import "XYGoodsInforViewController.h"
#import "XYNoDataView.h"
#import "FY_Cloud_deleteColect.h"
@interface FYCollectViewController ()
{
    NSMutableArray *collectArray;
}
@end

@implementation FYCollectViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"我的收藏", @"我的收藏标题");
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    collectArray = [[NSMutableArray alloc] init];
    [[FY_Cloud_getCollect share] setDelegate:(id)self];
    [[FY_Cloud_getCollect share] getCollect];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [[FY_Cloud_getCollect share] setDelegate:nil];
    [[FY_Cloud_deleteColect share] setDelegate:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark 网络回调
//获取收藏列表成功回调
-(void)syncGetCollectSuccess:(NSDictionary *)data
{
    collectArray = [[NSMutableArray alloc] initWithArray:[data objectForKey:@"data"]];
    [self.tableView reloadData];
}
-(void)syncGetCollectFailed:(NSString*)errMsg
{
    collectArray = nil;
    [self.tableView reloadData];
    XYNoDataView *noData = [[XYNoDataView alloc] initWithFrame:self.view.frame];
    noData.megUpLabel.text = @"这里空空如也";
    noData.megDownLabel.text = @"亲，您还没有收藏任何商品,赶快去挑选商品吧";
    [self.view addSubview:noData];
}
//删除收藏成功回调
- (void)syncDeleteCollectSuccess:(NSString *)message
{
    
}
- (void)syncDeleteCollectFailed:(NSString*)errCode
{
    
}

- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [WXCommonViewClass showHudInView:self.view title:errorCode];
}

#pragma mark tableView delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(collectArray == nil || collectArray.count == 0){
        return 0;
    }
    return collectArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *str = @"Cell";
    FYCollectCell *cell = (FYCollectCell*)[tableView dequeueReusableCellWithIdentifier:str];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"FYCollectCell" owner:self options:nil] lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary * object = [collectArray objectAtIndex:indexPath.row];
    NSString * image = [object objectForKey:@"img"];
    cell.imageview_collect.type = 1;
    cell.imageview_collect.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    cell.imageview_collect.imageURL = [NSString stringWithFormat:@"%@",image];
    cell.lbl_info.text = [NSString stringWithFormat:@"%@", [object objectForKey:@"productTitle"]];
    cell.lbl_money.text = [NSString stringWithFormat:@"￥%@", [object objectForKey:@"originPrice"]];
    cell.lbl_mans.text = [NSString stringWithFormat:@"%@人收藏", [object objectForKey:@"popularity"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *productID = [[collectArray objectAtIndex:indexPath.row] objectForKey:@"productID"];
    XYGoodsInforViewController *infoVC = [[XYGoodsInforViewController alloc] initWithNibName:@"XYGoodsInforViewController" bundle:nil];
    infoVC.productId = productID;
    [self.navigationController pushViewController:infoVC animated:YES];
    
    
}

- (NSString*)tableView:(UITableView *)tableView
titleForDeleteConfirmationButtonForRowAtIndexPath:
(NSIndexPath *)indexPath
{
    return @"删除";
}

- (BOOL)tableView:(UITableView *)tableView
canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:
(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:
(NSIndexPath *)indexPath
{
    
    [[FY_Cloud_deleteColect share] setDelegate:(id)self];
    [[FY_Cloud_deleteColect share] deleteCollectWithProductID:[[[collectArray objectAtIndex:indexPath.row] objectForKey:@"productID"] intValue]];
    [collectArray removeObjectAtIndex:indexPath.row];
    [self.tableView reloadData];
    
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)
tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}



@end
