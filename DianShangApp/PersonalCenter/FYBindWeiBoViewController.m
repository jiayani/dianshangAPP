//
//  FYBindWeiBoViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-7-11.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYBindWeiBoViewController.h"
#import "WXCommonViewClass.h"
#import "WXCommonSingletonClass.h"
#import "FY_Cloud_getWeiBo.h"
#import "FY_Cloud_setWeiBo.h"
#import "FY_Cloud_cancelWeiBo.h"
#import "WXConfigDataControll.h"
#import "WXUserDatabaseClass.h"
#import "UserInfo.h"
#import "FY_Cloud_userBindQQ.h"
#import "FY_Cloud_cancelBindQQ.h"
@interface FYBindWeiBoViewController ()

@end

@implementation FYBindWeiBoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"绑定微博", @"绑定微博标题");
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    _permissions = [NSArray arrayWithObjects:
                    kOPEN_PERMISSION_GET_USER_INFO,
                    kOPEN_PERMISSION_GET_SIMPLE_USER_INFO,
                    kOPEN_PERMISSION_ADD_ALBUM,
                    kOPEN_PERMISSION_ADD_ONE_BLOG,
                    kOPEN_PERMISSION_ADD_PIC_T,
                    kOPEN_PERMISSION_ADD_TOPIC,
                    kOPEN_PERMISSION_DEL_IDOL,
                    kOPEN_PERMISSION_GET_IDOLLIST,
                    kOPEN_PERMISSION_GET_INFO,
                    kOPEN_PERMISSION_GET_OTHER_INFO,
                    kOPEN_PERMISSION_GET_REPOST_LIST,
                    kOPEN_PERMISSION_LIST_ALBUM,
                    kOPEN_PERMISSION_UPLOAD_PIC,
                    kOPEN_PERMISSION_GET_VIP_INFO,
                    kOPEN_PERMISSION_GET_VIP_RICH_INFO,
                    nil];
   // NSString *appid = @"222222";
    //
    NSString *appid = [WXConfigDataControll getQQLoginID];
    _tencentOAuth = [[TencentOAuth alloc] initWithAppId:appid
											andDelegate:self];
    [self initTXSwitch];
    [self initQQSwitch];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    

}

- (void)viewWillDisappear:(BOOL)animated
{
    [[FY_Cloud_getWeiBo share] setDelegate:nil];
    [[FY_Cloud_cancelWeiBo share] setDelegate:nil];
    [[FY_Cloud_setWeiBo share] setDelegate:nil];
}
//初始化switch控件
- (void)initTXSwitch
{
    [[FY_Cloud_getWeiBo share] setDelegate:(id)self];
    [[FY_Cloud_getWeiBo share] getWeiBo];
}

- (void)initQQSwitch
{
    UserInfo *user = [WXUserDatabaseClass getCurrentUser];
    if ([user.bindQQID isEqualToString:@""] || user.bindQQID == nil) {
        self.switch_QQ.on = NO;
        //isHasQQ = NO;
    }else
    {
        self.switch_QQ.on = YES;
        //isHasQQ = YES;
    }
}
//获取绑定信息回调
-(void)getWeiBoSuccess:(NSDictionary *)data
{
    if ([data objectForKey:@"hasBindQQ"]) {
        self.switch_QQ.on = YES;
    }
    else{
        self.switch_QQ.on = NO;
    }
}

-(void)getWeiBoFailed:(NSString*)errMsg
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.shareStr = @"TX";
    singletonClass.shareWhere = 1;
  //  [singletonClass.wbapi loginWithDelegate:self andRootController:self];
}
//switch点击事件
- (IBAction)switch_QQ:(id)sender
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([self.switch_QQ isOn]) {
        [singletonClass.wbapi loginWithDelegate:self andRootController:self];
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"确定要取消绑定吗?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag = 100;
        [alert show];;
    }
}
//绑定QQ点击事件
- (IBAction)switch_QQLogin:(id)sender
{
    if ([self.switch_QQ isOn]) {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        singletonClass.shareStr = @"QQ";
        [_tencentOAuth authorize:_permissions inSafari:NO];
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"确定要取消绑定吗?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag = 101;
        [alert show];
    }
}

- (void)backBtnPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}
//授权成功回调
- (void)DidAuthFinished:(WeiboApi *)wbapi_
{
    [[FY_Cloud_setWeiBo share] setDelegate:(id)self];
    [[FY_Cloud_setWeiBo share] setWeiBoWithType:@"1" andQQOpenID:wbapi_.openid andNonce:wbapi_.accessToken andSingature:@"GET" andToken:wbapi_.refreshToken andTimestamp:[NSString stringWithFormat:@"%f", wbapi_.expires] andVeritier:@"1111"];
    [WXCommonViewClass showHudInView:self.view title:@"授权成功"];
    
}
- (void)DidAuthCanceled:(WeiboApi *)wbapi_
{
    [WXCommonViewClass showHudInView:self.view title:@"授权失败"];
    self.switch_QQ.on = NO;
}

/**
 * @brief   授权成功后的回调
 * @param   INPUT   error   标准出错信息
 * @return  无返回
 */
- (void)DidAuthFailWithError:(NSError *)error
{
    [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:@"授权失败"];
    self.switch_QQ.on = NO;
    
}

//绑定微博成功回调
-(void)setWeiBoSuccess:(NSString *)data
{
    
}

-(void)setWeiBoFailed:(NSString*)errMsg
{
    [WXCommonViewClass showHudInView:self.view title:errMsg];
}

//取消绑定回调
-(void)syncCancelWeiBoSuccess:(NSString*)EvaString
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    [singletonClass.wbapi cancelAuth];
    [WXCommonViewClass showHudInView:self.view title:EvaString];
}
-(void)syncCancelWeiBoFailed:(NSString*)errCode
{
    [WXCommonViewClass showHudInView:self.view title:errCode];
    self.switch_QQ.on = YES;
}

#pragma mark QQ登录回调
- (void)tencentDidLogin
{
	// 登录成功
    if([_tencentOAuth getUserInfo]){
        [WXCommonViewClass showHudInView:self.view title:@"授权成功"];
        
    }else
    {
        [WXCommonViewClass showHudInView:self.view title:@"获取用户信息失败"];
    }
}

- (void)tencentDidNotLogin:(BOOL)cancelled
{
	if (cancelled){
        [WXCommonViewClass showHudInView:self.view title:@"用户取消登录"];
	}
	else {
        [WXCommonViewClass showHudInView:self.view title:@"登录失败"];
	}
    
}
-(void)tencentDidNotNetWork
{
    [WXCommonViewClass showHudInView:self.view title:@"无网络连接"];
}

- (void)getUserInfoResponse:(APIResponse*) response {
    if (response.retCode == URLREQUEST_SUCCEED)
	{
        NSLog(@"%@", response.jsonResponse);
        [[FY_Cloud_userBindQQ share] setDelegate:(id)self];
        [[FY_Cloud_userBindQQ share] BindQQWithOpenID:_tencentOAuth.openId andNickName:[response.jsonResponse objectForKey:@"nickname"]];
    }else
    {
		UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:@"操作失败" delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"确定") otherButtonTitles:nil];
        [alert show];
	}
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100) {
        if (buttonIndex == 1) {
            [[FY_Cloud_cancelWeiBo share] setDelegate:(id)self];
            [[FY_Cloud_cancelWeiBo share] cancelWeiBoWithType:1];
        }
    }else if (alertView.tag == 101) {
        if (buttonIndex == 1) {
            [[FY_Cloud_cancelBindQQ share] setDelegate:(id)self];
            [[FY_Cloud_cancelBindQQ share] cancelBindQQ];
        }
    }
}

//绑定QQ回调
-(void)syncBindQQSuccess:(NSString*)EvaString
{
    [WXCommonViewClass showHudInView:self.view title:@"绑定成功"];
}
-(void)syncBindQQFailed:(NSString*)errCode
{
    [WXCommonViewClass showHudInView:self.view title:@"绑定失败"];
}

//取消QQ绑定回调
-(void)syncCancelQQSuccess:(NSString*)EvaString
{
    [WXCommonViewClass showHudInView:self.view title:@"取消绑定成功"];
}
-(void)syncCancelQQFailed:(NSString*)errCode
{
    [WXCommonViewClass showHudInView:self.view title:@"取消绑定失败"];
}

- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [WXCommonViewClass showHudInView:self.view title:errorCode];
}

@end
