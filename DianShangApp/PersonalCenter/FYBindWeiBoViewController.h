//
//  FYBindWeiBoViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-7-11.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeiboApi.h"
#import <TencentOpenAPI/TencentOAuth.h>
#import "WX_Cloud_Delegate.h"
@interface FYBindWeiBoViewController : UIViewController <WeiboAuthDelegate,WeiboRequestDelegate, TencentSessionDelegate, UIAlertViewDelegate,WX_Cloud_Delegate>
{
    TencentOAuth* _tencentOAuth;
    NSArray* _permissions;
}
@property (weak, nonatomic) IBOutlet UISwitch *switch_QQ;
@property (weak, nonatomic) IBOutlet UISwitch *switch_QQLogin;

- (IBAction)switch_QQ:(id)sender;
- (IBAction)switch_QQLogin:(id)sender;


@end
