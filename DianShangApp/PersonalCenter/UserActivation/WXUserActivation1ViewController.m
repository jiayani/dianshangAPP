//
//  WXUserActivation1ViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXUserActivation1ViewController.h"
#import "WXCommonViewClass.h"
#import "WX_Cloud_IsConnection.h"
#import "UserInfo.h"
#import "WXUserDatabaseClass.h"
#import "WXVerifyMethodsClass.h"
#import "WXUserActivation2ViewController.h"
#import "WX_Cloud_BindPhoneFist.h"

@interface WXUserActivation1ViewController()

@end

@implementation WXUserActivation1ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (self.isFromBindPhone) {
        self.title = NSLocalizedString(@"绑定手机", @"绑定手机");
    }else{
        self.title = NSLocalizedString(@"会员激活", @"会员激活1页面标题");
    }

    [self initAllControllers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[WX_Cloud_BindPhoneFist share] setDelegate:nil];
     mbProgressHUD.delegate = nil;
}
- (void)viewDidUnload {
    [self setFirstLabel:nil];
    [self setSecondLabel:nil];
    [self setThirdLabel:nil];
    [self setPhoneText:nil];
    [self setGetVerifyCodeBtn:nil];
    [self setDirectionsTextView:nil];
    [self setUserPhoneLabel:nil];
    [self setZheZhaoView:nil];
    self.errorStr = nil;
    [super viewDidUnload];
}
#pragma mark - UITextFieldDelegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)textFieldChanged:(id)sender {
    if (self.phoneText.text.length <= 0 ) {
        self.errorStr = NSLocalizedString(@"手机号不能为空", @"会员激活1/手机号不能为空");
        
    }
}
#pragma mark - All My Methods
/*初始化所有页面显示内容*/
- (void)initAllControllers{
    /*设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
 
    [self.getVerifyCodeBtn setTitle:NSLocalizedString(@"获取验证码", @"找回密码1/获取验证码") forState:UIControlStateNormal];
    
    
    self.firstLabel.text = NSLocalizedString(@"填写手机号", @"会员激活1/填写注册手机号");
    self.secondLabel.text = NSLocalizedString(@"验证身份", @"会员激活1/验证身份");
    if (self.isFromBindPhone){
        self.thirdLabel.text = NSLocalizedString(@"完成绑定", @"会员激活1/完成绑定");
    }else{
        self.thirdLabel.text = NSLocalizedString(@"激活成功", @"会员激活1/激活成功");
    }
    
    self.phoneText.placeholder = NSLocalizedString(@"请输入您的手机号", @"会员激活1/请输入您的手机号");
    self.directionsTextView.text = NSLocalizedString(@"说明：\n1、绑定您常用的手机号。\n2、绑定手机号可以享受积分返点、商家优惠等服务。", @"会员激活1/会员激活的说明");
    self.directionsTextView.editable = NO;
    self.phoneText.keyboardType = UIKeyboardTypeNumberPad;
    self.currentUser = [WXUserDatabaseClass getCurrentUser];
    if (self.currentUser != nil && self.isFromBindPhone) {
        self.userPhoneLabel.hidden = NO;
        if (self.currentUser.phoneNumber != nil && self.currentUser.phoneNumber.length > 0) {
            self.userPhoneLabel.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"您绑定的手机号", @"会员激活1/您绑定的手机号"),self.currentUser.phoneNumber];
        }else{
            self.userPhoneLabel.text = NSLocalizedString(@"您尚未绑定手机号", @"会员激活1/您尚未绑定手机号");
        }
    }else{
        self.userPhoneLabel.hidden = YES;
    }
    
    
    
}
//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    if ([touch view] == self.view || [touch view] == self.zheZhaoView) {
        [self.phoneText resignFirstResponder];
    }
}
- (IBAction)getVerifyBtnPressed:(id)sender {
    if ([self.phoneText isFirstResponder]) {
        [self.phoneText resignFirstResponder];
    }
    NSString *phoneStr = [self.phoneText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    BOOL isValidatePhone = [WXVerifyMethodsClass isValidatePhone:phoneStr];
    if (phoneStr.length <= 0) {
        self.errorStr = NSLocalizedString(@"手机号不能为空", @"会员激活1/手机号不能为空");
       mbProgressHUD = [WXCommonViewClass showHudInView:self.view title:self.errorStr];
        mbProgressHUD.delegate = self;
        
    }else if (!isValidatePhone) {
        self.errorStr = NSLocalizedString(@"请输入正确的手机号", @"会员激活1/请输入正确的手机号");
        mbProgressHUD = [WXCommonViewClass showHudInView:self.view title:self.errorStr];
        mbProgressHUD.delegate = self;
        
        
    }else{
        
        //本地判断是否连接网络
        BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
        if (!isConnection) {//无网络情况
            NSString *errCode = NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用");
            [WXCommonViewClass showHudInView:self.view title:errCode];
        }else{
            //小圈转动等待验证
            [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"等待验证...", @"会员激活1/等待验证...") nav:self.navigationController];
            //调用后台方法进行服务器断验证
            [[WX_Cloud_BindPhoneFist share] setDelegate:(id)self];
            //0 邮箱  1:手机号
            [[WX_Cloud_BindPhoneFist share] verifyPhone:phoneStr];
        }
        
    }
    
    
}
#pragma mark － MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[self.phoneText becomeFirstResponder];
}

#pragma mark - WYDCloudDelegate Methods
//无网络连接/意外错误
- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{

    [[WX_Cloud_BindPhoneFist share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
    
}
//验证码验证
- (void)verifyPhoneOrEmailSuccess:(NSString*)keyWordStr{
    [[WX_Cloud_BindPhoneFist share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    WXUserActivation2ViewController *activation2VC = [[WXUserActivation2ViewController alloc]initWithNibName:@"WXUserActivation2ViewController" bundle:nil];
    activation2VC.title = self.title;
    activation2VC.phoneStr = self.phoneText.text;
    activation2VC.willPushViewController = self.willPushViewController;
    activation2VC.isFromBindPhone = self.isFromBindPhone;
    [self.navigationController pushViewController:activation2VC animated:YES];
}
- (void)verifyPhoneOrEmailFailed:(NSString*)keyWordStr errCode:(NSString*)errCode{
    [[WX_Cloud_BindPhoneFist share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInView:self.view title:errCode];
}

@end
