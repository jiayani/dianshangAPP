//
//  WXUserActivation3ViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UserInfo;

@interface WXUserActivation3ViewController : UIViewController{
    UserInfo *currentUser;
}
@property (strong,nonatomic) NSString *phoneStr;
@property (readwrite)  BOOL isFromBindPhone;
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdLabel;
@property (weak, nonatomic) IBOutlet UITextView *directionTextView;
@property (weak, nonatomic) IBOutlet UIButton *okBtn;

@property (weak, nonatomic) UIViewController *willPushViewController;

- (IBAction)okBtnPressed:(id)sender;

@end
