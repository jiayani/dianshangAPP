//
//  WXUserActivation3ViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXUserActivation3ViewController.h"
#import "WXCommonViewClass.h"
#import "UserInfo.h"
#import "WXUserDatabaseClass.h"
#import "WXPersonalCenterViewController.h"
#import "WXUserLoginViewController.h"


@interface WXUserActivation3ViewController ()

@end

@implementation WXUserActivation3ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initAllControllers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload {
    [self setFirstLabel:nil];
    [self setSecondLabel:nil];
    [self setThirdLabel:nil];
    [self setDirectionTextView:nil];
    [self setOkBtn:nil];
    [super viewDidUnload];
}
#pragma mark - UITextFieldDelegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - All My Methods
/*初始化所有页面显示内容*/
- (void)initAllControllers{
    /*设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    [self.okBtn setTitle:NSLocalizedString(@"完成", @"会员激活3/完成") forState:UIControlStateNormal];
    
    self.firstLabel.text = NSLocalizedString(@"填写手机号", @"会员激活3/填写注册手机号");
    self.secondLabel.text = NSLocalizedString(@"验证身份", @"会员激活3/验证身份");
    NSString *str;
    if (self.isFromBindPhone){
        self.thirdLabel.text = NSLocalizedString(@"完成绑定", @"会员激活1/完成绑定");
        str = [NSString stringWithFormat:@"您已成功绑定%@\n祝您使用愉快",self.phoneStr];
        
    }else{
        self.thirdLabel.text = NSLocalizedString(@"激活成功", @"会员激活1/激活成功");
        str = [NSString stringWithFormat:@"您的账号已成功激活！绑定的手机号为%@\n祝您使用愉快",self.phoneStr];
    }
    self.directionTextView.text = str;
    self.directionTextView.editable = NO;
    
}

//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)okBtnPressed:(id)sender {
    NSArray *tempArray = self.navigationController.viewControllers;
    int index = tempArray.count;
    if (self.isFromBindPhone && index > 4) {
        index = index - 4;
        UIViewController *tempVC = [tempArray objectAtIndex:index];
        [self.navigationController popToViewController:tempVC animated:YES];
    }else{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    

}
@end
