//
//  WXUserActivation2ViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXUserActivation2ViewController.h"
#import "WXCommonViewClass.h"
#import "WX_Cloud_IsConnection.h"
#import "UserInfo.h"
#import "WXUserDatabaseClass.h"
#import "WXUserActivation3ViewController.h"
#import "WX_Cloud_BindPhoneSecond.h"
#import "WX_Cloud_BindPhoneFist.h"

@interface WXUserActivation2ViewController ()

@end

@implementation WXUserActivation2ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initAllControllers];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!iPhone5) {
        //键盘的监听事件，获取高度
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [newTimer invalidate];
    [[WX_Cloud_BindPhoneSecond share] setDelegate:nil];
    [[WX_Cloud_BindPhoneFist share] setDelegate:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    mbProgressHUD.delegate = nil;
    mbProgressHUD = nil;
}
- (void)viewDidUnload {
    [self setFirstLabel:nil];
    [self setSecondLabel:nil];
    [self setThirdLabel:nil];
    [self setPhoneLabel:nil];
    [self setVerifyVodeText:nil];
    [self setSubmitVerifyCodeBtn:nil];
    [self setResendVerifyCodeBtn:nil];
    [self setMyAnimationView:nil];
    self.errorStr = nil;
    [super viewDidUnload];
}
#pragma mark - UITextFieldDelegate Methods
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (!iPhone5) {
        [self ViewMoveToZero];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)textFieldChanged:(id)sender {
    if (self.verifyVodeText.text.length <= 0 ) {

        self.errorStr = NSLocalizedString(@"验证码不能为空", @"会员激活2/验证码不能为空");
        
    }
}

#pragma mark - All My Methods
/*初始化所有页面显示内容*/
- (void)initAllControllers{
    /*设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    [self.submitVerifyCodeBtn setTitle:NSLocalizedString(@"提交验证码", @"找回密码1/提交验证码") forState:UIControlStateNormal];
    
    self.myAnimationView.hidden = YES;
    self.firstLabel.text = NSLocalizedString(@"填写手机号", @"会员激活2/填写注册手机号");
    self.secondLabel.text = NSLocalizedString(@"验证身份", @"会员激活2/验证身份");
    if (self.isFromBindPhone){
        self.thirdLabel.text = NSLocalizedString(@"完成绑定", @"会员激活1/完成绑定");
    }else{
        self.thirdLabel.text = NSLocalizedString(@"激活成功", @"会员激活1/激活成功");
    }
    NSString *tempStr = NSLocalizedString(@"您的手机号是", @"会员激活2/您的手机号是");
    NSString *str = [NSString stringWithFormat:@"%@:%@",tempStr,self.phoneStr];
    self.phoneLabel.text = str;
    
    self.verifyVodeText.placeholder = NSLocalizedString(@"请输入验证码", @"会员激活2/请输入验证码");
    self.resendVerifyCodeBtn.alpha = 0.8f;
    self.timerLabel.alpha = 0.8f;
    self.resendVerifyCodeBtn.enabled = NO;
    countdownInt = 60;
    sendCount = 0;
    startDate = [NSDate date];
    NSString *tempStr2 = [NSString stringWithFormat:@"%d\"",countdownInt];
    self.timerLabel.text = tempStr2;
    
    //每秒钟刷新一次倒计时显示
    newTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(refreshCountdown) userInfo:nil repeats:YES];
    
    if (iOS7) {
        pointY = 64;
    }else{
        pointY = 0;
    }
    
    
}
//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    if ([touch view] == self.view) {
        [self.verifyVodeText resignFirstResponder];
    }
}

//键盘事件
- (void)keyboardWillShow:(NSNotification*)notification{
    CGRect keyboardRect = [[[notification userInfo] objectForKey:_UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat txtHeight = self.verifyVodeText.frame.origin.y + self.verifyVodeText.frame.size.height;
    CGFloat keyY = self.view.frame.size.height - keyboardRect.size.height;
    if (keyY < txtHeight) {
        isMove = YES;
        moveFloat = txtHeight - keyY;
    }else{
        isMove = NO;
        moveFloat = 0;
    }
    if (isMove) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.2];
        
        self.view.frame = CGRectMake(0, -(moveFloat + 20), self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }else{
        if (!iPhone5) {
            [self ViewMoveToZero];
        }
    }
    
}
/*恢复view刚开始的位置*/
- (void)ViewMoveToZero{
    if (self.view.frame.origin.y != pointY) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.2];
        self.view.frame = CGRectMake(0, pointY, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }
}
- (void)refreshCountdown{
    countdownInt--;
    NSString *resendStr = NSLocalizedString(@"重新获取", @"找回密码2/重新获取");
    NSString *tempStr;
    
    tempStr = [NSString stringWithFormat:@"%d\"",countdownInt];
    
    if (countdownInt <= 0) {
        self.resendVerifyCodeBtn.enabled = YES;
        self.resendVerifyCodeBtn.alpha = 1.0f;
        self.timerLabel.alpha = 1.0f;
        self.timerLabel.text = resendStr;
    }else{
        self.resendVerifyCodeBtn.enabled = NO;
        self.resendVerifyCodeBtn.alpha = 0.8f;
        self.timerLabel.alpha = 0.8f;
        self.timerLabel.text = tempStr;
        
    }
    
}

- (IBAction)submitVerityCodeBtnPressed:(id)sender {
    [self.verifyVodeText resignFirstResponder];
    NSString *verifyCodeStr = [self.verifyVodeText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //输入的验证码是否为空
    if (verifyCodeStr.length <= 0) {

        self.errorStr = NSLocalizedString(@"验证码不能为空", @"会员激活2/验证码不能为空");
        mbProgressHUD = [WXCommonViewClass showHudInView:self.verifyVodeText title:self.errorStr];
        mbProgressHUD.delegate = self;
        return;
    }else{
        //把验证码发给后台服务器端进行验证
        sendCount++;
        //验证成功后调用的代码
        //本地判断是否连接网络
        BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
        if (!isConnection) {//无网络情况
            NSString *errCode = NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用");
            [WXCommonViewClass showHudInView:self.view title:errCode];
        }else{
            //小圈转动等待验证
            self.myAnimationView.hidden = NO;
            [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"等待验证...", @"会员激活2/等待验证...") View:self.myAnimationView];
            //调用后台方法进行服务器断验证
            
            [[WX_Cloud_BindPhoneSecond share] setDelegate:(id)self];
            
            [[WX_Cloud_BindPhoneSecond share] sendVerifyCode:verifyCodeStr flag:sendCount];
        }
        
   }
}

- (IBAction)resendVerifyCodeBtnPressed:(id)sender {
    [self.verifyVodeText resignFirstResponder];
    //begin:每秒钟刷新一次倒计时显示
    self.resendVerifyCodeBtn.alpha = 0.8f;
    self.timerLabel.alpha = 0.8f;
    self.resendVerifyCodeBtn.enabled = NO;
    countdownInt = 60;
    NSString *tempStr2 = [NSString stringWithFormat:@"%d\"",countdownInt];
    self.timerLabel.text = tempStr2;
    startDate = [NSDate date];
    //end
    
    //调用后台方法进行服务器断验证
    [[WX_Cloud_BindPhoneFist share] setDelegate:(id)self];
    [[WX_Cloud_BindPhoneFist share] verifyPhone:self.phoneStr];
}

#pragma mark － MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[self.verifyVodeText becomeFirstResponder];
}

#pragma mark - WYDCloudDelegate Methods
//无网络连接/意外错误
- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[WX_Cloud_BindPhoneFist share] setDelegate:nil];
    [[WX_Cloud_BindPhoneSecond share] setDelegate:nil];
    //小圈停止运动
    self.myAnimationView.hidden = YES;
    [WXCommonViewClass setActivityWX:NO content:nil View:self.myAnimationView];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}
//验证码验证
- (void)verifyCodeSuccess:(NSString*)keywordsID{
    [[WX_Cloud_BindPhoneSecond share] setDelegate:nil];
    
    //小圈停止运动
    self.myAnimationView.hidden = YES;
    [WXCommonViewClass setActivityWX:NO content:nil View:self.myAnimationView];
    UserInfo *currentUser = [WXUserDatabaseClass getCurrentUser];
    [WXUserDatabaseClass updatePhoneOfUser:currentUser phoneStr:self.phoneStr];
    WXUserActivation3ViewController *actiovation3VC = [[WXUserActivation3ViewController alloc]initWithNibName:@"WXUserActivation3ViewController" bundle:nil];
    actiovation3VC.phoneStr = self.phoneStr;
    actiovation3VC.title = self.title;
    actiovation3VC.isFromBindPhone = self.isFromBindPhone;
    actiovation3VC.willPushViewController = self.willPushViewController;
    [self.navigationController pushViewController:actiovation3VC animated:YES];
    
}
- (void)verifyCodeFaild:(NSString*)keywordsID andErrorMessage:(NSString*)errCode{
    [[WX_Cloud_BindPhoneSecond share] setDelegate:nil];
    
    //小圈停止运动
    self.myAnimationView.hidden = YES;
    [WXCommonViewClass setActivityWX:NO content:nil View:self.myAnimationView];
    [WXCommonViewClass showHudInView:self.view title:errCode];
}
#pragma mark - WX_Cloud_Delegate 获取验证码方法
//验证码验证
- (void)verifyPhoneOrEmailSuccess:(NSString*)keyWordStr{
    [[WX_Cloud_BindPhoneFist share] setDelegate:nil];

    
}
- (void)verifyPhoneOrEmailFailed:(NSString*)keyWordStr errCode:(NSString*)errCode
{
    [[WX_Cloud_BindPhoneFist share] setDelegate:nil];

}

@end
