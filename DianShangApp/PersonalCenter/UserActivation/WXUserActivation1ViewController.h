//
//  WXUserActivation1ViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"
#import "MBProgressHUD.h"
@class UserInfo;

@interface WXUserActivation1ViewController : UIViewController<UITextFieldDelegate,WX_Cloud_Delegate,MBProgressHUDDelegate>{
    MBProgressHUD *mbProgressHUD;
}
@property (strong, nonatomic)  UserInfo *currentUser;
@property (readwrite)  BOOL isFromBindPhone;
@property (weak, nonatomic) NSString *errorStr;
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdLabel;
@property (weak, nonatomic) IBOutlet UITextField *phoneText;
@property (weak, nonatomic) IBOutlet UIButton *getVerifyCodeBtn;
@property (weak, nonatomic) IBOutlet UILabel *userPhoneLabel;
@property (strong, nonatomic) IBOutlet UIView *zheZhaoView;
@property (weak, nonatomic) IBOutlet UITextView *directionsTextView;
@property (weak, nonatomic) UIViewController *willPushViewController;
- (IBAction)getVerifyBtnPressed:(id)sender;
- (IBAction)textFieldChanged:(id)sender;

@end
