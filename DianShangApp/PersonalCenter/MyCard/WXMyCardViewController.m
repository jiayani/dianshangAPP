//
//  WXMyCardViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-18.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXMyCardViewController.h"
#import "WXMemberCardTypeViewController.h"
#import "WX_Cloud_MyCard.h"
#import "WX_Cloud_CardUp.h"
#import "WXCommonViewClass.h"
#import "AsynImageView.h"
#import "WXUserDatabaseClass.h"
#import "WX_Cloud_IsConnection.h"
#import "UserInfo.h"
#import "MBProgressHUD.h"

@interface WXMyCardViewController ()

@end

@implementation WXMyCardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"会员卡", @"我的会员卡页面标题");
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initAllControllers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getLatestDatasFromServer];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[WX_Cloud_MyCard share] setDelegate:nil];
    [[WX_Cloud_CardUp share] setDelegate:nil];
    mbProgressHUDView.delegate = nil;
    mbProgressHUDView = nil;
}

#pragma mark - All My Methods
- (void)initAllControllers{
    /*begin:设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    //end
    
    //begin:设置导航右侧按钮
    self.shengJiBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil title:NSLocalizedString(@"申请升级", @"我的会员卡/申请升级")];
    [self.shengJiBtn addTarget:self action:@selector(shengJiBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.shengJiBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    //end
    
    self.currentUser = [WXUserDatabaseClass getCurrentUser];
    self.infoLabel1.text = NSLocalizedString(@"普通", @"我的会员卡/普通");
    self.accumulatedIntegralInfoLabel.text = NSLocalizedString(@"累计积分:", @"我的会员卡/累计积分");
    self.quanYiLabel.text = NSLocalizedString(@"权益:", @"我的会员卡/权益:");
    self.kaJiBieLabelInfo.text = NSLocalizedString(@"卡级别:", @"我的会员卡/卡级别:");
    self.cardIntegralInfoLabel.text = NSLocalizedString(@"当前积分:", @"我的会员卡/当前积分");
    self.cardNumberInfoLabel.text = NSLocalizedString(@"卡号:", @"我的会员卡/卡号");
    self.oldCardNumberInfoLabel.text = NSLocalizedString(@"实体卡卡号:", @"我的会员卡/实体卡卡号");
    [self.cardTypeBtn setTitle:NSLocalizedString(@"会员卡种类介绍", @"我的会员卡/会员卡种类介绍") forState:UIControlStateNormal];
    
    [self.huiYuanQuanYiBtn setTitle:NSLocalizedString(@"会员卡信息", @"我的会员卡/会员卡信息") forState:UIControlStateNormal];
    
    
    self.huiYuanQuanYiView.hidden = YES;
    self.cardTypeView.hidden = NO;
    self.myScrollView.scrollEnabled = NO;
    if (!iPhone5) {
        //使用scrollView很关键的地方，设置contentSize，一般是安你的scrollView的size来设置，后面的height可以适当多个300～400
        self.myScrollView.contentSize = CGSizeMake(320, self.myScrollView.frame.size.height + 100.0f);
    }else{
        self.myScrollView.contentSize = CGSizeMake(320, self.myScrollView.frame.size.height + 30.0f);
    }
    
}
//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)huiYuanQuanYiBtnPressed:(id)sender {
    NSString *imageNameStr ;
    CGRect cardTypeFrame = self.cardTypeView.frame;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    
    if (self.huiYuanQuanYiView.hidden == YES) {
        self.myScrollView.scrollEnabled = YES;
        self.huiYuanQuanYiView.hidden = NO;
        imageNameStr = @"quanYiUpArrows.png";
        cardTypeFrame.origin.y = self.huiYuanQuanYiView.frame.size.height + self.huiYuanQuanYiView.frame.origin.y + 10;
        self.cardTypeView.frame = cardTypeFrame;
        
    }else{
        self.myScrollView.scrollEnabled = NO;
        self.myScrollView.contentOffset = CGPointMake(0, 0);
        self.huiYuanQuanYiView.hidden = YES;
        imageNameStr = @"yuanYiDownArrows.png";
        
        cardTypeFrame.origin.y = self.myView.frame.size.height + self.myView.frame.origin.y + 10;
        self.cardTypeView.frame = cardTypeFrame;
        
    }
    self.upOrDownArrorImageView.image = [UIImage imageNamed:imageNameStr];
    [UIView commitAnimations];
    
}

- (IBAction)cardTypeBtnPressed:(id)sender {
    WXMemberCardTypeViewController *memberCardTypeVC = [[WXMemberCardTypeViewController alloc]initWithNibName:@"WXMemberCardTypeViewController" bundle:nil];
    [self.navigationController pushViewController:memberCardTypeVC animated:YES];
}

- (void)reloadUI{
    if (myDataDic != nil) {
        if ([[myDataDic objectForKey:@"topLevel"]intValue] == 1) {
            self.shengJiBtn.hidden = YES;
        }else{
            self.shengJiBtn.hidden = NO;
        }
        self.kaJiBieLabelValue.text = [myDataDic objectForKey:@"cardName"];
        self.infoLabel1.text = [myDataDic objectForKey:@"cardInfo"];
        self.accumulatedIntegralLabel.text = [myDataDic objectForKey:@"accumulatedIntegral"];
        self.cardIntegralLabel.text = [myDataDic objectForKey:@"cardIntegral"];
        self.cardNumberLabel.text = [myDataDic objectForKey:@"cardNumber"];//电子卡
        NSString *oldCardNumberStr = [myDataDic objectForKey:@"oldcardNumber"];

        if (oldCardNumberStr == nil || [oldCardNumberStr isEqualToString:@"0"] || oldCardNumberStr.length <= 0 || [oldCardNumberStr isEqualToString:@""]) {
            self.oldCardNumberLabel.text = NSLocalizedString(@"请到店绑定实体卡", @"我的会员卡/请到店绑定实体卡");
            
        }else{
            self.oldCardNumberLabel.text = oldCardNumberStr;//实体卡
        }
        //begin:会员卡图片
        NSDictionary *picDic = [myDataDic objectForKey:@"cardPic"];
        self.cardPicImageView.type = 1;
        self.cardPicImageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
        self.cardPicImageView.imageURL = [picDic objectForKey:@"img"];
        //end
        
    }
}
- (IBAction)shengJiBtnPressed:(id)sender {
    
    [[WX_Cloud_CardUp share] setDelegate:(id)self];
    [[WX_Cloud_CardUp share] syncCardUp:self.currentUser.userId];
}
- (void)getLatestDatasFromServer{
     [WXCommonViewClass setActivityWX:YES content:WaitNetConnect View:self.view];
    //从服务器端取得最新数据
    [[WX_Cloud_MyCard share] setDelegate:(id)self];
    [[WX_Cloud_MyCard share] syncGetMyCard:self.currentUser.userId];
}
#pragma mark － MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[self reloadUI];
}
#pragma mark - WX_Cloud_Delegate Methods
//意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[WX_Cloud_MyCard share] setDelegate:nil];
    [[WX_Cloud_CardUp share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil View:self.view];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}
#pragma  获得会员卡信息
-(void)syncGetMyCardSuccess:(NSDictionary *)dataDic{
    [[WX_Cloud_MyCard share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil View:self.view];
    myDataDic = dataDic;
    [self reloadUI];
    
}

-(void)syncGetMyCardFailed:(NSString *)errCode{
    [[WX_Cloud_MyCard share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil View:self.view];
    [WXCommonViewClass showHudInView:self.view title:errCode];
    
}
#pragma  获得会员卡升级
-(void)syncGetCardUpSuccess:(NSDictionary *)dataDic{
    [[WX_Cloud_MyCard share]setDelegate:nil];
    myDataDic = dataDic;
    NSDictionary *tempDic = [[NSDictionary alloc]initWithObjectsAndKeys:[dataDic objectForKey:@"cardPic"],@"img", nil];
    [myDataDic setValue:tempDic forKey:@"cardPic"];
    [self reloadUI];
    int type = [[dataDic objectForKey:@"code"] intValue];
    NSString *alertStr;
    if (type == 101) {//101后台是手动升级
        alertStr = NSLocalizedString(@"您的申请已提交，请等待！", @"我的会员卡/您的申请已提交，请等待！");
    }else{//后台是自动升级
        alertStr = NSLocalizedString(@"恭喜您，会员卡升级成功！", @"我的会员卡/恭喜您，会员卡升级成功！");
        
    }
    mbProgressHUDView = [WXCommonViewClass showHudInView:self.view title:alertStr];
    mbProgressHUDView.delegate = self;

}

-(void)syncGetCardUpFailed:(NSString *)errCode{
    [[WX_Cloud_CardUp share]setDelegate:nil];
    [WXCommonViewClass showHudInView:self.view title:errCode];
}
@end
