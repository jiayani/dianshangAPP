//
//  WXMemberCardTypeViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-18.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXMemberCardTypeViewController.h"
#import "WXCommonViewClass.h"
#import "AsynImageView.h"
#import "WX_Cloud_CardType.h"
#import "WXMemberCardTypeTableViewCell.h"
#import "MembershipCard.h"
#import "AppDelegate.h"


@interface WXMemberCardTypeViewController ()

@end

@implementation WXMemberCardTypeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"会员卡类别", @"会员卡类别页面标题");
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initAllController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self syncGetAllData];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[WX_Cloud_CardType share] setDelegate:nil];
}
#pragma mark - All My Methods
- (void)initAllController{
    /*begin:设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    //end
    quanYiArray = [[NSMutableArray alloc]init];
    isShowArray = [[NSMutableArray alloc]init];
}
//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)syncGetAllData{
    [WXCommonViewClass setActivityWX:YES content:WaitNetConnect View:self.view];
    [[WX_Cloud_CardType share] setDelegate:(id)self];
    [[WX_Cloud_CardType share] syncGetCardType];
}
- (void)reloadUI:(BOOL)isSuccess{
    if (isSuccess) {
        //把数据保存到数据库中
        [self insertObjects:myDataArray];
        
    }
    myDataArray = [self fetchRequestData];
    for (MembershipCard *membershipCard in myDataArray) {
    
        NSString *str = membershipCard.content;
        CGFloat height = [self getHeightOfLabel:str fontSize:14.0f labelWidth:300];
        

        [quanYiArray addObject:[NSNumber numberWithFloat:height]];
        [isShowArray addObject:[NSNumber numberWithBool:NO]];
    }
    [self.myTableView reloadData];
    
}
//返回字符串的高度
- (CGFloat)getHeightOfLabel:(NSString *)str fontSize:(CGFloat)fontSize labelWidth:(CGFloat)labelWidth {
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    //设置一个行高上限
    CGSize size = CGSizeMake(labelWidth,2000);
    //计算实际frame大小，并将label的frame变成实际大小
    CGSize labelsize = [str sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
    return labelsize.height;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return myDataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    BOOL isShow = [[isShowArray objectAtIndex:section]boolValue];
    if (isShow) {
        return 1;
    }else{
        return 0;
    }

    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d%d", [indexPath section], [indexPath row]];//以indexPath来唯一确定cell
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault
                                     reuseIdentifier:CellIdentifier];
    }

    MembershipCard *membershipCard;
    membershipCard = [myDataArray objectAtIndex:indexPath.section];
    float height = [[quanYiArray objectAtIndex:indexPath.section]floatValue];
    UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(10, 0, 300, height)];
    [textView setScrollEnabled:NO];
    textView.font = [UIFont systemFontOfSize:14.0f];
    textView.text = membershipCard.content;
    [cell addSubview:textView];
   
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d", section];//以indexPath来唯一确定cell
    WXMemberCardTypeTableViewCell *cell;
    
    if (cell == nil) {
        cell = (WXMemberCardTypeTableViewCell *)[[WXMemberCardTypeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                                                     reuseIdentifier: CellIdentifier];
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"WXMemberCardTypeTableViewCell" owner:self options:nil];
        cell = (WXMemberCardTypeTableViewCell *)[nib objectAtIndex:0];
    }
    
    MembershipCard *membershipCard;
    membershipCard = [myDataArray objectAtIndex:section];
    cell.titleLabel.text = membershipCard.title;
    cell.typePicImageView.type = 1;
    cell.typePicImageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    cell.typePicImageView.imageURL = membershipCard.pic_url;
    //设置选中时没有背景色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.quanYiBtn.tag = section;
    [cell.quanYiBtn addTarget:self action:@selector(quanYiBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    //把所有的保存到数组中
    [headerViewArray addObject:cell];

    
    return cell;
}
- (void)quanYiBtnPressed:(UIButton *)btn
{
    
    int section = btn.tag;
    BOOL isShow = [[isShowArray objectAtIndex:section]boolValue];
    [isShowArray replaceObjectAtIndex:section withObject:[NSNumber numberWithBool:!isShow]];
    [self.myTableView reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
    
    
}
#pragma mark - Table view delegate

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSNumber *heightNum = [quanYiArray objectAtIndex:indexPath.section];
    return  [heightNum floatValue] + 20 ;
}

- (float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 252.0f;
}
#pragma mark - Database Methods
- (BOOL)insertObjects:(NSArray *)dateArray{
    
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext * context = app.managedObjectContext;
    
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"MembershipCard" inManagedObjectContext:context];
    NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSArray *tempArr = [context executeFetchRequest:fetch error:nil];
    //删除数据库中所有的数据
    for (MembershipCard *tempObject in tempArr) {
        [context deleteObject:tempObject];
    }
    //插入数据
    for (NSDictionary *dataDic in dateArray) {
        MembershipCard *membershipCard = [NSEntityDescription insertNewObjectForEntityForName:@"MembershipCard" inManagedObjectContext:context];
        membershipCard.cardLevel = [dataDic objectForKey:@"level"];
        membershipCard.title = [dataDic objectForKey:@"title"];;
        membershipCard.content = [dataDic objectForKey:@"content"];
        NSArray *cardPicArr = [dataDic objectForKey:@"image"];
        NSDictionary * cardPicDic = [cardPicArr objectAtIndex:0];
        membershipCard.pic_url = [cardPicDic objectForKey:@"img"];
        [context insertObject:membershipCard];
       
    }
    NSError *error;
    if ([context save:&error]) {
        return YES;
    }else{
        return NO;
    }
}
- (NSArray *)fetchRequestData{
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext * context = app.managedObjectContext;
    
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"MembershipCard" inManagedObjectContext:context];
    NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"cardLevel"ascending:YES];
    NSArray *sortDescriptions = [[NSArray alloc]initWithObjects:sortDescriptor, nil];
    [fetch setSortDescriptors:sortDescriptions];
    [fetch setEntity:entity];
    NSArray * tempArr = [context executeFetchRequest:fetch error:nil];
    //删除数据库中所有的数据
    if (tempArr != nil && tempArr.count > 0) {
        return tempArr;
    }else{
        return nil;
    }
    
}
#pragma mark - WX_Cloud_Delegate Methods
//意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[WX_Cloud_CardType share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil View:self.view];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
    [self reloadUI:NO];
}

//获得会员卡类别
-(void)syncGetCardTypeSuccess:(NSArray *)dataArray{
    
    [[WX_Cloud_CardType share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil View:self.view];
    myDataArray = dataArray;
    [self reloadUI:YES];
}
-(void)syncGetCardTypeFailed:(NSString *)errCode{
    [[WX_Cloud_CardType share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil View:self.view];
    [self reloadUI:NO];
}
@end
