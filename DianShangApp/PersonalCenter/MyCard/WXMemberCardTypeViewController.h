//
//  WXMemberCardTypeViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-6-18.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WXMemberCardTypeTableViewCell;

@interface WXMemberCardTypeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSArray *myDataArray;
    NSMutableArray *quanYiArray;
    NSMutableArray *isShowArray;
    int mySection;
    NSMutableArray *headerViewArray;
}
@property (weak, nonatomic) IBOutlet UITableView *myTableView;

@end
