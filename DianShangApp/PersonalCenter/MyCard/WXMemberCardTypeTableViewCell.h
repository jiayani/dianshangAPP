//
//  WXMemberCardTypeTableViewCell.h
//  DianShangApp
//
//  Created by 霞 王 on 14-6-19.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AsynImageView;

@interface WXMemberCardTypeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet AsynImageView *typePicImageView;
@property (strong, nonatomic) IBOutlet UIButton *quanYiBtn;
@property (strong, nonatomic) IBOutlet UIView *quanYiTitleView;

@end
