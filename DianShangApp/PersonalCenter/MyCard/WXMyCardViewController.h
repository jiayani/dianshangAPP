//
//  WXMyCardViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-6-18.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "WX_Cloud_Delegate.h"
@class AsynImageView;
@class UserInfo;

@interface WXMyCardViewController : UIViewController<MBProgressHUDDelegate,WX_Cloud_Delegate>{
    NSDictionary *myDataDic;
    MBProgressHUD *mbProgressHUDView;
}

@property (strong, nonatomic) UserInfo *currentUser;

@property (strong, nonatomic) UIImageView *defaultImageView;
@property (strong, nonatomic) IBOutlet AsynImageView *cardPicImageView;
@property (strong, nonatomic) IBOutlet UILabel *infoLabel1;
@property (strong, nonatomic) IBOutlet UILabel *accumulatedIntegralInfoLabel;
@property (strong, nonatomic) IBOutlet UILabel *accumulatedIntegralLabel;
@property (strong, nonatomic) IBOutlet UILabel *cardIntegralInfoLabel;
@property (strong, nonatomic) IBOutlet UILabel *cardIntegralLabel;
@property (strong, nonatomic) IBOutlet UILabel *oldCardNumberLabel;
@property (strong, nonatomic) IBOutlet UILabel *oldCardNumberInfoLabel;
@property (strong, nonatomic) IBOutlet UILabel *cardNumberInfoLabel;
@property (strong, nonatomic) IBOutlet UILabel *cardNumberLabel;
@property (strong, nonatomic) IBOutlet UIButton *cardTypeBtn;
@property (strong, nonatomic) IBOutlet UIButton *shengJiBtn;
@property (strong, nonatomic) IBOutlet UIView *huiYuanQuanYiView;
@property (strong, nonatomic) IBOutlet UIView *cardTypeView;
@property (strong, nonatomic) IBOutlet UIButton *huiYuanQuanYiBtn;
@property (strong, nonatomic) IBOutlet UIImageView *upOrDownArrorImageView;
@property (strong, nonatomic) IBOutlet UILabel *quanYiLabel;
@property (strong, nonatomic) IBOutlet UILabel *kaJiBieLabelInfo;
@property (strong, nonatomic) IBOutlet UILabel *kaJiBieLabelValue;
@property (strong, nonatomic) IBOutlet  UIScrollView *myScrollView;
@property (weak, nonatomic) IBOutlet UIView *myView;

@end
