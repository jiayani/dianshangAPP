//
//  WXUserInfoViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXUserInfoViewController.h"
#import "WXCommonViewClass.h"
#import "WXCommonDateClass.h"
#import "WXUserDatabaseClass.h"
#import "WX_Cloud_IsConnection.h"
#import "UserInfo.h"
#import "AppDelegate.h"
#import "WX_Cloud_UserInfo.h"

#define TagCount 7
@interface WXUserInfoViewController ()

@end

@implementation WXUserInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"个人资料", @"个人资料");
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //注册监听统计UITextFiled输入字符的个数
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textFiledEditChanged:) name:@"UITextFieldTextDidChangeNotification" object:self.myEditingTextField];
    [self initToolBar];
    [self initAllControllers];
    
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    orginYOfView = self.view.frame.origin.y;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[WX_Cloud_UserInfo share] setDelegate:nil];
}
- (void)viewDidUnload{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"UITextFieldTextDidChangeNotification" object:self.myEditingTextField];
    [self setNoteTxt:nil];
    [self setMarriageBtn:nil];
    [self setUnMarriagebtn:nil];
    [self setWomanBtn:nil];
    [self setManBtn:nil];
    [self setEducationTxt:nil];
    [self setWorkTxt:nil];
    [self setCityTxt:nil];
    [self setNiChengTxt:nil];
    [self setBirthdayTxt:nil];
    [self setOkBtn:nil];
    [self setScrollView:nil];
    [self setLabelArray:nil];
    [self setLabelInfoArray:nil];
    [self setToolbar:nil];
    [self setLoveTxt:nil];
    [self setMyEditingTextField:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    self.myEditingTextField = textField;
    NSUInteger tag=[textField tag];
    [self checkBarButton:tag];
    [self animateView:tag];

}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.myEditingTextField = nil;
}

#pragma mark - ToolBar mehtods
/*初始化toolbar*/
- (void)initToolBar{
    
    if (self.toolbar == nil) {
        self.toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 38.0f)];
        self.toolbar.barStyle = UIBarStyleBlackTranslucent;
        
        UIBarButtonItem *preBarItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"上一项", @"上一项")
                                                                       style:UIBarButtonItemStyleBordered
                                                                      target:self
                                                                      action:@selector(previousField:)];
        UIBarButtonItem *nextBarItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"下一项", @"下一项")
                                                                        style:UIBarButtonItemStyleBordered
                                                                       target:self
                                                                       action:@selector(nextField:)];
        
        UIBarButtonItem *spaceBarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                      target:nil
                                                                                      action:nil];
        
        UIBarButtonItem *doneBarItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"完成", @"完成")
                                                                        style:UIBarButtonItemStyleDone
                                                                       target:self
                                                                       action:@selector(resignKeyboard:)];
        
        [self.toolbar setItems:[NSArray arrayWithObjects:preBarItem,nextBarItem,spaceBarItem,doneBarItem,nil]];
        
    }
    
}

//单击完成时，隐藏键盘
-(IBAction)resignKeyboard:(id)sender
{
    
    [self.myEditingTextField resignFirstResponder];
    CGRect viewRect = self.view.frame;
    viewRect.origin.y = orginYOfView;
    self.view.frame = viewRect;
    [self.scrollView setContentOffset:CGPointMake(0, 0)];
    if (!iPhone5) {
        if (!iPhone5) {
            self.scrollView.contentSize = CGSizeMake(320, self.view.frame.size.height + 80.0f);
        }
    }else{
        self.scrollView.contentSize = self.view.frame.size;
    }
    
}
//单击前一项按钮执行的操作
-(IBAction)previousField:(id)sender
{
    id firstResponder = self.myEditingTextField;
    if ([firstResponder isKindOfClass:[UITextField class]]) {
        NSUInteger tag = [(UITextField *)firstResponder tag];
        NSUInteger previousTag = tag ==1 ? 1 : tag-1;
        [self checkBarButton:previousTag];
        [self animateView:previousTag];
        
        UITextField *previousField = (UITextField *)[self.view viewWithTag:previousTag];
        [previousField becomeFirstResponder];
        
    }
    
}
//单击下一项按钮执行的操作
-(IBAction)nextField:(id)sender
{
    id firstResponder = self.myEditingTextField;
    if ([firstResponder isKindOfClass:[UITextField class]]) {
        NSUInteger tag = [(UITextField *)firstResponder tag];
        NSUInteger nextTag=tag == TagCount ? TagCount : tag+1;
        [self checkBarButton:nextTag];
        [self animateView:nextTag];
        
        UITextField *previousField = (UITextField *)[self.view viewWithTag:nextTag];
        
        [previousField becomeFirstResponder];
        
        
    }
}
//动态滚动到相应位置
-(void)animateView:(NSUInteger)tag
{
    CGRect viewRect = [self.view frame];
    CGRect rect=[[self.view viewWithTag:tag] frame];
    CGFloat tempHeight = viewRect.size.height - 320;
    CGPoint point = CGPointMake(0, rect.origin.y - tempHeight);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    if (iPhone5) {
         self.scrollView.contentSize = CGSizeMake(320, viewRect.size.height + 254.0f);
    }else{
        self.scrollView.contentSize = CGSizeMake(320, self.view.frame.size.height + 334.0f);
    }
   
    if (tag > beginScrollInt && tag <= TagCount) {
        [self.scrollView setContentOffset:point];
        viewRect.origin.y = orginYOfView;
        
    }
    else{
        [self.scrollView setContentOffset:CGPointMake(0, 0)];
        
        viewRect.origin.y = orginYOfView;
    }
    
    self.view.frame = viewRect;
    [UIView commitAnimations];
}
//检查previous和nextbutton 可不可用
-(void)checkBarButton:(NSUInteger)tag
{
    UIBarButtonItem *previousBarItem=(UIBarButtonItem *)[[self.toolbar items] objectAtIndex:0];
    UIBarButtonItem *nextBarItem=(UIBarButtonItem *)[[self.toolbar items] objectAtIndex:1];
    
    [previousBarItem setEnabled:tag == 1 ? NO: YES];
    [nextBarItem setEnabled:tag == TagCount ? NO : YES];
}
#pragma mark - Create DatePicker
- (void)createDatePicker{
    if (self.datePicker == nil) {
        self.datePicker = [[UIDatePicker alloc]init];
        [self.datePicker setLocale:[NSLocale currentLocale]];
        [self.datePicker setTimeZone:[NSTimeZone defaultTimeZone]];
        [self.datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
        NSDate *now = [NSDate date];
        self.selectedDate = now;
        self.datePicker.date = now;
        [self.datePicker setMaximumDate:now];//生日最大不能超过今天
        [self.datePicker setDatePickerMode:UIDatePickerModeDate];
        NSString *selectedDateStr = [WXCommonDateClass getDateStringOfFormat:@"yyyy-MM-dd" fromDate:self.selectedDate];
        self.birthdayTxt.text = selectedDateStr;
    }
}
- (void)dateChanged:(UIDatePicker *)myPicker{
    self.selectedDate = self.datePicker.date;
    
    NSString *selectedDateStr = [WXCommonDateClass getDateStringOfFormat:@"yyyy-MM-dd" fromDate:self.selectedDate];
    self.birthdayTxt.text = selectedDateStr;
}

#pragma mark - All My Methods

//初始化页面控件信息
- (void)initAllControllers{
    
    /*设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    
    self.currentUser = [WXUserDatabaseClass getCurrentUser];
    self.labelInfoArray = [[NSArray alloc]initWithObjects:
                           NSLocalizedString(@"昵称:", @"个人资料/昵称:"),
                           NSLocalizedString(@"性别:", @"个人资料/性别:"),
                           NSLocalizedString(@"生日:", @"个人资料/生日:"),
                           NSLocalizedString(@"城市:", @"个人资料/城市:"),
                           NSLocalizedString(@"职业:", @"个人资料/职业:"),
                           NSLocalizedString(@"婚姻:", @"个人资料/婚姻:"),
                           NSLocalizedString(@"教育:", @"个人资料/教育:"),
                           NSLocalizedString(@"爱好:", @"个人资料/爱好:"),
                           NSLocalizedString(@"签名:", @"个人资料/签名:"),
                           NSLocalizedString(@"男", @"个人资料/男"),
                           NSLocalizedString(@"女", @"个人资料/女"),
                           NSLocalizedString(@"已婚", @"个人资料/已婚"),
                           NSLocalizedString(@"未婚", @"个人资料/未婚"),
                           nil];
    //循环初始化页面上的label信息
    int i = 0;
    for (UILabel *oneLabel in self.labelArray) {
        oneLabel.text = [self.labelInfoArray objectAtIndex:i];
        i++;
    }
    [self.okBtn setTitle:NSLocalizedString(@"确认", @"个人资料/确认") forState:UIControlStateNormal];
    if (!iPhone5) {
        //使用scrollView很关键的地方，设置contentSize，一般是安你的scrollView的size来设置，后面的height可以适当多个300～400
        self.scrollView.contentSize = CGSizeMake(320, self.scrollView.frame.size.height + 50.0f);
        
        beginScrollInt = 2;//从第二个UITextField开始滑动
    }else{
        beginScrollInt = 4;//从第三个UITextField开始滑动
    }
    self.scrollView.scrollEnabled = YES;
    scrollHeight = self.scrollView.contentSize.height;
    [self initToolBar];
    [self createDatePicker];
    
    //初始化
    
    self.niChengTxt.inputAccessoryView = self.toolbar;
    self.birthdayTxt.inputAccessoryView = self.toolbar;
    self.cityTxt.inputAccessoryView = self.toolbar;
    self.workTxt.inputAccessoryView = self.toolbar;
    self.educationTxt.inputAccessoryView = self.toolbar;
    self.loveTxt.inputAccessoryView = self.toolbar;
    self.noteTxt.inputAccessoryView = self.toolbar;
    self.birthdayTxt.inputView = self.datePicker;
    
    [self setAllValuesFormDatabase];
    if (!iPhone5) {
        self.scrollView.contentSize = CGSizeMake(320, self.view.frame.size.height + 80.0f);
    }
}

//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setAllValuesFormDatabase{
    if (self.currentUser != nil) {
        //昵称
        self.niChengTxt.text = self.currentUser.nickName == nil? @"" :self.currentUser.nickName;
        //性别
        if (self.currentUser.sex == nil) {
            self.sexNumber = [NSNumber numberWithInt:1];//默认选择的是男
        }else{
            self.sexNumber = self.currentUser.sex;
        }
        if ([self.sexNumber intValue] == 1) {
            [self sexRadioButtonPressed:self.manBtn];
        }else{
            [self sexRadioButtonPressed:self.womanBtn];
        }
        
        //生日
        if (self.currentUser.birthday == nil) {
            
            self.selectedDate = [WXCommonDateClass getDateFromString:@"1990-01-01" OfFormat:@"yyyy-MM-dd"];
        }else{
            self.selectedDate = self.currentUser.birthday;
        }
        self.datePicker.date = self.selectedDate;
        NSString *selectedDateStr = [WXCommonDateClass getDateStringOfFormat:@"yyyy-MM-dd" fromDate:self.selectedDate];
        self.birthdayTxt.text = selectedDateStr;
        //城市
        self.cityTxt.text = self.currentUser.city;
        //职业
        self.workTxt.text = self.currentUser.profession;
        //婚姻
        if (self.currentUser.marriage == nil) {
            self.marriageStr = @"未婚";
            [self marriageRadioButtonPressed:self.unMarriagebtn];
        }else{
            if ([self.currentUser.marriage isEqualToString:@"未婚"]) {
                self.marriageStr = @"未婚";
                [self marriageRadioButtonPressed:self.unMarriagebtn];
            }else{//已婚
                self.marriageStr = @"已婚";
                [self marriageRadioButtonPressed:self.marriageBtn];
            }
        }
        //教育
        self.educationTxt.text = self.currentUser.education;
        //爱好
       self.loveTxt.text = self.currentUser.hobby;
        
        //签名
        if (self.currentUser.signature != nil) {
            self.noteTxt.text = self.currentUser.signature;
        }else{
            self.noteTxt.text = @"";
        }
   }
    
}
//性别单选按钮事件
- (IBAction)sexRadioButtonPressed:(UIButton *)sender {
    NSString *checkRadioimageName , *uncheckRadioImageName;
    checkRadioimageName = @"checkRadio.png";
    uncheckRadioImageName = @"uncheckRadio.png";
    if (sender.tag == 1) {//选择是男
        
        self.sexNumber = [NSNumber numberWithInt:1];
        [self.manBtn setBackgroundImage:[UIImage imageNamed:checkRadioimageName] forState:UIControlStateNormal];
        [self.womanBtn setBackgroundImage:[UIImage imageNamed:uncheckRadioImageName] forState:UIControlStateNormal];
        
    }else if (sender.tag == 0) {//选择是女
        
        self.sexNumber = [NSNumber numberWithInt:0];
        [self.manBtn setBackgroundImage:[UIImage imageNamed:uncheckRadioImageName] forState:UIControlStateNormal];
        [self.womanBtn setBackgroundImage:[UIImage imageNamed:checkRadioimageName] forState:UIControlStateNormal];
    }
    
}
//婚姻单选按钮事件
- (IBAction)marriageRadioButtonPressed:(UIButton *)sender {
    NSString *checkRadioimageName , *uncheckRadioImageName;
    checkRadioimageName = @"checkRadio.png";
    uncheckRadioImageName = @"uncheckRadio.png";
    if (sender.tag == 0) {
        self.marriageStr = @"未婚";
        [self.unMarriagebtn setBackgroundImage:[UIImage imageNamed:checkRadioimageName] forState:UIControlStateNormal];
        [self.marriageBtn setBackgroundImage:[UIImage imageNamed:uncheckRadioImageName] forState:UIControlStateNormal];
    }else if (sender.tag == 1) {
        self.marriageStr = @"已婚";
        [self.unMarriagebtn setBackgroundImage:[UIImage imageNamed:uncheckRadioImageName] forState:UIControlStateNormal];
        [self.marriageBtn setBackgroundImage:[UIImage imageNamed:checkRadioimageName] forState:UIControlStateNormal];
    }
}

- (IBAction)okBtnPressed:(id)sender {
    
    //本地判断是否连接网络
    BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
    if (!isConnection) {//无网络情况
        NSString *errCode = NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:errCode delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"确定") otherButtonTitles:nil];
        [alert show];
    }else{
        [self setDatabaseDic];
        //小圈转动等待服务器端验证
        [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"正在提交...", @"个人资料/正在提交...") nav:self.navigationController];
        //调用后台服务器端方法修改密码
        [[WX_Cloud_UserInfo share] setDelegate:(id)self];
        [[WX_Cloud_UserInfo share]updateUserInfo:self.dataDic];
    }
    
}

/*把读到的数据打包到一个数据字典中*/
- (void)setDatabaseDic{
    self.dataDic = [[NSMutableDictionary alloc]init];
    
    NSString *niChengStr = self.niChengTxt.text;
    if (niChengStr == nil) {
        niChengStr = @"";
    }
    [self.dataDic setObject:niChengStr forKey:@"nickname"];
    [self.dataDic setObject:self.sexNumber forKey:@"sex"];
    [self.dataDic setObject:self.selectedDate forKey:@"birthday"];
    [self.dataDic setObject:self.cityTxt.text == nil? @"" : self.cityTxt.text forKey:@"city"];
    [self.dataDic setObject:self.workTxt.text == nil ? @"" : self.workTxt.text forKey:@"profession"];
    [self.dataDic setObject:self.marriageStr forKey:@"marriage"];
    [self.dataDic setObject:self.educationTxt.text == nil ? @"" : self.educationTxt.text forKey:@"education"];
    [self.dataDic setObject:self.loveTxt.text == nil ? @"" : self.loveTxt.text forKey:@"hobby"];
    [self.dataDic setObject:self.noteTxt.text == nil ? @"" : self.noteTxt.text  forKey:@"signature"];
    
    
}

-(void)textFiledEditChanged:(NSNotification *)obj{
    
    UITextField *textField = (UITextField *)obj.object;
    NSInteger tag = textField.tag;
    int MAX_CHARS;
    if(tag == 1){
        MAX_CHARS = 12;
    }else if (tag == 6)
        MAX_CHARS = 50;
    else{
        MAX_CHARS = 10;
    }
    
    NSString *toBeString = textField.text;
    NSString *lang = [[UITextInputMode currentInputMode] primaryLanguage]; // 键盘输入模式
    if ([lang isEqualToString:@"zh-Hans"]) { // 简体中文输入，包括简体拼音，健体五笔，简体手写
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > MAX_CHARS) {
                textField.text = [toBeString substringToIndex:MAX_CHARS];
            }
        }
        // 有高亮选择的字符串，则暂不对文字进行统计和限制
        else{
            
        }
    }
    // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
    else{
        if (toBeString.length > MAX_CHARS) {
            textField.text = [toBeString substringToIndex:MAX_CHARS];
        }
    }
}

#pragma mark - DataBase Methods
/*修改当前用户的个人信息*/
- (void)upDateDatabase{
     self.currentUser.nickName = [self.dataDic objectForKey:@"nickname"];
     self.currentUser.sex = [self.dataDic objectForKey:@"sex"];
     self.currentUser.birthday = [self.dataDic objectForKey:@"birthday"];
     self.currentUser.city = [self.dataDic objectForKey:@"city"];
     self.currentUser.profession = [self.dataDic objectForKey:@"profession"];
     self.currentUser.marriage = [self.dataDic objectForKey:@"marriage"];
     self.currentUser.education = [self.dataDic objectForKey:@"education"];
     self.currentUser.hobby = [self.dataDic objectForKey:@"hobby"];
     self.currentUser.signature = [self.dataDic objectForKey:@"signature"];
    
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[appDelegate managedObjectContext];
    
    NSError *error;
    [context save:&error];
    
    
}
#pragma mark - WX_Cloud_Delegate Methods

//无网络连接/意外错误
- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[WX_Cloud_UserInfo share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
    
    
}
//个人资料修改
- (void)updateUserInfoSuccess:(NSDictionary*)dataDic{
    [[WX_Cloud_UserInfo share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    //保存到本地数据库中
    [self upDateDatabase];
    //刷新页面空间内容
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)updateUserInfoFailed:(NSString*)errCode{
    [[WX_Cloud_UserInfo share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInView:self.view title:errCode];
}
@end
