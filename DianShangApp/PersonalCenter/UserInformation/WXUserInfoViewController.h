//
//  WXUserInfoViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"
@class UserInfo;

@interface WXUserInfoViewController : UIViewController<UITextFieldDelegate,WX_Cloud_Delegate>{
    CGFloat scrollHeight;
    int beginScrollInt;
    float orginYOfView;
}
@property (strong,nonatomic) UserInfo *currentUser;
@property (strong,nonatomic) NSMutableDictionary *dataDic;
@property (retain,nonatomic) UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak,nonatomic) UITextField *myEditingTextField;
@property (strong,nonatomic) NSNumber *sexNumber;
@property (strong,nonatomic) NSDate *selectedDate;
@property (strong,nonatomic) NSString *marriageStr;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labelArray;
@property (strong, nonatomic) NSArray *labelInfoArray;
@property (weak, nonatomic) IBOutlet UIButton *manBtn;
@property (weak, nonatomic) IBOutlet UIButton *womanBtn;
@property (weak, nonatomic) IBOutlet UIButton *unMarriagebtn;
@property (weak, nonatomic) IBOutlet UIButton *marriageBtn;
@property (weak, nonatomic) IBOutlet UIButton *okBtn;
@property (weak, nonatomic) IBOutlet UITextField *noteTxt;
@property (weak, nonatomic) IBOutlet UITextField *loveTxt;
@property (weak, nonatomic) IBOutlet UITextField *niChengTxt;
@property (weak, nonatomic) IBOutlet UITextField *birthdayTxt;
@property (weak, nonatomic) IBOutlet UITextField *workTxt;
@property (weak, nonatomic) IBOutlet UITextField *educationTxt;
@property (weak, nonatomic) IBOutlet UITextField *cityTxt;
@property (nonatomic,strong) UIDatePicker *datePicker;
@property (nonatomic,strong)  UIDatePicker *timePicker;


//性别单选按钮事件
- (IBAction)sexRadioButtonPressed:(UIButton *)sender;
//婚姻单选按钮事件
- (IBAction)marriageRadioButtonPressed:(UIButton *)sender;
- (IBAction)okBtnPressed:(id)sender;
- (void)setAllValuesFormDatabase;
@end
