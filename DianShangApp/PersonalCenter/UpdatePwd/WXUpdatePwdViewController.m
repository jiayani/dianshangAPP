//
//  WXUpdatePwdViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXUpdatePwdViewController.h"
#import "WXCommonViewClass.h"
#import "WXUserDatabaseClass.h"
#import "WX_Cloud_IsConnection.h"
#import "WXVerifyMethodsClass.h"
#import "WXUserLoginViewController.h"
#import "WX_Cloud_UpdatePwd.h"
#import "MyMD5.h"


@interface WXUpdatePwdViewController ()

@end

@implementation WXUpdatePwdViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"修改密码", @"修改密码页面标题");
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initAllControllers];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[WX_Cloud_UpdatePwd share] setDelegate:nil];
    mbProgressHUD.delegate = nil;
    mbProgressHUD = nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload {
    [self setOkBtn:nil];
    [self setMyOldPwdTxt:nil];
    [self setMyNewPwdTXt:nil];
    [self setMyNewPwdAgainTxt:nil];
    self.errorStr = nil;
    self.myEditingTextField = nil;
    [super viewDidUnload];
}

#pragma mark - UITextFieldDelegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    self.myEditingTextField = textField;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.myEditingTextField = nil;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([self.myOldPwdTxt isFirstResponder]) {
        [self.myNewPwdTXt becomeFirstResponder];
    }else if ([self.myNewPwdTXt isFirstResponder]){
        [self.myNewPwdAgainTxt becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return YES;
}


#pragma mark - All My Methods
- (void)initAllControllers{
    /*设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;

    self.myOldPwdTxt.placeholder = NSLocalizedString(@"请输入旧密码", @"修改密码/请输入旧密码");
    self.myNewPwdTXt.placeholder = NSLocalizedString(@"请输入新密码", @"修改密码/请输入新密码");
    self.myNewPwdAgainTxt.placeholder = NSLocalizedString(@"确认新密码", @"修改密码/确认新密码");
    
    [self.okBtn setTitle:NSLocalizedString(@"确认修改", @"确认修改") forState:UIControlStateNormal];
    currentUser = [WXUserDatabaseClass getCurrentUser];
}
//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    if ([touch view] == self.view) {
        if (self.myEditingTextField != nil) {
            [self.myEditingTextField resignFirstResponder];
            
        }
    }
}
- (IBAction)okBtnPressed:(id)sender {
    if (self.myEditingTextField != nil) {
        [self.myEditingTextField resignFirstResponder];
    }
    
    BOOL isValidatePwd = [WXVerifyMethodsClass isValidatePassword:self.myNewPwdTXt.text];
    BOOL isAllValidate = YES;
    NSString *errorStr;
    if (self.myOldPwdTxt.text.length <= 0 ){
        isAllValidate = NO;
        errorStr = NSLocalizedString(@"旧密码不能为空", @"修改密码/旧密码不能为空");
        alertInt = 10;
        
    }else if (self.myNewPwdTXt.text.length <= 0){
        isAllValidate = NO;
        errorStr = NSLocalizedString(@"新密码不能为空", @"修改密码/新密码不能为空");
        alertInt = 11;
        
    }else if (self.myNewPwdAgainTxt.text.length <= 0){
        isAllValidate = NO;
        errorStr = NSLocalizedString(@"确认密码不能为空", @"修改密码/确认密码不能为空");
        alertInt = 12;
        
    }else if (self.myNewPwdTXt.text.length < 6){
        isAllValidate = NO;
        errorStr = NSLocalizedString(@"密码不能少于6个字符", @"修改密码/密码不能少于6个字符");
        alertInt = 11;
        
    }else if(self.myNewPwdTXt.text.length > 16){
        isAllValidate = NO;
        errorStr = NSLocalizedString(@"密码不能超过16个字符", @"修改密码/密码不能超过16个字符");
        alertInt = 11;
        
    }else if (!isValidatePwd){
        isAllValidate = NO;
        errorStr = NSLocalizedString(@"密码只能由数字、大小写字母组成", @"修改密码/密码只能由数字、大小写字母组成");
        alertInt = 11;
        
        
    }else if (![self.myNewPwdTXt.text isEqualToString:self.myNewPwdAgainTxt.text]){
        isAllValidate = NO;
        errorStr = NSLocalizedString(@"两次输入的新密码不一致，请重新输入", @"修改密码/两次输入的新密码不一致，请重新输入");
        alertInt = 12;
    }
    if (!isAllValidate) {

        self.errorStr = errorStr;
        mbProgressHUD = [WXCommonViewClass showHudInView:self.view title:self.errorStr];
        mbProgressHUD.delegate = self;
        
    }else{
        //本地判断是否连接网络
        BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
        if (!isConnection) {//无网络情况
            NSString *errCode = NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用");
            [WXCommonViewClass showHudInView:self.view title:errCode];
        }else{
            
            //小圈转动等待服务器端验证
            [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"正在修改...", @"修改密码/正在修改...") nav:self.navigationController];
            //调用后台服务器端方法修改密码
            NSString * oldPasswordMD5 = [MyMD5 md5:self.myOldPwdTxt.text];    //MD5加密
            NSString * newPasswordMD5 = [MyMD5 md5:self.myNewPwdTXt.text];    //MD5加密
            NSString * rePasswordMD5 = [MyMD5 md5:self.myNewPwdTXt.text];      //MD5加密
            [[WX_Cloud_UpdatePwd share] setDelegate:(id)self];
            [[WX_Cloud_UpdatePwd share]updatePwdWithOldPwd:oldPasswordMD5 newPassword:newPasswordMD5 ReNewPassword:rePasswordMD5];
        }
    }
    
    
}
-(void)pushLoginViewController {
    WXUserLoginViewController  *userLoginViewController = [[WXUserLoginViewController alloc]initWithNibName:@"WXUserLoginViewController" bundle:nil];
    userLoginViewController.isBackPersionCenter = YES;
    [self.navigationController pushViewController:userLoginViewController animated:YES];
   
}
#pragma mark － MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
    if (alertInt == 10) {
        [self.myOldPwdTxt becomeFirstResponder];
    }else if (alertInt == 11){
        [self.myNewPwdTXt becomeFirstResponder];
    }else if (alertInt == 12){
         [self.myNewPwdAgainTxt becomeFirstResponder];
    }else if (alertInt == 13){
        [self pushLoginViewController];
    }
}
#pragma mark - WYDCloudDelegate Methods
//无网络连接/意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[WX_Cloud_UpdatePwd share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInView:self.view title:errorStr];

}
//修改密码成功后的回调方法
-(void)updatePwdSuccess{
    [[WX_Cloud_UpdatePwd share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    //修改本地数据库
    NSString *newPasswordMD5 = [MyMD5 md5:self.myNewPwdTXt.text];
    [WXUserDatabaseClass updatePwdOfUser:currentUser pwdStr:newPasswordMD5];
    //修改注销当前用户
    [WXUserDatabaseClass setCurrentUserKey:nil];
    alertInt = 13;
    mbProgressHUD = [WXCommonViewClass showHudInView:self.view title:NSLocalizedString(@"修改密码成功",@"修改密码成功")];
    mbProgressHUD.delegate = self;
    
}

//修改密码失败后的回调方法
-(void)updatePwdFailed:(NSString*)errCode{
    [[WX_Cloud_UpdatePwd share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    alertInt = 10;
    mbProgressHUD = [WXCommonViewClass showHudInView:self.view title:errCode];
    mbProgressHUD.delegate = self;
}
@end
