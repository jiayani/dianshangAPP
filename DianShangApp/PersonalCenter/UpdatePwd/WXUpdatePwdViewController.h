//
//  WXUpdatePwdViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"
#import "UserInfo.h"
#import "MBProgressHUD.h"

@interface WXUpdatePwdViewController : UIViewController<UITextFieldDelegate,WX_Cloud_Delegate,MBProgressHUDDelegate>{
    UserInfo *currentUser;
    int alertInt;
    MBProgressHUD *mbProgressHUD;
}

@property (strong, nonatomic) UITextField *myEditingTextField;
@property (strong, nonatomic) IBOutlet UITextField *myOldPwdTxt;
@property (strong, nonatomic) IBOutlet UITextField *myNewPwdTXt;
@property (strong, nonatomic) IBOutlet UITextField *myNewPwdAgainTxt;
@property (strong, nonatomic) IBOutlet UIButton *okBtn;
@property (weak, nonatomic) NSString *errorStr;

- (IBAction)okBtnPressed:(id)sender;



@end
