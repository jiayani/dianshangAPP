//
//  WXUserDatabaseClass.m
//  TianLvApp
//
//  Created by 霞 王 on 13-7-12.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import "WXUserDatabaseClass.h"
#import "UserInfo.h"
#import "AppDelegate.h"
#import "MembershipCard.h"
#import "UserextroInfo.h"
#import "WXCommonDateClass.h"
#import "WXPersonalCenterViewController.h"
#import "UserextroInfo.h"



@implementation WXUserDatabaseClass
/*添加一条用户信息*/
+ (void)addUser:(NSMutableDictionary *)dic{
    
//    [self deleteUsers];//先清表，确保标中最多可以有一条数据
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[appDelegate managedObjectContext];
    
    NSError *error;
    NSEntityDescription *entityDescription=[NSEntityDescription entityForName:@"UserInfo" inManagedObjectContext:context];
    UserInfo *theObject;
    
    //查询本地数据库中是否有该数据 有：进行修改  无：添加一条数据
    NSFetchRequest *request=[[NSFetchRequest alloc]init];
    NSPredicate *pred=[NSPredicate predicateWithFormat:@"(userId=%@)",[dic objectForKey:@"uid"]];
    [request setPredicate:pred];
    [request setEntity:entityDescription];
    [context lock];
    NSArray *objects=[context executeFetchRequest:request error:&error];
    if (objects!=nil && objects.count>0) {
        
        theObject=(UserInfo *)[objects objectAtIndex:0];
        
    }else{
        theObject=(UserInfo *)[[NSManagedObject alloc]initWithEntity:entityDescription insertIntoManagedObjectContext:context];
    }
    theObject.bindQQID = [dic objectForKey:@"bindQQID"];
    theObject.userId = [dic objectForKey:@"uid"];
    theObject.userEmail = [dic objectForKey:@"username"];
    theObject.userPhone = [dic objectForKey:@"phone"];
    theObject.userPassword = [dic objectForKey:@"password"];
    theObject.integral = [NSNumber numberWithInt:[[dic objectForKey:@"integralCount"] intValue]];
    theObject.phoneNumber = [dic objectForKey:@"userphone"];
    theObject.wooUserName = [dic objectForKey:@"wooname"];
    theObject.wooIntegral = [NSNumber numberWithInt:[[dic objectForKey:@"woonumber"] intValue]];
    theObject.wooPassword = [dic objectForKey:@"woopassword"];
    theObject.isBindWoo = [NSNumber numberWithInt:[[dic objectForKey:@"isbindingwoo"] intValue]];
    theObject.nickName = [dic objectForKey:@"nickname"];
    theObject.sex = [NSNumber numberWithInt:[[dic objectForKey:@"sex"] intValue]];
    theObject.birthday = [WXCommonDateClass getDateFromString:[dic objectForKey:@"birthday"] OfFormat:@"yyyy-MM-dd"];
    theObject.profession = [dic objectForKey:@"profession"];
    theObject.marriage = [dic objectForKey:@"marriage"];
    theObject.education = [dic objectForKey:@"education"];
    NSArray * hobbyArray = [dic objectForKey:@"hobby"];
    NSString *hobbyStr = @"";
    if (hobbyArray != nil && hobbyArray.count > 0 ) {
        for (NSString *str in hobbyArray) {
           hobbyStr = [hobbyStr stringByAppendingString:str];
        }
    }
    
    theObject.hobby = hobbyStr;
    theObject.signature = [dic objectForKey:@"signature"];
    theObject.city = [dic objectForKey:@"city"];
   if ([context save:&error]) {
       [WXUserDatabaseClass setCurrentUserKey:theObject.userId];
       [WXUserDatabaseClass setLastTimeUserKey:theObject.userEmail];
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
       singletonClass.currentUser = theObject;
    }
    [context unlock];
    
}
/*
 *注册的时候，如果本地有需要删除本地的，如果没有需要添加到本地数据库中
 */
+ (void)registerUser:(NSMutableDictionary *)dic{
    //
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[appDelegate managedObjectContext];
    
    NSError *error;
    NSEntityDescription *entityDescription=[NSEntityDescription entityForName:@"UserInfo" inManagedObjectContext:context];
    UserInfo *theObject;
    
    //查询本地数据库中是否有该数据 有：进行修改  无：添加一条数据
    NSFetchRequest *request=[[NSFetchRequest alloc]init];
    NSPredicate *pred=[NSPredicate predicateWithFormat:@"(userId=%@)",[dic objectForKey:@"uid"]];
    [request setPredicate:pred];
    [request setEntity:entityDescription];
    NSArray *objects=[context executeFetchRequest:request error:&error];
    if (objects!=nil && objects.count>0) {
        
        theObject=(UserInfo *)[objects objectAtIndex:0];
        [context deleteObject:theObject];//删除该对象
        
    }
    theObject=(UserInfo *)[[NSManagedObject alloc]initWithEntity:entityDescription insertIntoManagedObjectContext:context];

    theObject.userId = [NSString stringWithFormat:@"%@",[dic objectForKey:@"uid"]];
    theObject.userEmail = [dic objectForKey:@"username"];
    theObject.userPassword = [dic objectForKey:@"password"];
    theObject.integral = [NSNumber numberWithInt:[[dic objectForKey:@"integralCount"] intValue]];
    if ([dic objectForKey:@"phone"] != nil) {
        theObject.userPhone = [dic objectForKey:@"phone"];
    }
    if ([context save:&error]) {
      
        [WXUserDatabaseClass setCurrentUserKey:theObject.userId];
        
    }
}

/*把当前登录的用户的主键保存到NSUserDefaults中*/
+ (void)setCurrentUserKey:(NSString *)currentUserKey{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.currentUserId = currentUserKey;
    singletonClass.currentUser = nil;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:currentUserKey forKey:@"CurrentUserKey"];
    [defaults synchronize];
    
}
//
+ (NSString *)getCurrentUserKey{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"CurrentUserKey"];
}
/*保存上一次登陆的用户名*/
+ (void)setLastTimeUserKey:(NSString *)LastTimeUserKey{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:LastTimeUserKey forKey:@"LastTimeUserKey"];
    [defaults synchronize];
    
}

+ (NSString *)getLastTimeUserKey{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"LastTimeUserKey"];
}

/*得到当前用户*/
+ (UserInfo *)getCurrentUser{
    NSString *keyStr = [self getCurrentUserKey];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if (singletonClass.currentUser == nil && keyStr != nil) {
        UserInfo *user=nil;
        AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *context = appDelegate.managedObjectContext;
        NSEntityDescription *entityDescription=[NSEntityDescription entityForName:@"UserInfo" inManagedObjectContext:context];
        NSFetchRequest *request=[[NSFetchRequest alloc]init];
        NSPredicate *pred=[NSPredicate predicateWithFormat:@"(userId=%@)",keyStr];
        [request setPredicate:pred];
        [request setEntity:entityDescription];
        
        NSError *error;
        [context lock];
        NSArray *objects=[context executeFetchRequest:request error:&error];
        if (objects!=nil && objects.count>0) {
            
            user=(UserInfo *)[objects objectAtIndex:0];

            
        }
        [context unlock];
        singletonClass.currentUser = user;
    }else{
        if (keyStr == nil) {
            singletonClass.currentUser = nil;
        }
    }
    return singletonClass.currentUser;
}
/*删除用户表中的所有数据*/
+ (BOOL)deleteUsers{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[appDelegate managedObjectContext];
    NSEntityDescription *entityDescription=[NSEntityDescription entityForName:@"UserInfo" inManagedObjectContext:context];
    NSFetchRequest *request=[[NSFetchRequest alloc]init];
    [request setEntity:entityDescription];
    
    NSError *error;
    NSArray *objects=[context executeFetchRequest:request error:&error];
    if (objects!=nil && objects.count>0) {
        UserInfo *user=(UserInfo *)[objects objectAtIndex:0];
        [context deleteObject:user];
        if (![context save:&error]) {
            return NO;
        }
        
    }
    return YES;
}
/*绑定手机号成功后 需要修改本地数据库*/
+ (void)updatePhoneOfUser:(UserInfo *)user phoneStr:(NSString *)phoneStr{
    user.phoneNumber = phoneStr;
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[appDelegate managedObjectContext];
    NSError *error;
    [context save:&error];
}
/*绑定邮箱成功后 需要修改本地数据库*/
+(void)updateEmailOfUser:(UserInfo *)user emailStr:(NSString *)emailStr
{
//    user.userName = emailStr;
//    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
//    NSManagedObjectContext *context=[appDelegate managedObjectContext];
//    NSError *error;
//    [context save:&error];
}


/*修改密码*/
+ (void)updatePwdOfUser:(UserInfo *)user pwdStr:(NSString *)pwdStr{
    
    user.userPassword = pwdStr;
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[appDelegate managedObjectContext];
    NSError *error;
    [context lock];
    [context save:&error];
    [context unlock];
}

//跳转到个人中心页面
+ (void)goToPersonVC:(UIViewController *)tempVC{
    WXPersonalCenterViewController *personalVC = [[WXPersonalCenterViewController alloc]initWithNibName:@"WXPersonalCenterViewController" bundle:nil];
    
    id tempNav = [tempVC.navigationController initWithRootViewController:personalVC];
    tempNav = nil;
    [tempVC removeFromParentViewController];
}
+ (NSDictionary *)getUserCountInfo:(UserInfo *)user{
    
    NSMutableDictionary *dataDic;
    UserextroInfo *theObject = user.userextroInfo;
    if (theObject != nil) {

        [dataDic setObject:theObject.cardLevel forKey:@"cardLevel"];
        [dataDic setObject:theObject.integral forKey:@"current_integral"];
        [dataDic setObject:theObject.totalintegral forKey:@"total_integral"];
        [dataDic setObject:theObject.proWaitPay forKey:@"product_wait_pay"];
        [dataDic setObject:theObject.proWaitDeliver forKey:@"product_wait_send"];
        [dataDic setObject:theObject.proWaitGet forKey:@"product_wait_receive"];
        [dataDic setObject:theObject.proWaitEvaluate forKey:@"product_wait_comment"];
        [dataDic setObject:theObject.giftWaitDeliver forKey:@"gift_wait_send"];
        [dataDic setObject:theObject.giftWaitGet forKey:@"gift_wait_receive"];
        
    }
    return dataDic;

}
//添加到数据库中
+ (void)addUserextroInfo:(UserInfo *)user dataDic:(NSDictionary *)dataDic{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[appDelegate managedObjectContext];
    UserextroInfo *theObject = user.userextroInfo;
    if (theObject == nil) {
        NSEntityDescription *entityDescription=[NSEntityDescription entityForName:@"UserextroInfo" inManagedObjectContext:context];
        theObject=(UserextroInfo *)[[NSManagedObject alloc]initWithEntity:entityDescription insertIntoManagedObjectContext:context];
    }
    
    theObject.cardLevel = [dataDic objectForKey:@"cardLevel"];
    theObject.integral = [dataDic objectForKey:@"current_integral"];
    theObject.totalintegral = [dataDic objectForKey:@"total_integral"];
    theObject.proWaitPay = [dataDic objectForKey:@"product_wait_pay"];
    theObject.proWaitDeliver = [dataDic objectForKey:@"product_wait_send"];
    theObject.proWaitGet = [dataDic objectForKey:@"product_wait_receive"];
    theObject.proWaitEvaluate = [dataDic objectForKey:@"product_wait_comment"];
    theObject.giftWaitDeliver = [dataDic objectForKey:@"gift_wait_send"];
    theObject.giftWaitGet = [dataDic objectForKey:@"gift_wait_receive"];
   
    
    NSError *error;
    [context save:&error];
}
@end
