//
//  WXUserLoginViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"
#import "MBProgressHUD.h"
#import <TencentOpenAPI/TencentOAuth.h>

@interface WXUserLoginViewController : UIViewController<UITextFieldDelegate,WX_Cloud_Delegate,MBProgressHUDDelegate,TencentSessionDelegate>{
    int alertTag;
    MBProgressHUD *mbProgressHUD;
    TencentOAuth* _tencentOAuth;
    NSArray* _permissions;
}

@property (readwrite) BOOL isShowToolBar;
@property (readwrite) BOOL isBackPersionCenter;
@property (weak, nonatomic) UITextField *myEditingTxt;
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UITextField *pwdText;
@property (weak, nonatomic) IBOutlet UIButton *forgetPwdBtn;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;
@property (weak, nonatomic) UIViewController *willPushViewController;
@property (strong, nonatomic) NSString *userNameOfPreLoginStr;
@property (weak,nonatomic) CustomNavigationController *proNav;
@property (weak, nonatomic) NSString * popFlag;

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
- (IBAction)forgetPwdBtnPressed:(id)sender;
- (IBAction)loginBtnPressed:(id)sender;
- (IBAction)registerBtnPressed:(id)sender;
- (void)myAlert:(int)tag;
- (IBAction)qqLoginBtnpress:(id)sender;

@end
