//
//  JYNweclomVC.m
//  TianLvApp
//
//  Created by wa on 14-8-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "JYNweclomVC.h"
#import "WXCommonSingletonClass.h"
@interface JYNweclomVC ()
{
    int create_user;
}
@end

@implementation JYNweclomVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIButton * backBtn = [WXCommonViewClass getBackButton];
    [backBtn addTarget:self action:@selector(backpresslogin) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftBtnItem;
    self.view.backgroundColor = RGBA(230.0f, 230.0f, 230.0f, 1);
    self.navigationItem.title = NSLocalizedString(@"欢迎", @"欢迎页面标题") ;
    WXCommonSingletonClass *single = [WXCommonSingletonClass share];
    NSString *qqname = [NSString stringWithFormat:@"%@", [single.qqUserInfoDic objectForKey:@"nickname"]];
    NSString *appName = [WXConfigDataControll getAppName];
    if (qqname ==nil) {
        self.qqnamelabel.text = [NSString stringWithFormat:@"Hi!"];
    }
    else{
        self.qqnamelabel.text = [NSString stringWithFormat:@"Hi,%@",qqname];

    }
    self.nicknameLabel.text = [NSString stringWithFormat:@"欢迎来到%@",appName];
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    
}


#pragma  mark - buttonpress
- (IBAction)activeBtnPress:(id)sender
{
    JYNActivate1ViewController *findVC = [[JYNActivate1ViewController alloc]initWithNibName:@"JYNActivate1ViewController" bundle:nil];
    [self.navigationController pushViewController:findVC animated:YES];
}
- (IBAction)NactiveBtnpress:(id)sender
{
    JYNsetMessVC *setmessaegVC = [[JYNsetMessVC alloc]initWithNibName:@"JYNsetMessVC" bundle:nil];
    [self.navigationController pushViewController:setmessaegVC animated:YES];
 
    
}
- (IBAction)pinlessBtnPress:(id)sender
{
    JYNpinlessVC *pinvc = [[JYNpinlessVC alloc]initWithNibName:@"JYNpinlessVC" bundle:nil];
    [self.navigationController pushViewController:pinvc animated:YES];
    
}
-(void)backpresslogin
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
