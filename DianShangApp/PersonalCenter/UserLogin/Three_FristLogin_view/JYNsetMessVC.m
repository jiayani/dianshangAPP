//
//  JYNsetMessVC.m
//  TianLvApp
//
//  Created by wa on 14-8-28.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "JYNsetMessVC.h"
#import "WXVerifyMethodsClass.h"
#import "WXCommonSingletonClass.h"
#import "FY_Cloud_checkThridLogin.h"
@interface JYNsetMessVC ()

@end

@implementation JYNsetMessVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationItem.title = @"设置密码";
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = RGBA(230.0f, 230.0f, 230.0f, 1);
    
    UIButton * backBtn = [WXCommonViewClass getBackButton];
    [backBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftBtnItem;
    
    UIImageView *topImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 20, 300, 91)];
    topImage.image = [UIImage imageNamed:@"twoRowInput.png"];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[FY_Cloud_checkThridLogin share] setDelegate:nil];
}

#pragma mark - All My Methods
- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UitextFileddelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    long tag = textField.tag;
    if (tag ==101) {
        [self.SpasswordTextFiled becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
        [self sureBtnpress:nil];
    }
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.passwordTextFiled resignFirstResponder];
    [self.SpasswordTextFiled resignFirstResponder];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sureBtnpress:(id)sender
{
   BOOL isValidatePwd = [WXVerifyMethodsClass isValidatePassword:self.passwordTextFiled.text];
    if (self.passwordTextFiled.text.length < 6){
        
        [self showInfo:@"提示信息" show_message:@"密码不能少于6个字符"];
        
    }else if(self.passwordTextFiled.text.length > 16){
       
        [self showInfo:@"提示信息" show_message:@"密码不能超过16个字符"];

        
    }else if (!isValidatePwd){
       
        [self showInfo:@"提示信息" show_message:@"密码只能由数字、大小写字母组成"];

        
    }else if (![self.passwordTextFiled.text isEqualToString:self.SpasswordTextFiled.text])
    {
        
        [self showInfo:@"提示信息" show_message:@"两次输入的新密码不一致，请重新输入"];


    }
    else
    {
        WXCommonSingletonClass *single = [WXCommonSingletonClass share];
        NSString *city = [NSString stringWithFormat:@"%@%@", [single.qqUserInfoDic objectForKey:@"province"],[single.qqUserInfoDic objectForKey:@"city"]];
        [[FY_Cloud_checkThridLogin share] setDelegate:(id)self];
        [[FY_Cloud_checkThridLogin share] checkThirdLoginWithOpenID:single.qqOpenID andLoginType:2 andCreateUser:1 andNickname:[single.qqUserInfoDic objectForKey:@"nickname"] andPassword:self.passwordTextFiled.text andSex:[single.qqUserInfoDic objectForKey:@"gender"] andCity:city];
        [WXCommonViewClass setActivityWX:YES content:@"请等待" nav:self.navigationController];
       }
}

-(void)checkLoginSuccess:(NSDictionary *)data
{
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.isThirdPartyLogging = YES;
    singletonClass.currentUserId = [data objectForKey:@"uid"];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)checkLoginFailed:(NSString*)errMsg
{
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInButtonOfView:self.view title:errMsg closeInSecond:0.3];
}

//无网络连接
-(void)haveNoNetwork{
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInButtonOfView:self.view title:NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用") closeInSecond:3];
}
//意外错误
-(void)accidentError:(NSError*)error{
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    NSString *errorStr = NSLocalizedString(@"网络异常", @"网络异常");
    [WXCommonViewClass showHudInButtonOfView:self.view title:errorStr closeInSecond:3];
}

-(void)showInfo:(NSString *)titles show_message:(NSString *)messages
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:titles message:messages delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
}
@end
