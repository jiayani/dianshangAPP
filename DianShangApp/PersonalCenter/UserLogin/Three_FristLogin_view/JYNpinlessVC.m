//
//  pinlessVC.m
//  TianLvApp
//
//  Created by wa on 14-8-28.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "JYNpinlessVC.h"
#import "FY_Cloud_bindOtherForQQ.h"
#import "WXCommonSingletonClass.h"
#import "WXCommonViewClass.h"
@interface JYNpinlessVC ()
{
    NSMutableData *mutableData;
}
@end

@implementation JYNpinlessVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    UIButton * backBtn = [WXCommonViewClass getBackButton];
    [backBtn addTarget:self action:@selector(backpresslogin) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftBtnItem;
    self.title = NSLocalizedString(@"绑定已有账号",@"绑定已有账号页面标题");
    self.view.backgroundColor = RGBA(230.0f, 230.0f, 230.0f, 1);
    mutableData = [[NSMutableData alloc]init];
    [WXCommonViewClass hideTabBar:self isHiden:YES];

}

#pragma mark - UitextFileddelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    long tag = textField.tag;
    if (tag ==101) {
        [self.passwordTextFiled becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
        [self surebtnpress:nil];
    }
    return YES;}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.numeberTextFiled resignFirstResponder];
    [self.passwordTextFiled resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)surebtnpress:(id)sender
{
    WXCommonSingletonClass *single = [WXCommonSingletonClass share];
    NSString * passwordMD5 = [MyMD5 md5:self.passwordTextFiled.text];  //加密
    [[FY_Cloud_bindOtherForQQ share] setDelegate:(id)self];
    [[FY_Cloud_bindOtherForQQ share] bindOtherWithAccount:self.numeberTextFiled.text andPassword:passwordMD5 andQQToken:single.qqOpenID andNickname:[single.qqUserInfoDic objectForKey:@"nickname"]];

}

-(void)bindOtherSuccess:(NSDictionary*)data
{
    [WXCommonViewClass showHudInView:self.view title:@"绑定成功"];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.isThirdPartyLogging = YES;
    singletonClass.currentUserId = [data objectForKey:@"uid"];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)bindOtherFailed:(NSString*)errCode
{
    [WXCommonViewClass showHudInView:self.view title:errCode];
}

-(void)showInfo:(NSString *)titles show_message:(NSString *)messages
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:titles message:messages delegate:nil cancelButtonTitle:@"好" otherButtonTitles:nil, nil];
    [alert show];
}
-(void)backpresslogin
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
