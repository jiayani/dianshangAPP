//
//  WXUserLoginViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXUserLoginViewController.h"
#import "WXCommonViewClass.h"
#import "WXUserDatabaseClass.h"
#import "WXFindPwd1ViewController.h"
#import "JYNUserRegisterViewController.h"
#import "WX_Cloud_IsConnection.h"
#import "WXVerifyMethodsClass.h"
#import "WX_Cloud_UserLogin.h"
#import "WXPersonalCenterViewController.h"
#import "WXCommonSingletonClass.h"
#import "MyMD5.h"
#import "FY_Cloud_checkThridLogin.h"
#import "JYN_cloud_loginnumber.h"
#import "JYNweclomVC.h"
#import "FYNewMoreViewController.h"
@interface WXUserLoginViewController ()

@end

@implementation WXUserLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"登录", @"登录页面标题");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initAllControllers];
    
    _permissions = [NSArray arrayWithObjects:
                    kOPEN_PERMISSION_GET_USER_INFO,
                    kOPEN_PERMISSION_GET_SIMPLE_USER_INFO,
                    kOPEN_PERMISSION_ADD_ALBUM,
                    kOPEN_PERMISSION_ADD_IDOL,
                    kOPEN_PERMISSION_ADD_ONE_BLOG,
                    kOPEN_PERMISSION_ADD_PIC_T,
                    kOPEN_PERMISSION_ADD_SHARE,
                    kOPEN_PERMISSION_ADD_TOPIC,
                    kOPEN_PERMISSION_CHECK_PAGE_FANS,
                    kOPEN_PERMISSION_DEL_IDOL,
                    kOPEN_PERMISSION_DEL_T,
                    kOPEN_PERMISSION_GET_FANSLIST,
                    kOPEN_PERMISSION_GET_IDOLLIST,
                    kOPEN_PERMISSION_GET_INFO,
                    kOPEN_PERMISSION_GET_OTHER_INFO,
                    kOPEN_PERMISSION_GET_REPOST_LIST,
                    kOPEN_PERMISSION_LIST_ALBUM,
                    kOPEN_PERMISSION_UPLOAD_PIC,
                    kOPEN_PERMISSION_GET_VIP_INFO,
                    kOPEN_PERMISSION_GET_VIP_RICH_INFO,
                    kOPEN_PERMISSION_GET_INTIMATE_FRIENDS_WEIBO,
                    kOPEN_PERMISSION_MATCH_NICK_TIPS_WEIBO,
                    nil];
    NSString *appid = @"1101127983";
    //    NSString *appid = [WXConfigDataControll getQQLoginID];
    _tencentOAuth = [[TencentOAuth alloc] initWithAppId:appid
											andDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
/*给UIImageView(checkbox Imageview)添加事件响应，用图片来模拟checkbox事件*/
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //单击页面时隐藏键盘
    if (self.myEditingTxt != nil) {
        [self.myEditingTxt resignFirstResponder];
    }
    
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    if (self.isShowToolBar) {
        [WXCommonViewClass hideTabBar:self isHiden:NO];
        self.navigationItem.hidesBackButton = YES;
    }else{
        [WXCommonViewClass hideTabBar:self isHiden:YES];
        self.navigationItem.hidesBackButton = NO;
        
    }
    self.userNameOfPreLoginStr = [WXUserDatabaseClass getLastTimeUserKey];
    if (self.userNameOfPreLoginStr != nil) {
        self.nameText.text = self.userNameOfPreLoginStr;
    }else{
        self.nameText.placeholder = NSLocalizedString(@"请输入您的邮箱", @"登录/请输入您的邮箱");
    }

    
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[WX_Cloud_UserLogin share] setDelegate:nil];
    mbProgressHUD.delegate = nil;
    mbProgressHUD = nil;
}
- (void)dealloc{
    [self setNameText:nil];
    [self setPwdText:nil];
    [self setForgetPwdBtn:nil];
    [self setLoginBtn:nil];
    [self setRegisterBtn:nil];
    if (self.myEditingTxt != nil) {
        [self.myEditingTxt resignFirstResponder];
    }
}

#pragma mark - UITextFieldDelegate Mthods

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    self.myEditingTxt = textField;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.myEditingTxt = nil;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    int tag = textField.tag;
    if (tag == 0) {
        [self.pwdText becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
        if (textField.text.length > 0 && self.nameText.text.length > 0) {
            [self loginBtnPressed:nil];
        }else{
            [textField resignFirstResponder];
        }
        
    }
    return YES;
}


#pragma mark - All My Methods
- (void)initAllControllers{
    /*设置导航取消按钮*/
    if (!self.isShowToolBar) {
        UIButton *cancelBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:NSLocalizedString(@"取消", @"登录/取消")];
        [cancelBtn addTarget:self action:@selector(cancelBtnPressed) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:cancelBtn];
        self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    }
    
    
    //设置导航右侧按钮
    self.registerBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:NSLocalizedString(@"更多", @"登录/注册")];
    [self.registerBtn addTarget:self action:@selector(moreBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.registerBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    self.pwdText.placeholder = NSLocalizedString(@"请输入密码", @"登录/请输入密码");
    [self.forgetPwdBtn setTitle:NSLocalizedString(@"忘记密码了？", @"登录/忘记密码了？") forState: UIControlStateNormal];
    [self.loginBtn setTitle:NSLocalizedString(@"立即登录", @"登录/立即登录") forState: UIControlStateNormal];
    
    
    //begin:给找回密码加下划线
    CGPoint originPoint = self.forgetPwdBtn.frame.origin;
    CGPoint originLable = self.forgetPwdBtn.titleLabel.frame.origin;
    CGSize sizePwd = self.forgetPwdBtn.titleLabel.frame.size;
    UIImageView *underlineImage = [[UIImageView alloc]initWithFrame:CGRectMake(originPoint.x + originLable.x,originPoint.y+originLable.y + sizePwd.height  ,sizePwd.width,1)];
    underlineImage.backgroundColor = [UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:0.8f];
    [self.view addSubview:underlineImage];
    //end
    
    
    //begin
    NSArray *array=[[NSArray alloc]initWithObjects:self.view ,nil];
    NSSet *set=[[NSSet alloc]initWithArray:array];
    [self touchesBegan:set withEvent:nil];
    //end
    self.nameText.keyboardType = UIKeyboardTypeEmailAddress;
    
    
}

//退出登录页面
-(void)cancelBtnPressed{
    
    [self pushOrPresend];
}
- (void)moreBtnPressed{
    FYNewMoreViewController *moreViewController = [[FYNewMoreViewController alloc]initWithNibName:@"FYNewMoreViewController" bundle:nil];
    [self.navigationController pushViewController:moreViewController animated:YES];
}

- (IBAction)forgetPwdBtnPressed:(id)sender {
    if (self.myEditingTxt != nil) {
        [self.myEditingTxt resignFirstResponder];
    }
    WXFindPwd1ViewController *findPwd1VC = [[WXFindPwd1ViewController alloc]initWithNibName:@"WXFindPwd1ViewController" bundle:nil];
    findPwd1VC.loginVC = self;
    [self.navigationController pushViewController:findPwd1VC animated:YES];
}

- (IBAction)loginBtnPressed:(id)sender {
    if (self.myEditingTxt != nil) {
        [self.myEditingTxt resignFirstResponder];
    }
    
    if (self.nameText.text.length <= 0){
        mbProgressHUD = [WXCommonViewClass showHudInView:self.view title:NSLocalizedString(@"用户名和密码不能为空", @"登录/用户名和密码不能为空")];
        mbProgressHUD.delegate =self;
        alertTag = 98;
        
        
    }else if(self.pwdText.text.length <=0){
        mbProgressHUD = [WXCommonViewClass showHudInView:self.view title:NSLocalizedString(@"用户名和密码不能为空", @"登录/用户名和密码不能为空")];
        mbProgressHUD.delegate =self;
        alertTag = 99;
        
    }else{
        
        NSString *name = [self.nameText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        BOOL isValidateEmail = [WXVerifyMethodsClass isValidateEmail:name];//本地格式验证
        BOOL isValidatePhone = [WXVerifyMethodsClass isValidatePhone:name];
        if (isValidateEmail || isValidatePhone) {
            //本地判断是否连接网络
            BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
            if (!isConnection) {//无网络情况
                NSString *errCode = NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用");
                [WXCommonViewClass showHudInView:self.view title:errCode];
            }else{
                //小圈运动,等待登录验证
                [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"正在登录...", @"登录/正在登录...") nav:self.navigationController];
                //调用后台方法进行服务器断验证
                NSString *passwordMD5 = [MyMD5 md5:self.pwdText.text];  //加密
                [[WX_Cloud_UserLogin share] setDelegate:(id)self];
                [[WX_Cloud_UserLogin share] userLoginWithUserName:name UserPassword:passwordMD5];
            }
            
            
        }else{
            [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
            mbProgressHUD = [WXCommonViewClass showHudInView:self.view title:NSLocalizedString(@"输入的账号必须是邮箱或电话号码", @"登录/输入的账号必须是邮箱或电话号码")];
            mbProgressHUD.delegate = self;
            alertTag = 99;
        }
    }
    [[JYN_cloud_loginnumber share]setDelegate:nil];
    [[JYN_cloud_loginnumber share]sendNumberToBackground];

}

- (IBAction)qqLoginBtnpress:(id)sender
{
    //小圈运动,等待登录验证
    [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"正在登录...", @"登录/正在登录...") nav:self.navigationController];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.shareStr = @"QQ";
    [_tencentOAuth authorize:_permissions inSafari:NO];
    
}
#pragma mark QQ登录回调
- (void)tencentDidLogin
{
	// 登录成功
    if([_tencentOAuth getUserInfo]){
        
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        singletonClass.qqOpenID = _tencentOAuth.openId;
        
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"警告" message:@"获取用户信息失败,请重新登录" delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"确定") otherButtonTitles:nil,nil];
        [alert show];
    }
}

- (void)tencentDidNotLogin:(BOOL)cancelled
{
	if (cancelled){
        [WXCommonViewClass showHudInView:self.view title:@"用户取消登录"];
	}
	else {
        [WXCommonViewClass showHudInView:self.view title:@"登录失败"];
	}
    
}
-(void)tencentDidNotNetWork
{
    [WXCommonViewClass showHudInView:self.view title:@"无网络连接"];
}

- (void)getUserInfoResponse:(APIResponse*) response {
    
	if (response.retCode == URLREQUEST_SUCCEED)
	{
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        singletonClass.qqUserInfoDic = response.jsonResponse;
        [[FY_Cloud_checkThridLogin share] setDelegate:(id)self];
        [[FY_Cloud_checkThridLogin share] checkThirdLoginWithOpenID:singletonClass.qqOpenID andLoginType:2 andCreateUser:0 andNickname:[response.jsonResponse objectForKey:@"nickname"] andPassword:@"" andSex:@"" andCity:@""];
    }
    
	else
    {
		UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:@"操作失败" delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"确定") otherButtonTitles:nil];
        [alert show];
	}
}

-(void)checkLoginSuccess:(NSDictionary *)data
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.isThirdPartyLogging = YES;
    singletonClass.currentUserId = [data objectForKey:@"uid"];
    /*登录成功后给后台发送接口以便后台进行登录次数的统计*/
    [[JYN_cloud_loginnumber share]setDelegate:nil];
    [[JYN_cloud_loginnumber share]sendNumberToBackground];
    
    //用户存在
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)checkLoginFailed:(NSString*)errMsg
{
    //用户不存在
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    JYNweclomVC *welcomeVC = [[JYNweclomVC alloc] initWithNibName:@"JYNweclomVC" bundle:nil];
    [self.navigationController pushViewController:welcomeVC animated:YES];

   }

-(void)checkLoginFrozen:(NSString*)errMsg
{
    //用户账号被冻结
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInView:self.view title:errMsg];
}
-(void)checkLoginOtherFailed:(NSString*)errMsg
{
    //其他错误
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInView:self.view title:errMsg];
}

- (IBAction)registerBtnPressed:(id)sender {
    if (self.myEditingTxt != nil) {
        [self.myEditingTxt resignFirstResponder];
    }
    
    JYNUserRegisterViewController *registerVC = [[JYNUserRegisterViewController alloc]initWithNibName:@"JYNUserRegisterViewController" bundle:nil];
    [self.navigationController pushViewController:registerVC animated:YES];
}
//根据不同的呈现方式 来处理消失的方式
- (void)pushOrPresend{
    if (self.isShowToolBar) {
        [self.navigationController popViewControllerAnimated:NO];
        
    }else if(self.isBackPersionCenter){
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}
- (void)myAlert:(int)tag{
    if (tag == 98 || tag == 100) {
        [self.nameText becomeFirstResponder];
    }else if(tag == 99){
        [self.pwdText becomeFirstResponder];
    }
}
#pragma mark － MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[self myAlert:alertTag];
}

#pragma mark - WX_Cloud_Delegate Methods
//意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[WX_Cloud_UserLogin share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}
//用户登陆
-(void)userLoginSuccess:(NSMutableDictionary*)databaseDictionary{
    [[WX_Cloud_UserLogin share] setDelegate:nil];
     WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.myLastLoginName = self.nameText.text;

    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    NSString *passwordMD5 = [MyMD5 md5:self.pwdText.text];  //加密
    [databaseDictionary setObject:passwordMD5 forKey:@"password"];
    [WXUserDatabaseClass addUser:databaseDictionary];
    [self pushOrPresend];
    
}

-(void)userLoginFailed:(NSString*)errCode{
    [[WX_Cloud_UserLogin share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    mbProgressHUD = [WXCommonViewClass showHudInView:self.view title:errCode];
    mbProgressHUD.delegate = self;
    alertTag = 100;
    
}

@end
