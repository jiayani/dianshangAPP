//
//  JYNActivate3ViewController.m
//  TianLvApp
//
//  Created by wa on 14-9-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "JYNActivate3ViewController.h"
#import "WX_Cloud_createThirdPartUserByPhoneAndPass.h"
#import "WXCommonViewClass.h"
#import "WXVerifyMethodsClass.h"
#import "WX_Cloud_IsConnection.h"
#import "JYNActivate4ViewController.h"

@interface JYNActivate3ViewController ()

@end

@implementation JYNActivate3ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"会员激活", @"会员激活3页面标题");
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addBackButton];
    [self initAllControllers];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[WX_Cloud_createThirdPartUserByPhoneAndPass share] setDelegate:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - All My Methods
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    if ([touch view] == self.view) {
        if (self.myEditingTextField != nil) {
            [self.myEditingTextField resignFirstResponder];
            
        }
    }
}
/*初始化所有页面显示内容*/
- (void)initAllControllers{
    self.firstLabel.text = @"填写手机号";
    self.secondLabel.text = NSLocalizedString(@"验证身份", @"会员激活3/验证身份");
    self.thirdLabel.text = NSLocalizedString(@"设置密码", @"会员激活3/验证身份");
    self.fourLabel.text = NSLocalizedString(@"绑定成功", @"会员激活3/绑定成功");
    self.myPwdTXt.placeholder = @"输入登录密码";
    self.myPwdAgainTxt.placeholder = @"再次输入密码";
}
//创建返回按钮
- (void)addBackButton{
    UIButton * backBtn = [WXCommonViewClass getBackButton];
    [backBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftBtnItem;
}

- (void)goBack{
    //跳转到个人中心页面
    [self.navigationController popViewControllerAnimated:YES];
    
}
//本地进行密码验证
- (BOOL)verifyPassWord{
    BOOL isAllValidate = YES;
    BOOL isValidatePwd = [WXVerifyMethodsClass isValidatePassword:self.myPwdTXt.text];
    
    NSString *errorStr;
    if (self.myPwdTXt.text == nil || self.myPwdTXt.text.length <= 0) {
        isAllValidate = NO;
        errorStr = @"请填写登录密码";
    }else if (self.myPwdAgainTxt.text == nil || self.myPwdAgainTxt.text.length <= 0){
        isAllValidate = NO;
        errorStr = @"请填写确认密码";
    }else if (self.myPwdTXt.text.length < 6){
        isAllValidate = NO;
        errorStr = NSLocalizedString(@"密码不能少于6个字符", @"修改密码/密码不能少于6个字符");
        
    }else if(self.myPwdTXt.text.length > 16){
        isAllValidate = NO;
        errorStr = NSLocalizedString(@"密码不能超过16个字符", @"修改密码/密码不能超过16个字符");
        
    }else if (!isValidatePwd){
        isAllValidate = NO;
        errorStr = NSLocalizedString(@"密码只能由数字、大小写字母组成", @"修改密码/密码只能由数字、大小写字母组成");
        
    }else if (![self.myPwdTXt.text isEqualToString:self.myPwdAgainTxt.text]){
        isAllValidate = NO;
        errorStr = NSLocalizedString(@"两次输入的新密码不一致，请重新输入", @"修改密码/两次输入的新密码不一致，请重新输入");
        
    }
    if (isAllValidate == NO) {
        [WXCommonViewClass showHudInView:self.view title:errorStr];
    }
    return isAllValidate;
}

- (IBAction)okBtnPressed:(id)sender{
    if (self.myEditingTextField != nil) {
        [self.myEditingTextField resignFirstResponder];
    }
    BOOL isAllValidate= [self verifyPassWord];
    //本地判断是否连接网络
    if (isAllValidate) {
        BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
        if (!isConnection) {//无网络情况
            NSString *errCode = NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用");
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:errCode delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"确定") otherButtonTitles:nil];
            [alert show];
        }else{
            
            //小圈转动等待服务器端验证
            [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"正在修改...", @"修改密码/正在修改...") nav:self.navigationController];
            [self syncSetUserPwd:self.myPwdTXt.text];
        }
        
    }
    
}
//同步设置第三方登录的密码
- (void)syncSetUserPwd:(NSString *)pwd{
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setValue:@"2" forKey:@"authorize_type"];
    [dic setValue:self.phoneStr forKey:@"phone"];
    [dic setValue:self.validCodeStr forKey:@"validCode"];
    [dic setValue:pwd forKey:@"password"];
    [[WX_Cloud_createThirdPartUserByPhoneAndPass share] setDelegate:(id)self];
    [[WX_Cloud_createThirdPartUserByPhoneAndPass share] createThirdPartUserByPhoneAndPass:dic];
}

- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:errorCode];
}
#pragma mark - UITextFieldDelegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    self.myEditingTextField = textField;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([self.myPwdTXt isFirstResponder]) {
        [self.myPwdAgainTxt becomeFirstResponder];
    }
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.myEditingTextField = nil;
}

#pragma mark - WYDCloudDelegate Methods
//无网络连接
-(void)haveNoNetwork{
    [[WX_Cloud_createThirdPartUserByPhoneAndPass share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    NSString *errorStr = NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用");
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:errorStr delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"找回密码2/确定") otherButtonTitles:nil];
    [alert show];

}
//意外错误
-(void)accidentError:(NSError*)error{
    [[WX_Cloud_createThirdPartUserByPhoneAndPass share] setDelegate:nil];
    
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    NSString *errorStr = NSLocalizedString(@"网络异常", @"网络异常");
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:errorStr delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"找回密码2/确定") otherButtonTitles:nil];
    [alert show];}
//第三方登录后绑定手机激活设置密码接口
-(void)createThirdPartUserByPhoneAndPassSuccess:(NSMutableDictionary *)data{
    [[WX_Cloud_createThirdPartUserByPhoneAndPass share] setDelegate:nil];
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.isThirdPartyLogging = YES;
    singletonClass.currentUserId = [data objectForKey:@"uid"];
    //跳转到下一步
    JYNActivate4ViewController  *activate4VC = [[JYNActivate4ViewController alloc]initWithNibName:@"JYNActivate4ViewController" bundle:nil];
    activate4VC.phoneStr = self.phoneStr;
    [self.navigationController pushViewController:activate4VC animated:YES];

}
-(void)createThirdPartUserByPhoneAndPassFailed:(NSString*)errMsg{
    [[WX_Cloud_createThirdPartUserByPhoneAndPass share] setDelegate:nil];
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInView:self.view title:errMsg];

}
@end
