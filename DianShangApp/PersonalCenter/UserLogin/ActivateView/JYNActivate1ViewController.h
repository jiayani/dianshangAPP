//
//  JYNActivate1ViewController.h
//  TianLvApp
//
//  Created by wa on 14-9-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"
@class User;
@interface JYNActivate1ViewController : UIViewController<UITextFieldDelegate,WX_Cloud_Delegate>


@property (strong, nonatomic)  User *currentUser;
@property (readwrite)  BOOL isFromBindPhone;
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdLabel;
@property (weak, nonatomic) IBOutlet UILabel *fourLabel;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UITextField *phoneText;
@property (weak, nonatomic) IBOutlet UIButton *getVerifyCodeBtn;
@property (weak, nonatomic) IBOutlet UILabel *userPhoneLabel;

@property (weak, nonatomic) IBOutlet UITextView *directionsTextView;

- (IBAction)getVerifyBtnPressed:(id)sender;
- (IBAction)textFieldChanged:(id)sender;

@end

