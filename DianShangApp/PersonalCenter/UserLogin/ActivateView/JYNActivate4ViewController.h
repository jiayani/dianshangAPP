//
//  JYNActivate4ViewController.h
//  TianLvApp
//
//  Created by wa on 14-9-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYNActivate4ViewController : UIViewController

@property (strong,nonatomic) NSString *phoneStr;
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdLabel;
@property (weak, nonatomic) IBOutlet UILabel *fourLabel;
@property (weak, nonatomic) IBOutlet UITextView *infomationTextView;

@property (strong, nonatomic) IBOutlet UIButton *okBtn;
- (IBAction)okBtnPressed:(id)sender;

@end

