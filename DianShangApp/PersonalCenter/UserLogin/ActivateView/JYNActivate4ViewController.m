//
//  JYNActivate4ViewController.m
//  TianLvApp
//
//  Created by wa on 14-9-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "JYNActivate4ViewController.h"
#import "WXTabBarViewController.h"
#import "WXCommonViewClass.h"

@interface JYNActivate4ViewController ()

@end

@implementation JYNActivate4ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
     self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        // Custom initialization
        [WXCommonViewClass hideTabBar:self isHiden:YES];
        self.title = NSLocalizedString(@"会员激活", @"会员激活4页面标题");
    }
    return self;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //ios7 导航适配
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    // Do any additional setup after loading the view from its nib.
    [self addBackButton];
    [self initAllControllers];
    
}

- (void)goBack
{
    //单击返回按钮返回到个人中心页面
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload {
    [self setFirstLabel:nil];
    [self setSecondLabel:nil];
    [self setThirdLabel:nil];
    [self setFourLabel:nil];
    [self setOkBtn:nil];
    
    [super viewDidUnload];
}

#pragma mark - All My Methods
/*初始化所有页面显示内容*/
- (void)initAllControllers{
       [self.okBtn setTitle:NSLocalizedString(@"完成", @"完成") forState:UIControlStateNormal];
    [self.infomationTextView setEditable:NO];
     self.infomationTextView.text = [NSString stringWithFormat:@"您的账号已成功激活\n绑定的手机号为%@，祝您使用愉快。",self.phoneStr];
    
    
}
//创建返回按钮
- (void)addBackButton{
    UIButton * backBtn = [WXCommonViewClass getBackButton];
    [backBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftBtnItem;
}

- (IBAction)okBtnPressed:(id)sender {
    //跳转到个人中心页面
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
@end
