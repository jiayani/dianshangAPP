//
//  JYNActivate3ViewController.h
//  TianLvApp
//
//  Created by wa on 14-9-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"

@interface JYNActivate3ViewController : UIViewController<WX_Cloud_Delegate,UITextFieldDelegate>
@property (nonatomic,strong) NSString *phoneStr;
@property (nonatomic,strong) NSString *validCodeStr;
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdLabel;
@property (weak, nonatomic) IBOutlet UILabel *fourLabel;
@property (weak, nonatomic) UITextField *myEditingTextField;
@property (weak, nonatomic) IBOutlet UITextField *myPwdTXt;
@property (weak, nonatomic) IBOutlet UITextField *myPwdAgainTxt;

- (IBAction)okBtnPressed:(id)sender;
@end
