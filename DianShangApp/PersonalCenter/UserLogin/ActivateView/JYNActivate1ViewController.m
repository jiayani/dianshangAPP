//
//  JYNActivate1ViewController.m
//  TianLvApp
//
//  Created by wa on 14-9-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "JYNActivate1ViewController.h"
#import "JYNActivate2ViewController.h"
#import "WXCommonViewClass.h"
#import "WXUserDatabaseClass.h"
#import "WXVerifyMethodsClass.h"
#import "AppDelegate.h"
//#import "WYDGetKeywords.h"
#import "WX_Cloud_IsConnection.h"
#import "MyMD5.h"
#import "FY_Cloud_phoneForQQFirst.h"

@interface JYNActivate1ViewController ()
{
    NSMutableData *mutableData;
}
@end

@implementation JYNActivate1ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];    if (self) {
        self.title = NSLocalizedString(@"会员激活", @"会员激活1页面标题");
        [WXCommonViewClass hideTabBar:self isHiden:YES];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    mutableData = [[NSMutableData alloc]init];
    //ios7 导航适配
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    UIButton * backBtn = [WXCommonViewClass getBackButton];
    [backBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftBtnItem;
    [self initAllControllers];
}

-(void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[FY_Cloud_phoneForQQFirst share] setDelegate:nil];
}

#pragma mark - UITextFieldDelegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)textFieldChanged:(id)sender {
    if (self.phoneText.text.length <= 0 ) {
        self.getVerifyCodeBtn.enabled = NO;
        self.errorLabel.hidden = NO;
        self.errorLabel.text = NSLocalizedString(@"手机号不能为空", @"会员激活1/手机号不能为空");
        
    }else{
        self.getVerifyCodeBtn.enabled = YES;
        
    }
}
#pragma mark - All My Methods
/*初始化所有页面显示内容*/
- (void)initAllControllers{
    self.firstLabel.text = @"填写手机号";
    self.secondLabel.text = NSLocalizedString(@"验证身份", @"会员激活1/验证身份");
    self.thirdLabel.text = NSLocalizedString(@"设置密码", @"会员激活1/验证身份");
    self.fourLabel.text = NSLocalizedString(@"绑定成功", @"会员激活1/绑定成功");
    [self.getVerifyCodeBtn setTitle:NSLocalizedString(@"获取验证码", @"会员激活1/获取验证码") forState:UIControlStateNormal];
    self.phoneText.placeholder = NSLocalizedString(@"请输入您的手机号", @"会员激活1/请输入您的手机号");
    self.directionsTextView.text = NSLocalizedString(@"说明：\n1、绑定您常用的手机号。\n2、绑定手机号可以享受帐号保护、积分返点、商家优惠等服务。", @"会员激活1/会员激活的说明");
    self.directionsTextView.editable = NO;
    self.phoneText.keyboardType = UIKeyboardTypeNumberPad;
    self.errorLabel.hidden = YES;
//    self.currentUser = [WXUserDatabaseClass getCurrentUser];
//    if (self.currentUser != nil) {
//        if (self.currentUser.userPhone != nil && self.currentUser.userPhone.length > 0) {
//            self.userPhoneLabel.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"您绑定的手机号", @"会员激活1/您绑定的手机号"),self.currentUser.userPhone];
//        }else{
//            self.userPhoneLabel.text = [NSString stringWithFormat:@"%@",NSLocalizedString(@"您尚未绑定手机号", @"会员激活1/您尚未绑定手机号")];
//            
//        }
//    }
    self.getVerifyCodeBtn.enabled = NO;
    
    
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    if ([touch view] == self.view) {
        [self.phoneText resignFirstResponder];
    }
}

- (long)date {
    NSDate *datenow = [NSDate date];
    long timSp = (long)[datenow timeIntervalSince1970];
    return timSp;
}

- (IBAction)getVerifyBtnPressed:(id)sender {
    NSString *phoneStr = [self.phoneText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    BOOL isValidatePhone = [WXVerifyMethodsClass isValidatePhone:phoneStr];
    
    if (!isValidatePhone) {
        NSString *alertStr = NSLocalizedString(@"请输入正确的手机号", @"会员激活1/请输入正确的手机号");
        self.errorLabel.hidden = NO;
        self.errorLabel.text = alertStr;
        
    }else{
        self.errorLabel.hidden = YES;
        //本地判断是否连接网络
        BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
        if (!isConnection) {//无网络情况
            NSString *errCode = NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用");
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:errCode delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"找回密码1/确定") otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            //小圈转动等待验证
            [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"等待验证...", @"会员激活1/等待验证...") nav:self.navigationController];
            NSString *time = [NSString stringWithFormat:@"%ld", [self date]];
            NSString *token = [NSString stringWithFormat:@"8ca8188ca3d9b1c8%@", time];
            NSString * tokenMD5 = [MyMD5 md5:token];  //加密
            WXCommonSingletonClass *single = [WXCommonSingletonClass share];
            [[FY_Cloud_phoneForQQFirst share] setDelegate:(id)self];
            [[FY_Cloud_phoneForQQFirst share] checkFirstWithPhone:self.phoneText.text andTime:time andToken:tokenMD5 andOpenID:single.qqOpenID andType:2];
        }
    }
}

-(void)showInfo:(NSString *)titles show_message:(NSString *)messages
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:titles message:messages delegate:nil cancelButtonTitle:@"好" otherButtonTitles:nil, nil];
        [alert show];
    }


#pragma mark - WYDCloudDelegate Methods
//无网络连接
-(void)haveNoNetwork{
   // [[WYDGetKeywords share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    NSString *errorStr = NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用");
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:errorStr delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"确定") otherButtonTitles:nil];
    [alert show];
    //    [WXCommonViewClass showHudInButtonOfView:self.view title:NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用") closeInSecond:3];
}
//意外错误
-(void)accidentError:(NSError*)error{
   // [[WYDGetKeywords share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    NSString *errorStr = NSLocalizedString(@"网络异常", @"网络异常");
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:errorStr delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"确定") otherButtonTitles:nil];
    [alert show];
    
    //    [WXCommonViewClass showHudInButtonOfView:self.view title:errorStr closeInSecond:3];
}

-(void)checkFirstSuccess:(NSString *)message
{
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    JYNActivate2ViewController *secondVC = [[JYNActivate2ViewController alloc] initWithNibName:@"JYNActivate2ViewController" bundle:nil];
    secondVC.phoneStr = self.phoneText.text;
    [self.navigationController pushViewController:secondVC animated:YES];
}

-(void)checkFirstFailed:(NSString*)errMsg
{
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"警告" message:errMsg delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
}


- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:errorCode];
}

@end
