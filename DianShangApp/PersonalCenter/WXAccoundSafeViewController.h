//
//  WXAccoundSafeViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-23.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserInfo.h"

@interface WXAccoundSafeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *dataArray;

}
@property (weak, nonatomic) IBOutlet UITableView *myTableVew;
@property (strong,nonatomic) UserInfo *currentUser;
@property (weak, nonatomic) IBOutlet UIButton *updatePwdBtn;
@property (weak, nonatomic) IBOutlet UIButton *bindPhoneBtn;
@property (weak, nonatomic) IBOutlet UIButton *bindWeiBoBtn;
@property (weak, nonatomic) IBOutlet UIButton *bindWooBtn;
@property (weak, nonatomic) IBOutlet UILabel *bindPhoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *bindWeiBoLabel;
@property (weak, nonatomic) IBOutlet UILabel *bindWooLabel;
@property (weak, nonatomic) IBOutlet UIButton *bindOtherBtn;



- (IBAction)bindPhoneBtnPressed:(id)sender;
- (IBAction)updateBtnPressed:(id)sender;
- (IBAction)bindOtherPressed:(id)sender;
- (IBAction)bindWeiBoBtnPressed:(id)sender;
- (IBAction)bindWooBtnPressed:(id)sender;


@end
