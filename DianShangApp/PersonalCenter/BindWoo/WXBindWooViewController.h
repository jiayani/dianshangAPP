//
//  WXBindWooViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"
#import "MBProgressHUD.h"
@class UserInfo;

@interface WXBindWooViewController : UIViewController<UITextFieldDelegate,WX_Cloud_Delegate,MBProgressHUDDelegate,UIAlertViewDelegate>{
    int alertTag;
    MBProgressHUD *mbProgressHUD;

}
@property (strong,nonatomic) UserInfo *currentUser;
@property (weak, nonatomic) UITextField *myEditingTextField;
@property (weak, nonatomic) IBOutlet UITextField *wooNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *wooPwdTxt;
@property (weak, nonatomic) IBOutlet UIButton *bindWooBtn;
@property (weak, nonatomic) IBOutlet UIButton *downloadCreditsBtn;
@property (weak, nonatomic) IBOutlet UILabel *bindCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *bindedWooNameLabel;
@property (weak, nonatomic) IBOutlet UIView *bindedView;


- (IBAction)bindWooBtnPressed:(id)sender;
- (IBAction)downloadCreditsBtnPressed:(id)sender;
- (void)myAlert:(int)tag;

@end
