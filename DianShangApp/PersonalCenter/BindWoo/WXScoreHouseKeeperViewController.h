//
//  WXScoreHouseKeeperViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface WXScoreHouseKeeperViewController : UIViewController<MBProgressHUDDelegate,UIWebViewDelegate>{
    MBProgressHUD *mbProgressHUD;
}
@property (weak, nonatomic) IBOutlet UIWebView *myWebView;

@end
