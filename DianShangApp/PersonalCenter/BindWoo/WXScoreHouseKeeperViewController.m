//
//  WXScoreHouseKeeperViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXScoreHouseKeeperViewController.h"
#import "WXCommonViewClass.h"
#import "WX_Cloud_IsConnection.h"

@interface WXScoreHouseKeeperViewController ()

@end

@implementation WXScoreHouseKeeperViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"积分管家",@"积分管家");
        [WXCommonViewClass hideTabBar:self isHiden:YES];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initAllControllers];
    
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    mbProgressHUD.delegate = nil;
    mbProgressHUD = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - All My Methods

- (void)initAllControllers{
    /*begin:设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    //end
    
    self.myWebView.backgroundColor = BackgroundColorOfPage;
    [self.myWebView setOpaque:NO];
    
    BOOL isHaveNet = [[WX_Cloud_IsConnection share] isConnectionAvailable];
    if (isHaveNet) {
        //小圈转动等待加载静态页面
        [WXCommonViewClass setActivityWX:YES content:WaitNetConnect nav:self.navigationController];
        /*加载静态页面方法*/
        NSString *htmlStr = @"http://www.591woo.com";
        NSURLRequest *request = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:htmlStr]];
        [self.myWebView loadRequest:request];
    
    }else{
        mbProgressHUD = [WXCommonViewClass showHudInView:self.view title:NoHaveNetwork];
        mbProgressHUD.delegate = self;
    }
}
//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - UIWebViewDelegate Methods

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
}
#pragma mark － MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[self.navigationController popViewControllerAnimated:YES];
}
@end
