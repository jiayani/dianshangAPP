//
//  WXBindWooViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXBindWooViewController.h"
#import "WXCommonViewClass.h"
#import "WX_Cloud_BindWoo.h"
#import "WXUserDatabaseClass.h"
#import "UserInfo.h"
#import "WXScoreHouseKeeperViewController.h"
#import "AppDelegate.h"

@interface WXBindWooViewController ()

@end

@implementation WXBindWooViewController
@synthesize currentUser;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"绑定哇点账号", @"绑定哇点账号标题");
         [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initAllControllers];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self resetValues];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[WX_Cloud_BindWoo share] setDelegate:nil];
    mbProgressHUD.delegate = nil;
    mbProgressHUD = nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setWooNameTxt:nil];
    [self setWooPwdTxt:nil];
    [self setBindWooBtn:nil];
    [self setDownloadCreditsBtn:nil];
    [super viewDidUnload];
}

#pragma mark - UITextFieldDelegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    self.myEditingTextField = textField;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.myEditingTextField = nil;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([self.wooNameTxt isFirstResponder]) {
        [self.wooPwdTxt becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark - UIAlertViewDelegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    int tag = alertView.tag;
    
    if (tag == 98 ) {
        [self.wooNameTxt becomeFirstResponder];
    }else if(tag == 99){
        [self.wooPwdTxt becomeFirstResponder];
    }else if (alertView.tag == 101)
    {
        if (buttonIndex == 1)
        {
            //把哇点账号和密码提交给后台进行服务器端验证
            //小圈转动等待服务器端处理
            NSString *wooName = [self.wooNameTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"正在绑定...", @"绑定哇点账号/正在绑定...") nav:self.navigationController];
            [[WX_Cloud_BindWoo share] setDelegate:(id)self];
            [[WX_Cloud_BindWoo share] bindingWoo:wooName Password:self.wooPwdTxt.text Flag:1];
        }
    }else if (alertView.tag == 105)
    {
        if (buttonIndex == 0)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
}
- (void)myAlert:(int)tag{
    if (alertTag == 98) {
        [self.wooNameTxt becomeFirstResponder];
    }else if (alertTag == 99){
        [self.wooPwdTxt becomeFirstResponder];
    }
}

#pragma mark - All My Methods
- (void)initAllControllers{
    /*设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    //设置导航右侧按钮
    self.downloadCreditsBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:NSLocalizedString(@"积分管家", @"绑定哇点账号/积分管家")];
    [self.downloadCreditsBtn addTarget:self action:@selector(downloadCreditsBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.downloadCreditsBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
    self.wooNameTxt.placeholder = NSLocalizedString(@"请输入哇点账号", @"绑定哇点账号/请输入哇点账号");
    self.wooPwdTxt.placeholder = NSLocalizedString(@"请输入哇点密码", @"绑定哇点账号/绑定哇点账号/请输入哇点密码");
    
    self.bindCountLabel.text = NSLocalizedString(@"哇点账号绑定最多可以更改5次", @"绑定哇点账号/哇点账号绑定最多可以更改5次");
    
}
- (void)resetValues{
    currentUser = [WXUserDatabaseClass getCurrentUser];
    CGRect btnFrame = self.bindWooBtn.frame;
    if ([currentUser.isBindWoo boolValue] == YES) {
        self.bindedView.hidden = NO;
        self.bindedWooNameLabel.text = currentUser.wooUserName;
        [self.bindWooBtn setTitle:@"更改绑定的哇点账号" forState:UIControlStateNormal];
        btnFrame.origin.y = 90;
        self.bindWooBtn.tag = 0;
    }else{
        self.bindedView.hidden = YES;
        [self.bindWooBtn setTitle:NSLocalizedString(@"授权并绑定", @"绑定哇点账号/授权并绑定") forState:UIControlStateNormal];
        btnFrame.origin.y = 175;
        self.bindWooBtn.tag = 1;
        
    }
    [self.bindWooBtn setFrame:btnFrame];
}
//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    if ([touch view] == self.view) {
        if (self.myEditingTextField != nil) {
            [self.myEditingTextField resignFirstResponder];
            
        }
    }
}
//切换密码显示样式
- (IBAction)changePwdShowType:(id)sender {
    if (self.myEditingTextField != nil) {
        [self.myEditingTextField resignFirstResponder];
    }
    UIButton *btn = (UIButton *)sender;
    NSString *imageNameStr = @"showPwdBtnImage.png";
    if (btn.tag == 100) {
        btn.tag = 101;
        imageNameStr = @"unShowPwdBtnImage.png";
        self.wooPwdTxt.secureTextEntry = YES;
        
    }else{
        btn.tag = 100;
        imageNameStr = @"showPwdBtnImage.png";
        self.wooPwdTxt.secureTextEntry = NO;
    }
    [btn setBackgroundImage:[UIImage imageNamed:imageNameStr] forState:UIControlStateNormal];
}

- (IBAction)downloadCreditsBtnPressed:(id)sender {
    WXScoreHouseKeeperViewController *houseKeeper = [[WXScoreHouseKeeperViewController alloc]initWithNibName:@"WXScoreHouseKeeperViewController" bundle:nil];
    [self.navigationController pushViewController:houseKeeper animated:YES];
}

- (IBAction)bindWooBtnPressed:(id)sender {
    [self.myEditingTextField resignFirstResponder];
    if (self.bindWooBtn.tag == 0) {
        CGRect btnFrame = self.bindWooBtn.frame;
        self.bindedView.hidden = YES;
        [self.bindWooBtn setTitle:NSLocalizedString(@"授权并绑定", @"绑定哇点账号/授权并绑定") forState:UIControlStateNormal];
        btnFrame.origin.y = 175;
        self.bindWooBtn.tag = 1;
        [self.bindWooBtn setFrame:btnFrame];
    }else{
    
        if (self.wooNameTxt.text.length <= 0){
            mbProgressHUD = [WXCommonViewClass showHudInView:self.view title:NSLocalizedString(@"账号不能为空", @"登录/账号不能为空")];
            mbProgressHUD.delegate = self;
            alertTag = 98;
        }else if(self.wooPwdTxt.text.length <=0){
            mbProgressHUD = [WXCommonViewClass showHudInView:self.view title:NSLocalizedString(@"密码不能为空", @"登录/密码不能为空")];
            mbProgressHUD.delegate = self;
            alertTag = 99;
            
        }else{
            //把哇点账号和密码提交给后台进行服务器端验证
            //小圈转动等待服务器端处理
            NSString *wooName = [self.wooNameTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"正在绑定...", @"绑定哇点账号/正在绑定...") nav:self.navigationController];
            [[WX_Cloud_BindWoo share] setDelegate:(id)self];
            [[WX_Cloud_BindWoo share] bindingWoo:wooName Password:self.wooPwdTxt.text Flag:0];
            
        }
    }
    
}

#pragma mark - DataBase Controller Methods
/*修改数据库中的哇点账号和密码*/
- (void)updateWooInfo{
    if (currentUser != nil) {
        currentUser.wooUserName = self.wooNameTxt.text;
        currentUser.wooPassword = self.wooPwdTxt.text;
        currentUser.isBindWoo = [NSNumber numberWithBool:YES];
    }
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context=[appDelegate managedObjectContext];
    [context save:nil];
}
#pragma mark － MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[self myAlert:alertTag];
}

#pragma mark - WX_Cloud_Delegate Methods
//无网络连接/意外错误
- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[WX_Cloud_BindWoo share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}

//绑定哇点账号成功或失败
-(void)bindWooSuccess:(NSDictionary*)dataDic{
    [[WX_Cloud_BindWoo share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    //验证成功，修改本地数据库信息
    [self updateWooInfo];
    int index = self.navigationController.viewControllers.count - 2;
    UIViewController *presendVC = [self.navigationController.viewControllers objectAtIndex:index];
    [WXCommonViewClass showHudInButtonOfView:presendVC.view title:@"绑定成功" closeInSecond:3];
    [self.navigationController popViewControllerAnimated:YES];
//    int changeBindingCount = [[dataDic objectForKey:@"change_count"] intValue];
//    NSString * tips1 = NSLocalizedString(@"您还可以进行", @"更改绑定账号次数提示");
//    NSString * tips2 = NSLocalizedString(@"次更改绑定操作", @"更改绑定账号次数提示");
//    NSString * tips = [NSString stringWithFormat:@"%@ %d %@",tips1,5 - changeBindingCount,tips2];
//    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"提示" message:tips delegate:self cancelButtonTitle:@"确认" otherButtonTitles: nil];
//    alert.tag = 105;
//    [alert show];

}
-(void)bindWooFailed:(NSString*)errCode Flag:(int)flag{
    [[WX_Cloud_BindWoo share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    if (flag == 102) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:errCode delegate:self cancelButtonTitle:NSLocalizedString(@"取消", @"取消") otherButtonTitles:NSLocalizedString(@"确定", @"确定"),nil];
        alert.tag = 101;
        alert.delegate = self;
        [alert show];
    }else{
        //验证失败，弹框提示
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"警告", @"警告") message:errCode delegate:self cancelButtonTitle:NSLocalizedString(@"确定", @"确定") otherButtonTitles:nil];
        [alert show];

    }
   
}
@end

