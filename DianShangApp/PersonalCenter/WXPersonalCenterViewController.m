 //
//  WXPersonalCenterViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-4-22.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXPersonalCenterViewController.h"
#import "WXCommonViewClass.h"
#import "WXUserDatabaseClass.h"
#import "CustomNavigationController.h"
#import "WXUserDatabaseClass.h"
#import "FYNewMoreViewController.h"
#import "WXAccoundSafeViewController.h"
#import "WXUserInfoViewController.h"
#import "FYMyOrderViewController.h"
#import "WX_Cloud_SignAndSendCredits.h"
#import "WXShoppingCartViewController.h"
#import "FYjifenInfoViewController.h"
#import "FYjifenChangeViewController.h"
#import "WXUserLoginViewController.h"
#import "WXUpdatePwdViewController.h"
#import "XYManageAddressViewController.h"
#import "FYCollectViewController.h"
#import "FYLeaveMessageViewController.h"
#import "AppDelegate.h"
#import "WXCommonSingletonClass.h"
#import "WXMyCardViewController.h"
#import "WX_Cloud_UserCountInfo.h"
#import "MKNumberBadgeView.h"
#import "WX_Cloud_UserLogin.h"
#import "WX_Cloud_thirdPartyLogging_syncUserInfo.h"
#import "JYN_cloud_loginnumber.h"
#import "WXUnionShopListViewController.h"
#define numberOfSections 7
@interface WXPersonalCenterViewController ()

@end

@implementation WXPersonalCenterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"个人中心", @"个人中心页面标题");
        [WXCommonViewClass hideTabBar:self isHiden:NO];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addRightBtnOfNavigation];
    [self initAllControllers];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //begin
    [self.myTableView  setContentOffset:CGPointMake(0, 0) animated:NO];
    [self.headView addSubview:self.noLoginView];
    
    self.currentUser = nil;
    self.cancelLoginBtnView.hidden = YES;
    [self setValueOfBadgetView:0 CartNum:self.waitPayOfProductView];
    [self setValueOfBadgetView:0 CartNum:self.waitSendOfProductView];
    [self setValueOfBadgetView:0 CartNum:self.waitReciveOfProduct];
    [self setValueOfBadgetView:0 CartNum:self.waitCommetnOfProduct];
    [self setValueOfBadgetView:0 CartNum:self.waitSendOfGiftView];
    [self setValueOfBadgetView:0 CartNum:self.waitReciveOfGiftView];
    //end
    self.currentUser = [WXUserDatabaseClass getCurrentUser];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    /** 用户信息同步
     *  1.如果是第三方登录，则调用第三方登录接口同步用户信息
     *  2.否则调用普通用户登录接口同步用不信息
     */
    if (singletonClass.isThirdPartyLogging && singletonClass.currentUserId != nil) {
        [[WX_Cloud_thirdPartyLogging_syncUserInfo share] setDelegate:(id)self];
        [[WX_Cloud_thirdPartyLogging_syncUserInfo share] currentUserId];
        [self sendLoginNumberPress];//三方登录后再次进入程序时向后台发送登陆次数
    }else if (singletonClass.isThirdPartyLogging == NO && self.currentUser != nil){//现有积分，累计积分初始化
        [[WX_Cloud_UserLogin share] setDelegate:(id)self];
//        //手机的
       [[WX_Cloud_UserLogin share] userLoginWithUserName:singletonClass.myLastLoginName UserPassword:self.currentUser.userPassword];
        [self sendLoginNumberPress];//正常登录后再次进入程序时向后台发送登陆次数
        
        
    }else{
        self.cancelLoginBtnView.hidden = YES;
    }

    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (self.currentUser != nil) {
        //begin:注册赠送的积分
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        if (singletonClass.integralFromRegister > 0) {
            NSString *titleStr = [NSString stringWithFormat:@"注册成功，赠送您%ld积分",singletonClass.integralFromRegister];
            [WXCommonViewClass showHudInButtonOfView:self.view title:titleStr closeInSecond:3];
            singletonClass.integralFromRegister = 0;
        }
        //end
    }
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[WX_Cloud_SignAndSendCredits share]setDelegate:nil];
    [[WX_Cloud_UserCountInfo share]setDelegate:nil];
    [[WX_Cloud_UserLogin share] setDelegate:nil];
    progressHUD.delegate = nil;
    progressHUD = nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    self.myTableView = nil;
    self.signinBtn = nil;
    self.nickNameBtn = nil;
    self.integarLabel = nil;
    self.cardLeveNameBtn = nil;
    self.productOrderBtn = nil;
    self.giftOrderBtn = nil;
    self.cancelLoginBtnView = nil;
    self.productView = nil;
    self.giftView = nil;
    self.productAndOrderItemImageView = nil;
    
    // Dispose of any resources that can be recreated.

}
#pragma mark - My All Methods
- (void)syncGetAllDatas{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    [[WX_Cloud_UserCountInfo share] setDelegate:(id)self];
    [[WX_Cloud_UserCountInfo share] syncGetUserCountInfo:singletonClass.currentUserId];
}
//跳转到登陆页面
- (IBAction)goToLoginVC:(id)sender{
    WXUserLoginViewController *lognVC = [[WXUserLoginViewController alloc]initWithNibName:@"WXUserLoginViewController" bundle:nil];
    lognVC.isShowToolBar = NO;
    [self.navigationController pushViewController:lognVC animated:YES];
    
}
- (void)initAllControllers{
    
    NSString *siginStr = NSLocalizedString(@"签到", @"个人中心/签到");
    [self.signinBtn setTitle:siginStr forState:UIControlStateNormal];
    NSArray *tempArray0 = [[NSArray alloc]initWithObjects:
                  NSLocalizedString(@"全部订单", @"个人中心/全部订单") ,nil];
    
    NSArray *tempArray1;
    tempArray1 = [[NSArray alloc]initWithObjects:
                  NSLocalizedString(@"积分明细", @"个人中心/积分明细"),
                  NSLocalizedString(@"积分互换", @"个人中心/积分互换") ,
                  NSLocalizedString(@"账户安全", @"个人中心/账户安全"),nil];
    
    NSArray *tempArray2 = [[NSArray alloc]initWithObjects:
                           NSLocalizedString(@"我的收藏", @"个人中心/我的收藏"),
                           NSLocalizedString(@"留言消息", @"个人中心/留言消息"),nil];
    NSArray *tempArray3 = [[NSArray alloc]initWithObjects:
                           NSLocalizedString(@"管理收货地址", @"个人中心/管理收货地址"),nil];
    NSArray *tempArray4 = [[NSArray alloc]initWithObjects:
                           NSLocalizedString(@"积分同盟", @"个人中心/积分同盟"),nil];
    
    titleArray = [[NSMutableArray alloc]initWithObjects:tempArray0,tempArray1,tempArray2,tempArray3, tempArray4,nil];
    NSArray *tempImageArray0 = [[NSArray alloc]initWithObjects:@"allOrderBtn.png", nil];
    NSArray *tempImageArray1 = [[NSArray alloc]initWithObjects:@"integeralInfoBtn.png",@"integaralChangeBtn.png",@"accoundSafeBtn.png", nil];
    NSArray *tempImageArray2 = [[NSArray alloc]initWithObjects:@"favorableBtn.png",@"newsBtn.png", nil];
    NSArray *tempImageArray3 = [[NSArray alloc]initWithObjects:@"addressManageBtn.png", nil];
    NSArray *tempImageArray4 = [[NSArray alloc]initWithObjects:@"IntegralUnionBtn.png", nil];
    
    backgroundImageBtnArray = [[NSMutableArray alloc]initWithObjects:tempImageArray0,tempImageArray1,tempImageArray2,tempImageArray3,tempImageArray4, nil];
    
    //设置按钮样式
    self.cancelLoginBtnView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.myTableView.frame.size.width, 150)];
    self.cancelLoginBtnView.backgroundColor = [UIColor clearColor];
    UIButton *cancelLoginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelLoginBtn.frame = CGRectMake(13, 0, 295, 40);
    [cancelLoginBtn setBackgroundImage:[UIImage imageNamed:@"addContaBtn.png"] forState:UIControlStateNormal];
    [cancelLoginBtn setBackgroundImage:[UIImage imageNamed:@"addContaBtncl.png"] forState:UIControlStateHighlighted];
    [cancelLoginBtn addTarget:self action:@selector(cancelLoginBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cancelLoginBtn setTitle:NSLocalizedString(@"注销", @"个人中心/注销") forState:UIControlStateNormal];
    [self.cancelLoginBtnView addSubview:cancelLoginBtn];
    
    NSArray *titleOfProductOrderBtnArray = [[NSArray alloc]initWithObjects:NSLocalizedString(@"待付款", @"个人中心/待付款"),
        NSLocalizedString(@"待发货", @"个人中心/待发货"),
        NSLocalizedString(@"待收货", @"个人中心/待收货"),
        NSLocalizedString(@"待评价", @"个人中心/待评价"),nil];
    //设置按钮样式
    UIEdgeInsets edgetInsetsOfOrderInfo = UIEdgeInsetsMake(40, 0, 0, 0);
    for (int i = 0 ;i < titleOfProductOrderBtnArray.count ;i++) {
        UIButton *tempBtn = (UIButton *)[self.productOrderBtn objectAtIndex:i];
        NSString *tempStr = [titleOfProductOrderBtnArray objectAtIndex:i];
        [tempBtn setTitle:tempStr  forState:UIControlStateNormal];
        [tempBtn setTitleEdgeInsets:edgetInsetsOfOrderInfo];
        
    }
    UIButton *tempBtn1 = (UIButton *)[self.giftOrderBtn objectAtIndex:0];
    UIButton *tempBtn2 = (UIButton *)[self.giftOrderBtn objectAtIndex:1];
     [tempBtn1 setTitle:[titleOfProductOrderBtnArray objectAtIndex:1]  forState:UIControlStateNormal];
    [tempBtn2 setTitle:[titleOfProductOrderBtnArray objectAtIndex:2]  forState:UIControlStateNormal];
    [tempBtn1 setTitleEdgeInsets:edgetInsetsOfOrderInfo];
    [tempBtn2 setTitleEdgeInsets:edgetInsetsOfOrderInfo];

    //设置UIScrollView可以滑动
    if (iPhone5) {
        //使用scrollView很关键的地方，设置contentSize，一般是安你的scrollView的size来设置，后面的height可以适当多个300～400
         self.myTableView.contentSize = CGSizeMake(320, self.myTableView.frame.size.height + 300.0f);
        
    }else{
         self.myTableView.contentSize = CGSizeMake(320, self.myTableView.frame.size.height + 320.0f);
    }
    //默认选中的是商品订单
    [self productOrderItemBtnPressed:nil];
   

}
- (void)userNewDatasInit:(BOOL)isNetError{
    
    if (self.currentUser != nil && isNetError == NO) {
        
        self.cancelLoginBtnView.hidden = NO;
        [self.noLoginView removeFromSuperview];
        [self syncGetAllDatas];
        NSString *nickNameStr = self.currentUser.nickName;
        if (nickNameStr == nil || nickNameStr.length <= 0) {
            nickNameStr = self.currentUser.nickName;
        }
        [self.nickNameBtn setTitle:nickNameStr forState:UIControlStateNormal];
    }else{
        self.cancelLoginBtnView.hidden = YES;
        [self.headView addSubview:self.noLoginView];
    }
}
- (void)addRightBtnOfNavigation{
    //设置导航右侧按钮
    UIButton *moreBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:NSLocalizedString(@"更多", @"个人中心/更多")];
    [moreBtn addTarget:self action:@selector(moreBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:moreBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}
- (void)moreBtnPressed{
    FYNewMoreViewController *moreViewController = [[FYNewMoreViewController alloc]initWithNibName:@"FYNewMoreViewController" bundle:nil];
    [self.navigationController pushViewController:moreViewController animated:YES];
}

- (IBAction)userInfoBtnPressed:(id)sender {
    if (self.currentUser == nil) {
        [self goToLoginVC:nil];
    }else{
        WXUserInfoViewController *userInfoViewController = [[WXUserInfoViewController alloc]initWithNibName:@"WXUserInfoViewController" bundle:nil];
        [self.navigationController pushViewController:userInfoViewController animated:YES];
    }
}

- (IBAction)productOrderItemBtnPressed:(id)sender {
    
    if (self.isProductOrderItemSelected) {
        return;
    }
    self.productAndOrderItemImageView.image = [UIImage imageNamed:@"productOrder.png"];
    self.productView.hidden = NO;
    self.giftView.hidden = YES;
    self.isProductOrderItemSelected = YES;
    
    
}

- (IBAction)giftOrderItemPressed:(id)sender {
    
    if (self.isProductOrderItemSelected) {
        self.productAndOrderItemImageView.image = [UIImage imageNamed:@"giftOrder.png"];
        self.productView.hidden = YES;
        self.giftView.hidden = NO;
        self.isProductOrderItemSelected = NO;
    }
    

}

- (IBAction)productOrderBtnPressed:(id)sender
{
    if (self.currentUser == nil) {
        [self goToLoginVC:nil];
    }else{
        UIButton *btn = (UIButton *)sender;
        FYMyOrderViewController *myOrderVC = [[FYMyOrderViewController alloc]initWithNibName:@"FYMyOrderViewController" bundle:nil];
        switch (btn.tag) {
            case 1:
                myOrderVC.status = 2;
                myOrderVC.style = 1;
                [self.navigationController pushViewController:myOrderVC animated:YES];
                break;
            case 2:
                myOrderVC.status = 1;
                myOrderVC.style = 1;
                [self.navigationController pushViewController:myOrderVC animated:YES];
                break;
            case 3:
                myOrderVC.status = 3;
                myOrderVC.style = 1;
                [self.navigationController pushViewController:myOrderVC animated:YES];
                break;
            case 4:
                myOrderVC.status = 4;
                myOrderVC.style = 1;
                [self.navigationController pushViewController:myOrderVC animated:YES];
                break;
                
            default:
                break;
        }
    }
}

- (IBAction)giftOrderBtnPressed:(id)sender
{
    if (self.currentUser == nil) {
        [self goToLoginVC:nil];
    }else{
        UIButton *btn = (UIButton *)sender;
        FYMyOrderViewController *myOrderVC = [[FYMyOrderViewController alloc]initWithNibName:@"FYMyOrderViewController" bundle:nil];
        switch (btn.tag) {
            case 5:
                myOrderVC.status = 2;
                myOrderVC.style = 2;
                [self.navigationController pushViewController:myOrderVC animated:YES];
                break;
            case 6:
                myOrderVC.status = 1;
                myOrderVC.style = 2;
                [self.navigationController pushViewController:myOrderVC animated:YES];
                break;
                
            default:
                break;
        }
    }
}

- (IBAction)integralBtnPressed:(id)sender {
    if (self.currentUser == nil) {
        [self goToLoginVC:nil];
    }else{
        FYjifenInfoViewController *FYjifenVC = [[FYjifenInfoViewController alloc] initWithNibName:@"FYjifenInfoViewController" bundle:nil];
        [self.navigationController pushViewController:FYjifenVC animated:YES];
    }
}



- (IBAction)signBtnPressed:(id)sender {
    [[WX_Cloud_SignAndSendCredits share] setDelegate:(id)self];
    [[WX_Cloud_SignAndSendCredits share] signAndSendCredits];
}

- (IBAction)allOrderBtnPressed:(id)sender {
    if (self.currentUser == nil) {
        [self goToLoginVC:nil];
    }else{
        FYMyOrderViewController *myOrderVC = [[FYMyOrderViewController alloc]initWithNibName:@"FYMyOrderViewController" bundle:nil];
        myOrderVC.status = -1;
        myOrderVC.style = 1;
        [self.navigationController pushViewController:myOrderVC animated:YES];
    }
}

- (IBAction)integralChangeBtnPressed:(id)sender {
    if (self.currentUser == nil) {
        [self goToLoginVC:nil];
    }else{
        FYjifenChangeViewController *changeVC = [[FYjifenChangeViewController alloc] initWithNibName:@"FYjifenChangeViewController" bundle:nil];
        [self.navigationController pushViewController:changeVC animated:YES];
    }
}


- (IBAction)accountSafeBtnPressed:(id)sender {
    if (self.currentUser == nil) {
       // [self goToLoginVC:nil];
        WXAccoundSafeViewController *accoundSafeVC = [[WXAccoundSafeViewController alloc]initWithNibName:@"WXAccoundSafeViewController" bundle:nil];
        accoundSafeVC.currentUser = self.currentUser;
        [self.navigationController pushViewController:accoundSafeVC animated:YES];
    }else{
        WXAccoundSafeViewController *accoundSafeVC = [[WXAccoundSafeViewController alloc]initWithNibName:@"WXAccoundSafeViewController" bundle:nil];
        accoundSafeVC.currentUser = self.currentUser;
        [self.navigationController pushViewController:accoundSafeVC animated:YES];
    }
}

- (IBAction)myFavoriteBtnPressed:(id)sender {
    if (self.currentUser == nil) {
        [self goToLoginVC:nil];
    }else{
        FYCollectViewController *FYCollectVC = [[FYCollectViewController alloc]initWithNibName:@"FYCollectViewController" bundle:nil];
        [self.navigationController pushViewController:FYCollectVC animated:YES];
    }
}

//- (IBAction)leaveMessageAndNewsBtnPressed:(id)sender
//{
//    if (self.currentUser == nil) {
//        [self goToLoginVC:nil];
//    }else{
//        FYLeaveMessageViewController *leaveVC = [[FYLeaveMessageViewController alloc] initWithNibName:@"FYLeaveMessageViewController" bundle:nil];
//        [self.navigationController pushViewController:leaveVC animated:YES];
//    }
//}

- (IBAction)addressMangeBtnPressed:(id)sender {
    if (self.currentUser == nil) {
        [self goToLoginVC:nil];
    }else{
        XYManageAddressViewController *AddressVC = [[XYManageAddressViewController alloc]initWithNibName:@"XYManageAddressViewController" bundle:nil];
        [self.navigationController pushViewController:AddressVC animated:YES];
    }
}

- (IBAction)integralUnionBtnPressed:(id)sender {
    WXUnionShopListViewController *unionShopListViewController = [[WXUnionShopListViewController alloc]initWithNibName:@"WXUnionShopListViewController" bundle:nil];
    [self.navigationController pushViewController:unionShopListViewController animated:YES];
}
//注销用户
- (IBAction)cancelLoginBtnPressed:(id)sender {
    [WXUserDatabaseClass setCurrentUserKey:nil];
    [self deleteSingle];
    [self deleteUserTime];
    [self goToLoginVC:nil];
}

// 付岩-添加注销时清空用户数据时间
- (void)deleteSingle
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.addressDic = [[NSDictionary alloc] init];
    singletonClass.sendDic = [[NSDictionary alloc] init];
    singletonClass.payMent = @"";

}

- (void)deleteUserTime
{
    [self localStoData:0 key:@"pushTime"];
    [self localStoData:0 key:@"opinionTime"];
    [self localStoData:0 key:@"reviewTime"];
    [self localStoData:0 key:@"infomationTime"];
}

- (void)localStoData:(long)object key:(NSString *)key{
    NSNumber *longNumber = [NSNumber numberWithLong:object];
    NSString *longStr = [longNumber stringValue];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:longStr forKey:key];
    [defaults synchronize];
}
//--------------------------

- (IBAction)cardLevelBtnPressed:(id)sender {
    WXMyCardViewController *myCardVC = [[WXMyCardViewController alloc]initWithNibName:@"WXMyCardViewController" bundle:nil];
    [self.navigationController pushViewController:myCardVC animated:YES];
}
-(void)setValueOfBadgetView:(NSInteger)newNum CartNum:(MKNumberBadgeView *)cartNum{

    cartNum.hideWhenZero = YES;
    cartNum.value = newNum;
    cartNum.shadow = NO;
   
}
- (void)reloadUI:(NSDictionary *)dataDic{
    [self.cardLeveNameBtn setTitle:[dataDic objectForKey:@"vip_card_level"] forState:UIControlStateNormal];
    self.integarLabel.text = [NSString stringWithFormat:@"现有积分: %@ 累计积分: %@",[dataDic objectForKey:@"current_integral"],[dataDic objectForKey:@"total_integral"]];
    int product_wait_pay =[[dataDic objectForKey:@"product_wait_pay"]intValue];
    int product_wait_send =[[dataDic objectForKey:@"product_wait_send"]intValue];
    int product_wait_receive =[[dataDic objectForKey:@"product_wait_receive"]intValue];
    int product_wait_comment =[[dataDic objectForKey:@"product_wait_comment"]intValue];
    int gift_wait_send =[[dataDic objectForKey:@"gift_wait_send"]intValue];
    int gift_wait_receive =[[dataDic objectForKey:@"gift_wait_receive"]intValue];
    
    [self setValueOfBadgetView:product_wait_pay CartNum:self.waitPayOfProductView];
    [self setValueOfBadgetView:product_wait_send CartNum:self.waitSendOfProductView];
    [self setValueOfBadgetView:product_wait_receive CartNum:self.waitReciveOfProduct];
    [self setValueOfBadgetView:product_wait_comment CartNum:self.waitCommetnOfProduct];
    [self setValueOfBadgetView:gift_wait_send CartNum:self.waitSendOfGiftView];
    [self setValueOfBadgetView:gift_wait_receive CartNum:self.waitReciveOfGiftView];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0 || section == numberOfSections - 1) {
        return 1;
    }else{
        int numberOfRows = ((NSArray *)[titleArray objectAtIndex:section - 1]).count;

        return numberOfRows;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d%d", [indexPath section], [indexPath row]];//以indexPath来唯一确定cell
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault
                                     reuseIdentifier:CellIdentifier];
    }
    //设置选中时没有背景色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.section == 0) {
        [cell addSubview:self.headView];
    }else if (indexPath.section == numberOfSections - 1){
        cell.backgroundColor = [UIColor clearColor];
        [cell addSubview:self.cancelLoginBtnView];
    }else{
        UIView *myView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 45)];
        myView.backgroundColor = [UIColor clearColor];
        NSArray *tmepImageArray = [backgroundImageBtnArray objectAtIndex:indexPath.section - 1];
        NSString *imageNameStr = [tmepImageArray objectAtIndex:indexPath.row];
        UIImage *myImage = [UIImage imageNamed:imageNameStr];
        UIImageView *myImageView = [[UIImageView alloc]initWithFrame:myView.frame];
        myImageView.image = myImage;
        UIButton *myBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        myBtn.frame = myView.frame;
        myBtn.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        [myBtn setTitleColor:[UIColor colorWithRed:43.0f/255.0f green:43.0f/255.0f blue:43.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
        NSArray *tmepArray = [titleArray objectAtIndex:indexPath.section - 1];
        NSString *titleStr = [tmepArray objectAtIndex:indexPath.row];
        [myBtn setTitle:titleStr forState:UIControlStateNormal];
        myBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        UIEdgeInsets edgetInsets = UIEdgeInsetsMake(0, 50, 0, 0);
        [myBtn setTitleEdgeInsets:edgetInsets];
        [myView setUserInteractionEnabled:NO];
        [myBtn setUserInteractionEnabled:NO];
        [myView addSubview:myImageView];
        [myView addSubview:myBtn];
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        if (indexPath.section == 2 && indexPath.row == 1) {
            if (singletonClass.isChange == NO) {
                UILabel *tempOpenLabel = [[UILabel alloc]initWithFrame:CGRectMake(195, 11, 93, 21)];
                tempOpenLabel.textColor = [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1.0f];
                tempOpenLabel.textAlignment = NSTextAlignmentRight;
                tempOpenLabel.font = [UIFont systemFontOfSize:15.0f];
                tempOpenLabel.text = NSLocalizedString(@"本商家未开通", @"本商家未开通");
                [myView addSubview:tempOpenLabel];

            }
        }
        
        [cell addSubview:myView];
        
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    int row = indexPath.row;
    switch (indexPath.section) {
        case 1:
        {
            if (row == 0) {
                [self allOrderBtnPressed:nil];
            }
        }
            break;
        case 2:
        {
            if (row == 0) {
                [self integralBtnPressed:nil];
            }else if (row == 1){
                if (singletonClass.isChange) {
                    [self integralChangeBtnPressed:nil];
                }
            }else if (row == 2){
                [self accountSafeBtnPressed:nil];
            }
        }
            break;
        case 3:
        {
            if (row == 0) {
                [self myFavoriteBtnPressed:nil];
            }else if (row == 1){
                [self leaveMessageAndNewsBtnPressed:nil];
            }
        }
            break;
        case 4:
        {
            if (row == 0) {
                [self addressMangeBtnPressed:nil];
            }
        }
            break;
        case 5:
        {
            if (row == 0) {
                if (singletonClass.isChange) {
                    [self integralUnionBtnPressed:nil];
                }
            }
        }
            break;
        default:
            break;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 208.0f;
    }else if (indexPath.section == numberOfSections - 1){
        return 100;
    }else{
        return 45.0f;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section <= 1) {
        return 0.0f;
    }else{
        return 20.0f;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* myView = [[UIView alloc] init];
    myView.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0f];
    myView.layer.borderWidth = 0;
    return myView;
}
#pragma mark － MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	//重新获得用户数据
    [self syncGetAllDatas];
}

#pragma mark - WX_Cloud_Delegate Methods
//无网络和意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[WX_Cloud_UserLogin share] setDelegate:nil];
    [[WX_Cloud_SignAndSendCredits share] setDelegate:nil];
    [[WX_Cloud_UserCountInfo share] setDelegate:nil];
    [WXCommonViewClass setActivityWX:NO content:nil View:self.view];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
    [self userNewDatasInit:YES];
}

- (void)signAndSendCreditsSuccess:(NSString*)message{
    [[WX_Cloud_SignAndSendCredits share] setDelegate:nil];
    
    progressHUD = [WXCommonViewClass showHudInButtonOfView:self.view title:message closeInSecond:3];
    
    progressHUD.delegate = self;
}
//获得个人中心用户相关的数据信息
-(void)syncGetUserCountInfoSuccess:(NSDictionary *)dataDic{
    [[WX_Cloud_UserCountInfo share] setDelegate:nil];
    [WXCommonViewClass setActivityWX:NO content:nil View:self.view];
    //刷新界面
    [WXUserDatabaseClass addUserextroInfo:self.currentUser dataDic:dataDic];
    [self reloadUI:dataDic];
}
-(void)syncGetUserCountInfoFailed:(NSString *)errCode{
    [[WX_Cloud_UserCountInfo share] setDelegate:nil];
    [WXCommonViewClass setActivityWX:NO content:nil View:self.view];
    [WXCommonViewClass showHudInView:self.view title:errCode];
}
//用户登陆
-(void)userLoginSuccess:(NSMutableDictionary*)databaseDictionary{
    [[WX_Cloud_UserLogin share] setDelegate:nil];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.currentUser = nil;
    [databaseDictionary setObject:self.currentUser.userPassword forKey:@"password"];
    [WXUserDatabaseClass addUser:databaseDictionary];
    self.currentUser = [WXUserDatabaseClass getCurrentUser];
    [self userNewDatasInit:NO];
    

}

-(void)userLoginFailed:(NSString*)errCode{
    //注销当前用户
    [[WX_Cloud_UserLogin share] setDelegate:nil];
    [WXUserDatabaseClass setCurrentUserKey:nil];
    self.currentUser = [WXUserDatabaseClass getCurrentUser];
    [self userNewDatasInit:NO];
}
#pragma mark -  第三方登录后同步用户信息

-(void)syncGetUserInfoByThirdPartyLoggingSuccess:(NSMutableDictionary *)data{
    [[WX_Cloud_thirdPartyLogging_syncUserInfo share] setDelegate:nil];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.currentUser = nil;
    [WXUserDatabaseClass addUser:data];
    self.currentUser = [WXUserDatabaseClass getCurrentUser];
    [self userNewDatasInit:NO];
}
-(void)syncGetUserInfoByThirdPartyLoggingFailed:(NSString*)errMsg{
    [[WX_Cloud_thirdPartyLogging_syncUserInfo share] setDelegate:nil];
    [WXUserDatabaseClass setCurrentUserKey:nil];
    self.currentUser = [WXUserDatabaseClass getCurrentUser];
    [self userNewDatasInit:NO];
}

//向后台发送登录次数接口
-(void)sendLoginNumberPress
{
    WXCommonSingletonClass *singletionClass = [WXCommonSingletonClass share];
    if (singletionClass.isCloseFromBack ==NO) {
        /*登录成功后给后台发送接口以便后台进行登录次数的统计*/
        [[JYN_cloud_loginnumber share]setDelegate:nil];
        [[JYN_cloud_loginnumber share]sendNumberToBackground];
        singletionClass.isCloseFromBack = YES;
    }
}

@end
