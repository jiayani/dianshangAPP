//
//  WXUserDatabaseClass.h
//  TianLvApp
//
//  Created by 霞 王 on 13-7-12.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UserInfo;


@interface WXUserDatabaseClass : NSObject
/*添加一条用户信息*/
+ (void)addUser:(NSMutableDictionary *)dic;

/*
 *注册的时候，如果本地有需要删除本地的，如果没有需要添加到本地数据库中
 */
+ (void)registerUser:(NSMutableDictionary *)dic;

/*把当前登录的用户的主键保存到NSUserDefaults中*/
+ (void)setCurrentUserKey:(NSString *)currentUserKey;

/*得到当前用户的主键*/
+ (NSString *)getCurrentUserKey;

/*得到当前用户*/
+ (UserInfo *)getCurrentUser;

/*保存上一次登陆的用户名*/
+ (void)setLastTimeUserKey:(NSString *)LastTimeUserKey;

+ (NSString *)getLastTimeUserKey;

/*删除用户标中的所有数据*/
+ (BOOL)deleteUsers;

/*绑定手机号成功后 需要修改本地数据库*/
+ (void)updatePhoneOfUser:(UserInfo *)user phoneStr:(NSString *)phoneStr;
/*绑定邮箱成功后，需要修改本地数据库*/
+ (void)updateEmailOfUser:(UserInfo *)user emailStr:(NSString *)emailStr;

/*修改密码*/
+ (void)updatePwdOfUser:(UserInfo *)user pwdStr:(NSString *)pwdStr;

//跳转到个人中心页面
+ (void)goToPersonVC:(UIViewController *)tempVC;

//从数据库中读取所有的用户中心页面的相关数据
+ (NSDictionary *)getUserCountInfo:(UserInfo *)user;

//添加到数据库中
+ (void)addUserextroInfo:(UserInfo *)user dataDic:(NSDictionary *)dataDic;
@end
