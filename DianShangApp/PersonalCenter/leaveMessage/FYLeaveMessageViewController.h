//
//  FYLeaveMessageViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-6-5.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYLeaveMessageViewController : UIViewController
- (IBAction)btn_opinionClick:(id)sender;
- (IBAction)btn_pushClick:(id)sender;
- (IBAction)btn_reviewClick:(id)sender;
- (IBAction)btn_infomationClick:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_first;
@property (weak, nonatomic) IBOutlet UILabel *lbl_for;
@property (weak, nonatomic) IBOutlet UILabel *lbl_sec;
@property (weak, nonatomic) IBOutlet UILabel *lbl_thi;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_first;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_second;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_third;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_four;

@end
