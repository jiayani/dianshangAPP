//
//  FYOpinionViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-6-5.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableFooterView.h"
@interface FYOpinionViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIScrollViewDelegate, EGORefreshTableDelegate>
@property (weak, nonatomic) IBOutlet UITableView *chatTableView;
@property (weak, nonatomic) IBOutlet UITextField *messageTextField;
@property (weak, nonatomic) IBOutlet UIView *messageView;
- (IBAction)sendMessage_Click:(id)sender;
@property (nonatomic, retain) NSMutableArray *chatArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
