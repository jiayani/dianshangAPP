//
//  FYMyReviewViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-6-11.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableFooterView.h"
#import "WX_Cloud_Delegate.h"

@interface FYMyReviewViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, EGORefreshTableDelegate,WX_Cloud_Delegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
