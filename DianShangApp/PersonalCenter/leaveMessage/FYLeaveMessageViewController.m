//
//  FYLeaveMessageViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-6-5.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYLeaveMessageViewController.h"
#import "FYOpinionViewController.h"
#import "WXCommonViewClass.h"
#import "FYInfomationViewController.h"
#import "FYMyReviewViewController.h"
#import "WXBusinessPushNotificViewController.h"
#import "FY_Cloud_getNumber.h"
#import "XYLoadingView.h"
#import "WXUserDatabaseClass.h"
@interface FYLeaveMessageViewController ()
{
    XYLoadingView *loadingView;
}
@end

@implementation FYLeaveMessageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"留言消息", @"留言消息标题");
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    self.imageView_first.hidden = YES;
    self.imageView_second.hidden = YES;
    self.imageView_third.hidden = YES;
    self.imageView_four.hidden = YES;
    
    
}
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self getNum];
}

- (void)getNum
{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
    if ([user objectForKey:@"pushTime"] == nil) {
        [self localStoData:0 key:@"pushTime"];
    }
    if ([user objectForKey:@"opinionTime"] == nil) {
        [self localStoData:0 key:@"opinionTime"];
    }
    if ([user objectForKey:@"reviewTime"] == nil) {
        [self localStoData:0 key:@"reviewTime"];
    }
    if ([user objectForKey:@"infomationTime"] == nil) {
        [self localStoData:0 key:@"infomationTime"];
    }
    [[FY_Cloud_getNumber share] setDelegate:(id)self];
    [[FY_Cloud_getNumber share] getNumberWithSuggestDate:[user objectForKey:@"opinionTime"] andPushMsgDate:[user objectForKey:@"pushTime"] andDiscussDate:[user objectForKey:@"reviewTime"] andConsultDate:[user objectForKey:@"infomationTime"]];
    
}

//意见反馈
- (IBAction)btn_opinionClick:(id)sender
{
    UserInfo *user = [WXUserDatabaseClass getCurrentUser];
    if (user == nil) {
        [WXCommonViewClass goToLogin:self.navigationController];
        return;
    }
    FYOpinionViewController *opinionVC = [[FYOpinionViewController alloc] initWithNibName:@"FYOpinionViewController" bundle:nil];
    [self.navigationController pushViewController:opinionVC animated:YES];
}
//商品推送
- (IBAction)btn_pushClick:(id)sender
{
    WXBusinessPushNotificViewController *businessPushNotificViewController = [[WXBusinessPushNotificViewController alloc]initWithNibName:@"WXBusinessPushNotificViewController" bundle:nil];
    long time = [self date];
    [self localStoData:time key:@"pushTime"];
    [self.navigationController pushViewController:businessPushNotificViewController animated:YES];
    
}
//商品评论
- (IBAction)btn_reviewClick:(id)sender
{
    UserInfo *user = [WXUserDatabaseClass getCurrentUser];
    if (user == nil) {
        [WXCommonViewClass goToLogin:self.navigationController];
        return;
    }
    FYMyReviewViewController *myReview = [[FYMyReviewViewController alloc] initWithNibName:@"FYMyReviewViewController" bundle:nil];
    long time = [self date];
    [self localStoData:time key:@"reviewTime"];
    [self.navigationController pushViewController:myReview animated:YES];
}
//商品咨询
- (IBAction)btn_infomationClick:(id)sender
{
    UserInfo *user = [WXUserDatabaseClass getCurrentUser];
    if (user == nil) {
        [WXCommonViewClass goToLogin:self.navigationController];
        return;
    }
    FYInfomationViewController *infoVC = [[FYInfomationViewController alloc] initWithNibName:@"FYInfomationViewController" bundle:nil];
    infoVC.pushViewController = self;
    long time = [self date];
    [self localStoData:time key:@"infomationTime"];
    [self.navigationController pushViewController:infoVC animated:YES];
}

- (void)localStoData:(long)object key:(NSString *)key{
    NSNumber *longNumber = [NSNumber numberWithLong:object];
    NSString *longStr = [longNumber stringValue];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:longStr forKey:key];
    [defaults synchronize];
}
- (long)date {
    NSDate *datenow = [NSDate date];
//    NSTimeZone *zone = [NSTimeZone  timeZoneWithName:@"Asia/Shanghai"];
//    //    NSTimeZone *zone = [NSTimeZone  systemTimeZone];
//    NSInteger interval = [zone secondsFromGMTForDate:datenow];
//    NSDate *localeDate = [datenow  dateByAddingTimeInterval: interval];
    long timSp = (long)[datenow timeIntervalSince1970];
    return timSp;
}

- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}

//获得消息提示成功
-(void)syncGetNumberSuccess:(NSDictionary *)data
{
    [loadingView removeFromSuperview];
    if ([[[data objectForKey:@"suggestNum"] stringValue]isEqualToString:@"0"] || [data objectForKey:@"suggestNum"] == nil) {
        self.lbl_first.hidden = YES;
        self.imageView_first.hidden = YES;
    }else
    {
        self.imageView_first.hidden = NO;
        if ([[data objectForKey:@"suggestNum"] intValue] > 99) {
            
            self.lbl_first.text = @"99";
        }else{
            self.lbl_first.text = [[data objectForKey:@"suggestNum"] stringValue];
        }
    }
    
    if ([[[data objectForKey:@"pushMsgNum"] stringValue]isEqualToString:@"0"] || [data objectForKey:@"pushMsgNum"] == nil) {
        self.lbl_sec.hidden = YES;
        self.imageView_second.hidden = YES;
    }else
    {
        self.imageView_second.hidden = NO;
        if ([[data objectForKey:@"pushMsgNum"] intValue] > 99) {
            self.lbl_sec.text = @"99";
        }else{
        self.lbl_sec.text = [[data objectForKey:@"pushMsgNum"]stringValue];
        }
    }
    
    if ([[[data objectForKey:@"discussNum"] stringValue]isEqualToString:@"0"] || [data objectForKey:@"discussNum"] == nil) {
        self.lbl_thi.hidden = YES;
        self.imageView_third.hidden = YES;
    }else
    {
        self.imageView_third.hidden = NO;
        if ([[data objectForKey:@"discussNum"] intValue] > 99) {
            self.lbl_thi.text = @"99";
        }else{
            self.lbl_thi.text = [[data objectForKey:@"discussNum"] stringValue];
        }
    }
    
    if ([[[data objectForKey:@"consultNum"] stringValue]isEqualToString:@"0"] || [data objectForKey:@"consultNum"] == nil) {
        self.lbl_for.hidden = YES;
        self.imageView_four.hidden = YES;
    }else
    {
        self.imageView_four.hidden = NO;
        self.lbl_for.hidden = NO;
        if ([[data objectForKey:@"consultNum"] intValue] > 99) {
            self.lbl_for.text = @"99";
        }else{
            self.lbl_for.text = [[data objectForKey:@"consultNum"] stringValue];
        }
    }
}

-(void)syncGetNumberFailed:(NSString*)errMsg
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errMsg];
}

@end
