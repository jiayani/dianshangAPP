//
//  FYInfomation2Cell.h
//  DianShangApp
//
//  Created by Fuy on 14-6-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYInfomation2Cell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *textView_que;
@property (weak, nonatomic) IBOutlet UITextView *textView_ans;
@property (weak, nonatomic) IBOutlet UILabel *lbl_product;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_product;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;
@property (weak, nonatomic) IBOutlet UIButton *btn_delete;

@end
