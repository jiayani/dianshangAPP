//
//  FYReview2Cell.h
//  DianShangApp
//
//  Created by Fuy on 14-6-10.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AsynImageView;
@interface FYReview2Cell : UITableViewCell
@property (weak, nonatomic) IBOutlet AsynImageView *imageView_product;
@property (weak, nonatomic) IBOutlet UILabel *lbl_producy;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;
@property (weak, nonatomic) IBOutlet UITextView *textView_que;
@property (weak, nonatomic) IBOutlet UITextView *textView_ans;
@property (weak, nonatomic) IBOutlet UIButton *btn_delete;

@end
