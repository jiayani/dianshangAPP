//
//  FYOpinionViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-6-5.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYOpinionViewController.h"
#import "ChatCustomCell.h"
#import "WXCommonViewClass.h"
#import "FY_Cloud_GetOpinion.h"
#import "FY_Cloud_SetOpinion.h"
#import "WX_Cloud_Delegate.h"
#import "WX_Cloud_IsConnection.h"
#import "XYNoDataView.h"
#define BEGIN_FLAG @"[/"
#define END_FLAG @"]"
#define KFacialSizeWidth  18
#define KFacialSizeHeight 18
#define MAX_WIDTH 145
#define maxCountOfOnePage 8;
@interface FYOpinionViewController ()
{
    NSMutableArray *opinionArray;
    int page;
    BOOL isHaveMore;
    BOOL reloading;
    EGORefreshTableFooterView *refreshFooterView;
}
@end

@implementation FYOpinionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"意见反馈", @"意见反馈标题");
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    self.chatArray = [[NSMutableArray alloc] init];
    page = 1;
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    //监听键盘高度的变换
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.chatTableView addGestureRecognizer:tapGestureRecognizer];
    opinionArray = [[NSMutableArray alloc] init];
    [self initRefreshFooterView];
    [self getData];
    
}
- (void)initRefreshFooterView{
    
    int height = MAX(self.tableView.bounds.size.height, self.tableView.contentSize.height);
    refreshFooterView = [[EGORefreshTableFooterView alloc]  initWithFrame:CGRectZero];
    refreshFooterView.frame =CGRectMake(0.0f, height, self.view.frame.size.width, self.tableView.bounds.size.height);
    //如果contentsize的高度比表的高度小，那么就需要把刷新视图放在表的bounds的下面
    
    refreshFooterView.delegate = self;
    //下拉刷新的控件添加在tableView上
    
    [self.tableView addSubview:refreshFooterView];
    reloading = NO;
    
}
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[FY_Cloud_GetOpinion share] setDelegate:nil];
}

-(void)syncGetOpinionSuccess:(NSDictionary *)dataDic
{
    if (opinionArray.count == 0) {
        opinionArray = [NSMutableArray arrayWithArray:[dataDic objectForKey:@"list"]];
    }else{
        [opinionArray addObjectsFromArray:[dataDic objectForKey:@"list"]] ;
    }
    if ([[dataDic objectForKey:@"list"] count] == 8) {
        isHaveMore = YES;
    }else{
        isHaveMore = NO;
    }
    
    self.chatArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < opinionArray.count; i++) {
        
        NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:(long)[[[opinionArray objectAtIndex:i] objectForKey:@"suggest_date"]longLongValue]];
        NSTimeZone *zone = [NSTimeZone  timeZoneWithName:@"Asia/Shanghai"];
        NSInteger interval = [zone secondsFromGMTForDate:startDate];
        startDate = [startDate  dateByAddingTimeInterval: interval];
        
        [self.chatArray addObject:startDate];
        UIView *chatView = [self bubbleView:[NSString stringWithFormat:@"%@", [[opinionArray objectAtIndex:i] objectForKey:@"suggest_content"]] from:YES];
        [self.chatArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@", [[opinionArray objectAtIndex:i] objectForKey:@"suggest_content"]], @"text", @"self", @"speaker", chatView, @"view", nil]];
        
        //查看是否有商家回复，有则添加
        if ([[opinionArray objectAtIndex:i] objectForKey:@"replay_content"] != nil) {
            //商家回复时间
            NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:(long)[[[opinionArray objectAtIndex:i] objectForKey:@"replay_date"]longLongValue]];
            NSTimeZone *zone = [NSTimeZone  timeZoneWithName:@"Asia/Shanghai"];
            NSInteger interval = [zone secondsFromGMTForDate:startDate];
            startDate = [startDate  dateByAddingTimeInterval: interval];
            [self.chatArray addObject:startDate];
            
            //商家回复内容
            UIView *chatView = [self bubbleView:[NSString stringWithFormat:@"%@", [[opinionArray objectAtIndex:i] objectForKey:@"replay_content"]] from:NO];
            [self.chatArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@", [[opinionArray objectAtIndex:i] objectForKey:@"replay_content"]], @"text", @"shop", @"speaker", chatView, @"view", nil]];
            
        }
        
        
    }
    if ([dataDic objectForKey:@"time"] != nil) {
            NSString *longStr = [[dataDic objectForKey:@"time"] stringValue];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:longStr forKey:@"opinionTime"];
            [defaults synchronize];
    }
    //刷新UI
    [self reloadUI];
    
}
-(void)syncGetOpinionFailed:(NSString*)errCode
{
    //[WXCommonViewClass showHudInView:self.view title:errCode];
    if (page == 1) {
        [WXCommonViewClass showHudInView:self.view title:@"亲，您尚未留言，请留下您的宝贵意见吧"];
    }
    
}

- (void)getData
{
    [[FY_Cloud_GetOpinion share] setDelegate:(id)self];
    [[FY_Cloud_GetOpinion share] getOpinion:page];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    
}
#pragma mark 发表意见反馈
- (IBAction)sendMessage_Click:(id)sender
{
	if ([self.messageTextField.text isEqualToString:@""] || self.messageTextField.text == nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入内容" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    [[FY_Cloud_SetOpinion share] setDelegate:(id)self];
    [[FY_Cloud_SetOpinion share] setOpinion:self.messageTextField.text];
    page = 1;
    opinionArray = [[NSMutableArray alloc] init];
    [self getData];
    //	NSMutableString *sendString=[NSMutableString stringWithCapacity:100];
    //	[sendString appendString:self.messageTextField.text];
    //
    //	// 发送后生成泡泡显示出来
    //    NSDate *datenow = [NSDate date];
    //    NSTimeZone *zone = [NSTimeZone  timeZoneWithName:@"Asia/Shanghai"];
    //    //    NSTimeZone *zone = [NSTimeZone  systemTimeZone];
    //    NSInteger interval = [zone secondsFromGMTForDate:datenow];
    //    NSDate *localeDate = [datenow  dateByAddingTimeInterval: interval];

    //    [self.chatArray addObject:localeDate];
    //	UIView *chatView = [self bubbleView:[NSString stringWithFormat:@"%@", self.messageTextField.text] from:YES];
    //	[self.chatArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:self.messageTextField.text, @"text", @"self", @"speaker", chatView, @"view", nil]];
    //
    //
    //	[self.chatTableView reloadData];
    //	[self.chatTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.chatArray count]-1 inSection:0]
    //							  atScrollPosition: UITableViewScrollPositionBottom
    //									  animated:YES];
    [self.messageTextField resignFirstResponder];
    self.messageTextField.text = @"";
    
}
//意见发送后回调
-(void)syncSetOpinionSuccess:(NSString *)message
{

}
-(void)syncSetOpinionFailed:(NSString*)errCode
{

}

-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}
//键盘高度监听通知
#pragma mark Responding to keyboard events
- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    [self autoMovekeyBoard:keyboardRect.size.height];
}


- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    [self autoMovekeyBoard:0];
}
- (void)autoMovekeyBoard:(float)h{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
	self.messageView.frame = CGRectMake(0.0f, (float)(self.view.frame.size.height - h - 46.0), self.messageView.frame.size.width, self.messageView.frame.size.height);
    if (!iOS7) {
        self.messageView.frame = CGRectMake(0.0f, (float)(self.view.frame.size.height - h - 46.0), self.messageView.frame.size.width, self.messageView.frame.size.height);
    }
    if (!iPhone5) {
        self.messageView.frame = CGRectMake(0.0f, (float)(self.view.frame.size.height - h - 46.0), self.messageView.frame.size.width, self.messageView.frame.size.height);
    }
    [self.view bringSubviewToFront:self.messageView];
	self.chatTableView.frame = CGRectMake(0.0f, 0.0f, 320.0f,(float)(self.view.frame.size.height - h - 18.0));
    [UIView commitAnimations];
}
//键盘回落
- (void)singleTap:(UITapGestureRecognizer*)recognizer
{
    [self.messageTextField resignFirstResponder];
}
#pragma mark Table View DataSource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.chatArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if ([[self.chatArray objectAtIndex:[indexPath row]] isKindOfClass:[NSDate class]]) {
		return 30;
	}else {
		UIView *chatView = [[self.chatArray objectAtIndex:[indexPath row]] objectForKey:@"view"];
		return chatView.frame.size.height+10;
	}
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    static NSString *CommentCellIdentifier = @"CommentCell";
	ChatCustomCell *cell = (ChatCustomCell*)[tableView dequeueReusableCellWithIdentifier:CommentCellIdentifier];
	if (cell == nil) {
		cell = [[[NSBundle mainBundle] loadNibNamed:@"ChatCustomCell" owner:self options:nil] lastObject];
	}
	
	if ([[self.chatArray objectAtIndex:[indexPath row]] isKindOfClass:[NSDate class]]) {
		NSDateFormatter  *formatter = [[NSDateFormatter alloc] init];
		[formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Etc/UTC"]];
		NSMutableString *timeString = [NSMutableString stringWithFormat:@"%@",[formatter stringFromDate:[self.chatArray objectAtIndex:[indexPath row]]]];
        cell.dateLabel.font = [UIFont systemFontOfSize:12];
		[cell.dateLabel setText:timeString];
        
	}else {
		// Set up the cell...
		NSDictionary *chatInfo = [self.chatArray objectAtIndex:[indexPath row]];
		UIView *chatView = [chatInfo objectForKey:@"view"];
		[cell.contentView addSubview:chatView];
	}
    return cell;
}

/*
 生成泡泡UIView
 */
#pragma mark -
#pragma mark Table view methods
- (UIView *)bubbleView:(NSString *)text from:(BOOL)fromSelf {
	
    UIView *returnView =  [self assembleMessageAtIndex:text from:fromSelf];
    returnView.backgroundColor = [UIColor clearColor];
    UIView *cellView = [[UIView alloc] initWithFrame:CGRectZero];
    cellView.backgroundColor = [UIColor clearColor];
    
	UIImage *bubble = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fromSelf?@"bubbleSelf":@"bubble" ofType:@"png"]];
	UIImageView *bubbleImageView = [[UIImageView alloc] initWithImage:[bubble stretchableImageWithLeftCapWidth:20 topCapHeight:14]];
    
    if(fromSelf){
        returnView.frame= CGRectMake(9.0f, 15.0f, returnView.frame.size.width, returnView.frame.size.height);
        bubbleImageView.frame = CGRectMake(0.0f, 14.0f, returnView.frame.size.width+24.0f, returnView.frame.size.height+24.0f );
        cellView.frame = CGRectMake(315.0f-bubbleImageView.frame.size.width, 0.0f,bubbleImageView.frame.size.width+50.0f, bubbleImageView.frame.size.height+30.0f);
        
    }
	else{
        returnView.frame= CGRectMake(27.0f, 16.0f, returnView.frame.size.width, returnView.frame.size.height);
        bubbleImageView.frame = CGRectMake(10.0f, 14.0f, returnView.frame.size.width+24.0f, returnView.frame.size.height+24.0f);
		cellView.frame = CGRectMake(0.0f, 0.0f, bubbleImageView.frame.size.width+30.0f,bubbleImageView.frame.size.height+30.0f);
        
    }
    
    
    
    [cellView addSubview:bubbleImageView];
    [cellView addSubview:returnView];
    
	return cellView;
    
}
//判断是否有表情
-(void)getImageRange:(NSString*)message :(NSMutableArray*)array {
    //筛选字符串
    NSRange range=[message rangeOfString: BEGIN_FLAG];
    NSRange range1=[message rangeOfString: END_FLAG];
    
    if (range.length>0 && range1.length>0) {
        if (range.location > 0) {
            //表情替换
            [array addObject:[message substringToIndex:range.location]];
            [array addObject:[message substringWithRange:NSMakeRange(range.location, range1.location+1-range.location)]];
            NSString *str=[message substringFromIndex:range1.location+1];
            [self getImageRange:str :array];
        }else {
            NSString *nextstr=[message substringWithRange:NSMakeRange(range.location, range1.location+1-range.location)];
            //排除文字是“”的
            if (![nextstr isEqualToString:@""]) {
                [array addObject:nextstr];
                NSString *str=[message substringFromIndex:range1.location+1];
                [self getImageRange:str :array];
            }else {
                return;
            }
        }
        
    } else if (message != nil) {
        [array addObject:message];
    }
}


-(UIView *)assembleMessageAtIndex:(NSString *)message from:(BOOL)fromself
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    [self getImageRange:message :array];
    UIView *returnView = [[UIView alloc] initWithFrame:CGRectZero];
    NSArray *data = array;
    UIFont *fon = [UIFont systemFontOfSize:14.0f];
    CGFloat upX = 0;
    CGFloat upY = 0;
    CGFloat X = 0;
    CGFloat Y = 0;
    if (data) {
        for (int i=0;i < [data count];i++) {
            NSString *str=[data objectAtIndex:i];
            if ([str hasPrefix: BEGIN_FLAG] && [str hasSuffix: END_FLAG])
            {
                if (upX >= MAX_WIDTH)
                {
                    upY = upY + KFacialSizeHeight;
                    upX = 0;
                    X = 150;
                    Y = upY;
                }
                NSString *imageName=[str substringWithRange:NSMakeRange(2, str.length - 3)];
                UIImageView *img=[[UIImageView alloc]initWithImage:[UIImage imageNamed:imageName]];
                img.frame = CGRectMake(upX, upY, KFacialSizeWidth, KFacialSizeHeight);
                [returnView addSubview:img];
                upX=KFacialSizeWidth+upX;
                if (X<150) X = upX;
                
                
            } else {
                for (int j = 0; j < [str length]; j++) {
                    NSString *temp = [str substringWithRange:NSMakeRange(j, 1)];
                    if (upX >= MAX_WIDTH)
                    {
                        upY = upY + KFacialSizeHeight;
                        upX = 0;
                        X = 155;
                        Y =upY;
                    }
                    CGSize size=[temp sizeWithFont:fon constrainedToSize:CGSizeMake(145, 40)];
                    UILabel *la = [[UILabel alloc] initWithFrame:CGRectMake(upX,upY,size.width,size.height)];
                    la.font = fon;
                    la.text = temp;
                    la.backgroundColor = [UIColor clearColor];
                    if (fromself) {
                        la.textColor = [UIColor whiteColor];
                    }else{
                        la.textColor = [UIColor colorWithRed:43.0f/255.0f green:43.0f/255.0f blue:43.0f/255.0f alpha:1];
                    }
                    
                    [returnView addSubview:la];
                    upX=upX+size.width;
                    if (X<145) {
                        X = upX;
                    }
                }
            }
        }
    }
    returnView.frame = CGRectMake(15.0f,2.0f, X, Y); // 需要将该view的尺寸记下，方便以后使用
    return returnView;
}

-(void)requestProductList{
    [self getData];
}

#pragma mark - 下拉刷新数据方法
//请求数据
-(void)requestData
{
    reloading = YES;
    BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
    if (isConnection) {//有网络情况
        
        //每次进入该页面的时候，重新从服务器端读取数据
        if(refreshFooterView.hasMoreData)
        {
            
            int myCount = opinionArray.count;
            int pageCount = myCount/maxCountOfOnePage;
            page += pageCount;
            //下载团购信息
            [self requestProductList];
            
        }
    }else{//无网络情况
        [WXCommonViewClass showHudInButtonOfView:self.view title:NoHaveNetwork closeInSecond:2];
    }
    
}

-(void)reloadUI
{
    reloading = NO;
    //停止下拉的动作,恢复表格的便宜
	[refreshFooterView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    //更新界面
    [self.tableView reloadData];
    
    [self setRefreshViewFrame];
    
}

-(void)setRefreshViewFrame
{
    //如果contentsize的高度比表的高度小，那么就需要把刷新视图放在表的bounds的下面
    int height = MAX(self.tableView.bounds.size.height, self.tableView.contentSize.height);
    refreshFooterView.frame =CGRectMake(0.0f, height, self.view.frame.size.width, self.tableView.bounds.size.height);
}

#pragma mark - EGORefreshTableFooterDelegate
//出发下拉刷新动作，开始拉取数据
- (void)egoRefreshTableDidTriggerRefresh:(EGORefreshPos)aRefreshPos
{
    [self requestData];
}


//返回当前刷新状态：是否在刷新
- (BOOL)egoRefreshTableDataSourceIsLoading:(UIView*)view{
	
	return reloading; // should return if data source model is reloading
	
}
//返回刷新时间
// if we don't realize this method, it won't display the refresh timestamp
- (NSDate*)egoRefreshTableDataSourceLastUpdated:(UIView*)view
{
	
	return [NSDate date]; // should return date data source was last changed
	
}

#pragma mark - UIScrollView

//此代理在scrollview滚动时就会调用
//在下拉一段距离到提示松开和松开后提示都应该有变化，变化可以在这里实现
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    refreshFooterView.hasMoreData = isHaveMore;
    [refreshFooterView egoRefreshScrollViewDidScroll:scrollView];
}
//松开后判断表格是否在刷新，若在刷新则表格位置偏移，且状态说明文字变化为loading...
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    refreshFooterView.hasMoreData = isHaveMore;
    [refreshFooterView egoRefreshScrollViewDidEndDragging:scrollView];
}

@end
