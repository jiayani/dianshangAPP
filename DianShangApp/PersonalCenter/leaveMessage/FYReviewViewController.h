//
//  FYReviewViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-6-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FYReviewCell.h"
#import "EGORefreshTableFooterView.h"
#import "XYLoadingView.h"
#import "XYNoDataView.h"
@interface FYReviewViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,EGORefreshTableDelegate>
{
    int page;
    NSMutableArray * infoArray;
    FYReviewCell *cell;
    BOOL isHaveMore;
    BOOL reloading;
    EGORefreshTableFooterView *refreshFooterView;
    XYLoadingView * loadingView;
    XYNoDataView * noDataView;
}
@property (weak, nonatomic) IBOutlet UILabel *evaluateValueCountLabel;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@property (nonatomic, retain) NSString *productID;
@property (assign, nonatomic) float evaluateValue;
@property (assign, nonatomic) NSString * evaluateValueCount;
@end
