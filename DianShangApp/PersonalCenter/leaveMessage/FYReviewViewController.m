//
//  FYReviewViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-6-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYReviewViewController.h"
#import "WXCommonViewClass.h"
#import "WX_Cloud_Delegate.h"
#import "WX_Cloud_IsConnection.h"
#import "FYReviewCell.h"
#import "FY_Cloud_getReview.h"
#import "FYReviewCell.h"
#define maxCountOfOnePage 8;
@interface FYReviewViewController ()

@end

@implementation FYReviewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"商品评价", @"商品评价标题");
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    UIView * starView = [WXCommonViewClass getStarView:self.evaluateValue :110 :20];
    [self.view addSubview:starView];
    
    self.evaluateValueCountLabel.text = self.evaluateValueCount;
    
    page = 1;
    [self requestGetReview];
    [self initRefreshFooterView];
}

- (void)initRefreshFooterView{
    
    int height = MAX(self.mainTableView.bounds.size.height, self.mainTableView.contentSize.height);
    refreshFooterView = [[EGORefreshTableFooterView alloc]  initWithFrame:CGRectZero];
    refreshFooterView.frame =CGRectMake(0.0f, height, self.view.frame.size.width, self.mainTableView.bounds.size.height);
    //如果contentsize的高度比表的高度小，那么就需要把刷新视图放在表的bounds的下面
    
    refreshFooterView.delegate = self;
    //下拉刷新的控件添加在tableView上
    
    [self.mainTableView addSubview:refreshFooterView];
    reloading = NO;
    
}
-(void)requestGetReview
{
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
    [[FY_Cloud_getReview share] setDelegate:(id)self];
    [[FY_Cloud_getReview share] getReview:page :self.productID];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[FY_Cloud_getReview share] setDelegate:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

-(void)syncGetReviewSuccess:(NSDictionary *)data
{
    [loadingView removeFromSuperview];
    if (infoArray.count == 0) {
        infoArray = [NSMutableArray arrayWithArray:[data objectForKey:@"data"]];
    }else{
        [infoArray addObjectsFromArray:[data objectForKey:@"data"]];
    }
    
    int hasMore = [[data  objectForKey:@"hasMore"]intValue];
    if (hasMore == 1) {
        isHaveMore = YES;
    }else{
        isHaveMore = NO;
    }
    
    //刷新UI
    [self reloadUI];
    
}
-(void)syncGetReviewFailed:(NSString*)errMsg
{
    noDataView =[[XYNoDataView alloc]initWithFrame:self.view.frame];
    noDataView.megUpLabel.text = @"暂时没有数据";
    noDataView.megDownLabel.text = @"先去其他页面看看吧";
    [self.view addSubview:noDataView];
    [loadingView removeFromSuperview];
    [[FY_Cloud_getReview share] setDelegate:nil];
}

//无网络连接或意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark UITableView delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return infoArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell = (FYReviewCell *)[[[NSBundle  mainBundle]  loadNibNamed:@"FYReviewCell" owner:self options:nil]  lastObject];
    cell.commentContentLabel.text = [[infoArray objectAtIndex:indexPath.row]objectForKey:@"commentContent"];
    cell.replyLabel.text = [[infoArray objectAtIndex:indexPath.row]objectForKey:@"reply_evaluate"];
    
    return 70.0f+[self size:cell.commentContentLabel].height + [self size:cell.replyLabel].height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CommentCellIdentifier = @"CommentCell";
    
    cell = (FYReviewCell*)[tableView dequeueReusableCellWithIdentifier:CommentCellIdentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"FYReviewCell" owner:self options:nil] lastObject];
    }
    
    cell.commentContentLabel.text = [[infoArray objectAtIndex:indexPath.row]objectForKey:@"commentContent"];
    cell.commentNameLabel.text = [[infoArray objectAtIndex:indexPath.row]objectForKey:@"commentName"];
    cell.replyLabel.text = [[infoArray objectAtIndex:indexPath.row]objectForKey:@"reply_evaluate"];
    //时间解析
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:(long)[[[infoArray objectAtIndex:indexPath.row] objectForKey:@"commentTime"]longLongValue]];
    NSTimeZone *zone = [NSTimeZone  timeZoneWithName:@"Asia/Shanghai"];
    NSInteger interval = [zone secondsFromGMTForDate:startDate];
    startDate = [startDate  dateByAddingTimeInterval: interval];
    NSDateFormatter  *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Etc/UTC"]];
    NSMutableString *timeString = [NSMutableString stringWithFormat:@"%@",[formatter stringFromDate:startDate]];
    cell.commentTimeLabel.text = timeString;
    
    CGRect rect;
    
    rect = cell.commentContentLabel.frame;
    rect.size.height += [self size:cell.commentContentLabel].height;
    [cell.commentContentLabel setFrame:rect];
    
    rect = cell.replyLabel.frame;
    rect.origin.y += [self size:cell.commentContentLabel].height;
    [cell.replyLabel setFrame:rect];
    
    rect = cell.replyLabel.frame;
    rect.size.height += [self size:cell.replyLabel].height;
    [cell.replyLabel setFrame:rect];
    
    rect = cell.commentNameLabel.frame;
    rect.origin.y += [self size:cell.commentContentLabel].height + [self size:cell.replyLabel].height;
    [cell.commentNameLabel setFrame:rect];
    
    rect = cell.commentTimeLabel.frame;
    rect.origin.y += [self size:cell.commentContentLabel].height + [self size:cell.replyLabel].height;
    [cell.commentTimeLabel setFrame:rect];
    
    rect = cell.singLine.frame;
    rect.origin.y += [self size:cell.commentContentLabel].height + [self size:cell.replyLabel].height;
    [cell.singLine setFrame:rect];
    return cell;
}
-(CGSize)size:(UILabel * )label{
    CGSize size = [label.text sizeWithFont:label.font
                         constrainedToSize:CGSizeMake(label.frame.size.width, 10000)];
    return size;
}

#pragma mark - 下拉刷新数据方法
//请求数据
-(void)requestData
{
    reloading = YES;
    BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
    if (isConnection) {//有网络情况
        
        //每次进入该页面的时候，重新从服务器端读取数据
        if(refreshFooterView.hasMoreData)
        {
            
            int myCount = infoArray.count;
            int pageCount = myCount/maxCountOfOnePage;
            page = pageCount + 1;
            
            [self requestGetReview];
            
        }
    }else{//无网络情况
        [WXCommonViewClass showHudInButtonOfView:self.view title:NoHaveNetwork closeInSecond:2];
    }
    
}

-(void)reloadUI
{
    reloading = NO;
    //停止下拉的动作,恢复表格的便宜
	[refreshFooterView egoRefreshScrollViewDataSourceDidFinishedLoading:self.mainTableView];
    //更新界面
    [self.mainTableView reloadData];
    [self setRefreshViewFrame];
    
}

-(void)setRefreshViewFrame
{
    //如果contentsize的高度比表的高度小，那么就需要把刷新视图放在表的bounds的下面
    int height = MAX(self.mainTableView.bounds.size.height, self.mainTableView.contentSize.height);
    refreshFooterView.frame =CGRectMake(0.0f, height, self.view.frame.size.width, self.mainTableView.bounds.size.height);
}

#pragma mark - EGORefreshTableFooterDelegate
//出发下拉刷新动作，开始拉取数据
- (void)egoRefreshTableDidTriggerRefresh:(EGORefreshPos)aRefreshPos
{
    [self requestData];
}


//返回当前刷新状态：是否在刷新
- (BOOL)egoRefreshTableDataSourceIsLoading:(UIView*)view{
	
	return reloading; // should return if data source model is reloading
	
}
//返回刷新时间
// if we don't realize this method, it won't display the refresh timestamp
- (NSDate*)egoRefreshTableDataSourceLastUpdated:(UIView*)view
{
	
	return [NSDate date]; // should return date data source was last changed
	
}

#pragma mark - UIScrollView

//此代理在scrollview滚动时就会调用
//在下拉一段距离到提示松开和松开后提示都应该有变化，变化可以在这里实现
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    refreshFooterView.hasMoreData = isHaveMore;
    [refreshFooterView egoRefreshScrollViewDidScroll:scrollView];
}
//松开后判断表格是否在刷新，若在刷新则表格位置偏移，且状态说明文字变化为loading...
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    refreshFooterView.hasMoreData = isHaveMore;
    [refreshFooterView egoRefreshScrollViewDidEndDragging:scrollView];
}


@end
