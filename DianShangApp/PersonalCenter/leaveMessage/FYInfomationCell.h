//
//  FYInfomationCell.h
//  DianShangApp
//
//  Created by Fuy on 14-6-6.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYInfomationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *textView_que;
@property (weak, nonatomic) IBOutlet UITextView *textView_ans;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_que;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_ans;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;

@end
