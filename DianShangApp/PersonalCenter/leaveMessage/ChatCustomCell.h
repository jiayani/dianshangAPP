//
//  FYOpinionViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-6-5.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatCustomCell : UITableViewCell{
	UILabel *dateLabel;
}

@property (nonatomic, retain) IBOutlet UILabel *dateLabel;

@end
