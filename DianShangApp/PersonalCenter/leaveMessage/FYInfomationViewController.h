//
//  FYInfomationViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-6-6.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableFooterView.h"
#import "WX_Cloud_Delegate.h"
@interface FYInfomationViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,EGORefreshTableDelegate, UIAlertViewDelegate,WX_Cloud_Delegate>
@property (weak, nonatomic) UIViewController *pushViewController;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextView *textView_info;
- (IBAction)btn_infoClick:(id)sender;
- (IBAction)btn_setClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *makeView;
@property (nonatomic) int productId;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UIButton *btn_info;
- (float)heightForTextView:(UITextView *)textView andText:(NSString *)strText;
@end
