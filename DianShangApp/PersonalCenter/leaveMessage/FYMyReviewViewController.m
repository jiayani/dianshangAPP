//
//  FYMyReviewViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-6-11.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYMyReviewViewController.h"
#import "FYReview2Cell.h"
#import "WXCommonViewClass.h"
#import "WX_Cloud_Delegate.h"
#import "WX_Cloud_IsConnection.h"
#import "FY_Cloud_getMyReview.h"
#import "XYNoDataView.h"
#import "XYLoadingView.h"
#import "AsynImageView.h"
#import "FY_Cloud_deleteReview.h"
#define maxCountOfOnePage 8;
@interface FYMyReviewViewController ()
{
    NSMutableArray *infoArray;
    NSMutableArray *queArray;
    NSMutableArray *ansArray;
    FYReview2Cell *cell1;
    int page;
    BOOL isHaveMore;
    BOOL reloading;
    EGORefreshTableFooterView *refreshFooterView;
    XYLoadingView *loadingView;
    int btnTag;
}
@end

@implementation FYMyReviewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"商品评价", @"我的商品评价标题");
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    infoArray = [[NSMutableArray alloc] init];
    queArray = [[NSMutableArray alloc] initWithCapacity:10];
    ansArray = [[NSMutableArray alloc] initWithCapacity:10];
    page = 1;
    [self initRefreshFooterView];
    [self initView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[FY_Cloud_getMyReview share] setDelegate:nil];
}

- (void)initView
{
    
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
    [[FY_Cloud_getMyReview share] setDelegate:(id)self];
    [[FY_Cloud_getMyReview share] getMyReview:page];
}
//获取成功回调
-(void)syncGetMyReviewSuccess:(NSArray *)data;
{
    //infoArray = [NSArray arrayWithArray:data];
    if (infoArray.count == 0) {
        infoArray = [NSMutableArray arrayWithArray:data];
    }else{
        [infoArray addObjectsFromArray:data] ;
    }
    if (data.count == 8) {
        isHaveMore = YES;
    }else{
        isHaveMore = NO;
    }
    for (int i = 0; i < data.count; i++)
    {
        NSString *str = [NSString stringWithFormat:@"%@", [[infoArray objectAtIndex:i] objectForKey:@"commentContent"]];
        float txtheight;
        if ([[infoArray objectAtIndex:i] objectForKey:@"commentContent"] != nil) {
             txtheight = [self heightForTextView:cell1.textView_que andText:str];
        }else
        {
            txtheight = 0.0;
        }
        
        [queArray addObject:[NSNumber numberWithFloat:txtheight]];
        if (![[[data objectAtIndex:i] objectForKey:@"reply_evaluate"]  isEqual: @""]) {
            float txtheight2 = [self heightForTextView:cell1.textView_ans andText:[NSString stringWithFormat:@"%@", [[data objectAtIndex:i] objectForKey:@"reply_evaluate"]]];
            [ansArray addObject:[NSNumber numberWithFloat:txtheight2]];
        }else
        {
            [ansArray addObject:[NSNumber numberWithFloat:0.0]];
        }
        
    }
    [loadingView removeFromSuperview];
    [self.tableView reloadData];
}

-(void)syncGetMyReviewFailed:(NSString*)errMsg
{
    [loadingView removeFromSuperview];
    XYNoDataView *noData = [[XYNoDataView alloc] initWithFrame:self.view.frame];
    noData.megUpLabel.text = @"这里空空如也";
    noData.megDownLabel.text = @"亲，还没有商品评价信息哦";
    [self.view addSubview:noData];
}

//无网络连接或意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}

- (void)initRefreshFooterView{
    
    int height = MAX(self.tableView.bounds.size.height, self.tableView.contentSize.height);
    refreshFooterView = [[EGORefreshTableFooterView alloc]  initWithFrame:CGRectZero];
    refreshFooterView.frame =CGRectMake(0.0f, height, self.view.frame.size.width, self.tableView.bounds.size.height);
    //如果contentsize的高度比表的高度小，那么就需要把刷新视图放在表的bounds的下面
    
    refreshFooterView.delegate = self;
    //下拉刷新的控件添加在tableView上
    
    [self.tableView addSubview:refreshFooterView];
    reloading = NO;
    
}

- (float)heightForTextView:(UITextView *)textView andText: (NSString *)strText{
    float fPadding = 16.0;
    CGSize constraint = CGSizeMake(276 - fPadding, CGFLOAT_MAX);
    
    CGSize size = [strText sizeWithFont: [UIFont systemFontOfSize:14] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    float fHeight = size.height + 23.0;
    
    return fHeight;
}


#pragma mark UITableView delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return infoArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float y = 0.0;
    if ([[[infoArray objectAtIndex:indexPath.row]objectForKey:@"reply_evaluate"]  isEqual: @""]) {
        y = [[queArray objectAtIndex:indexPath.row] floatValue] + 83;
    }else
    {
        y = [[queArray objectAtIndex:indexPath.row] floatValue] + 58 + [[ansArray objectAtIndex:indexPath.row] floatValue];
    }
    return y;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CommentCellIdentifier = @"CommentCell";
    
    FYReview2Cell *cell = (FYReview2Cell*)[tableView dequeueReusableCellWithIdentifier:CommentCellIdentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"FYReview2Cell" owner:self options:nil] lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.btn_delete.tag = indexPath.row;
    [cell.btn_delete addTarget:self action:@selector(deleteClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.imageView_product.type = 1;
    cell.imageView_product.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    cell.imageView_product.imageURL = [[infoArray objectAtIndex:indexPath.row] objectForKey:@"img"];
    cell.lbl_producy.text = [NSString stringWithFormat:@"%@", [[infoArray objectAtIndex:indexPath.row] objectForKey:@"productName"]];
    cell.textView_que.frame = CGRectMake(cell.textView_que.frame.origin.x, cell.textView_que.frame.origin.y, cell.textView_que.frame.size.width, [[queArray objectAtIndex:indexPath.row] intValue]);
    cell.textView_que.text = [[infoArray objectAtIndex:indexPath.row] objectForKey:@"commentContent"];
    //时间解析
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:(long)[[[infoArray objectAtIndex:indexPath.row] objectForKey:@"commentTime"]longLongValue]];
    NSTimeZone *zone = [NSTimeZone  timeZoneWithName:@"Asia/Shanghai"];
    NSInteger interval = [zone secondsFromGMTForDate:startDate];
    startDate = [startDate  dateByAddingTimeInterval: interval];
    NSDateFormatter  *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Etc/UTC"]];
    NSMutableString *timeString = [NSMutableString stringWithFormat:@"%@",[formatter stringFromDate:startDate]];
    cell.lbl_date.text = timeString;
    if ([[[infoArray objectAtIndex:indexPath.row] objectForKey:@"reply_evaluate"]  isEqual: @""]) {
        cell.textView_ans.hidden = YES;
    }else
    {
        cell.textView_ans.hidden = NO;
        if ([[[infoArray objectAtIndex:indexPath.row] objectForKey:@"commentContent"]  isEqual: @""] ) {
            cell.textView_ans.frame = CGRectMake(cell.textView_ans.frame.origin.x, cell.textView_que.frame.origin.y , cell.textView_ans.frame.size.width, [[ansArray objectAtIndex:indexPath.row] intValue]);
        }else
        {
            cell.textView_ans.frame = CGRectMake(cell.textView_ans.frame.origin.x, cell.textView_que.frame.origin.y + cell.textView_que.frame.size.height - 10, cell.textView_ans.frame.size.width, [[ansArray objectAtIndex:indexPath.row] intValue]);
        }
        cell.textView_ans.text = [[infoArray objectAtIndex:indexPath.row] objectForKey:@"reply_evaluate"];
        
    }
    
    return cell;
}

-(void)requestProductList{
    [[FY_Cloud_getMyReview share] setDelegate:(id)self];
    [[FY_Cloud_getMyReview share] getMyReview:page];
}

- (void)deleteClick:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    btnTag = btn.tag;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"确定要删除此条信息吗?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
    [alert show];
    
}
#pragma mark UITableView delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [[FY_Cloud_deleteReview share] setDelegate:(id)self];
        [[FY_Cloud_deleteReview share] deleteInfoWithEvaluateID:[NSString stringWithFormat:@"%@", [[infoArray objectAtIndex:btnTag] objectForKey:@"evaluateId"]]];
    }
}

-(void)syncDeleteReviewSuccess:(NSString*)EvaString
{
    [WXCommonViewClass showHudInView:self.view title:@"删除成功"];
    page = 1;
    infoArray = [[NSMutableArray alloc] init];
    queArray = [[NSMutableArray alloc] init];
    ansArray = [[NSMutableArray alloc] init];
    [self initView];
    
}

-(void)syncDeleteReviewFailed:(NSString*)errCode
{
    [WXCommonViewClass showHudInView:self.view title:errCode];
}

#pragma mark - 下拉刷新数据方法
//请求数据
-(void)requestData
{
    reloading = YES;
    BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
    if (isConnection) {//有网络情况
        
        //每次进入该页面的时候，重新从服务器端读取数据
        if(refreshFooterView.hasMoreData)
        {
            
            int myCount = infoArray.count;
            int pageCount = myCount/maxCountOfOnePage;
            page += pageCount;
            //下载团购信息
            [self requestProductList];
            
        }
    }else{//无网络情况
        [WXCommonViewClass showHudInButtonOfView:self.view title:NoHaveNetwork closeInSecond:2];
    }
    
}

-(void)reloadUI
{
    reloading = NO;
    //停止下拉的动作,恢复表格的便宜
	[refreshFooterView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    //更新界面
    [self.tableView reloadData];
    
    [self setRefreshViewFrame];
    
}

-(void)setRefreshViewFrame
{
    //如果contentsize的高度比表的高度小，那么就需要把刷新视图放在表的bounds的下面
    int height = MAX(self.tableView.bounds.size.height, self.tableView.contentSize.height);
    refreshFooterView.frame =CGRectMake(0.0f, height, self.view.frame.size.width, self.tableView.bounds.size.height);
}

#pragma mark - EGORefreshTableFooterDelegate
//出发下拉刷新动作，开始拉取数据
- (void)egoRefreshTableDidTriggerRefresh:(EGORefreshPos)aRefreshPos
{
    [self requestData];
}


//返回当前刷新状态：是否在刷新
- (BOOL)egoRefreshTableDataSourceIsLoading:(UIView*)view{
	
	return reloading; // should return if data source model is reloading
	
}
//返回刷新时间
// if we don't realize this method, it won't display the refresh timestamp
- (NSDate*)egoRefreshTableDataSourceLastUpdated:(UIView*)view
{
	
	return [NSDate date]; // should return date data source was last changed
	
}

#pragma mark - UIScrollView

//此代理在scrollview滚动时就会调用
//在下拉一段距离到提示松开和松开后提示都应该有变化，变化可以在这里实现
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    refreshFooterView.hasMoreData = isHaveMore;
    [refreshFooterView egoRefreshScrollViewDidScroll:scrollView];
}
//松开后判断表格是否在刷新，若在刷新则表格位置偏移，且状态说明文字变化为loading...
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    refreshFooterView.hasMoreData = isHaveMore;
    [refreshFooterView egoRefreshScrollViewDidEndDragging:scrollView];
}

@end
