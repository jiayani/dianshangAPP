//
//  FYInfomationViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-6-6.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYInfomationViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "WXCommonViewClass.h"
#import "WX_Cloud_Delegate.h"
#import "WX_Cloud_IsConnection.h"
#import "FY_Cloud_setInfomation.h"
#import "FY_Cloud_geyInfomation.h"
#import "FYInfomationCell.h"
#import "FYInfomation2Cell.h"
#import "FYLeaveMessageViewController.h"
#import "XYNoDataView.h"
#import "XYLoadingView.h"
#import "UserInfo.h"
#import "WXUserDatabaseClass.h"
#import "FY_Cloud_deleteInfo.h"
#define maxCountOfOnePage 8;
@interface FYInfomationViewController ()
{
    NSMutableArray *infoArray;
    UIView *grayView;
    FYInfomationCell *cell1;
    FYInfomation2Cell *cell2;
    NSMutableArray *queArray;
    NSMutableArray *ansArray;
    BOOL isHaveMore;
    BOOL reloading;
    EGORefreshTableFooterView *refreshFooterView;
    int page;
    XYLoadingView *loadingView;
    XYNoDataView *noData;
    int btnTag;
}
@end

@implementation FYInfomationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"商品咨询", @"商品咨询标题");
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    UIColor *textColor = [UIColor colorWithRed:236.0/255.0 green:236.0/255.0 blue:236.0/255.0 alpha:1.0];
    self.textView_info.layer.borderColor = textColor.CGColor;
    self.textView_info.layer.borderWidth = 1.0;
    self.textView_info.layer.cornerRadius =5.0;
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    if ([self.pushViewController isKindOfClass:[FYLeaveMessageViewController class]])
    {
        self.infoView.hidden = YES;
        [self.btn_info setHidden:YES];
        self.tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);;
    }
    else
    {
        self.tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - self.infoView.frame.size.height);
    }
    queArray = [[NSMutableArray alloc] initWithCapacity:10];
    ansArray = [[NSMutableArray alloc] initWithCapacity:10];
    self.makeView.frame = CGRectMake(0, self.view.frame.size.height + 50, self.view.frame.size.width, self.makeView.frame.size.height);
    
    infoArray = [[NSMutableArray alloc] init];
    page = 1;
    [self initRefreshFooterView];
    [self initView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [[FY_Cloud_geyInfomation share] setDelegate:nil];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)initRefreshFooterView{
    
    int height = MAX(self.tableView.bounds.size.height, self.tableView.contentSize.height);
    refreshFooterView = [[EGORefreshTableFooterView alloc]  initWithFrame:CGRectZero];
    refreshFooterView.frame =CGRectMake(0.0f, height, self.view.frame.size.width, self.tableView.bounds.size.height);
    //如果contentsize的高度比表的高度小，那么就需要把刷新视图放在表的bounds的下面
    
    refreshFooterView.delegate = self;
    //下拉刷新的控件添加在tableView上
    
    [self.tableView addSubview:refreshFooterView];
    reloading = NO;
    
}

- (void)initView
{
    
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
    [[FY_Cloud_geyInfomation share] setDelegate:(id)self];
    [[FY_Cloud_geyInfomation share] getInfomation:page andProductID:self.productId];
    
}

- (IBAction)btn_infoClick:(id)sender
{
    grayView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap1:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    grayView.backgroundColor = [UIColor darkGrayColor];
    grayView.alpha = 0.5;
    [grayView addGestureRecognizer:tapGestureRecognizer];
    [self.view addSubview:grayView];
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.2];
//    if (iPhone5) {
//        self.makeView.frame = CGRectMake(0, self.view.frame.size.height - self.makeView.frame.size.height - 136, self.makeView.frame.size.width, self.makeView.frame.size.height);
//    }else
//    {
//    self.makeView.frame = CGRectMake(0, self.view.frame.size.height - self.makeView.frame.size.height - 236, self.makeView.frame.size.width, self.makeView.frame.size.height);
//    }
//    [UIView commitAnimations];
    
    [self.view bringSubviewToFront:self.makeView];
    
    //监听键盘高度的变换
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self.textView_info becomeFirstResponder];
    
}
- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    [self autoMovekeyBoard:keyboardRect.size.height];
}


- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    [self autoMovekeyBoard:0];
}
- (void)autoMovekeyBoard:(float)h{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
	self.makeView.frame = CGRectMake(0.0f, (float)(self.view.frame.size.height - h - 18.0) - 103, self.makeView.frame.size.width, self.makeView.frame.size.height);
	self.tableView.frame = CGRectMake(0.0f, 0.0f, 320.0f,(float)(self.view.frame.size.height - h - 46.0));
    [UIView commitAnimations];
}
- (void)singleTap1:(id)sender
{
    [self.textView_info resignFirstResponder];
    self.makeView.frame = CGRectMake(0, self.view.frame.size.height + 50, self.view.frame.size.width, self.makeView.frame.size.height);
    [grayView removeFromSuperview];
}

- (IBAction)btn_setClick:(id)sender
{
    UserInfo *user = [WXUserDatabaseClass getCurrentUser];
    if (user == nil) {
        [WXCommonViewClass goToLogin:self.navigationController];
        return;
    }
    [[FY_Cloud_setInfomation share] setDelegate:(id)self];
    [[FY_Cloud_setInfomation share] setInfomation:self.textView_info.text andProductID:self.productId];
}
//成功获取商品咨询回调
- (void)syncGetInfomationSuccess:(NSArray *)dataArray
{
    if (infoArray.count == 0) {
        infoArray = [NSMutableArray arrayWithArray:dataArray];
    }else{
        [infoArray addObjectsFromArray:dataArray] ;
    }
    if (dataArray.count == 8) {
        isHaveMore = YES;
    }else{
        isHaveMore = NO;
    }
    for (int i = 0; i < dataArray.count; i++)
    {
        NSString *str = [NSString stringWithFormat:@"%@", [[dataArray objectAtIndex:i] objectForKey:@"question"]];
        float txtheight;
        if ([self.pushViewController isKindOfClass:[FYLeaveMessageViewController class]])
        {
            txtheight = [self heightForTextView:cell2.textView_que andText:str];
        }
        else{
            txtheight = [self heightForTextView:cell1.textView_que andText:str];
        }
        [queArray addObject:[NSNumber numberWithFloat:txtheight]];
        if (![[[dataArray objectAtIndex:i] objectForKey:@"answerDate"]  isEqual: @""]) {
            float txtheight2 = [self heightForTextView:cell1.textView_ans andText:[NSString stringWithFormat:@"%@", [[dataArray objectAtIndex:i] objectForKey:@"answerContent"]]];
            [ansArray addObject:[NSNumber numberWithFloat:txtheight2]];
        }else
        {
            [ansArray addObject:[NSNumber numberWithFloat:0.0]];
        }
        
    }

    [loadingView removeFromSuperview];
    [self reloadUI];
}

- (void)syncGetInfomationFailed:(NSString*)errCode
{
    if (page == 1) {
        [loadingView removeFromSuperview];
        noData = [[XYNoDataView alloc] initWithFrame:self.view.frame];
        noData.megUpLabel.text = @"这里空空如也";
        noData.megDownLabel.text = @"亲，还没有商品咨询信息哦";
        [self.view addSubview:noData];
    }
    isHaveMore = NO;
    [self reloadUI];
    [self.view bringSubviewToFront:self.infoView];
}

//发送回调
- (void)syncSetInfomationSuccess:(NSString *)message
{

    [self.textView_info resignFirstResponder];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    self.makeView.frame = CGRectMake(0, self.view.frame.size.height + 1, self.makeView.frame.size.width, self.makeView.frame.size.height);
    [UIView commitAnimations];
    [grayView removeFromSuperview];
    [loadingView removeFromSuperview];
    infoArray = [[NSMutableArray alloc] init];
    queArray = [[NSMutableArray alloc] init];
    ansArray = [[NSMutableArray alloc] init];
    page = 1;
    [noData removeFromSuperview];
    [self initView];
}

- (void)syncSetInfomationFailed:(NSString*)errCode
{
    [loadingView removeFromSuperview];
}

- (float)heightForTextView:(UITextView *)textView andText: (NSString *)strText{
    float fPadding = 16.0;
    CGSize constraint = CGSizeMake(276 - fPadding, CGFLOAT_MAX);
    
    CGSize size = [strText sizeWithFont: [UIFont systemFontOfSize:14] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    float fHeight = size.height + 23.0;
    
    return fHeight;
}

//无网络连接或意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}

- (void)deleteClick:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    btnTag = btn.tag;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"确定要删除此条信息吗?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [[FY_Cloud_deleteInfo share] setDelegate:(id)self];
        [[FY_Cloud_deleteInfo share] deleteInfoWithInfoID:[[infoArray objectAtIndex:btnTag] objectForKey:@"consultId"]];
    }
}
//删除回调
-(void)syncDeleteInfoSuccess:(NSString*)EvaString;
{
    [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:@"删除成功"];
    page = 1;
    infoArray = [[NSMutableArray alloc] init];
    queArray = [[NSMutableArray alloc] init];
    ansArray = [[NSMutableArray alloc] init];
    [self initView];
    
}
-(void)syncDeleteInfoFailed:(NSString*)errCode;
{
    [WXCommonViewClass showHudInView:self.view title:errCode];
}


#pragma mark tableView delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float y = 0.0;
    if (![self.pushViewController isKindOfClass:[FYLeaveMessageViewController class]])
    {
        if ([[[infoArray objectAtIndex:indexPath.row]objectForKey:@"answerDate"] intValue]  == 0) {
            y = [[queArray objectAtIndex:indexPath.row] floatValue] + 25;
        }else
        {
            y = [[queArray objectAtIndex:indexPath.row] floatValue] + 5 + [[ansArray objectAtIndex:indexPath.row] floatValue];
        }
        
    }
    else
    {
        if ([[[infoArray objectAtIndex:indexPath.row]objectForKey:@"answerDate"] intValue] == 0) {
            y = [[queArray objectAtIndex:indexPath.row] floatValue] + 83;
        }else
        {
            y = [[queArray objectAtIndex:indexPath.row] floatValue] + 73 + [[ansArray objectAtIndex:indexPath.row] floatValue];
        }
        
        
    }
    return y;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return infoArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CommentCellIdentifier = @"CommentCell";
    
    if ([self.pushViewController isKindOfClass:[FYLeaveMessageViewController class]]) {
        FYInfomation2Cell *cell = (FYInfomation2Cell*)[tableView dequeueReusableCellWithIdentifier:CommentCellIdentifier];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"FYInfomation2Cell" owner:self options:nil] lastObject];
            
        }
        cell.btn_delete.tag = indexPath.row;
        [cell.btn_delete addTarget:self action:@selector(deleteClick:) forControlEvents:UIControlEventTouchUpInside];
        cell.imageView_product.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [[infoArray objectAtIndex:indexPath.row] objectForKey:@"productImageUrl"]]]]];
        cell.lbl_product.text = [NSString stringWithFormat:@"%@", [[infoArray objectAtIndex:indexPath.row] objectForKey:@"productName"]];
        cell.textView_que.frame = CGRectMake(cell.textView_que.frame.origin.x , cell.textView_que.frame.origin.y - 4, cell.textView_que.frame.size.width, [[queArray objectAtIndex:indexPath.row] intValue]);
        cell.textView_que.text = [[infoArray objectAtIndex:indexPath.row] objectForKey:@"question"];
        //时间解析
        NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:(long)[[[infoArray objectAtIndex:indexPath.row] objectForKey:@"questionDate"]longLongValue]];
        NSTimeZone *zone = [NSTimeZone  timeZoneWithName:@"Asia/Shanghai"];
        NSInteger interval = [zone secondsFromGMTForDate:startDate];
        startDate = [startDate  dateByAddingTimeInterval: interval];
        NSDateFormatter  *formatter = [[NSDateFormatter alloc] init];
		[formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Etc/UTC"]];
		NSMutableString *timeString = [NSMutableString stringWithFormat:@"%@",[formatter stringFromDate:startDate]];
        cell.lbl_date.text = timeString;
        cell.lbl_date.frame = CGRectMake(cell.lbl_date.frame.origin.x, cell.textView_que.frame.origin.y + cell.textView_que.frame.size.height - 10 , cell.lbl_date.frame.size.width, cell.lbl_date.frame.size.height);
        if ([[[infoArray objectAtIndex:indexPath.row] objectForKey:@"answerDate"]  isEqual: @""]) {
            cell.textView_ans.hidden = YES;
        }else
        {
            cell.textView_ans.hidden = NO;
            cell.textView_ans.frame = CGRectMake(cell.textView_ans.frame.origin.x, cell.lbl_date.frame.origin.y + cell.lbl_date.frame.size.height - 5, cell.textView_ans.frame.size.width, [[ansArray objectAtIndex:indexPath.row] intValue]);
            cell.textView_ans.text = [[infoArray objectAtIndex:indexPath.row] objectForKey:@"answerContent"];
        }
        return cell;
        
	}else
    {
        FYInfomationCell *cell = (FYInfomationCell*)[tableView dequeueReusableCellWithIdentifier:CommentCellIdentifier];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"FYInfomationCell" owner:self options:nil] lastObject];
        }
        //提问内容显示
        cell.textView_que.frame = CGRectMake(cell.textView_que.frame.origin.x, cell.textView_que.frame.origin.y, cell.textView_que.frame.size.width, [[queArray objectAtIndex:indexPath.row] intValue]);
        cell.textView_que.text = [[infoArray objectAtIndex:indexPath.row] objectForKey:@"question"];
        cell.lbl_name.text = [NSString stringWithFormat:@"%@", [[infoArray objectAtIndex:indexPath.row] objectForKey:@"questionName"]];
        //解析时间戳
        NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:(long)[[[infoArray objectAtIndex:indexPath.row] objectForKey:@"questionDate"]longLongValue]];
        NSTimeZone *zone = [NSTimeZone  timeZoneWithName:@"Asia/Shanghai"];
        NSInteger interval = [zone secondsFromGMTForDate:startDate];
        startDate = [startDate  dateByAddingTimeInterval: interval];
        NSDateFormatter  *formatter = [[NSDateFormatter alloc] init];
		[formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Etc/UTC"]];
		NSMutableString *timeString = [NSMutableString stringWithFormat:@"%@",[formatter stringFromDate:startDate]];
        cell.lbl_date.text = timeString;
        cell.lbl_name.frame = CGRectMake(cell.lbl_name.frame.origin.x, [[queArray objectAtIndex:indexPath.row] intValue] - 2, cell.lbl_name.frame.size.width, cell.lbl_date.frame.size.height);
        cell.lbl_date.frame = CGRectMake(cell.lbl_date.frame.origin.x, [[queArray objectAtIndex:indexPath.row] intValue] - 2, cell.lbl_date.frame.size.width, cell.lbl_date.frame.size.height);
        //判断是否有回答
        if ([[[infoArray objectAtIndex:indexPath.row] objectForKey:@"answerDate"] intValue] == 0) {
            cell.textView_ans.hidden = YES;
            cell.imageView_ans.hidden = YES;
        }else
        {
            cell.textView_ans.hidden = NO;
            cell.imageView_ans.hidden = NO;
            cell.textView_ans.frame = CGRectMake(cell.textView_ans.frame.origin.x, cell.lbl_name.frame.origin.y + 12, cell.textView_ans.frame.size.width, [[ansArray objectAtIndex:indexPath.row] intValue]);
            cell.imageView_ans.frame = CGRectMake(cell.imageView_ans.frame.origin.x, cell.textView_ans.frame.origin.y + 10, cell.imageView_ans.frame.size.width , cell.imageView_ans.frame.size.height);
            cell.textView_ans.text = [[infoArray objectAtIndex:indexPath.row] objectForKey:@"answerContent"];
        }
        
        return cell;
        
    }
    
}

-(void)requestProductList{
    [[FY_Cloud_geyInfomation share] setDelegate:(id)self];
    [[FY_Cloud_geyInfomation share] getInfomation:page andProductID:self.productId];
}

#pragma mark - 下拉刷新数据方法
//请求数据
-(void)requestData
{
    reloading = YES;
    BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
    if (isConnection) {//有网络情况
        
        //每次进入该页面的时候，重新从服务器端读取数据
        if(refreshFooterView.hasMoreData)
        {
            
            int myCount = infoArray.count;
            int pageCount = myCount/maxCountOfOnePage;
            page += pageCount;
            //下载团购信息
            [self requestProductList];
            
        }
    }else{//无网络情况
        [WXCommonViewClass showHudInButtonOfView:self.view title:NoHaveNetwork closeInSecond:2];
    }
    
}

-(void)reloadUI
{
    reloading = NO;
    //停止下拉的动作,恢复表格的便宜
	[refreshFooterView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    //更新界面
    [self.tableView reloadData];
    
    [self setRefreshViewFrame];
    
}

-(void)setRefreshViewFrame
{
    //如果contentsize的高度比表的高度小，那么就需要把刷新视图放在表的bounds的下面
    int height = MAX(self.tableView.bounds.size.height, self.tableView.contentSize.height);
    refreshFooterView.frame =CGRectMake(0.0f, height, self.view.frame.size.width, self.tableView.bounds.size.height);
}

#pragma mark - EGORefreshTableFooterDelegate
//出发下拉刷新动作，开始拉取数据
- (void)egoRefreshTableDidTriggerRefresh:(EGORefreshPos)aRefreshPos
{
    [self requestData];
}


//返回当前刷新状态：是否在刷新
- (BOOL)egoRefreshTableDataSourceIsLoading:(UIView*)view{
	
	return reloading; // should return if data source model is reloading
	
}
//返回刷新时间
// if we don't realize this method, it won't display the refresh timestamp
- (NSDate*)egoRefreshTableDataSourceLastUpdated:(UIView*)view
{
	
	return [NSDate date]; // should return date data source was last changed
	
}

#pragma mark - UIScrollView

//此代理在scrollview滚动时就会调用
//在下拉一段距离到提示松开和松开后提示都应该有变化，变化可以在这里实现
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    refreshFooterView.hasMoreData = isHaveMore;
    [refreshFooterView egoRefreshScrollViewDidScroll:scrollView];
}
//松开后判断表格是否在刷新，若在刷新则表格位置偏移，且状态说明文字变化为loading...
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    refreshFooterView.hasMoreData = isHaveMore;
    [refreshFooterView egoRefreshScrollViewDidEndDragging:scrollView];
}

@end
