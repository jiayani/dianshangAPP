//
//  WXUserRegisterProtocolViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface WXUserRegisterProtocolViewController : UIViewController<UIWebViewDelegate,MBProgressHUDDelegate>{
    MBProgressHUD *mbProgressHUD;
}

@property (weak, nonatomic) IBOutlet UIWebView *myWebView;

@end
