//
//  WXUserRegisterViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXUserRegisterViewController.h"

#import "WXCommonViewClass.h"
#import "WX_Cloud_UserRegister.h"
#import "WXUserActivation1ViewController.h"
#import "WXPersonalCenterViewController.h"
#import "WXVerifyMethodsClass.h"
#import "WXUserDatabaseClass.h"
#import "WXUserRegisterProtocolViewController.h"
#import "WX_Cloud_IsConnection.h"
#import "WXUserLoginViewController.h"
#import "WXPersonalCenterViewController.h"
#import "MyMD5.h"


@interface WXUserRegisterViewController ()

@end

@implementation WXUserRegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"注册", @"注册页面标题");
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initAllControllers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[WX_Cloud_UserRegister share] setDelegate:nil];
    mbProgressHUD.delegate = nil;
    mbProgressHUD = nil;
}
- (void)viewDidUnload {
    [self setNameText:nil];
    [self setPwdText:nil];
    [self setRegisterBtn:nil];
    [self setUserProtocolBtn:nil];
    [self setReadLabel:nil];
    [super viewDidUnload];
}
/*给UIImageView(checkbox Imageview)添加事件响应，用图片来模拟checkbox事件*/
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //单击页面时隐藏键盘
    if (self.myEditingTxt != nil) {
        [self.myEditingTxt resignFirstResponder];
    }
    
    
}
#pragma mark - UITextFieldDelegate Mthods

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    self.myEditingTxt = textField;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.myEditingTxt = nil;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    long tag = textField.tag;
    if (tag == 0) {
        [self.pwdText becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
        if (textField.text.length > 0 && self.nameText.text.length > 0) {
            [self registerBtnPressed:nil];
        }else{
            [textField resignFirstResponder];
        }
        
    }
    return YES;
}

#pragma mark - UIAlertViewDelegate Methods
- (void)willPresentAlertView:(UIAlertView *)alertView{
    //设置alertView中文字居左对其
    if (alertView.tag == 100) {
        for( UIView * view in alertView.subviews )
            
        {
            if( [view isKindOfClass:[UILabel class]] )
                
            {
                UILabel* label = (UILabel*) view;
                
                label.textAlignment = NSTextAlignmentLeft;
            }
            
        }
    }
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    long tag = alertView.tag ;
    if (tag == 100) {
        if (buttonIndex == 0) {
            WXUserActivation1ViewController *userActvationVC = [[WXUserActivation1ViewController alloc]initWithNibName:@"WXUserActivation1ViewController" bundle:nil];
            userActvationVC.willPushViewController = self.willPushViewController;
            [self.navigationController pushViewController:userActvationVC animated:YES];
        }else if (buttonIndex == 1){
            [self pushOrPresend];
            
        }
    }
    
}
- (void)myAlert:(int)tag{
    if (tag == 98 || tag == 101){
        [self.nameText becomeFirstResponder];
    }else if (tag == 99 || tag == 102){
        [self.pwdText becomeFirstResponder];
    }
}
//根据不同的呈现方式 来处理消失的方式
- (void)pushOrPresend{
    NSArray *tempArray = self.navigationController.viewControllers;
    UIViewController *tempVC = [tempArray objectAtIndex:0];
    if ([tempVC isKindOfClass:[WXUserLoginViewController class]]) {
        [WXCommonViewClass goToTabItem:self.navigationController tabItem:5];
    }else{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}
#pragma mark - All My Methods
- (void)initAllControllers{
    /*设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    self.nameText.placeholder = NSLocalizedString(@"请输入注册邮箱", @"注册/请输入注册邮箱");
    self.pwdText.placeholder = NSLocalizedString(@"请输入您的密码", @"注册/请输入您的密码");
    self.readLabel.text = NSLocalizedString(@"注册即视为同意", @"注册/注册即视为同意");
    [self.userProtocolBtn setTitle:NSLocalizedString(@"用户协议", @"注册/用户协议") forState: UIControlStateNormal];
    [self.registerBtn setTitle:NSLocalizedString(@"立即注册", @"注册/立即注册") forState: UIControlStateNormal];
    
    self.nameText.keyboardType = UIKeyboardTypeEmailAddress;
    
    //begin:给用户注册协议下划线
     CGPoint originPoint = self.userProtocolBtn.frame.origin;
     CGPoint originLable = self.userProtocolBtn.titleLabel.frame.origin;
     CGSize sizePwd = self.userProtocolBtn.titleLabel.frame.size;
     UIImageView *underlineImage = [[UIImageView alloc]initWithFrame:CGRectMake(originPoint.x + originLable.x,originPoint.y+originLable.y + sizePwd.height + 1,sizePwd.width,0.5)];
     
     underlineImage.backgroundColor = [UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1.0f];
     [self.view addSubview:underlineImage];
     //end 
    
}
//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)registerBtnPressed:(id)sender {
    if (self.myEditingTxt != nil) {
        [self.myEditingTxt resignFirstResponder];
    }
    
    NSString *name = [self.nameText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //本地格式验证
    BOOL isValidateEmail = [WXVerifyMethodsClass isValidateEmail:name];
    BOOL isValidatePwd = [WXVerifyMethodsClass isValidatePassword:self.pwdText.text];
    BOOL isValidatePwdLength = [WXVerifyMethodsClass isValidatePwdLength:self.pwdText.text];
    NSString *alertStr = nil;
    BOOL isAllValidate = YES;
    int tag = 0;
    if (self.nameText.text.length <= 0) {
        tag = 98;
        isAllValidate = NO;
        alertStr = NSLocalizedString(@"账号不能为空", @"注册/账号不能为空");
    }else if(self.pwdText.text.length <= 0){
        
        tag = 99;
        isAllValidate = NO;
        alertStr = NSLocalizedString(@"密码不能为空", @"注册/密码不能为空");
    }else if (!isValidateEmail) {
        tag = 101;
        isAllValidate = NO;
        alertStr = NSLocalizedString(@"邮箱输入错误，请重新输入", @"注册/邮箱输入错误，请重新输入");
    }else if (!isValidatePwdLength){
        tag = 102;
        isAllValidate = NO;
        alertStr = NSLocalizedString(@"密码长度应为6-16位", @"注册/密码长度应为6-16位");
    }else if (!isValidatePwd){
        tag = 102;
        isAllValidate = NO;
        alertStr = NSLocalizedString(@"密码格式应为字母或数字", @"注册/密码格式应为字母或数字");

    }else if (isRead){
        isAllValidate = NO;
        alertStr = NSLocalizedString(@"请阅读并同意用户注册协议", @"注册/请阅读并同意用户注册协议");
    }
    if (!isAllValidate) {
        //小圈停止运动
        [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
        alertTag = tag;
        mbProgressHUD = [WXCommonViewClass showHudInView:self.view title:alertStr];
        mbProgressHUD.delegate = self;
        
    }else{
        //本地判断是否连接网络
        BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
        if (!isConnection) {//无网络情况

            NSString *errorStr = NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用");
            [WXCommonViewClass showHudInView:self.view title:errorStr];
        }else{
            //小圈转动等待验证
            [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"正在注册...", @"正在注册...") nav:self.navigationController];
            //进行后台服务器端验证
             NSString *passwordMD5 = [MyMD5 md5:self.pwdText.text];
            [[WX_Cloud_UserRegister share] setDelegate:self];
//            [[WX_Cloud_UserRegister share] userRegister:name UserPassword:passwordMD5];
        }
        
        
    }
    
}
- (IBAction)userRegisterProtocolBtnPressed:(id)sender {
     WXUserRegisterProtocolViewController *userRegisterProtocolVC = [[WXUserRegisterProtocolViewController alloc]initWithNibName:@"WXUserRegisterProtocolViewController" bundle:nil];
    [self.navigationController pushViewController:userRegisterProtocolVC animated:YES];
}

#pragma mark － MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[self myAlert:alertTag];
}

#pragma mark - WX_Cloud_Delegate Methods
//无网络和意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[WX_Cloud_UserRegister share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}
//用户注册成功
-(void)userRegisterSuccess:(NSMutableDictionary*)databaseDictionary{
    [[WX_Cloud_UserRegister share] setDelegate:nil];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.integralFromRegister = [[databaseDictionary objectForKey:@"integralCount"]intValue];

    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    //数据库操作
    NSString *passwordMD5 = [MyMD5 md5:self.pwdText.text];
    [databaseDictionary setObject:passwordMD5 forKey:@"password"];
    [WXUserDatabaseClass registerUser:databaseDictionary];
    NSString *name = [self.nameText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *str1 = NSLocalizedString(@"亲爱的用户:\n\n您的账号", @"注册/亲爱的用户:\n\n您的账号");
    NSString *str2 = NSLocalizedString(@"已成功注册，激活并绑定手机号可以享受账号保护、积分返点、商家优惠等服务。您是否立即激活？", @"注册/已成功注册，激活并绑定手机号可以享受账号保护、积分返点、商家优惠等服务。您是否立即激活？");
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@%@%@",str1,name,str2] delegate:self cancelButtonTitle:NSLocalizedString(@"现在激活", @"注册/现在激活") otherButtonTitles:NSLocalizedString(@"稍后再说", @"注册/稍后再说"),nil];
    
    alert.tag = 100;
    alert.delegate = self;
    [alert show];
    
    
}
-(void)userRegisterFailed:(NSString*)errCode{
    
    [[WX_Cloud_UserRegister share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    mbProgressHUD = [WXCommonViewClass showHudInView:self.view title:errCode];
    mbProgressHUD.delegate = self;
    alertTag = 101;

}
@end

