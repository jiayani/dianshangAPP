//
//  JYNUserRegisterViewController.m
//  DianShangApp
//
//  Created by wa on 14-10-10.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "JYNUserRegisterViewController.h"
#import "WXCommonViewClass.h"
#import "WXUserActivation1ViewController.h"
#import "WXUserLoginViewController.h"
#import "WXVerifyMethodsClass.h"
#import "WX_Cloud_UserRegister.h"
#import "MyMD5.h"
#import "WXUserDatabaseClass.h"
#import "WXPersonalCenterViewController.h"
#import "JYN_Cloud_getRegPhoneCode.h"
#import "JYN_cloud_loginnumber.h"
#import "FY_Cloud_getRegPhoneCode.h"
@interface JYNUserRegisterViewController ()
{
    int loginType;
    long viewTag;
    NSInteger second;
}
@end

@implementation JYNUserRegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"注册", @"注册页面标题");
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = BackgroundColorOfPage;
    [self initAllControllers];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[WX_Cloud_UserRegister share] setDelegate:nil];
    mbProgressHUD.delegate = nil;
    mbProgressHUD = nil;
}
/*给UIImageView(checkbox Imageview)添加事件响应，用图片来模拟checkbox事件*/
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //单击页面时隐藏键盘
    if (self.myEditingTxt != nil) {
        [self.myEditingTxt resignFirstResponder];
    }
    
}

#pragma mark - IBActions
- (IBAction)phoneRegSectionClick:(id)sender {
    
    viewTag = 1001;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.0f];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [self.emailSlider setBackgroundImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
    [self.phoneSlider setBackgroundImage:[UIImage imageNamed:@"register_select"] forState:UIControlStateNormal];
    self.phoneView.frame = CGRectMake(0, self.phoneView.frame.origin.y, self.phoneView.frame.size.width, self.phoneView.frame.size.height);
    self.emailView.frame = CGRectMake(320, self.emailView.frame.origin.y, self.emailView.frame.size.width, self.emailView.frame.size.height);
    
    [UIView commitAnimations];
    
    [self.phoneSlider setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.emailSlider setTitleColor:[UIColor grayColor]forState:UIControlStateNormal];
    
    
}

- (IBAction)emailRegSectionClick:(id)sender {
    
    viewTag = 1002;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.0f];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    [self.emailSlider setBackgroundImage:[UIImage imageNamed:@"register_select"] forState:UIControlStateNormal];
    [self.phoneSlider setBackgroundImage:[UIImage imageNamed:nil] forState:UIControlStateNormal];
    
    
    self.phoneView.frame = CGRectMake(-320, self.phoneView.frame.origin.y, self.phoneView.frame.size.width, self.phoneView.frame.size.height);
    self.emailView.frame = CGRectMake(0, self.emailView.frame.origin.y, self.emailView.frame.size.width, self.emailView.frame.size.height);
    
    [UIView commitAnimations];
    [self.emailSlider setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.phoneSlider setTitleColor:[UIColor grayColor]forState:UIControlStateNormal];
    
    
}


#pragma mark - UITextFieldDelegate Mthods

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    self.myEditingTxt = textField;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.myEditingTxt = nil;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    long tag = textField.tag;
    if (viewTag ==1001) {
        if (tag ==101) {
            [self.phoneYzm becomeFirstResponder];
        }
        else if (tag ==102){
            [self.phonePsd becomeFirstResponder];
        }
        else {
            [textField resignFirstResponder];
            if (textField.text.length >0) {
                [self handlePhoneReg:nil];
            }
            else{
                [textField resignFirstResponder];
            }
        }
        
    }
    else{
        if (tag == 104) {
            [self.emailPsd becomeFirstResponder];
        }else{
            [textField resignFirstResponder];
            if (textField.text.length > 0) {
                [self handleEmailReg:nil];
            }else{
                [textField resignFirstResponder];
            }
            
        }
    }
    return YES;
}

#pragma mark - UIAlertViewDelegate Methods
- (void)willPresentAlertView:(UIAlertView *)alertView{
    //设置alertView中文字居左对其
    if (alertView.tag == 100) {
        for( UIView * view in alertView.subviews )
            
        {
            if( [view isKindOfClass:[UILabel class]] )
                
            {
                UILabel* label = (UILabel*) view;
                
                label.textAlignment = NSTextAlignmentLeft;
            }
            
        }
    }
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    long tag = alertView.tag ;
    if (tag == 100) {
        if (buttonIndex == 0) {
            WXUserActivation1ViewController *userActvationVC = [[WXUserActivation1ViewController alloc]initWithNibName:@"WXUserActivation1ViewController" bundle:nil];
            userActvationVC.willPushViewController = self.willPushViewController;
            [self.navigationController pushViewController:userActvationVC animated:YES];
        }else if (buttonIndex == 1){
            [self pushOrPresend];
            
        }
    }
    
}
//根据不同的呈现方式 来处理消失的方式
- (void)pushOrPresend{
    NSArray *tempArray = self.navigationController.viewControllers;
    UIViewController *tempVC = [tempArray objectAtIndex:0];
    if ([tempVC isKindOfClass:[WXUserLoginViewController class]]) {
        [WXCommonViewClass goToTabItem:self.navigationController tabItem:5];
    }else{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

- (IBAction)xieyiButtonpress:(id)sender
{
    
}


#pragma mark - All My Methods
- (void)initAllControllers{
    /*设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    //begin:给用户注册协议下划线
    CGPoint originPoint = self.xieyiButton.frame.origin;
    CGPoint originLable = self.xieyiButton.titleLabel.frame.origin;
    CGSize sizePwd = self.xieyiButton.titleLabel.frame.size;
    UIImageView *underlineImage = [[UIImageView alloc]initWithFrame:CGRectMake(originPoint.x + originLable.x,originPoint.y+originLable.y + sizePwd.height + 1,sizePwd.width,0.5)];
    
    underlineImage.backgroundColor = [UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1.0f];
    [self.view addSubview:underlineImage];
    //end
    [self.phoneValidate addTarget:self action:@selector(handleValidatePhone:) forControlEvents:UIControlEventTouchUpInside];
    [self.phoneReg addTarget:self action:@selector(handlePhoneReg:) forControlEvents:UIControlEventTouchUpInside];
    [self.emailReg addTarget:self action:@selector(handleEmailReg:) forControlEvents:UIControlEventTouchUpInside];
    
}
//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}
//Send verification code
- (void) handleValidatePhone:(UIButton *)button
{
    NSString *phoneStr = [self.phonetf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    BOOL isValidatePhone = [WXVerifyMethodsClass isValidatePhone:phoneStr];
    if (phoneStr.length <= 0) {
        [WXCommonViewClass showHudInView:self.view title:@"手机号不能为空"];
        
    }else if (!isValidatePhone) {
        [WXCommonViewClass showHudInView:self.view title:@"手机号码输入有误"];
    }else{
        [WXCommonViewClass setActivityWX:YES content:@"正在发送" nav:self.navigationController];
        [self.phonetf resignFirstResponder];
        NSString *time = [NSString stringWithFormat:@"%ld", [self date]];
        NSString *token = [NSString stringWithFormat:@"8ca8188ca3d9b1c8%@", time];
        token = [MyMD5 md5:token];  //加密
        [[JYN_Cloud_getRegPhoneCode share] setDelegate:(id)self];
        [[JYN_Cloud_getRegPhoneCode share] getPhoneCodeWithPhone:self.phonetf.text andTime:time andToken:token];
    }
}
- (long)date {
    NSDate *datenow = [NSDate date];
    long timSp = (long)[datenow timeIntervalSince1970];
    return timSp;
}
//手机注册
- (void) handlePhoneReg:(UIButton *) sender
{
    if ([self.phonetf.text isEqual:@""]) {
        [WXCommonViewClass showHudInView:self.view title:@"手机号不能为空"];
        
    }else if ([self.phoneYzm.text isEqual:@""]){
        [WXCommonViewClass showHudInView:self.view title:@"验证码不能为空"];
        
    }else if ([self.phonePsd isEqual:@""]){
        [WXCommonViewClass showHudInView:self.view title:@"密码不能为空"];
        
    }else{
        NSString *phoneStr = [self.phonetf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        BOOL isValidatePhone = [WXVerifyMethodsClass isValidatePhone:phoneStr];
        BOOL isValidatePwdLength = [WXVerifyMethodsClass isValidatePwdLength:self.phonePsd.text];
        BOOL isValidatePwd = [WXVerifyMethodsClass isValidatePassword:self.phonePsd.text];
        if (!isValidatePhone) {
            
            [WXCommonViewClass showHudInView:self.view title:@"手机号码输入有误"];
            
            
        } else if(!isValidatePwdLength) {
            [WXCommonViewClass showHudInView:self.view title:@"密码长度应为6-16位"];
            
        }else if (!isValidatePwd){
            [WXCommonViewClass showHudInView:self.view title:@"密码格式应为字母或数字"];
            
        } else
        {
            loginType = 2;//phone
            [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"正在注册...", @"正在注册...") nav:self.navigationController];
            NSString *phonePsd = [NSString stringWithFormat:@"%@",self.phonePsd.text];
            NSString *phoneStr = [self.phonetf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            [[WX_Cloud_UserRegister share] setDelegate:(id)self];
            [[WX_Cloud_UserRegister share] userRegisterWithUserName:phoneStr UserPassword:phonePsd andType:loginType andCode:self.phoneYzm.text];
        }
    }
    
}

- (void) handleEmailReg:(UIButton *) sender
{
    loginType = 1;//email
    if ([self.emailAddr.text isEqual:@""]) {
        [WXCommonViewClass showHudInView:self.view title:@"邮箱不能为空"];
        
    }else if ([self.emailPsd.text isEqual:@""]){
        [WXCommonViewClass showHudInView:self.view title:@"密码不能为空"];
        
    }else{
        
        BOOL isValidateEmail = [WXVerifyMethodsClass isValidateEmail:self.emailAddr.text];
        BOOL isValidatePwdLength = [WXVerifyMethodsClass isValidatePwdLength:self.emailPsd.text];
        BOOL isValidatePwd = [WXVerifyMethodsClass isValidatePassword:self.emailPsd.text];
        
        if (!isValidateEmail) {
            [WXCommonViewClass showHudInView:self.view title:@"邮箱输入错误，请重新输入"];
            
        } else if (!isValidatePwdLength){
            [WXCommonViewClass showHudInView:self.view title:@"密码长度应为6-16位"];
            
        }else if (!isValidatePwd){
            [WXCommonViewClass showHudInView:self.view title:@"密码格式应为字母或数字"];
            
        }
        else
        {
            [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"正在注册...", @"正在注册...") nav:self.navigationController];
            NSString *phonePsd = [NSString stringWithFormat:@"%@",self.emailPsd.text];
            NSString *passwordMD5 = [MyMD5 md5:phonePsd];

            NSString *phoneStr = [self.emailAddr.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            [[WX_Cloud_UserRegister share] setDelegate:(id)self];
            [[WX_Cloud_UserRegister share] userRegisterWithUserName:phoneStr UserPassword:passwordMD5 andType:loginType andCode:@""];
        }
    }
    
}

//注册成功
-(void)userRegisterSuccess:(NSMutableDictionary*)databaseDictionary
{
    [[WX_Cloud_UserRegister share] setDelegate:nil];
    
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.isThirdPartyLogging = NO;
    singletonClass.currentUserId = nil;
        if (loginType == 1) {
        singletonClass.myLastLoginName = self.emailAddr.text;
        [[WX_Cloud_UserRegister share] setDelegate:nil];
    //    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
//        singletonClass.integralFromRegister = [[databaseDictionary objectForKey:@"integralCount"]longValue];
        //小圈停止运动
        [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
        NSString * passwordMD5 = [MyMD5 md5:self.emailPsd.text];  //加密
        
        [databaseDictionary setObject:passwordMD5 forKey:@"password"];
        
        [WXUserDatabaseClass registerUser:databaseDictionary];
        NSString *name = [self.emailAddr.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString *str1 = NSLocalizedString(@"亲爱的用户:\n\n您的账号", @"注册/亲爱的用户:\n\n您的账号");
        NSString *str2 = NSLocalizedString(@"已成功注册，激活并绑定手机号可以享受账号保护、积分返点、商家优惠等服务。您是否立即激活？", @"注册/已成功注册，激活并绑定手机号可以享受账号保护、积分返点、商家优惠等服务。您是否立即激活？");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@%@%@",str1,name,str2] delegate:self cancelButtonTitle:NSLocalizedString(@"现在激活", @"注册/现在激活") otherButtonTitles:NSLocalizedString(@"稍后再说", @"注册/稍后再说"),nil];
        alert.tag = 100;
        alert.delegate = self;
        [alert show];
    }else
    {
        singletonClass.myLastLoginName = self.phonetf.text;
        [[WX_Cloud_UserRegister share] setDelegate:nil];
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        singletonClass.integralFromRegister = [[databaseDictionary objectForKey:@"integralCount"]intValue];
        //小圈停止运动
        [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];

        //数据库操作
        NSString * passwordMD5 = [MyMD5 md5:self.phonePsd.text];  //加密
        [databaseDictionary setObject:passwordMD5 forKey:@"password"];
        [WXUserDatabaseClass registerUser:databaseDictionary];
        [self pushOrPresend];

           }
    /*登录成功后给后台发送接口以便后台进行登录次数的统计*/
    [[JYN_cloud_loginnumber share]setDelegate:nil];
    [[JYN_cloud_loginnumber share]sendNumberToBackground];
    
}
-(void)getRegPhoneCodeSuccess:(NSDictionary*)data
{
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    self.lbl_code.text = @"重新获取";
    [self.phoneValidate setBackgroundImage:[UIImage imageNamed:@"readSeconds.png"] forState:UIControlStateNormal];
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(handlTimer:) userInfo:self repeats:YES];
    self.phoneValidate.enabled = NO;
    self.phonetf.userInteractionEnabled = NO;
    [[FY_Cloud_getRegPhoneCode share] setDelegate:nil];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"验证码发送成功,验证码有效时间10分钟" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)getRegPhoneCodeFailed:(NSString*)errCode
{
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInView:self.view title:errCode];
}

- (void)handlTimer:(NSTimer *)timer{
    second--;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.01];
    self.lbl_code.text = [NSString stringWithFormat:@"重新获取(%i秒)", second];
    [UIView commitAnimations];
    if(second == 0){
        self.lbl_code.text = @"重新获取";
        [self.phoneValidate setBackgroundImage:[UIImage imageNamed:@"getVerifyImage.png"] forState:UIControlStateNormal];
        self.phoneValidate.enabled = YES;
        self.phonetf.userInteractionEnabled = YES;
        second = 60;
        [timer invalidate];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [WXCommonViewClass showHudInView:self.view title:errorCode];
}

@end
