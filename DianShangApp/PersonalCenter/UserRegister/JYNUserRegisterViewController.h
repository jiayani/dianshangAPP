//
//  JYNUserRegisterViewController.h
//  DianShangApp
//
//  Created by wa on 14-10-10.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"
#import "MBProgressHUD.h"
@interface JYNUserRegisterViewController : UIViewController<UITextFieldDelegate, UIAlertViewDelegate,WX_Cloud_Delegate,MBProgressHUDDelegate>

{
    BOOL isRead;
    int alertTag;
    MBProgressHUD *mbProgressHUD;
}
@property (strong, nonatomic) UITextField *myEditingTxt;

// upper
@property (weak, nonatomic) IBOutlet UIButton    *phoneSlider;
@property (weak, nonatomic) IBOutlet UIButton    *emailSlider;
@property (weak, nonatomic) IBOutlet UILabel *lbl_code;


// phone reg.
@property (weak, nonatomic) IBOutlet UIView      *phoneView;
@property (weak, nonatomic) IBOutlet UITextField *phonetf;
@property (weak, nonatomic) IBOutlet UITextField *phoneYzm;
@property (weak, nonatomic) IBOutlet UITextField *phonePsd;
@property (weak, nonatomic) IBOutlet UIButton    *phoneValidate;
@property (weak, nonatomic) IBOutlet UIButton    *phoneReg;
@property (weak, nonatomic) IBOutlet UIButton    *xieyiButton;
@property (weak, nonatomic) IBOutlet UIButton    *Exieyibutton;
- (IBAction)xieyiButtonpress:(id)sender;



// email reg.
@property (weak, nonatomic) IBOutlet UIView      *emailView;
@property (strong, nonatomic) UIViewController *willPushViewController;

@property (weak, nonatomic) IBOutlet UITextField *emailAddr;
@property (weak, nonatomic) IBOutlet UITextField *emailPsd;


@property (weak, nonatomic) IBOutlet UIButton    *emailReg;

- (IBAction)phoneRegSectionClick:(id)sender;

- (IBAction)emailRegSectionClick:(id)sender;



@end
