//
//  WXUserRegisterViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"
#import "MBProgressHUD.h"

@interface WXUserRegisterViewController : UIViewController<UITextFieldDelegate,UIAlertViewDelegate,WX_Cloud_Delegate,MBProgressHUDDelegate>{
    BOOL isRead;
    int alertTag;
    MBProgressHUD *mbProgressHUD;
}
@property (weak, nonatomic) UITextField *myEditingTxt;
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UITextField *pwdText;
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;
@property (weak, nonatomic) IBOutlet UIButton *userProtocolBtn;
@property (weak, nonatomic) IBOutlet UILabel *readLabel;
@property (weak, nonatomic) UIViewController *willPushViewController;
- (void)myAlert:(int)tag;
@end
