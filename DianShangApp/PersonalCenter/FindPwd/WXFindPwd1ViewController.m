//
//  WXFindPwd1ViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-19.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXFindPwd1ViewController.h"
#import "WX_Cloud_FindPwdFirst.h"
#import "WXCommonViewClass.h"
#import "WXVerifyMethodsClass.h"
#import "WXFindPwd2ViewController.h"
#import "WX_Cloud_IsConnection.h"

@interface WXFindPwd1ViewController ()

@end

@implementation WXFindPwd1ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"找回密码", @"找回密码1页面标题");
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    [self initAllControllers];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!iPhone5) {
        //键盘的监听事件，获取高度
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[WX_Cloud_FindPwdFirst share] setDelegate:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    mbProgressHUD.delegate = nil;
    mbProgressHUD = nil;
}
- (void)viewDidUnload {
    [self setFirstLabel:nil];
    [self setSecondLabel:nil];
    [self setThirsdLabel:nil];
    [self setEmailOrPhoneTxt:nil];
    [self setGetVerifyBtn:nil];
    self.findPwdByEmailBtn = nil;
    self.findPwdByPhoneBtn = nil;
    self.redLineLabel = nil;
    self.loginVC = nil;
    self.errorStr = nil;
    [super viewDidUnload];
}
#pragma mark - UITextFieldDelegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if (!iPhone5) {
        [self ViewMoveToZero];
    }
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
    if (!iPhone5) {
        [self ViewMoveToZero];
    }
    return YES;
}
- (IBAction)textFieldChanged:(id)sender {
    if (isEmail) {
        emailStr = [self.emailOrPhoneTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }else{
        phoneStr = [self.emailOrPhoneTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    
}
#pragma mark - All My Methods
/*初始化所有页面显示内容*/
- (void)initAllControllers{
    /*设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    
     [self.getVerifyBtn setTitle:NSLocalizedString(@"获取验证码", @"找回密码1/获取验证码") forState:UIControlStateNormal];
    self.firstLabel.text = NSLocalizedString(@"填写注册信息", @"找回密码1/填写注册信息");
    self.secondLabel.text = NSLocalizedString(@"验证身份", @"找回密码1/验证身份");
    self.thirsdLabel.text = NSLocalizedString(@"设置新密码", @"找回密码1/设置新密码");
    self.errorStr = NSLocalizedString(@"该邮箱不存在", @"找回密码1/该邮箱不存在");
    [self.findPwdByEmailBtn setTitle:NSLocalizedString(@"邮箱找回", @"找回密码1/邮箱找回") forState:UIControlStateNormal];
    [self.findPwdByPhoneBtn setTitle:NSLocalizedString(@"手机找回", @"找回密码1/手机找回") forState:UIControlStateNormal];
    self.emailOrPhoneTxt.placeholder = NSLocalizedString(@"请输入您的邮箱", @"找回密码1/请输入您的邮箱");
    isEmail = YES;
    [self tabButtonChange];
    self.emailOrPhoneTxt.keyboardType = UIKeyboardTypeEmailAddress;
    
    [self.findPwdByEmailBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.findPwdByPhoneBtn setTitleColor:[UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0f] forState:UIControlStateNormal];
    
    emailStr = @"";
    phoneStr = @"";
    
    if (iOS7) {
        pointY = 64;
    }else{
        pointY = 0;
    }
    
}
//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}
//根据选择不同的按钮，切换页面显示效果
- (void)tabButtonChange{
    CGRect newRect;
    if (isEmail) {
        newRect = CGRectMake(25.0f, 44.0f, 110.0f, 1.0f);
    }else{
        newRect = CGRectMake(185.0f, 44.0f, 110.0f, 1.0f);
    }
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    self.redLineLabel.frame= newRect;
    [UIView commitAnimations];
}
/*恢复view刚开始的位置*/
- (void)ViewMoveToZero{
    if (self.view.frame.origin.y != pointY  && !iPhone5) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.2];
        self.view.frame = CGRectMake(0, pointY, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    if ([touch view] == self.view) {
        [self.emailOrPhoneTxt resignFirstResponder];
    }
}
//键盘事件
- (void)keyboardWillShow:(NSNotification*)notification{
    CGRect keyboardRect = [[[notification userInfo] objectForKey:_UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat txtHeight = self.emailOrPhoneTxt.frame.origin.y + self.emailOrPhoneTxt.frame.size.height;
    CGFloat keyY = self.view.frame.size.height - keyboardRect.size.height;
    if (keyY < txtHeight) {
        isMove = YES;
        moveFloat = txtHeight - keyY;
    }else{
        isMove = NO;
        moveFloat = 0;
    }
    if (isMove) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.2];
        
        self.view.frame = CGRectMake(0, -(moveFloat + 20), self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }else{
        if (!iPhone5) {
            [self ViewMoveToZero];
        }
    }
    
}
- (IBAction)findPwdByEmailBtnPressed:(id)sender {
    
    isEmail = YES;
    [self tabButtonChange];
    [self.emailOrPhoneTxt resignFirstResponder];
    self.emailOrPhoneTxt.keyboardType = UIKeyboardTypeEmailAddress;
    if (emailStr.length <= 0) {
        self.emailOrPhoneTxt.placeholder = NSLocalizedString(@"请输入您的邮箱", @"找回密码1/请输入您的邮箱");
        self.emailOrPhoneTxt.text = nil;
        
    }else{
        
        self.emailOrPhoneTxt.text = emailStr;
    }
    
    [self.findPwdByEmailBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.findPwdByPhoneBtn setTitleColor:[UIColor colorWithRed:178.0/255.0 green:178.0/255.0 blue:178.0/255.0 alpha:1.0f] forState:UIControlStateNormal];
}

- (IBAction)findPwdByPhoneBtnPressed:(id)sender {
    
    isEmail = NO;
    [self tabButtonChange];
    [self.emailOrPhoneTxt resignFirstResponder];
    self.emailOrPhoneTxt.keyboardType = UIKeyboardTypeNumberPad;
    if (phoneStr.length <= 0) {
        self.emailOrPhoneTxt.placeholder = NSLocalizedString(@"请输入您的手机号", @"找回密码1/请输入您的手机号");
        self.emailOrPhoneTxt.text = nil;
        
    }else{
        
        self.emailOrPhoneTxt.text = phoneStr;
    }
    
    [self.findPwdByPhoneBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.findPwdByEmailBtn setTitleColor:[UIColor colorWithRed:178.0/255.0 green:178.0/255.0 blue:178.0/255.0 alpha:1.0f] forState:UIControlStateNormal];
}
/*对输入的邮箱或手机号进行本地格式验证*/
- (BOOL)verifyEmailOrPhone{
    NSString *emailOrPhoneStr = [self.emailOrPhoneTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    BOOL isValidate ;
    if (isEmail) {
        isValidate = [WXVerifyMethodsClass isValidateEmail:emailOrPhoneStr];
    }else{
        isValidate = [WXVerifyMethodsClass isValidatePhone:emailOrPhoneStr];
    }
    return isValidate;
}

- (IBAction)getVerityBtnPressed:(id)sender {
    [self.emailOrPhoneTxt resignFirstResponder];
    BOOL isValidate = [self verifyEmailOrPhone];
    if (self.emailOrPhoneTxt.text.length <= 0 ) {
        
        if (isEmail) {
            self.errorStr = NSLocalizedString(@"邮箱不能为空", @"找回密码1/邮箱不能为空");
        }else{
            self.errorStr = NSLocalizedString(@"手机号不能为空", @"找回密码1/手机号不能为空");
        }

        mbProgressHUD = [WXCommonViewClass showHudInView:self.view title:self.errorStr];
        mbProgressHUD.delegate = self;
        
        
    }else if (!isValidate) {
        
        if (isEmail) {
            self.errorStr = NSLocalizedString(@"您输入的邮箱不正确", @"找回密码1/您输入的邮箱不正确");
        }else{
            self.errorStr = NSLocalizedString(@"您输入的手机号不正确", @"找回密码1/您输入的手机号不正确");
        }
        mbProgressHUD = [WXCommonViewClass showHudInView:self.view title:self.errorStr];
        mbProgressHUD.delegate = self;
        
    }else{
        //本地判断是否连接网络
        BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
        if (!isConnection) {//无网络情况
            NSString *errCode = NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用");
            [WXCommonViewClass showHudInView:self.view title:errCode];
        }else{
            //小圈转动等待验证
            [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"等待验证...", @"找回密码1/等待验证...") nav:self.navigationController];
            //调用后台方法进行服务器断验证
            [[WX_Cloud_FindPwdFirst share] setDelegate:(id)self];
            //0 邮箱  1:手机号
            int emailOrPhoneFlage = 0;
            if (isEmail) {
                emailOrPhoneFlage = 0;
            }else{
                emailOrPhoneFlage = 1;
            }
            NSString *emailOrPhoneStr = [self.emailOrPhoneTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            [[WX_Cloud_FindPwdFirst share] findPasswordFirst:emailOrPhoneFlage phoneOrEmailStr:emailOrPhoneStr];
        }
    }
}

#pragma mark － MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[self.emailOrPhoneTxt becomeFirstResponder];
}

#pragma mark - WX_Cloud_Delegate Methods

//无网络连接和意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[WX_Cloud_FindPwdFirst share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInView:self.view title:errorStr];

}
//验证码验证
-(void)verifyPhoneOrEmailSuccess:(NSString*)keyWordStr{
    [[WX_Cloud_FindPwdFirst share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    WXFindPwd2ViewController *findPwd2VC = [[WXFindPwd2ViewController alloc]initWithNibName:@"WXFindPwd2ViewController" bundle:nil];
    findPwd2VC.loginVC = self.loginVC;
    findPwd2VC.isEmail = isEmail;
    findPwd2VC.phoneOrEmailStr = [self.emailOrPhoneTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    findPwd2VC.uiIDFromCloud = [keyWordStr intValue];
    [self.navigationController pushViewController:findPwd2VC animated:YES];
}
-(void)verifyPhoneOrEmailFailed:(NSString*)keyWordStr errCode:(NSString*)errCode{
    [[WX_Cloud_FindPwdFirst share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInView:self.view title:errCode];
    
}
@end
