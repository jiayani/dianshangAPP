//
//  WXFindPwd2ViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXFindPwd2ViewController.h"
#import "WXCommonViewClass.h"
#import "WX_Cloud_FindPwdFirst.h"
#import "WX_Cloud_FindPwdSecond.h"
#import "WX_Cloud_IsConnection.h"
#import "WXFindPwd3ViewController.h"


@interface WXFindPwd2ViewController ()

@end

@implementation WXFindPwd2ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"找回密码", @"找回密码2页面标题");
        [WXCommonViewClass hideTabBar:self isHiden:YES];

    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initAllControllers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!iPhone5) {
        //键盘的监听事件，获取高度
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    }
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [newTimer invalidate];
    [[WX_Cloud_FindPwdSecond share] setDelegate:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    mbProgressHUD.delegate = nil;
    mbProgressHUD = nil;
}
- (void)viewDidUnload {
    [self setFirstLabel:nil];
    [self setSecondLabel:nil];
    [self setThirdLabel:nil];
    [self setSubmitVerifyCodeBtn:nil];
    [self setResendVerifyCodeBtn:nil];
    [self setVerifyCodeTxt:nil];
    [self setPhoneOrEmailLabel:nil];
    [self setMyAnimationView:nil];
    self.errorStr = nil;
    [super viewDidUnload];
}
#pragma mark - UITextFieldDelegate Methods
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (!iPhone5) {
        [self ViewMoveToZero];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)textFieldChanged:(id)sender {
    if (self.verifyCodeTxt.text.length <= 0 ) {

        self.errorStr = NSLocalizedString(@"验证码不能为空", @"会员激活2/验证码不能为空");
        
    }
}
#pragma mark - All My Methods
/*初始化所有页面显示内容*/
- (void)initAllControllers{
    /*设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    

    [self.submitVerifyCodeBtn setTitle:NSLocalizedString(@"提交验证码", @"找回密码1/提交验证码") forState:UIControlStateNormal];
    self.myAnimationView.hidden = YES;
    
    self.firstLabel.text = NSLocalizedString(@"填写注册信息", @"找回密码2/填写注册信息");
    self.secondLabel.text = NSLocalizedString(@"验证身份", @"找回密码2/验证身份");
    self.thirdLabel.text = NSLocalizedString(@"设置新密码", @"找回密码2/设置新密码");
    NSString *tempStr,*tempStr2;
    countdownInt = 60;
    startDate = [NSDate date];
    if (self.isEmail) {
        tempStr = NSLocalizedString(@"您的邮箱是", @"找回密码2/您的邮箱是");
        
    }else{
        tempStr = NSLocalizedString(@"您的手机号是", @"找回密码2/您的手机号是");
    }
    tempStr2 = [NSString stringWithFormat:@"%d\"",countdownInt];
    self.timerLabel.text = tempStr2;
    NSString *str = [NSString stringWithFormat:@"%@:%@",tempStr,self.phoneOrEmailStr];
    self.phoneOrEmailLabel.text = str;
    self.verifyCodeTxt.placeholder = NSLocalizedString(@"请输入验证码", @"找回密码2/请输入验证码");
    
    sendCount = 0;
    //每秒钟刷新一次倒计时显示
    self.resendVerifyCodeBtn.enabled = NO;
    self.resendVerifyCodeBtn.alpha = 0.8f;
    self.timerLabel.alpha = 0.8f;
    newTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(refreshCountdown) userInfo:nil repeats:YES];
    if (iOS7) {
        pointY = 64;
    }else{
        pointY = 0;
    }
}

//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    if ([touch view] == self.view) {
        [self.verifyCodeTxt resignFirstResponder];
    }
}
//键盘事件
- (void)keyboardWillShow:(NSNotification*)notification{
    CGRect keyboardRect = [[[notification userInfo] objectForKey:_UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat txtHeight = self.verifyCodeTxt.frame.origin.y + self.verifyCodeTxt.frame.size.height;
    CGFloat keyY = self.view.frame.size.height - keyboardRect.size.height;
    if (keyY < txtHeight) {
        isMove = YES;
        moveFloat = txtHeight - keyY;
    }else{
        isMove = NO;
        moveFloat = 0;
    }
    if (isMove) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.2];
        
        self.view.frame = CGRectMake(0, -(moveFloat + 20), self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }else{
        if (!iPhone5) {
            [self ViewMoveToZero];
        }
    }
    
}
/*恢复view刚开始的位置*/
- (void)ViewMoveToZero{
    if (self.view.frame.origin.y != pointY) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.2];
        self.view.frame = CGRectMake(0, pointY, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }
}
- (IBAction)submitVerifyCodeBtnPressed:(id)sender {
    [self.verifyCodeTxt resignFirstResponder];
    NSString *verifyCodeStr = [self.verifyCodeTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //输入的验证码是否为空
    if (verifyCodeStr.length <= 0) {
        self.errorStr = NSLocalizedString(@"验证码不能为空", @"找回密码2/验证码不能为空");
        mbProgressHUD = [WXCommonViewClass showHudInView:self.view title:self.errorStr];
        mbProgressHUD.delegate = self;
        
        return;
    }else{
        //本地判断是否连接网络
        BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
        if (!isConnection) {//无网络情况
            NSString *errorStr = NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用");
            [WXCommonViewClass showHudInView:self.view title:errorStr];
        }else{
            //小圈转动等待验证
            self.myAnimationView.hidden = NO;
            [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"等待验证...", @"找回密码2/等待验证...") View:self.myAnimationView];
            sendCount++;
            //调用后台方法进行服务器断验证
            int newType = self.isEmail == YES ? 0 : 1;
            [[WX_Cloud_FindPwdSecond share] setDelegate:(id)self];
            [[WX_Cloud_FindPwdSecond share] findPasswordWithKeywords:verifyCodeStr flag:self.uiIDFromCloud type:newType];
        }
    }
    
}

- (void)refreshCountdown{
    countdownInt--;
    NSString *tempStr = @"";
    if (countdownInt <= 0) {
        tempStr = NSLocalizedString(@"重新获取", @"找回密码2/重新获取");
        self.resendVerifyCodeBtn.enabled = YES;
        self.resendVerifyCodeBtn.alpha = 1.0f;
        self.timerLabel.alpha = 1.0f;
        self.timerLabel.text = tempStr;
    }else{
        tempStr = [NSString stringWithFormat:@"%d\"",countdownInt];
        self.resendVerifyCodeBtn.enabled = NO;
        self.resendVerifyCodeBtn.alpha = 0.8f;
        self.timerLabel.alpha = 0.8f;
        self.timerLabel.text = tempStr;
    }

}
- (IBAction)resendVerifyCodeBtnPressed:(id)sender {
    countdownInt = 60;
    NSString *tempStr = [NSString stringWithFormat:@"%d\"",countdownInt];
    self.resendVerifyCodeBtn.enabled = NO;
    self.resendVerifyCodeBtn.alpha = 0.8f;
    self.timerLabel.alpha = 0.8f;
    self.timerLabel.text = tempStr;

    startDate = [NSDate date];
    //调用后台方法进行服务器断验证
    [[WX_Cloud_FindPwdSecond share] setDelegate:(id)self];
    //0 邮箱  1:手机号
    int emailOrPhoneFlage = 0;
    if (self.isEmail) {
        emailOrPhoneFlage = 0;
    }else{
        emailOrPhoneFlage = 1;
    }
    
    [[WX_Cloud_FindPwdFirst share] findPasswordFirst:emailOrPhoneFlage phoneOrEmailStr:self.phoneOrEmailStr];
    
}

#pragma mark － MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[self.verifyCodeTxt becomeFirstResponder];
}

#pragma mark - WX_Cloud_Delegate Methods 提交验证码
//无网络连接或意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    //小圈停止运动
    [[WX_Cloud_FindPwdSecond share] setDelegate:nil];
    self.myAnimationView.hidden = YES;
    [WXCommonViewClass setActivityWX:NO content:nil View:self.myAnimationView];
    [WXCommonViewClass showHudInView:self.view title:errorStr];

}
//验证码验证
-(void)verifyCodeSuccess:(NSString*)keywordsID{
    [[WX_Cloud_FindPwdSecond share] setDelegate:nil];
    //小圈停止运动
    self.myAnimationView.hidden = YES;
    [WXCommonViewClass setActivityWX:NO content:nil View:self.myAnimationView];
    WXFindPwd3ViewController *findPwd3VC = [[WXFindPwd3ViewController alloc]initWithNibName:@"WXFindPwd3ViewController" bundle:nil];
    findPwd3VC.loginVC = self.loginVC;
    findPwd3VC.uiIDFromCloud = self.uiIDFromCloud;
    [self.navigationController pushViewController:findPwd3VC animated:YES];
    
}
-(void)verifyCodeFaild:(NSString*)keywordsID andErrorMessage:(NSString*)errCode{
    [[WX_Cloud_FindPwdSecond share] setDelegate:nil];
    //小圈停止运动
    self.myAnimationView.hidden = YES;
    [WXCommonViewClass setActivityWX:NO content:nil View:self.myAnimationView];
    [WXCommonViewClass showHudInView:self.view title:errCode];
    
}
#pragma mark - WX_Cloud_Delegate 获取验证码方法
//验证码验证
-(void)verifyPhoneOrEmailSuccess:(NSString*)keyWordStr{
    [[WX_Cloud_FindPwdFirst share] setDelegate:nil];
    
}
-(void)verifyPhoneOrEmailFailed:(NSString*)keyWordStr errCode:(NSString*)errCode{
    [[WX_Cloud_FindPwdFirst share] setDelegate:nil];
    
}

@end
