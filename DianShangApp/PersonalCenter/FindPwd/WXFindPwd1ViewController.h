//
//  WXFindPwd1ViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-19.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"
#import "MBProgressHUD.h"
@class WXUserLoginViewController;

@interface WXFindPwd1ViewController : UIViewController<UITextFieldDelegate,WX_Cloud_Delegate,MBProgressHUDDelegate>{
    BOOL isEmail;
    NSString *phoneStr;
    NSString *emailStr;
    BOOL isMove;//是否需要将view上移
    CGFloat moveFloat;//上移动多少
    CGFloat pointY;//距顶部的距离
    MBProgressHUD *mbProgressHUD;
    
}
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirsdLabel;
@property (weak, nonatomic) IBOutlet UIButton *findPwdByEmailBtn;
@property (weak, nonatomic) IBOutlet UIButton *findPwdByPhoneBtn;
@property (weak, nonatomic) IBOutlet UITextField *emailOrPhoneTxt;
@property (weak, nonatomic) NSString *errorStr;
@property (weak, nonatomic) IBOutlet UIButton *getVerifyBtn;
@property (weak, nonatomic) IBOutlet UILabel *redLineLabel;
@property (weak, nonatomic) WXUserLoginViewController *loginVC;

- (IBAction)textFieldChanged:(id)sender;
- (IBAction)findPwdByEmailBtnPressed:(id)sender;
- (IBAction)findPwdByPhoneBtnPressed:(id)sender;

- (IBAction)getVerityBtnPressed:(id)sender;

@end
