//
//  WXFindPwd3ViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"
#import "MBProgressHUD.h"
@class WXUserLoginViewController;


@interface WXFindPwd3ViewController : UIViewController<UITextFieldDelegate,WX_Cloud_Delegate,MBProgressHUDDelegate>{
    int alertTag;
    MBProgressHUD *mbProgressHUD;
}
@property (weak,nonatomic) UITextField *myEditingTextField;
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdLabel;
@property (weak, nonatomic) IBOutlet UITextField *pwdTxt;
@property (weak, nonatomic) IBOutlet UITextField *pwdAgainTxt;
@property (weak, nonatomic) NSString *errorStr;


@property (weak, nonatomic) IBOutlet UIButton *okBtnPressed;

@property (weak, nonatomic)  WXUserLoginViewController *loginVC;

@property (readwrite) int uiIDFromCloud;//找回密码返回的uiid

- (IBAction)textFieldChanged:(id)sender;
- (IBAction)okBtnPressed:(id)sender;

@end


