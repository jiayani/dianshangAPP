//
//  WXFindPwd2ViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WXUserLoginViewController.h"
#import "WX_Cloud_Delegate.h"
#import "MBProgressHUD.h"

@interface WXFindPwd2ViewController : UIViewController<UITextFieldDelegate,WX_Cloud_Delegate,MBProgressHUDDelegate>{
    int countdownInt;//倒计时计数
    int sendCount;//记录发送验证码的次数
    NSDate *startDate;
    BOOL isMove;//是否需要将view上移
    CGFloat moveFloat;//上移动多少
    CGFloat pointY;//距顶部的距离
    NSTimer *newTimer;
    MBProgressHUD *mbProgressHUD;
}
@property (readwrite) BOOL isEmail;
@property (weak, nonatomic) NSString *errorStr;
@property (strong,nonatomic) NSString *phoneOrEmailStr;
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneOrEmailLabel;
@property (weak, nonatomic) IBOutlet UITextField *verifyCodeTxt;
@property (weak, nonatomic) IBOutlet UIButton *submitVerifyCodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *resendVerifyCodeBtn;
@property (weak, nonatomic) IBOutlet UIView *myAnimationView;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;


@property (weak, nonatomic) WXUserLoginViewController *loginVC;

@property (readwrite) int uiIDFromCloud;//找回密码返回的uiid

- (IBAction)textFieldChanged:(id)sender;
- (IBAction)submitVerifyCodeBtnPressed:(id)sender;
- (IBAction)resendVerifyCodeBtnPressed:(id)sender;

@end
