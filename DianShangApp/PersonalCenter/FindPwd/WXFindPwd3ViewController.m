//
//  WXFindPwd3ViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXFindPwd3ViewController.h"
#import "WXCommonViewClass.h"
#import "WX_Cloud_IsConnection.h"
#import "WX_Cloud_ResetPwd.h"
#import "WXVerifyMethodsClass.h"
#import "MyMD5.h"
#import "WXUserDatabaseClass.h"
#import "UserInfo.h"

@interface WXFindPwd3ViewController ()

@end

@implementation WXFindPwd3ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"找回密码", @"找回密码3页面标题");
        [WXCommonViewClass hideTabBar:self isHiden:YES];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initAllControllers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[WX_Cloud_ResetPwd share] setDelegate:nil];
    mbProgressHUD.delegate = nil;
    mbProgressHUD = nil;
}
- (void)viewDidUnload {
    [self setFirstLabel:nil];
    [self setSecondLabel:nil];
    [self setThirdLabel:nil];
    [self setPwdTxt:nil];
    [self setPwdAgainTxt:nil];
    [self setOkBtnPressed:nil];
    self.myEditingTextField = nil;
    self.errorStr = nil;
    [super viewDidUnload];
}
#pragma mark - UITextFieldDelegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    self.myEditingTextField = textField;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.myEditingTextField = nil;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([self.pwdTxt isFirstResponder]) {
        [self.pwdAgainTxt becomeFirstResponder];
    }else if ([self.pwdAgainTxt isFirstResponder]){
        [self.pwdAgainTxt resignFirstResponder];
    }
    return YES;
}
- (IBAction)textFieldChanged:(id)sender {
    if (self.pwdTxt.text.length <= 0) {
        self.errorStr = NSLocalizedString(@"新密码不能为空", @"注册/新密码不能为空");

    }else if(self.pwdAgainTxt.text.length <= 0){
        self.errorStr = NSLocalizedString(@"确认密码不能为空", @"注册/确认密码不能为空");

    }
}
#pragma mark - All My Methods
/*初始化所有页面显示内容*/
- (void)initAllControllers{
    /*设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
 
    [self.okBtnPressed setTitle:NSLocalizedString(@"完成", @"找回密码1/完成") forState:UIControlStateNormal];
    
    self.firstLabel.text = NSLocalizedString(@"填写注册信息", @"找回密码3/填写注册信息");
    self.secondLabel.text = NSLocalizedString(@"验证身份", @"找回密码3/验证身份");
    self.thirdLabel.text = NSLocalizedString(@"设置新密码", @"找回密码3/设置新密码");
    self.pwdTxt.placeholder = NSLocalizedString(@"请输入新密码", @"找回密码3/请输入新密码");
    self.pwdAgainTxt.placeholder = NSLocalizedString(@"确认新密码", @"找回密码3/确认新密码");
    
}

//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    if ([touch view] == self.view) {
        if (self.myEditingTextField != nil) {
            [self.myEditingTextField resignFirstResponder];
            
        }
    }
}

- (IBAction)okBtnPressed:(id)sender {
    if (self.myEditingTextField != nil) {
        [self.myEditingTextField resignFirstResponder];
        
    }
    //    BOOL isValidatePwdLength = [WXVerifyMethodsClass isValidatePwdLength:self.pwdTxt.text];
    BOOL isValidatePwd = [WXVerifyMethodsClass isValidatePassword:self.pwdTxt.text];
    BOOL isAllValidate = YES;
    NSString *errorStr;
    if (self.pwdTxt.text.length <= 0) {
        
        isAllValidate = NO;
        errorStr = NSLocalizedString(@"新密码不能为空", @"注册/新密码不能为空");
        alertTag = 10;
        
    }else if(self.pwdAgainTxt.text.length <= 0){
        
        isAllValidate = NO;
        errorStr = NSLocalizedString(@"确认密码不能为空", @"注册/确认密码不能为空");
        alertTag = 11;
        
    }else if (self.pwdTxt.text.length < 6){
        isAllValidate = NO;
        errorStr = NSLocalizedString(@"密码不能少于6个字符", @"注册/密码不能少于6个字符");
        alertTag = 10;
        
    }else if(self.pwdTxt.text.length > 16){
        isAllValidate = NO;
        errorStr = NSLocalizedString(@"密码不能超过16个字符", @"注册/密码不能超过16个字符");
        alertTag = 10;
        
    }else if (!isValidatePwd){
        isAllValidate = NO;
        errorStr = NSLocalizedString(@"密码格式应为字母或数字", @"注册/密码格式应为字母或数字");
        alertTag = 10;
        
    }else if (![self.pwdTxt.text isEqualToString:self.pwdAgainTxt.text]){
        isAllValidate = NO;
        errorStr = NSLocalizedString(@"密码输入不一致，请重新输入", @"找回密码3/密码输入不一致，请重新输入");
        alertTag = 11;
        
    }
    if (!isAllValidate) {
        self.errorStr = errorStr;
        mbProgressHUD = [WXCommonViewClass showHudInView:self.view title:self.errorStr];
        mbProgressHUD.delegate = self;
        
    }else{
        //本地判断是否连接网络
        BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
        if (!isConnection) {//无网络情况
            NSString *errorStr = NSLocalizedString(@"目前网络连接不可用", @"目前网络连接不可用");
            [WXCommonViewClass showHudInView:self.view title:errorStr];
        }else{
            //小圈转动等待服务器端验证
            [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"正在修改...", @"修改密码/正在修改...") nav:self.navigationController];
            //调用后台服务器端方法修改密码
            [[WX_Cloud_ResetPwd share] setDelegate:(id)self];
            /*RePassword是上个页面传过来的shopid*/
             NSString * newPasswordMD5 = [MyMD5 md5:self.pwdTxt.text];    //MD5加密
            [[WX_Cloud_ResetPwd share]changePassword:newPasswordMD5 RePassword:[NSString stringWithFormat:@"%d",self.uiIDFromCloud]];
        }
    }
    
}
-(void)popToRootViewController{
    if (self.loginVC != nil) {
        [self.navigationController popToViewController:(UIViewController *)self.loginVC animated:YES];

    }

}

#pragma mark － MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
    if (alertTag == 10) {
        [self.pwdTxt becomeFirstResponder];
    }else{
        [self.pwdAgainTxt becomeFirstResponder];
        
    }

}

#pragma mark - WYDCloudDelegate Methods
//无网络连接/意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[WX_Cloud_ResetPwd share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}
//用户找回密码、修改密码
-(void)updatePwdSuccess{
    [[WX_Cloud_ResetPwd share] setDelegate:nil];
    
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    //修改本地数据库
    NSString *newPasswordMD5 = [MyMD5 md5:self.pwdTxt.text];    //MD5加密
    UserInfo *currentUser = [WXUserDatabaseClass getCurrentUser];
    [WXUserDatabaseClass updatePwdOfUser:currentUser pwdStr:newPasswordMD5];
    [WXCommonViewClass showHudInButtonOfView:self.view title:NSLocalizedString(@"修改密码成功",@"修改密码成功") closeInSecond:2];
    [NSTimer scheduledTimerWithTimeInterval:1 target:self
                                   selector:@selector(popToRootViewController) userInfo:nil
                                    repeats:NO];
    
}

-(void)updatePwdFailed:(NSString*)errCode{
    [[WX_Cloud_ResetPwd share] setDelegate:nil];
    //小圈停止运动
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInView:self.view title:errCode];
}
@end
