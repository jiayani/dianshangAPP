//
//  FYEvaluateViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-6-16.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "WX_Cloud_Delegate.h"

@interface FYEvaluateViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, MBProgressHUDDelegate,UITextFieldDelegate,WX_Cloud_Delegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) int orderID;
@end
