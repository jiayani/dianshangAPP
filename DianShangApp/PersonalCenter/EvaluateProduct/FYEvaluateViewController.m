//
//  FYEvaluateViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-6-16.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYEvaluateViewController.h"
#import "WX_Cloud_IsConnection.h"
#import "WXCommonViewClass.h"
#import "FYEvaluateCell.h"
#import "FY_Cloud_getOrderInfo.h"
#import "FY_Cloud_setEvalution.h"
#import "WXCommonDataClass.h"
#import "AsynImageView.h"
@interface FYEvaluateViewController ()
{
    NSArray *productArray;
    NSMutableArray *evaArray;
    UIButton *rightBtn;
}
@end

@implementation FYEvaluateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"评价";
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    rightBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil title:@"提交评论"];
    [rightBtn addTarget:self action:@selector(rightBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    [self initData];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [[FY_Cloud_getOrderInfo share] setDelegate:nil];
    [[FY_Cloud_setEvalution share] setDelegate:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBtnPressed
{

    for (int i = 0; i < evaArray.count; i++) {
        if ([[[evaArray objectAtIndex:i] objectForKey:@"evaluatePoint"] isEqualToString:@""]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"确定" message:@"您有商品尚未评论" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
    }
    NSString *jsonStr = [WXCommonDataClass toJsonStr:evaArray];
    [[FY_Cloud_setEvalution share] setDelegate:(id)self];
    [[FY_Cloud_setEvalution share] setEvaluateWithOrderID:self.orderID andEvalulate:jsonStr];
}
- (void)initData
{
    [[FY_Cloud_getOrderInfo share] setDelegate:(id)self];
    [[FY_Cloud_getOrderInfo share] getOrderInfoWithOrderID:self.orderID];

}
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}

//获取列表成功回调
-(void)syncGetOrderInfoSuccess:(NSArray *)data
{
    productArray = [NSArray arrayWithArray:data];
    evaArray = [[NSMutableArray alloc] initWithCapacity:data.count];
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:@" ",@"productID", @"",@"evaluatePoint",@"",@"evaluateContent",nil];
    for (int i = 0; i < data.count; i++) {
        [evaArray addObject:dic];
    }
    
    [self.tableView reloadData];
    
}
//评价成功回调
-(void)syncSetEvalutionSuccess:(NSString*)EvaString
{
    [WXCommonViewClass showHudInView:self.view title:@"评论成功"];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)hudWasHidden:(MBProgressHUD *)hud
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)syncSetEvalutionFailed:(NSString*)errCode
{
    [WXCommonViewClass showHudInView:self.view title:errCode];
}

-(void)syncGetOrderInfoFailed:(NSString*)errMsg
{
    [WXCommonViewClass showHudInView:self.view title:errMsg];
}

#pragma mark tableView delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return productArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    FYEvaluateCell *cell = (FYEvaluateCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[NSBundle  mainBundle]  loadNibNamed:@"FYEvaluateCell" owner:self options:nil]  lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lbl_name.text = [NSString stringWithFormat:@"%@",[[productArray objectAtIndex:indexPath.row] objectForKey:@"productName"]];
    cell.lbl_count.text = [NSString stringWithFormat:@"购买数量:%@", [[productArray objectAtIndex:indexPath.row]  objectForKey:@"buyedCount"]];
    cell.textFiled.delegate = self;
    cell.textFiled.tag = indexPath.row;
    [cell.textFiled addTarget:self action:@selector(textFieldEditChanged:) forControlEvents:UIControlEventEditingChanged];
    cell.imageView_product.type = 1;
    cell.imageView_product.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    cell.imageView_product.imageURL = [[productArray objectAtIndex:indexPath.row] objectForKey:@"img"];
    [cell.btn_star1 addTarget:self action:@selector(starChange:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btn_star2 addTarget:self action:@selector(starChange:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btn_star3 addTarget:self action:@selector(starChange:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btn_star4 addTarget:self action:@selector(starChange:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btn_star5 addTarget:self action:@selector(starChange:) forControlEvents:UIControlEventTouchUpInside];
    if (!iOS7) {
        cell.btn_star1.adjustsImageWhenHighlighted = NO;
        cell.btn_star2.adjustsImageWhenHighlighted = NO;
        cell.btn_star3.adjustsImageWhenHighlighted = NO;
        cell.btn_star4.adjustsImageWhenHighlighted = NO;
        cell.btn_star5.adjustsImageWhenHighlighted = NO;
    }
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchME:)];
    singleTap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:singleTap];
    return cell;
}

- (void)touchME:(id)sender
{
    [self.view becomeFirstResponder];
}

- (void)starChange:(id)sender
{
    UIView *view=[sender superview];
    UIButton *btn = (UIButton *)sender;
    UITableViewCell *cell;
    if (iOS7) {
        cell=(FYEvaluateCell *)[[view superview] superview];
    }else
    {
        cell=(FYEvaluateCell *)[view superview];
    }
    NSIndexPath* indexPath= [self.tableView indexPathForCell:cell];
    NSString *str;
    switch (btn.tag) {
        case 1:
        {
          FYEvaluateCell *cell = (FYEvaluateCell *)[self.tableView cellForRowAtIndexPath:indexPath];
            [cell.btn_star1 setBackgroundImage:[UIImage imageNamed:@"star_yes.png"] forState:UIControlStateNormal];
            [cell.btn_star2 setBackgroundImage:[UIImage imageNamed:@"star_no.png"] forState:UIControlStateNormal];
            [cell.btn_star3 setBackgroundImage:[UIImage imageNamed:@"star_no.png"] forState:UIControlStateNormal];
            [cell.btn_star4 setBackgroundImage:[UIImage imageNamed:@"star_no.png"] forState:UIControlStateNormal];
            [cell.btn_star5 setBackgroundImage:[UIImage imageNamed:@"star_no.png"] forState:UIControlStateNormal];
            str = cell.textFiled.text;
        }
            break;
        case 2:
        {
            FYEvaluateCell *cell = (FYEvaluateCell *)[self.tableView cellForRowAtIndexPath:indexPath];
           
            [cell.btn_star1 setBackgroundImage:[UIImage imageNamed:@"star_yes.png"] forState:UIControlStateNormal];
            [cell.btn_star2 setBackgroundImage:[UIImage imageNamed:@"star_yes.png"] forState:UIControlStateNormal];
            [cell.btn_star3 setBackgroundImage:[UIImage imageNamed:@"star_no.png"] forState:UIControlStateNormal];
            [cell.btn_star4 setBackgroundImage:[UIImage imageNamed:@"star_no.png"] forState:UIControlStateNormal];
            [cell.btn_star5 setBackgroundImage:[UIImage imageNamed:@"star_no.png"] forState:UIControlStateNormal];
            str = cell.textFiled.text;
        }
            break;
        case 3:
        {
            FYEvaluateCell *cell = (FYEvaluateCell *)[self.tableView cellForRowAtIndexPath:indexPath];
            [cell.btn_star1 setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"star_yes" ofType:@"png"]] forState:UIControlStateNormal];
            [cell.btn_star2 setBackgroundImage:[UIImage imageNamed:@"star_yes.png"] forState:UIControlStateNormal];
            [cell.btn_star3 setBackgroundImage:[UIImage imageNamed:@"star_yes.png"] forState:UIControlStateNormal];
            [cell.btn_star4 setBackgroundImage:[UIImage imageNamed:@"star_no.png"] forState:UIControlStateNormal];
            [cell.btn_star5 setBackgroundImage:[UIImage imageNamed:@"star_no.png"] forState:UIControlStateNormal];
            str = cell.textFiled.text;
        }
            break;
        case 4:
        {
            FYEvaluateCell *cell = (FYEvaluateCell *)[self.tableView cellForRowAtIndexPath:indexPath];
            [cell.btn_star1 setBackgroundImage:[UIImage imageNamed:@"star_yes.png"] forState:UIControlStateNormal];
            [cell.btn_star2 setBackgroundImage:[UIImage imageNamed:@"star_yes.png"] forState:UIControlStateNormal];
            [cell.btn_star3 setBackgroundImage:[UIImage imageNamed:@"star_yes.png"] forState:UIControlStateNormal];
            [cell.btn_star4 setBackgroundImage:[UIImage imageNamed:@"star_yes.png"] forState:UIControlStateNormal];
            [cell.btn_star5 setBackgroundImage:[UIImage imageNamed:@"star_no.png"] forState:UIControlStateNormal];
            str = cell.textFiled.text;
        }
            break;
        case 5:
        {
            FYEvaluateCell *cell = (FYEvaluateCell *)[self.tableView cellForRowAtIndexPath:indexPath];
            [cell.btn_star1 setBackgroundImage:[UIImage imageNamed:@"star_yes.png"] forState:UIControlStateNormal];
            [cell.btn_star2 setBackgroundImage:[UIImage imageNamed:@"star_yes.png"] forState:UIControlStateNormal];
            [cell.btn_star3 setBackgroundImage:[UIImage imageNamed:@"star_yes.png"] forState:UIControlStateNormal];
            [cell.btn_star4 setBackgroundImage:[UIImage imageNamed:@"star_yes.png"] forState:UIControlStateNormal];
            [cell.btn_star5 setBackgroundImage:[UIImage imageNamed:@"star_yes.png"] forState:UIControlStateNormal];
            str = cell.textFiled.text;
        }
            break;
        default:
            break;
    }
    NSString *ID = [NSString stringWithFormat:@"%@", [[productArray objectAtIndex:indexPath.row]  objectForKey:@"productId"]];
    NSString *point = [NSString stringWithFormat:@"%d", btn.tag];
    if (str == nil) {
        str = @"";
    }
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:ID,@"productID", point,@"evaluatePoint",str,@"evaluateContent",nil];
    [evaArray replaceObjectAtIndex:indexPath.row withObject:dic];
}

- (void)textFieldEditChanged:(UITextField *)textField
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:[evaArray objectAtIndex:textField.tag]];
    [dic setObject:textField.text forKey:@"evaluateContent"];
    [evaArray replaceObjectAtIndex:textField.tag withObject:dic];
}

#pragma mark UITextField delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
