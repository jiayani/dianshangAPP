//
//  FYEvaluateCell.h
//  DianShangApp
//
//  Created by Fuy on 14-6-16.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AsynImageView;
@interface FYEvaluateCell : UITableViewCell
@property (weak, nonatomic) IBOutlet AsynImageView *imageView_product;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_count;
@property (weak, nonatomic) IBOutlet UITextField *textFiled;
@property (weak, nonatomic) IBOutlet UIButton *btn_star1;
@property (weak, nonatomic) IBOutlet UIButton *btn_star2;
@property (weak, nonatomic) IBOutlet UIButton *btn_star3;
@property (weak, nonatomic) IBOutlet UIButton *btn_star4;
@property (weak, nonatomic) IBOutlet UIButton *btn_star5;

@end
