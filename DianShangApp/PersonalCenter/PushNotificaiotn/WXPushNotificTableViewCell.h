//
//  WXPushNotificTableViewCell.h
//  DianShangApp
//
//  Created by 霞 王 on 14-6-23.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WXPushNotificTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *pushContentTextView;
@property (weak, nonatomic) IBOutlet UILabel *pushDateLabel;
@property (weak, nonatomic) IBOutlet UITextView *titleTextView;

@end
