//
//  WXBusinessPushNotificViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-23.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXBusinessPushNotificViewController.h"
#import "WXCommonViewClass.h"
#import "WXPushNotificTableViewCell.h"
#import "WX_Cloud_GetPushNotificList.h"
#import "WX_Cloud_SetRedPushNotific.h"
#import "WXFavorableActivityDetailViewController.h"
#import "XYGoodsInforViewController.h"
#import "XYNoDataView.h"

@interface WXBusinessPushNotificViewController ()

@end

@implementation WXBusinessPushNotificViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"商家推送", @"商家推送页面标题");
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initAllController];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
    [self syncGetPushNoteList];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[WX_Cloud_GetPushNotificList share] setDelegate:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - All My Methods
- (void)initAllController{
    /*begin:设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    //end

}
//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)reloadUI{
    titleHeightArray = [[NSMutableArray alloc]init];
    contentHeightArray = [[NSMutableArray alloc]init];
    for (int i = 0; i < pushNotificArray.count; i++) {
        NSDictionary *dic = [pushNotificArray objectAtIndex:i];
        NSString *content = [dic objectForKey:@"content"];
        CGFloat contentHeight = [self getHeightOfLabel:content fontSize:12 labelWidth:300];
        [contentHeightArray insertObject:[NSNumber numberWithFloat:contentHeight + 10] atIndex:i];
        
        NSString *title = [dic objectForKey:@"title"];
        CGFloat titleHeight = [self getHeightOfLabel:title fontSize:15 labelWidth:300];
        [titleHeightArray insertObject:[NSNumber numberWithFloat:titleHeight + 10] atIndex:i];
    }
    [self.myTableView reloadData];
}
- (void)syncGetPushNoteList{
    [[WX_Cloud_GetPushNotificList share] setDelegate:(id)self];
    [[WX_Cloud_GetPushNotificList share] syncGetPushNotific];
}
//返回字符串的高度
- (CGFloat)getHeightOfLabel:(NSString *)str fontSize:(CGFloat)fontSize labelWidth:(CGFloat)labelWidth {
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    //设置一个行高上限
    CGSize size = CGSizeMake(labelWidth,2000);
    //计算实际frame大小，并将label的frame变成实际大小
    CGSize labelsize = [str sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
    return labelsize.height;
}

-(void)syncGetReviewFailed
{
    noDataView =[[XYNoDataView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    noDataView.megUpLabel.text = @"暂时没有数据";
    noDataView.megDownLabel.text = @"先去其他页面看看吧";
    [self.view addSubview:noDataView];
    [loadingView removeFromSuperview];
    [[WX_Cloud_GetPushNotificList share] setDelegate:nil];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return pushNotificArray.count;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d%d", [indexPath row],[indexPath section]];//以indexPath来唯一确定cell
    WXPushNotificTableViewCell *cell;
    
    if (cell == nil) {
        cell = (WXPushNotificTableViewCell *)[[WXPushNotificTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                                                         reuseIdentifier: CellIdentifier];
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"WXPushNotificTableViewCell" owner:self options:nil];
        cell = (WXPushNotificTableViewCell *)[nib objectAtIndex:0];
        
   }
    cell.backgroundView.backgroundColor = [UIColor clearColor];
    int section = indexPath.section;
    NSDictionary *dic = [pushNotificArray objectAtIndex:section];
    cell.titleTextView.text = [dic objectForKey:@"title"];
    cell.pushContentTextView.text = [dic objectForKey:@"content"];
    cell.pushDateLabel.text = [dic objectForKey:@"addtime"];
    //根据文字内容来调整高度
    CGRect titleFrame = cell.titleTextView.frame;
    titleFrame.size.height = [[titleHeightArray objectAtIndex:section]floatValue];
    cell.titleTextView.frame = titleFrame;
    CGRect contentFrame = cell.pushContentTextView.frame;
    contentFrame.origin.y = titleFrame.origin.y + titleFrame.size.height ;
    contentFrame.size.height = [[contentHeightArray objectAtIndex:section]floatValue];
    cell.pushContentTextView.frame = contentFrame;
    //已读跟不读取得到的数据不一样
    int read = [[dic objectForKey:@"isread"]intValue];
    if (read == 0)
    {
        cell.titleTextView.textColor = [UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1.0];
        cell.pushContentTextView.textColor = [UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1.0];
        
    }
    else if (read == 1)
    {
        cell.titleTextView.textColor = [UIColor colorWithRed:178.0/255.0 green:178.0/255.0 blue:178.0/255.0 alpha:1.0];
        cell.pushContentTextView.textColor = [UIColor colorWithRed:178.0/255.0 green:178.0/255.0 blue:178.0/255.0 alpha:1.0];
        
    }

    //设置选中时没有背景色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    int section = indexPath.section;
    NSDictionary *dic = [pushNotificArray objectAtIndex:section];
    int type = [[dic objectForKey:@"type"]intValue];
    
    NSString *url = [dic objectForKey:@"url"];
    //通知后台该推送已读
    int msg_id = [[dic objectForKey:@"msg_id"]integerValue];
    int read = [[dic objectForKey:@"isread"]intValue];
    if (read == 0) {
        [WX_Cloud_SetRedPushNotific setRedPushNotific:msg_id];
        //修改字体颜色
        WXPushNotificTableViewCell *cell = (WXPushNotificTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.titleTextView.textColor = [UIColor colorWithRed:178.0/255.0 green:178.0/255.0 blue:178.0/255.0 alpha:1.0];
        cell.pushContentTextView.textColor = [UIColor colorWithRed:178.0/255.0 green:178.0/255.0 blue:178.0/255.0 alpha:1.0];
        
    }
    
    //2:商品详情 3:优惠活动详情 4，5广告跳转网页
    switch (type) {
        case 2:     //新增单品
        {
            XYGoodsInforViewController *goodsInforViewController = [[XYGoodsInforViewController alloc]initWithNibName:@"XYGoodsInforViewController" bundle:nil];
            goodsInforViewController.productId = url;
            [self.navigationController pushViewController:goodsInforViewController animated:YES];
            break;
        }
        case 3: {    //新增优惠活动
        
            WXFavorableActivityDetailViewController *favorableActivityDetailViewController = [[WXFavorableActivityDetailViewController alloc]initWithNibName:@"WXFavorableActivityDetailViewController" bundle:nil];
            favorableActivityDetailViewController.keyStr = url;
            [self.navigationController pushViewController:favorableActivityDetailViewController animated:YES];
            break;
        }
        case 4: case 5:    //外链
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
            break;
        }
            
        default:
            break;
    }
    
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    int section = indexPath.section;
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        
       [pushNotificArray removeObjectAtIndex:section];
        NSIndexSet *indexSet = [[NSIndexSet alloc]initWithIndex:section];
        [tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationBottom];
        
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"删除";
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView* myView = [[UIView alloc] init];
    myView.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0f];
    myView.layer.borderWidth = 0;
    return myView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    int section = indexPath.section;
    CGFloat titleHeight = [[titleHeightArray objectAtIndex:section]floatValue];
    CGFloat contentHeight = [[contentHeightArray objectAtIndex:section]floatValue];
    return titleHeight + contentHeight + 20;
}

#pragma mark - WX_Cloud_Delegate Methods
//无网络和意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[WX_Cloud_GetPushNotificList share] setDelegate:nil];
    [self syncGetReviewFailed];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}
//获得推送
-(void)syncGetPushNotificSuccess:(NSArray *)dataArray{
    [[WX_Cloud_GetPushNotificList share] setDelegate:nil];
    if (dataArray != nil && dataArray.count > 0) {
        [loadingView removeFromSuperview];  
    }else{
        [self syncGetReviewFailed];
    }
   
    pushNotificArray = [dataArray mutableCopy];
    [self reloadUI];
}
-(void)syncGetPushNotificpeFailed:(NSString *)errCode{
    [[WX_Cloud_GetPushNotificList share] setDelegate:nil];
    [self syncGetReviewFailed];
    

}
@end
