//
//  WXBusinessPushNotificViewController.h
//  DianShangApp
//
//  Created by 霞 王 on 14-6-23.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"
@class XYLoadingView;
@class XYNoDataView;

@interface WXBusinessPushNotificViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,WX_Cloud_Delegate>{
    NSMutableArray *pushNotificArray;
    XYLoadingView *loadingView;
    XYNoDataView *noDataView;
    NSMutableArray *contentHeightArray;
    NSMutableArray *titleHeightArray;
}
@property (weak, nonatomic) IBOutlet UITableView *myTableView;

@end
