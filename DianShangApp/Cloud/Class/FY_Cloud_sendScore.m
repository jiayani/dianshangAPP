//
//  FY_Cloud_sendScore.m
//  DianShangApp
//
//  Created by 霞 王 on 14-7-7.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_sendScore.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_sendScore * sendScore = nil;
@implementation FY_Cloud_sendScore
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!sendScore)
    {
        sendScore = [[FY_Cloud_sendScore alloc] init];
    }
    return sendScore;
}

-(void)sendScoreWithShareType:(NSString *)shareType andShareTime:(NSString *)shareTime
{
    
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr;
        urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/handselCredits?uid=%@&shareType=%@&shopid=%@&shareTime=%@",singletonClass.currentUserId, shareType, singletonClass.shopid, shareTime]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSDictionary *allGiftInfo = [dic objectForKey:@"data"];
        [delegate syncSendScoreSuccess:allGiftInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncSendScoreFailed:@"请求失败"];
        }
        else
        {
            [delegate syncSendScoreFailed:[dic objectForKey:@"message"]];
        }
    }
}

@end
