//
//  WX_Cloud_checkPhoneAndValidCodeForRegist.m
//  TianLvApp
//
//  Created by 霞 王 on 14-9-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_checkPhoneAndValidCodeForRegist.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
#import "MyMD5.h"

extern NSString *serviceURL;
extern NSString * shopid;

static WX_Cloud_checkPhoneAndValidCodeForRegist *
checkPhoneAndValidCodeForRegist = nil;

@implementation WX_Cloud_checkPhoneAndValidCodeForRegist

@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!checkPhoneAndValidCodeForRegist)
    {
        checkPhoneAndValidCodeForRegist = [[WX_Cloud_checkPhoneAndValidCodeForRegist alloc] init];
    }
    return checkPhoneAndValidCodeForRegist;
}

-(void)checkPhoneAndValidCodeForRegist:(NSString *)checkPhone validCode:(NSString *)validCode;
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/checkPhoneAndValidCodeForRegist?shopid=%@&checkPhone=%@&validCode=%@",singletonClass.shopid,checkPhone,validCode]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
  [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    /*收到成功代码后的操作*/
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        [delegate checkPhoneAndValidCodeForRegistSuccess:[dic objectForKey:@"data"]];
    }else
    {
        [delegate checkPhoneAndValidCodeForRegistFailed:[dic objectForKey:@"message"]];
    }
   
}

@end




