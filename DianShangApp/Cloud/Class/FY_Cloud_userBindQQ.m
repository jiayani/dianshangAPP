//
//  FY_Cloud_userBindQQ.m
//  TianLvApp
//
//  Created by Fuy on 14-9-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_userBindQQ.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_userBindQQ * setQQLogin = nil;
@implementation FY_Cloud_userBindQQ
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!setQQLogin)
    {
        setQQLogin = [[FY_Cloud_userBindQQ alloc] init];
    }
    return setQQLogin;
}

-(void)BindQQWithOpenID:(NSString *)openID andNickName:(NSString *)nickname
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr;
        urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/userBindQQ?&uid=%@&shopid=%@&authorize_id=%@&authorize_nick=%@",singletonClass.currentUserId, singletonClass.shopid, openID, nickname]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSString *weibo = [dic objectForKey:@"message"];
        [delegate syncBindQQSuccess:weibo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncBindQQFailed:@"请求失败"];
        }
        else
        {
            [delegate syncBindQQFailed:[dic objectForKey:@"message"]];
        }
    }
}
@end
