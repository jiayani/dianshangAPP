//
//  WX_Cloud_UserRegister.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_UserRegister.h"
#import "AppDelegate.h"
#import "MyMD5.h"
#import "WX_Cloud_IsConnection.h"
#import "SBJson.h"


static WX_Cloud_UserRegister *userRegister = nil;

@implementation WX_Cloud_UserRegister
@synthesize delegate;
@synthesize recivedData;

+ (id)share
{
    if (!userRegister)
    {
        userRegister = [[WX_Cloud_UserRegister alloc] init];
    }
    return userRegister;
}

-(void)userRegisterWithUserName:(NSString*)username UserPassword:(NSString*)password andType:(int)type andCode:(NSString *)code
{

    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:@"Iphone/userReg"];
        NSURL * url = [[NSURL alloc] initWithString:urlStr];
        NSString * body = [NSString stringWithFormat:@"shopid=%@&username=%@&password=%@&tokenID=%@&loginType=%d&validCode=%@", singletonClass.shopid, username, password,singletonClass.deviceTokenStr, type, code];
        NSData * bodyData = [body dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:bodyData];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {    //无网络
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{    //意外错误
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];

    NSDictionary * dic = [[NSDictionary alloc] init];
    dic = [str JSONValue];
    
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSMutableDictionary * dataDic = [dic objectForKey:@"data"];
        [delegate userRegisterSuccess:dataDic];
    }
    else{
       

        [delegate userRegisterFailed:[dic objectForKey:@"message"]];
    }
}


@end
