//
//  FY_Cloud_GiftListSync.h
//  DianShangApp
//
//  Created by Fuy on 14-6-3.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"
@interface FY_Cloud_GiftListSync : NSObject
{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;
    
    NSString * secondMenuID_;
    
    BOOL isFirstPage;
    int _giftType;  //自有0 哇点1
}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)giftSync:(NSString *)firstMenuID second:(NSString *)secondMenuID page:(int)page giftType:(int)type;
@end
