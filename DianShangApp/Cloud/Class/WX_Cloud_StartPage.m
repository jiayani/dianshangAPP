//
//  WYDStartPage.m
//  TianLvApp
//
//  Created by 霞 王 on 14-4-24.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//
//

#import "WX_Cloud_StartPage.h"
#import "SBJson.h"
#import "WYDDownloadImage.h"
#import "GuideAnimation.h"
#import "AppDelegate.h"
#import "WX_Cloud_IsConnection.h"


static WX_Cloud_StartPage *startPage = nil;
@implementation WX_Cloud_StartPage
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!startPage)
    {
        startPage = [[WX_Cloud_StartPage alloc] init];
    }
    return startPage;
}

-(void)loadingStartPageAndImage
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/StartPic?shopid=%@",singletonClass.shopid]];   //shopid=? platform=0  0代表ios
        NSURL * url = [NSURL URLWithString:urlStr];

        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:30.0];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
       [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
   
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];

    NSDictionary * dic = [str JSONValue];

    /*收到成功代码后的操作*/
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        closeTime = 0;
        
        
        NSDictionary * dataDic = [dic objectForKey:@"data"];
        
        //begin：启动页图图片
        NSDictionary * startPageDic = [dataDic objectForKey:@"startpage"];
        NSString * tempStartpageImageStr = [startPageDic objectForKey:@"img"];
        NSString * startpageTimeStr = [startPageDic objectForKey:@"imgDate"];
        
        //轮播图片
        NSArray * tempGuidImageArray = [dataDic objectForKey:@"image"];
        NSMutableArray * guidImageArray = [NSMutableArray array];

        for (int i=0; i<[tempGuidImageArray count]; i++)
        {
            NSDictionary * image_Dic = [tempGuidImageArray objectAtIndex:i];
            NSString * imageUrlPathStr = [image_Dic objectForKey:@"img"];
            NSString * imageUrlPathTime = [image_Dic objectForKey:@"imgDate"];
     
            [guidImageArray addObject:imageUrlPathStr];
            if ([imageUrlPathTime longLongValue] > closeTime)
            {

                closeTime = (long)[imageUrlPathTime longLongValue];
            }
        }
        if ([startpageTimeStr longLongValue] > closeTime)
        {
            closeTime = (long)[startpageTimeStr longLongValue];
        }
        
        AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *context=[appDelegate managedObjectContext];

        NSEntityDescription * entity = [NSEntityDescription entityForName:@"GuideAnimation" inManagedObjectContext:context];
        NSFetchRequest * fetch = [[NSFetchRequest alloc] init];
        [fetch setEntity:entity];
        NSArray * tmpArr = [context executeFetchRequest:fetch error:nil];
        GuideAnimation * guide;
        if ([tmpArr count] == 0)
        {
            guide = [NSEntityDescription insertNewObjectForEntityForName:@"GuideAnimation" inManagedObjectContext:context];
            guide.updateDate = [NSDate dateWithTimeIntervalSince1970:1];
        }
        else
        {
            guide = [tmpArr objectAtIndex:0];
        }
        
        guide.appStartPic = tempStartpageImageStr;
        guide.guideAnimationPic = guidImageArray;
        [context save:nil];
        
        
        NSDate * dat = [NSDate dateWithTimeIntervalSince1970:closeTime];
        if ([guide.updateDate compare:dat] == NSOrderedAscending)
        {
            dispatch_async(queue, ^{
                 [[WYDDownloadImage share] downloadImage:tempStartpageImageStr];
            });
        }

        for (int i=1; i<=[guidImageArray count]; i++)
        {
            NSString *serverImageUrlStr = [guidImageArray objectAtIndex:i -1];
            if ([guide.updateDate compare:dat] == NSOrderedAscending)
            {
                dispatch_async(queue, ^{
                    [[WYDDownloadImage share] downloadImage:serverImageUrlStr];
                });
            }
        }
        
        [delegate syncSuccess:nil];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncFailed:@"请求失败"];
        }
        else
        {
            [delegate syncFailed:[dic objectForKey:@"message"]];
        }
    }
    
}

@end
