//
//  WX_Cloud_SendDeviceToken.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-24.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_SendDeviceToken.h"
#import "WXCommonDateClass.h"
#import "SBJson.h"
#import "WX_Cloud_IsConnection.h"
#import "WXCommonSingletonClass.h"


@implementation WX_Cloud_SendDeviceToken
@synthesize delegate;


+ (void)sendDeviceToken:(NSString*)token
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
         NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/token?shopid=%@&token=%@&sys_type=2",singletonClass.shopid,token]];
        

        NSURL *url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        [NSURLConnection connectionWithRequest:request delegate:self];
       
    }
    
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{

}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{

}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{

}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
}

@end

