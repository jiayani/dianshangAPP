//
//  FY_Cloud_bindOtherForQQ.h
//  TianLvApp
//
//  Created by Fuy on 14-9-16.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"
#import "AppDelegate.h"
@interface FY_Cloud_bindOtherForQQ : NSObject
{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;
    
}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)bindOtherWithAccount:(NSString *)account andPassword:(NSString *)password andQQToken:(NSString *)qqToken andNickname:(NSString *)nickname;

@end
