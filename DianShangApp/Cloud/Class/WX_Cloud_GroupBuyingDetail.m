//
//  WX_Cloud_GroupBuyingDetail.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-13.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_GroupBuyingDetail.h"
#import "WXCommonDateClass.h"
#import "SBJson.h"
#import "WX_Cloud_IsConnection.h"
#import "WXCommonSingletonClass.h"

static WX_Cloud_GroupBuyingDetail * groupBuyingDetail = nil;

@implementation WX_Cloud_GroupBuyingDetail
@synthesize delegate;
@synthesize recivedData;

+ (id)share
{
    if (!groupBuyingDetail)
    {
        groupBuyingDetail = [[WX_Cloud_GroupBuyingDetail alloc] init];
    }
    return groupBuyingDetail;
}

- (void)syncGroupBuyingDetail:(NSString *)groupbuyIdStr
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_groupbuyDetail?shopid=%@&groupbuyId=%@&version=%@", singletonClass.shopid, groupbuyIdStr, singletonClass.version]];
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }else{
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        [delegate syncGetGroupBuyDetailSuccess:[dic objectForKey:@"data"]];
    }
    else
    {
        NSString * str = [dic objectForKey:@"message"];
        [delegate syncGetGroupBuyDetailFailed:str];
    }
}

@end




