//
//  XY_Cloud_GoodInforData.m
//  DianShangApp
//
//  Created by wa on 14-6-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XY_Cloud_GoodInforData.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"

static XY_Cloud_GoodInforData * goodInforData = nil;
@implementation XY_Cloud_GoodInforData
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!goodInforData)
    {
        goodInforData = [[XY_Cloud_GoodInforData alloc] init];
    }
    return goodInforData;
}
-(void)goodInforSync:(int)productId
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr;
        WXCommonSingletonClass *  singletonClass  = [WXCommonSingletonClass share];
        urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_productDetail?shopid=%@&uid=%@&productId=%i&version=%@", singletonClass.shopid, singletonClass.currentUserId, productId, singletonClass.version]];
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSDictionary * dataDic = [dic objectForKey:@"data"];
        [delegate goodInforSyncSuccess:dataDic];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate goodInforSyncFailed:@"请求失败"];
        }
        else
        {
            NSString * message = [dic objectForKey:@"message"];
            [delegate goodInforSyncFailed:message];
        }
    }
}


@end
