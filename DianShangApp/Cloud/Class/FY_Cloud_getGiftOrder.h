//
//  FY_Cloud_getGiftOrder.h
//  DianShangApp
//
//  Created by Fuy on 14-6-17.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"
@interface FY_Cloud_getGiftOrder : NSObject
{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;

}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)getGiftOrderListWithPage:(int)page andState:(int)state andWooGiftsCount:(int)count;
@end
