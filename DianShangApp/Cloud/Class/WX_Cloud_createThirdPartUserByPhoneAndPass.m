//
//  WX_Cloud_createThirdPartUserByPhoneAndPass.m
//  TianLvApp
//
//  Created by 霞 王 on 14-9-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_createThirdPartUserByPhoneAndPass.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
#import "MyMD5.h"


static WX_Cloud_createThirdPartUserByPhoneAndPass *createThirdPartUserByPhoneAndPass = nil;
@implementation WX_Cloud_createThirdPartUserByPhoneAndPass
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!createThirdPartUserByPhoneAndPass)
    {
        createThirdPartUserByPhoneAndPass = [[WX_Cloud_createThirdPartUserByPhoneAndPass alloc] init];
    }
    return createThirdPartUserByPhoneAndPass;
}

-(void)createThirdPartUserByPhoneAndPass:(NSMutableDictionary *)dic
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSDictionary *myDic = singletonClass.qqUserInfoDic;
    
        NSString *authorize_type = [dic objectForKey:@"authorize_type"];
        NSString *authorize_nick = [myDic objectForKey:@"nickname"];
        NSString *password = [MyMD5 md5:[dic objectForKey:@"password"]];
        NSString *phone = [dic objectForKey:@"phone"];
        NSString *validCode = [dic objectForKey:@"validCode"];
        NSString *sex = [myDic objectForKey:@"gender"];
        NSString *city = [NSString stringWithFormat:@"%@%@",[myDic objectForKey:@"province"],[myDic objectForKey:@"city"]];
        
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/createThirdPartUserByPhoneAndPass?shopid=%@&authorize_id=%@&authorize_type=%@&authorize_nick=%@&password=%@&phone=%@&validCode=%@&sex=%@&city=%@",singletonClass.shopid,singletonClass.qqOpenID,authorize_type,authorize_nick,password,phone,validCode,sex,city]];
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
     [delegate accidentError:NoHaveNetwork errorCode:@"1"];    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
[delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    /*收到成功代码后的操作*/
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        [delegate createThirdPartUserByPhoneAndPassSuccess:[dic objectForKey:@"data"]];
    }else
    {
        [delegate createThirdPartUserByPhoneAndPassFailed:[dic objectForKey:@"message"]];
    }
    
}

@end



