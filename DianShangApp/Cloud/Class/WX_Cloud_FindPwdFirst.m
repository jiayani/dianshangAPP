//
//  WX_Cloud_FindPwdFirst.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_FindPwdFirst.h"
#import "SBJson.h"
#import "WX_Cloud_IsConnection.h"
#import "MyMD5.h"


static WX_Cloud_FindPwdFirst *findPwdFirst = nil;

@implementation WX_Cloud_FindPwdFirst

@synthesize delegate;
@synthesize recivedData;

+ (id)share
{
    if (!findPwdFirst)
    {
        findPwdFirst = [[WX_Cloud_FindPwdFirst alloc] init];
    }
    return findPwdFirst;
}

- (void)findPasswordFirst:(int)type phoneOrEmailStr:(NSString*)phoneOrEmailStr
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        long myTime = (long)[[NSDate date] timeIntervalSince1970];
        NSString *tokenStr = [NSString stringWithFormat:@"%@8ca8188ca3d9b1c8%ld",phoneOrEmailStr,myTime];
        NSString *md5TokenStr = [MyMD5 md5:tokenStr];
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/findMyPasswordOne?type=%d&number=%@&shopid=%@&token=%@&time=%ld",type,phoneOrEmailStr,singletonClass.shopid,md5TokenStr,myTime]];
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlStr];

        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {   //无网络
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];

    NSDictionary * dic = [str JSONValue];

    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSString * backUid = [[dic objectForKey:@"data"] objectForKey:@"uid"];
        [delegate verifyPhoneOrEmailSuccess:backUid];
    }
    else
    {
        NSString * errMsg = [dic objectForKey:@"message"];
        [delegate verifyPhoneOrEmailFailed:nil errCode:errMsg];
    }
    
}

@end


