//
//  FY_Cloud_getInfoOfProductDeatil.m
//  DianShangApp
//
//  Created by Fuy on 14-6-18.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_getInfoOfProductDeatil.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_getInfoOfProductDeatil * getGiftInfo = nil;
@implementation FY_Cloud_getInfoOfProductDeatil
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!getGiftInfo)
    {
        getGiftInfo = [[FY_Cloud_getInfoOfProductDeatil alloc] init];
    }
    return getGiftInfo;
}

-(void)getGiftOrderListWithOrderID:(NSString *)orderID
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_getOrderInfo?shopid=%@&uid=%@&orderFormID=%@", singletonClass.shopid, singletonClass.currentUserId, orderID]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSDictionary * allInfo = [dic objectForKey:@"data"];
        [delegate syncGetMyProductSuccess:allInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncGetMyProductFailed:@"请求失败"];
        }
        else
        {
            [delegate syncGetMyProductFailed:[dic objectForKey:@"message"]];
        }
    }
}

@end
