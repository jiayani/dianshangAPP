//
//  FY_Cloud_Pay.h
//  DianShangApp
//
//  Created by Fuy on 14-6-24.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"
@interface FY_Cloud_Pay : NSObject
{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;

}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)getOrderWithGoods:(NSString *)goods andGroupBuys:(NSString *)groupBuys andRemark:(NSString *)remark andPayment:(int)payment andContactld:(NSString *)contactId andConsumeType:(NSString *)consumeType;

@end
