//
//  XY_Cloud_ProductList.m
//  DianShangApp
//
//  Created by wa on 14-6-10.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XY_Cloud_ProductList.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"

static XY_Cloud_ProductList * productList = nil;
@implementation XY_Cloud_ProductList
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!productList)
    {
        productList = [[XY_Cloud_ProductList alloc] init];
    }
    return productList;
}
-(void)productListSyncWithType:(int)type andKeyword:(NSString *)keyword andSortype:(int)sortType  andClass1:(int)class1_id andClass2:(int)class2_id andPage:(int)page andAtt1:(NSString *)att1_id andAtt2:(NSString *)att2_id
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr;
        if (keyword == nil) {
            keyword = @"";
        }
        WXCommonSingletonClass *  singletonClass  = [WXCommonSingletonClass share];
        urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_productList?shopid=%@&type=%i&keyword=%@&sortType=%i&class1_id=%i&class2_id=%i&att1_id=%d&att2_id=%d&page=%i",singletonClass.shopid,type,keyword,sortType,class1_id,class2_id,[att1_id intValue],[att2_id intValue],page]];
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
//        NSDictionary * dataDic = [dic objectForKey:@"data"];
        [delegate productListSyncSuccess:dic];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate productListSyncFailed:@"请求失败"];
        }
        else
        {
            NSString * message = [dic objectForKey:@"message"];
            [delegate productListSyncFailed:message];
        }
    }
}

@end
