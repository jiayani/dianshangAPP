//
//  JYNSendKeywords.m
//  TianLvApp
//
//  Created by wa on 14-9-10.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "JYNSendKeywords.h"
#import "SBJson.h"
#import "WX_Cloud_IsConnection.h"


static JYNSendKeywords * sendKeywords = nil;
@implementation JYNSendKeywords
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!sendKeywords)
    {
        sendKeywords = [[JYNSendKeywords alloc] init];
    }
    return sendKeywords;
}

-(void)getPasswordWithKeywords:(NSString*)keywords andemail:(NSString *) email flag:(int)keywordsID
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/userBindMail?email=%@&shopid=%@&uid=%@&validCode=%@&email=%@",keywords,singletonClass.shopid,singletonClass.currentUserId,keywords,email]];
        NSURL * url = [[NSURL alloc] initWithString:urlStr];
        NSURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100 )
    {
        [delegate checkPhoneAndValidCodeForRegistSuccess:[dic objectForKey:@"data"]];
    }
    else
    {
        [delegate checkPhoneAndValidCodeForRegistFailed:[dic objectForKey:@"message"]];
    }
}

@end

