//
//  WX_Cloud_getGiftCart.h
//  DianShangApp
//
//  Created by 霞 王 on 14-6-12.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"

@interface WX_Cloud_getGiftCart : NSObject

@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData *recivedData;

+ (id)share;
- (void)syncGiftCart:(NSString *)jsonStr;
@end
