//
//  WX_Cloud_UserLogin.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-19.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_UserLogin.h"
#import "WX_Cloud_IsConnection.h"
#import "SBJson.h"
#import "MyMD5.h"
#import "WXCommonDateClass.h"

static WX_Cloud_UserLogin *userLogin = nil;


@implementation WX_Cloud_UserLogin
@synthesize delegate;
@synthesize recivedData;

+ (id)share
{
    if (!userLogin)
    {
        userLogin = [[WX_Cloud_UserLogin alloc] init];
    }
    return userLogin;
}

- (void)userLoginWithUserName:(NSString*)username UserPassword:(NSString*)password
{

    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:@"Iphone/userLogin"];
        NSURL * url = [[NSURL alloc] initWithString:urlStr];
        NSString * body = [NSString stringWithFormat:@"shopid=%@&username=%@&password=%@&tokenID=%@",singletonClass.shopid,username,password,singletonClass.deviceTokenStr];
        NSData * bodyData = [body dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:bodyData];
        [request setTimeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        //无网络
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [[NSDictionary alloc] init];
    dic = [str JSONValue];
    
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        //数据解析
        NSMutableDictionary * tempDic = [dic objectForKey:@"data"];
        [delegate userLoginSuccess:tempDic];
    }
    else{
        
        NSString * codeMessage = [dic objectForKey:@"message"];
        [delegate userLoginFailed:codeMessage];
    }
    
}


@end
