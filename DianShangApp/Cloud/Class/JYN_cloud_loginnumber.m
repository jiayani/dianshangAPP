//
//  JYN_cloud_loginnumber.m
//  TianLvApp
//
//  Created by wa on 14-9-29.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "JYN_cloud_loginnumber.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static JYN_cloud_loginnumber *loginnumber = nil;
@implementation JYN_cloud_loginnumber
@synthesize delegate;
@synthesize recivedData;
+(id)share
{
    if (!loginnumber)
    {
        loginnumber = [[JYN_cloud_loginnumber alloc] init];
    }
    return loginnumber;
}
-(void)sendNumberToBackground
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [@"http：//admin.kstapp.com/Iphone/userStatistic" stringByAppendingString:@"Iphone/userStatistic"];
        NSURL * url = [[NSURL alloc] initWithString:urlStr];
        NSString * body = [NSString stringWithFormat:@"shopid=%@&uid=%@&type=nil",singletonClass.shopid,singletonClass.currentUserId];
        NSData * bodyData = [body dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:bodyData];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
       [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }

}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        [delegate syncGetPushNotificSuccess:[dic objectForKey:@"data"]];
    }
    else
    {
        NSString * str = [dic objectForKey:@"message"];
        [delegate syncGetPushNotificpeFailed:str];
    }
}
@end

