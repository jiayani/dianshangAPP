//
//  FY_Cloud_deleteColect.m
//  DianShangApp
//
//  Created by Fuy on 14-6-12.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_deleteColect.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_deleteColect * deleteCollect = nil;
@implementation FY_Cloud_deleteColect
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!deleteCollect)
    {
        deleteCollect = [[FY_Cloud_deleteColect alloc] init];
    }
    return deleteCollect;
}

- (void)deleteCollectWithProductID:(int)productID
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_deleteCollection?shopid=%@&uid=%@&productId=%d", singletonClass.shopid,singletonClass.currentUserId, productID]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSString * allInfo = [dic objectForKey:@"message"];
        [delegate syncDeleteCollectSuccess:allInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncDeleteCollectFailed:@"请求失败"];
        }
        else
        {
            [delegate syncDeleteCollectFailed:[dic objectForKey:@"message"]];
        }
    }
}

@end
