//
//  WX_Cloud_FindPwdSecond.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-21.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"

@interface WX_Cloud_FindPwdSecond : NSObject{
    id <WX_Cloud_Delegate> delegate;
}


@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData *recivedData;

+ (id)share;
- (void)findPasswordWithKeywords:(NSString*)keywords flag:(int)keywordsID type:(int)type;
@end
