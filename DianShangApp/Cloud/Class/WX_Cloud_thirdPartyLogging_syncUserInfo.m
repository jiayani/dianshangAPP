//
//  WX_Cloud_thirdPartyLogging_syncUserInfo.m
//  TianLvApp
//
//  Created by 霞 王 on 14-9-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_thirdPartyLogging_syncUserInfo.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"



static WX_Cloud_thirdPartyLogging_syncUserInfo *thirdPartyLogging_syncUserInfo = nil;
@implementation WX_Cloud_thirdPartyLogging_syncUserInfo

@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!thirdPartyLogging_syncUserInfo)
    {
        thirdPartyLogging_syncUserInfo = [[WX_Cloud_thirdPartyLogging_syncUserInfo alloc] init];
    }
    return thirdPartyLogging_syncUserInfo;
}

-(void)syncUserInfo:(NSString *)userID
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/userInfo?shopid=%@&uid=%@",singletonClass.shopid,userID]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
  [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    /*收到成功代码后的操作*/
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        //数据解析
        NSDictionary * tempDic = [dic objectForKey:@"data"];
        
        NSString * userid = [NSString stringWithFormat:@"%@",[tempDic objectForKey:@"uid"]];
        NSString * username = [tempDic objectForKey:@"username"];
        NSString * password = [tempDic objectForKey:@"password"];
        NSNumber * integralCount = [NSNumber numberWithInt:[[tempDic objectForKey:@"integralCount"] intValue]];
        NSString * userphone = [tempDic objectForKey:@"userphone"];
        //哇点相关
        NSNumber * wDNumber = [NSNumber numberWithInt:[[tempDic objectForKey:@"woonumber"] intValue]];
        NSString * wooname = [tempDic objectForKey:@"wooname"];
        NSNumber * isbindingwoo;
        if ([[tempDic objectForKey:@"isbindingwoo"] intValue] == 0)
        {
            isbindingwoo = [NSNumber numberWithBool:NO];
        }
        else
        {
            isbindingwoo = [NSNumber numberWithBool:YES];
        }
        NSString * woopassword = [tempDic objectForKey:@"woopassword"];
        //个人信息
        NSString * nickname = [tempDic objectForKey:@"nickname"];
        NSNumber * sex = [NSNumber numberWithInt:[[tempDic objectForKey:@"sex"] intValue]];
        NSDate * birthday = [NSDate date];
        if (nil == [tempDic objectForKey:@"birthday"])
        {
            birthday = [WXCommonDateClass getDateFromString:[tempDic objectForKey:@"birthday"] OfFormat:@"yyyy-MM-dd"];
        }
        
        NSString * profession = [tempDic objectForKey:@"profession"];
        NSString * marriage = [tempDic objectForKey:@"marriage"];
        NSString * education = [tempDic objectForKey:@"education"];
        NSArray * hobby = [tempDic objectForKey:@"hobby"];
        NSString * signature = [tempDic objectForKey:@"signature"];
        NSString * city = [tempDic objectForKey:@"city"];
        NSString *QQid = [NSString stringWithFormat:@"%@", [tempDic objectForKey:@"bindQQId"]];
        NSMutableDictionary * databaseDic = [NSMutableDictionary dictionary];
        [databaseDic setObject:QQid forKey:@"bindQQID"];
        [databaseDic setObject:userid forKey:@"userID"];
        [databaseDic setObject:username forKey:@"userName"];
        [databaseDic setObject:password forKey:@"password"];
        [databaseDic setObject:integralCount forKey:@"integralCount"];
        [databaseDic setObject:userphone forKey:@"userPhone"];
        [databaseDic setObject:wDNumber forKey:@"wDNumber"];
        [databaseDic setObject:wooname forKey:@"wooName"];
        [databaseDic setObject:isbindingwoo forKey:@"isBindingWoo"];
        [databaseDic setObject:woopassword forKey:@"wooPwd"];
        [databaseDic setObject:nickname forKey:@"nickname"];
        [databaseDic setObject:sex forKey:@"sex"];
        [databaseDic setObject:birthday forKey:@"birthday"];
        [databaseDic setObject:profession forKey:@"profession"];
        [databaseDic setObject:marriage forKey:@"marriage"];
        [databaseDic setObject:education forKey:@"education"];
        [databaseDic setObject:hobby forKey:@"hobby"];
        [databaseDic setObject:signature forKey:@"signature"];
        [databaseDic setObject:city forKey:@"city"];

        [delegate syncGetUserInfoByThirdPartyLoggingSuccess:databaseDic];
    }else
    {
        [delegate syncGetUserInfoByThirdPartyLoggingFailed:[dic objectForKey:@"message"]];
    }
}

@end



