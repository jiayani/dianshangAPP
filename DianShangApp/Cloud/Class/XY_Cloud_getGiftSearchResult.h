//
//  XY_Cloud_getGiftSearchResult.h
//  DianShangApp
//
//  Created by wa on 14-6-12.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"

@interface XY_Cloud_getGiftSearchResult : NSObject
{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;
}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)productListSync:(NSString *)keyString :(NSString *)score :(int)page :(int)flag;
@end
