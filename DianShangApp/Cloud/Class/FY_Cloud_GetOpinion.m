//
//  FY_Cloud_GetOpinion.m
//  DianShangApp
//
//  Created by Fuy on 14-6-6.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_GetOpinion.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_GetOpinion * getOpinion = nil;
@implementation FY_Cloud_GetOpinion
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!getOpinion)
    {
        getOpinion = [[FY_Cloud_GetOpinion alloc] init];
    }
    return getOpinion;
}

-(void)getOpinion:(int)temp
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_feedbackList?shopid=%@&uid=%@&page=%d", singletonClass.shopid,singletonClass.currentUserId, temp]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSDictionary * allGiftInfo = [dic objectForKey:@"data"] ;
        [delegate syncGetOpinionSuccess:allGiftInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncGetOpinionFailed:@"请求失败"];
        }
        else
        {
            [delegate syncGetOpinionFailed:[dic objectForKey:@"message"]];
        }
    }
}

@end
