//
//  FY_Cloud_geyInfomation.m
//  DianShangApp
//
//  Created by Fuy on 14-6-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_geyInfomation.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_geyInfomation * getInfomation = nil;
@implementation FY_Cloud_geyInfomation
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!getInfomation)
    {
        getInfomation = [[FY_Cloud_geyInfomation alloc] init];
    }
    return getInfomation;
}

-(void)getInfomation:(int)temp andProductID:(int)productID;
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr;
        if (productID == 0) {
            urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_getProductConsult?shopid=%@&uid=%@&page=%d", singletonClass.shopid,singletonClass.currentUserId, temp]];
        }else
        {
             urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_getProductConsult?shopid=%@&uid=%@&page=%d&productID=%d", singletonClass.shopid,singletonClass.currentUserId, temp, productID]];
        }
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSArray * allGiftInfo = [dic objectForKey:@"data"];
        [delegate syncGetInfomationSuccess:allGiftInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncGetInfomationFailed:@"请求失败"];
        }
        else
        {
            [delegate syncGetInfomationFailed:[dic objectForKey:@"message"]];
        }
    }
}

@end
