//
//  WX_Cloud_UserCountInfo.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_UserCountInfo.h"
#import "WXCommonDateClass.h"
#import "SBJson.h"
#import "WX_Cloud_IsConnection.h"
#import "WXCommonSingletonClass.h"

static WX_Cloud_UserCountInfo *userCountInfo = nil;

@implementation WX_Cloud_UserCountInfo
@synthesize delegate;
@synthesize recivedData;

+ (id)share
{
    if (!userCountInfo)
    {
        userCountInfo = [[WX_Cloud_UserCountInfo alloc] init];
    }
    return userCountInfo;
}

- (void)syncGetUserCountInfo:(NSString *)userId
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_getUserCountInfo?shopid=%@&uid=%@",singletonClass.shopid,userId]];
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }else{
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        [delegate syncGetUserCountInfoSuccess:[dic objectForKey:@"data"]];
    }
    else
    {
        NSString * str = [dic objectForKey:@"message"];
        [delegate syncGetUserCountInfoFailed:str];
    }
}

@end

