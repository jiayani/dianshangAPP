//
//  FY_Cloud_userBindQQ.h
//  TianLvApp
//
//  Created by Fuy on 14-9-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"
#import "AppDelegate.h"
@interface FY_Cloud_userBindQQ : NSObject
{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;
    
}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)BindQQWithOpenID:(NSString *)openID andNickName:(NSString *)nickname;
@end
