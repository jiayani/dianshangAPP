//
//  JYN_Cloud_getRegPhoneCode.h
//  DianShangApp
//
//  Created by wa on 14-10-14.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"
#import "AppDelegate.h"
@interface JYN_Cloud_getRegPhoneCode : NSObject

{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;
    
}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)getPhoneCodeWithPhone:(NSString *)phone andTime:(NSString *)time andToken:(NSString *)token;
@end

