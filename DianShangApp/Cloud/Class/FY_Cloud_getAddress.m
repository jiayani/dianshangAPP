//
//  FY_Cloud_getAddress.m
//  DianShangApp
//
//  Created by Fuy on 14-6-19.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_getAddress.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_getAddress * getAddress = nil;
@implementation FY_Cloud_getAddress
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!getAddress)
    {
        getAddress = [[FY_Cloud_getAddress alloc] init];
    }
    return getAddress;
}

-(void)getAddressWithIsOnlyDefault:(int)isOnlyDefault
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_getContactAddress?shopid=%@&uid=%@&isOnlyDefault=%d", singletonClass.shopid, singletonClass.currentUserId, isOnlyDefault]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSArray * allInfo = [[dic objectForKey:@"data"] objectForKey:@"contactAddress"];
        [delegate syncGetAddressSuccess:allInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncGetAddressFailed:@"请求失败"];
        }
        else
        {
            [delegate syncGetAddressFailed:[dic objectForKey:@"message"]];
        }
    }
}

@end
