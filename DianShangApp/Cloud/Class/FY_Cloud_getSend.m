//
//  FY_Cloud_getSend.m
//  DianShangApp
//
//  Created by Fuy on 14-6-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_getSend.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_getSend * getSend = nil;
@implementation FY_Cloud_getSend
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!getSend)
    {
        getSend = [[FY_Cloud_getSend alloc] init];
    }
    return getSend;
}

-(void)getSendWithGoods:(NSString *)goods andGroupBuys:(NSString *)groupBuys andContactld:(int)contactId
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr;
        if ([groupBuys isEqualToString:@""]) {
            urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_getConsumeType?shopid=%@&uid=%@&goods=%@&contactId=%d", singletonClass.shopid, singletonClass.currentUserId, goods, contactId]];
        }
        else
        {
            urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_getConsumeType?shopid=%@&uid=%@&&groupBuys=%@&contactId=%d", singletonClass.shopid, singletonClass.currentUserId, groupBuys, contactId]];
        }
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSArray * allInfo = [dic objectForKey:@"data"] ;
        [delegate syncGetSendSuccess:allInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncGetSendFailed:@"请求失败"];
        }
        else
        {
            [delegate syncGetSendFailed:[dic objectForKey:@"message"]];
        }
    }
}

@end
