//
//  WX_Cloud_getGiftCart.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-12.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_getGiftCart.h"
#import "WXCommonDateClass.h"
#import "SBJson.h"
#import "WX_Cloud_IsConnection.h"
#import "WXCommonSingletonClass.h"

static WX_Cloud_getGiftCart * giftCart = nil;

@implementation WX_Cloud_getGiftCart


@synthesize delegate;
@synthesize recivedData;

+ (id)share
{
    if (!giftCart)
    {
        giftCart = [[WX_Cloud_getGiftCart alloc] init];
    }
    return giftCart;
}

- (void)syncGiftCart:(NSString *)jsonStr
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_giftsSync?gifts=%@&shopid=%@",jsonStr,singletonClass.shopid]];
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }else{
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        [delegate syncGetGiftCartScuccess:[dic objectForKey:@"data"]];
    }
    else
    {
        NSString * str = [dic objectForKey:@"message"];
        [delegate syncGetGiftCartFailed:str];
    }
}

@end



