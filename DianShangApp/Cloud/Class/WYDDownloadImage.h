//
//  WYDDownloadImage.h
//  TianLvApp
//
//  Created by 王寅冬 on 13-7-18.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WYDDownloadImage : NSObject
{
    
}

+(id)share;
- (void)downloadImage:(NSString*)imageURL;


@end
