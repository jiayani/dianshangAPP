//
//  WX_Cloud_BindWoo.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"

@interface WX_Cloud_BindWoo : NSObject{
    id <WX_Cloud_Delegate> delegate;
}


@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData *recivedData;

+ (id)share;
- (void)bindingWoo:(NSString*)name Password:(NSString*)password Flag:(int)flag;

@end
