//
//  WX_Cloud_UserRegister.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"

@interface WX_Cloud_UserRegister : NSObject

@property (weak,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData *recivedData;

+ (id)share;
-(void)userRegisterWithUserName:(NSString*)username UserPassword:(NSString*)password andType:(int)type andCode:(NSString *)code;
@end
