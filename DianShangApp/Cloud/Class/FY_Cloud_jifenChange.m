//
//  FY_Cloud_jifenChange.m
//  DianShangApp
//
//  Created by Fuy on 14-6-21.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_jifenChange.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_jifenChange * setChange = nil;
@implementation FY_Cloud_jifenChange
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!setChange)
    {
        setChange = [[FY_Cloud_jifenChange alloc] init];
    }
    return setChange;
}

-(void)setJifenChange:(int)swapval andType:(int)type
{
    
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr;
        urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/creditsSwap?&uid=%@&swapval=%i&type=%i&shopid=%@",singletonClass.currentUserId,swapval,type,singletonClass.shopid]];
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];

    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        [delegate syncSetJifenChangeSuccess:[dic objectForKey:@"data"]];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncSetJifenChangeFailed:@"请求失败"];
        }
        else
        {
            [delegate syncSetJifenChangeFailed:[dic objectForKey:@"发送失败"]];
        }
    }
}

@end
