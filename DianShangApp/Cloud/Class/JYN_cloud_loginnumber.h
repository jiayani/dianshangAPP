//
//  JYN_cloud_loginnumber.h
//  TianLvApp
//
//  Created by wa on 14-9-29.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"
#import "AppDelegate.h"
@interface JYN_cloud_loginnumber : NSObject
{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;
    
}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;

-(void)sendNumberToBackground;
@end
