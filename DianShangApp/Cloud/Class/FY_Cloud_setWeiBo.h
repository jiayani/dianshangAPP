//
//  FY_Cloud_setWeiBo.h
//  DianShangApp
//
//  Created by Fuy on 14-7-8.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"
@interface FY_Cloud_setWeiBo : NSObject
{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;

}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)setWeiBoWithType:(NSString *)type andQQOpenID:(NSString *)openID andNonce:(NSString *)nonce andSingature:(NSString *)singature andToken:(NSString *)token andTimestamp:(NSString *)timestamp andVeritier:(NSString *)veritier;

@end
