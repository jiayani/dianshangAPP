//
//  WX_Cloud_UpdatePwd.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-22.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"

@interface WX_Cloud_UpdatePwd : NSObject{
    id <WX_Cloud_Delegate> delegate;
}

@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData *recivedData;

+(id)share;
-(void)updatePwdWithOldPwd:(NSString*)oldpassword newPassword:(NSString*)newPassword ReNewPassword:(NSString*)rePassword;

@end
