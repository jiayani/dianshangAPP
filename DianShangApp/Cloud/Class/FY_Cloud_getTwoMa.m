//
//  FY_Cloud_getTwoMa.m
//  DianShangApp
//
//  Created by Fuy on 14-6-27.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_getTwoMa.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_getTwoMa *getMa = nil;
@implementation FY_Cloud_getTwoMa
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!getMa)
    {
        getMa = [[FY_Cloud_getTwoMa alloc] init];
    }
    return getMa;
}

-(void)getTwoMa
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr;
        urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/qrc?shopid=%@",singletonClass.shopid]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSDictionary * allInfo = [dic objectForKey:@"data"];
        [delegate shareTwoMaSuccess:allInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate shareTwoMaFailed:@"请求失败"];
        }
        else
        {
            [delegate shareTwoMaFailed:[dic objectForKey:@"message"]];
        }
    }
}

@end
