//
//  FY_Cloud_phoneForQQFirst.m
//  TianLvApp
//
//  Created by Fuy on 14-9-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_phoneForQQFirst.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_phoneForQQFirst *chechQQFirst = nil;
@implementation FY_Cloud_phoneForQQFirst
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!chechQQFirst)
    {
        chechQQFirst = [[FY_Cloud_phoneForQQFirst alloc] init];
    }
    return chechQQFirst;
}

-(void)checkFirstWithPhone:(NSString *)phone andTime:(NSString *)time andToken:(NSString *)token andOpenID:(NSString *)openID andType:(int)type
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr;
        urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/checkUserPhoneValidForBindUser?shopid=%@&checkPhone=%@&time=%@&token=%@&authorize_id=%@&authorize_type=%d", singletonClass.shopid, phone, time, token, openID, type]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
 }

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSString *allInfo = [dic objectForKey:@"message"];
        [delegate checkFirstSuccess:allInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate checkFirstFailed:@"请求失败"];
        }
        else
        {
            [delegate checkFirstFailed:[dic objectForKey:@"message"]];
        }
    }
}
@end
