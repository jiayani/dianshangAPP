//
//  WX_Cloud_ResetPwd.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-21.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_ResetPwd.h"
#import "SBJson.h"
#import "MyMD5.h"
#import "WX_Cloud_IsConnection.h"

static WX_Cloud_ResetPwd *resetPwd = nil;

@implementation WX_Cloud_ResetPwd

@synthesize delegate;
@synthesize recivedData;

+ (id)share
{
    if (!resetPwd)
    {
        resetPwd = [[WX_Cloud_ResetPwd alloc] init];
    }
    return resetPwd;
}

- (void)changePassword:(NSString*)newPassword RePassword:(NSString*)rePassword
{
   
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/findMyPasswordThree"]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSString * body = [NSString stringWithFormat:@"newPassword=%@&uid=%@&shopid=%@",newPassword,rePassword,singletonClass.shopid];
        NSData * bodyData = [body dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:bodyData];
        [request setTimeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{

    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [[NSDictionary alloc] init];
    dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        [delegate updatePwdSuccess];
    }
    else
    {
        [delegate updatePwdFailed:[dic objectForKey:@"message"]];
        
    }
}

@end

