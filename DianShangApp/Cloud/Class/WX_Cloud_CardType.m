//
//  WX_Cloud_CardType.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-18.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_CardType.h"
#import "WXCommonDateClass.h"
#import "SBJson.h"
#import "WX_Cloud_IsConnection.h"
#import "WXCommonSingletonClass.h"

static WX_Cloud_CardType *cardType = nil;

@implementation WX_Cloud_CardType
@synthesize delegate;
@synthesize recivedData;

+ (id)share
{
    if (!cardType)
    {
        cardType = [[WX_Cloud_CardType alloc] init];
    }
    return cardType;
}

- (void)syncGetCardType
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/UserCard?shopid=%@",singletonClass.shopid]];
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }else{
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        [delegate syncGetCardTypeSuccess:[dic objectForKey:@"data"]];
    }
    else
    {
        NSString * str = [dic objectForKey:@"message"];
        [delegate syncGetCardTypeFailed:str];
    }
}

@end





