//
//  FY_Cloud_getWooJiFen.m
//  DianShangApp
//
//  Created by Fuy on 14-6-24.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_getWooJiFen.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_getWooJiFen * getwoo = nil;
@implementation FY_Cloud_getWooJiFen
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!getwoo)
    {
        getwoo = [[FY_Cloud_getWooJiFen alloc] init];
    }
    return getwoo;
}

-(void)getWooJiFen
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        
       NSString *urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/creditsWooInfo?shopid=%@&uid=%@", singletonClass.shopid, singletonClass.currentUserId]];
       // urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSDictionary * allInfo = [dic objectForKey:@"data"] ;
        [delegate syncGetWooSuccess:allInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncGetWooFailed:@"请求失败"];
        }
        else
        {
            [delegate syncGetWooFailed:[dic objectForKey:@"message"]];
        }
    }
}

@end
