//
//  FY_Cloud_getGiftOrder.m
//  DianShangApp
//
//  Created by Fuy on 14-6-17.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_getGiftOrder.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_getGiftOrder * getGiftOrder = nil;
@implementation FY_Cloud_getGiftOrder
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!getGiftOrder)
    {
        getGiftOrder = [[FY_Cloud_getGiftOrder alloc] init];
    }
    return getGiftOrder;
}

-(void)getGiftOrderListWithPage:(int)page andState:(int)state andWooGiftsCount:(int)count
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/shopGiftExchangeRecords?shopid=%@&uid=%@&page=%d&state=%d&wooGiftsCount=%d", singletonClass.shopid, singletonClass.currentUserId, page, state, count]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSArray * allInfo = [dic objectForKey:@"data"];
        [delegate syncGetGiftOrderListSuccess:allInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncGetGiftOrderListFailed:@"请求失败"];
        }
        else
        {
            [delegate syncGetGiftOrderListFailed:[dic objectForKey:@"message"]];
        }
    }
}
@end
