//
//  FY_Cloud_checkThridLogin.h
//  TianLvApp
//
//  Created by Fuy on 14-9-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"
#import "AppDelegate.h"
@interface FY_Cloud_checkThridLogin : NSObject
{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;
    
}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)checkThirdLoginWithOpenID:(NSString *)openID andLoginType:(int)type andCreateUser:(int)create andNickname:(NSString *)nickname andPassword:(NSString *)password andSex:(NSString *)sex andCity:(NSString *)city;
@end
