//
//  FY_Cloud_getRegPhoneCode.m
//  TianLvApp
//
//  Created by Fuy on 14-9-17.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_getRegPhoneCode.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_getRegPhoneCode *getRegCode = nil;
@implementation FY_Cloud_getRegPhoneCode
@synthesize delegate;
@synthesize recivedData;
+(id)share
{
    if (!getRegCode)
    {
        getRegCode = [[FY_Cloud_getRegPhoneCode alloc] init];
    }
    return getRegCode;
}

-(void)getPhoneCodeWithPhone:(NSString *)phone andTime:(NSString *)time andToken:(NSString *)token
{
    
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr;
        urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/userRegGetValidCode?shopid=%@&phone=%@&time=%@&token=%@",singletonClass.shopid, phone, time, token]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
      [delegate accidentError:NoHaveNetwork errorCode:@"1"];    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
   [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSDictionary * allInfo = [dic objectForKey:@"data"];
        [delegate getRegPhoneCodeSuccess:allInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate getRegPhoneCodeFailed:@"请求失败"];
        }
        else
        {
            [delegate getRegPhoneCodeFailed:[dic objectForKey:@"message"]];
        }
    }
}

@end
