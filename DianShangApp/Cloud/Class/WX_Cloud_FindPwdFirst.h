//
//  WX_Cloud_FindPwdFirst.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"

@interface WX_Cloud_FindPwdFirst : NSObject{
    id <WX_Cloud_Delegate> delegate;
}


@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData *recivedData;

+ (id)share;
- (void)findPasswordFirst:(int)type phoneOrEmailStr:(NSString*)phoneOrEmailStr;
@end
