//
//  FY_Cloud_GiftListSync.m
//  DianShangApp
//
//  Created by Fuy on 14-6-3.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_GiftListSync.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_GiftListSync * giftList = nil;
@implementation FY_Cloud_GiftListSync
@synthesize delegate;
@synthesize recivedData;
+(id)share
{
    if (!giftList)
    {
        giftList = [[FY_Cloud_GiftListSync alloc] init];
    }
    return giftList;
}

-(void)giftSync:(NSString *)firstMenuID second:(NSString *)secondMenuID page:(int)page giftType:(int)type
{
    _giftType = type;
    if (page == 1)
    {
        isFirstPage = YES;
    }
    else
    {
        isFirstPage = NO;
    }
    
    secondMenuID_ = secondMenuID;
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/giftsList_GetImp?shopid=%@&firstMenuID=%@&secondMenuID=%@&page=%d&giftType=%d",singletonClass.shopid,firstMenuID,secondMenuID,page,type]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }

}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSDictionary * allGiftInfo = [dic objectForKey:@"data"];//礼品列表
        [delegate syncGifeListSuccess:allGiftInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncGifeListFailed:@"请求失败"];
        }
        else
        {
            [delegate syncGifeListFailed:[dic objectForKey:@"message"]];
        }
    }
}
@end
