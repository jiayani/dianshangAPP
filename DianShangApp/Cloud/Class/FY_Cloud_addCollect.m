//
//  FY_Cloud_addCollect.m
//  DianShangApp
//
//  Created by Fuy on 14-6-12.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_addCollect.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_addCollect * addCollect = nil;
@implementation FY_Cloud_addCollect
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!addCollect)
    {
        addCollect = [[FY_Cloud_addCollect alloc] init];
    }
    return addCollect;
}

- (void)addCollectWithProductID:(int)productID
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_addCollection?shopid=%@&uid=%@&productId=%d", singletonClass.shopid,singletonClass.currentUserId, productID]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSString * allInfo = [dic objectForKey:@"message"];
        [delegate syncAddCollectSuccess:allInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncAddCollectFailed:@"请求失败"];
        }
        else
        {
            [delegate syncAddCollectFailed:[dic objectForKey:@"message"]];
        }
    }
}
@end
