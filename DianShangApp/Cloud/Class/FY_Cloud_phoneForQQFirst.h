//
//  FY_Cloud_phoneForQQFirst.h
//  TianLvApp
//
//  Created by Fuy on 14-9-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"
#import "AppDelegate.h"
@interface FY_Cloud_phoneForQQFirst : NSObject
{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;
    
}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)checkFirstWithPhone:(NSString *)phone andTime:(NSString *)time andToken:(NSString *)token andOpenID:(NSString *)openID andType:(int)type;
@end
