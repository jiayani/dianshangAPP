//
//  WX_Cloud_StartPage.h
//  DianShang
//
//  Created by 霞 王 on 14-4-24.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"
@class WXCommonSingletonClass;

@interface WX_Cloud_StartPage : NSObject
{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;
    WXCommonSingletonClass *singletonClass;
    long  closeTime;      //记录最近的图片更新时间
}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+ (id)share;
- (void)loadingStartPageAndImage;

@end
