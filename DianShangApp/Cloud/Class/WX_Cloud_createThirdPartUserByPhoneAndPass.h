//
//  WX_Cloud_createThirdPartUserByPhoneAndPass.h
//  TianLvApp
//
//  Created by 霞 王 on 14-9-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"

@interface WX_Cloud_createThirdPartUserByPhoneAndPass :NSObject{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;
}

@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)createThirdPartUserByPhoneAndPass:(NSMutableDictionary *)dic;


@end
