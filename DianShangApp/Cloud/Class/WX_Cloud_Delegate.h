//
//  WX_Cloud_Delegate.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-8.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol WX_Cloud_Delegate <NSObject>

@required
/**wangxia
 *1.没有网络
 *2.有网的情况下出现的异常现象
 */
- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode;

@optional
//用户注册
- (void)userRegisterSuccess:(NSMutableDictionary*)databaseDictionary;
- (void)userRegisterFailed:(NSString*)errCode;

//用户登陆
- (void)userLoginSuccess:(NSMutableDictionary*)databaseDictionary;
- (void)userLoginFailed:(NSString*)errCode;

//验证手机号、邮箱
- (void)verifyPhoneOrEmailSuccess:(NSString*)keyWordStr;
- (void)verifyPhoneOrEmailFailed:(NSString*)keyWordStr errCode:(NSString*)errCode;

//验证码验证
- (void)verifyCodeSuccess:(NSString*)keywordsID;
- (void)verifyCodeFaild:(NSString*)keywordsID andErrorMessage:(NSString*)errCode;

//重新设置密码
- (void)updatePwdSuccess;
- (void)updatePwdFailed:(NSString*)errCode;

//绑定哇点账号
- (void)bindWooSuccess:(NSDictionary*)dataDic;
- (void)bindWooFailed:(NSString*)errCode Flag:(int)flag;

//修改用户信息
- (void)updateUserInfoSuccess:(NSDictionary*)dataDic;
- (void)updateUserInfoFailed:(NSString*)errCode;

//签到送积分
- (void)signAndSendCreditsSuccess:(NSString*)message;
- (void)signAndSendCreditsFailed:(NSString*)errCode;

//礼品分类
-(void)syncGiftMenuSuccess:(id)database;
-(void)syncGiftMenuFailed:(NSString*)errCode;

//礼品列表
-(void)syncGifeListSuccess:(id)database;
-(void)syncGifeListFailed:(NSString*)errCode;

//积分明细
-(void)syncjifenSuccess:(id)database;
-(void)syncjifenFailed:(NSString*)errCode;

//判断商品是否上下架或更新
-(void)checkGoodsExist:(NSArray *)dataArray;
-(void)checkGoodsExistFailed:(NSString*)errCode;

//同步礼品购物车
-(void)syncGetGiftCartScuccess:(NSArray *)dataArray;
-(void)syncGetGiftCartFailed:(NSString*)errCode;

//更新商品的属性
-(void)updateAttributeSuccess:(NSDictionary *)dataDic;
-(void)updateAttributeFailed:(NSString*)errCode;

//获得团购列表信息
-(void)syncGetGroupBuyingSuccess:(NSDictionary *)dataDic;
-(void)syncGetGroupBuyingFailed:(NSString*)errCode;

//获得团购列详情
-(void)syncGetGroupBuyDetailSuccess:(NSDictionary *)dataDic;
-(void)syncGetGroupBuyDetailFailed:(NSString*)errCode;

//获得优惠活动详情
-(void)syncGetFavorableDetailSuccess:(NSDictionary *)dataDic;
-(void)syncGetFavorableDetailFailed:(NSString*)errCode;

//获得优惠活动列表信息
-(void)syncGetFavorableActivitySuccess:(NSDictionary *)dataDic;
-(void)syncGetFavorableActivityFailed:(NSString*)errCode;

//获得会员卡详情
-(void)syncGetMyCardSuccess:(NSDictionary *)dataDic;
-(void)syncGetMyCardFailed:(NSString *)errCode;

//获得会员卡类别
-(void)syncGetCardTypeSuccess:(NSArray *)dataArray;
-(void)syncGetCardTypeFailed:(NSString *)errCode;

//获得会员卡升级
-(void)syncGetCardUpSuccess:(NSDictionary *)dataDic;
-(void)syncGetCardUpFailed:(NSString *)errCode;

//获得个人中心用户相关的数据信息
-(void)syncGetUserCountInfoSuccess:(NSDictionary *)dataDic;
-(void)syncGetUserCountInfoFailed:(NSString *)errCode;

//获得推送
-(void)syncGetPushNotificSuccess:(NSArray *)dataArray;
-(void)syncGetPushNotificpeFailed:(NSString *)errCode;

//获得意见反馈信息
-(void)syncGetOpinionSuccess:(NSDictionary *)dataArray;
-(void)syncGetOpinionFailed:(NSString*)errCode;

//发送意见反馈信息
-(void)syncSetOpinionSuccess:(NSString *)message;
-(void)syncSetOpinionFailed:(NSString*)errCode;

//取得广告信息
-(void)syncSuccess:(NSDictionary*)adDic;
-(void)syncFailed:(NSString*)errCode;

//获取联系人地址列表
-(void)getContactAndAdressSuccess:(NSDictionary *)data;
-(void)getContactAndAdressFailed:(NSString*)errMsg;

//同步联系人地址列表
-(void)syncContactAndAdressSuccess:(NSDictionary *)data;
-(void)syncContactAndAdressFailed:(NSString*)errMsg;

//增加联系人地址列表
-(void)addContactAndAdressSuccess:(NSDictionary *)data;
-(void)addContactAndAdressFailed:(NSString*)errMsg;

//获取礼品详情
-(void)giftInforSyncSuccess:(NSDictionary *)data;
-(void)giftInforSyncFailed:(NSString*)errMsg;

//获取主页最新数量
-(void)getBestGoodsNoticeNumSuccess:(NSDictionary *)data;
-(void)getBestGoodsNoticeNumFailed:(NSString*)errMsg;

//获取主页最新热销商品
-(void)getBestGoodsIndexSimpleSuccess:(NSDictionary *)data;
-(void)getBestGoodsIndexSimpleFailed:(NSString*)errMsg;

//获得商品咨询信息
-(void)syncGetInfomationSuccess:(NSArray *)dataArray;
-(void)syncGetInfomationFailed:(NSString*)errCode;

//发送商品咨询信息
- (void)syncSetInfomationSuccess:(NSString *)message;
- (void)syncSetInfomationFailed:(NSString*)errCode;

//获得商品详情信息
-(void)goodInforSyncSuccess:(NSDictionary *)data;
-(void)goodInforSyncFailed:(NSString*)errMsg;

//获得商品详情信息
-(void)productListSyncSuccess:(NSDictionary *)data;
-(void)productListSyncFailed:(NSString*)errMsg;

//获得商品详情信息
-(void)syncGetMyReviewSuccess:(NSArray *)data;
-(void)syncGetMyReviewFailed:(NSString*)errMsg;

//得到礼品搜索信息
-(void)getGiftSearchResultSuccess:(NSDictionary *)data;
-(void)getGiftSearchResultFailed:(NSString*)errMsg;

//得到积分详细信息
-(void)synGetJifenSuccess:(NSDictionary *)data;
-(void)synGetJifenFailed:(NSString*)errMsg;

//添加收藏
- (void)syncAddCollectSuccess:(NSString *)message;
- (void)syncAddCollectFailed:(NSString*)errCode;

//删除收藏
- (void)syncDeleteCollectSuccess:(NSString *)message;
- (void)syncDeleteCollectFailed:(NSString*)errCode;

//得到收藏详细信息
-(void)syncGetCollectSuccess:(NSDictionary *)data;
-(void)syncGetCollectFailed:(NSString*)errMsg;

//获得商品订单列表信息
-(void)syncGetOrderListSuccess:(NSArray *)data;
-(void)syncGetOrderListFailed:(NSString*)errMsg;

//获取支付方式需要的参数
-(void)ready:(NSString*)RSAString;
-(void)notReady:(NSString*)errCode;

//确认收货
-(void)syncConfirmSuccess:(NSString*)RSAString;
-(void)syncConfirmFailed:(NSString*)errCode;

//获得商品订单评论列表信息
-(void)syncGetOrderInfoSuccess:(NSArray *)data;
-(void)syncGetOrderInfoFailed:(NSString*)errMsg;

//发表评论
-(void)syncSetEvalutionSuccess:(NSString*)EvaString;
-(void)syncSetEvalutionFailed:(NSString*)errCode;

//获得商品订单评论列表信息
-(void)syncGetGiftOrderListSuccess:(NSArray *)data;
-(void)syncGetGiftOrderListFailed:(NSString*)errMsg;

//获得我的商品订单详情
-(void)syncGetMyProductSuccess:(NSDictionary *)data;
-(void)syncGetMyProductFailed:(NSString*)errMsg;

//获得商品评论信息
-(void)syncGetReviewSuccess:(NSDictionary *)data;
-(void)syncGetReviewFailed:(NSString*)errMsg;

//获得联系人地址
-(void)syncGetAddressSuccess:(NSArray *)data;
-(void)syncGetAddressFailed:(NSString*)errMsg;

//获得配送方式
-(void)syncGetSendSuccess:(NSArray *)data;
-(void)syncGetSendFailed:(NSString*)errMsg;

//获取积分互换
-(void)syncSetJifenChangeSuccess:(NSDictionary *)data;
-(void)syncSetJifenChangeFailed:(NSString*)errMsg;

//版本升级
-(void)versionUpdateSuccess:(NSArray *)data;
-(void)versionUpdateFailed:(NSString*)errMsg;

//获得哇点积分
-(void)syncGetWooSuccess:(NSDictionary *)data;
-(void)syncGetWooFailed:(NSString*)errMsg;

//获得订单号
-(void)syncGetPayOrderSuccess:(NSDictionary *)data;
-(void)syncGetPayOrderFailed:(NSString*)errMsg;

//获得礼品订单号
-(void)syncGetGiftPayOrderSuccess:(NSDictionary *)data;
-(void)syncGetGiftPayOrderFailed:(NSString*)errMsg;

//获得消息提示
-(void)syncGetNumberSuccess:(NSDictionary *)data;
-(void)syncGetNumberFailed:(NSString*)errMsg;

//获得关于我们
-(void)syncGetMoreAdsSuccess:(NSDictionary *)data;
-(void)syncGetMoreAdsFailed:(NSString*)errMsg;

//获得二维码
-(void)shareTwoMaSuccess:(NSDictionary *)data;
-(void)shareTwoMaFailed:(NSString*)errMsg;

//删除商品咨询
-(void)syncDeleteInfoSuccess:(NSString*)EvaString;
-(void)syncDeleteInfoFailed:(NSString*)errCode;

//取消订单
-(void)syncCancelOrderSuccess:(NSString*)EvaString;
-(void)syncCancelOrderFailed:(NSString*)errCode;

//申请退货
-(void)syncOrderReturnSuccess:(NSString*)EvaString;
-(void)syncOrderReturnFailed:(NSString*)errCode;

//首次分享增积分
-(void)syncSendScoreSuccess:(NSDictionary *)data;
-(void)syncSendScoreFailed:(NSString*)errMsg;

//获取用户绑定信息
-(void)getWeiBoSuccess:(NSDictionary *)data;
-(void)getWeiBoFailed:(NSString*)errMsg;

//绑定微博信息
-(void)setWeiBoSuccess:(NSString *)data;
-(void)setWeiBoFailed:(NSString*)KerrMsg;

//删除商品评论
-(void)syncDeleteReviewSuccess:(NSString*)EvaString;
-(void)syncDeleteReviewFailed:(NSString*)errCode;

//取消微博绑定
-(void)syncCancelWeiBoSuccess:(NSString*)EvaString;
-(void)syncCancelWeiBoFailed:(NSString*)errCode;

//取消退货
-(void)syncCancelReturnSuccess:(NSString*)EvaString;
-(void)syncCancelReturnFailed:(NSString*)errCode;

//取消礼品订单
-(void)syncGiftCancelOrderSuccess:(NSString*)EvaString;
-(void)syncGiftCancelOrderFailed:(NSString*)errCode;

//礼品退货
-(void)syncGiftOrderReturnSuccess:(NSString*)EvaString;
-(void)syncGiftOrderReturnFailed:(NSString*)errCode;

//获得会员卡详情
-(void)syncFYGetMyCardSuccess:(NSDictionary *)dataDic;
-(void)syncFYGetMyCardFailed:(NSString *)errCode;

//首次分享增积分
-(void)getIsChangeSuccess:(NSDictionary *)data;
-(void)getIsChangeFailed:(NSString*)errMsg;

//得到联盟商家的列表
- (void)syncGetUnionShopListSuccess:(NSArray *)dataArray;
- (void)syncGetUnionShopListFailed:(NSString *)errCode;

//获得商品属性类表列表
- (void)syncGetGoodsAttributeCatalogSuccess:(NSArray *)dataArray;
- (void)syncGetGoodsAttributeCatalogFailed:(NSString *)errCode;

//得到联盟礼品的列表
- (void)syncGetUnionGiftListSuccess:(id)data;
- (void)syncGetUnionGiftListFailed:(NSString *)errCode;
//第三方登录后同步用户信息
-(void)syncGetUserInfoByThirdPartyLoggingSuccess:(NSMutableDictionary *)data;
-(void)syncGetUserInfoByThirdPartyLoggingFailed:(NSString*)errMsg;

//第三方登录后绑定手机激活设置密码接口
-(void)createThirdPartUserByPhoneAndPassSuccess:(NSMutableDictionary *)data;
-(void)createThirdPartUserByPhoneAndPassFailed:(NSString*)errMsg;

//设置QQ绑定
-(void)syncBindQQSuccess:(NSString*)EvaString;
-(void)syncBindQQFailed:(NSString*)errCode;

//取消QQ绑定
-(void)syncCancelQQSuccess:(NSString*)EvaString;
-(void)syncCancelQQFailed:(NSString*)errCode;

//第三方登录验证回调
-(void)checkLoginSuccess:(NSDictionary *)data; //返回100
-(void)checkLoginFailed:(NSString*)errMsg;  //返回101
-(void)checkLoginFrozen:(NSString*)errMsg;  //返回103
-(void)checkLoginOtherFailed:(NSString*)errMsg;  //其他错误

//第三方账户登录激活第一步
-(void)checkFirstSuccess:(NSString *)message;
-(void)checkFirstFailed:(NSString*)errMsg;

//第三方账户登录激活第二步
-(void)checkPhoneAndValidCodeForRegistSuccess:(NSMutableDictionary *)data;
-(void)checkPhoneAndValidCodeForRegistFailed:(NSString*)errMsg;
//绑定其他账号
-(void)bindOtherSuccess:(NSDictionary*)data;
-(void)bindOtherFailed:(NSString*)errCode;
//绑定注册界面验证码
-(void)getRegPhoneCodeSuccess:(NSDictionary*)data;
-(void)getRegPhoneCodeFailed:(NSString*)errCode;
@end
