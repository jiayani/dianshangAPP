//
//  WX_Cloud_BindWoo.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_BindWoo.h"
#import "WX_Cloud_IsConnection.h"
#import "WXCommonSingletonClass.h"
#import "MyMD5.h"
#import "SBJson.h"

static WX_Cloud_BindWoo * bindWoo = nil;

@implementation WX_Cloud_BindWoo
@synthesize delegate;
@synthesize recivedData;

+ (id)share
{
    if (!bindWoo)
    {
        bindWoo = [[WX_Cloud_BindWoo alloc] init];
    }
    return bindWoo;
}

- (void)bindingWoo:(NSString*)name Password:(NSString*)password Flag:(int)flag
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * passwordMD5 = [MyMD5 md5:password];  //对密码采用MD5加密
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"android/BindAccount"]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSString * body = [NSString stringWithFormat:@"shopid=%@&uid=%@&woo_account=%@&woo_password=%@&flag=%d",singletonClass.shopid,singletonClass.currentUserId,name,passwordMD5,flag];
        NSData * bodyData = [body dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:bodyData];
        
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];

    NSDictionary * dic = [str JSONValue];
    /*收到成功代码后的操作*/
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSDictionary * dataDic = [dic objectForKey:@"data"];
        [delegate bindWooSuccess:dataDic];
        
    }else
    {
        [delegate bindWooFailed:[dic objectForKey:@"message"] Flag:[[dic objectForKey:@"code"] intValue]];
        
    }
}

@end

