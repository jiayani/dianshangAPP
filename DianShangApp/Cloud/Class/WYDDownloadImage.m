//
//  WYDDownloadImage.m
//  TianLvApp
//
//  Created by 王寅冬 on 13-7-18.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import "WYDDownloadImage.h"

static WYDDownloadImage * downloadImage = nil;
@implementation WYDDownloadImage

+(id)share
{
    if (!downloadImage)
    {
        downloadImage = [[WYDDownloadImage alloc] init];
    }
    return downloadImage;
}

- (void)downloadImage:(NSString*)imageURL
{
    
    if (imageURL == nil)
    {
        return;
    }
    //begin:拼接图片URL
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    NSString  *serviceURL = singletonClass.getImageServiceRUL;
    serviceURL = [serviceURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (![imageURL hasPrefix:@"http://"]) {
        imageURL = [NSString stringWithFormat:@"%@%@",serviceURL,imageURL];
        
    }
    //end
    if(imageURL)
    {
        //确定图片的缓存地址
        NSArray *path=NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *docDir=[path objectAtIndex:0];
        NSFileManager *fm = [NSFileManager defaultManager];
        //begin 获得图片路径
        NSString *tmpPath;
        //去掉IP地址 如http://192.168.1.10//A/B/1.png  得到的是 A/B/1.png
        NSString *tempUrl = imageURL;
        if (serviceURL != nil && [imageURL hasPrefix:serviceURL] && imageURL.length >= serviceURL.length + 1) {
            tempUrl = [imageURL substringFromIndex:serviceURL.length + 1];
        }
        
        NSArray *lineArray = [tempUrl componentsSeparatedByString:@"/"];
        for (int i = 0; i < lineArray.count - 1; i++) {
            NSString *superPath , *currentPath;
            superPath = @"";
            currentPath = [lineArray objectAtIndex:i];
            for (int j = 0; j < i; j++) {
                if (j == 0) {
                    superPath = [lineArray objectAtIndex:j];
                }else{
                    superPath = [NSString stringWithFormat:@"%@/%@",superPath,[lineArray objectAtIndex:j]];
                }
                
            }
            if (i > 0 && superPath.length > 0) {
                superPath = [NSString stringWithFormat:@"%@/",superPath];
            }
            if (superPath != nil) {
                
                currentPath = [superPath stringByAppendingFormat:@"%@",currentPath];
                
            }
            tmpPath = [docDir stringByAppendingPathComponent:currentPath];
            //循环创建目录
            if(![fm fileExistsAtPath:tmpPath])
            {
                [fm createDirectoryAtPath:tmpPath withIntermediateDirectories:YES attributes:nil error:nil];
            }
        }
        //end
        NSString *fileName = [NSString stringWithFormat:@"%@/%@", docDir, tempUrl];
        //拼接图片的绝对路径url
        NSURL * url = [NSURL URLWithString:imageURL];
        //下载图片数据
        NSData * imageData = [NSData dataWithContentsOfURL:url];
        
        //图片已经成功下载到本地缓存，指定图片
        UIImage *newImage = nil;
        newImage = [[UIImage alloc]initWithData:imageData];
        if (newImage != nil) {
            [imageData writeToFile:fileName atomically:YES];
        }
    }

}


@end
