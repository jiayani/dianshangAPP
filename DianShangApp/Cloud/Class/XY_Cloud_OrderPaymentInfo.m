//
//  XY_Cloud_OrderPaymentInfo.m
//  DianShangApp
//
//  Created by wa on 14-6-14.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XY_Cloud_OrderPaymentInfo.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"

static XY_Cloud_OrderPaymentInfo * orderPaymentInfo = nil;
@implementation XY_Cloud_OrderPaymentInfo
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!orderPaymentInfo)
    {
        orderPaymentInfo = [[XY_Cloud_OrderPaymentInfo alloc] init];
    }
    return orderPaymentInfo;
}
-(void)readyToPay:(NSString *)orderID
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
          WXCommonSingletonClass *  singletonClass  = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_orderPaymentInfo?uid=%@&orderID=%@&shopid=%@",singletonClass.currentUserId,orderID,singletonClass.shopid]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    
    /*收到成功代码后的操作*/
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSDictionary * dataDic = [dic objectForKey:@"data"];
        NSString * RSAString = [dataDic objectForKey:@"request_data"];
        [delegate ready:RSAString];
    }
    else
    {
        [delegate notReady:[dic objectForKey:@"message"]];
    }
}

@end
