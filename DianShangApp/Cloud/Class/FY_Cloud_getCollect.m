//
//  FY_Cloud_getCollect.m
//  DianShangApp
//
//  Created by Fuy on 14-6-12.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_getCollect.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_getCollect * getCollect = nil;
@implementation FY_Cloud_getCollect
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!getCollect)
    {
        getCollect = [[FY_Cloud_getCollect alloc] init];
    }
    return getCollect;
}

-(void)getCollect
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_getCollection?shopid=%@&uid=%@", singletonClass.shopid,singletonClass.currentUserId]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSDictionary * allInfo = [dic objectForKey:@"data"];
        [delegate syncGetCollectSuccess:allInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncGetCollectFailed:@"请求失败"];
        }
        else
        {
            [delegate syncGetCollectFailed:[dic objectForKey:@"message"]];
        }
    }
}

@end
