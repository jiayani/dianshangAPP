//
//  XY_Cloud_VersionUpdate.m
//  DianShangApp
//
//  Created by wa on 14-6-24.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XY_Cloud_VersionUpdate.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
#import "AppDelegate.h"
#import "WXConfigDataControll.h"

static XY_Cloud_VersionUpdate * versionUpdate = nil;
@implementation XY_Cloud_VersionUpdate
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!versionUpdate)
    {
        versionUpdate = [[XY_Cloud_VersionUpdate alloc] init];
    }
    return versionUpdate;
}
-(void)getCurrentVersion
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *  singletonClass  = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/newCheckUpdate?shopid=%@",singletonClass.shopid]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    
    /*收到成功代码后的操作*/
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSDictionary * resultDic = [dic objectForKey:@"data"];
        
        /*版本更新的内容*/
        NSString * version = [resultDic objectForKey:@"lastestVersion_iPhone"];
        NSString * versionEnterprise = [resultDic objectForKey:@"lastestVersion_iPhone_Enterprise"];
        NSString * updateInfo = [resultDic objectForKey:@"content_iPhone"];
        NSString * updateInfoEnterprise = [resultDic objectForKey:@"content_iPhone_Enterprise"];
        NSString * updateUrl = [resultDic objectForKey:@"downloads_iPhone"];
        NSString * updateUrlEnterprise = [resultDic objectForKey:@"downloads_iPhone_Enterprise"];
        [AppDelegate delegate].versionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:version,@"version",versionEnterprise,@"versionEnterprise",updateInfo,@"updateInfo",updateInfoEnterprise,@"updateInfoEnterprise",updateUrl,@"updateUrl",updateUrlEnterprise,@"updateUrlEnterprise", nil];
        
        NSDictionary *dic = [[NSBundle mainBundle] infoDictionary]; //获取info－plistß
        NSString *appName = [dic objectForKey:@"CFBundleIdentifier"]; //获取Bundle identifier
        NSString *sinaKey, * tencentKey,* QQLogin,* UMengSecret, *UMengChannelId, *weixinKey;
        if ([appName rangeOfString:@"kstapp"].location != NSNotFound)
        {
            sinaKey = [resultDic objectForKey:@"sina_weibo_key_appstore"];
            tencentKey = [resultDic objectForKey:@"tecent_weibo_key_appstore"];
            QQLogin = [resultDic objectForKey:@"qqlogin_appid_appstore"];
            UMengSecret = [resultDic objectForKey:@"UMengIphoneSecret_appstore"];
            weixinKey = [resultDic objectForKey:@"openWeixinAppID_appstore"];
            UMengChannelId = [resultDic objectForKey:@"UMengChannelId"];
        }else
        {
            sinaKey = [resultDic objectForKey:@"sina_weibo_key_enterprise"];
            tencentKey = [resultDic objectForKey:@"tecent_weibo_key_enterprise"];
            QQLogin = [resultDic objectForKey:@"qqlogin_appid_enterprise"];
            UMengSecret = [resultDic objectForKey:@"UMengIphoneSecret"];
            weixinKey = [resultDic objectForKey:@"openWeixinAppID"];
            UMengChannelId = [resultDic objectForKey:@"UMengChannelId"];
        }
        
        /*腾讯微博*/
        [WXConfigDataControll setQQAppKey:tencentKey];
        //设置微信AppKey
        [WXConfigDataControll setWeiXinAppKey:weixinKey];
        /*新浪微博*/
        [WXConfigDataControll setSinaAppKey:sinaKey];
        /*友盟*/
        [WXConfigDataControll setUMengiPhoneSecret:UMengSecret];
        //QQ登录 付岩
        [WXConfigDataControll setQQLoginID:QQLogin];
        //友盟
        [WXConfigDataControll setUMengiPhoneSecret:UMengSecret];
        [WXConfigDataControll setUMengChannelId:UMengChannelId];
        
        /* 三种支付方式是否支持 ，退款是否支持 */
        NSString * can_comepay = [resultDic objectForKey:@"can_comepay"];   //到店支付
        [WXConfigDataControll setCan_comepay:can_comepay];
        
        NSString * can_embed = [resultDic objectForKey:@"can_embed"];       //支付宝支付
        [WXConfigDataControll setCan_embed:can_embed];
        
        NSString * can_return = [resultDic objectForKey:@"can_return"];     //是否支持退款
        [WXConfigDataControll setCan_return:can_return];
        
        NSString * can_wappay = [resultDic objectForKey:@"can_wappay"];     //网页支付
        [WXConfigDataControll setCan_wappay:can_wappay];
        
        NSString * can_paydelivery = [resultDic objectForKey:@"can_pay_on_delivery"];      //货到付款
        [WXConfigDataControll setCan_delivery:can_paydelivery];
        NSString * can_shopDelivery = [resultDic objectForKey:@"support_shop_delivery"];     //是否支持商家配送
        [WXConfigDataControll setCan_shopDelivery:can_shopDelivery];
        
        [delegate versionUpdateSuccess:nil];
    }
    else
    {
        [delegate versionUpdateFailed:[dic objectForKey:@"message"]];
    }
}


@end
