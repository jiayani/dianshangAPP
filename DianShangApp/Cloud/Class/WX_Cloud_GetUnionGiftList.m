//
//  WX_Cloud_GetUnionGiftList.m
//  DianShangApp
//
//  Created by 霞 王 on 14-10-10.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_GetUnionGiftList.h"
#import "WXCommonDateClass.h"
#import "SBJson.h"
#import "WX_Cloud_IsConnection.h"
#import "WXCommonSingletonClass.h"


static WX_Cloud_GetUnionGiftList *getUnionGiftList = nil;

@implementation WX_Cloud_GetUnionGiftList

@synthesize delegate;
@synthesize recivedData;

+ (id)share
{
    if (!getUnionGiftList)
    {
        getUnionGiftList = [[WX_Cloud_GetUnionGiftList alloc] init];
    }
    return getUnionGiftList;
}

- (void)getUnionGiftList:(int)page
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/getUnionGiftList?shopid=%@&page=%d",singletonClass.shopid,page]];
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }else{
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        [delegate syncGetUnionGiftListSuccess:[dic objectForKey:@"data"]];
    }
    else
    {
        NSString * str = [dic objectForKey:@"message"];
        [delegate syncGetUnionGiftListFailed:str];
    }
}

@end





