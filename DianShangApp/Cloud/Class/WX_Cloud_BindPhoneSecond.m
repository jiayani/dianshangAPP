//
//  WX_Cloud_BindPhoneSecond.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-22.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_BindPhoneSecond.h"
#import "SBJson.h"
#import "WX_Cloud_IsConnection.h"


static WX_Cloud_BindPhoneSecond *bindPhoneSecond = nil;

@implementation WX_Cloud_BindPhoneSecond
@synthesize delegate;
@synthesize recivedData;

+ (id)share
{
    if (!bindPhoneSecond)
    {
        bindPhoneSecond = [[WX_Cloud_BindPhoneSecond alloc] init];
    }
    return bindPhoneSecond;
}

- (void)sendVerifyCode:(NSString*)keywords flag:(int)keywordsID
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/bindPhoneTwo?keywords=%@&shopid=%@&uid=%@",keywords,singletonClass.shopid,singletonClass.currentUserId]];
        NSURL * url = [[NSURL alloc] initWithString:urlStr];
        NSURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        //无网络
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    
    NSDictionary * dic = [str JSONValue];
    
    if ([[dic objectForKey:@"code"] intValue] == 100 )
    {
        [delegate verifyCodeSuccess:nil];
    }
    else
    {
        [delegate verifyCodeFaild:nil andErrorMessage:[dic objectForKey:@"message"]];
        
    }
}

@end
