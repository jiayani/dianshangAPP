//
//  FY_Cloud_getOrderList.m
//  DianShangApp
//
//  Created by Fuy on 14-6-13.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_getOrderList.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_getOrderList * getOrderList = nil;
@implementation FY_Cloud_getOrderList
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!getOrderList)
    {
        getOrderList = [[FY_Cloud_getOrderList alloc] init];
    }
    return getOrderList;
}

-(void)getOrderWithStatus:(int)status andPage:(int)page
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_myOrderList?shopid=%@&uid=%@&status=%d&page=%d", singletonClass.shopid,singletonClass.currentUserId, status, page]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSArray * allInfo = [dic objectForKey:@"data"];
        [delegate syncGetOrderListSuccess:allInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncGetOrderListFailed:@"请求失败"];
        }
        else
        {
            [delegate syncGetOrderListFailed:[dic objectForKey:@"message"]];
        }
    }
}

@end
