//
//  WX_Cloud_SetRedPushNotific.h
//  DianShangApp
//
//  Created by 霞 王 on 14-6-24.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WX_Cloud_SetRedPushNotific : NSObject
+ (void)setRedPushNotific:(int)keyId;
@end
