//
//  FY_Cloud_bindOtherForQQ.m
//  TianLvApp
//
//  Created by Fuy on 14-9-16.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_bindOtherForQQ.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_bindOtherForQQ *checkOther = nil;
@implementation FY_Cloud_bindOtherForQQ
@synthesize delegate;
@synthesize recivedData;
+(id)share
{
    if (!checkOther)
    {
        checkOther = [[FY_Cloud_bindOtherForQQ alloc] init];
    }
    return checkOther;
}

-(void)bindOtherWithAccount:(NSString *)account andPassword:(NSString *)password andQQToken:(NSString *)qqToken andNickname:(NSString *)nickname
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
         WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:@"Iphone/bindAccountForQQ"];
        NSURL * url = [[NSURL alloc] initWithString:urlStr];
        NSString * body = [NSString stringWithFormat:@"account=%@&password=%@&qqToken=%@&nickname=%@&shopid=%@", account, password, qqToken, nickname,singletonClass.shopid];
        NSData * bodyData = [body dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:bodyData];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
    [delegate accidentError:NoHaveNetwork errorCode:@"1"];    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSDictionary *allInfo = [dic objectForKey:@"data"];
        [delegate bindOtherSuccess:allInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate bindOtherFailed:@"请求失败"];
        }
        else
        {
            [delegate bindOtherFailed:[dic objectForKey:@"message"]];
        }
    }
}
@end
