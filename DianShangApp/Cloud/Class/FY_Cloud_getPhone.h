//
//  FY_Cloud_getPhone.h
//  DianShangApp
//
//  Created by Fuy on 14-10-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FY_Cloud_getPhone : NSObject
{
    NSMutableData * recivedData;
}
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)getPhone;

@end
