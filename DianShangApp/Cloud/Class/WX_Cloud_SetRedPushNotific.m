//
//  WX_Cloud_SetRedPushNotific.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-24.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_SetRedPushNotific.h"
#import "WXCommonDateClass.h"
#import "SBJson.h"
#import "WX_Cloud_IsConnection.h"
#import "WXCommonSingletonClass.h"

@implementation WX_Cloud_SetRedPushNotific

+ (void)setRedPushNotific:(int)keyId;
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/setSystemToRead?shopid=%@&device_id=%@&msgid=%d",singletonClass.shopid,singletonClass.deviceTokenStr,keyId]];
        
        NSURL *url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        [NSURLConnection connectionWithRequest:request delegate:self];
        
    }
    
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
}

@end

