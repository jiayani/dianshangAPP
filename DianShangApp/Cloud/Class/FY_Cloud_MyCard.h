//
//  FY_Cloud_MyCard.h
//  DianShangApp
//
//  Created by Fuy on 14-7-19.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"
@interface FY_Cloud_MyCard : NSObject
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData *recivedData;

+ (id)share;
- (void)syncGetMyCard:(NSString *)userID;

@end
