//
//  WX_Cloud_UserInfo.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_UserInfo.h"
#import "WXCommonDateClass.h"
#import "SBJson.h"
#import "WX_Cloud_IsConnection.h"
#import "WXCommonSingletonClass.h"

static WX_Cloud_UserInfo * userInfo = nil;

@implementation WX_Cloud_UserInfo

@synthesize delegate;
@synthesize recivedData;

+ (id)share
{
    if (!userInfo)
    {
        userInfo = [[WX_Cloud_UserInfo alloc] init];
    }
    return userInfo;
}
- (void)updateUserInfo:(NSMutableDictionary *)dic
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        
        NSString *nickname = [dic objectForKey:@"nickname"];
        int sex = [[dic objectForKey:@"sex"]intValue];
        
        NSDate *birthdayDate = [dic objectForKey:@"birthday"];
        NSString *birthday = [WXCommonDateClass getDateString:birthdayDate];
        
        NSString *city = [dic objectForKey:@"city"];
        NSString *profession = [dic objectForKey:@"profession"];
        NSString *marriage = [dic objectForKey:@"marriage"];
        NSString *edu = [dic objectForKey:@"education"];
        NSArray *hobby = [dic objectForKey:@"hobby"];
        NSString *signature = [dic objectForKey:@"signature"];
        
        NSString * str = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/myProfileModify?shopid=%@&uid=%@&nickname=%@&sex=%d&birthday=%@&city=%@&profession=%@&marriage=%@&education=%@&hobby=%@&signature=%@",singletonClass.shopid,singletonClass.currentUserId,nickname,sex,birthday,city,profession,marriage,edu,hobby,signature]];
        
        NSString * urlStr = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [[NSURL alloc] initWithString:urlStr];
        NSURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];

    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        [delegate updateUserInfoSuccess:nil];
    }
    else
    {
        NSString * str = [dic objectForKey:@"message"];
        [delegate updateUserInfoFailed:str];
    }
}

@end
