//
//  WX_Cloud_UpdatePwd.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-22.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_UpdatePwd.h"
#import "MyMD5.h"
#import "SBJson.h"
#import "WX_Cloud_IsConnection.h"


static WX_Cloud_UpdatePwd *updatePwd = nil;

@implementation WX_Cloud_UpdatePwd
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!updatePwd)
    {
        updatePwd = [[WX_Cloud_UpdatePwd alloc] init];
    }
    return updatePwd;
}

-(void)updatePwdWithOldPwd:(NSString*)oldpassword newPassword:(NSString*)newPassword ReNewPassword:(NSString*)rePassword{
    
   
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/passwordModify"]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSString * body = [NSString stringWithFormat:@"shopid=%@&uid=%@&oldPassword=%@&newPassword=%@&reNewPassword=%@",singletonClass.shopid,singletonClass.currentUserId,oldpassword,newPassword,rePassword];
        NSData * bodyData = [body dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:bodyData];
        [request setTimeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [[NSDictionary alloc] init];
    dic = [str JSONValue];

    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        [delegate updatePwdSuccess];
    }
    else
    {
        [delegate updatePwdFailed:[dic objectForKey:@"message"]];
    }
}

@end
