//
//  FY_Cloud_setInfomation.m
//  DianShangApp
//
//  Created by Fuy on 14-6-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_setInfomation.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_setInfomation * setInfomation = nil;
@implementation FY_Cloud_setInfomation
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!setInfomation)
    {
        setInfomation = [[FY_Cloud_setInfomation alloc] init];
    }
    return setInfomation;
}

-(void)setInfomation:(NSString *)content andProductID:(int)productID
{
    
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr;
        urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_productConsultCommit?shopid=%@&uid=%@&content=%@&productID=%d",singletonClass.shopid,singletonClass.currentUserId, content, productID]];
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSString *allGiftInfo = [dic objectForKey:@"message"];
        [delegate syncSetInfomationSuccess:allGiftInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncSetInfomationFailed:@"请求失败"];
        }
        else
        {
            [delegate syncSetInfomationFailed:[dic objectForKey:@"message"]];
        }
    }
    
}


@end
