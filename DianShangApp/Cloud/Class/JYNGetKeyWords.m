//
//  JYNGetKeyWords.m
//  DianShangApp
//
//  Created by wa on 14-10-16.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "JYNGetKeyWords.h"
#import "SBJson.h"
#import "WX_Cloud_IsConnection.h"
#import "MyMD5.h"

static JYNGetKeyWords * getKeywords = nil;

@implementation JYNGetKeyWords
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!getKeywords)
    {
        getKeywords = [[JYNGetKeyWords alloc] init];
    }
    return getKeywords;
}

- (long)date {
    NSDate *datenow = [NSDate date];
    long timSp = (long)[datenow timeIntervalSince1970];
    return timSp;
}


-(void)getKeywordsWithNumber:(NSString*)number
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        long myTime = (long)[[NSDate date] timeIntervalSince1970];
        NSString *tokenStr = [NSString stringWithFormat:@"8ca8188ca3d9b1c8%ld",myTime];
        NSString *md5TokenStr = [MyMD5 md5:tokenStr];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/bindPhoneOne?number=%@&shopid=%@&uid=%@&token=%@&time=%ld",number,singletonClass.shopid,singletonClass.currentUserId,md5TokenStr,myTime]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    
    NSDictionary * dic = [str JSONValue];
    
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        [delegate checkPhoneAndValidCodeForRegistSuccess:[dic objectForKey:@"data"]];
    }
    else
    {
        [delegate checkPhoneAndValidCodeForRegistFailed:[dic objectForKey:@"message"]];
    }
}

@end

