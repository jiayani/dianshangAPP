//
//  FY_Cloud_sendScore.h
//  DianShangApp
//
//  Created by 霞 王 on 14-7-7.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"
@interface FY_Cloud_sendScore : NSObject
{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;
    
}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)sendScoreWithShareType:(NSString *)shareType andShareTime:(NSString *)shareTime;
@end
