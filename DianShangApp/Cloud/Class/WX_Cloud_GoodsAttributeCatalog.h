//
//  WX_Cloud_GoodsAttributeCatalog.h
//  DianShangApp
//
//  Created by 霞 王 on 14-10-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"

@interface WX_Cloud_GoodsAttributeCatalog : NSObject
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData *recivedData;

+ (id)share;
-(void)syncGoodsAttributeCatalog:(int)flag;
@end
