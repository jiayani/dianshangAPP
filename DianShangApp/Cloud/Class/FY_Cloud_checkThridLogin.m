//
//  FY_Cloud_checkThridLogin.m
//  TianLvApp
//
//  Created by Fuy on 14-9-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_checkThridLogin.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
#import "MyMD5.h"
static FY_Cloud_checkThridLogin *checkLogin = nil;
@implementation FY_Cloud_checkThridLogin
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!checkLogin)
    {
        checkLogin = [[FY_Cloud_checkThridLogin alloc] init];
    }
    return checkLogin;
}

-(void)checkThirdLoginWithOpenID:(NSString *)openID andLoginType:(int)type andCreateUser:(int)create andNickname:(NSString *)nickname andPassword:(NSString *)password andSex:(NSString *)sex andCity:(NSString *)city
{
    
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
         WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * passwordMD5 = [MyMD5 md5:password];
        NSString * urlStr;
        urlStr = [singletonClass.serviceURL  stringByAppendingString:[NSString stringWithFormat:@"Iphone/checkThridLogin?&authorize_id=%@&shopid=%@&authorize_type=%d&create_user=%d&authorize_nick=%@&password=%@&sex=%@&city=%@", openID, singletonClass.shopid, type, create, nickname, passwordMD5, sex, city]];
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
     [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSDictionary *data = [dic objectForKey:@"data"];
        [delegate checkLoginSuccess:data];
    }
    else if([[dic objectForKey:@"code"] intValue] == 101)
    {
        [delegate checkLoginFailed:[dic objectForKey:@"message"]];
        
    }else if([[dic objectForKey:@"code"] intValue] == 103)
    {
        [delegate checkLoginFrozen:[dic objectForKey:@"message"]];
        
    }else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate checkLoginOtherFailed:@"请求失败"];
        }else
        {
            [delegate checkLoginOtherFailed:[dic objectForKey:@"message"]];
        }
    }
}

@end
