//
//  FY_Cloud_giftCancel.h
//  DianShangApp
//
//  Created by Fuy on 14-7-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"
@interface FY_Cloud_giftCancel : NSObject
{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;

}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)cancelOrderWithOrderID:(NSString *)orderID andGiftType:(NSString *)giftType;

@end
