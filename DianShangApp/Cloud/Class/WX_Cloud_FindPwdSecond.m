//
//  WX_Cloud_FindPwdSecond.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-21.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_FindPwdSecond.h"
#import "SBJson.h"
#import "WX_Cloud_IsConnection.h"

static WX_Cloud_FindPwdSecond *findPwdSecond = nil;

@implementation WX_Cloud_FindPwdSecond
@synthesize delegate;
@synthesize recivedData;

+ (id)share
{
    if (!findPwdSecond)
    {
        findPwdSecond = [[WX_Cloud_FindPwdSecond alloc] init];
    }
    return findPwdSecond;
}

- (void)findPasswordWithKeywords:(NSString*)keywords flag:(int)keywordsID type:(int)type
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/findMyPasswordTwo?keywords=%@&shopid=%@&uid=%d&type=%d",keywords,singletonClass.shopid,keywordsID,type]];
        NSURL * url = [[NSURL alloc] initWithString:urlStr];
        NSURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    
    if ([[dic objectForKey:@"code"] intValue] == 100 )
    {
        [delegate verifyCodeSuccess:nil];
    }
    else
    {
        [delegate verifyCodeFaild:nil andErrorMessage:[dic objectForKey:@"message"]];

    }
}



@end
