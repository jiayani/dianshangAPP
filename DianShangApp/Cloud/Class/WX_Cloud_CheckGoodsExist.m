//
//  WX_Cloud_CheckGoodsExist.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-4.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_CheckGoodsExist.h"
#import "WX_Cloud_IsConnection.h"
#import "SBJson.h"
#import "MyMD5.h"
#import "WXCommonDateClass.h"
#import "AppDelegate.h"

static WX_Cloud_CheckGoodsExist *shoppingCart = nil;

@implementation WX_Cloud_CheckGoodsExist

@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!shoppingCart)
    {
        shoppingCart = [[WX_Cloud_CheckGoodsExist alloc] init];
    }
    return shoppingCart;
}

- (void)checkGoods:(NSString *)jsonStr
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_goodsSync?dish=%@&shopid=%@&version=%@", jsonStr, singletonClass.shopid, singletonClass.version]];
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }else{
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{

    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];

    NSDictionary * dic = [str JSONValue];

    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSArray * goodsArray = [dic objectForKey:@"data"];

        [delegate checkGoodsExist:goodsArray];
        
    }
    else
    {
        [delegate checkGoodsExistFailed:[dic objectForKey:@"message"]];
    }
}

@end
