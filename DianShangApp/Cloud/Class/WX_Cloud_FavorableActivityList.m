//
//  WX_Cloud_FavorableActivityList.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_FavorableActivityList.h"
#import "WXCommonDateClass.h"
#import "SBJson.h"
#import "WX_Cloud_IsConnection.h"
#import "WXCommonSingletonClass.h"

static WX_Cloud_FavorableActivityList * favorableActivityList = nil;

@implementation WX_Cloud_FavorableActivityList


@synthesize delegate;
@synthesize recivedData;

+ (id)share
{
    if (!favorableActivityList)
    {
        favorableActivityList = [[WX_Cloud_FavorableActivityList alloc] init];
    }
    return favorableActivityList;
}

- (void)syncFavorableActivityByPage:(NSString *)productId page:(int)page
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        
        NSString * str = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_activity?page=%d&productId=%@&shopid=%@",page,productId,singletonClass.shopid]];
        
        NSString * urlStr = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [[NSURL alloc] initWithString:urlStr];
        NSURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        [delegate syncGetFavorableActivitySuccess:dic];
    }
    else
    {
        NSString * str = [dic objectForKey:@"message"];
        [delegate syncGetFavorableActivityFailed:str];
    }
}

@end


