//
//  FY_Cloud_getPhone.m
//  DianShangApp
//
//  Created by Fuy on 14-10-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_getPhone.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_getPhone *getPhone = nil;
@implementation FY_Cloud_getPhone

@synthesize recivedData;

+(id)share
{
    if (!getPhone)
    {
        getPhone = [[FY_Cloud_getPhone alloc] init];
    }
    return getPhone;
}

-(void)getPhone
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr;
        urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/get400?shopid=%@",singletonClass.shopid]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSString * phone = [[dic objectForKey:@"data"] objectForKey:@"phone"];
        singletonClass.phoneNumber = phone;
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            singletonClass.phoneNumber = @"";
        }
        else
        {
            singletonClass.phoneNumber = @"";
        }
    }
}

@end
