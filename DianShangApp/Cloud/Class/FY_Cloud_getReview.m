//
//  FY_Cloud_getReview.m
//  DianShangApp
//
//  Created by Fuy on 14-6-10.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_getReview.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"

static FY_Cloud_getReview * getReview = nil;
@implementation FY_Cloud_getReview
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!getReview)
    {
        getReview = [[FY_Cloud_getReview alloc] init];
    }
    return getReview;
}

-(void)getReview:(int)page :(NSString *)productid
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_getProductEvaluate?shopid=%@&uid=%@&page=%d&productId=%@", singletonClass.shopid,singletonClass.currentUserId,page,productid]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
//        NSArray * allInfo = [dic objectForKey:@"data"];
        [delegate syncGetReviewSuccess:dic];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncGetReviewFailed:@"请求失败"];
        }
        else
        {
            [delegate syncGetReviewFailed:[dic objectForKey:@"message"]];
        }
    }
}

@end
