//
//  WX_Cloud_BindPhoneFist.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-22.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WX_Cloud_BindPhoneFist.h"
#import "SBJson.h"
#import "WX_Cloud_IsConnection.h"
#import "MyMD5.h"

static WX_Cloud_BindPhoneFist *bindPhone = nil;
@implementation WX_Cloud_BindPhoneFist


@synthesize delegate;
@synthesize recivedData;


+ (id)share
{
    if (!bindPhone)
    {
        bindPhone = [[WX_Cloud_BindPhoneFist alloc] init];
    }
    return bindPhone;
}

- (void)verifyPhone:(NSString*)phoneStr
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        long myTime = (long)[[NSDate date] timeIntervalSince1970];
        NSString *tokenStr = [NSString stringWithFormat:@"%@8ca8188ca3d9b1c8%ld",phoneStr,myTime];
        NSString *md5TokenStr = [MyMD5 md5:tokenStr];
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/bindPhoneOne?number=%@&shopid=%@&uid=%@&token=%@&time=%ld",phoneStr,singletonClass.shopid,singletonClass.currentUserId,md5TokenStr,myTime]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        //无网络
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];

    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        [delegate verifyPhoneOrEmailSuccess:nil];
    }
    else
    {
        NSString * errMsg = [dic objectForKey:@"message"];
        [delegate verifyPhoneOrEmailFailed:nil errCode:errMsg];
    }
    
}

@end

