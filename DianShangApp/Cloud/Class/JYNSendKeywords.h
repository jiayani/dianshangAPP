//
//  JYNSendKeywords.h
//  TianLvApp
//
//  Created by wa on 14-9-10.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"
@interface JYNSendKeywords : NSObject

{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;
}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)getPasswordWithKeywords:(NSString*)keywords andemail:(NSString *) email flag:(int)keywordsID;


@end
