//
//  FY_Cloud_giftPay.h
//  DianShangApp
//
//  Created by Fuy on 14-6-25.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"
@interface FY_Cloud_giftPay : NSObject
{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;

}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)getOrderWithGifts:(NSString *)gifts andRemark:(NSString *)remark andContactld:(NSString *)contactId;

@end
