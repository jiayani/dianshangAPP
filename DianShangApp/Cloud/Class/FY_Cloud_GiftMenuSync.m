//
//  FY_Cloud_GiftMenuSync.m
//  DianShangApp
//
//  Created by Fuy on 14-5-30.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_GiftMenuSync.h"
#import "SBJson.h"
#import "WYDDownloadImage.h"
#import "GuideAnimation.h"
#import "AppDelegate.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_GiftMenuSync * giftMenu = nil;

@implementation FY_Cloud_GiftMenuSync
@synthesize delegate;
@synthesize recivedData;
+(id)share
{
    if (!giftMenu)
    {
        giftMenu = [[FY_Cloud_GiftMenuSync alloc] init];
    }
    return giftMenu;
}

-(void)giftMenuSync
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/getImpGiftsTypeListByShop?shopid=%@",singletonClass.shopid]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];

    NSDictionary * dic = [str JSONValue];

    if ([[dic objectForKey:@"code"] intValue]== 100)
    {
        
        NSDictionary * allGiftInfo = [dic objectForKey:@"data"];//礼品集合
        
        [delegate syncGiftMenuSuccess:allGiftInfo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate syncGiftMenuFailed:@"请求失败"];
        }
        else
        {
            [delegate syncGiftMenuFailed:[dic objectForKey:@"message"]];
        }
        
    }
    
}
@end
