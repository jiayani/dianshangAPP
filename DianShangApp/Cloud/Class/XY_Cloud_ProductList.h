//
//  XY_Cloud_ProductList.h
//  DianShangApp
//
//  Created by wa on 14-6-10.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"

@interface XY_Cloud_ProductList : NSObject
{
    id <WX_Cloud_Delegate> delegate;
    NSMutableData * recivedData;
}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)productListSyncWithType:(int)type andKeyword:(NSString *)keyword andSortype:(int)sortType  andClass1:(int)class1_id andClass2:(int)class2_id andPage:(int)page andAtt1:(NSString *)att1_id andAtt2:(NSString *)att2_id;
@end
