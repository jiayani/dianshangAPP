//
//  FY_Cloud_setWeiBo.m
//  DianShangApp
//
//  Created by Fuy on 14-7-8.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FY_Cloud_setWeiBo.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
static FY_Cloud_setWeiBo * setWeiBo = nil;
@implementation FY_Cloud_setWeiBo
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!setWeiBo)
    {
        setWeiBo = [[FY_Cloud_setWeiBo alloc] init];
    }
    return setWeiBo;
}

-(void)setWeiBoWithType:(NSString *)type andQQOpenID:(NSString *)openID andNonce:(NSString *)nonce andSingature:(NSString *)singature andToken:(NSString *)token andTimestamp:(NSString *)timestamp andVeritier:(NSString *)veritier
{
    
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr;
        urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/bindWeibo?&uid=%@&type=%@&shopid=%@&qq_openid=%@&qq_nonce=%@&qq_signature_method=%@&qq_token=%@&qq_timestamp=%@&qq_veritier=%@",singletonClass.currentUserId, type, singletonClass.shopid, openID, nonce, singature, token, timestamp, veritier]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];

    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSString *weibo = [dic objectForKey:@"message"];
        [delegate setWeiBoSuccess:weibo];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate setWeiBoFailed:@"请求失败"];
        }
        else
        {
            [delegate setWeiBoFailed:[dic objectForKey:@"message"]];
        }
    }
}

@end
