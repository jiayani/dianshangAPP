//
//  AsynImageView.m
//  AsynImage
//
//  Created by administrator on 13-3-5.
//  Copyright (c) 2013年 enuola. All rights reserved.
//

#import "AsynImageView.h"
#import <QuartzCore/QuartzCore.h>
#import "WXCommonViewClass.h"
#import "WX_Cloud_IsConnection.h"

@implementation AsynImageView

@synthesize imageURL = _imageURL;
@synthesize placeholderImage = _placeholderImage;
@synthesize defaultImageView = _defaultImageView;
@synthesize type = _type;


@synthesize fileName = _fileName;
@synthesize delegate = _delegate;
@synthesize isFromMore = _isFromMore;
@synthesize isFromWooGift = _isFromWooGift;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _isFromMore = NO;
        _isFromWooGift = NO;
        
    }
    return self;
}
/*根据压缩完的图片设置图片大小*/
- (void)resetImageViewFrame{

    if (_defaultImageView != nil && self.image != nil) {
        [_defaultImageView removeFromSuperview];
    
        
    }
    if (self.image != nil) {
        CGRect newRect = self.frame;
        CGSize picSize;
        if (_type == 1) {//按照高度伸缩
            picSize = [WXCommonViewClass getThumbnailsByHeight:self.image maxSize:newRect.size];
        }else if (_type == 2){//按照长度伸缩
            picSize = [WXCommonViewClass getThumbnailsByWidth:self.image maxSize:newRect.size];
        
        }else{
            picSize = newRect.size;
        }
        //x的偏移量
        CGFloat pointX = (newRect.size.width - picSize.width) / 2;
        //Y的偏移量
        CGFloat pointY = (newRect.size.height - picSize.height) / 2;
        
        self.frame = CGRectMake(newRect.origin.x + pointX, newRect.origin.y + pointY, picSize.width, picSize.height);
        
    }
}
//重写placeholderImage的Setter方法
-(void)setPlaceholderImage:(UIImage *)placeholderImage
{
    _placeholderImage = nil;
    _defaultImageView = nil;
    if(placeholderImage != _placeholderImage)
    {
        _placeholderImage = placeholderImage;
        
        if (_defaultImageView == nil) {
            int width = 50;
            if (self.frame.size.width > 200) {
                width = 100;
            }
            _defaultImageView =  [[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width-width)/2,(self.frame.size.height-width)/2,width,width)];
            _defaultImageView.image = _placeholderImage;
            [self addSubview:_defaultImageView];
        }
    }
}

//重写imageURL的Setter方法
-(void)setImageURL:(NSString *)imageURL
{
    if(imageURL != _imageURL)
    {
        if (_defaultImageView == nil) {
            int width = 50;
            if (_type == 1 || _type == 2 || _type == 3) {
                width = 100;
            }
            _defaultImageView =  [[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width-width)/2,(self.frame.size.height-width)/2,width,width)];
            _defaultImageView.image = _placeholderImage;
            [self addSubview:_defaultImageView];
        }
        

        _imageURL = imageURL ;
    }
    if (_imageURL == nil || _imageURL.length <= 0) {
        return;
    }
    BOOL isRigntURL = NO;
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    NSString  *serviceURL = singletonClass.getImageServiceRUL;
    NSString *htmlSertURL = singletonClass.htmlServiceRUL;
    NSString *serviceURlOfWooGist = singletonClass.imageServiceURLOfWooGift;
    
    serviceURL = [serviceURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (![self.imageURL hasPrefix:@"http://"] && _isFromMore == NO) {
        if (_isFromWooGift) {
            self.imageURL = [NSString stringWithFormat:@"%@%@",serviceURlOfWooGist,self.imageURL];
        }else{
            self.imageURL = [NSString stringWithFormat:@"%@%@",serviceURL,self.imageURL];
        }
        
    }
    isRigntURL = YES;

    if(self.imageURL && isRigntURL == YES)
    {
        //确定图片的缓存地址
        NSArray *path=NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *docDir=[path objectAtIndex:0];
        NSFileManager *fm = [NSFileManager defaultManager];
        //begin 获得图片路径 
        NSString *tmpPath;
        //去掉IP地址 如http://192.168.1.10//A/B/1.png  得到的是 A/B/1.png
        NSString *tempUrl ;
        if (_isFromMore) {
            tempUrl = [self.imageURL substringFromIndex:htmlSertURL.length];
        }else{
            if (_isFromWooGift &&  self.imageURL.length >= (serviceURlOfWooGist.length + 1)) {
                tempUrl = [self.imageURL substringFromIndex:serviceURlOfWooGist.length + 1];
            }else if( self.imageURL.length >= (serviceURL.length + 1)){
                tempUrl = [self.imageURL substringFromIndex:serviceURL.length + 1];
            }else{
                return;
            }
            
            
        }
        NSArray *lineArray = [tempUrl componentsSeparatedByString:@"/"];
        for (int i = 0; i < lineArray.count - 1; i++) {
            NSString *superPath , *currentPath;
            superPath = @"";
            currentPath = [lineArray objectAtIndex:i];
            for (int j = 0; j < i; j++) {
                if (j == 0) {
                    superPath = [lineArray objectAtIndex:j];
                }else{
                    superPath = [NSString stringWithFormat:@"%@/%@",superPath,[lineArray objectAtIndex:j]];
                }
            
            }
            if (i > 0 && superPath.length > 0) {
                superPath = [NSString stringWithFormat:@"%@/",superPath];
            }
            if (superPath != nil) {
                
                currentPath = [superPath stringByAppendingFormat:@"%@",currentPath];
                
            }
            tmpPath = [docDir stringByAppendingPathComponent:currentPath];
            //循环创建目录
            if(![fm fileExistsAtPath:tmpPath])
            {
                [fm createDirectoryAtPath:tmpPath withIntermediateDirectories:YES attributes:nil error:nil];
            }
        }
        //end
       ;
        self.fileName = [NSString stringWithFormat:@"%@/%@", docDir, tempUrl];
        
        //判断图片是否已经下载过，如果已经下载到本地缓存，则不用重新下载。如果没有，请求网络进行下载。
        if(![[NSFileManager defaultManager] fileExistsAtPath:_fileName])
        {
            /*2013-10-16 wyd*
             *添加一个条件判断，当没有网络的情况下，不去下载图片，使用默认图片*
             *用来解决无网络情况下，加载列表时严重卡的问题*
             */
            if ([[WX_Cloud_IsConnection share] isConnectionAvailable]) {
                //下载图片，保存到本地缓存中
                [self loadImage];
            }
            else
            {
                int width = 50;
                _defaultImageView =  [[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width-width)/2,(self.frame.size.height-width)/2,width,width)];
                _defaultImageView.image = _placeholderImage;
                [self addSubview:_defaultImageView];
            }
            
        }
        else
        {
            //本地缓存中已经存在，直接指定请求的网络图片
            self.image = [UIImage imageWithContentsOfFile:_fileName];
            [self resetImageViewFrame];
        }
    }
}

//网络请求图片，缓存到本地沙河中
-(void)loadImage
{
    //对路径进行编码
    @try {
        //请求图片的下载路径
        //定义一个缓存cache
        NSURLCache *urlCache = [NSURLCache sharedURLCache];
        /*设置缓存大小为1M*/
        [urlCache setMemoryCapacity:2*124*1024];
        
        //设子请求超时时间为30s
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.imageURL] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0];
        
        //从请求中获取缓存输出
        NSCachedURLResponse *response = [urlCache cachedResponseForRequest:request];
        if(response != nil)
        {
            //            NSLog(@"如果又缓存输出，从缓存中获取数据");
            [request setCachePolicy:NSURLRequestReturnCacheDataDontLoad];
        }
        
        /*创建NSURLConnection*/
        if(!connection)
            connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
        
        //开启一个runloop，使它始终处于运行状态
        
        UIApplication *app = [UIApplication sharedApplication];
        app.networkActivityIndicatorVisible = YES;

        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];

        
    }
    @catch (NSException *exception) {
        //        NSLog(@"没有相关资源或者网络异常");
    }
    @finally {
        ;//.....
    }
}

#pragma mark - NSURLConnection Delegate Methods
//请求成功，且接收数据(每接收一次调用一次函数)
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if(loadData==nil)
    {
        loadData=[[NSMutableData alloc]initWithCapacity:2048];
    }
    [loadData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    
}

-(NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    return cachedResponse;
    //    NSLog(@"将缓存输出");
}

-(NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response
{
    //    NSLog(@"即将发送请求");
    return request;
}
//下载完成，将文件保存到沙河里面
-(void)connectionDidFinishLoading:(NSURLConnection *)theConnection
{
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = NO;
    
    //图片已经成功下载到本地缓存，指定图片
    UIImage *newImage = nil;
    newImage = [[UIImage alloc]initWithData:loadData];
    if(newImage != nil && [loadData writeToFile:_fileName atomically:YES])
    {
        self.image = [UIImage imageWithContentsOfFile:_fileName];
        [self resetImageViewFrame];
    }

    connection = nil;
    loadData = nil;
    
}
//网络连接错误或者请求成功但是加载数据异常
-(void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error
{
    UIApplication *app = [UIApplication sharedApplication];
    app.networkActivityIndicatorVisible = NO;
    
    //如果发生错误，则重新加载
    connection = nil;
    loadData = nil;
    [self loadImage];
}

-(void)dealloc
{
    _fileName  = nil;
    connection = nil;
    _placeholderImage = nil;
    _imageURL = nil;
    _defaultImageView = nil;
    
}

@end
