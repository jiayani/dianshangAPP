//
//  WYDIsConnection.m
//  TianLvApp
//
//  Created by 王寅冬 on 13-8-1.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import "WX_Cloud_IsConnection.h"
#import "Reachability.h"

static WX_Cloud_IsConnection * isConnection = nil;
@implementation WX_Cloud_IsConnection

+(id)share
{
    if (!isConnection)
    {
        isConnection = [[WX_Cloud_IsConnection alloc] init];
    }
    return isConnection;
}

-(BOOL) isConnectionAvailable
{
    
    BOOL isExistenceNetwork = YES;
    Reachability *reach = [Reachability reachabilityWithHostName:@"www.apple.com"];
    switch ([reach currentReachabilityStatus]) {
        case NotReachable:
        {
            isExistenceNetwork = NO;
            break;
        }
        case ReachableViaWiFi:
        {
            isExistenceNetwork = YES;
            break;
        }
        case ReachableViaWWAN:
        {
            isExistenceNetwork = YES;
            break;
        }
    }
    
    return isExistenceNetwork;
}

@end
