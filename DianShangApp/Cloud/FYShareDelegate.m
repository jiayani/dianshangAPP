//
//  FYShareDelegate.m
//  DianShangApp
//
//  Created by Fuy on 14-7-2.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYShareDelegate.h"
#import "WXCommonViewClass.h"
#import "WXConfigDataControll.h"
#import "FY_Cloud_sendScore.h"
@implementation FYShareDelegate
@synthesize wbapi;
#pragma mark WeiXinRequestDelegate
-(void) onResp:(BaseResp*)resp
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if([resp isKindOfClass:[SendMessageToWXResp class]])
    {
        NSString *mesStr;
        switch (resp.errCode) {
            case 0:
                mesStr = @"分享成功";
                break;
            case -1:
                mesStr = @"意外错误";
                break;
            case -2:
                mesStr = @"用户取消操作";
                break;
            case -3:
                mesStr = @"发送失败";
                break;
            case -4:
                mesStr = @"授权失败";
                break;
            default:
                mesStr = @"意外错误";
                break;
        }
        if (singletonClass.currentUserId == nil) {
            [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:mesStr];
            return;
        }else
        {
        //调用接口，判断是否为当日首次分享
        long time = [self date];
        NSNumber *longNumber = [NSNumber numberWithLong:time];
        NSString *longStr = [longNumber stringValue];
            if (resp.errCode == 0) {
                [[FY_Cloud_sendScore share] setDelegate:(id)self];
                [[FY_Cloud_sendScore share] sendScoreWithShareType:@"1" andShareTime:longStr];
            }else
            {
                [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:mesStr];
            }
        }
    }
}
#pragma mark    SinaWeiboRequestDelegate
//新浪微博回调
- (void)didReceiveWeiboRequest:(WBBaseRequest *)request
{
    //    if ([request isKindOfClass:WBProvideMessageForWeiboRequest.class])
    //    {
    //        ProvideMessageForWeiboViewController *controller = [[[ProvideMessageForWeiboViewController alloc] init] autorelease];
    //        [self.viewController presentModalViewController:controller animated:YES];
    //    }
}

- (void)didReceiveWeiboResponse:(WBBaseResponse *)response
{
    if ([response isKindOfClass:WBSendMessageToWeiboResponse.class])
    {
        NSString *mesStr;
        switch ((int)response.statusCode) {
            case -1:
                mesStr = @"取消分享";
                break;
            case 0:
                mesStr = @"分享成功";
                break;
            case -2:
                mesStr = @"发送失败";
                break;
            case -3:
                mesStr = @"授权失败";
                break;
            case -4:
                mesStr = @"取消安装";
                break;
            default:
                mesStr = @"意外错误";
                break;
        }
        if ((int)response.statusCode == 0)
        {
            WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
            if (singletonClass.currentUserId == nil) {
                [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:@"分享成功"];
                return;
            }
            //调用接口，判断是否为当日首次分享
            long time = [self date];
            NSNumber *longNumber = [NSNumber numberWithLong:time];
            NSString *longStr = [longNumber stringValue];
            [[FY_Cloud_sendScore share] setDelegate:(id)self];
            [[FY_Cloud_sendScore share] sendScoreWithShareType:@"1" andShareTime:longStr];
            return;
        }
        
        [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:mesStr];
        
    }
    else if ([response isKindOfClass:WBAuthorizeResponse.class])
    {
        NSString *title = @"认证结果";
        NSString *message = [NSString stringWithFormat:@"响应状态: %d\nresponse.userId: %@\nresponse.accessToken: %@\n响应UserInfo数据: %@\n原请求UserInfo数据: %@",(int)response.statusCode,[(WBAuthorizeResponse *)response userID], [(WBAuthorizeResponse *)response accessToken], response.userInfo, response.requestUserInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
    }
}
//获取当前时间的时间戳
- (long)date {
    NSDate *datenow = [NSDate date];
    NSTimeZone *zone = [NSTimeZone  timeZoneWithName:@"Asia/Shanghai"];
    //    NSTimeZone *zone = [NSTimeZone  systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate:datenow];
    NSDate *localeDate = [datenow  dateByAddingTimeInterval: interval];
    long  timSp = (long)[localeDate timeIntervalSince1970];

    return timSp;
}
//赠积分回调
-(void)syncSendScoreSuccess:(NSDictionary *)data
{
    NSString *str = [NSString stringWithFormat:@"%@", [data objectForKey:@"isfirst"]];
    if ([str isEqualToString:@"1"]) {
        [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:[NSString stringWithFormat:@"分享成功,为您赠送%@积分", [data objectForKey:@"sendIntegral"]]];
    }else
    {
        [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:@"分享成功"];
    }
}

-(void)syncSendScoreFailed:(NSString*)errMsg
{
    [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:@"分享成功"];
}

#pragma mark    TXWeiboRequestDelegate

/**
 * @brief   接口调用成功后的回调
 * @param   INPUT   data    接口返回的数据
 * @param   INPUT   request 发起请求时的请求对象，可以用来管理异步请求
 * @return  无返回
 */
- (void)didReceiveRawData:(NSData *)data reqNo:(int)reqno
{
    NSString *strResult = [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
    //[NSString stringWithCharacters:[data bytes] length:[data length]];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:strResult delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
    
}
/**
 * @brief   接口调用失败后的回调
 * @param   INPUT   error   接口返回的错误信息
 * @param   INPUT   request 发起请求时的请求对象，可以用来管理异步请求
 * @return  无返回
 */
- (void)didFailWithError:(NSError *)error reqNo:(int)reqno
{
    NSString *str = [[NSString alloc] initWithFormat:@"refresh token error, errcode = %@",error.userInfo];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:str delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)DidAuthFinished:(WeiboApi *)wbapi_
{
//    NSString *str = [[NSString alloc]initWithFormat:@"accesstoken = %@\r openid = %@\r appkey=%@ \r appsecret=%@\r", wbapi_.accessToken, wbapi_.openid, wbapi_.appKey, wbapi_.appSecret];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"json", @"format", @"hi,weibo sdk", @"content", nil];
    [wbapi requestWithParams:params apiName:@"t/add_pic" httpMethod:@"POST" delegate:self];
    
}
/**
 * @brief   授权成功后的回调
 * @param   INPUT   wbapi   weiboapi 对象，取消授权后，授权信息会被清空
 * @return  无返回
 */
- (void)DidAuthCanceled:(WeiboApi *)wbapi_
{
    
}

/**
 * @brief   授权成功后的回调
 * @param   INPUT   error   标准出错信息
 * @return  无返回
 */
- (void)DidAuthFailWithError:(NSError *)error
{
//    NSString *str = [[NSString alloc] initWithFormat:@"refresh token error, errcode = %@",error.userInfo];
    
}
@end
