//
//  FYShareDelegate.h
//  DianShangApp
//
//  Created by Fuy on 14-7-2.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
//微信相关
#import "WXApi.h"
#import "WeiboSDK.h"
#import "WeiboApi.h"
@interface FYShareDelegate : NSObject <WXApiDelegate,WeiboSDKDelegate>
{
   WeiboApi *wbapi;
}
@property (nonatomic , retain) WeiboApi *wbapi;
@end
