//
//  WYDIsConnection.h
//  DianShang
//
//  Created by 霞 王 on 14-5-12.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WX_Cloud_IsConnection : NSObject

+(id)share;
-(BOOL) isConnectionAvailable;

@end
