//
//  AsynImageView.h
//  AsynImage
//
//  Created by administrator on 13-3-5.
//  Copyright (c) 2013年 enuola. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol  AsynImageViewDelegate <NSObject>
@optional
- (void)reloadDatas;
@end
@interface AsynImageView : UIImageView
{
    NSURLConnection *connection;
    NSMutableData *loadData;
    CGRect *myFrame;
}
@property (nonatomic) BOOL isFromMore;
@property (nonatomic) BOOL isFromWooGift;
//图片对应的缓存在沙河中的路径
@property (nonatomic, retain) NSString *fileName;

//指定默认未加载时，显示的默认图片
@property (nonatomic, retain) UIImage *placeholderImage;
//请求网络图片的URL
@property (nonatomic, retain) NSString *imageURL;
@property (nonatomic, assign) CGFloat WidthOfdefautImage;

//默认图片
@property (nonatomic, retain) UIImageView *defaultImageView;
/* 
   1:按照高度来伸缩
   2:按照长度伸缩

 */
@property (readwrite) int type;

@property (strong,nonatomic) id <AsynImageViewDelegate> delegate;

@end
