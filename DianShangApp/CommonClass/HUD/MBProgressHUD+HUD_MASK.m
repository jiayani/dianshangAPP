//
//  MBProgressHUD+HUD_MASK.m
//  HS5iScaleBodyFat
//
//  Created by zhaoRan on 12-8-6.
//
//

#import "MBProgressHUD+HUD_MASK.h"

@implementation MBProgressHUD (HUD_MASK)
+(id)shareInstanceWithView:(UIView *)view{
    static MBProgressHUD *HUD;
    if (HUD == nil) {
        HUD = [[MBProgressHUD alloc] initWithView:view];
    }
        else{
        [HUD release];
        HUD = nil;
        HUD = [[MBProgressHUD alloc] initWithView:view];

    }
    return HUD;
}
+(void)maskScreenForScaleActionE2P:(UIView *)currentView Title:(NSString *)Title{
    MBProgressHUD *HUD = [self shareInstanceWithView:currentView];
    [currentView addSubview:HUD];
    HUD.mode = MBProgressHUDModeDeterminate;
    HUD.labelText = Title;
    HUD.progress = 0.0f;
    [HUD showWhileExecuting:@selector(myProgressTask:) onTarget:self withObject:HUD animated:YES];
}

+(void)myProgressTask:(MBProgressHUD *)HUD{
    float progress = 0.0f;
    while (progress < 1.0f) {
        progress += 0.01f;
        HUD.progress = progress;
        usleep(30000);
    }
}

+(void)deleteShareInstance{
    MBProgressHUD *HUD = [self shareInstanceWithView:nil];
    [HUD release];
}
    
    

@end
