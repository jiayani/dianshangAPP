//
//  MBProgressHUD+HUD_MASK.h
//  HS5iScaleBodyFat
//
//  Created by zhaoRan on 12-8-6.
//
//

#import "MBProgressHUD.h"

@interface MBProgressHUD (HUD_MASK)
+(id)shareInstanceWithView:(UIView *)view;
+(void)maskScreenForScaleActionE2P:(UIView *)currentView Title:(NSString *)Title;
+(void)deleteShareInstance;
@end
