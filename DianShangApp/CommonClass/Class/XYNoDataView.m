//
//  XYNoDataView.m
//  DianShangApp
//
//  Created by wa on 14-6-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XYNoDataView.h"
#import "AppDelegate.h"
@implementation XYNoDataView

- (id)initWithFrame:(CGRect)frame
{
    XYNoDataView *noDataView = [[[NSBundle mainBundle] loadNibNamed:@"XYNoDataView" owner:nil options:
                                 nil] objectAtIndex:0];
    noDataView.megDownLabel.lineBreakMode = NSLineBreakByWordWrapping;
    noDataView.megDownLabel.numberOfLines = 0;
    noDataView.megDownLabel.textAlignment = NSTextAlignmentCenter;
    noDataView.megUpLabel.text = @"";
    noDataView.megDownLabel.text = @"";
    
    if (noDataView) {
        noDataView.frame = frame;
    }
    
    return noDataView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
