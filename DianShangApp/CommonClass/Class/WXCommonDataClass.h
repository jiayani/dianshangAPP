//
//  WXCommonDataClass.h
//  TianLvApp
//
//  Created by 霞 王 on 13-7-24.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WXCommonDataClass : NSObject

/*
 * 把从后台服务器端读取400电话保存到NSUserDefaults中
 */
+ (void)setPhoneNumber:(NSString *)phoneNumberStr;

/*
 * 得到400电话
 */
+ (NSString *)setPhoneNumber;

/*
 * 根据当前时间生成主键id
 */
+ (NSString *)getPrimaryKeyFromDate;

/**wangxia 2013-9-16
 *得到本地图片地址
 */
+ (NSString *)getImageURL:(NSString *)imageURL;

/*zengjun 2014-2-11 */
+ (NSString *)getPayWayStrBy:(int)payNo;

/**wangxia 2014-6-4
 *得到价格保留2位小数的字符串
 */
+ (NSString *)getPriceString:(NSNumber *)price;

/**wangxia 2014-6-12
 *字典或者数组转化为json字符串
 */
+ (NSString *)toJsonStr:(id)theData;

/**wangxia 保存团购时间
 */
+ (void)setGroupBuyTime:(NSString *)time;

/**wangxia 保存优惠时间
 */
+ (void)setPreferInforTime:(NSString *)time;
/**比较版本号
 */
+(BOOL)compareVersion:(NSString *)newVersionStr :(NSString *)currentVersion;

//拨打客服电话 付岩2014-10-9
+ (void)callPhone:(NSString *)phoneNumber;

//从服务器端获得数据
- (NSURLConnection *)connectURL:(NSString *)connectorName parametersStr:(NSString *)parametersStr isPost:(BOOL)isPost delegate:(id)delegate;
@end
