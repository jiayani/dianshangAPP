//
//  WXConfigDataControll.h
//  TianLvApp
//
//  Created by 霞 王 on 13-7-1.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WXConfigDataControll : NSObject

/** wangxia 2013-7-1
 *得到配置文件中的所有配置信息
 *
 */
+ (NSDictionary *)getInfoOfConfig;

/** wangxia 2013-7-1
 *得到导航条的背景颜色
 */
+ (UIColor *)getBackgroundColorOfNav;

/** wangxia 2013-10-25
 **得到导航上title的字体颜色
 */
+ (UIColor *)getTitleFontColorOfNav;

/** wangxia 2013-11-23
 **得到导航上title字体阴影的颜色
 */
+ (UIColor *)getTitleFontShadowColorOfNav;

/** wangxia 2013-11-28
 **得到导航上title字体阴影的方向
 */
+ (CGFloat)getDirectionOfShadowColorOfNav;


/**wangxia 2013-10-25
 *TitleFontSizeOfNav:得到导航的字体大小
 */
+ (CGFloat)getTitleFontSizeOfNav;

/** wangxia 2013-7-1
 *把从配置文件中读取的信息保存到本地NSUserDefaults中
 *BackgroundColorOfNac: 导航背景颜色
 */
+ (void)saveConfigInfoToDefualt;

/**wangxia 2013-7-1
 *FontSizeOfMenu:得到菜单字体大小
 */
+ (NSString *)getFontSizeOfMenu;

/**wangxia 2013-9-12
 *getFontColorOfMenuArray:得到菜单字体颜色数组
 */
+ (NSArray *)getFontColorOfMenuArray;

/**wangxia 2013-7-1
 *FontColorOfMenu:得到菜单字体颜色
 */
+ (UIColor *)getFontColorOfMenu;

/**wangxia 2013-10-28
 *FontColorOfSelectedMenu:得到选中后的菜单字体颜色
 */
+ (UIColor *)getFontColorOfSelectedMenu;

/**wangxia 2014-5-14
 *FontColorOfPrice:价格字体颜色
 */
+ (UIColor *)getFontColorOfPrice;

/**wangxia 2014-5-14
 *ColorOfSubscript:脚标颜色
 */
+ (UIColor *)getColorOfSubscript;

/**wangxia 2014-5-14
 *FontColorOfIntegral:积分字体颜色
 */
+ (UIColor *)getFontColorOfIntegral;

/**wangxia 2013-7-24
 *ServiceURL:服务器地址
 */

+ (NSString *)getServiceURLStr;
/**wangxia
 *ServiceURL:图片服务服务器地址
 */
+ (NSString *)getImageServiceRUL;

/**wangxia
 *imageServiceURLOfWooGift:哇点礼品图片服务服务器地址
 */
+ (NSString *)getImageServiceURLOfWooGift;

/**wangxia 2013-7-24
 *ShopID:得到商户的ID
 */
+ (NSString *)getShopID;

/** zj 2013-8-1
 *AppName 得到应用名称
 */
+ (NSString *)getAppName;

/**wangxia 2013-7-24
 *AppShare:得到应用分享的模版
 */
+ (NSString *)getAppShare;

/**wangxia 2013-7-24
 *ProductShare:得到商品分享的模版
 */
+ (NSString *)getproductShare;

/**
 zj 2013-11-20
 @returns 得到商品(不带价格)分享的模板
 */
+ (NSString *)getProductNoPriceShare;

/**wangxia 2013-7-24
 *PromotionsShare:得到优惠活动分享的模版
 */
+ (NSString *)getPromotionsShare;

/** wangxia 2013-9-30
 *DeviceToken  设置旧的设备口令牌
 */
+ (void)setDeviceToken:(NSString *)deviceToken;
/** wangxia 2013-9-30
 *DeviceToken  得到旧的设备口令牌
 */
+ (NSString *)getDeviceToken;

/** wangxia 2013-9-3
 *PhoneNumber 得到客服电话
 */
+ (NSString *)getPhoneNumber;

/** wangxia 2013-9-28
 *得到当前程序的版本号
 */
+ (NSString *) getCuurentVersion;

/** wangxia 2013-9-3
 *PhoneNumber 设置得到客服电话
 */
+ (void)setPhoneNumber:(NSString *)phoneStr;

/**wangxia 2013-9-12
 *StaticHtmlUrl:得到静态页面访问服务器地址
 */
+ (NSString *)getStaticHtmlUrlStr;

/**wangxia 2013-9-27
 *BaiduMapKey:设置百度的key
 */
+ (void)setBaiduMapKey:(NSString *)baiduMapKeyStr;

/**wangxia 2013-9-27
 *BaiduMapKey:得到百度的key
 */
+ (NSString *)getBaiduMapKey;

/**wangxia 2013-9-27
 *RedirectUrl:请正确填写您的应用网址，否则将导致授权失败
 */
+ (NSString *)getRedirectUrl;

/**wangxia 2013-9-29
 *RedirectUrl :setting
 */
+ (void) setRedirectUrl:(NSString *)redirectUrlStr;


/**wangxia 2013-9-29
 *set SinaAppKey
 */
+ (void) setSinaAppKey:(NSString *)sinaAppKeyStr;

/**wangxia 2013-9-29
 */
+ (NSString *) getSinaAppKey;

/**wangxia 2013-9-29
 *set SinaAppSecret
 */
+ (void) setSinaAppSecret:(NSString *)sinaAppSecretStr;

/**wangxia 2013-9-29
 *get SinaAppSecret
 */
+ (NSString *) getSinaAppSecret;

/**fuyan 2014-10-15
 *set UMengiPhoneSecret
 */
+ (void) setUMengiPhoneSecret:(NSString *)uMengiPhoneSecretStr;

/**fuyan 2014-10-15
 *get UMengiPhoneSecret
 */
+ (NSString *) getUMengiPhoneSecret;

+ (void) setUMengChannelId:(NSString *)uMengiPhoneSecretStr;

+ (NSString *) getUMengChannelId;

/**wangxia 2013-9-29
 *set QQAppKey
 */
+ (void) setQQAppKey:(NSString *)qqAppKeyStr;

/**wangxia 2013-9-29
 *get QQAppKey
 */
+ (NSString *) getQQAppKey;

/**wangxia 2013-9-29
 *set QQAppSecret
 */
+ (void) setQQAppSecret:(NSString *)qqAppSecretStr;

/**wangxia 2013-9-29
 *get QQAppSecret
 */
+ (NSString *) getQQAppSecret;

/* wyd...2014-02-10
 * set can_comepay
 */
+(void)setCan_comepay:(NSString*)yesOrNo;

/* wyd...2014-02-10
  get can_comepay
 */
+(NSString*)getCan_comepay;

/* wyd...2014-02-10
 * set can_embed
 */
+(void)setCan_embed:(NSString*)yesOrNo;

/* wyd...2014-02-10
 get can_embed
 */
+(NSString*)getCan_embed;

/* wyd...2014-02-10
 * set can_wappay
 */
+(void)setCan_wappay:(NSString*)yesOrNo;

/* wyd...2014-02-10
 get can_wappay
 */
+(NSString*)getCan_wappay;

/* wyd...2014-02-10
 * set can_return
 */
+(void)setCan_return:(NSString*)yesOrNo;

/* wyd...2014-02-10
 get can_return
 */
+(NSString*)getCan_return;


/* rong...2014-03-18
 * set can_delivery
 */
+(void)setCan_delivery:(NSString*)yesOrNo;

/* rong...2014-03-18
 get can_delivery
 */
+(NSString*)getCan_delivery;

/* rong...2014-03-18
 * set can_delivery
 */
+(void)setCan_shopDelivery:(NSString*)yesOrNo;

/* rong...2014-03-18
 get can_delivery
 */
+(NSString*)getCan_shopDelivery;

/**wangxia 2014-8-18
 *set WeiXinAppKey
 */
+ (void)setWeiXinAppKey:(NSString *)appKeyStr;

/**wangxia 2014-8-18
 *get WeiXinAppKey
 */
+ (NSString *)getWeiXinAppKey;

//2014-10-14 付岩 QQID
+ (void)setQQLoginID:(NSString *)appID;
+ (NSString *)getQQLoginID;
@end
