//
//  WXCommonDataClass.m
//  TianLvApp
//
//  Created by 霞 王 on 13-7-24.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//  这个类中主要是所有数据操作方法的封装

#import "WXCommonDataClass.h"

@implementation WXCommonDataClass

/*
 * 把从后台服务器端读取400电话保存到NSUserDefaults中
 */
+ (void)setPhoneNumber:(NSString *)phoneNumberStr{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:phoneNumberStr forKey:@"PhoneNumber"];
    [defaults synchronize];

}
/*
 * 得到400电话
 */
+ (NSString *)setPhoneNumber{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return  [defaults objectForKey:@"PhoneNumber"];
}
/*
 * 根据当前时间生成主键id
 */
+ (NSString *)getPrimaryKeyFromDate{
    
    NSDate *now =[NSDate date];
    return [NSString stringWithFormat:@"%f",[now timeIntervalSince1970]];
}

/**wangxia 2013-9-16
 *得到本地图片地址
 */
+ (NSString *)getImageURL:(NSString *)imageURL{
    if (imageURL.length <= 0) {
        return @"";
    }
    //确定图片的缓存地址
    NSArray *path=NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *docDir=[path objectAtIndex:0];
    //begin:拼接图片URL
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];

    NSString  *serviceURL = singletonClass.getImageServiceRUL;
    serviceURL = [serviceURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([imageURL hasPrefix:@"http://"]) {
        imageURL = [imageURL substringFromIndex:serviceURL.length + 1];
        
    }else{
        imageURL = [imageURL substringFromIndex:1];
    }
    //end
    NSString *fileName = [NSString stringWithFormat:@"%@/%@", docDir, imageURL];
    return fileName;
}
/*zengjun 2014-2-11 */
+ (NSString *)getPayWayStrBy:(int)payNo{
    switch (payNo) {
        case 1:
            return @"支付宝客户端支付";
            break;
        case 2:
            return @"支付宝网页支付";
            break;
        case 3:
            return @"到店支付";
            break;
        case 4:
            return @"货到付款";
            break;
        default:
            return @"";
            break;
    }
}

/**wangxia 2014-6-4
 *得到价格保留2位小数的字符串
 */
+ (NSString *)getPriceString:(NSNumber *)price{
    return [NSString stringWithFormat:@"%.2f",[price doubleValue]];
}

/**wangxia 2014-6-12
 *字典或者数组转化为json字符串  NSJSONWritingPrettyPrinted
 */
+ (NSString *)toJsonStr:(id)theData{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:theData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}

/**wangxia 保存团购时间
 */
+ (void)setGroupBuyTime:(NSString *)time{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:time forKey:@"groupBuy"];
    [defaults synchronize];
}
/**wangxia 保存优惠时间
 */
+ (void)setPreferInforTime:(NSString *)time{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:time forKey:@"preferInfor"];
    [defaults synchronize];
}
/**比较版本号
 */
+(BOOL)compareVersion:(NSString *)newVersionStr :(NSString *)currentVersion
{
    NSArray * newVersionArr = [newVersionStr componentsSeparatedByString:@"."];
    NSArray * currentVersionArr = [currentVersion componentsSeparatedByString:@"."];
    int arrayCount = newVersionArr.count <= currentVersionArr.count ? newVersionArr.count : currentVersionArr.count;
    for (int i = 0; i < arrayCount; i++) {
        if ([newVersionArr objectAtIndex:i] != [currentVersionArr objectAtIndex:i]) {
            if ([[newVersionArr objectAtIndex:i] intValue] > [[currentVersionArr objectAtIndex:i] intValue]) {
                return YES;
            }else
            {
                return NO;
            }
        }
    }
    if (newVersionArr.count > currentVersionArr.count) {
        return YES;
    }
    return NO;
}

+ (void)callPhone:(NSString *)phoneNumber
{
    NSString * phoneStr = [NSString stringWithFormat:@"tel://%@",phoneNumber];
    phoneStr = [phoneStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *phoneURL = [NSURL URLWithString:phoneStr];
    [[UIApplication sharedApplication] openURL:phoneURL];
}

/**
 *connectorName:接口名称
 *parametersStr:接口传的参数除了通用的部分如shopid....
 *isPost:Yes 是post请求方式  No 是get请求方式
 */
- (NSURLConnection *)connectURL:(NSString *)connectorName parametersStr:(NSString *)parametersStr isPost:(BOOL)isPost delegate:(id)delegate{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    NSString *urlStr;
    NSURL *url;
    NSURLConnection *connection;
    NSString *bodyStr;
    if (parametersStr == nil || parametersStr.length <= 0) {
        bodyStr = [NSString stringWithFormat:@"shopid=%@",singletonClass.shopid];
    }else{
        bodyStr = [NSString stringWithFormat:@"shopid=%@&%@",singletonClass.shopid,parametersStr];
    }
    if (isPost) {
        
        urlStr = [NSString stringWithFormat:@"%@Iphone/%@",singletonClass.serviceURL,connectorName];
        url = [[NSURL alloc] initWithString:urlStr];
        NSData * bodyData = [bodyStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:bodyData];
        connection = [[NSURLConnection alloc] initWithRequest:request delegate:delegate];
    }else{
        urlStr = [NSString stringWithFormat:@"%@Iphone/%@?%@",singletonClass.serviceURL,connectorName,bodyStr];
        
        //没有这个处理的话，如果有中文的时候会出现乱码
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        connection = [NSURLConnection connectionWithRequest:request delegate:delegate];
    }
    
    return connection;
}

@end
