//
//  WXCommonViewClass.h
//  TianLvApp
//
//  Created by 霞 王 on 13-7-3.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"
@interface WXCommonViewClass : NSObject{
    CGFloat scaleBy;
}

/**wangxia 2013-7-3
 *得到导航条上的返回按钮
 */
+ (UIButton*)getBackButton;

/*WYD 2013-7-8 导航右侧按钮*/
+(UIButton*)getRightBtnOfNav:(NSString *)imageName hightlightedImageName:(NSString *)hightlightedImageName  title:(NSString *)titleStr;
/*获取导航左侧返回按钮 大小为*/
+(UIButton*)getLeftBtnOfNav:(NSString *)imageName title:(NSString *)titleStr frame:(CGRect)rect;
/*wangxia 2013-9-5 设置动画的状态*/
+ (void)setActivityWX:(BOOL)isShow content:(NSString *)content View:(UIView *)myView;
/*wangxia 2013-7-12 设置等待动画的状态*/
+ (void)setActivityWX:(BOOL)isShow content:(NSString *)content nav:(UINavigationController *)nav;

/*wangxia 2013-7-31 进入下一个页面的时候隐藏Tab*/
+ (void)hideTabBar:(UIViewController *)myVC isHiden:(BOOL)isHiden;

/*zj 2013-8-6 弹出提示语*/
+ (MBProgressHUD *)showHudInView:(UIView *)view title:(NSString *)title;
/*zj 2013-8-27 在view底部弹出提示语*/
+ (MBProgressHUD *)showHudInButtonOfView:(UIView *)view title:(NSString *)title closeInSecond:(NSInteger)second;

/*zj 2013-8-20 加载中*/
+ (MBProgressHUD *)showLoadingView:(UIView *)view;

/*zj 2013-8-20 加载中*/
+ (MBProgressHUD *)showLoadingFrame:(CGRect)frame andView:(UIView *)view;

/*wangxia 2016-6-18 按照高度来压缩图片
 oldImage 图片
 maxSize 默认相框的大小
 */
+ (CGSize)getThumbnailsByHeight:(UIImage *)oldImage maxSize:(CGSize)maxSize;

/*wangxia 2016-6-18 按照长度度来压缩图片
 oldImage 图片
 maxWidth 缩放的宽度
 */
+ (CGSize)getThumbnailsByWidth:(UIImage *)oldImage maxSize:(CGSize)maxSize;

/*wangxia 2013-08-08 成比例缩放图片
 oldImage 图片
 maxSize 相框的大小
 */
+ (UIImage *)getThumbnails:(UIImage *)oldImage maxSize:(CGSize)maxSize;


/**wangxia 2014-5-4
 *从一个选项卡下的页面跳转到另一个选项卡主页
 */
+ (void)goToTabItem:(UINavigationController *)nav tabItem:(int)tabItem;

/*添加星星页面*/
+ (UIView *)getStarView :(float)evaluateValue :(float)pointX :(float)pointY;

/*跳转到登录页面*/
+ (void)goToLogin:(UINavigationController *)nav;
@end
