//
//  XYLoadingView.h
//  DianShangApp
//
//  Created by wa on 14-6-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XYLoadingView : UIView
@property(strong,nonatomic)IBOutlet UIActivityIndicatorView *activityIndicatorView;
- (id)initWithType:(NSInteger)type;
@end
