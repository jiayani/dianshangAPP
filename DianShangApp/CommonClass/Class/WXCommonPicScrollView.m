//
//  WXCommonPicScrollView.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-16.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXCommonPicScrollView.h"
#import "CustomPageControl.h"
#import "AsynImageView.h"
#import "ImageEnlarge.h"
#import "WXCommonViewClass.h"
#import "WXCommonSingletonClass.h"

@implementation WXCommonPicScrollView
@synthesize myPageControl;
@synthesize array;
@synthesize nav;
@synthesize picScrollView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self = [[[NSBundle mainBundle] loadNibNamed:@"WXCommonPicScrollView" owner:nil options:nil] objectAtIndex:0];
        if (self) {
            self.frame = frame;
        }
    }
    return self;
}
- (void)initAllControllers:(NSArray *)picArray newNav:(UINavigationController *)newNav{
    nav = newNav;
    if (picArray == nil || picArray.count == 0) {
        picArray = [[NSArray alloc]initWithObjects:@"", nil];
    }
    //绑定图片的 单击事件
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [picScrollView addGestureRecognizer:tapGestureRecognizer];
    array = picArray;
    myCurrentPage = 0;
    pageWidth = self.frame.size.width;
    pageCount = array.count;
    
    [self setScrollView];
    [self setPageControllerMethod];
   
}

-(void)setScrollView{
    
    // 是否按页滚动
    self.picScrollView.pagingEnabled = YES;
    self.picScrollView.backgroundColor = [UIColor clearColor];
    // 隐藏滚动条
    self.picScrollView.showsVerticalScrollIndicator = NO;
    self.picScrollView.showsHorizontalScrollIndicator = NO;
    
    self.picScrollView.canCancelContentTouches = NO;
    
    self.picScrollView.clipsToBounds = YES;
    //设置委托
    self.picScrollView.delegate = self;

    
    CGRect rect = self.picScrollView.frame;
    rect = CGRectMake(0, 0, rect.size.width, rect.size.height);
    
    CGFloat width = rect.size.width;
    
    int i = 0;
    for (NSString *name in array)
    {
        // 创建ImageView
        AsynImageView *imageView = [[AsynImageView alloc] initWithFrame:rect];
        imageView.type = 2;
        imageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
        imageView.imageURL = name;
        // 加入到ScrollView
        [self.picScrollView addSubview:imageView];
        
        // 修改子视图位置
        CGRect frame = imageView.frame;
        frame.origin.x = width*i;
        imageView.frame = frame;
        
        i++;
    }
    // 设置ScrollView大小
    self.picScrollView.contentSize = CGSizeMake(width*[array count],self.picScrollView.frame.size.height);
    
}

- (void)setPageControllerMethod{
    
    self.myPageControl.userInteractionEnabled = YES;
    
    self.myPageControl.backgroundColor = [UIColor clearColor];
    
    [self.myPageControl setNumberOfPages: pageCount];//指定页面个数
    
    [self.myPageControl setCurrentPage: myCurrentPage];//指定pagecontroll的值，默认选中的小白点（第一个）
 
}
#pragma mark - UIScrollViewDelegate Methods
//scrollview的委托方法，当滚动时执行
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    
    CGFloat contentOffsetX = self.picScrollView.contentOffset.x;
    if(contentOffsetX > (pageCount - 1)*pageWidth + pageWidth/5){
        //滑到最后一页时，再次滑动 滑到第一页
         myCurrentPage = 0;
    }else{
         myCurrentPage = contentOffsetX / pageWidth;//通过滚动的偏移量来判断目前页面所对应的小白点
    }
    [self.myPageControl setCurrentPage:myCurrentPage];//pagecontroll响应值的变化
}

#pragma mark -
//点击小图查看大图
-(void)singleTap:(UITapGestureRecognizer*)recognizer{
    
    singletionClass.isNotFistLoad = YES;
    self.nav.view.backgroundColor = [UIColor blackColor];
    AsynImageView *imageView = (AsynImageView *)[picScrollView.subviews objectAtIndex:myPageControl.currentPage ];
    ImageEnlarge *pictureVC = [[ImageEnlarge alloc]init];
    if(imageView.image != nil){
        pictureVC.images = imageView.image;
        
        float height = imageView.image.size.height * 320.0 / imageView.image.size.width;
        pictureVC.imageView.frame = CGRectMake(0,0, 320,height);
        pictureVC.view.backgroundColor = [UIColor blackColor];
//        [pictureVC.view setOpaque:YES];
//        pictureVC.view.opaque = 1.0f;
        pictureVC.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f];
        
        pictureVC.view.transform = CGAffineTransformMakeScale(1, 1);
        
        
        [self.nav pushViewController:pictureVC animated:NO];
        [UIView commitAnimations];
    }else{
        //弹出提示语
        [WXCommonViewClass showHudInView:self title:@"未发现本地图片"];
    }
}
@end
