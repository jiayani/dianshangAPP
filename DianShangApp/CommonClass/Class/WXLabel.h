//
//  WXLabel.h
//  DianShangApp
//
//  Created by 霞 王 on 14-10-9.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum VerticalAlignment
{
    VerticalAlignmentTop = 0,
    VerticalAlignmentMiddle,    //default
    VerticalAlignmentBottom,
    
}VerticalAlignment;

@interface WXLabel : UILabel
@property (nonatomic) VerticalAlignment verticalAlignment;
@end
