//
//  WXCommonDateClass.h
//  TianLvApp
//
//  Created by 霞 王 on 13-7-11.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WXCommonDateClass : NSObject

/*得到系统日期*/
+(NSString *)getSystemDateString:(NSDate *)date;
/*得到系统时间*/
+(NSString *)getSystemTimeString:(NSDate *)date;
/*日期和时间的字符串*/
+(NSString *)getDateAndTimeString:(NSDate *)date;
/*根据字符串格式 输出时间 "yyyy/MM/dd hh:mm:ss" "yyyy-MM-dd hh:mm:ss"*/
+(NSString *)getDateStringOfFormat:(NSString *)format fromDate:(NSDate *)date;
/*根据字符串格式 输出时间 对象*/
+(NSDate *)getDateFromString:(NSString *)string OfFormat:(NSString *)formate;

+(NSString *)getDateString:(NSDate *)date;

+(NSString *)getYYYYMMDDString:(NSDate *)date WithIden:(NSString*)iden;

+(NSDate*)stringToDate:(NSString*)string;

/*wangxia 返回的是 23日 23:22:00
 *剩余时间倒计时
 */
+ (NSString *)getCountdownStr:(NSDate *)endDate;
@end
