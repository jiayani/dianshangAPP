//
//  WXCommonSingletonClass.h
//  DianShangApp
//
//  Created by 霞 王 on 14-5-23.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomNavigationController.h"
@class UserInfo;
@class WeiboApi;

@interface WXCommonSingletonClass : NSObject

/*通讯所需要的变量*/
@property (nonatomic,retain) NSString * serviceURL;   //服务器地址
@property (nonatomic,retain) NSString * shopid;             //商家id
@property (nonatomic,retain) NSString * currentUserId;             //当前用户id
@property (nonatomic,retain) NSString *deviceTokenStr;  //设备token
@property (nonatomic, strong) NSDictionary *addressDic; //地址选择
@property (nonatomic, strong) NSDictionary *sendDic; //配送方式
@property (nonatomic, strong) NSArray *pushNotifiArray;//用来保存推送
@property (nonatomic, weak) NSString *payMent; //支付方式
@property (nonatomic) bool isPayFirst; //配送选择
@property (nonatomic, strong) NSString *getImageServiceRUL;//图片服务器地址
@property (nonatomic, strong) NSString *imageServiceURLOfWooGift;//哇点礼品图片服务器地址
@property (nonatomic, strong) NSString *htmlServiceRUL;//静态网页服务器地址
@property (nonatomic, strong) NSString *shareStr; //判断是分享还是支付
@property (readwrite) BOOL isGifstShopCard;//表识是否是从礼品进入购物车的
@property (nonatomic, strong)  UIColor *fontColorOfPrice; //价格颜色
@property (nonatomic, strong) UIColor *fontColorOfIntegral;//积分颜色
@property (nonatomic, strong) UIColor *colorOfSubscript;//脚标颜色
@property (nonatomic) int shareWhere; //从哪个界面分享
@property (readwrite) BOOL isNotFistLoad; //用来判断是否是团购页面的第一次加载
@property BOOL isThirdPartyLogging;//是否是第三方登录表识 NO:不是第三方 YES：是第三方登录
@property (nonatomic,strong) UserInfo *currentUser;//用来保存当前用户的
@property (nonatomic, strong) WeiboApi *wbapi;
@property (nonatomic) bool isPayBack;//是否从选择支付页面返回
@property (nonatomic) bool isChange; //判断商家是否支持积分互换
@property (nonatomic, strong) NSDictionary *changeDic;//积分哇点兑换比
@property (readwrite) long integralFromRegister;//注册赠送的积分
@property (nonatomic, retain) NSString *version; //版本号
@property (nonatomic, retain) NSString *phoneNumber; //商家电话号码
@property BOOL isRegisterQQ;//是否组测腾讯
@property BOOL isRegisterSina;//是否组测腾讯
@property BOOL isRegisterWeiXin;//是否组测微信
@property (nonatomic,strong) NSString *qqOpenID;//腾讯登录openid
@property (nonatomic,strong) NSDictionary *qqUserInfoDic;//腾讯登录后取得的用户信息
@property (nonatomic,strong) NSString *myLastLoginName;//非三方登录成功后的用户名称
@property  BOOL isSameLoginName;//在修改邮箱/手机/昵称时判断是否和登录/注册是同一种方式
@property  BOOL isCloseFromBack;//判断程序是否彻底的从后台关闭

+ (id)share;
@end
