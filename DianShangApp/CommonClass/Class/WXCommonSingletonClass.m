//
//  WXCommonSingletonClass.m
//  DianShangApp
//
//  Created by 霞 王 on 14-5-23.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXCommonSingletonClass.h"

static WXCommonSingletonClass *singletonClass = nil;

@implementation WXCommonSingletonClass

@synthesize serviceURL;
@synthesize shopid;
@synthesize currentUserId;
@synthesize deviceTokenStr;
@synthesize pushNotifiArray;
@synthesize getImageServiceRUL;
@synthesize imageServiceURLOfWooGift;
@synthesize htmlServiceRUL;
@synthesize isGifstShopCard;
@synthesize fontColorOfPrice; //价格颜色
@synthesize fontColorOfIntegral;//积分颜色
@synthesize colorOfSubscript;//脚标颜色
@synthesize isNotFistLoad;
@synthesize currentUser;
@synthesize integralFromRegister;
@synthesize isThirdPartyLogging;
@synthesize version;
@synthesize isRegisterQQ;
@synthesize isRegisterSina;
@synthesize isRegisterWeiXin;
@synthesize myLastLoginName;
@synthesize qqOpenID;
@synthesize qqUserInfoDic;
@synthesize isSameLoginName;
@synthesize isCloseFromBack;

+ (id)share{
    if (!singletonClass) {
        singletonClass = [[WXCommonSingletonClass alloc]init];
    }
    return singletonClass;
}
@end
