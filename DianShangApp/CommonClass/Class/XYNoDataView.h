//
//  XYNoDataView.h
//  DianShangApp
//
//  Created by wa on 14-6-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XYNoDataView : UIView
@property(strong,nonatomic)IBOutlet UIImageView *imageView;
@property(strong,nonatomic)IBOutlet UILabel *megUpLabel;
@property(strong,nonatomic)IBOutlet UILabel *megDownLabel;
@end
