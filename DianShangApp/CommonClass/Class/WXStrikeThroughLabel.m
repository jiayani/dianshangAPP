//
//  WXStrikeThroughLabel.m
//  DianShangApp
//
//  Created by 霞 王 on 14-6-10.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXStrikeThroughLabel.h"

@implementation WXStrikeThroughLabel

- (void)drawTextInRect:(CGRect)rect{
    
    [super drawTextInRect:rect];
    
    _strikeThroughEnabled = YES;
    CGSize textSize = [[self text] sizeWithFont:[self font]];
    CGFloat strikeWidth = textSize.width;
    CGRect lineRect;
    
    if ([self textAlignment] == NSTextAlignmentRight) {
        lineRect = CGRectMake(rect.size.width - strikeWidth, rect.size.height/2, strikeWidth, 1);
    } else if ([self textAlignment] == NSTextAlignmentCenter) {
        lineRect = CGRectMake(rect.size.width/2 - strikeWidth/2, rect.size.height/2, strikeWidth, 1);
    } else {
        lineRect = CGRectMake(0, rect.size.height/2, strikeWidth, 1);
    }
    
    if (_strikeThroughEnabled) {
        CGContextRef context = UIGraphicsGetCurrentContext();
        //设置中划线颜色
        CGContextSetFillColorWithColor(context, self.textColor.CGColor);

        CGContextFillRect(context, lineRect);
    }
}


@end


