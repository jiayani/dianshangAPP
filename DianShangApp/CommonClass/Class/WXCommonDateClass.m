//
//  WXCommonDateClass.m
//  TianLvApp
//
//  Created by 霞 王 on 13-7-11.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import "WXCommonDateClass.h"

@implementation WXCommonDateClass

/*得到系统日期*/
+(NSString *)getSystemDateString:(NSDate *)date{
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
        NSLocale *locale = [NSLocale currentLocale];
        [dateFormatter setLocale:locale];
    }
    NSString *dateString;
    dateString = [dateFormatter stringFromDate:date];
    return dateString;
}
/*得到系统时间*/
+(NSString *)getSystemTimeString:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterNoStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    NSLocale *locale = [NSLocale currentLocale];
    [dateFormatter setLocale:locale];
    NSString *tempString = [dateFormatter stringFromDate:date];
    return tempString;
}
/*得到年月日 中间以点隔开的*/
+(NSString *)getYYYYMMDDString:(NSDate *)date WithIden:(NSString*)iden{
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:[NSString stringWithFormat:@"yyyy%@MM%@dd",iden,iden]];
        [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
    }
    NSString *dateString;
    dateString = [dateFormatter stringFromDate:date];
    return dateString;
}
//+(NSDate*)string

/*日期和时间的字符串*/
+(NSString *)getDateAndTimeString:(NSDate *)date{
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm"];
        [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
    }
    NSString *dateString;
    dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

/*日期和时间的字符串*/
+(NSString *)getDateString:(NSDate *)date{
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
    }
    NSString *dateString;
    dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

/*字符串日期转date类型*/
+(NSDate*)stringToDate:(NSString*)string
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSDate * date = [formatter dateFromString:string];
    return date;
}
/*根据字符串格式 输出时间 对象*/
+(NSDate *)getDateFromString:(NSString *)string OfFormat:(NSString *)formate{
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:formate];
    }
    NSDate * date = [dateFormatter dateFromString:string];
    return date;

}
/*根据字符串格式 输出时间 "yyyy/MM/dd hh:mm:ss" "yyyy-MM-dd hh:mm:ss" */
+(NSString *)getDateStringOfFormat:(NSString *)format fromDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    return [dateFormatter stringFromDate:date];
}
/*wangxia 返回的是 23日 23:22:00
 *剩余时间倒计时
 */
+ (NSString *)getCountdownStr:(NSDate *)endDate
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *endDat = [[NSDateComponents alloc] init];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:endDate];
    
    static int year;
    static int month;
    static int day;
    static int hour;
    static int minute;
    static int second;
    
    year = [[dateString substringWithRange:NSMakeRange(0, 4)] intValue];
    month = [[dateString substringWithRange:NSMakeRange(5, 2)] intValue];
    day = [[dateString substringWithRange:NSMakeRange(8, 2)] intValue];
    hour = [[dateString substringWithRange:NSMakeRange(11, 2)] intValue];
    minute = [[dateString substringWithRange:NSMakeRange(14, 2)] intValue];
    second = [[dateString substringWithRange:NSMakeRange(17, 2)] intValue];
    
    [endDat setYear:year];
    [endDat setMonth:month];
    [endDat setDay:day];
    [endDat setHour:hour];
    [endDat setMinute:minute];
    [endDat setSecond:second];
    
    NSDate *todate =  [cal dateFromComponents:endDat];
    NSDate *star = [NSDate date];
    NSTimeZone *zone = [NSTimeZone  timeZoneWithName:@"Asia/Shanghai"];
    //    NSTimeZone *zone = [NSTimeZone  systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate:star];
    NSDate *localeDate = [star  dateByAddingTimeInterval: interval];
    
    unsigned int unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    
    NSDateComponents *d = [cal components:unitFlags fromDate:localeDate toDate:todate options:0];
    
    NSString *xiaoshi = [NSString stringWithFormat:@"%d", [d hour]];
    if([d hour] < 10 && [d hour] >=0) {
        xiaoshi = [NSString stringWithFormat:@"0%d",[d hour]];
    }
    
    NSString *fen = [NSString stringWithFormat:@"%d", [d minute]];
    if([d minute] < 10 && [d minute] >=0) {
        fen = [NSString stringWithFormat:@"0%d",[d minute]];
    }
    NSString *miao = [NSString stringWithFormat:@"%d", [d second]];
    if([d second] < 10 && [d second] >=0) {
        miao = [NSString stringWithFormat:@"0%d",[d second]];
    }
    return  [NSString stringWithFormat:@"%d天 %@:%@:%@",[d day], xiaoshi, fen, miao];
}
@end
