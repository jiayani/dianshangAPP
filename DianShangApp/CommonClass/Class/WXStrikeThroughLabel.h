//
//  WXStrikeThroughLabel.h
//  DianShangApp
//  画中划线，颜色跟字体的颜色一致
//  Created by 霞 王 on 14-6-10.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WXStrikeThroughLabel : UILabel{
    BOOL _strikeThroughEnabled;
}

@end
