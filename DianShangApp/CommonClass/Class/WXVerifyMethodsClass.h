//
//  WXVerifyMethodsClass.h
//  TianLvApp
//
//  Created by 霞 王 on 13-7-12.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WXVerifyMethodsClass : NSObject
/*验证邮箱格式*/
+ (BOOL)isValidateEmail:(NSString *)email;

/*验证密码长度在6到16位*/
+ (BOOL)isValidatePwdLength:(NSString *)password1;

/*验证密码必须是数字大小写字母*/
+ (BOOL)isValidatePassword:(NSString *)password2;

/*wangxia 2013-07-31  验证手机号码*/
+ (BOOL)isValidatePhone:(NSString *)phone;

/*验证键盘输入为英文或者汉字*/
+ (BOOL)isValidateWord:(NSString *)phone;

/*验证非负整数（正整数 + 0）*/
+ (BOOL)isValidateNumber:(NSString *)str;
@end
