//
//  WXConfigDataControll.m
//  TianLvApp
//
//  Created by 霞 王 on 13-7-1.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import "WXConfigDataControll.h"


@implementation WXConfigDataControll

/** wangxia 2013-7-1
 *得到配置文件中的所有配置信息
 */
+ (NSDictionary *)getInfoOfConfig{
    
    NSString *name = @"WXConfig";
    NSString *pathStr = [[NSBundle mainBundle]pathForResource:name ofType:@"plist"];
    NSDictionary *dic = [[NSDictionary alloc]initWithContentsOfFile:pathStr];
    return dic;
}



/** wangxia 2013-7-1
 *把从配置文件中读取的信息保存到本地NSUserDefaults中
 *BackgroundColorOfNac: 导航背景颜色
 */
+ (void)saveConfigInfoToDefualt{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //是第一次安装App时需要把所有的配置信息保存到NSUserDefaults中
    if ([[defaults objectForKey:@"IsNotFistInstall"]boolValue] == NO){
        //设置第一次安装App的标示
        [defaults setObject:[NSNumber numberWithBool:YES] forKey:@"IsNotFistInstall"];
        NSDictionary *configDic = [self getInfoOfConfig];
        if(configDic != nil){

            //导航的背景颜色
            NSString *backgroundColorOfNavStr = [configDic objectForKey:@"BackgroundColorOfNav"];
            //设置导航背景颜色
            [defaults setObject:backgroundColorOfNavStr  forKey:@"BackgroundColorOfNav"];
            //设置导航的字体颜色
            [defaults setObject:[configDic objectForKey:@"TitleFontColorOfNav"]  forKey:@"TitleFontColorOfNav"];
            
            
            //设置导航的字体大小
            [defaults setObject:[configDic objectForKey:@"TitleFontSizeOfNav"]  forKey:@"TitleFontSizeOfNav"];

            //设置菜单字体大小
            NSString *fontSizeOfMenuStr =  [configDic objectForKey:@"FontSizeOfMenu"];
            [defaults setObject:fontSizeOfMenuStr  forKey:@"FontSizeOfMenu"];

            //设置菜单字体颜色
            NSString *fontColorOfMenuStr = [configDic objectForKey:@"FontColorOfMenu"];
            [defaults setObject:fontColorOfMenuStr  forKey:@"FontColorOfMenu"];
            
            //设置选中之后的菜单字体颜色
            NSString *fontColorOfSelectedMenuStr = [configDic objectForKey:@"FontColorOfSelectedMenu"];
            [defaults setObject:fontColorOfSelectedMenuStr  forKey:@"FontColorOfSelectedMenu"];
            
            //设置价格字体颜色
            NSString *fontColorOfPrice = [configDic objectForKey:@"FontColorOfPrice"];
            [defaults setObject:fontColorOfPrice  forKey:@"FontColorOfPrice"];
            
            //设置积分字体颜色
            NSString *fontColorOfIntegral = [configDic objectForKey:@"FontColorOfIntegral"];
            [defaults setObject:fontColorOfIntegral forKey:@"FontColorOfIntegral"];
            
            //设置脚标背景颜色
            NSString *colorOfSubscript = [configDic objectForKey:@"ColorOfSubscript"];
            [defaults setObject:colorOfSubscript forKey:@"ColorOfSubscript"];
            
            //设置图片服务器
            NSString *imageServiceRUL = [configDic objectForKey:@"imageServiceRUL"];
            [defaults setObject:imageServiceRUL forKey:@"imageServiceRUL"];
            
            //设置哇点礼品图片服务器
            NSString *imageServiceURLOfWooGift = [configDic objectForKey:@"imageServiceURLOfWooGift"];
            [defaults setObject:imageServiceURLOfWooGift forKey:@"imageServiceURLOfWooGift"];
            
            
            //设置服务端地址
            NSString *serviceURLStr = [configDic objectForKey:@"ServiceURL"];
            [defaults setObject:serviceURLStr  forKey:@"ServiceURL"];
            
            //设置商户的ID
            NSString *shopIdStr = [configDic objectForKey:@"ShopID"];
            [defaults setObject:shopIdStr  forKey:@"ShopID"];
            
            //设置应用分享的模版
            NSString *appShareStr = [configDic objectForKey:@"AppShare"];
            [defaults setObject:appShareStr  forKey:@"AppShare"];
            
            //设置商品分享的模版
            NSString *productShareStr = [configDic objectForKey:@"ProductShare"];
            [defaults setObject:productShareStr  forKey:@"ProductShare"];
            
            //设置促销活动分享的模版
            NSString *promotionsShareStr = [configDic objectForKey:@"PromotionsShare"];
            [defaults setObject:promotionsShareStr  forKey:@"PromotionsShare"];
            
            //设置静态网页服务器地址
            NSString *staticHtmlUrlStr = [configDic objectForKey:@"StaticHtmlUrl"]; 

            [defaults setObject:staticHtmlUrlStr forKey:@"StaticHtmlUrl"];
            
            //设置百度Key
            NSString *baiduMapKeyStr = [configDic objectForKey:@"BaiduMapKey"];
            [defaults setObject:baiduMapKeyStr forKey:@"BaiduMapKey"];
            
            //设置分享key
            NSString *productNoPriceShare = [configDic objectForKey:@"ProductNoPriceShare"];
            [defaults setObject:productNoPriceShare forKey:@"ProductNoPriceShare"];
            [defaults synchronize];
            
            
            
            
            [defaults synchronize];
            
            //设置分享回调URL
            NSString *redirectUrlStr = [configDic objectForKey:@"RedirectUrl"];
            [self setRedirectUrl:redirectUrlStr];
            //设置SinaAppKey
            [self setSinaAppKey:[configDic objectForKey:@"SinaAppKey"]];
            //设置SinaAppSecret
            [self setSinaAppSecret:[configDic objectForKey:@"SinaAppSecret"]];
            //设置QQAppKey
            [self setQQAppKey:[configDic objectForKey:@"QQAppKey"]];
            //设置QQAppSecret
            [self setQQAppSecret:[configDic objectForKey:@"QQAppSecret"]]; 
        }
        
    }
    
}
/** wangxia 2013-7-1
 *得到导航条的背景颜色
 */
+ (UIColor *)getBackgroundColorOfNav{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *colorStr = [defaults stringForKey:@"BackgroundColorOfNav"];
    CGFloat r = 0.0;
    CGFloat g = 109.0;
    CGFloat b = 54.0;
    NSArray *tempArray = [colorStr componentsSeparatedByString:@","];
    if (tempArray.count >= 3) {
        r = [[tempArray objectAtIndex:0]floatValue];
        g = [[tempArray objectAtIndex:1]floatValue];
        b = [[tempArray objectAtIndex:2]floatValue];
    }
    UIColor *color = [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0f];
    return color;
}

/** wangxia 2013-10-25
 **得到导航上title的字体颜色
 */
+ (UIColor *)getTitleFontColorOfNav{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *colorStr = [defaults stringForKey:@"TitleFontColorOfNav"];
    CGFloat r = 0.0;
    CGFloat g = 0.0;
    CGFloat b = 0.0;
    NSArray *tempArray = [colorStr componentsSeparatedByString:@","];
    if (tempArray.count >= 3) {
        r = [[tempArray objectAtIndex:0]floatValue];
        g = [[tempArray objectAtIndex:1]floatValue];
        b = [[tempArray objectAtIndex:2]floatValue];
    }
    UIColor *color = [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0f];
    return color;
}
/** wangxia 2013-11-23
 **得到导航上title字体阴影的颜色
 */
+ (UIColor *)getTitleFontShadowColorOfNav{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *colorStr = [defaults stringForKey:@"TitleFontShadowColorOfNav"];
    CGFloat r = 255.0;
    CGFloat g = 255.0;
    CGFloat b = 255.0;
    NSArray *tempArray = [colorStr componentsSeparatedByString:@","];
    if (tempArray.count >= 3) {
        r = [[tempArray objectAtIndex:0]floatValue];
        g = [[tempArray objectAtIndex:1]floatValue];
        b = [[tempArray objectAtIndex:2]floatValue];
    }
    UIColor *color = [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0f];
    return color;
}
/** wangxia 2013-11-28
 **得到导航上title字体阴影的方向
 */
+ (CGFloat)getDirectionOfShadowColorOfNav{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *direction = [defaults stringForKey:@"directionOfShadowColorOfNav"];
    return [direction floatValue];
}


/**wangxia 2013-10-25
 *TitleFontSizeOfNav:得到导航的字体大小
 */
+ (CGFloat)getTitleFontSizeOfNav{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *titleFontSizeOfNavStr = [defaults stringForKey:@"TitleFontSizeOfNav"];;
    if(titleFontSizeOfNavStr == nil){
        return 19.0f;
    }else{
        return [titleFontSizeOfNavStr floatValue];
    }
    
}

/**wangxia 2013-7-1
 *FontSizeOfMenu:得到菜单字体大小
 */
+ (NSString *)getFontSizeOfMenu{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *FontSizeOfMenuStr = [defaults stringForKey:@"FontSizeOfMenu"];;
    if(FontSizeOfMenuStr == nil){
        return @"14.0";
    }else{
        return FontSizeOfMenuStr;
    }

}
/**wangxia 2013-9-12
 *getFontColorOfMenuArray:得到菜单字体颜色数组
 */
+ (NSArray *)getFontColorOfMenuArray{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *colorStr = [defaults stringForKey:@"FontColorOfMenu"];
    NSArray *tempArray = [colorStr componentsSeparatedByString:@","];
    if (tempArray.count >= 3) {
        return tempArray;
    }else{
        NSNumber *tempNum = [NSNumber numberWithFloat:0.0];
        tempArray = [[NSArray alloc]initWithObjects:tempNum,tempNum,tempNum, nil];
    }
    return tempArray;
}
/**wangxia 2013-7-1
 *FontColorOfMenu:得到菜单字体颜色
 */
+ (UIColor *)getFontColorOfMenu{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *colorStr = [defaults stringForKey:@"FontColorOfMenu"];
    CGFloat r = 103;
    CGFloat g = 103;
    CGFloat b = 103;
    NSArray *tempArray = [colorStr componentsSeparatedByString:@","];
    if (tempArray.count >= 3) {
        r = [[tempArray objectAtIndex:0]floatValue];
        g = [[tempArray objectAtIndex:1]floatValue];
        b = [[tempArray objectAtIndex:2]floatValue];
    }
    UIColor *color = [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0f];
    return color;
}
/**wangxia 2013-10-28
 *FontColorOfSelectedMenu:得到选中后的菜单字体颜色
 */
+ (UIColor *)getFontColorOfSelectedMenu{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *colorStr = [defaults stringForKey:@"FontColorOfSelectedMenu"];
    CGFloat r = 255;
    CGFloat g = 51;
    CGFloat b = 51;
    NSArray *tempArray = [colorStr componentsSeparatedByString:@","];
    if (tempArray.count >= 3) {
        r = [[tempArray objectAtIndex:0]floatValue];
        g = [[tempArray objectAtIndex:1]floatValue];
        b = [[tempArray objectAtIndex:2]floatValue];
    }
    UIColor *color = [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0f];
    return color;
}
/**wangxia 2014-5-14
 *FontColorOfPrice:价格字体颜色
 */
+ (UIColor *)getFontColorOfPrice{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if (singletonClass.fontColorOfPrice == nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *colorStr = [defaults stringForKey:@"FontColorOfPrice"];
        CGFloat r = 255;
        CGFloat g = 30;
        CGFloat b = 30;
        NSArray *tempArray = [colorStr componentsSeparatedByString:@","];
        if (tempArray.count >= 3) {
            r = [[tempArray objectAtIndex:0]floatValue];
            g = [[tempArray objectAtIndex:1]floatValue];
            b = [[tempArray objectAtIndex:2]floatValue];
        }
        UIColor *color = [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0f];
        singletonClass.fontColorOfPrice = color;
        return color;
    }else{
        return singletonClass.fontColorOfPrice;
    }
    
}
/**wangxia 2014-5-14
 *FontColorOfIntegral:积分字体颜色
 */
+ (UIColor *)getFontColorOfIntegral{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if (singletonClass.fontColorOfIntegral == nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *colorStr = [defaults stringForKey:@"FontColorOfIntegral"];
        CGFloat r = 255;
        CGFloat g = 30;
        CGFloat b = 30;
        NSArray *tempArray = [colorStr componentsSeparatedByString:@","];
        if (tempArray.count >= 3) {
            r = [[tempArray objectAtIndex:0]floatValue];
            g = [[tempArray objectAtIndex:1]floatValue];
            b = [[tempArray objectAtIndex:2]floatValue];
        }
        UIColor *color = [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0f];
        singletonClass.fontColorOfIntegral = color;
        return color;
    }else{
        return singletonClass.fontColorOfIntegral;
    }
    
    
}
/**wangxia 2014-5-14
 *ColorOfSubscript:脚标颜色
 */
+ (UIColor *)getColorOfSubscript{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if (singletonClass.colorOfSubscript == nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *colorStr = [defaults stringForKey:@"ColorOfSubscript"];
        CGFloat r = 255;
        CGFloat g = 30;
        CGFloat b = 30;
        NSArray *tempArray = [colorStr componentsSeparatedByString:@","];
        if (tempArray.count >= 3) {
            r = [[tempArray objectAtIndex:0]floatValue];
            g = [[tempArray objectAtIndex:1]floatValue];
            b = [[tempArray objectAtIndex:2]floatValue];
        }
        UIColor *color = [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0f];
        singletonClass.colorOfSubscript = color;
        return color;
    }else{
        return singletonClass.colorOfSubscript;
    }
    
}

/**wangxia 2013-7-24
 *ServiceURL:服务器地址
 */
+ (NSString *)getServiceURLStr{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *serviceURLStr = [defaults stringForKey:@"ServiceURL"];
    return serviceURLStr;
}
/**wangxia
 *ServiceURL:图片服务服务器地址
 */
+ (NSString *)getImageServiceRUL{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *imageServiceRUL = [defaults stringForKey:@"imageServiceRUL"];
    return imageServiceRUL;
}
/**wangxia
 *imageServiceURLOfWooGift:哇点礼品图片服务服务器地址
 */
+ (NSString *)getImageServiceURLOfWooGift{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *imageServiceURLOfWooGift = [defaults stringForKey:@"imageServiceURLOfWooGift"];
    return imageServiceURLOfWooGift;
}

/**wangxia 2013-7-24
 *ShopID:得到商户的ID
 */
+ (NSString *)getShopID{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *shopid = [defaults stringForKey:@"ShopID"];
    NSString *shopIdStr = [NSString stringWithFormat:@"%@&version=%@", shopid, [self getCuurentVersion]];
    return shopIdStr;
}

/**wangxia 2013-7-24
 *AppShare:得到应用分享的模版
 */
+ (NSString *)getAppShare{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *appShareStr = [defaults stringForKey:@"AppShare"];
//    NSString *serverHttp = [self getServiceURLStr];
    NSString *serverHttp = @"http://apps.kstapp.com/";
    NSString *shopId = [self getShopID];
    NSString *donwloadStr = [NSString stringWithFormat:@"%@downloads/%@.html",serverHttp,shopId];
    if (appShareStr.length > 0) {
        appShareStr = [appShareStr stringByReplacingOccurrencesOfString:@"http://XXXX" withString:donwloadStr];
    }
    return appShareStr;
}
/**wangxia 2013-7-24
 *ProductShare:得到商品分享的模版
 */
+ (NSString *)getproductShare{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *productShareStr = [defaults stringForKey:@"ProductShare"];
//    NSString *serverHttp = [self getServiceURLStr];
    NSString *serverHttp = @"http://apps.kstapp.com/";
    NSString *shopId = [self getShopID];
    NSString *donwloadStr = [NSString stringWithFormat:@"%@downloads/%@.html",serverHttp,shopId];
    if (productShareStr.length > 0) {
        productShareStr = [productShareStr stringByReplacingOccurrencesOfString:@"http://XXXX" withString:donwloadStr];
    }
    return productShareStr;
}

/**
 zj 2013-11-20
 @returns 得到商品(不带价格)分享的模板
 */
+ (NSString *)getProductNoPriceShare{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *productShareStr = [defaults stringForKey:@"ProductNoPriceShare"];
    //    NSString *serverHttp = [self getServiceURLStr];
    NSString *serverHttp = @"http://apps.kstapp.com/";
    NSString *shopId = [self getShopID];
    NSString *donwloadStr = [NSString stringWithFormat:@"%@downloads/%@.html",serverHttp,shopId];
    if (productShareStr.length > 0) {
        productShareStr = [productShareStr stringByReplacingOccurrencesOfString:@"http://XXXX" withString:donwloadStr];
    }
    return productShareStr;
}

/**wangxia 2013-7-24
 *PromotionsShare:得到优惠活动分享的模版
 */
+ (NSString *)getPromotionsShare{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *promotionsShareStr = [defaults stringForKey:@"PromotionsShare"];
//    NSString *serverHttp = [self getServiceURLStr];
    NSString *serverHttp = @"http://apps.kstapp.com/";
    NSString *shopId = [self getShopID];
    NSString *donwloadStr = [NSString stringWithFormat:@"%@downloads/%@.html",serverHttp,shopId];
    if (promotionsShareStr.length > 0) {
        promotionsShareStr = [promotionsShareStr stringByReplacingOccurrencesOfString:@"http://XXXX" withString:donwloadStr];
    }
    return promotionsShareStr;
}

/** zj 2013-8-1
 *AppName 得到应用名称
 */
+ (NSString *)getAppName{
    NSDictionary* infoDict =[[NSBundle mainBundle] infoDictionary];
    NSString *appName =[infoDict objectForKey:@"CFBundleDisplayName"];
    return appName;
}

/** wangxia 2013-9-28
 *得到当前程序的版本号
 */
+ (NSString *) getCuurentVersion{
    NSString *versionStr = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    return versionStr;
}

/** wangxia 2013-9-30
 *DeviceToken  设置旧的设备口令牌
 */
+ (void)setDeviceToken:(NSString *)deviceToken{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:deviceToken forKey:@"DeviceToken"];
    [defaults synchronize];
}

/** wangxia 2013-9-30
 *DeviceToken  得到旧的设备口令牌
 */
+ (NSString *)getDeviceToken{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *deviceToken =[defaults stringForKey:@"DeviceToken"];
    return deviceToken;
}

/** wangxia 2013-9-3
 *PhoneNumber 得到客服电话
 */
+ (NSString *)getPhoneNumber{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *phoneStr = [defaults objectForKey:@"PhoneNumber"];
    if (phoneStr == nil) {
        phoneStr = @"13612165431";
    }
    return phoneStr;
}
/** wangxia 2013-9-3
 *PhoneNumber 设置得到客服电话
 */
+ (void)setPhoneNumber:(NSString *)phoneStr{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:phoneStr forKey:@"PhoneNumber"];
    [defaults synchronize];
}

/**wangxia 2013-9-12
 *StaticHtmlUrl:得到静态页面访问服务器地址
 */
+ (NSString *)getStaticHtmlUrlStr{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if (singletonClass.htmlServiceRUL == nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *serviceURLStr = [defaults stringForKey:@"StaticHtmlUrl"];
        singletonClass.htmlServiceRUL = serviceURLStr;
        return serviceURLStr;
    }else{
        return singletonClass.htmlServiceRUL;
    }
    
    
}
/**wangxia 2013-9-27
 *BaiduMapKey:设置百度的key
 */
+ (void)setBaiduMapKey:(NSString *)baiduMapKeyStr{
    if (baiduMapKeyStr != nil && baiduMapKeyStr.length > 0) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:baiduMapKeyStr forKey:@"BaiduMapKey"];
        [defaults synchronize];
    }
    
}

/**wangxia 2013-9-27
 *BaiduMapKey:得到百度的key
 */
+ (NSString *)getBaiduMapKey{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *baiduMapKeyStr = [defaults stringForKey:@"BaiduMapKey"];
    return baiduMapKeyStr;
}
/**wangxia 2013-9-29
 *RedirectUrl :setting
 */
+ (void) setRedirectUrl:(NSString *)redirectUrlStr{
    if (redirectUrlStr != nil && redirectUrlStr.length > 0) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:redirectUrlStr forKey:@"RedirectUrl"];
        [defaults synchronize];
    }
    
}
/**wangxia 2013-9-27
 *RedirectUrl:请正确填写您的应用网址，否则将导致授权失败
 */
+ (NSString *)getRedirectUrl{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *redirectUrlStr = [defaults stringForKey:@"RedirectUrl"];
    return redirectUrlStr;
}

/**wangxia 2013-9-29
 *set SinaAppKey
 */
+ (void) setSinaAppKey:(NSString *)sinaAppKeyStr{
    if (sinaAppKeyStr != nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:sinaAppKeyStr forKey:@"SinaAppKey"];
        [defaults synchronize];
    }
    
}
/**wangxia 2013-9-29
 *get SinaAppKey
 */
+ (NSString *) getSinaAppKey{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *sinaAppKeyStr = [defaults stringForKey:@"SinaAppKey"];
    return sinaAppKeyStr;
}
/**wangxia 2013-9-29
 *set SinaAppSecret
 */
+ (void) setSinaAppSecret:(NSString *)sinaAppSecretStr{
    if (sinaAppSecretStr != nil && sinaAppSecretStr.length > 0) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:sinaAppSecretStr forKey:@"SinaAppSecret"];
        [defaults synchronize];
    }
    
}
/**wangxia 2013-9-29
 *get SinaAppSecret
 */
+ (NSString *) getSinaAppSecret{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *sinaAppSecretStr = [defaults stringForKey:@"SinaAppSecret"];
    return sinaAppSecretStr;
}

/**wangxia 2013-9-29
 *set QQAppKey
 */
+ (void) setQQAppKey:(NSString *)qqAppKeyStr{
    if (qqAppKeyStr != nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:qqAppKeyStr forKey:@"QQAppKey"];
        [defaults synchronize];
    }
    
}
/**wangxia 2013-9-29
 *get QQAppKey
 */
+ (NSString *) getQQAppKey{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *qqAppKeyStr = [defaults stringForKey:@"QQAppKey"];
    return qqAppKeyStr;
}
/**wangxia 2013-9-29
 *set QQAppSecret
 */
+ (void) setQQAppSecret:(NSString *)qqAppSecretStr{
    if (qqAppSecretStr != nil && qqAppSecretStr.length >  0) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:qqAppSecretStr forKey:@"QQAppSecret"];
        [defaults synchronize];
    }
   
}
/**wangxia 2013-9-29
 *get QQAppSecret
 */
+ (NSString *) getQQAppSecret{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *qqAppSecretStr = [defaults stringForKey:@"QQAppSecret"];
    return qqAppSecretStr;
}


+ (void) setUMengiPhoneSecret:(NSString *)uMengiPhoneSecretStr
{
    if (uMengiPhoneSecretStr != nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:uMengiPhoneSecretStr forKey:@"UMengiPhoneSecret"];
        [defaults synchronize];
    }
}

+ (void) setUMengChannelId:(NSString *)uMengiPhoneSecretStr
{
    if (uMengiPhoneSecretStr != nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:uMengiPhoneSecretStr forKey:@"UMengChannelId"];
        [defaults synchronize];
    }
}

+ (NSString *)getUMengiPhoneSecret
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *uMengiPhoneSecretStr = [defaults stringForKey:@"UMengiPhoneSecret"];
    return uMengiPhoneSecretStr;
}

+ (NSString *)getUMengChannelId
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *uMengiPhoneSecretStr = [defaults stringForKey:@"UMengChannelId"];
    return uMengiPhoneSecretStr;
}


/* wyd...2014-02-10
 * set can_comepay ---------- 是否支持到店支付
 */
+(void)setCan_comepay:(NSString*)yesOrNo
{
    int i = [yesOrNo intValue];
    if (i==0 || i==1) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:yesOrNo forKey:@"can_comepay"];
        [defaults synchronize];
    }
}

/* wyd...2014-02-10
 get can_comepay
 */
+(NSString*)getCan_comepay
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *can_comepay = [defaults stringForKey:@"can_comepay"];
    return can_comepay;
}

/* wyd...2014-02-10
 * set can_embed ---------- 是否支持支付宝支付
 */
+(void)setCan_embed:(NSString*)yesOrNo
{
    int i = [yesOrNo intValue];
    if (i==0 || i==1) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:yesOrNo forKey:@"can_embed"];
        [defaults synchronize];
    }
}

/* wyd...2014-02-10
 get can_embed
 */
+(NSString*)getCan_embed
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *can_embed = [defaults stringForKey:@"can_embed"];
    return can_embed;
}

/* wyd...2014-02-10
 * set can_wappay ---------- 是否支持网页支付
 */
+(void)setCan_wappay:(NSString*)yesOrNo
{
    int i = [yesOrNo intValue];
    if (i==0 || i==1) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:yesOrNo forKey:@"can_wappay"];
        [defaults synchronize];
    }
}

/* wyd...2014-02-10
 get can_wappay
 */
+(NSString*)getCan_wappay
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *can_wappay = [defaults stringForKey:@"can_wappay"];
    return can_wappay;
}

/* wyd...2014-02-10
 * set can_return ---------- 是否支持退款
 */
+(void)setCan_return:(NSString*)yesOrNo
{
    int i = [yesOrNo intValue];
    if (i==0 || i==1) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:yesOrNo forKey:@"can_return"];
        [defaults synchronize];
        
    }
}

/* wyd...2014-02-10
 get can_return
 */
+(NSString*)getCan_return
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *can_return = [defaults stringForKey:@"can_return"];
    return can_return;
}

/* rong...2014-03-18
 * set Can_delivery ---------- 是否支持货到付款
 */
+(void)setCan_delivery:(NSString*)yesOrNo
{
    int i = [yesOrNo intValue];
    if (i==0 || i==1) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:yesOrNo forKey:@"can_pay_on_delivery"];
        [defaults synchronize];
    }
}

/* rong...2014-03-18
 get Can_delivery
 */
+(NSString*)getCan_delivery
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *can_wappay = [defaults stringForKey:@"can_pay_on_delivery"];
    return can_wappay;
}

/* rong...2014-03-18
 * set Can_shopDelivery ---------- 是否支持商家配送
 */
+(void)setCan_shopDelivery:(NSString*)yesOrNo
{
    int i = [yesOrNo intValue];
    if (i==0 || i==1) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:yesOrNo forKey:@"support_shop_delivery"];
        [defaults synchronize];
    }
}

/* rong...2014-03-18
 get Can_shopDelivery
 */
+(NSString*)getCan_shopDelivery
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *can_wappay = [defaults stringForKey:@"support_shop_delivery"];
    return can_wappay;
}

/**wangxia 2014-8-18
 *set WeiXinAppKey
 */
+ (void)setWeiXinAppKey:(NSString *)appKeyStr{
    if (appKeyStr != nil ) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:appKeyStr forKey:@"WeiXinAppKey"];
        [defaults synchronize];
    }
    
}
/**wangxia 2014-8-18
 *get WeiXinAppKey
 */
+ (NSString *)getWeiXinAppKey{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *appKeyStr = [defaults stringForKey:@"WeiXinAppKey"];
    return appKeyStr;
}

//付岩 2014-9-11
//设置QQ登录AppID
+ (void)setQQLoginID:(NSString *)appID
{
    if (appID != nil ) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:appID forKey:@"QQLoginID"];
        [defaults synchronize];
    }
}

//获取QQ登录APPID
+ (NSString *)getQQLoginID
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *appID = [defaults stringForKey:@"QQLoginID"];
    return appID;
}
@end
