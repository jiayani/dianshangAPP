//
//  WXCommonViewClass.m
//  TianLvApp
//
//  Created by 霞 王 on 13-7-3.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import "WXCommonViewClass.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "WXTabBarViewController.h"
#import "WXUserLoginViewController.h"
@implementation WXCommonViewClass

/**wangxia 2013-7-3
 *得到导航条上的返回按钮
 */
+ (UIButton*)getBackButton{
  
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setTitle:@"返回" forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 0, 70, 44);
    backButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [backButton setTitleColor:[AppDelegate delegate].titleFontColorOfNav forState:UIControlStateNormal];
     UIColor *color = [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1.0f];
    [backButton setTitleColor:color forState:UIControlStateHighlighted];
    [backButton setBackgroundImage:[UIImage imageNamed:@"backImage.png"] forState:UIControlStateNormal];
    backButton.imageEdgeInsets = UIEdgeInsetsMake(0, -13, 0, 0);
    backButton.titleLabel.textAlignment = NSTextAlignmentLeft;
    UIEdgeInsets edgetInsets = UIEdgeInsetsMake(0, 13, 0, 0);
    [backButton setTitleEdgeInsets:edgetInsets];
    
    return backButton;
    
}

/*WYD 2013-7-8 导航右侧按钮*/
+(UIButton*)getRightBtnOfNav:(NSString *)imageName hightlightedImageName:(NSString *)hightlightedImageName  title:(NSString *)titleStr
{
    UIButton * rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    if (titleStr.length > 3) {
        rightBtn.frame = CGRectMake(0, 0, 18.0*(titleStr.length), 29.0);
    }else{
        rightBtn.frame = CGRectMake(0, 0, 49.0, 29.0);
    }
    [rightBtn setTitleColor:[AppDelegate delegate].titleFontColorOfNav forState:UIControlStateNormal];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    UIColor *color = [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1.0f];
    
    [rightBtn setTitleColor:color forState:UIControlStateHighlighted];
    if (imageName != nil) {
        [rightBtn setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [rightBtn setBackgroundImage:[UIImage imageNamed:hightlightedImageName] forState:UIControlStateHighlighted];
    }
    if (titleStr != nil) {
        [rightBtn setTitle:titleStr forState:UIControlStateNormal];
    }
    return rightBtn;
}
/*获取导航左侧返回按钮*/
+(UIButton*)getLeftBtnOfNav:(NSString *)imageName title:(NSString *)titleStr frame:(CGRect)rect{
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = rect;
    if (imageName != nil) {
        [btn setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    if (titleStr != nil) {
        [btn setTitle:titleStr forState:UIControlStateNormal];
    }
    return btn;
}
/*wangxia 2013-9-5 设置动画的状态*/
+ (void)setActivityWX:(BOOL)isShow content:(NSString *)content View:(UIView *)myView{
    if (!isShow) {
        //小圈停止转动
        [MBProgressHUD hideHUDForView:myView animated:YES];
    }else {
        //小圈转动
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:myView animated:YES];
        hud.labelText = content ;
    }
}
/*wangxia 2013-7-12 设置动画的状态*/
+ (void)setActivityWX:(BOOL)isShow content:(NSString *)content nav:(CustomNavigationController *)nav{
    if (!isShow) {
        //小圈停止转动
        [MBProgressHUD hideHUDForView:nav.view animated:YES];
    }else {
        //小圈转动
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:nav.view animated:YES];
        hud.labelText = content ;
    }
}
/*wangxia 2013-7-31 进入下一个页面的时候隐藏Tab*/
+ (void)hideTabBar:(UIViewController *)myVC isHiden:(BOOL)isHiden
{
//    AppDelegate* appDelegate = [AppDelegate delegate];

    if (isHiden) {
//        static NSInteger dir = 0;
//                dir++;
//        appDelegate.wxTabBarController.animateDriect = dir % 2;
        myVC.hidesBottomBarWhenPushed = YES;
    }else{
        myVC.hidesBottomBarWhenPushed = NO;

    }
    

}
/*zj 2013-8-6 弹出提示语*/
+ (MBProgressHUD *)showHudInView:(UIView *)view title:(NSString *)title{
    MBProgressHUD *mBProgressHUD = [[MBProgressHUD alloc]initWithView:view];
    [mBProgressHUD setLabelFont:[UIFont systemFontOfSize:13]];
    [mBProgressHUD setLabelText:title];
    //设置纯文字显示效果
    mBProgressHUD.mode = MBProgressHUDModeText;
    //指定距离中心点的X轴和Y轴的偏移量，如果不指定则在屏幕中间显示
//    mBProgressHUD.yOffset = 120.0f;
//    mBProgressHUD.xOffset = 90.0f;
    [view addSubview:mBProgressHUD];
    
    [mBProgressHUD show:YES];
    [mBProgressHUD hide:YES afterDelay:3];
    return mBProgressHUD;
}

/*zj 2013-8-27 在view底部弹出提示语*/
+ (MBProgressHUD *)showHudInButtonOfView:(UIView *)view title:(NSString *)title closeInSecond:(NSInteger)second{
    MBProgressHUD *mBProgressHUD = [[MBProgressHUD alloc]initWithView:view];
    //设置纯文字显示效果
    mBProgressHUD.mode = MBProgressHUDModeText;
    //指定距离中心点的X轴和Y轴的偏移量，如果不指定则在屏幕中间显示
//    mBProgressHUD.yOffset =    120.0f;
    mBProgressHUD.yOffset = view.frame.size.height/2 - 100.0f;
    [view addSubview:mBProgressHUD];
    [mBProgressHUD setLabelText:title];
    [mBProgressHUD setLabelFont:[UIFont systemFontOfSize:13]];
    [mBProgressHUD show:YES];
    [mBProgressHUD hide:YES afterDelay:second];
    return mBProgressHUD;
}
/*wangxia 2016-6-18 按照高度来压缩图片
 oldImage 图片
 maxSize 默认相框的大小
 */
+ (CGSize)getThumbnailsByHeight:(UIImage *)oldImage maxSize:(CGSize)maxSize{
    CGFloat maxHeight = maxSize.height;
    CGSize size = oldImage.size;
    CGSize newSize;
    CGFloat scale;
    CGFloat tempWidth;
    if (size.height < maxHeight)
    {
        //放大图片
        scale = maxHeight / size.height;
        tempWidth = size.width * scale;
        if (tempWidth > maxSize.width) {
            tempWidth = maxSize.width;
        }
        newSize = CGSizeMake(tempWidth, size.height * scale);

    }else{
        //压缩图片
        scale = size.height / maxHeight;
        tempWidth = size.width / scale;
        if (tempWidth > maxSize.width) {
            tempWidth = maxSize.width;
        }
        newSize = CGSizeMake(tempWidth, size.height/scale);

    }
    
    return newSize;
    
}
/*wangxia 2016-6-18 按照长度度来压缩图片
 oldImage 图片
 maxWidth 缩放的宽度
 */
+ (CGSize)getThumbnailsByWidth:(UIImage *)oldImage maxSize:(CGSize)maxSize{
    CGFloat maxWidth = maxSize.width;
    CGSize size = oldImage.size;
    CGSize newSize;
    CGFloat scale,tempHeight;
    if (size.width < maxWidth)
    {
        //放大图片
        scale = maxWidth / size.width;
        tempHeight = size.height * scale;
        if (tempHeight > maxSize.height) {
            tempHeight = maxSize.height;
        }
        newSize = CGSizeMake(size.width * scale, tempHeight);
        
    }else{
        //压缩图片
        scale = size.width / maxWidth;
        tempHeight = size.height / scale;
        if (tempHeight > maxSize.height) {
            tempHeight = maxSize.height;
        }
        newSize = CGSizeMake(size.width / scale, tempHeight);
    }
   
    return newSize;
    
}
/*wangxia 2013-08-08 成比例缩放图片
 oldImage 图片
 maxSize 相框的大小
 */
+ (CGSize)getSizeOfThumbnails:(UIImage *)oldImage maxSize:(CGSize)maxSize{
    
    CGSize size = oldImage.size;
    CGSize newSize;
    if (size.width < maxSize.width && size.height < maxSize.height)
    {
        //放大图片
        newSize = [self fitsizeFangDa:size maxSize:maxSize];
        
    }else{
        //压缩图片
        newSize = [self fitsize:size maxSize:maxSize];
    }
    /*
    //begin：缩略图
    UIGraphicsBeginImageContext(newSize);
    [oldImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //end：缩略图
     */
    return newSize;
    
}

/*wangxia 2013-08-08 成比例缩放图片
 oldImage 图片
 maxSize 相框的大小
 */
+ (UIImage *)getThumbnails:(UIImage *)oldImage maxSize:(CGSize)maxSize{
    
    CGSize size = oldImage.size;
    CGSize newSize;
    if (size.width < maxSize.width && size.height < maxSize.height)
    {
        //放大图片
        newSize = [self fitsizeFangDa:size maxSize:maxSize];
        
    }else{
        //压缩图片
        newSize = [self fitsize:size maxSize:maxSize];
    }
    //begin：缩略图
    UIGraphicsBeginImageContext(newSize);
    [oldImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //end：缩略图
    
//    NSData *newData = UIImagePNGRepresentation(newImage);
//    [newData writeToFile:@"/Users/wangxia/Desktop/7.png" atomically:NO];
    return newImage;
    
}
/*放大图片大小*/
+ (CGSize)fitsizeFangDa:(CGSize)thisSize maxSize:(CGSize)maxSize
{
    if(thisSize.width == 0 && thisSize.height == 0){
        return CGSizeMake(0, 0);
    }
    CGFloat wscale = maxSize.width / thisSize.width ;
    CGFloat hscale = maxSize.height / thisSize.height;
    CGFloat scale = ( wscale < hscale ) ? wscale : hscale;
    CGSize newSize = CGSizeMake(thisSize.width * scale, thisSize.height * scale);
    return newSize;
}
/*压缩图片大小*/
+ (CGSize)fitsize:(CGSize)thisSize maxSize:(CGSize)maxSize
{
    if(thisSize.width == 0 && thisSize.height == 0){
        return CGSizeMake(0, 0);
    }
    CGFloat wscale = thisSize.width / maxSize.width;
    CGFloat hscale = thisSize.height / maxSize.height;
    CGFloat scale = ( wscale > hscale ) ? wscale : hscale;
    CGSize newSize = CGSizeMake(thisSize.width/scale, thisSize.height/scale);
    return newSize;
}

/*zj 2013-8-20 加载中*/
+ (MBProgressHUD *)showLoadingView:(UIView *)view{
    MBProgressHUD *mBProgressHUD = [[MBProgressHUD alloc] initWithView:view];
    [view addSubview:mBProgressHUD];
    [view bringSubviewToFront:mBProgressHUD];
    mBProgressHUD.labelText = @"加载中...";
    [mBProgressHUD show:YES];
    return mBProgressHUD;
}
/*zj 2013-8-20 加载中*/
+ (MBProgressHUD *)showLoadingFrame:(CGRect)frame andView:(UIView *)view{
    MBProgressHUD *mBProgressHUD = [[MBProgressHUD alloc] initWithFrame:frame];
  //  [view addSubview:mBProgressHUD];
    [view bringSubviewToFront:mBProgressHUD];
    mBProgressHUD.labelText = @"加载中...";
    mBProgressHUD.alpha = 0;
    [mBProgressHUD show:YES];
    return mBProgressHUD;
}

/**wangxia 2014-5-4
 *从一个选项卡下的页面跳转到另一个选项卡主页
 */
+ (void)goToTabItem:(UINavigationController *)nav tabItem:(int)tabItem{
    if (nav != nil) {
        [nav popToRootViewControllerAnimated:NO];
    }
    WXTabBarViewController *wxBarController = [[AppDelegate delegate] getWXBarViewController];
    int myCount =  wxBarController.tabBar.subviews.count;
    if (tabItem <= 0) {
        tabItem = 0;
    }else if (tabItem >= myCount) {
        tabItem = myCount;
    }
    UIButton *tempBtn = [wxBarController.tabBar.subviews objectAtIndex:tabItem];
    [wxBarController.tabBar tabBarButtonClicked:tempBtn];
    
}

/*添加星星页面*/
+ (UIView *)getStarView :(float)evaluateValue :(float)pointX :(float)pointY
{
    UIView * mainView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 68, 12)];
    UIView * fullStarView = [[UIView alloc]initWithFrame:CGRectMake(pointX, pointY, evaluateValue*11.8 + 2*(int)evaluateValue, 12)];
    [fullStarView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"star_50_1@.png"]]];
    UIImageView * backgroundStarImg = [[UIImageView alloc]init];
    [backgroundStarImg setFrame:CGRectMake(pointX, pointY, 68, 12)];
    [backgroundStarImg setImage:[UIImage imageNamed:@"star_0.png"]];
    [mainView addSubview:fullStarView];
    [mainView addSubview:backgroundStarImg];
    return mainView;
}
/*跳转到登录页面*/
+ (void)goToLogin:(UINavigationController *)nav{
    WXUserLoginViewController *userLoginViewController  = [[WXUserLoginViewController alloc]initWithNibName:@"WXUserLoginViewController" bundle:nil];
    [nav pushViewController:userLoginViewController animated:YES];
}


@end
