//
//  WXVerifyMethodsClass.m
//  TianLvApp
//
//  Created by 霞 王 on 13-7-12.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import "WXVerifyMethodsClass.h"

@implementation WXVerifyMethodsClass
/*验证邮箱格式*/
+ (BOOL)isValidateEmail:(NSString *)email {
    NSString *emailRegex = @"^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

/*验证密码长度在6到16位*/
+ (BOOL)isValidatePwdLength:(NSString *)password {
    if (password.length < 6 || password.length > 16) {
        return NO;
    }else{
        return YES;
    }
}

/*验证密码必须是数字大小写字母*/
+ (BOOL)isValidatePassword:(NSString *)password {
    NSString *passwordRegex = @"^[a-zA-Z\\d]+$";
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passwordRegex];
    return [passwordTest evaluateWithObject:password];
}
/*验证非负整数（正整数 + 0）*/
+ (BOOL)isValidateNumber:(NSString *)str{
    NSString *passwordRegex = @"^\\d+$";
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passwordRegex];
    return [passwordTest evaluateWithObject:str];
}
/*wangxia 2013-07-31  验证手机号码*/
+ (BOOL)isValidatePhone:(NSString *)phone {
    NSString *phoneRegex = @"^(13|14|15|18)\\d{9}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:phone];
}
/*验证键盘输入为英文或者汉字*/
+ (BOOL)isValidateWord:(NSString *)input{
    NSString *cWord = @"[a-zA-Z\u4e00-\u9fa5]+";
    NSPredicate *cWordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cWord];
    if([cWordTest evaluateWithObject:input]){
        return YES;
    }
    return NO;
}
@end
