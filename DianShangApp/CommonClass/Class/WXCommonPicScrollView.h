//
//  WXCommonPicScrollView.h
//  DianShangApp
//
//  Created by 霞 王 on 14-6-16.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CustomPageControl;
@class WXCommonSingletonClass;

@interface WXCommonPicScrollView : UIView<UIScrollViewDelegate>{
    int pageCount;
    int myCurrentPage;
    int pageWidth;
    WXCommonSingletonClass *singletionClass;
}

@property (strong, nonatomic) NSArray *array;
@property (strong, nonatomic) IBOutlet CustomPageControl *myPageControl;
@property (strong, nonatomic) IBOutlet UIScrollView *picScrollView;
@property (strong, nonatomic) UINavigationController *nav;

- (void)initAllControllers:(NSArray *)picArray newNav:(UINavigationController *)newNav;
@end
