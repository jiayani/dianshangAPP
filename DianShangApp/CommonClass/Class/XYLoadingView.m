//
//  XYLoadingView.m
//  DianShangApp
//
//  Created by wa on 14-6-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XYLoadingView.h"
#import "AppDelegate.h"
@implementation XYLoadingView

/* type为遮罩类型 0 遮罩全部  1不遮罩导航*/
- (id)initWithType:(NSInteger)type
{
    XYLoadingView *loadingView = [[[NSBundle mainBundle] loadNibNamed:@"XYLoadingView" owner:nil options:nil] objectAtIndex:0];
    if (loadingView) {
        if(iPhone5){
            if(type == 1){
                if (iOS7) {
                    loadingView.frame = CGRectMake(0, 64, 320,504);
                }
                else
                {
                    loadingView.frame = CGRectMake(0, 0, 320,504);
                }
                
            }else{
                loadingView.frame = CGRectMake(0, 0, 320,568);
            }
            
        }else{
            if(type == 1){
                loadingView.frame = CGRectMake(0, 64, 320,416);
            }
            else if (type ==2){
                
                loadingView.frame = CGRectMake(0, -45, 320,480);
            }
            else if (type == 3)
            {
                loadingView.frame = CGRectMake(0, 0, 320,480);
            }
        }
        [loadingView.activityIndicatorView startAnimating];
    }
    return loadingView;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
