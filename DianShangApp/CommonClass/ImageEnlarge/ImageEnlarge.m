//
//  ImageEnlarge.m
//  ImageEnlarge
//
//  Created by 东 王 on 12-3-9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ImageEnlarge.h"
#import "WXCommonViewClass.h"
#import "AppDelegate.h"
@interface ImageEnlarge()
-(CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center; 
@end

#define ZOOMSCALE 3.0
@implementation ImageEnlarge
@synthesize images,imageView,imageScroll;

-(void)dealloc
{
    [imageScroll release];
    [imageView release];
    [images release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    self.imageScroll = nil;  
	self.imageView = nil;  
    self.images = nil;
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES];
    [super viewWillAppear:animated];
}

-(void)viewDidLoad
{
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    [super viewDidLoad];

    imageView=[[UIImageView alloc]initWithImage:images]; 
    imageScroll=[[UIScrollView alloc] init];
    imageScroll.delegate=self;
    imageScroll.showsHorizontalScrollIndicator=NO;
    imageScroll.showsVerticalScrollIndicator=NO;
    imageScroll.contentSize= CGSizeMake(1024,768);
    
    //绑定单击事件
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    [tapGestureRecognizer setNumberOfTapsRequired:1];
    [imageView addGestureRecognizer:tapGestureRecognizer];
    CGRect frame = imageView.frame;
    if(iPhone5){
        self.view.frame = CGRectMake(0,20,320,548);
        self.imageScroll.frame=CGRectMake(0,0,320,548);
        imageView.frame = CGRectMake((320 - frame.size.width)/2,(548-frame.size.height)/2, frame.size.width, frame.size.height);
    }else{
        self.view.frame = CGRectMake(0,20,320,460);
        self.imageScroll.frame=CGRectMake(0,0,320,460);
        imageView.frame = CGRectMake((320 - frame.size.width)/2,(460-frame.size.height)/2, frame.size.width, frame.size.height);        
    }
    
    

    
    self.view.backgroundColor = [UIColor blackColor];
    
    imageView.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *doubleTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    [doubleTap setNumberOfTapsRequired:2];
    [imageView addGestureRecognizer:doubleTap];
    
    
    [tapGestureRecognizer requireGestureRecognizerToFail:doubleTap];    
    float minimumScale = [imageScroll frame].size.width  / [imageView frame].size.width;
    imageScroll.scrollEnabled=YES;
    imageScroll.maximumZoomScale=5.0;
    imageScroll.minimumZoomScale=0.1;
    imageScroll.zoomScale=minimumScale;
    
    [self.imageScroll addSubview:imageView];
    [self.view addSubview:imageScroll];
    
    [tapGestureRecognizer release];
    [doubleTap release];
    
}

-(void)singleTap:(UITapGestureRecognizer*)recognizer{
//    CGAffineTransform tran = CGAffineTransformIdentity;
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.3f];
//    self.view.transform = CGAffineTransformTranslate(tran, -320, 480);
//    [self.navigationController popViewControllerAnimated:NO];
//    [UIView commitAnimations];
    
    
//    CATransition *transition=[CATransition animation];
//    transition.duration=3.0f;
//    transition.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
//    transition.type=kCATransitionMoveIn;
//    transition.subtype=kCATransitionFromRight;
//    transition.delegate=self;
//    [self.view.layer addAnimation:transition forKey:nil];
//    [self.navigationController popViewControllerAnimated:NO];
    
    [self.navigationController popViewControllerAnimated:NO];    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.8f];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:NO];
    [UIView commitAnimations];
    
}
- (void)scrollViewDidZoom:(UIScrollView *)scrollView NS_AVAILABLE_IOS(3_2){
    CGFloat xcenter = scrollView.center.x,ycenter = scrollView.center.y;
    xcenter = scrollView.contentSize.width > scrollView.frame.size.width?scrollView.contentSize.width/2 :xcenter;
    ycenter = scrollView.contentSize.height > scrollView.frame.size.height ?scrollView.contentSize.height/2 : ycenter;
    //双击放大时，图片不能越界，否则会出现空白。因此需要对边界值进行限制。
//    if(_isDoubleTapingForZoom){

//        xcenter = kMaxZoom*(kScreenWidth - _touchX);
//        ycenter = kMaxZoom*(kScreenHeight - _touchY);
//        if(xcenter > (kMaxZoom - 0.5)*kScreenWidth){//放大后左边超界
//            xcenter = (kMaxZoom - 0.5)*kScreenWidth;
//        }else if(xcenter <0.5*kScreenWidth){//放大后右边超界
//            xcenter = 0.5*kScreenWidth;
//        }
//        if(ycenter > (kMaxZoom - 0.5)*kScreenHeight){//放大后左边超界
//            ycenter = (kMaxZoom - 0.5)*kScreenHeight +_offsetY*kMaxZoom;
//        }else if(ycenter <0.5*kScreenHeight){//放大后右边超界
//            ycenter = 0.5*kScreenHeight +_offsetY*kMaxZoom;
//        }

//    }
    [imageView setCenter:CGPointMake(xcenter, ycenter)];
}
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return imageView;  
}  

-(void)handleDoubleTap:(UITapGestureRecognizer *)gestureRecognizer{
    float newScale=[imageScroll zoomScale]*ZOOMSCALE;
    if (newScale>3.0) {
        newScale=1.0;
    }
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];  
    [imageScroll zoomToRect:zoomRect animated:YES];
}

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center {  
	
	CGRect zoomRect;  
	
	// the zoom rect is in the content view's coordinates.   
	//    At a zoom scale of 1.0, it would be the size of the imageScrollView's bounds.  
	//    As the zoom scale decreases, so more content is visible, the size of the rect grows.  
	zoomRect.size.height = [imageScroll frame].size.height / scale;  
	zoomRect.size.width  = [imageScroll frame].size.width  / scale;  
	
	// choose an origin so as to get the right center.  
	zoomRect.origin.x    = center.x - (zoomRect.size.width  / 2.0);  
	zoomRect.origin.y    = center.y - (zoomRect.size.height / 2.0);  
    
	return zoomRect;
}
@end
