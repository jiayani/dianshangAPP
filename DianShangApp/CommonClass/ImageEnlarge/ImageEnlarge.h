//
//  ImageEnlarge.h
//  ImageEnlarge
//
//  Created by 东 王 on 12-3-9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ImageEnlarge : UIViewController<UIScrollViewDelegate>{
    UIScrollView *imageScroll;
    UIImageView  *imageView;
    UIImage      *images;
}

@property (nonatomic, retain) UIScrollView *imageScroll;
@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic, retain) UIImage *images;
@end
