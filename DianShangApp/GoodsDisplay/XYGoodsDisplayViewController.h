//
//  XYGoodsDisplayViewController.h
//  DianShangApp
//
//  Created by wa on 14-5-27.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomSeg.h"
#import "EGORefreshTableFooterView.h"
#import "XYLoadingView.h"
#import "XYNoDataView.h"
@interface XYGoodsDisplayViewController : UIViewController<CustomSegDelegate, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate,EGORefreshTableDelegate>
{
    NSMutableArray * tabArr;
    UISearchBar *serchBar;
    UIView * blackBackView;
    NSMutableArray * productListArr;
    BOOL isHaveMore;
    BOOL reloading;
    EGORefreshTableFooterView *refreshFooterView;
    BOOL setFlag;
    XYLoadingView * loadingView;
    XYNoDataView * noDataView;
    CustomSeg *customSeg;
    NSMutableArray * transProductListArr;
    NSDictionary *transDict;
}
@property (weak, nonatomic) IBOutlet UITableView *mianTableView;
@property(assign,nonatomic) int type;
@property(strong,nonatomic) NSString * keyword;
@property(assign,nonatomic) int sortType;
@property(assign,nonatomic) int class1_id;
@property(assign,nonatomic) int class2_id;
@property(assign,nonatomic) int page;
@property(strong,nonatomic) NSString * searchFlag;
@property (nonatomic, retain) NSString *att1_id;
@property (nonatomic, retain) NSString *att2_id;
@property(strong,nonatomic) NSString *searchClassStr;
@property(assign,nonatomic) BOOL isKeyWord;
@end
