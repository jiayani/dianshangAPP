//
//  XYGoodsDisplayViewController.m
//  DianShangApp
//
//  Created by wa on 14-5-27.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XYGoodsDisplayViewController.h"
#import "WXCommonViewClass.h"
#import "XY_Cloud_ProductList.h"
#import "AsynImageView.h"
#import "WXStrikeThroughLabel.h"
#import "XYGoodsInforViewController.h"
#import "WX_Cloud_IsConnection.h"
#import "WXConfigDataControll.h"

#define maxCountOfOnePage 8;
static BOOL isFirstComeIn = YES;
@interface XYGoodsDisplayViewController ()

@end

@implementation XYGoodsDisplayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [WXCommonViewClass hideTabBar:self isHiden:YES];
        /*设置导航返回按钮*/
        UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
        [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
        self.navigationItem.leftBarButtonItem = leftBarButtonItem;
        [self createSearchBar];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    transDict = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%i",self.type],@"type",
                 [NSString stringWithFormat:@"%i",self.sortType],@"sortType",
                 [NSString stringWithFormat:@"%i",self.page],@"page", nil];
    isFirstComeIn = YES;
    customSeg = [[[NSBundle mainBundle] loadNibNamed:@"CustomSeg" owner:self options:nil] objectAtIndex:0];
    customSeg.tag = 99;
    customSeg.delegate = self;
    customSeg.typeSeg = 1;
    if (self.sortType == 9) {
        [customSeg.slider setHidden:YES];
    }
    else
    {
        [customSeg.slider setHidden:NO];
    }
    [customSeg setBtn1Title:@"销量" andBtn2Title:@"价格" andBtn1Image:nil andBtn2Image:[UIImage imageNamed:@"price1.png"]];
    [customSeg.btn2 setContentEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 0)];
    [self.view addSubview:customSeg];
    
    
    if (self.isKeyWord) {
        serchBar.placeholder = @"请输入搜索关键字";
        if (self.searchClassStr != nil && self.searchClassStr.length > 0){
            serchBar.text = self.searchClassStr;
        }
    }else{
        serchBar.placeholder = [NSString stringWithFormat:@"在%@下搜索",self.searchClassStr];
        
    }
    
//    mianTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, customSeg.frame.size.height, self.view.frame.size.width , self.view.frame.size.height  - customSeg.frame.size.height +23) style:UITableViewStylePlain];
    self.mianTableView.tag = 1;
    self.mianTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.mianTableView.delegate = self;
    self.mianTableView.dataSource = self;
    self.mianTableView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.mianTableView];
    
    if ([self.searchFlag isEqualToString:@"searchFlag"]) {
        serchBar.text = self.keyword;
        [self searchBarTextDidBeginEditing:serchBar];
        
    }
    else{
        productListArr = [[NSMutableArray alloc]init];
        
        [self requestProductList];
        
    }
    [self initRefreshFooterView];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [loadingView removeFromSuperview];
    [noDataView removeFromSuperview];
    [[XY_Cloud_ProductList share] setDelegate:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
}

- (void)initRefreshFooterView{
    
    int height = MAX(self.mianTableView.bounds.size.height, self.mianTableView.contentSize.height);
    refreshFooterView = [[EGORefreshTableFooterView alloc]  initWithFrame:CGRectZero];
    refreshFooterView.frame =CGRectMake(0.0f, height, self.view.frame.size.width, self.mianTableView.bounds.size.height);

    //如果contentsize的高度比表的高度小，那么就需要把刷新视图放在表的bounds的下面
    
    refreshFooterView.delegate = self;
    //下拉刷新的控件添加在tableView上
    
    [self.mianTableView addSubview:refreshFooterView];
    reloading = NO;
    
}

-(void)requestProductList{
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
    [[XY_Cloud_ProductList share] setDelegate:(id)self];
    [[XY_Cloud_ProductList share] productListSyncWithType:self.type andKeyword:self.keyword andSortype:self.sortType andClass1:self.class1_id andClass2:self.class2_id andPage:self.page andAtt1:self.att1_id andAtt2:self.att2_id];
}
-(void)productListSyncSuccess:(NSDictionary *)data
{
    self.page = 1;
    [noDataView removeFromSuperview];
    [loadingView removeFromSuperview];
    if (productListArr.count == 0) {
        productListArr = [NSMutableArray arrayWithArray:[[data objectForKey:@"data"] objectForKey:@"items"]];
    }else{
        [productListArr addObjectsFromArray:[[data objectForKey:@"data"] objectForKey:@"items"]] ;
    }
    NSArray * arr = [[data objectForKey:@"data"] objectForKey:@"items"];
    if (arr.count == 0) {
        if(isFirstComeIn)
        {
            noDataView =[[XYNoDataView alloc]initWithFrame:self.view.frame];
            noDataView.megUpLabel.text = @"暂时没有数据";
            noDataView.megDownLabel.text = @"先去其他页面看看吧";
            [self.view addSubview:noDataView];
            isFirstComeIn = NO;
        }
        else{
            
        [WXCommonViewClass showHudInView:self.view title:@"没有获取相关数据"];
            
        }
    }
    if (productListArr.count == 0) {
        if(isFirstComeIn)
        {
            noDataView =[[XYNoDataView alloc]initWithFrame:self.view.frame];
            noDataView.megUpLabel.text = @"暂时没有数据";
            noDataView.megDownLabel.text = @"先去其他页面看看吧";
            [self.view addSubview:noDataView];
            isFirstComeIn = NO;
        }
        else{
            
            [WXCommonViewClass showHudInView:self.view title:@"没有获取相关数据"];
            
        }
    }
    int hasMore = [[data  objectForKey:@"hasMore"]intValue];
    if (hasMore == 1) {
        isHaveMore = YES;
    }else{
        isHaveMore = NO;
    }
    //刷新UI
    [self reloadUI];
}
-(void)productListSyncFailed:(NSString*)errMsg
{
    [noDataView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:@"没有获取相关数据"];
    [loadingView removeFromSuperview];
    [[XY_Cloud_ProductList share] setDelegate:nil];
    //刷新UI
    [self reloadUI];
    [self addNodataView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) createSearchBar {
    serchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(-10 , -4, 250.0f, 50.0f)];

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        [[[[serchBar.subviews objectAtIndex:0]subviews]objectAtIndex:0]removeFromSuperview];
    }
    else {
        for(id img in serchBar.subviews)
        {
            if([img isKindOfClass:NSClassFromString(@"UISearchBarBackground")])
            {
                [img removeFromSuperview];
            }
        }
        serchBar.frame = CGRectMake(-10 , -4, 250.0f, 50.0f);
        
    }
    serchBar.backgroundColor = [UIColor clearColor];
    [serchBar setContentMode:UIViewContentModeLeft];
    //    [searchBar searchTextPositionAdjustment:UIOffsetMake(20.0, 30.0)];
    serchBar.delegate = self;
    
    
    //将搜索条放在一个UIView上
    UIView *searchView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 250, 44)];
    searchView.backgroundColor = [UIColor clearColor];
    [searchView addSubview:serchBar];
    
    self.navigationItem.titleView = searchView;
    
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    serchBar.frame = CGRectMake(-10 , -4, 200.0f, 50.0f);
    UIButton * cancleBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:NSLocalizedString(@"取消", @"搜索/取消")];
    [cancleBtn addTarget:self action:@selector(cancleBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:cancleBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
    if ([self.searchFlag isEqualToString:@"searchFlag"]) {
        [self searchBarSearchButtonClicked:serchBar];
    }
    /*
     blackBackView=[[UIView alloc]initWithFrame:CGRectMake(0,
     64,
     [[UIScreen mainScreen]bounds].size.width,
     [[UIScreen mainScreen]bounds].size.height-64)];
     
     UITableView *keywordTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, blackBackView.frame.size.width , blackBackView.frame.size.height) style:UITableViewStylePlain];
     keywordTableView.tag = 2;
     keywordTableView.delegate = self;
     keywordTableView.dataSource = self;
     keywordTableView.backgroundColor = [UIColor clearColor];
     
     [blackBackView addSubview:keywordTableView];
     
     blackBackView.backgroundColor=[UIColor colorWithRed:255.0f
     green:255.0f
     blue:255.0f
     alpha:0.95];
     
     [[[UIApplication sharedApplication]keyWindow]addSubview:blackBackView];
     [[blackBackView superview]bringSubviewToFront:blackBackView];
     */
}

-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    productListArr = [[NSMutableArray alloc]init];
    
    if ([self.searchFlag isEqualToString:@"searchFlag"]) {
        self.sortType = 2;
        self.page = 1;
        self.searchFlag = @"";
    }
    else {
        self.navigationItem.rightBarButtonItem = nil;
        self.keyword = [NSString stringWithFormat:@"%@",serchBar.text];
        //        serchBar.text = @"";
        serchBar.frame = CGRectMake(-10 , -4, 250.0f, 50.0f);
        [serchBar resignFirstResponder];
        
        self.type = 0;
        self.sortType = 2;
        self.page = 1;
    }
    setFlag = !setFlag;
    [self requestProductList];
}

- (IBAction)cancleBtnPressed:(id)sender {
    self.navigationItem.rightBarButtonItem = nil;
    serchBar.text = @"";
    serchBar.frame = CGRectMake(-10 , -4, 250.0f, 50.0f);
    [serchBar resignFirstResponder];
    productListArr = [[NSMutableArray alloc]init];
    self.type = [[transDict objectForKey:@"type"]intValue];
    self.sortType = [[transDict objectForKey:@"sortType"]intValue];
    self.page = [[transDict objectForKey:@"page"]intValue];
    [customSeg.slider setHidden:YES];
    [customSeg setBtn1Title:@"销量" andBtn2Title:@"价格" andBtn1Image:nil andBtn2Image:[UIImage imageNamed:@"price1.png"]];
    [self requestProductList];
}

//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)segChange:(int)index andIsFirst:(int)firstTag
{
    [customSeg.slider setHidden:NO];
    productListArr = [[NSMutableArray alloc]init];
    if (index == 2) {
        
        if (firstTag == 0 ) {
            self.sortType = 0;
        }
        else if (firstTag == 1) {
            self.sortType = 1;
        }
    }
    else if (index == 1) {
        self.sortType = 2;
    }
    self.page = 1;
    [self requestProductList];
    setFlag = !setFlag;
}

- (void)addNodataView{
    if (noDataView != nil) {
        [noDataView removeFromSuperview];
        noDataView = nil;
    }
    if (productListArr == nil || productListArr.count <= 0) {
        noDataView =[[XYNoDataView alloc]initWithFrame:self.view.frame];
        noDataView.megUpLabel.text = @"暂时没有数据";
        noDataView.megDownLabel.text = @"先去其他页面看看吧";
        [self.view addSubview:noDataView];
    }
}
#pragma mark tableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return productListArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     NSString *CellIdentifier = [NSString stringWithFormat:@"Cell"];//以indexPath来唯一确定cell
    
    if (tableView.tag ==1) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:CellIdentifier];
        }
        for (UIView * subView  in cell.contentView.subviews) {
            [subView removeFromSuperview];
        }
        
        UIImageView * mainImg = [[UIImageView alloc]init];
        [mainImg setFrame:CGRectMake(10, 10, 90, 90)];
        [mainImg setImage:[UIImage imageNamed:@""]];
        
        
        AsynImageView * imageView = [[AsynImageView alloc] init];
        imageView.frame = CGRectMake(10, 10, 90, 90);
        imageView.type = 1;
        imageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
        NSString * image = [[productListArr objectAtIndex:indexPath.row]objectForKey:@"img"];
        imageView.imageURL = [NSString stringWithFormat:@"%@",image];
        
        UILabel * titleLab      = [[UILabel alloc] initWithFrame:CGRectMake(110, 4, 190, 50)];
        titleLab.numberOfLines = 2;
        titleLab.text            = [[productListArr objectAtIndex:indexPath.row]objectForKey:@"productTitle"];
        titleLab.backgroundColor = [UIColor clearColor];
        titleLab.font            = [UIFont systemFontOfSize:14];
        
        UILabel * priceLab       = [[UILabel alloc] initWithFrame:CGRectMake(110, 52, 114, 21)];
        priceLab.textColor       = [UIColor redColor];
        priceLab.text            = [NSString stringWithFormat:@"￥%.2f",[[[productListArr objectAtIndex:indexPath.row]objectForKey:@"discountPrice"] floatValue]];
        priceLab.backgroundColor = [UIColor clearColor];
        priceLab.font            = [UIFont boldSystemFontOfSize:16];
        
        
        WXStrikeThroughLabel * oldPriceLab       = [[WXStrikeThroughLabel alloc] initWithFrame:CGRectMake(182, 52, 114, 21)];
        oldPriceLab.textColor       = [UIColor lightGrayColor];
        oldPriceLab.text            = [NSString stringWithFormat:@"￥%.2f",[[[productListArr objectAtIndex:indexPath.row]objectForKey:@"originPrice"] floatValue]];
        oldPriceLab.backgroundColor = [UIColor clearColor];
        oldPriceLab.font            = [UIFont systemFontOfSize:12];
        
        if ([[[productListArr objectAtIndex:indexPath.row]objectForKey:@"discountPrice"] floatValue]== 0) {
            priceLab.text            = [NSString stringWithFormat:@"￥%.2f",[[[productListArr objectAtIndex:indexPath.row]objectForKey:@"originPrice"] floatValue]];
            oldPriceLab.text = @"";
        }
        
        priceLab.textColor = [WXConfigDataControll getFontColorOfPrice];
        
        float evaluateValue = [[[productListArr objectAtIndex:indexPath.row]objectForKey:@"evaluateValue"] floatValue];
        UIView * starView = [WXCommonViewClass getStarView:evaluateValue :112 :80];
        
        tabArr =[[NSMutableArray alloc]init];
        
        if ([[[productListArr objectAtIndex:indexPath.row]objectForKey:@"isPopularity"] intValue] == 1) {
            [tabArr addObject:@"renqi.png"];
        }
        
        if ([[[productListArr objectAtIndex:indexPath.row]objectForKey:@"isfreeFreight"] intValue] == 1) {
            [tabArr addObject:@"baoyou.png"];
        }
        if ([[[productListArr objectAtIndex:indexPath.row]objectForKey:@"isHotsell"] intValue] ==1) {
            [tabArr addObject:@"rexiao.png"];
        }
        for (int i = 0; i<tabArr.count; i++) {
            UIImageView * tabImg = [[UIImageView alloc]init];
            [tabImg setFrame:CGRectMake(188+i*32, 80, 30, 13)];
            [tabImg setImage:[UIImage imageNamed:[tabArr objectAtIndex:i]]];
            [cell.contentView addSubview:tabImg];
        }
        
        CGSize textSize = [priceLab.text sizeWithFont:priceLab.font];
        CGFloat strikeWidth = textSize.width;
        CGRect frame = priceLab.frame;
        frame.size.width = strikeWidth;
        priceLab.frame = frame;
        CGRect oldPriceFrame = oldPriceLab.frame;
        oldPriceFrame.origin.x = frame.origin.x + strikeWidth + 5.0f;
        oldPriceLab.frame = oldPriceFrame;
        
        
        UIView * singLine = [[UIView alloc]initWithFrame:CGRectMake(110, 112, 210, 0.5)];
        singLine.backgroundColor = [UIColor colorWithRed:235.0/255.0f green:235.0/255.0f blue:235.0/255.0f alpha:1.0];
        
        [cell.contentView addSubview:imageView];
        [cell.contentView addSubview:titleLab];
        [cell.contentView addSubview:mainImg];
        [cell.contentView addSubview:starView];
        [cell.contentView addSubview:singLine];
        [cell.contentView addSubview:priceLab];
        [cell.contentView addSubview:oldPriceLab];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
    else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:CellIdentifier];
        }
        
        UILabel * keywordLab       = [[UILabel alloc] initWithFrame:CGRectMake(12, 12, 114, 21)];
        keywordLab.text            = @"关键字";
        keywordLab.backgroundColor = [UIColor clearColor];
        keywordLab.font            = [UIFont systemFontOfSize:16];
        
        cell.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:keywordLab];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
    
}

- (CGFloat) tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 1) {
        return 115;
    }
    else {
        return 50;
    }
    
}

#pragma mark - 下拉刷新数据方法
//请求数据
-(void)requestData
{
    reloading = YES;
    BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
    if (isConnection) {//有网络情况
        
        //每次进入该页面的时候，重新从服务器端读取数据
        if(refreshFooterView.hasMoreData)
        {
            
            int myCount = productListArr.count;
            int pageCount = myCount/maxCountOfOnePage;
            self.page = pageCount + 1;
            //下载团购信息
            [self requestProductList];
            
        }
    }else{//无网络情况
        [WXCommonViewClass showHudInButtonOfView:self.view title:NoHaveNetwork closeInSecond:2];
    }
    
}

-(void)reloadUI
{
    reloading = NO;
    //停止下拉的动作,恢复表格的便宜
	[refreshFooterView egoRefreshScrollViewDataSourceDidFinishedLoading:self.mianTableView];
    //更新界面
    [self.mianTableView reloadData];
    
    if (setFlag) {
        self.mianTableView.contentOffset = CGPointMake(0, 0);
        setFlag = !setFlag;
    }
    
    [self setRefreshViewFrame];
    
}

-(void)setRefreshViewFrame
{
    //如果contentsize的高度比表的高度小，那么就需要把刷新视图放在表的bounds的下面
    int height = MAX(self.mianTableView.bounds.size.height, self.mianTableView.contentSize.height);
    refreshFooterView.frame =CGRectMake(0.0f, height, self.view.frame.size.width, self.mianTableView.bounds.size.height);
}

#pragma mark - EGORefreshTableFooterDelegate
//出发下拉刷新动作，开始拉取数据
- (void)egoRefreshTableDidTriggerRefresh:(EGORefreshPos)aRefreshPos
{
    [self requestData];
}


//返回当前刷新状态：是否在刷新
- (BOOL)egoRefreshTableDataSourceIsLoading:(UIView*)view{
	
	return reloading; // should return if data source model is reloading
	
}
//返回刷新时间
// if we don't realize this method, it won't display the refresh timestamp
- (NSDate*)egoRefreshTableDataSourceLastUpdated:(UIView*)view
{
	
	return [NSDate date]; // should return date data source was last changed
	
}

#pragma mark - UIScrollView

//此代理在scrollview滚动时就会调用
//在下拉一段距离到提示松开和松开后提示都应该有变化，变化可以在这里实现
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    refreshFooterView.hasMoreData = isHaveMore;
    [refreshFooterView egoRefreshScrollViewDidScroll:scrollView];
}
//松开后判断表格是否在刷新，若在刷新则表格位置偏移，且状态说明文字变化为loading...
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    refreshFooterView.hasMoreData = isHaveMore;
    [refreshFooterView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark tableView deleagte
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    XYGoodsInforViewController * xyGoodsInforVC= [[XYGoodsInforViewController alloc] initWithNibName:@"XYGoodsInforViewController" bundle:nil];
    xyGoodsInforVC.productId = [[productListArr objectAtIndex:indexPath.row]objectForKey:@"productID"];
    [self.navigationController pushViewController:xyGoodsInforVC animated:YES];
}

//无网络连接和意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[XY_Cloud_ProductList share] setDelegate:nil];
    //小圈停止运动
    [loadingView removeFromSuperview];
    [self addNodataView];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
    
    
}

@end
