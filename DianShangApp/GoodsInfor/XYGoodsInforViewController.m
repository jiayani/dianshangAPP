//
//  XYGoodsInforViewController.m
//  DianShangApp
//
//  Created by wa on 14-5-28.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XYGoodsInforViewController.h"
#import "AsynImageView.h"
#import "XY_Cloud_Advertising.h"
#import "WXCommonViewClass.h"
#import "FYInfomationViewController.h"
#import "WXShoppingCartViewController.h"
#import "WX_Cloud_UpdateAttributeOfProduct.h"
#import "WXAddShoppingCartViewController.h"
#import "XY_Cloud_GoodInforData.h"
#import "WXGroupByingListViewController.h"
#import "FYReviewViewController.h"
#import "FY_Cloud_addCollect.h"
#import "FY_Cloud_deleteColect.h"
#import "WXUserLoginViewController.h"
#import "MKNumberBadgeView.h"
#import "WXShoppingCartDatabaseClass.h"
#import "WXConfigDataControll.h"
#import "ImageEnlarge.h"
#import "WXCommonViewClass.h"
#import "WeiboSDK.h"
#import "WXApi.h"
#import "FYShareViewController.h"
/*当前显示广告索引*/

static NSInteger maxPageControllerIndex = 0;
//产品详情高度
static float productContentHight = 0.0;
//产品详情展开标志位
static BOOL switchFlag = NO;

@interface XYGoodsInforViewController ()
{
    NSString *productId;
}
@end

@implementation XYGoodsInforViewController
@synthesize inforArray;
@synthesize productId;
//@synthesize delegate = _delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //微信、新浪注册
    [self register];
    // Do any additional setup after loading the view from its nib.
    //<img src ='http://192.168.1.10:8088/Public/Uploads/original/image/20140116/20140116095053_69046.png' border = 0>
    //<IMG border=0 src='http://orc.wadiankeji.com/Public/Uploads/original/20140304/5315c56f5634d.jpg'>
    [self.view addSubview:self.shareView];
    self.shareView.frame = CGRectMake(0, self.view.frame.size.height, self.shareView.frame.size.width, self.shareView.frame.size.height);
    if (iPhone5) {
        self.mainScrollView.contentSize = CGSizeMake(320, self.mainScrollView.frame.size.height);
        
    }else{
        self.mainScrollView.contentSize = CGSizeMake(320, self.mainScrollView.frame.size.height);
    }
    
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [[UIApplication sharedApplication].keyWindow addSubview:loadingView];
    
    [[XY_Cloud_GoodInforData share] setDelegate:(id)self];
    [[XY_Cloud_GoodInforData share] goodInforSync:[self.productId intValue]];
    switchFlag = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[FY_Cloud_addCollect share] setDelegate:nil];
    [[FY_Cloud_deleteColect share] setDelegate:nil];
    [[WX_Cloud_UpdateAttributeOfProduct share] setDelegate:nil];
    [[XY_Cloud_GoodInforData share] setDelegate:nil];
    progressHUD = nil;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [[XY_Cloud_GoodInforData share] setDelegate:(id)self];
    [[XY_Cloud_GoodInforData share] goodInforSync:[self.productId intValue]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.shopCartBtn addSubview:[self MKNumberBadgeView:[WXShoppingCartDatabaseClass getAllShoppingCartCount]]];
}


//微信、新浪注册
- (void)register{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if(singletonClass.isRegisterSina == NO){//没有注册
        //注册新浪微博
        [WeiboSDK enableDebugMode:YES];
        if([WeiboSDK registerApp:[WXConfigDataControll getSinaAppKey]]){
            singletonClass.isRegisterSina = YES;
        }
        
    }
    //微信注册
    if (singletonClass.isRegisterWeiXin == NO) {
        [WXApi registerApp:[WXConfigDataControll getWeiXinAppKey] withDescription:@"DianShangApp"];
        singletonClass.isRegisterWeiXin = YES;
    }
    
}

-(MKNumberBadgeView *)MKNumberBadgeView:(NSInteger)newNum{
    MKNumberBadgeView * CartNum;
    if (CartNum) {
        [CartNum removeFromSuperview];
    }
    CartNum = [[MKNumberBadgeView alloc]initWithFrame:CGRectMake(29, -3, 18, 18)];
    CartNum.font = [UIFont boldSystemFontOfSize:14];
    CartNum.hideWhenZero = YES;
    CartNum.value = newNum;
    CartNum.shadow = NO;
    CartNum.layer.masksToBounds = NO;
    CartNum.layer.cornerRadius = 3.0;
    CartNum.layer.borderWidth = 1.3;
    CartNum.layer.borderColor = [[WXConfigDataControll getColorOfSubscript] CGColor];
    CartNum.alignment = NSTextAlignmentCenter;
    return CartNum;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)goodInforSyncSuccess:(NSDictionary *)data
{
    [loadingView removeFromSuperview];
    productDic = data;
    //    [self.inforWebview loadHTMLString:@"<IMG border=0 src='http://orc.wadiankeji.com/Public/Uploads/original/20140304/5315c56f5634d.jpg'>" baseURL:nil];
    if ([[[data objectForKey:@"hasCollect"]stringValue] isEqualToString:@"1"]) {
        [self.collectBtn setFrame:CGRectMake(216, 328, 35, 33)];
        [self.collectBtn setImage:[UIImage imageNamed:@"Already-collected-.png"] forState:UIControlStateNormal];
        self.collectBtn.tag = 1;
    }
    else
    {
        [self.collectBtn setFrame:CGRectMake(222, 328, 23, 33)];
        [self.collectBtn setImage:[UIImage imageNamed:@"collection-.png"] forState:UIControlStateNormal];
        self.collectBtn.tag = 0;
    }
    
    if ([[NSString stringWithFormat:@"%@",[data objectForKey:@"relateActivityCount"]] isEqualToString:@"0"]) {
        relateActive = YES;
    }
    else
    {
        relateActive = NO;
    }
    
    if (switchFlag == NO) {
        [self.inforWebview loadHTMLString:[NSString stringWithFormat:@"%@",[data objectForKey:@"productContent"]] baseURL:nil];
    }
    
    self.relateActivityCountLabel.text = [NSString stringWithFormat:@"(%@)",[data objectForKey:@"relateActivityCount"]];
    self.consultCountLabel.text = [NSString stringWithFormat:@"(%@)",[data objectForKey:@"consultCount"]];
    self.evaluateCountLabel.text = [NSString stringWithFormat:@"(%@)",[data objectForKey:@"evaluateCount"]];
    self.productTitleLabel.text = [data objectForKey:@"productTitle"];
    self.discountPriceLabel.text = [NSString stringWithFormat:@"￥%.2f",[[data objectForKey:@"discountPrice"] floatValue]];
    self.originPriceLabel.text = [NSString stringWithFormat:@"￥%.2f",[[data objectForKey:@"originPrice"] floatValue]];
    
    self.discountPriceLabel.textColor = [WXConfigDataControll getFontColorOfPrice];
    
    evaluateValue = [[data objectForKey:@"evaluateValue"] floatValue];
    
    UIView * starView = [WXCommonViewClass getStarView:evaluateValue :100 :512];
    [self.mainScrollView addSubview:starView];
    tabArr =[[NSMutableArray alloc]init];
    
    if ([[data objectForKey:@"isPopularity"] isEqualToString:@"1"]) {
        [tabArr addObject:@"renqi.png"];
    }
    if ([[data objectForKey:@"isfreeFreight"] isEqualToString:@"1"]) {
        [tabArr addObject:@"baoyou.png"];
    }
    if ([[data objectForKey:@"isHotsell"] isEqualToString:@"1"]) {
        [tabArr addObject:@"rexiao.png"];
    }
    
    if ([[data objectForKey:@"discountPrice"] floatValue]== 0) {
        self.discountPriceLabel.text            = [NSString stringWithFormat:@"￥%.2f",[[data objectForKey:@"originPrice"] floatValue]];
        self.originPriceLabel.text = @"";
    }
    
    CGSize textSize = [self.discountPriceLabel.text sizeWithFont:self.discountPriceLabel.font];
    CGFloat strikeWidth = textSize.width;
    CGRect frame = self.discountPriceLabel.frame;
    frame.size.width = strikeWidth;
    self.discountPriceLabel.frame = frame;
    CGRect oldPriceFrame = self.originPriceLabel.frame;
    oldPriceFrame.origin.x = frame.origin.x + strikeWidth + 5.0f;
    self.originPriceLabel.frame = oldPriceFrame;
    
    
    CGSize orTextSize = [self.originPriceLabel.text sizeWithFont:self.originPriceLabel.font];
    CGFloat orStrikeWidth = orTextSize.width;
    CGRect orFrame = self.originPriceLabel.frame;
    orFrame.size.width = orStrikeWidth;
    
    float x= 0.0 ;
    
    if (self.originPriceLabel.text.length == 0) {
        x= frame.origin.x + strikeWidth + 5.0f;
    }
    else
    {
        x=orFrame.origin.x + orStrikeWidth + 5.0f;;
    }
    
    for (int i = 0; i<tabArr.count; i++) {
        UIImageView * tabImg = [[UIImageView alloc]init];
        [tabImg setFrame:CGRectMake(x+i*32, 374, 30, 13)];
        [tabImg setImage:[UIImage imageNamed:[tabArr objectAtIndex:i]]];
        [self.mainScrollView addSubview:tabImg];
    }
    
    
    inforArray = [data objectForKey:@"productPics"];
    //    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    //    NSString *serviceURL = singletonClass.serviceURL;
    maxPageControllerIndex = inforArray.count + 1;
    self.inforScrollView.contentSize = CGSizeMake(320*([inforArray count]+2), 320);
    self.customPageControl.numberOfPages = [inforArray count];
    
    if (inforArray.count <= 1) {
        self.inforScrollView.scrollEnabled = NO;
    }
    else {
        self.inforScrollView.scrollEnabled = YES;
    }
    
    for (int i=0; i< [inforArray count]; i++)
    {
        AsynImageView * imageView = [[AsynImageView alloc] init];
        imageView.frame = CGRectMake((i+1)*320, 0, 320, 320);
        imageView.tag = i;
        
        imageView.type = 2;
        imageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
        NSDictionary * object = [inforArray objectAtIndex:i];
        NSString * image = [object objectForKey:@"img"];
        imageView.imageURL = [NSString stringWithFormat:@"%@",image];
        
        //绑定单击事件
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(inforSingleTap:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [imageView addGestureRecognizer:tapGestureRecognizer];
        imageView.userInteractionEnabled = YES;
        [self.inforScrollView addSubview:imageView];
    }
    //在第一个位置添加第一张广告图片
    [self loadAdHolderWithAdIndex:0];
    
    //在最后一个位置添加第一张广告图片
    [self loadAdHolderWithAdIndex:inforArray.count-1];
    
    self.inforScrollView.decelerationRate = 0;
    self.inforScrollView.frame = CGRectMake(0, 0, 320, 320);
    self.inforScrollView.showsHorizontalScrollIndicator = NO;
    self.inforScrollView.showsVerticalScrollIndicator = NO;
    [self.inforScrollView scrollRectToVisible:CGRectMake(320,0, 320, 320) animated:NO];
    self.customPageControl.currentPage = 0;
}

-(void)goodInforSyncFailed:(NSString*)errMsg
{
    [[XY_Cloud_GoodInforData share] setDelegate:nil];
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errMsg];
    
}
/*加载广告占位 第一张和最后一张*/
- (void)loadAdHolderWithAdIndex:(NSInteger)index{
    
    
    AsynImageView *imageView = [[AsynImageView alloc]init];
    NSDictionary * object;
    //第一张广告过渡
    if(index == 0){
        object = [inforArray objectAtIndex:inforArray.count - 1];
        
        //第一张广告过渡添加在0的位置
        imageView.frame = CGRectMake(0, 0, 320, 320);
        self.firstAdImageView = imageView;
        //最后一张广告过渡
    }else{
        object = [inforArray objectAtIndex:0];
        //最后一张广告过渡添加在最后一张广告过渡的后面
        imageView.frame = CGRectMake((index+2)*320, 0, 320, 320);
        self.lastAdImageView = imageView;
    }
    imageView.type = 2;
    imageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    
    NSString * image = [object objectForKey:@"img"];
    imageView.imageURL = [NSString stringWithFormat:@"%@",image];
    
    [self.inforScrollView addSubview:imageView];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.inforScrollView) {
        
        int cur = scrollView.contentOffset.x / scrollView.frame.size.width ;
        self.customPageControl.currentPage = cur-1;
        //当前存在广告信息
        if(inforArray !=nil && inforArray.count != 0){
            if (cur==0){
                [self.inforScrollView scrollRectToVisible:CGRectMake(320 * [inforArray count],0,320,460) animated:NO];
                self.customPageControl.currentPage = [inforArray count]-1;
                
            }else if (cur==([inforArray count]+1)){
                [self.inforScrollView scrollRectToVisible:CGRectMake(320,0,320,460) animated:NO];
                self.customPageControl.currentPage = 0;
                
            }
            //当前不存在广告信息 默认添加4张占位图片
        }else{
            if (cur==0){
                [self.inforScrollView scrollRectToVisible:CGRectMake(320 * 4,0,320,460) animated:NO];
                self.customPageControl.currentPage = 3;
                
            }else if (cur == 5){
                [self.inforScrollView scrollRectToVisible:CGRectMake(320,0,320,460) animated:NO];
                self.customPageControl.currentPage = 0;
                
            }
        }
    }
}

- (void)inforSingleTap:(UITapGestureRecognizer*)recognizer{
    
    self.navigationController.view.backgroundColor = [UIColor blackColor];
    AsynImageView *imageView = (AsynImageView *)[self.inforScrollView viewWithTag:self.customPageControl.currentPage];
    ImageEnlarge *pictureVC = [[ImageEnlarge alloc]init];
    
    
    if(imageView.image != nil){
        pictureVC.images = imageView.image;
        
        float height = imageView.image.size.height * 320.0 / imageView.image.size.width;
        pictureVC.imageView.frame = CGRectMake(0,0, 320,height);
        pictureVC.view.backgroundColor = [UIColor blackColor];
        pictureVC.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f];
        
        pictureVC.view.transform = CGAffineTransformMakeScale(1, 1);
        
        
        [self.navigationController pushViewController:pictureVC animated:NO];
        [UIView commitAnimations];
    }else{
        //弹出提示语
        [WXCommonViewClass showHudInView:self.view title:@"未发现本地图片"];
    }
    
    
}
#pragma WebviewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView { //webview 自适应高度
    //    NSString *height = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('img')[0].height"];
    NSString *height = [webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"];
    productContentHight = [height integerValue];
    //    productContentHight = 500;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [webView stringByEvaluatingJavaScriptFromString:@"var imgs = document.getElementsByTagName('img');"
         " for(var i= 0;i<imgs.length;i++){"
         "  imgs[i].width = 305;"
         "}"];
        CGRect webFrame = webView.frame;
        
        webView.frame = CGRectMake(webFrame.origin.x, webFrame.origin.y,webFrame.size.width,productContentHight);
        
        self.mainScrollView.contentSize = CGSizeMake(320, 721 + productContentHight);
    });
    
}
- (IBAction)collectBtnAct:(id)sender {
    UIButton *btn = (UIButton *)sender;
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if (singletonClass.currentUserId == NULL ||  singletonClass.currentUserId == nil) {
        WXUserLoginViewController * userLoginVC = [[WXUserLoginViewController alloc]initWithNibName:@"WXUserLoginViewController" bundle:nil];
        [self.navigationController pushViewController:userLoginVC animated:YES];
    }
    else
    {
        if (btn.tag == 0) {
            // btn.tag = 1;
            [[FY_Cloud_addCollect share] setDelegate:(id)self];
            [[FY_Cloud_addCollect share] addCollectWithProductID:[self.productId intValue]];
            
            
        }
        else {
            // btn.tag = 0;
            [[FY_Cloud_deleteColect share] setDelegate:(id)self];
            [[FY_Cloud_deleteColect share] deleteCollectWithProductID:[self.productId intValue]];
            
            
        }
    }
}

//添加收藏成功回调
- (void)syncAddCollectSuccess:(NSString *)message
{
    self.collectBtn.tag = 1;
    [self.collectBtn setFrame:CGRectMake(216, 328, 35, 33)];
    [self.collectBtn setImage:[UIImage imageNamed:@"Already-collected-.png"] forState:UIControlStateNormal];
    [WXCommonViewClass showHudInView:self.view title:NSLocalizedString(@"添加收藏成功",@"添加收藏成功")];
}
- (void)syncAddCollectFailed:(NSString*)errCode
{
    [[FY_Cloud_addCollect share] setDelegate:nil];
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errCode];
}

//删除收藏成功回调
- (void)syncDeleteCollectSuccess:(NSString *)message
{
    self.collectBtn.tag = 0;
    [self.collectBtn setFrame:CGRectMake(222, 328, 23, 33)];
    [self.collectBtn setImage:[UIImage imageNamed:@"collection-.png"] forState:UIControlStateNormal];
    [WXCommonViewClass showHudInView:self.view title:NSLocalizedString(@"取消收藏成功",@"取消收藏成功")];
}
- (void)syncDeleteCollectFailed:(NSString*)errCode
{
    [[FY_Cloud_deleteColect share] setDelegate:nil];
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errCode];
}
//无网络连接和意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[WX_Cloud_UpdateAttributeOfProduct share] setDelegate:nil];
    //小圈停止运动
    progressHUD =[WXCommonViewClass showHudInView:self.view title:errorStr];
    progressHUD.delegate = self;
    [loadingView removeFromSuperview];
}


#pragma mark - MBProgressHUDDelegate
- (void)hudWasHidden:(MBProgressHUD *)hud{
    progressHUD = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)backBtnAct:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)goodSpeakBtnAct:(id)sender {
    FYInfomationViewController * infomationVC= [[FYInfomationViewController alloc] initWithNibName:@"FYInfomationViewController" bundle:nil];
    infomationVC.productId= [self.productId intValue];
    infomationVC.pushViewController = self;
    [self.navigationController pushViewController:infomationVC animated:YES];
}

- (IBAction)goBuyBtnAct:(id)sender {
    [[WX_Cloud_UpdateAttributeOfProduct share] setDelegate:(id)self];
    [[WX_Cloud_UpdateAttributeOfProduct share] updateAttribute:[self.productId intValue]];
}

- (IBAction)goShopCartBtnAct:(id)sender {
    [[WX_Cloud_UpdateAttributeOfProduct share] setDelegate:(id)self];
    [[WX_Cloud_UpdateAttributeOfProduct share] updateAttribute:[self.productId intValue]];
}

-(void)updateAttributeSuccess:(NSDictionary *)dataDic{
    [[WX_Cloud_UpdateAttributeOfProduct share] setDelegate:nil];
    //小圈停止运动
    [loadingView removeFromSuperview];
    
    WXAddShoppingCartViewController * AddShoppingCartVC= [[WXAddShoppingCartViewController alloc] initWithNibName:@"WXAddShoppingCartViewController" bundle:nil];
    AddShoppingCartVC.dataDic = dataDic;
    AddShoppingCartVC.productDic = [NSMutableDictionary dictionaryWithDictionary:productDic];
    [self.navigationController pushViewController:AddShoppingCartVC animated:YES];
    
}

-(void)updateAttributeFailed:(NSString*)errCode{
    
    [[WX_Cloud_UpdateAttributeOfProduct share] setDelegate:nil];
    //小圈停止运动
    [loadingView removeFromSuperview];
    
}


- (IBAction)shopCartBtnAct:(id)sender {
    [WXCommonViewClass goToTabItem:self.navigationController tabItem:4];
}

- (IBAction)inforBtnAct:(id)sender {
    if (!switchFlag) {
        
        //        [self.inforWebview setFrame:CGRectMake(self.inforWebview.frame.origin.x, self.inforWebview.frame.origin.y, 320, 0)];
        [self.inforWebview setHidden:YES];
        self.mainScrollView.contentSize = CGSizeMake(320, 721);
        [self.inforImageView setImage:[UIImage imageNamed:@"Products-in-detail-.png"]];
        
        switchFlag = !switchFlag;
    }
    else {
        [self.inforWebview setHidden:NO];
        [self.inforWebview setFrame:CGRectMake(self.inforWebview.frame.origin.x, self.inforWebview.frame.origin.y,320,productContentHight)];
        
        self.mainScrollView.contentSize = CGSizeMake(320, 721 + productContentHight);
        
        
        [self.mainScrollView setContentOffset:CGPointMake(0 ,300)];
        //        [self.mainScrollView scrollRectToVisible:CGRectMake(0, productContentHight+200, 320, productContentHight) animated:YES];
        [self.inforImageView setImage:[UIImage imageNamed:@"Products-in-detail-open.png"]];
        switchFlag = !switchFlag;
    }
    
    
}
//分享按钮点击
- (IBAction)shareBtnAct:(id)sender
{
    //    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    //    if (singletonClass.currentUserId == nil) {
    //        //用户没有登录
    //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"每天分享一次可以获得积分，请先登录您的账户吧。" delegate:self cancelButtonTitle:nil otherButtonTitles:@"登录", @"不登录",nil];
    //        [alert show];
    //    }
    //    else
    //    {
    //        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    //        view.backgroundColor = [UIColor darkGrayColor];
    //        view.alpha = 0.5;
    //        view.tag = 100;
    //        [self.view addSubview:view];
    //        [self.view bringSubviewToFront:self.shareView];
    //        [UIView beginAnimations:nil context:nil];
    //        [UIView setAnimationDuration:0.2];
    //        self.shareView.frame = CGRectMake(0, self.view.frame.size.height - self.shareView.frame.size.height, self.shareView.frame.size.width, self.shareView.frame.size.height);
    //        [UIView commitAnimations];
    //    }
    NSMutableArray *mutableARR = [[NSMutableArray alloc]init];
    [mutableARR addObject:nil];
}
#pragma mark UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 200) {
        return;
    }
    if (alertView.tag == 101) {
        if (buttonIndex == 1) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[WXApi getWXAppInstallUrl]]];
        }
    }
    if (buttonIndex == 0) {
        UIView *view = [self.view viewWithTag:100];
        [view removeFromSuperview];
        self.shareView.frame = CGRectMake(0, self.view.frame.size.height , self.shareView.frame.size.width, self.shareView.frame.size.height);
        [WXCommonViewClass goToLogin:self.navigationController];
        
    }else if (buttonIndex == 1){
        //不登录
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        view.backgroundColor = [UIColor darkGrayColor];
        view.alpha = 0.5;
        view.tag = 100;
        [self.view addSubview:view];
        [self.view bringSubviewToFront:self.shareView];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2];
        self.shareView.frame = CGRectMake(0, self.view.frame.size.height - self.shareView.frame.size.height, self.shareView.frame.size.width, self.shareView.frame.size.height);
        [UIView commitAnimations];
    }
}

#pragma mark shareBtn Click
- (IBAction)shareCancelBtnAct:(id)sender
{
    UIView *view = [self.view viewWithTag:100];
    [view removeFromSuperview];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    self.shareView.frame = CGRectMake(0, self.view.frame.size.height , self.shareView.frame.size.width, self.shareView.frame.size.height);
    [UIView commitAnimations];
}

- (IBAction)goodEvoluBtnAct:(id)sender {
    FYReviewViewController * reviewVC= [[FYReviewViewController alloc] initWithNibName:@"FYReviewViewController" bundle:nil];
    reviewVC.productID = self.productId;
    reviewVC.evaluateValue = evaluateValue;
    reviewVC.evaluateValueCount = self.evaluateCountLabel.text;
    [self.navigationController pushViewController:reviewVC animated:YES];
}

- (IBAction)groupAboutBtnAct:(id)sender {
    WXGroupByingListViewController * groupByingListVC= [[WXGroupByingListViewController alloc] initWithNibName:@"WXGroupByingListViewController" bundle:nil];
    groupByingListVC.flagInt = 2;
    groupByingListVC.productId = self.productId;
    groupByingListVC.isHave = relateActive;
    [self.navigationController pushViewController:groupByingListVC animated:YES];
}



- (IBAction)btn_weixinGoodF:(id)sender
{
    if ([WXConfigDataControll getWeiXinAppKey] == nil || [[WXConfigDataControll getWeiXinAppKey] isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"此功能暂未开通" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alert.tag = 200;
        [alert show];
        return;
    }
    if ([WXApi isWXAppInstalled]) {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        UIView *view = [self.view viewWithTag:100];
        [view removeFromSuperview];
        self.shareView.frame = CGRectMake(0, self.view.frame.size.height , self.shareView.frame.size.width, self.shareView.frame.size.height);
        singletonClass.shareStr = @"weixin";
        singletonClass.shareWhere = 0;
        SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
        req.text = req.text = [NSString stringWithFormat:@"我在%@手机客户端看到了%@介绍,大家也来看看吧,下载链接是 http://apps.kstapp.com/downloads/%@.html",[WXConfigDataControll getAppName] ,self.productTitleLabel.text, singletonClass.shopid];
        req.bText = YES;
        [WXApi sendReq:req];
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"微信版本过低或未安装微信,现在去下载微信?" delegate:self cancelButtonTitle:@"暂不下载" otherButtonTitles:@"前往下载", nil];
        alert.tag = 101;
        [alert show];
    }
}
- (IBAction)btn_weixinFquan:(id)sender
{
    if ([WXConfigDataControll getWeiXinAppKey] == nil || [[WXConfigDataControll getWeiXinAppKey] isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"此功能暂未开通" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alert.tag = 200;
        [alert show];
        return;
    }
    if ([WXApi isWXAppInstalled]) {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        UIView *view = [self.view viewWithTag:100];
        [view removeFromSuperview];
        self.shareView.frame = CGRectMake(0, self.view.frame.size.height , self.shareView.frame.size.width, self.shareView.frame.size.height);
        singletonClass.shareStr = @"weixin";
        singletonClass.shareWhere = 0;
        SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
        req.text = req.text = [NSString stringWithFormat:@"我在%@手机客户端看到了%@介绍,大家也来看看吧,下载链接是 http://apps.kstapp.com/downloads/%@.html",[WXConfigDataControll getAppName] ,self.productTitleLabel.text, singletonClass.shopid];
        req.bText = YES;
        req.scene = WXSceneTimeline;
        [WXApi sendReq:req];
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"微信版本过低或未安装微信,现在去下载微信?" delegate:self cancelButtonTitle:@"暂不下载" otherButtonTitles:@"前往下载", nil];
        alert.tag = 101;
        [alert show];
    }
    
}

- (IBAction)btn_shareSina:(id)sender
{
    if ([WXConfigDataControll getSinaAppKey] == nil || [[WXConfigDataControll getSinaAppKey] isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"此功能暂未开通" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alert.tag = 200;
        [alert show];
        return;
    }
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    UIView *view = [self.view viewWithTag:100];
    [view removeFromSuperview];
    self.shareView.frame = CGRectMake(0, self.view.frame.size.height , self.shareView.frame.size.width, self.shareView.frame.size.height);
    singletonClass.shareStr = @"sina";
    singletonClass.shareWhere = 1;
    WBSendMessageToWeiboRequest *request = [WBSendMessageToWeiboRequest requestWithMessage:[self messageToShare]];
    request.userInfo = @{@"ShareMessageFrom": @"SendMessageToWeiboViewController",
                         @"Other_Info_1": [NSNumber numberWithInt:123],
                         @"Other_Info_2": @[@"obj1", @"obj2"],
                         @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
    [WeiboSDK sendRequest:request];
    
}
//处理新浪微博数据
- (WBMessageObject *)messageToShare
{
    WBMessageObject *message = [WBMessageObject message];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    message.text = [NSString stringWithFormat:@"我在%@手机客户端看到了%@介绍,大家也来看看吧,下载链接是 http://apps.kstapp.com/downloads/%@.html",[WXConfigDataControll getAppName] ,self.productTitleLabel.text, singletonClass.shopid];
    return message;
}
- (IBAction)btn_shareTX:(id)sender
{
    if ([WXConfigDataControll getQQAppKey] == nil || [[WXConfigDataControll getQQAppKey] isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"此功能暂未开通" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alert.tag = 200;
        [alert show];
        return;
    }
    FYShareViewController *shareVC = [[FYShareViewController alloc] initWithNibName:@"FYShareViewController" bundle:nil];
    shareVC.pushVC = self;
    shareVC.titleText = self.productTitleLabel.text;
    [self.navigationController pushViewController:shareVC animated:YES];
    
}

@end
