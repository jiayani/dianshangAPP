//
//  XYGoodsInforViewController.h
//  DianShangApp
//
//  Created by wa on 14-5-28.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomPageControl.h"
#import "XYLoadingView.h"
#import "MBProgressHUD.h"

//@protocol XYGoodsInforDelegate <NSObject>
//- (void) sendTextContent:(int)type;
//@end
@interface XYGoodsInforViewController : UIViewController<UIScrollViewDelegate,UIWebViewDelegate, UIAlertViewDelegate,MBProgressHUDDelegate>
{
    NSMutableArray * tabArr;
    NSDictionary * productDic;
    float evaluateValue;
    XYLoadingView * loadingView;
    BOOL relateActive;
    MBProgressHUD *progressHUD;
}

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *inforScrollView;
@property (weak, nonatomic) IBOutlet CustomPageControl *customPageControl;
@property (weak, nonatomic) IBOutlet UIWebView *inforWebview;
@property (weak, nonatomic) IBOutlet UIButton *collectBtn;
@property (weak, nonatomic) IBOutlet UIImageView *inforImageView;
@property (weak, nonatomic) IBOutlet UILabel *productTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *originPriceLabel;
@property(strong,nonatomic)NSString *productId;
@property(weak,nonatomic)NSArray *inforArray;
@property(retain,nonatomic)UIImageView *firstAdImageView;
@property (weak, nonatomic) IBOutlet UIView *shareView;
@property(retain,nonatomic)UIImageView *lastAdImageView;
@property (weak, nonatomic) IBOutlet UILabel *relateActivityCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *consultCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *shopCartBtn;
@property (weak, nonatomic) IBOutlet UILabel *evaluateCountLabel;
- (IBAction)collectBtnAct:(id)sender;
- (IBAction)backBtnAct:(id)sender;
- (IBAction)goodSpeakBtnAct:(id)sender;
- (IBAction)goBuyBtnAct:(id)sender;
- (IBAction)goShopCartBtnAct:(id)sender;
- (IBAction)shopCartBtnAct:(id)sender;
- (IBAction)inforBtnAct:(id)sender;
- (IBAction)shareBtnAct:(id)sender;
- (IBAction)shareCancelBtnAct:(id)sender;
- (IBAction)btn_weixinGoodF:(id)sender;
- (IBAction)btn_weixinFquan:(id)sender;
- (IBAction)btn_shareSina:(id)sender;
- (IBAction)btn_shareTX:(id)sender;


- (IBAction)goodEvoluBtnAct:(id)sender;
- (IBAction)groupAboutBtnAct:(id)sender;
//@property (nonatomic, assign) id<XYGoodsInforDelegate,NSObject> delegate;
@end
