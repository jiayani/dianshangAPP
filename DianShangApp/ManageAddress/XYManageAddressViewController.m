//
//  XYManageAddressViewController.m
//  DianShangApp
//
//  Created by wa on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XYManageAddressViewController.h"
#import "WXCommonViewClass.h"
#import "XYAddAdressViewController.h"
#import "XY_Cloud_GetAddress.h"
#import "XY_Cloud_SyncContactAddress.h"
@interface XYManageAddressViewController ()

@end

@implementation XYManageAddressViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"收货地址管理", @"收货地址管理");
        [WXCommonViewClass hideTabBar:self isHiden:YES];
        /*设置导航返回按钮*/
        UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
        [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
        self.navigationItem.leftBarButtonItem = leftBarButtonItem;
        
        UIButton * cancleBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:NSLocalizedString(@"添加", @"收货地址管理/添加")];
        [cancleBtn addTarget:self action:@selector(addBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:cancleBtn];
        self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    deleteContactAddIdArr = [[NSMutableArray alloc]init];
    localStoArr = [[NSMutableArray alloc]init];
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
    [self requestData];
    [self.mainTableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [loadingView removeFromSuperview];
    [noDataView removeFromSuperview];
    [[XY_Cloud_GetAddress share] setDelegate:nil];
    if (localStoArr.count == 0) {
        defaultContactAddId = -1;
    }
    else {
        for (int i = 0; i < [localStoArr count]; i++) {
            if ([[[localStoArr objectAtIndex:i] objectForKey:@"isDefault"] isEqualToString:@"1"]) {
                defaultContactAddId = [[[localStoArr objectAtIndex:i] objectForKey:@"contactAddID"] integerValue];
                break;
            }
            else {
                defaultContactAddId = -1;
            }
        }
    }
    if (deleteContactAddIdArr.count == 0) {
        deleteContactAddId = @"";
    }
    else {
        deleteContactAddId = [deleteContactAddIdArr componentsJoinedByString:@","];
    }
    NSString * postfixUrl = [NSString stringWithFormat:@"defaultContactAddId=%i&deleteContactAddId=%@",defaultContactAddId,deleteContactAddId];
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
    [[XY_Cloud_SyncContactAddress share] setDelegate:(id)self];
    [[XY_Cloud_SyncContactAddress share] SyncContactAddress:postfixUrl];
}

-(void)syncContactAndAdressSuccess:(NSDictionary *)data
{
    [[XY_Cloud_SyncContactAddress share] setDelegate:nil];
    [loadingView removeFromSuperview];
}
-(void)syncContactAndAdressFailed:(NSString*)errMsg
{
    [[XY_Cloud_SyncContactAddress share] setDelegate:nil];
     [loadingView removeFromSuperview];
}
- (void)requestData
{
    [[XY_Cloud_GetAddress share] setDelegate:(id)self];
    [[XY_Cloud_GetAddress share] getContactAndAddress];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getContactAndAdressSuccess:(NSDictionary*)data
{
    [loadingView removeFromSuperview];
    localStoArr = [[NSMutableArray alloc]initWithArray:[data objectForKey:@"contactAddress"]];
    [self.mainTableView reloadData];
}
-(void)getContactAndAdressFailed:(NSString*)errMsg
{
    [loadingView removeFromSuperview];
    [noDataView removeFromSuperview];
    CGRect rect;
    if (iPhone5) {
        rect = CGRectMake(0, 0, 320, 568);
    }
    else
    {
        rect = CGRectMake(0, 0, 320, 480);
    }
    noDataView =[[XYNoDataView alloc]initWithFrame:rect];
    noDataView.megUpLabel.text = @"暂时没有数据";
    noDataView.megDownLabel.text = @"先去其他页面看看吧";
    [self.view addSubview:noDataView];
}
//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addBtnPressed:(id)sender {
    
    XYAddAdressViewController * xyAddAdressVC= [[XYAddAdressViewController alloc] initWithNibName:@"XYAddAdressViewController" bundle:nil];
    [self.navigationController pushViewController:xyAddAdressVC animated:YES];
    
}

#pragma mark UITableViewDataSource
- (CGFloat) tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell = (XYManageAddressCell *)[[[NSBundle  mainBundle]  loadNibNamed:@"XYManageAddressCell" owner:self options:nil]  lastObject];
    cell.userTec.text = [NSString stringWithFormat:@"%@",[[localStoArr objectAtIndex:indexPath.row]objectForKey:@"contactName"]];
    
    cell.cityTec.text = [NSString stringWithFormat:@"%@%@%@",[[localStoArr objectAtIndex:indexPath.row]objectForKey:@"province"], [[localStoArr objectAtIndex:indexPath.row]objectForKey:@"city"],[[localStoArr objectAtIndex:indexPath.row]objectForKey:@"district"]];
    
    cell.streetTec.text = [[localStoArr objectAtIndex:indexPath.row]objectForKey:@"addressName"];
    return 76.0f + [self size:cell.userTec].height + [self size:cell.cityTec].height + [self size:cell.streetTec].height;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    return localStoArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CommentCellIdentifier = @"Cell";
    cell = (XYManageAddressCell*)[tableView dequeueReusableCellWithIdentifier:CommentCellIdentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"XYManageAddressCell" owner:self options:nil] lastObject];
    }
    [cell.clearView setHidden:YES];
    cell.addressDefault.tag = [indexPath row];
    [cell.addressDefault addTarget:self action:@selector(toChange:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.deleteBtn setHidden:NO];
    cell.deleteBtn.tag = [indexPath row];
    [cell.deleteBtn addTarget:self action:@selector(toDelete:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.userTec.text = [NSString stringWithFormat:@"%@",[[localStoArr objectAtIndex:indexPath.row]objectForKey:@"contactName"]];
    
    cell.cityTec.text = [NSString stringWithFormat:@"%@%@%@",[[localStoArr objectAtIndex:indexPath.row]objectForKey:@"province"], [[localStoArr objectAtIndex:indexPath.row]objectForKey:@"city"],[[localStoArr objectAtIndex:indexPath.row]objectForKey:@"district"]];
    
    cell.streetTec.text = [[localStoArr objectAtIndex:indexPath.row]objectForKey:@"addressName"];
    int anayFlag = [[[localStoArr objectAtIndex:indexPath.row]objectForKey:@"isDefault"] integerValue];

    cell.phoneTec.text = [[localStoArr objectAtIndex:indexPath.row]objectForKey:@"contactPhone"];
    
    CGRect rect;
    
    rect = cell.userTec.frame;
    rect.size.height += [self size:cell.userTec].height;
    [cell.userTec setFrame:rect];
    
    rect = cell.cityTec.frame;
    rect.origin.y += [self size:cell.userTec].height;
    [cell.cityTec setFrame:rect];
    
    rect = cell.cityTec.frame;
    rect.size.height += [self size:cell.cityTec].height;
    [cell.cityTec setFrame:rect];
    
    rect = cell.streetTec.frame;
    rect.origin.y += [self size:cell.cityTec].height + [self size:cell.userTec].height;
    [cell.streetTec setFrame:rect];
    
    rect = cell.streetTec.frame;
    rect.size.height += [self size:cell.streetTec].height;
    [cell.streetTec setFrame:rect];
    
    rect = cell.changeImageView.frame;
    rect.size.height += [self size:cell.userTec].height + [self size:cell.cityTec].height + [self size:cell.streetTec].height;
    [cell.changeImageView setFrame:rect];
    
    rect = cell.bottomImagView.frame;
    rect.origin.y += [self size:cell.userTec].height + [self size:cell.cityTec].height + [self size:cell.streetTec].height;
    [cell.bottomImagView setFrame:rect];

    CGSize textSize = [cell.userTec.text sizeWithFont:cell.userTec.font];
    if (textSize.width < 154) {
        CGFloat strikeWidth = textSize.width;
        CGRect frame = cell.userTec.frame;
        frame.size.width = strikeWidth;
        cell.userTec.frame = frame;
        CGRect phoneFrame = cell.phoneTec.frame;
        phoneFrame.origin.x = frame.origin.x + strikeWidth + 5.0f;
        phoneFrame.origin.y += 1.0f;
        cell.phoneTec.frame = phoneFrame;
    }
    
    
    cell.backgroundColor = [UIColor clearColor];
    if (anayFlag == 1) {
        cell.defaultText.text = @"默认地址";
        [cell.addressDefault setImage:[UIImage imageNamed:@"addDefault.png"] forState:UIControlStateNormal];
        [cell.deleteBtn setHidden:YES];
        [cell.clearView setHidden:NO];
        }

    return cell;
}

-(CGSize)size:(UILabel * )label{
    CGSize size = [label.text sizeWithFont:label.font
                         constrainedToSize:CGSizeMake(label.frame.size.width, 10000)];
    return size;
}

- (void)toChange:(id)sender{
    NSInteger index  = [sender tag];
    int flag = [[[localStoArr objectAtIndex:index]objectForKey:@"isDefault"]integerValue];
    //之前为选中 现在未选中
    if(flag == 1){
        //当前置为未选中
        
        [[localStoArr objectAtIndex:index] setObject:@"0" forKey:@"isDefault"];
        
    }else{
        
        for(int i=0;i<localStoArr.count;i++){
            [[localStoArr objectAtIndex:i] setObject:@"0" forKey:@"isDefault"];
            
        }
        NSMutableDictionary * commonObjectDic = [NSMutableDictionary dictionaryWithDictionary:[localStoArr objectAtIndex:index]];
        [commonObjectDic setObject:@"1" forKey:@"isDefault"];
        [localStoArr replaceObjectAtIndex:index withObject:commonObjectDic];
        
    }
    [self.mainTableView reloadData];
}

- (void)toDelete:(id)sender{
    globleIndex = [sender tag];
    
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"提示"
                                                         message:@"确实要删除吗"
                                                        delegate:self
                                               cancelButtonTitle:@"取消"
                                               otherButtonTitles:@"确定",nil];
    alertView.tag = 1000;
    [alertView show];
}

//无网络连接和意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[XY_Cloud_GetAddress share] setDelegate:nil];
    [[XY_Cloud_SyncContactAddress share] setDelegate:nil];
    //小圈停止运动
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
    
}

#pragma mark -UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
if (alertView.tag == 1000) {
    if (buttonIndex == 1) {
        [deleteContactAddIdArr addObject:[[localStoArr objectAtIndex:globleIndex]objectForKey:@"contactAddID"]];
        [localStoArr removeObjectAtIndex:globleIndex];
        [self.mainTableView reloadData];
    }
}
}
@end
