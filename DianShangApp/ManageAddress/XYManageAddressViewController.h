//
//  XYManageAddressViewController.h
//  DianShangApp
//
//  Created by wa on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XYManageAddressCell.h"
#import "XYLoadingView.h"
#import "XYNoDataView.h"
#import "WX_Cloud_Delegate.h"
@interface XYManageAddressViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,WX_Cloud_Delegate>
{
    XYManageAddressCell * cell;
    NSMutableArray * localStoArr;
    NSInteger globleIndex;
    int defaultContactAddId;
    NSString * deleteContactAddId;
    NSMutableArray * deleteContactAddIdArr;
    XYLoadingView * loadingView;
    XYNoDataView * noDataView;
}
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;

@end
