//
//  XYManageAddressCell.h
//  DianShangApp
//
//  Created by wa on 14-6-19.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XYManageAddressCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *addressDefault;
@property (weak, nonatomic) IBOutlet UILabel *defaultText;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIImageView *changeImageView;
@property (weak, nonatomic) IBOutlet UILabel *userTec;
@property (weak, nonatomic) IBOutlet UILabel *cityTec;
@property (weak, nonatomic) IBOutlet UILabel *streetTec;
@property (weak, nonatomic) IBOutlet UILabel *phoneTec;
@property (weak, nonatomic) IBOutlet UIImageView *bottomImagView;
@property (weak, nonatomic) IBOutlet UIView *clearView;


@end
