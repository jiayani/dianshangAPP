//
//  AppDelegate.m
//  DianShangApp
//
//  Created by 霞 王 on 14-4-22.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "AppDelegate.h"
#import "WXTabBarViewController.h"
#import "WXUserDatabaseClass.h"
#import "WXStarViewController.h"
#import "WXConfigDataControll.h"
#import "WXSearchViewController.h"
#import "WXPersonalCenterViewController.h"
#import "WXChoiceProductsViewController.h"
#import "FYCreditsGiftViewController.h"
#import "WXShoppingCartViewController.h"
#import "XYSelectedGoodsViewController.h"
#import "CustomNavigationController.h"
#import "XYSearchGoodsViewController.h"
#import "XYManageAddressViewController.h"
#import "FYPayViewController.h"
#import "XYGoodsDisplayViewController.h"
#import "WXUserLoginViewController.h"
#import "XYGoodsInforViewController.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_SendDeviceToken.h"
#import "WX_Cloud_UserLogin.h"
#import "UserInfo.h"
#import "WXCommonSingletonClass.h"
#import "WXCommonViewClass.h"
#import "FYShareDelegate.h"
#import "FYShareViewController.h"
#import "FY_Cloud_getPhone.h"
#import "WX_Cloud_thirdPartyLogging_syncUserInfo.h"
// Define your AppKey & AppSecret here to eliminate the errors
/*支付宝相关*/
#import "AlixPay.h"
#import "AlixPayResult.h"
#import "DataVerifier.h"
#import <sys/utsname.h>
@interface AppDelegate ()
- (void)parseURL:(NSURL *)url application:(UIApplication *)application;
@end

NSString *kSinaAppKey ;
NSString *kSinaAppSecret;

/*
 * Todo: 请填写您的应用的appkey和appsecret
 */
NSString *oauthAppKey ;
NSString *oauthAppSecret ;

/*
 * Todo：请正确填写您的应用网址，否则将导致授权失败
 */
NSString *redirect_uri ;

@implementation AppDelegate


@synthesize window;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize wxTabBarViewController;
@synthesize pushNotificationKey;
@synthesize versionDictionary;
@synthesize currentUser;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
   // [OneAPM setHost:@"10.128.17.41:8081"];
    [OneAPM startWithApplicationToken:@"AE894C04F94F340BD71CCE484FA1561D01"
     withSecurity:YES];
    [OneAPM printLog:YES];
    

    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    //把配置文件中的信息保存到NSUserDefaults中
    [WXConfigDataControll saveConfigInfoToDefualt];
    [self configGlobalVariable];        //执行配置全局变量
    [self setNavigation];//设置导航
    //begin:判断当前用户密码是否正确 ，
    currentUser = [WXUserDatabaseClass getCurrentUser];
    if (currentUser != nil) {
        [[WX_Cloud_UserLogin share] setDelegate:(id)self];
        [[WX_Cloud_UserLogin share] userLoginWithUserName:currentUser.userEmail UserPassword:currentUser.userPassword];
    }
    [[FY_Cloud_getPhone share] getPhone];
    WXStarViewController *starContr = [[WXStarViewController alloc]init];
    self.window.rootViewController = starContr;
    [self.window makeKeyAndVisible];
    
    [self RegisteringPushNotificaiotn:launchOptions]; //注册推送
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //得到新的设备口令牌
    NSString *newDeviceToken = [NSString stringWithFormat:@"%@",deviceToken];
    if (newDeviceToken.length == 0) {
        return;
    }
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"\\<\\>"];
    newDeviceToken = [newDeviceToken stringByTrimmingCharactersInSet:set];
    newDeviceToken = [newDeviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.deviceTokenStr = newDeviceToken;
    //调用通讯代码把token发给服务器
    //token
    [WX_Cloud_SendDeviceToken sendDeviceToken:newDeviceToken];
    
    
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    
    NSString *messageStr = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    //    NSString *appName = [WXConfigDataControll getAppName];
    if (messageStr != NULL) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"通知"
                                                        message:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]
                                                       delegate:self
                                              cancelButtonTitle:@"关闭"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    
} 

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    //begin:判断当前用户密码是否正确
    currentUser = [WXUserDatabaseClass getCurrentUser];
    if (currentUser != nil) {
        [[WX_Cloud_UserLogin share] setDelegate:(id)self];
        [[WX_Cloud_UserLogin share] userLoginWithUserName:currentUser.userEmail UserPassword:currentUser.userPassword];
    }
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 

            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"DianShangApp" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"DianShangApp.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
//        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma UINavigationControllerDelegate Method

/* 通过UINavigationControllerDelegate的代理方法来实现tabBar的隐藏和显示功能 */
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{

    if (viewController.hidesBottomBarWhenPushed) {
        [wxTabBarViewController hidesTabBar:YES animated:NO];
    }else
    {
        [wxTabBarViewController hidesTabBar:NO animated:NO];
    }
}

#pragma mark- 
#pragma mark - 注册推送
//注册推送
- (void)RegisteringPushNotificaiotn:(NSDictionary *)launchOptions
{
    
    [[UIApplication sharedApplication]
     
     registerForRemoteNotificationTypes:
     
     (UIRemoteNotificationTypeAlert |
      
      UIRemoteNotificationTypeBadge |
      
      UIRemoteNotificationTypeSound)];
    
    //判断程序是不是由推送服务完成的
    if (launchOptions) {
        self.pushNotificationKey = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        
    }
    
}
#pragma mark- Wangxia Methods
//设置导航的主页
- (void)setNavigation{
    

    //动态得到导航上标题的字体颜色
    self.titleFontColorOfNav = [WXConfigDataControll getTitleFontColorOfNav];

    //动态得到导航上标题的字体大小
    self.titleFontSizeOfNav = [WXConfigDataControll getTitleFontSizeOfNav];
    
    //动态得到菜单字体大小
    NSString *fontSizeOfMenu = [WXConfigDataControll getFontSizeOfMenu];
    UIColor *fontColorOfMenu = [WXConfigDataControll getFontColorOfMenu];
    UIColor *fontColorOfSelectedMenu = [WXConfigDataControll getFontColorOfSelectedMenu];
    
    self.fontStyleOfMenu = [[NSArray alloc]initWithObjects:fontSizeOfMenu,fontColorOfMenu, fontColorOfSelectedMenu,nil];
    
   
    //添加导航设置
    FYCreditsGiftViewController *firstMenu = [[FYCreditsGiftViewController alloc]init];
    XYSearchGoodsViewController *secondMenu = [[XYSearchGoodsViewController alloc]init];

    XYSelectedGoodsViewController *thirdMenu = [[XYSelectedGoodsViewController alloc]init];

    WXShoppingCartViewController *fourthMenu = [[WXShoppingCartViewController alloc]init];

   
    //在第二个界面，添加了导航条
    CustomNavigationController *menu03Nav = [[CustomNavigationController alloc]initWithRootViewController:thirdMenu];
    menu03Nav.delegate = self;
    
    CustomNavigationController *menu01Nav = [[CustomNavigationController alloc]initWithRootViewController:firstMenu ];
    menu01Nav.delegate = self;
    
    
    CustomNavigationController *menu02Nav = [[CustomNavigationController alloc]initWithRootViewController:secondMenu];
    menu02Nav.delegate = self;
    CustomNavigationController *menu04Nav = [[CustomNavigationController alloc]initWithRootViewController:fourthMenu];
    menu04Nav.delegate = self;
    

    WXPersonalCenterViewController *fifthMenu = [[WXPersonalCenterViewController alloc]init];
    CustomNavigationController *menu05Nav = [[CustomNavigationController alloc]initWithRootViewController:fifthMenu];

    menu05Nav.delegate = self;
    
    
//    menu03Nav.tabTag = 0;
//    menu01Nav.tabTag = 1;
//    menu02Nav.tabTag = 2;
//    menu04Nav.tabTag = 3;
//    menu05Nav.tabTag = 4;
    
    NSArray *ctrlArr = [NSArray arrayWithObjects:menu03Nav,menu01Nav,menu02Nav,menu04Nav,menu05Nav,nil];
    
    //利用字典机制，将三种状态的图片进行了定制
    NSMutableDictionary *imgDic1 = [NSMutableDictionary dictionaryWithCapacity:3];
    [imgDic1 setObject:[UIImage imageNamed:@"menu01Off.png"] forKey:@"Default"];
    [imgDic1 setObject:[UIImage imageNamed:@"menu01On.png"] forKey:@"Seleted"];
    [imgDic1 setObject:[UIImage imageNamed:@"menu01On.png"] forKey:@"Highlightde"];
    
    NSMutableDictionary *imgDic2 = [NSMutableDictionary dictionaryWithCapacity:3];
    [imgDic2 setObject:[UIImage imageNamed:@"menu02Off.png"] forKey:@"Default"];
    [imgDic2 setObject:[UIImage imageNamed:@"menu02On.png"] forKey:@"Seleted"];
    [imgDic2 setObject:[UIImage imageNamed:@"menu02On.png"] forKey:@"Highlightde"];
    
    NSMutableDictionary *imgDic3 = [NSMutableDictionary dictionaryWithCapacity:3];
    [imgDic3 setObject:[UIImage imageNamed:@"menu03Off.png"] forKey:@"Default"];
    [imgDic3 setObject:[UIImage imageNamed:@"menu03On.png"] forKey:@"Seleted"];
    [imgDic3 setObject:[UIImage imageNamed:@"menu03On.png"] forKey:@"Highlightde"];
    
    NSMutableDictionary *imgDic4 = [NSMutableDictionary dictionaryWithCapacity:3];
    [imgDic4 setObject:[UIImage imageNamed:@"menu04Off.png"] forKey:@"Default"];
    [imgDic4 setObject:[UIImage imageNamed:@"menu04On.png"] forKey:@"Seleted"];
    [imgDic4 setObject:[UIImage imageNamed:@"menu04On.png"] forKey:@"Highlightde"];
    
    NSMutableDictionary *imgDic5 = [NSMutableDictionary dictionaryWithCapacity:3];
    [imgDic5 setObject:[UIImage imageNamed:@"menu05Off.png"] forKey:@"Default"];
    [imgDic5 setObject:[UIImage imageNamed:@"menu05On.png"] forKey:@"Seleted"];
    [imgDic5 setObject:[UIImage imageNamed:@"menu05On.png"] forKey:@"Highlightde"];
    
    NSArray *imgArr = [NSArray arrayWithObjects:imgDic1,imgDic2,imgDic3,imgDic4,imgDic5, nil];
    
    NSArray *titleArray = [[NSArray alloc]initWithObjects:
                           NSLocalizedString(@"首页",@"toolBar/menu01"),
                           NSLocalizedString(@"积分礼品",@"toolBar/menu02"),
                           NSLocalizedString(@"搜索",@"toolBar/menu03"),
                           NSLocalizedString(@"购物车",@"toolBar/menu04"),
                           NSLocalizedString(@"个人中心",@"toolBar/menu05"),
                           nil];
    
    wxTabBarViewController = [[WXTabBarViewController alloc] initWithViewControllers:ctrlArr imageArray:imgArr titleArray:titleArray fontStyleOfMenu:self.fontStyleOfMenu];
    [wxTabBarViewController.tabBar setBackgroundImage:[UIImage imageNamed:@"tabbarbg.png"]];
    
    //设置标签栏透明
    [self.wxTabBarViewController setTabBarTransparent:YES];
}
+ (AppDelegate *)delegate {
	return (AppDelegate *)[[UIApplication sharedApplication]delegate];
}
- (UIWindow *)getCurrentWindow{
    return self.window;
}
- (WXTabBarViewController *)getWXBarViewController{
    return wxTabBarViewController;
}

- (void)updateVersion{
    //新浪微博
    // Define your AppKey & AppSecret here to eliminate the errors
    kSinaAppKey = [WXConfigDataControll getSinaAppKey];
    kSinaAppSecret = [WXConfigDataControll getSinaAppSecret];
    
    /*
     * Todo: 请填写您的应用的appkey和appsecret
     */
    oauthAppKey = [WXConfigDataControll getQQAppKey];
    oauthAppSecret = [WXConfigDataControll getQQAppSecret];
    
    /*
     * Todo：请正确填写您的应用网址，否则将导致授权失败
     */
    redirect_uri = [WXConfigDataControll getRedirectUrl];
    
}
/*配置全局变量
 */
-(void)configGlobalVariable
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.version = [WXConfigDataControll getCuurentVersion];
    singletonClass.shopid = [WXConfigDataControll getShopID];
    singletonClass.serviceURL = [WXConfigDataControll getServiceURLStr];
    singletonClass.currentUserId = [WXUserDatabaseClass getCurrentUserKey];
    singletonClass.getImageServiceRUL = [WXConfigDataControll getImageServiceRUL];
    singletonClass.htmlServiceRUL = [WXConfigDataControll getStaticHtmlUrlStr];
    singletonClass.imageServiceURLOfWooGift = [WXConfigDataControll getImageServiceURLOfWooGift];
    [self updateVersion];
}

#pragma mark -userMethod
//启动第三方APP后回调
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    FYShareDelegate *share = [[FYShareDelegate alloc] init];
    if ([singletonClass.shareStr isEqualToString:@"weixin"]) {
        singletonClass.shareStr = @"111";
        return  [WXApi handleOpenURL:url delegate:share];
    }else if([singletonClass.shareStr isEqualToString:@"sina"])
    {
        singletonClass.shareStr = @"111";
        return [WeiboSDK handleOpenURL:url delegate:share];
        
    }
    else if([singletonClass.shareStr isEqualToString:@"TX"])
    {
        singletonClass.shareStr = @"111";
        return [singletonClass.wbapi handleOpenURL:url];
    }
    else if([singletonClass.shareStr isEqualToString:@"QQ"])
    {
        return [TencentOAuth HandleOpenURL:url];
        singletonClass.shareStr = @"111";
    }
    else
    {
    [self parseURL:url application:application];
    }
	return YES;
}


//支付宝相关
- (void)parseURL:(NSURL *)url application:(UIApplication *)application {
	AlixPay *alixpay = [AlixPay shared];
	AlixPayResult *result = [alixpay handleOpenURL:url];
	if (result) {
		//是否支付成功
		if (9000 == result.statusCode) {
            if (self.orderDetailVC != nil) {
                [self.orderDetailVC initData];
            }
            if (self.myOrderVC != nil) {
                [self.myOrderVC getNewDataFromServer];
            }
			/*
			 *用公钥验证签名
			 */
			id<DataVerifier> verifier = CreateRSADataVerifier([[NSBundle mainBundle] objectForInfoDictionaryKey:@"RSA public key"]);
			if ([verifier verifyString:result.resultString withSign:result.signString]) {
				UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"提示"
																	 message:[self getMessageBy:result.statusCode]
																	delegate:self
														   cancelButtonTitle:@"确定"
														   otherButtonTitles:nil];
                
				alertView.tag = 1000;
                [alertView show];
			}//验签错误
			else {
				UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"提示"
																	 message:@"签名错误"
																	delegate:self
														   cancelButtonTitle:@"确定"
														   otherButtonTitles:nil];
				alertView.tag = 1000;
                [alertView show];
			}
		}
		//如果支付失败,可以通过result.statusCode查询错误码
		else {
			UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"提示"
																 message:result.statusMessage
																delegate:self
													   cancelButtonTitle:@"确定"
													   otherButtonTitles:nil];
			[alertView show];
		}
		
	}
}

- (NSString *)getMessageBy:(int)code{
    switch (code) {
        case 9000:
            return @"操作成功";
            break;
        case 4000:
            return @"系统异常";
            break;
        case 4001:
            return @"数据格式不正确";
            break;
        case 4003:
            return @"该用户绑定的支付宝账户被冻结或不允许支付";
            break;
        case 4004:
            return @"该用户已解除绑定";
            break;
        case 4005:
            return @"绑定失败或没有绑定";
            break;
        case 4006:
            return @"订单支付失败";
            break;
        case 4010:
            return @"重新绑定账户";
            break;
        case 6000:
            return @"支付服务正在进行升级操作";
            break;
        case 6001:
            return @"用户中途取消支付操作";
            break;
        case 7001:
            return @"网页支付失败";
            break;
        default:
            return @"";
            break;
    }
}
#pragma mark -UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 1000){
        if(buttonIndex == 0){
            if(_paySucessVC != nil){
                [_paySucessVC gotoPaySuccess];
            }
            else if (_orderDetailVC != nil){
                [_orderDetailVC gotoPaySuccess];
            }
            else if (_myOrderVC != nil){
                [_myOrderVC gotoPaySuccess];
            }
        }
    }
}

#pragma mark - 登录接口
//意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[WX_Cloud_UserLogin share] setDelegate:nil];
}
//用户登陆
-(void)userLoginSuccess:(NSMutableDictionary*)databaseDictionary{
    [[WX_Cloud_UserLogin share] setDelegate:nil];
    [databaseDictionary setObject:self.currentUser.userPassword forKey:@"password"];
    [WXUserDatabaseClass addUser:databaseDictionary];
}

-(void)userLoginFailed:(NSString*)errCode{
    //注销当前用户
    [[WX_Cloud_UserLogin share] setDelegate:nil];
    [WXUserDatabaseClass setCurrentUserKey:nil];
}
#pragma mark -  第三方登录后同步用户信息

-(void)syncGetUserInfoByThirdPartyLoggingSuccess:(NSMutableDictionary *)data{
    [[WX_Cloud_thirdPartyLogging_syncUserInfo share] setDelegate:nil];
    [WXUserDatabaseClass addUser:data];
    
    WXCommonSingletonClass *singletionClass = [WXCommonSingletonClass share];
    singletionClass.isCloseFromBack = NO;//处于登录状态
    
}
-(void)syncGetUserInfoByThirdPartyLoggingFailed:(NSString*)errMsg{
    [[WX_Cloud_thirdPartyLogging_syncUserInfo share] setDelegate:nil];
    [WXUserDatabaseClass setCurrentUserKey:nil];
    
}

@end
