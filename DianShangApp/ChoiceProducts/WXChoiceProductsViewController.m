//
//  WXChoiceProductsViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-4-23.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXChoiceProductsViewController.h"

@interface WXChoiceProductsViewController ()

@end

@implementation WXChoiceProductsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
