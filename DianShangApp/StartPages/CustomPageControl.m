//
//  CustomPageControl.m
//  TianLvApp
//
//  Created by ????on 13-7-2.
//  Copyright (c) 2013Âπ????? All rights reserved.
//

#import "CustomPageControl.h"

@implementation CustomPageControl

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    activeImage = [UIImage imageNamed:@"dotCurrentPage.png"];
    inactiveImage = [UIImage imageNamed:@"dotPage.png"];
    
    return self;
}
- (void)updateDots
{
    if (iOS7) {
        [self ios7UpdateDots];
    }else{
        for (int i = 0; i< [self.subviews count]; i++) {
            UIImageView* dot = [self.subviews objectAtIndex:i];
            
            if (i == self.currentPage){
                dot.image = activeImage;
            }
            else{
                dot.image = inactiveImage;
            }
        }
    }
}
- (void)ios7UpdateDots{
    //ios7改变原点
    for (int i = 0; i< [self.subviews count]; i++) {
        UIView* dotView = [self.subviews objectAtIndex:i];
        UIImageView* dot = nil;
        for (UIView* subview in dotView.subviews)
        {
            if ([subview isKindOfClass:[UIImageView class]])
            {
                dot = (UIImageView*)subview;
                break;
            }
        }
        
        if (dot == nil)
        {
            dot = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, dotView.frame.size.width, dotView.frame.size.height)];
            [dotView addSubview:dot];
        }
        
        if (i == self.currentPage){
            dot.image = activeImage;
        }else{
            dot.image = inactiveImage;
        }
        [dotView addSubview:dot];
    }
    
}
- (void)setCurrentPage:(NSInteger)currentPage
{
    [super setCurrentPage:currentPage];
    [self updateDots];
}
@end


