//
//  CustomPageControl.h
//  TianLvApp
//
//  Created by 霞 王 on 13-7-2.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPageControl : UIPageControl{
    UIImage *activeImage;  //当前圆点图片
    UIImage *inactiveImage;//其它圆点图片
}


@end
