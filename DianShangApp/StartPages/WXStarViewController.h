//
//  WXStarViewController.h
//  TianLvApp
//
//  Created by 霞 王 on 13-6-21.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomPageControl.h"

@interface WXStarViewController : UIViewController<UIScrollViewDelegate>{
    int pageWidth;
    int myCurrentPage;
    int pageCount;  //指定滚动页面的个数
    NSString *newVersionDownLoadUrlStr;//新版本下载地址
    NSString *newVersionDownLoadUrlEnterpriseStr;//新版本企业下载地址

}
@property (strong, nonatomic) UIWindow *myWindow;
@property (nonatomic,strong) NSArray *guidArray;
@property (nonatomic,strong) NSString *startImageNameStr;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *startImageView;

@property (retain, nonatomic) IBOutlet UIView *guideView;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet CustomPageControl *myPageControl;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *myButton;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *clikToSkipLabel;

- (IBAction)goToMianPage:(id)sender;

/*处理版本更新方法*/
- (void)doNewVersion;

@end
