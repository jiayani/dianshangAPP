//
//  WXStarViewController.m
//  TianLvApp
//
//  Created by 霞 王 on 13-6-21.
//  Copyright (c) 2013年 霞 王. All rights reserved.
//

#import "WXStarViewController.h"
#import "WXTabBarViewController.h"
#import "AppDelegate.h"
#import "GuideAnimation.h"
#import "WX_Cloud_StartPage.h"
#import "WXConfigDataControll.h"
#import "WXCommonViewClass.h"
#import "WXCommonDataClass.h"
#import "XY_Cloud_VersionUpdate.h"
#import "WXCommonSingletonClass.h"
#import "FY_Cloud_isJiFenChange.h"
//友盟相关
#import "MobClick.h"
@interface WXStarViewController ()

@end

@implementation WXStarViewController
@synthesize myWindow;
@synthesize scrollView;
@synthesize startImageView;
@synthesize myPageControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    //进入主页后，从服务器端得到最新版本的信息
    [[XY_Cloud_VersionUpdate share] setDelegate:(id)self];
    [[XY_Cloud_VersionUpdate share] getCurrentVersion];
    
    pageWidth = self.view.frame.size.width;
    myCurrentPage = 0;
    self.clikToSkipLabel.text = NSLocalizedString(@"点击\n跳过", @"轮番页面/点击跳过");
    [self getGuidImage];//从数据库中读取数据
    UIImage *newImage = [UIImage imageWithContentsOfFile:self.startImageNameStr];
    if (newImage) {
        self.startImageView.image = newImage;
    }else{
        if (iPhone5) {
            self.startImageNameStr = @"startPage_iphone5.png";
        }else{
            self.startImageNameStr = @"startPage.png";
        }
        self.startImageView.image = [UIImage imageNamed:self.startImageNameStr];
    }
    
    pageCount = (int)self.guidArray.count;
    [self setScrollView];
    [self setPageControllerMethod];
    
    
    //启动App后，3秒钟后，自动跳转到动画指导页面
    [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(goToGuiidPage) userInfo:nil repeats:NO];
    
    //调用后台方法
    [[WX_Cloud_StartPage share] setDelegate:(id)self];
    [[WX_Cloud_StartPage share] loadingStartPageAndImage];
    
    [[FY_Cloud_isJiFenChange share] setDelegate:(id)self];
    [[FY_Cloud_isJiFenChange share] getIsChange];
    
}



-(void)getIsChangeSuccess:(NSDictionary *)data
{
    WXCommonSingletonClass *singleClass = [WXCommonSingletonClass share];
    if ([[data objectForKey:@"p_woo"] intValue] != 0) {
        singleClass.isChange = YES;
        singleClass.changeDic = [NSDictionary dictionaryWithDictionary:data];
    }else
    {
        singleClass.isChange = NO;
    }
}

-(void)getIsChangeFailed:(NSString*)errMsg
{
    WXCommonSingletonClass *singleClass = [WXCommonSingletonClass share];
    singleClass.isChange = NO;
}

- (void)viewDidUnload{
    
    [self setScrollView:nil];
    self.myPageControl = nil;
    [self setStartImageView:nil];
    [self setMyButton:nil];
    [self setClikToSkipLabel:nil];
    [super viewDidUnload];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[WX_Cloud_StartPage share] setDelegate:nil];
    [[XY_Cloud_VersionUpdate share] setDelegate:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UIScrollViewDelegate Methods
//scrollview的委托方法，当滚动时执行
- (void)scrollViewDidScroll:(UIScrollView *)sender {

    CGFloat contentOffsetX = self.scrollView.contentOffset.x;
    if(contentOffsetX > (pageCount - 1)*pageWidth + pageWidth/5){
        /*滑到最后一页时，再次滑动 滑到第一页  2013-09-02
        myCurrentPage = 0;
        [self.myPageControl setCurrentPage:myCurrentPage];//pagecontroll响应值的变化
        UIImageView *imageView = (UIImageView*)[[self.myPageControl subviews]objectAtIndex:0];
        UIButton *button = (UIButton*)[imageView.subviews objectAtIndex:0];
        [self tapBotAction:button];
         */
        [self goToMianPage:nil];
        
    }else{
        myCurrentPage = contentOffsetX / pageWidth;//通过滚动的偏移量来判断目前页面所对应的小白点
        [self.myPageControl setCurrentPage:myCurrentPage];//pagecontroll响应值的变化
    }
}

#pragma mark - All My Methods
/**wangxia
 *3秒钟后自动执行下面这个方法
 *执行动画指导页面
 *动画指导页面执行完之后进入主页面
 */
- (void)goToGuiidPage{
    self.view = self.guideView;
    
    
}
- (void)setPageControllerMethod{
    
    self.myPageControl.userInteractionEnabled = YES;
    
    self.myPageControl.backgroundColor = [UIColor clearColor];
 
    [self.myPageControl setNumberOfPages: pageCount];//指定页面个数
    
    [self.myPageControl setCurrentPage: myCurrentPage];//指定pagecontroll的值，默认选中的小白点（第一个）
    
    //添加委托方法，当点击小白点就执行此方法
    [self.myPageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
    int i = 0;
    for (UIImageView *imgV in [self.myPageControl subviews]) {
        
        imgV.userInteractionEnabled = YES;   //默认为NO
        UIButton *botButton = [UIButton buttonWithType:UIButtonTypeCustom];
        botButton.frame = imgV.bounds;
        botButton.tag = i;
        botButton.backgroundColor = [UIColor clearColor];
        [botButton addTarget:self action:@selector(tapBotAction:) forControlEvents:UIControlEventTouchUpInside];
        [imgV addSubview:botButton];
        i++;
    }
    
    
}
//pagecontroll的委托方法
- (IBAction)changePage:(id)sender {
    
    int page = (int)self.myPageControl.currentPage;//获取当前pagecontroll的值
    [self.scrollView setContentOffset:CGPointMake(pageWidth * page, 0) animated:NO];//根据pagecontroll的值来改变scrollview的滚动位置，以此切换到指定的页面

    
}
-(void)setScrollView{

    // 是否按页滚动
    self.scrollView.pagingEnabled = YES;
    self.scrollView.backgroundColor = [UIColor clearColor];
    // 隐藏滚动条
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    
    self.scrollView.canCancelContentTouches = NO;
    
    self.scrollView.clipsToBounds = YES;
    //设置委托
    self.scrollView.delegate = self;
    // 图片
    NSArray *array = self.guidArray;
    
    CGRect rect = self.scrollView.frame;
    rect = CGRectMake(0, 0, rect.size.width, rect.size.height);
    
    CGFloat width = rect.size.width;
    
    int i = 0;
    for (NSString *name in array)
    {
        NSString *defaultPicUrl;
        if (iPhone5) {
            defaultPicUrl = [NSString stringWithFormat:@"guide_photo%d_iphone5.png",i+1];
        }else{
            defaultPicUrl = [NSString stringWithFormat:@"guide_photo%d.png",i+1];
        }
        
        // 创建ImageView
        UIImage *newImage = [UIImage imageWithContentsOfFile:name];
        if (newImage == nil) {
            newImage = [UIImage imageNamed:defaultPicUrl];
        }
        UIImage *image = newImage;
                
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:rect];
        imageView.image = image;

        // 加入到ScrollView
        imageView.autoresizingMask =  UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self.scrollView addSubview:imageView];
        
        // 修改子视图位置
        CGRect frame = imageView.frame;
        frame.origin = CGPointMake(width*i, 0);
        imageView.frame = frame;
        
        i++;
    }
    // 设置ScrollView大小
    self.scrollView.contentSize = CGSizeMake(width*[array count],self.scrollView.frame.size.height);
    
}

- (void)tapBotAction:(id)sender{
    int index = (int)[(UIButton *)sender tag];
    [self.myPageControl setCurrentPage:index];
    [self changePage:nil];
    
}

/**wangxia 2013-7-3
 *单击跳转到主页面
 */
- (IBAction)goToMianPage:(id)sender {
    
    WXTabBarViewController *wxBarController = [[AppDelegate delegate] getWXBarViewController];
    self.myWindow = [[AppDelegate delegate] getCurrentWindow];
//    self.myWindow.rootViewController = nil;
    [self.myWindow addSubview:wxBarController.view];
    
    
    
}

/**wangxia
    设置默认本地轮播图片
 */
- (void)setDefaultGuidImages{
    if (iPhone5) {
        self.startImageNameStr = @"startPage_iphone5.png";
        
        self.guidArray = [[NSArray alloc]initWithObjects:@"guide_photo1_iphone5.png", @"guide_photo2_iphone5.png",@"guide_photo3_iphone5.png",@"guide_photo4.png_iphone5", nil];
    }else{
        self.startImageNameStr = @"startPage.png";
        
        self.guidArray = [[NSArray alloc]initWithObjects:@"guide_photo1.png", @"guide_photo2.png",@"guide_photo3.png",@"guide_photo4.png", nil];
    }
}
/*从数据库中读取轮番图
 *本地数据库中为空，读取默认的一张启动图片跟一张默认的轮番图片
 *
 */
- (void)getGuidImage{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    NSEntityDescription *entityDescription=[NSEntityDescription entityForName:@"GuideAnimation" inManagedObjectContext:context];
    NSFetchRequest *request=[[NSFetchRequest alloc]init];
   [request setEntity:entityDescription];
    NSError *error;
    [context unlock];
    NSArray *objects=[context executeFetchRequest:request error:&error];
    if (objects != nil && objects.count > 0) {
        GuideAnimation *oneGuid = [objects objectAtIndex:0];
        if (oneGuid != nil) {
            if (oneGuid.appStartPic != nil && oneGuid.appStartPic.length > 0) {
                
                self.startImageNameStr = [WXCommonDataClass getImageURL:oneGuid.appStartPic ];
            }else{
                if (iPhone5) {
                    self.startImageNameStr = @"startPage_iphone5.png";
                }else{
                    self.startImageNameStr = @"startPage.png";
                }
                
            }
            if (oneGuid.guideAnimationPic != nil && ((NSArray *)oneGuid.guideAnimationPic).count > 0) {
                NSMutableArray *tempMultArray = [[NSMutableArray alloc]init];
                for (NSString *tempStr1 in oneGuid.guideAnimationPic) {
                    [tempMultArray addObject:[WXCommonDataClass getImageURL:tempStr1]];
                }
                self.guidArray = tempMultArray;
            }else{
                [self setDefaultGuidImages];//设置默认轮播图
                
            }
        }else{
                        
            [self setDefaultGuidImages];//设置默认轮播图
        }
        

    }else{
        
        [self setDefaultGuidImages];//设置默认轮播图
    }
}
/*处理版本更新方法*/
- (void)doNewVersion{
    NSString *cuurentVersion = [WXConfigDataControll getCuurentVersion];
    NSDictionary *dic = [AppDelegate delegate].versionDictionary;
    NSString *titleStr,*updateContentStr,*updateContentStrEnterprise;
    UIAlertView *alert;
    BOOL myBool = NO;
    NSString *alertTitleStr,*alertMessageStr;
    if (dic != nil) {
        NSString *newVersionStr = [dic objectForKey:@"version"];
        NSString *newVersionStrEnterprise = [dic objectForKey:@"versionEnterprise"];
        newVersionDownLoadUrlStr = [dic objectForKey:@"updateUrl"];
        newVersionDownLoadUrlEnterpriseStr = [dic objectForKey:@"updateUrlEnterprise"];
        updateContentStr = [dic objectForKey:@"updateInfo"];
        updateContentStrEnterprise = [dic objectForKey:@"updateInfoEnterprise"];
        NSDictionary *dic1 = [[NSBundle mainBundle] infoDictionary]; //获取info－plist
        
        NSString *appName = [dic1 objectForKey:@"CFBundleIdentifier"]; //获取Bundle identifier
        
        if ([appName rangeOfString:@"kstapp"].location != NSNotFound) {
            if ([newVersionStr length] != 0 && [newVersionDownLoadUrlStr length] != 0) {
                    if ([WXCommonDataClass compareVersion:newVersionStr :cuurentVersion]) {
                        myBool = YES;
                        alertTitleStr = newVersionStr;
                        alertMessageStr = updateContentStr;
                        
                        
                    }
            }
        } else {
            if ([newVersionStrEnterprise length] != 0 && [newVersionDownLoadUrlEnterpriseStr length] != 0) {
                if ([WXCommonDataClass compareVersion:newVersionStrEnterprise :cuurentVersion]) {
                    myBool = YES;
                    alertTitleStr = newVersionStrEnterprise;
                    alertMessageStr = updateContentStrEnterprise;
                    
                }
            }

        }
    }
    if (myBool) {
        titleStr = NSLocalizedString(@"有新版本可用", @"有新版本可用");
        alertTitleStr = [NSString stringWithFormat:@"%@ %@",titleStr,alertTitleStr];
        alert = [[UIAlertView alloc]initWithTitle:alertTitleStr message:alertMessageStr delegate:self cancelButtonTitle:NSLocalizedString(@"以后再说", @"以后再说") otherButtonTitles:NSLocalizedString(@"立即升级", @"立即升级"), nil];
        alert.delegate = self;
        alert.tag = 100;

        [alert show];
    }
}



#pragma mark - WYDCloudDelegate


//意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [[WX_Cloud_StartPage share] setDelegate:nil];
    [[XY_Cloud_VersionUpdate share] setDelegate:nil];

}
-(void)syncSuccess:(id)database
{
    [[WX_Cloud_StartPage share] setDelegate:nil];
}
-(void)syncFailed:(NSString*)errCode
{
    [[WX_Cloud_StartPage share] setDelegate:nil];

}
#pragma  mark 版本更新的回调函数
-(void)versionUpdateSuccess:(NSArray *)data
{
    [[XY_Cloud_VersionUpdate share] setDelegate:nil];
    [[AppDelegate delegate] updateVersion];//设置版本更新的内容
    [self doNewVersion];
    //[self umengTrack];
}
-(void)versionUpdateFailed:(NSString*)errMsg
{
    [[XY_Cloud_VersionUpdate share] setDelegate:nil];
    
}

#pragma mark - 友盟相关函数
- (void)umengTrack {
    //    [MobClick setCrashReportEnabled:NO]; // 如果不需要捕捉异常，注释掉此行
    //    [MobClick setLogEnabled:YES];  // 打开友盟sdk调试，注意Release发布时需要注释掉此行,减少io消耗
    [MobClick setAppVersion:XcodeAppVersion]; //参数为NSString * 类型,自定义app版本信息，如果不设置，默认从CFBundleVersion里取
    //
    //    [MobClick startWithAppkey:UMENG_APPKEY reportPolicy:(ReportPolicy) REALTIME channelId:@"keshangtong100003"];
    
    [MobClick startWithAppkey:[WXConfigDataControll getUMengiPhoneSecret] reportPolicy:SEND_INTERVAL   channelId:[WXConfigDataControll getUMengChannelId]];
    //App Store
    //keshangtong100003
    //   reportPolicy为枚举类型,可以为 REALTIME, BATCH,SENDDAILY,SENDWIFIONLY几种
    //   channelId 为NSString * 类型，channelId 为nil或@""时,默认会被被当作@"App Store"渠道
    
    [MobClick checkUpdate];   //自动更新检查, 如果需要自定义更新请使用下面的方法,需要接收一个(NSDictionary *)appInfo的参数
    [MobClick checkUpdateWithDelegate:self selector:@selector(updateMethod:)];
    
    [MobClick updateOnlineConfig];  //在线参数配置
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [MobClick setAppVersion:version];
    //    1.6.8之前的初始化方法
    //    [MobClick setDelegate:self reportPolicy:REALTIME];  //建议使用新方法
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onlineConfigCallBack:) name:UMOnlineConfigDidFinishedNotification object:nil];
}
- (void)updateMethod:(NSDictionary *)appInfo {
}


- (void)onlineConfigCallBack:(NSNotification *)note {
    
}

#pragma mark - UIAlertViewDelegate Methods

- (void)willPresentAlertView:(UIAlertView *)alertView{
    //设置alertView中文字居左对其
    if (alertView.tag == 100) {
        for( UIView * view in alertView.subviews )
            
        {
            if( [view isKindOfClass:[UILabel class]] )
                
            {
                UILabel* label = (UILabel*) view;
                label.textAlignment = NSTextAlignmentLeft;
                
            }
            
        }
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    int tag = (int)alertView.tag ;
    if (tag == 100) {
        if (buttonIndex == 1){
            //单击“立即升级” 调转到新版本更新页面
            NSDictionary *dic = [[NSBundle mainBundle] infoDictionary]; //获取info－plist
            
            NSString *appName = [dic objectForKey:@"CFBundleIdentifier"]; //获取Bundle identifier
            
            
            if ([appName rangeOfString:@"com.kstapp"].location != NSNotFound && [newVersionDownLoadUrlStr length] != 0) {
                
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:newVersionDownLoadUrlStr]];
                
            }else if([appName rangeOfString:@"kst"].location != NSNotFound && [newVersionDownLoadUrlEnterpriseStr length] != 0) {
                
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:newVersionDownLoadUrlEnterpriseStr]];
                
            }
            
        }
    }    
}

@end
