//
//  FYNewMoreViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-6-27.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYNewMoreViewController.h"
#import "FYAboutViewController.h"
#import "FYShareViewController.h"
#import "FYVersionViewController.h"
#import "FYHelpViewController.h"
#import "FYPrivacyViewController.h"
#import "FYCoprightViewController.h"
#import "FYServiceViewController.h"
#import "WXCommonViewClass.h"
#import "AsynImageView.h"
#import "XY_Cloud_VersionUpdate.h"
#import "AppDelegate.h"
#import "WXConfigDataControll.h"
#import "FY_Cloud_getMoreAds.h"
#import "FY_Cloud_getTwoMa.h"
#import "WXConfigDataControll.h"
#import "WeiboSDK.h"
#import "WXApi.h"
#import "WeiboApi.h"
#import "WXCommonDataClass.h"
static NSTimer *timer = nil;
@interface FYNewMoreViewController ()
{

    WeiboApi *wbapi;
}
@end

@implementation FYNewMoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //微信、新浪注册
    [self register];
    self.title = NSLocalizedString(@"更多", @"更多标题");
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    [self.view addSubview:self.shareView];
    self.shareView.frame = CGRectMake(0, self.view.frame.size.height, self.shareView.frame.size.width, self.shareView.frame.size.height);
    if (iPhone5) {
        self.backgroundScrollView.contentSize = CGSizeMake(320, 770);
    }else
    {
    self.backgroundScrollView.contentSize = CGSizeMake(320, 870);
    }
    self.imageView_new.hidden = YES;
    [self initSrollView];
    [self initImage];
}

- (void)initImage
{
    [[XY_Cloud_VersionUpdate share] setDelegate:(id)self];
    [[XY_Cloud_VersionUpdate share] getCurrentVersion];
    
    [[FY_Cloud_getTwoMa share] setDelegate:(id)self];
    [[FY_Cloud_getTwoMa share] getTwoMa];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    cuurentVersion = [WXConfigDataControll getCuurentVersion];
    self.lbl_version.text = [NSString stringWithFormat:@"V%@", cuurentVersion];
    //begin:wangxia
    isFirstLoad = YES;
    isLoadVerson = NO;
    //从服务器端得到最新版本的信息
    [[XY_Cloud_VersionUpdate share] setDelegate:(id)self];
    [[XY_Cloud_VersionUpdate share] getCurrentVersion];
    //end
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[XY_Cloud_VersionUpdate share] setDelegate:nil];
    [[FY_Cloud_getTwoMa share] setDelegate:nil];
}

- (void)initSrollView{
    //scrollView
    _scrollView.contentSize = CGSizeMake(300 * 4, 130);
    NSString *HtmlStr = [WXConfigDataControll getStaticHtmlUrlStr];
    for (int i = 1; i <= 4; i++)
    {
        AsynImageView * imageView = [[AsynImageView alloc] init];
        imageView.frame = CGRectMake((i - 1) * 300, 0, 300, 130);
        imageView.userInteractionEnabled = YES;
        NSString *urlStr = [NSString stringWithFormat:@"%@more_%i.jpg",HtmlStr,i];
        imageView.type = 2;
        imageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
        imageView.imageURL = urlStr;
        
        
        //添加点击事件
        UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(skipToPage)];
        [imageView addGestureRecognizer:tapGesture];
        tapGesture.numberOfTapsRequired = 1;
        [_scrollView addSubview:imageView];
    }
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    
    
    
   // timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(scrollViewAutoScroll) userInfo:nil repeats:YES];
}


//微信、新浪注册
- (void)register{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if(singletonClass.isRegisterSina == NO){//没有注册
        //注册新浪微博
        [WeiboSDK enableDebugMode:YES];
        if([WeiboSDK registerApp:[WXConfigDataControll getSinaAppKey]]){
            singletonClass.isRegisterSina = YES;
        }
        
    }
    //微信注册
    if (singletonClass.isRegisterWeiXin == NO) {
        [WXApi registerApp:[WXConfigDataControll getWeiXinAppKey] withDescription:@"DianShangApp"];
        singletonClass.isRegisterWeiXin = YES;
    }
    
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int cur = scrollView.contentOffset.x / scrollView.frame.size.width ;
    _pageControl.currentPage = cur;
}
#pragma mark - my methods

-(void)scrollViewAutoScroll
{
    CGFloat moveX = _scrollView.contentOffset.x;
    moveX += 300;
    if (moveX >= 300 * 4)
    {
        moveX = 0.0;
    }
    //添加动画效果
    [UIView animateWithDuration:0.7 animations:^{
        [_scrollView setContentOffset:CGPointMake(moveX, 0)];
        _pageControl.currentPage = _scrollView.contentOffset.x / 300;
    }];
    
}

/*广告链接跳转*/
-(void)skipToPage
{
    int imagePage = _pageControl.currentPage;
    NSString * url = [WXConfigDataControll getStaticHtmlUrlStr];
    switch (imagePage)
    {
        case 0:
        {
            NSString * url1 = [NSString stringWithFormat:@"%@%@",url,@"more_1.html"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url1]];
            break;
        }
        case 1:
        {
            NSString * url1 = [NSString stringWithFormat:@"%@%@",url,@"more_2.html"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url1]];
            break;
        }
        case 2:
        {
            NSString * url1 = [NSString stringWithFormat:@"%@%@",url,@"more_3.html"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url1]];
            break;
        }
        case 3:
        {
            NSString * url1 = [NSString stringWithFormat:@"%@%@",url,@"more_4.html"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url1]];
            break;
        }
            
        default:
            break;
    }
    
}
- (void)backBtnPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

#pragma mark - AlertViewDelegate Method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    int tag = (int)alertView.tag ;
    if (tag == 100) {
        if (buttonIndex == 1){
            //单击“立即升级” 调转到新版本更新页面
            NSDictionary *dic = [[NSBundle mainBundle] infoDictionary]; //获取info－plist
            
            NSString *appName = [dic objectForKey:@"CFBundleIdentifier"]; //获取Bundle identifier
            
            
            if ([appName rangeOfString:@"com.kstapp"].location != NSNotFound && [newVersionDownLoadUrlStr length] != 0) {
                
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:newVersionDownLoadUrlStr]];

            }else if([appName rangeOfString:@"com.kst"].location != NSNotFound && [newVersionDownLoadUrlEnterpriseStr length] != 0) {
                
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:newVersionDownLoadUrlEnterpriseStr]];
                
            }
            
        }
    }
    if (alertView.tag == 101) {
        if (buttonIndex == 1) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[WXApi getWXAppInstallUrl]]];
        }
    }
    if (tag == 200) {
        if (buttonIndex == 0)
        {
            [WXCommonViewClass goToLogin:self.navigationController];
        }else
        {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            view.backgroundColor = [UIColor darkGrayColor];
            view.alpha = 0.5;
            view.tag = 100;
            [self.view addSubview:view];
            [self.view bringSubviewToFront:self.shareView];
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.2];
            self.shareView.frame = CGRectMake(0, self.view.frame.size.height - self.shareView.frame.size.height, self.shareView.frame.size.width, self.shareView.frame.size.height);
            [UIView commitAnimations];
        }
    }
    
}


#pragma mark - 回调
- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [[XY_Cloud_VersionUpdate share] setDelegate:nil];
    [WXCommonViewClass setActivityWX:NO content:NO nav:self.navigationController];
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
    self.imageView_new.hidden = YES;
    self.lbl_version.text = [NSString stringWithFormat:@"V%@", cuurentVersion];
}
//获取更新
-(void)versionUpdateSuccess:(NSArray *)data
{
    [[XY_Cloud_VersionUpdate share] setDelegate:nil];
    [WXCommonViewClass setActivityWX:NO content:NO nav:self.navigationController];
    isLoadVerson = YES;
    [self doNewVersion];
}
-(void)versionUpdateFailed:(NSString*)errMsg
{
    [[XY_Cloud_VersionUpdate share] setDelegate:nil];
    [WXCommonViewClass setActivityWX:NO content:NO nav:self.navigationController];
}

//获取二维码回调
-(void)shareTwoMaSuccess:(NSDictionary *)data
{
    NSString *TwoMaStr = [data objectForKey:@"qrcpic"];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    NSString * shareUrl = [NSString stringWithFormat:@"%@%@",singletonClass.serviceURL,TwoMaStr];
    NSURL * url = [NSURL URLWithString:shareUrl];
    NSData *dat = [NSData dataWithContentsOfURL:url];
    self.imageView.image = [UIImage imageWithData:dat];
}

-(void)shareTwoMaFailed:(NSString*)errMsg
{

}
//关于我们
- (IBAction)btn_aboutClick:(id)sender
{
    FYAboutViewController *abontVC = [[FYAboutViewController alloc] init];
    [self.navigationController pushViewController:abontVC animated:YES];
}
//应用分享
- (IBAction)btn_shareClick:(id)sender
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    view.backgroundColor = [UIColor darkGrayColor];
    view.alpha = 0.5;
    view.tag = 100;
    [self.view addSubview:view];
    [self.view bringSubviewToFront:self.shareView];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    self.shareView.frame = CGRectMake(0, self.view.frame.size.height - self.shareView.frame.size.height, self.shareView.frame.size.width, self.shareView.frame.size.height);
    [UIView commitAnimations];
}

- (IBAction)shareCancelBtnAct:(id)sender
{
    UIView *view = [self.view viewWithTag:100];
    [view removeFromSuperview];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    self.shareView.frame = CGRectMake(0, self.view.frame.size.height , self.shareView.frame.size.width, self.shareView.frame.size.height);
    [UIView commitAnimations];
}

//版本更新
- (IBAction)btn_versionClick:(id)sender
{
    isFirstLoad = NO;
    
    //小圈运动,等待登录验证
    [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"正在检查更新，请稍后...", @"正在检查更新，请稍后...") nav:self.navigationController];
    if (isLoadVerson == NO) {
        //从服务器端得到最新版本的信息
        [[XY_Cloud_VersionUpdate share] setDelegate:(id)self];
        [[XY_Cloud_VersionUpdate share] getCurrentVersion];
    }else{
        [WXCommonViewClass setActivityWX:NO content:NO nav:self.navigationController];
        [self doNewVersion];
    }
    
    
}

- (IBAction)btn_helpClick:(id)sender
{
    FYHelpViewController *helpVC = [[FYHelpViewController alloc] init];
    [self.navigationController pushViewController:helpVC animated:YES];
}
//隐私保护
- (IBAction)btn_privateClick:(id)sender
{
    FYPrivacyViewController *privacyVC = [[FYPrivacyViewController alloc] init];
    [self.navigationController pushViewController:privacyVC animated:YES];
}
//版权声明
- (IBAction)btn_coprightClick:(id)sender
{
    FYCoprightViewController *coprightVC = [[FYCoprightViewController alloc] init];
    [self.navigationController pushViewController:coprightVC animated:YES];
}
//服务说明
- (IBAction)btn_serviceClick:(id)sender
{
    FYServiceViewController *serviceVC = [[FYServiceViewController alloc] init];
    [self.navigationController pushViewController:serviceVC animated:YES];
}



/*处理版本更新方法*/
- (void)doNewVersion{
    NSDictionary *dic = [AppDelegate delegate].versionDictionary;
    NSString *updateContentStr,*updateContentStrEnterprise;
    
    if (dic == nil) {
        [self makeVersion:NO newVersionContent:nil newVersion:nil];
    }else{
        NSString *newVersionStr = [dic objectForKey:@"version"];
        NSString *newVersionStrEnterprise = [dic objectForKey:@"versionEnterprise"];
        newVersionDownLoadUrlStr = [dic objectForKey:@"updateUrl"];
        newVersionDownLoadUrlEnterpriseStr = [dic objectForKey:@"updateUrlEnterprise"];
        updateContentStr = [dic objectForKey:@"updateInfo"];
        updateContentStrEnterprise = [dic objectForKey:@"updateInfoEnterprise"];
        NSDictionary *dic = [[NSBundle mainBundle] infoDictionary]; //获取info－plistß
        NSString *appName = [dic objectForKey:@"CFBundleIdentifier"]; //获取Bundle identifier
        
        if ([appName rangeOfString:@"kstapp"].location != NSNotFound && [newVersionStr length] != 0 && [newVersionDownLoadUrlStr length] != 0) {
            if ([WXCommonDataClass compareVersion:newVersionStr :cuurentVersion]) {
                [self makeVersion:YES newVersionContent:updateContentStr newVersion:newVersionStr];
            }else{
                [self makeVersion:NO newVersionContent:nil newVersion:nil];
            }
        }else if ([newVersionStrEnterprise length] != 0 && [newVersionDownLoadUrlEnterpriseStr length] != 0) {
            if ([WXCommonDataClass compareVersion:newVersionStrEnterprise :cuurentVersion]) {
                [self makeVersion:YES newVersionContent:updateContentStrEnterprise newVersion:newVersionStrEnterprise];
                
            }else{
                [self makeVersion:NO newVersionContent:nil newVersion:nil];
                
            }
        }else
        {
            [self makeVersion:NO newVersionContent:nil newVersion:nil];
        }
        
    }
}

- (void)makeVersion:(BOOL)isHaveVersion newVersionContent:(NSString *)updateContentStrEnterprise newVersion:(NSString *)newVersionStrEnterprise{
    NSString *titleStr;
    UIAlertView *alert;
    if (isHaveVersion) {
        self.imageView_new.hidden = NO;
        self.lbl_version.hidden = YES;
        if (!isFirstLoad) {
            titleStr = NSLocalizedString(@"有新版本可用", @"有新版本可用");
            titleStr = [NSString stringWithFormat:@"%@ %@",titleStr,newVersionStrEnterprise];
            alert = [[UIAlertView alloc]initWithTitle:titleStr message:updateContentStrEnterprise delegate:self cancelButtonTitle:NSLocalizedString(@"以后再说", @"以后再说") otherButtonTitles:NSLocalizedString(@"立即升级", @"立即升级"), nil];
            alert.delegate = self;
            alert.tag = 100;
            
            [alert show];
        }
        
    }else{
        self.imageView_new.hidden = YES;
        self.lbl_version.text = [NSString stringWithFormat:@"V%@", cuurentVersion];
        
        if (!isFirstLoad) {
            [WXCommonViewClass showHudInButtonOfView:self.view title:NSLocalizedString(@"您已使用最新版本，不需要更新", @"您已使用最新版本，不需要更新") closeInSecond:3];
        }
        
    }
}

- (IBAction)btn_weixinGoodF:(id)sender
{
    if ([WXConfigDataControll getWeiXinAppKey] == nil || [[WXConfigDataControll getWeiXinAppKey] isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"此功能暂未开通" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if ([WXApi isWXAppInstalled]) {
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    UIView *view = [self.view viewWithTag:100];
    [view removeFromSuperview];
    self.shareView.frame = CGRectMake(0, self.view.frame.size.height , self.shareView.frame.size.width, self.shareView.frame.size.height);
    singletonClass.shareStr = @"weixin";
    singletonClass.shareWhere = 1;
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.text = [NSString stringWithFormat:@"%@手机客户端挺好用的,推荐给大家,快来安装体验一下吧!下载链接是 http://apps.kstapp.com/downloads/%@.html",[WXConfigDataControll getAppName], singletonClass.shopid];;
    req.bText = YES;
    [WXApi sendReq:req];
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"微信版本过低或未安装微信,现在去下载微信?" delegate:self cancelButtonTitle:@"暂不下载" otherButtonTitles:@"前往下载", nil];
        alert.tag = 101;
        [alert show];
    }
}
- (IBAction)btn_weixinFquan:(id)sender
{
    if ([WXConfigDataControll getWeiXinAppKey] == nil || [[WXConfigDataControll getWeiXinAppKey] isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"此功能暂未开通" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if ([WXApi isWXAppInstalled]) {
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    UIView *view = [self.view viewWithTag:100];
    [view removeFromSuperview];
    self.shareView.frame = CGRectMake(0, self.view.frame.size.height , self.shareView.frame.size.width, self.shareView.frame.size.height);
    singletonClass.shareStr = @"weixin";
    singletonClass.shareWhere = 1;
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    req.text = [NSString stringWithFormat:@"%@手机客户端挺好用的,推荐给大家,快来安装体验一下吧!下载链接是 http://apps.kstapp.com/downloads/%@.html",[WXConfigDataControll getAppName], singletonClass.shopid];
    req.bText = YES;
    req.scene = WXSceneTimeline;
    
    [WXApi sendReq:req];
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"微信版本过低或未安装微信,现在去下载微信?" delegate:self cancelButtonTitle:@"暂不下载" otherButtonTitles:@"前往下载", nil];
        alert.tag = 101;
        [alert show];
    }
    
}

- (IBAction)btn_shareSina:(id)sender
{
    if ([WXConfigDataControll getSinaAppKey] == nil || [[WXConfigDataControll getSinaAppKey] isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"此功能暂未开通" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    UIView *view = [self.view viewWithTag:100];
    [view removeFromSuperview];
    self.shareView.frame = CGRectMake(0, self.view.frame.size.height , self.shareView.frame.size.width, self.shareView.frame.size.height);
    singletonClass.shareStr = @"sina";
    singletonClass.shareWhere = 1;
    WBSendMessageToWeiboRequest *request = [WBSendMessageToWeiboRequest requestWithMessage:[self messageToShare]];
    request.userInfo = @{@"ShareMessageFrom": @"SendMessageToWeiboViewController",
                         @"Other_Info_1": [NSNumber numberWithInt:123],
                         @"Other_Info_2": @[@"obj1", @"obj2"],
                         @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
    //    request.shouldOpenWeiboAppInstallPageIfNotInstalled = NO;
    
    [WeiboSDK sendRequest:request];
}

- (IBAction)btn_shareTX:(id)sender
{
    if ([WXConfigDataControll getQQAppKey] == nil || [[WXConfigDataControll getQQAppKey] isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"此功能暂未开通" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    //WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    UIView *view = [self.view viewWithTag:100];
    [view removeFromSuperview];
    self.shareView.frame = CGRectMake(0, self.view.frame.size.height, self.shareView.frame.size.width, self.shareView.frame.size.height);
    FYShareViewController *shareVC = [[FYShareViewController alloc] initWithNibName:@"FYShareViewController" bundle:nil];
    shareVC.pushVC = self;
    [self.navigationController pushViewController:shareVC animated:YES];

//    singletonClass.shareStr = @"TX";
//    singletonClass.shareWhere = 1;
//    wbapi = [[WeiboApi alloc]initWithAppKey:@"801416469" andSecret:@"971f5b17327e7badb566f0d98e9795c9" andRedirectUri:[WXConfigDataControll getRedirectUrl]];
//    [wbapi loginWithDelegate:self andRootController:self];
}

//处理新浪微博数据
- (WBMessageObject *)messageToShare
{
    WBMessageObject *message = [WBMessageObject message];
     WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    message.text = [NSString stringWithFormat:@"%@手机客户端挺好用的,推荐给大家,快来安装体验一下吧!下载链接是 http://apps.kstapp.com/downloads/%@.html",[WXConfigDataControll getAppName], singletonClass.shopid];;
    return message;
}

@end
