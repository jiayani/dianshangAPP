//
//  FYHelpViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-5-22.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYHelpViewController.h"
#import "WXCommonViewClass.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "WXConfigDataControll.h"
#import "XYLoadingView.h"

@interface FYHelpViewController ()

@end

@implementation FYHelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{

    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [WXCommonViewClass hideTabBar:self isHiden:YES];    //隐藏tabbar
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"帮助手册", @"帮助手册标题");
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    /*加载静态页面方法*/
    NSString *HtmlStr = [WXConfigDataControll getStaticHtmlUrlStr];
    HtmlStr = [NSString stringWithFormat:@"%@%@",HtmlStr,@"help.html"];
    UIWebView *webView = [[UIWebView alloc]initWithFrame:CGRectMake(10, 10, 300, self.view.frame.size.height - 64)];
    
    webView.backgroundColor = [UIColor whiteColor];
    [webView setOpaque:NO];
    id scroller = [webView.subviews objectAtIndex:0];
    for (UIView *subView in [scroller subviews])
    {
        if ([[[subView class] description] isEqualToString:@"UIImageView"])
            subView.hidden = YES;
    }
    [self.view addSubview:webView];
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:HtmlStr]];
    [webView loadRequest:request];
    UIButton * backBtn = [WXCommonViewClass getBackButton];
    [backBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftBtnItem;
    
}

-(void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
