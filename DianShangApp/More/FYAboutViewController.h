//
//  FYAboutViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-5-22.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"
@class AsynImageView;
@interface FYAboutViewController : UIViewController<UIWebViewDelegate,WX_Cloud_Delegate>

@property (weak, nonatomic) IBOutlet AsynImageView *imageView_title;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end
