//
//  FYShareViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-5-22.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeiboApi.h"
#import "WX_Cloud_Delegate.h"
@interface FYShareViewController : UIViewController<WeiboAuthDelegate,WeiboRequestDelegate,WX_Cloud_Delegate>
{
    WeiboApi*wbapi;
}
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (nonatomic , strong) WeiboApi *wbapi;
@property (nonatomic, weak) UIViewController *pushVC;
@property (nonatomic, weak) NSString *titleText;
@end
