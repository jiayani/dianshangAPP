//
//  FYNewMoreViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-6-27.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYNewMoreViewController : UIViewController <UIAlertViewDelegate, UIScrollViewDelegate>
{
    NSString *newVersionDownLoadUrlStr;//新版本下载地址
    NSString *newVersionDownLoadUrlEnterpriseStr;//新版本企业下载地址
    NSString *cuurentVersion;
    //wangxia add 版本更新
    BOOL isFirstLoad;//是否是第一次登录
    BOOL isLoadVerson;//是否下载下来了版本更新的数据
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *backgroundScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_new;
@property (weak, nonatomic) IBOutlet UIView *shareView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_version;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
- (IBAction)btn_aboutClick:(id)sender;
- (IBAction)btn_shareClick:(id)sender;
- (IBAction)btn_versionClick:(id)sender;
- (IBAction)btn_helpClick:(id)sender;
- (IBAction)btn_privateClick:(id)sender;
- (IBAction)btn_coprightClick:(id)sender;
- (IBAction)btn_serviceClick:(id)sender;
- (IBAction)btn_weixinGoodF:(id)sender;
- (IBAction)btn_weixinFquan:(id)sender;
- (IBAction)btn_shareSina:(id)sender;
- (IBAction)shareCancelBtnAct:(id)sender;
- (IBAction)btn_shareTX:(id)sender;
@end
