//
//  FYPrivacyViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-5-22.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYPrivacyViewController.h"
#import "WXConfigDataControll.h"
#import "WXCommonViewClass.h"
@interface FYPrivacyViewController ()

@end

@implementation FYPrivacyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"隐私保护", @"隐私保护标题");
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    NSString *HtmlStr = [WXConfigDataControll getStaticHtmlUrlStr];
    HtmlStr = [NSString stringWithFormat:@"%@%@",HtmlStr,@"privacy.html"];
    UIWebView *webView = [[UIWebView alloc]initWithFrame:CGRectMake(10, 10, 300, self.view.frame.size.height)];
    webView.backgroundColor = [UIColor whiteColor];
    [webView setOpaque:NO];
    id scroller = [webView.subviews objectAtIndex:0];
    for (UIView *subView in [scroller subviews])
    {
        if ([[[subView class] description] isEqualToString:@"UIImageView"])
            subView.hidden = YES;
    }
    [self.view addSubview:webView];
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:HtmlStr]];
    [webView loadRequest:request];
    UIButton * backBtn = [WXCommonViewClass getBackButton];
    [backBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftBtnItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
