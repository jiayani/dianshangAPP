//
//  FYAboutViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-5-22.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYAboutViewController.h"
#import "WXCommonViewClass.h"
#import "XYLoadingView.h"
#import "FY_Cloud_getMoreAds.h"
#import "AsynImageView.h"
@interface FYAboutViewController ()

@end

@implementation FYAboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"关于我们", @"关于我们标题");
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;


}

- (void)viewWillAppear:(BOOL)animated
{
    [[FY_Cloud_getMoreAds share] setDelegate:(id)self];
    [[FY_Cloud_getMoreAds share] getMoreAds];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[FY_Cloud_getMoreAds share] setDelegate:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)backBtnPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView { //webview 自适应高度
    
    [webView stringByEvaluatingJavaScriptFromString:@"var imgs = document.getElementsByTagName('img');"
     " for(var i= 0;i<imgs.length;i++){"
     "  if(imgs[i].width > 305)"
     "  imgs[i].width = 305;"
     "}"];
    
    //    NSString *height = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('img')[0].height"];
    NSString *height = [webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"];
    dispatch_async(dispatch_get_main_queue(), ^{
        CGRect webFrame = webView.frame;
        webView.frame = CGRectMake(webFrame.origin.x, webFrame.origin.y,webFrame.size.width,[height integerValue]);
        self.scrollView.contentSize = CGSizeMake(320, webView.frame.origin.y + webView.frame.size.height);
    });
    
}

-(void)syncGetMoreAdsSuccess:(NSDictionary *)data
{

    self.imageView_title.type = 1;
    self.imageView_title.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    self.imageView_title.imageURL = [data objectForKey:@"shopImage"];
    [self.webView loadHTMLString:[NSString stringWithFormat:@"%@",[data objectForKey:@"shopInformation"]] baseURL:nil];
    self.webView.frame = CGRectMake(self.webView.frame.origin.x, self.webView.frame.origin.y, self.webView.frame.size.width, self.webView.scrollView.contentSize.height);
    self.webView.scrollView.scrollEnabled = NO;
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.webView.frame.origin.y + self.webView.frame.size.height);
}

-(void)syncGetMoreAdsFailed:(NSString*)errMsg
{
    [WXCommonViewClass showHudInView:self.view title:errMsg];
}

- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}

@end
