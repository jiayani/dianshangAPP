//
//  FYShareViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-5-22.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYShareViewController.h"
#import "WXCommonViewClass.h"
#import <QuartzCore/QuartzCore.h>
#import "WXConfigDataControll.h"
#import "FYNewMoreViewController.h"
#import "XYGoodsInforViewController.h"
#import "WXFavorableActivityDetailViewController.h"
#import "WXCommonSingletonClass.h"
#import "WXConfigDataControll.h"
#import "FY_Cloud_getWeiBo.h"
#import "FY_Cloud_setWeiBo.h"
#import "FY_Cloud_sendScore.h"
#import "WX_Cloud_IsConnection.h"
#import "WeiboSDK.h"
@interface FYShareViewController ()
{
    BOOL isOK;
}
@end

@implementation FYShareViewController
@synthesize wbapi;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //新浪，腾讯注册
    [self register];
    
    self.title = @"分享";
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    self.textView.layer.borderColor = UIColor.grayColor.CGColor;
    self.textView.layer.borderWidth = 1;
    [self initButton];
    [self initText];
    [self initTimestamp];
}
//腾讯注册
- (void)register{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if(singletonClass.wbapi == nil){
        //注册腾讯微博
        singletonClass.wbapi = [[WeiboApi alloc]initWithAppKey:[WXConfigDataControll getQQAppKey] andSecret:[WXConfigDataControll getQQAppSecret] andRedirectUri:[WXConfigDataControll getRedirectUrl]];
        
    }
    
}
- (void)initButton
{
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    UIButton *shareBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:NSLocalizedString(@"分享", @"优惠活动/分享")];
    [shareBtn addTarget:self action:@selector(shareButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:shareBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}

- (void)initText
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    self.wbapi = singletonClass.wbapi;
    if ([self.pushVC isKindOfClass:[FYNewMoreViewController class]]) {
        self.textView.text = [NSString stringWithFormat:@"%@手机客户端挺好用的,推荐给大家,快来安装体验一下吧!下载链接是 http://apps.kstapp.com/downloads/%@.html",[WXConfigDataControll getAppName], singletonClass.shopid];
    }
    if ([self.pushVC isKindOfClass:[XYGoodsInforViewController class]]) {
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        self.textView.text = [NSString stringWithFormat:@"我在%@手机客户端看到了%@介绍,大家也来看看吧,下载链接是 http://apps.kstapp.com/downloads/%@.html",[WXConfigDataControll getAppName] ,self.titleText, singletonClass.shopid];
    }
    if ([self.pushVC isKindOfClass:[WXFavorableActivityDetailViewController class]]) {
        self.textView.text = [NSString stringWithFormat:@"我在%@手机客户端看到了%@介绍,大家也来看看吧,下载链接是 http://apps.kstapp.com/downloads/%@.html",[WXConfigDataControll getAppName] ,self.titleText, singletonClass.shopid];
        
    }
}

- (void)initTimestamp
{
    if(![[WX_Cloud_IsConnection share]isConnectionAvailable]){
        [WXCommonViewClass showHudInButtonOfView:[UIApplication sharedApplication].keyWindow title:NSLocalizedString(@"目前网络连接不可用",@"目前网络连接不可用") closeInSecond:2];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.shareStr = @"TX";
    singletonClass.shareWhere = 1;
    if ([singletonClass.wbapi isAuthValid]) {
        
    }
    else
    {
        [[FY_Cloud_getWeiBo share] setDelegate:(id)self];
        [[FY_Cloud_getWeiBo share] getWeiBo];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[FY_Cloud_getWeiBo share] setDelegate:nil];
    [[FY_Cloud_sendScore share] setDelegate:nil];
    [[FY_Cloud_sendScore share] setDelegate:nil];
}
- (void)shareButtonPressed:(id)sender
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.shareStr = @"TX";
    singletonClass.shareWhere = 1;
    isOK = YES;
    if ([singletonClass.wbapi isAuthValid]) {
        NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"json", @"format", self.textView.text, @"content", nil];
        [singletonClass.wbapi requestWithParams:params apiName:@"t/add_pic" httpMethod:@"POST" delegate:self];
        
    }
    else
    {
        [[FY_Cloud_getWeiBo share] setDelegate:(id)self];
        [[FY_Cloud_getWeiBo share] getWeiBo];
    }

    
}

- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}
//获取用户绑定信息回调
-(void)getWeiBoSuccess:(NSDictionary *)data
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.shareStr = @"TX";
    singletonClass.shareWhere = 1;
    NSDictionary *qqDic = [[NSDictionary alloc] initWithDictionary:[data objectForKey:@"qqInfo"]];
    if ([data objectForKey:@"hasBindQQ"]) {
        long nowTime = [self date];
        if (nowTime > [[qqDic objectForKey:@"qq_timestamp"] doubleValue]) {
            [singletonClass.wbapi loginWithDelegate:self andRootController:self];
            return;
        }
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"yyyy-MM-dd HH-mm-ss"];
        NSDate *date1 = [NSDate dateWithTimeIntervalSince1970:[[qqDic objectForKey:@"qq_timestamp"] doubleValue]];
       // NSDate *date2 = [NSDate date];
        NSTimeInterval time1 = [date1 timeIntervalSince1970];
        [singletonClass.wbapi loginWithAccesstoken:[qqDic objectForKey:@"qq_nonce"] andOpenId:[qqDic objectForKey:@"qq_openid"] andExpires:time1 andRefreshToken:[qqDic objectForKey:@"qq_token"] andDelegate:self];
        isOK = YES;
        
    }
    else{
        [singletonClass.wbapi loginWithDelegate:self andRootController:self];
    }
}

-(void)getWeiBoFailed:(NSString*)errMsg
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.shareStr = @"TX";
    singletonClass.shareWhere = 1;
    [singletonClass.wbapi loginWithDelegate:self andRootController:self];
}

#pragma mark    TXWeiboRequestDelegate

/**
 * @brief   接口调用成功后的回调
 * @param   INPUT   data    接口返回的数据
 * @param   INPUT   request 发起请求时的请求对象，可以用来管理异步请求
 * @return  无返回
 */
//发送成功回调
- (void)didReceiveRawData:(NSData *)data reqNo:(int)reqno
{
//    NSString *strResult = [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
    //[NSString stringWithCharacters:[data bytes] length:[data length]];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if (singletonClass.currentUserId == nil) {
        [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:@"分享成功"];
        return;
    }
    //调用接口，判断是否为当日首次分享
    long time = [self date];
    NSNumber *longNumber = [NSNumber numberWithLong:time];
    NSString *longStr = [longNumber stringValue];
    [[FY_Cloud_sendScore share] setDelegate:(id)self];
    [[FY_Cloud_sendScore share] sendScoreWithShareType:@"2" andShareTime:longStr];
    
    
}

/**
 * @brief   发送失败后的回调
 * @param   INPUT   error   接口返回的错误信息
 * @param   INPUT   request 发起请求时的请求对象，可以用来管理异步请求
 * @return  无返回
 */
- (void)didFailWithError:(NSError *)error reqNo:(int)reqno
{
    [WXCommonViewClass showHudInView:self.view title:@"分享失败"];
}


//授权成功回调
- (void)DidAuthFinished:(WeiboApi *)wbapi_
{
//    NSString *str = [[NSString alloc]initWithFormat:@"accesstoken = %@\r openid = %@\r appkey=%@ \r appsecret=%@\r", wbapi_.accessToken, wbapi_.openid, wbapi_.appKey, wbapi_.appSecret];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if (singletonClass.currentUserId != nil) {
        [[FY_Cloud_setWeiBo share] setDelegate:(id)self];
        [[FY_Cloud_setWeiBo share] setWeiBoWithType:@"1" andQQOpenID:wbapi_.openid andNonce:wbapi_.accessToken andSingature:@"GET" andToken:wbapi_.refreshToken andTimestamp:[NSString stringWithFormat:@"%f", wbapi_.expires] andVeritier:@"1111"];
    }
    if (isOK) {
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"json", @"format", self.textView.text, @"content", nil];
        [singletonClass.wbapi requestWithParams:params apiName:@"t/add_pic" httpMethod:@"POST" delegate:self];
        return; 
    }
    [WXCommonViewClass showHudInView:self.view title:@"授权成功"];
    
}
//绑定微博成功回调
-(void)setWeiBoSuccess:(NSString *)data
{

}

-(void)setWeiBoFailed:(NSString*)errMsg
{
    [WXCommonViewClass showHudInView:self.view title:errMsg];
}
/**
 * @brief   授权成功后的回调
 * @param   INPUT   wbapi   weiboapi 对象，取消授权后，授权信息会被清空
 * @return  无返回
 */
- (void)DidAuthCanceled:(WeiboApi *)wbapi_
{
    [WXCommonViewClass showHudInView:self.view title:@"授权失败"];
}

/**
 * @brief   授权成功后的回调
 * @param   INPUT   error   标准出错信息
 * @return  无返回
 */
- (void)DidAuthFailWithError:(NSError *)error
{
    [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:@"授权失败"];
    
}

//获取当前时间的时间戳
- (long)date {
    NSDate *datenow = [NSDate date];
    NSTimeZone *zone = [NSTimeZone  timeZoneWithName:@"Asia/Shanghai"];
    //    NSTimeZone *zone = [NSTimeZone  systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate:datenow];
    NSDate *localeDate = [datenow  dateByAddingTimeInterval: interval];
    long  timSp = (long)[localeDate timeIntervalSince1970];
    return timSp;
}

#pragma mark 赠送积分回调
//赠积分回调
-(void)syncSendScoreSuccess:(NSDictionary *)data
{
    NSString *str = [NSString stringWithFormat:@"%@", [data objectForKey:@"isfirst"]];
    if ([str isEqualToString:@"1"]) {
        [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:[NSString stringWithFormat:@"分享成功,为您赠送%@积分", [data objectForKey:@"sendIntegral"]]];
    }else
    {
        [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:@"分享成功"];
    }
}

-(void)syncSendScoreFailed:(NSString*)errMsg
{
    [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:@"分享成功"];
}

/**
 * @brief   重刷授权成功后的回调
 * @param   INPUT   wbapi 成功后返回的WeiboApi对象，accesstoken,openid,refreshtoken,expires 等授权信息都在此处返回
 * @return  无返回
 */
- (void)DidAuthRefreshed:(WeiboApi *)wbapi
{
    
}

/**
 * @brief   重刷授权失败后的回调
 * @param   INPUT   error   标准出错信息
 * @return  无返回
 */
- (void)DidAuthRefreshFail:(NSError *)error
{
    
}

- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:errorCode];
}

@end
