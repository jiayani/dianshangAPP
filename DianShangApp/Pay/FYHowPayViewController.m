//
//  FYHowPayViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-6-24.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYHowPayViewController.h"
#import "WXCommonViewClass.h"
#import "WXCommonSingletonClass.h"
@interface FYHowPayViewController ()
{
    NSMutableArray *howArray;
}
@end

@implementation FYHowPayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"选择支付方式";
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    howArray = [[NSMutableArray alloc] initWithCapacity:10];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    self.payStr = singletonClass.payMent;
    for (int i = 0; i < self.payArray.count; i++)
        {
            if ([[self.payArray objectAtIndex:i] isEqualToString:@"1"]) {
                switch (i) {
                    case 0:
                        [howArray addObject:@"支付宝客户端支付"];
                        break;
                    case 1:
                        [howArray addObject:@"支付宝网页支付"];
                        break;
                    case 2:
                        if ([[[singletonClass.sendDic objectForKey:@"consumeTypeId"] substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"1"] || [[[singletonClass.sendDic objectForKey:@"consumeTypeId"] substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"3"])
                        {
                            break;
                        }
                        [howArray addObject:@"货到付款"];
                        break;
                    case 3:
                        if ([[[singletonClass.sendDic objectForKey:@"consumeTypeId"] substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"1"] || [[[singletonClass.sendDic objectForKey:@"consumeTypeId"] substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"2"])
                        {
                            break;
                        }
                        [howArray addObject:@"到店支付"];
                        break;
                    default:
                        break;
                }
            }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark tableView dataSourse
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return howArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = [howArray objectAtIndex:indexPath.row];
    if ([self.payStr isEqualToString:[howArray objectAtIndex:indexPath.row]]) {
        UIImageView *yesImage = [[UIImageView alloc] initWithFrame:CGRectMake(290, 10, 14, 11)];
        yesImage.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"address_yes" ofType:@"png"]];
        [cell.contentView addSubview:yesImage];
        
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.payMent = [howArray objectAtIndex:indexPath.row];
    singletonClass.isPayFirst = NO;
    singletonClass.isPayBack = YES;
    [self.navigationController popViewControllerAnimated:YES];
}

@end
