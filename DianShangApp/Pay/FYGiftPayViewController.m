//
//  FYGiftPayViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-6-11.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYGiftPayViewController.h"
#import "addressView.h"
#import "listTableViewCell.h"
#import "FYDetailTableViewCell.h"
#import "FYLeaveView.h"
#import "FYAddressViewController.h"
#import "WXCommonSingletonClass.h"
#import "AsynImageView.h"
#import "XYLoadingView.h"
#import "FY_Cloud_getAddress.h"
#import "WXCommonViewClass.h"
#import "GiftCart.h"
#import "WXCommonDataClass.h"
#import "FY_Cloud_giftPay.h"
#import "FYGiftSuccessViewController.h"
#import "WXShoppingCartDatabaseClass.h"
static NSInteger sectionSelected = 1;
@interface FYGiftPayViewController ()
{
    XYLoadingView *loadingView;
    NSDictionary *addressDic;
    NSString *remark;
    BOOL isfirst;

}
@end

@implementation FYGiftPayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"礼品兑换", @"礼品兑换页面标题");
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    UIButton *rightBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:@"呼叫客服"];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [rightBtn addTarget:self action:@selector(rightButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    self.lbl_count.text = [NSString stringWithFormat:@"共计%d件礼品", self.count];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"总积分: %d积分", (int)self.money]];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0,4)];
    self.lbl_money.attributedText = str;
    remark = @"";
    isfirst = YES;
    [self initAddress];
}

- (void)backBtnPressed{
    [loadingView removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightButtonPressed
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    [WXCommonDataClass callPhone:singletonClass.phoneNumber];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    addressDic = singletonClass.addressDic;
    if (addressDic != NULL) {
        [self.tableView reloadData];
    }
}
- (void)viewWillDisappear:(BOOL)animated
{
    [[FY_Cloud_getAddress share] setDelegate:nil];
}

- (void)initAddress
{
    
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
    [[FY_Cloud_getAddress share] setDelegate:(id)self];
    [[FY_Cloud_getAddress share] getAddressWithIsOnlyDefault:1];
    
}
#pragma mark 网络回调
-(void)syncGetAddressSuccess:(NSArray *)data
{
    [loadingView removeFromSuperview];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.addressDic = [[NSDictionary alloc] initWithDictionary:[data objectAtIndex:0]];
    addressDic = [[NSDictionary alloc] initWithDictionary:singletonClass.addressDic];
    [self.tableView reloadData];
}

-(void)syncGetAddressFailed:(NSString*)errMsg
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errMsg];
}

//无网络连接
- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}

#pragma mark section click
- (void)addressTap:(UITapGestureRecognizer*)recognizer
{
    FYAddressViewController *addressView = [[FYAddressViewController alloc] initWithNibName:@"FYAddressViewController" bundle:nil];
    [self.navigationController pushViewController:addressView animated:YES];
    
}

- (void)singleTap:(UITapGestureRecognizer*)recognizer
{
    if(sectionSelected == 1){
        sectionSelected = -1;
        isfirst = NO;
    }else{
        sectionSelected = 1;
        isfirst = YES;
    }
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
    if(sectionSelected != -1){
        //判断该一级菜单下的二级菜单是否超过当前屏幕
        NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:1];
        [self.tableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    [self.tableView reloadData];
    
}

#pragma mark UITableView delegate
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        addressView *aView = [[addressView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addressTap:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [aView addGestureRecognizer:tapGestureRecognizer];
        if ([addressDic objectForKey:@"contactName"] == nil || [[addressDic objectForKey:@"contactAddID"] isKindOfClass:[NSNull class]]) {
            aView.lbl_name.text = @"";
            aView.lbl_phone.text = @"";
            aView.lbl_city.text = @"";
            aView.lbl_noData.hidden = NO;
        }else
        {
            aView.lbl_name.text = [addressDic objectForKey:@"contactName"];
            aView.lbl_phone.text = [addressDic objectForKey:@"contactPhone"];
            aView.lbl_city.text = [NSString stringWithFormat:@"%@%@%@%@", [addressDic objectForKey:@"province"], [addressDic objectForKey:@"city"], [addressDic objectForKey:@"district"],[addressDic objectForKey:@"addressName"]];
            aView.lbl_noData.hidden = YES;
        }
        return aView;
        
    }else if(section == 1)
    {
        listTableViewCell *listCell = [[listTableViewCell alloc] init];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [listCell addGestureRecognizer:tapGestureRecognizer];
        listCell.lbl_name.text = @"礼品清单";
        if (isfirst) {
            listCell.imageView_1.image = [UIImage imageNamed:@"quanYiUpArrows.png"];
        }else
        {
            listCell.imageView_1.image = [UIImage imageNamed:@"yuanYiDownArrows.png"];
        }
        listCell.lbl_money.hidden = YES;
        listCell.lbl_zongji.hidden = YES;
        listCell.lbl_count.hidden = YES;
        return listCell;
        
    }else
    {
        FYLeaveView *leaveView = [[FYLeaveView alloc] init];
        leaveView.textView.delegate = self;
        leaveView.lbl_info.text = @"备注信息";
        leaveView.textView.text = remark;
        return leaveView;
    }
    
    return nil;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //二级分类的数量
    if(section != 1){
        return 0;
    }else{
        
        if(sectionSelected == -1){
            return 0;
        }else{
            return self.giftArray.count;
        }
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *str = @"cell";
    FYDetailTableViewCell *cell = (FYDetailTableViewCell *)[tableView dequeueReusableCellWithIdentifier:str];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"FYDetailTableViewCell" owner:self options:nil] lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    GiftCart *shop = (GiftCart *)[self.giftArray objectAtIndex:indexPath.row];
    cell.lbl_name.text = shop.title;
    cell.lbl_price.text = [NSString stringWithFormat:@"%@积分", shop.integral];
    cell.lbl_count.text = [NSString stringWithFormat:@"数量:%@", shop.count];
    cell.imageView_product.type = 1;
    if ([shop.type intValue] == 1) {
        cell.imageView_product.isFromWooGift = YES;
    }
    cell.imageView_product.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    cell.imageView_product.imageURL = shop.pic_url;
    
    return cell;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 100;
    }
    if (section == 1) {
        return 60;
    }
    if (section == 2) {
        return 125;
    }
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(sectionSelected == -1 ){
        return 0;
    }else{
        //被选中的一级菜单下的二级菜单
        if(indexPath.section == sectionSelected){
            return 80;
            //广告位置固定为152
        }else{
            return 0;
        }
    }
}
#pragma mark UITextView delegate
- (void)textViewDidEndEditing:(UITextView *)textView
{
    remark = textView.text;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    if (textView.text.length > 200) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"字数不能大于200!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return NO;
    }
    return YES;
}

#pragma mark 订单相关
- (IBAction)btn_orderClick:(id)sender
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [[UIApplication sharedApplication].keyWindow addSubview:loadingView];
    if ([addressDic objectForKey:@"contactName"] == nil || [[addressDic objectForKey:@"contactAddID"] isKindOfClass:[NSNull class]]) {
        [WXCommonViewClass showHudInView:self.view title:@"请选择收货地址"];
        [loadingView removeFromSuperview];
        return;
    }
    NSMutableArray *orderArray = [[NSMutableArray alloc] initWithCapacity:10];
    NSString *jsonStr;
    for (int i = 0; i < self.giftArray.count; i++) {
        GiftCart *shop = (GiftCart *)[self.giftArray objectAtIndex:i];
        NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:shop.giftid,@"giftID", shop.count,@"count", shop.type, @"giftType",nil];
        [orderArray addObject:dic];
        
        jsonStr = [WXCommonDataClass toJsonStr:orderArray];
    }
    [[FY_Cloud_giftPay share] setDelegate:(id)self];
    [[FY_Cloud_giftPay share] getOrderWithGifts:jsonStr andRemark:remark andContactld:[singletonClass.addressDic objectForKey:@"contactAddID"]];
}

-(void)syncGetGiftPayOrderSuccess:(NSDictionary *)data
{
    [loadingView removeFromSuperview];
    for (int i = 0; i < self.giftArray.count; i++) {
        GiftCart *shop = (GiftCart *)[self.giftArray objectAtIndex:i];
        [WXShoppingCartDatabaseClass deleteOneGiftCart:shop];
    }
    FYGiftSuccessViewController *successVC = [[FYGiftSuccessViewController alloc] initWithNibName:@"FYGiftSuccessViewController" bundle:nil];
    successVC.orderID = [NSString stringWithFormat:@"%@", [[data objectForKey:@"orderFormID"] objectAtIndex:0]];
    successVC.money = [NSString stringWithFormat:@"%d",(int)self.money];
    [self.navigationController pushViewController:successVC animated:YES];
}

-(void)syncGetGiftPayOrderFailed:(NSString*)errMsg
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errMsg];
}

@end
