//
//  FYSendCell.h
//  DianShangApp
//
//  Created by Fuy on 14-6-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYSendCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_style;
@property (weak, nonatomic) IBOutlet UILabel *lbl_money;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end
