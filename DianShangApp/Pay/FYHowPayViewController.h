//
//  FYHowPayViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-6-24.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYHowPayViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) NSArray *payArray;
@property (weak, nonatomic) NSString *payStr;
@end
