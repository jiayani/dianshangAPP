//
//  FYGiftPayViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-6-11.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"

@interface FYGiftPayViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate,WX_Cloud_Delegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_count;

@property (weak, nonatomic) IBOutlet UILabel *lbl_money;
@property (nonatomic, retain) NSArray *giftArray;
@property (nonatomic) int contactId;
@property (nonatomic) int count;
@property (nonatomic) double money;
- (IBAction)btn_orderClick:(id)sender;
@end
