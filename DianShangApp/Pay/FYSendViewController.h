//
//  FYSendViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-6-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYSendViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) NSArray *sendArray;
@end
