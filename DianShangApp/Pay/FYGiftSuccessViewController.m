//
//  FYGiftSuccessViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-6-25.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYGiftSuccessViewController.h"
#import "WXCommonViewClass.h"
@interface FYGiftSuccessViewController ()

@end

@implementation FYGiftSuccessViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"订单已提交", @"礼品订单提交标题");
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    self.lbl_order.text = self.orderID;
    self.lbl_money.text = [NSString stringWithFormat:@"%@积分",self.money];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)backBtnPressed
{
    [WXCommonViewClass goToTabItem:self.navigationController tabItem:4];
}

- (IBAction)btn_continueClick:(id)sender
{
    [WXCommonViewClass goToTabItem:self.navigationController tabItem:2];
}
@end
