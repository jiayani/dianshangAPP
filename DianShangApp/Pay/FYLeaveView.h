//
//  FYLeaveView.h
//  DianShangApp
//
//  Created by Fuy on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYLeaveView : UIView
@property (weak, nonatomic) IBOutlet UILabel *lbl_other;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_info;

@end
