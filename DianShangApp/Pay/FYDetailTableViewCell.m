//
//  FYDetailTableViewCell.m
//  DianShangApp
//
//  Created by Fuy on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYDetailTableViewCell.h"
#import "WXConfigDataControll.h"
@implementation FYDetailTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    self.lbl_price.textColor = [WXConfigDataControll getFontColorOfPrice];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
