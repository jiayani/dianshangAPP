//
//  FYPayViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYPayViewController.h"
#import "addressView.h"
#import "listTableViewCell.h"
#import "FYDetailTableViewCell.h"
#import "FYPayForView.h"
#import "FYLeaveView.h"
#import "FYAddressViewController.h"
#import "WXCommonViewClass.h"
#import "FY_Cloud_getAddress.h"
#import "WXCommonSingletonClass.h"
#import "ShopCart.h"
#import "AsynImageView.h"
#import "WXCommonDataClass.h"
#import "FY_Cloud_getSend.h"
#import "FYSendViewController.h"
#import "XYLoadingView.h"
#import "WXConfigDataControll.h"
#import "FYHowPayViewController.h"
#import "FY_Cloud_Pay.h"
#import "XYOrderPayViewController.h"
#import "XYPaySuccessViewController.h"
#import "WXShoppingCartDatabaseClass.h"
static NSInteger sectionSelected = 1;
@interface FYPayViewController ()
{
    NSDictionary *addressDic;
    NSMutableArray *sendSetArray;
    NSArray *sendArray;
    NSDictionary *sendDic;
    XYLoadingView *loadingView;
    NSArray *payArray;
    NSString *payStr;
    NSString *remark;
    int payment;
    NSArray *groupArray;
    BOOL isfirst;
}
@end

@implementation FYPayViewController
@synthesize payStr;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"结算";
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self initController];
    [self initData];
    [self initAddress];
    //[self initPay];
}

- (void)viewWillAppear:(BOOL)animated
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    addressDic = singletonClass.addressDic;
    sendDic = singletonClass.sendDic;
    payStr = singletonClass.payMent;
    if (singletonClass.isPayFirst) {
        [self initPay];
    }
    
    if (addressDic != NULL && ![[addressDic objectForKey:@"contactAddID"] isKindOfClass:[NSNull class]]) {
        sendSetArray = [[NSMutableArray alloc] initWithCapacity:5];
        [self initSend];
        [self.tableView reloadData];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightButtonPressed
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    [WXCommonDataClass callPhone:singletonClass.phoneNumber];
}

- (void)initController
{
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    UIButton *rightBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:@"呼叫客服"];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [rightBtn addTarget:self action:@selector(rightButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}

- (void)initData
{
    sendSetArray = [[NSMutableArray alloc] initWithCapacity:5];
    self.lbl_count.text = [NSString stringWithFormat:@"共计%d件商品", self.count];
    remark = @"";
    payment = 0;
    isfirst = YES;
    addressDic = [[NSDictionary alloc] init];
    if (self.isTuan) {
        groupArray = [[NSArray alloc] initWithArray:[[self.productArray objectAtIndex:0] objectForKey:@"products"]];
        int count1 = 0;
        for (int i = 0; i < groupArray.count; i++) {
            count1 += [[[groupArray objectAtIndex:i] objectForKey:@"count"] intValue];
        }
        self.lbl_count.text = [NSString stringWithFormat:@"共计%d件商品", self.count * count1];
    }
    NSMutableAttributedString *str;
    str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"总金额: ￥%.2f", self.money]];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0,4)];
    self.lbl_money.attributedText = str;
}

- (void)initAddress
{
    
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
    payArray = [[NSArray alloc] initWithObjects:[WXConfigDataControll getCan_embed], [WXConfigDataControll getCan_wappay],
        [WXConfigDataControll getCan_delivery],[WXConfigDataControll getCan_comepay],nil];
    [[FY_Cloud_getAddress share] setDelegate:(id)self];
    [[FY_Cloud_getAddress share] getAddressWithIsOnlyDefault:1];
        
}

- (void)initSend
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
   // NSString *jsonStr = [WXCommonDataClass toJsonStr:sendSetArray];
    if (self.isTuan) {
        NSMutableArray *orderArray = [[NSMutableArray alloc] initWithCapacity:10];
        NSDictionary *dic1 = [[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%@", [[self.productArray objectAtIndex:0]objectForKey:@"groupbuyID"]],@"groupBuyId", [NSNumber numberWithInt:self.count] ,@"groupBuyNumber",nil];
        [orderArray addObject:dic1];
        NSString *jsonStr1 = [WXCommonDataClass toJsonStr:orderArray];
         [[FY_Cloud_getSend share] setDelegate:(id)self];
         [[FY_Cloud_getSend share] getSendWithGoods:@"" andGroupBuys:jsonStr1 andContactld:[[singletonClass.addressDic objectForKey:@"contactAddID"] intValue]];
    }else
    {
        for (int i = 0; i < self.productArray.count; i++) {
            ShopCart *shop = (ShopCart *)[self.productArray objectAtIndex:i];
            NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:shop.productid,@"dishID", shop.count,@"count",nil];
            [sendSetArray addObject:dic];
        }
        NSString *jsonStr = [WXCommonDataClass toJsonStr:sendSetArray];
        
        [[FY_Cloud_getSend share] setDelegate:(id)self];
        [[FY_Cloud_getSend share] getSendWithGoods:jsonStr andGroupBuys:@"" andContactld:[[singletonClass.addressDic objectForKey:@"contactAddID"] intValue]];
    }
    
}
#pragma mark 网络回调
//获得配送方式成功回调
-(void)syncGetSendSuccess:(NSArray *)data;
{
    [loadingView removeFromSuperview];
    sendArray = [[NSArray alloc] initWithArray:data];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if (singletonClass.isPayFirst == NO && singletonClass.isPayBack == NO) {
         singletonClass.sendDic = [[NSDictionary alloc] initWithDictionary:[data objectAtIndex:0]];
    }
    sendDic = singletonClass.sendDic;
    NSMutableAttributedString *str;
    if (sendDic == nil || [sendDic isKindOfClass:[NSNull class]]) {
        str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"总金额: ￥%.2f", self.money]];
    }else
    {
        str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"总金额: ￥%.2f", self.money + [[sendDic objectForKey:@"price"] floatValue]]];
    }
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0,4)];
    self.lbl_money.attributedText = str;
    if (singletonClass.isPayBack == NO) {
        [self initPay];
    }
    [self.tableView reloadData];
}
-(void)syncGetSendFailed:(NSString*)errMsg;
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errMsg];
}
//获取收货地址成功回调
-(void)syncGetAddressSuccess:(NSArray *)data
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.addressDic = [[NSDictionary alloc] initWithDictionary:[data objectAtIndex:0]];
    addressDic = [[NSDictionary alloc] initWithDictionary:singletonClass.addressDic];
    if ([[addressDic objectForKey:@"contactAddID"] isKindOfClass:[NSNull class]]) {
        [loadingView removeFromSuperview];
        // [self.tableView reloadData];
    }else
    {
        [self initSend];
    }
    // [self.tableView reloadData];
}

-(void)syncGetAddressFailed:(NSString*)errMsg
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errMsg];
}

//无网络连接
- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}
- (void)initPay
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    for (int i = 0; i < payArray.count; i++) {
        if ([[payArray objectAtIndex:i] isEqualToString:@"1"]) {
            switch (i) {
                case 0:
                    payStr = @"支付宝客户端支付";
                    break;
                case 1:
                    payStr = @"支付宝网页支付";
                    break;
                case 2:
                    payStr = @"货到付款";
                    if ([[[sendDic objectForKey:@"consumeTypeId"] substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"1"] || [[[sendDic objectForKey:@"consumeTypeId"] substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"3"]) {
                        payStr = @"";
                    }
                    break;
                case 3:
                    payStr = @"到店支付";
                    if ([[[sendDic objectForKey:@"consumeTypeId"] substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"1"] || [[[sendDic objectForKey:@"consumeTypeId"] substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"2"]) {
                        payStr = @"";
                    }
                    break;
                default:
                    break;
            }
            
            if (![payStr isEqualToString:@""]) {
                break;
            }
        }
        payStr = @"";
    }
    if (sendDic == nil) {
        payStr = @"";
    }
    singletonClass.payMent = payStr;
}

#pragma mark Section Click
- (void)singleTap:(UITapGestureRecognizer*)recognizer
{
    if(sectionSelected == 1){
        sectionSelected = -1;
        isfirst = NO;
    }else{
        sectionSelected = 1;
        isfirst = YES;
    }
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
    if(sectionSelected != -1){
        //判断该一级菜单下的二级菜单是否超过当前屏幕
        NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:1];
        [self.tableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    [self.tableView reloadData];
    
}

- (void)addressTap:(UITapGestureRecognizer*)recognizer
{
    FYAddressViewController *addressView = [[FYAddressViewController alloc] initWithNibName:@"FYAddressViewController" bundle:nil];
    [self.navigationController pushViewController:addressView animated:YES];
    
}

- (void)singleTapSend:(UITapGestureRecognizer*)recognizer
{
    if ([[addressDic objectForKey:@"contactAddID"] isKindOfClass:[NSNull class]]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请选择收货地址" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    FYSendViewController *sendVC = [[FYSendViewController alloc] initWithNibName:@"FYSendViewController" bundle:nil];
    sendVC.sendArray = sendArray;
    [self.navigationController pushViewController:sendVC animated:YES];
}

- (void)singleTapPay:(UITapGestureRecognizer*)recognizer
{
    if ([[addressDic objectForKey:@"contactAddID"] isKindOfClass:[NSNull class]]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请选择收货地址" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if ([[sendDic objectForKey:@"consumeTypeId"] isKindOfClass:[NSNull class]]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请选择配送方式" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    FYHowPayViewController *PayVC = [[FYHowPayViewController alloc] initWithNibName:@"FYHowPayViewController" bundle:nil];
    if ([payStr isEqualToString:@""]) {
      //  payArray = @[];
    }
    PayVC.payArray = payArray;
    [self.navigationController pushViewController:PayVC animated:YES];
}

#pragma mark tableView dataSourse

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        addressView *aView = [[addressView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addressTap:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [aView addGestureRecognizer:tapGestureRecognizer];
        if ([addressDic objectForKey:@"contactName"] == nil || [[addressDic objectForKey:@"contactAddID"] isKindOfClass:[NSNull class]]) {
            aView.lbl_name.text = @"";
            aView.lbl_phone.text = @"";
            aView.lbl_city.text = @"";
            aView.lbl_noData.hidden = NO;
        }else
        {
        aView.lbl_name.text = [addressDic objectForKey:@"contactName"];
        aView.lbl_phone.text = [addressDic objectForKey:@"contactPhone"];
        aView.lbl_city.text = [NSString stringWithFormat:@"%@%@%@%@", [addressDic objectForKey:@"province"], [addressDic objectForKey:@"city"], [addressDic objectForKey:@"district"],[addressDic objectForKey:@"addressName"]];
        aView.lbl_noData.hidden = YES;
        }
        return aView;

    }else if(section == 1)
    {
        listTableViewCell *listCell = [[listTableViewCell alloc] init];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [listCell addGestureRecognizer:tapGestureRecognizer];
        listCell.lbl_count.text = [NSString stringWithFormat:@"共%d件", self.count];
        if (self.isTuan) {
            int count1 = 0;
            for (int i = 0; i < groupArray.count; i++) {
                count1 += [[[groupArray objectAtIndex:i] objectForKey:@"count"] intValue];
            }
            listCell.lbl_count.text = [NSString stringWithFormat:@"共%d件", self.count * count1];
        }
        listCell.lbl_money.text = [NSString stringWithFormat:@"￥%.2f", self.money];
        if (isfirst) {
            listCell.imageView_1.image = [UIImage imageNamed:@"quanYiUpArrows.png"];
        }else
        {
            listCell.imageView_1.image = [UIImage imageNamed:@"yuanYiDownArrows.png"];
        }
        return listCell;
        
    }else if (section == 2)
    {
        FYPayForView *payView = [[FYPayForView alloc] init];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapSend:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [payView addGestureRecognizer:tapGestureRecognizer];
        if (sendDic == nil) {
            payView.lbl_second.text = @"";
        }else
        {
        payView.lbl_second.text = [sendDic objectForKey:@"consumeType"];
        }
        payView.lbl_first.text = @"配送方式";
        return payView;
    }else if (section == 3)
    {
        //WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        FYPayForView *payView = [[FYPayForView alloc] init];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapPay:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [payView addGestureRecognizer:tapGestureRecognizer];
        if (payStr == nil) {
            payView.lbl_second.text = @"";
        }else
        {
        payView.lbl_second.text = payStr;
        }
        payView.lbl_first.text = @"付款方式";
        return payView;
    }else
    {
        FYLeaveView *leaveView = [[FYLeaveView alloc] init];
        leaveView.textView.delegate = self;
        leaveView.textView.text = remark;
        return leaveView;
    }
    
    return nil;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //二级分类的数量
    if(section != 1){
        return 0;
    }else{
     
        if(sectionSelected == -1){
            return 0;
        }else{
            if (self.isTuan) {
                return groupArray.count;
            }
            return self.productArray.count;
        }
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *str = @"cell";
    FYDetailTableViewCell *cell = (FYDetailTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"FYDetailTableViewCell" owner:self options:nil] lastObject];;
    
    if (cell == nil) {
        cell = (FYDetailTableViewCell *)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (self.isTuan) {
        NSDictionary *shop1 = [[NSDictionary alloc] initWithDictionary:[groupArray objectAtIndex:indexPath.row]];
        cell.lbl_name.text = [NSString stringWithFormat:@"%@", [shop1 objectForKey:@"title"]];
        cell.lbl_price.text = [NSString stringWithFormat:@"￥%@", [shop1 objectForKey:@"price"]];
        cell.lbl_count.text = [NSString stringWithFormat:@"共%d件", [[shop1 objectForKey:@"count"] intValue] * self.count];
        cell.lbl_size.text = @"";
        cell.imageView_product.type = 1;
        cell.imageView_product.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
        cell.imageView_product.imageURL = [NSString stringWithFormat:@"%@", [shop1 objectForKey:@"picUrl"]];
        return cell;
    }
    else{
        ShopCart *shop = (ShopCart *)[self.productArray objectAtIndex:indexPath.row];
        cell.lbl_name.text = shop.title;
        cell.lbl_price.text = [NSString stringWithFormat:@"%@", shop.price];
        cell.lbl_count.text = [NSString stringWithFormat:@"数量:%@", shop.count];
        NSDictionary *dic = [[NSDictionary alloc] initWithDictionary:shop.mode];
        NSMutableString *muString = [NSMutableString stringWithFormat:@""];
        for (int i = 0; i < ([dic allKeys].count - 1) / 2; i++) {
            [muString appendString:[NSString stringWithFormat:@"%@:%@  ",[dic objectForKey:[NSString stringWithFormat:@"type%d", i + 1]],[dic objectForKey:[NSString stringWithFormat:@"value%d", i + 1]]]];
    }
        cell.lbl_size.text = muString;
        cell.imageView_product.type = 1;
        cell.imageView_product.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
        cell.imageView_product.imageURL = shop.pic_url;
        return cell;
    }
}

- (NSString *)decimalwithFormat:(NSString *)format floatV:(float)floatV
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    
    [numberFormatter setPositiveFormat:format];
    
    return  [numberFormatter stringFromNumber:[NSNumber numberWithFloat:floatV]];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 100;
    }
    if (section == 1) {
        return 60;
    }
    if (section == 2) {
        return 50;
    }
    if (section == 3) {
        return 50;
    }
    if (section == 4) {
        return 125;
    }
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(sectionSelected == -1 ){
        return 0;
    }else{
        //被选中的一级菜单下的二级菜单
        if(indexPath.section == sectionSelected){
            return 80;
            //广告位置固定为152
        }else{
            return 0;
        }
    }
}
#pragma mark UITextView delegate
- (void)textViewDidEndEditing:(UITextView *)textView
{
    remark = textView.text;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    self.tableView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height);
    [UIView commitAnimations];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    if (textView.text.length > 200) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"字数不能大于200!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return NO;
    }
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    self.tableView.frame = CGRectMake(0, -168, self.tableView.frame.size.width, self.tableView.frame.size.height);
    [UIView commitAnimations];
}
#pragma mark Order Click
//提交订单
- (IBAction)btn_orderClick:(id)sender
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [[UIApplication sharedApplication].keyWindow addSubview:loadingView];
   
    if ([payStr isEqualToString:@"支付宝客户端支付"]) {
        payment = 1;
    }
    if ([payStr isEqualToString:@"支付宝网页支付"]) {
        payment = 2;
    }
    if ([payStr isEqualToString:@"货到付款"]) {
        payment = 4;
    }
    if ([payStr isEqualToString:@"到店支付"]) {
        payment = 3;
    }
    if (self.isTuan)
    {
        //是团购
        if ([addressDic objectForKey:@"contactName"] == nil || [[addressDic objectForKey:@"contactAddID"] isKindOfClass:[NSNull class]]) {
            [WXCommonViewClass showHudInView:self.view title:@"请选择收货地址"];
            [loadingView removeFromSuperview];
            return;
        }else if([[singletonClass.sendDic
                   objectForKey:@"consumeTypeId"] isEqualToString:@""] || [singletonClass.sendDic objectForKey:@"consumeTypeId"] == nil)
        {
            [WXCommonViewClass showHudInView:self.view title:@"请选择配送方式"];
            [loadingView removeFromSuperview];
            return;
        }else if (payment == 0)
        {
            [WXCommonViewClass showHudInView:self.view title:@"请选择支付方式"];
            [loadingView removeFromSuperview];
            return;
        }
        else{
        NSMutableArray *orderArray = [[NSMutableArray alloc] initWithCapacity:10];
        NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[[self.productArray objectAtIndex:0] objectForKey:@"groupbuyID"],@"groupBuyId", [NSNumber numberWithInt:self.count],@"groupBuyNumber",nil];
        [orderArray addObject:dic];
        NSString *jsonStr = [WXCommonDataClass toJsonStr:orderArray];
        [[FY_Cloud_Pay share] setDelegate:(id)self];
        [[FY_Cloud_Pay share] getOrderWithGoods:@"" andGroupBuys:jsonStr andRemark:remark andPayment:payment andContactld:[singletonClass.addressDic objectForKey:@"contactAddID"] andConsumeType:[singletonClass.sendDic objectForKey:@"consumeTypeId"]];
        }
    }else
    {
        //不是团购
     //   NSMutableArray *orderArray = [[NSMutableArray alloc] initWithCapacity:10];
        NSMutableString *orderString = [[NSMutableString alloc] initWithString:@"{"];
        for (int i = 0; i < self.productArray.count; i++) {
            ShopCart *shop = (ShopCart *)[self.productArray objectAtIndex:i];
      //      NSMutableDictionary *modeDic = [[NSMutableDictionary alloc] initWithCapacity:5];
            NSString *pStr = @"\"property\":{";
            for (int i = 0; i < ([shop.mode allKeys].count - 1) / 2; i++) {
                //[modeDic setObject:[shop.mode objectForKey:[NSString stringWithFormat:@"value%d", i + 1]] forKey:[shop.mode objectForKey:[NSString stringWithFormat:@"type%d", i + 1]]];
               // [modeDic setObject:[NSString stringWithFormat:@"%d", i] forKey:[NSString stringWithFormat:@"%d%d", i, i]];
                if (![[shop.mode objectForKey:[NSString stringWithFormat:@"type%d", i + 1]]  isEqual:@""]) {
                    pStr = [pStr stringByAppendingString:[NSString stringWithFormat:@"\"%@\":\"%@\",", [shop.mode objectForKey:[NSString stringWithFormat:@"type%d", i + 1]], [shop.mode objectForKey:[NSString stringWithFormat:@"value%d", i + 1]]]];
                }
            }
            NSMutableString *propertyStr = [[NSMutableString alloc] initWithString:pStr];
            [propertyStr deleteCharactersInRange:NSMakeRange(propertyStr.length - 1, 1)];
            pStr = [propertyStr stringByAppendingString:@"}"];
            pStr = [orderString stringByAppendingString:[NSString stringWithFormat:@"\"dishID\":\"%@\",\"count\":%@,%@},", shop.productid, shop.count, pStr]];
          //  NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:shop.productid,@"dishID", shop.count,@"count", modeDic, @"property",nil];
           // [orderArray addObject:dic];
            orderString = [NSMutableString stringWithString:pStr];
            //[orderString appendString:pStr];
            [orderString appendString:@"{"];
        }
       // [orderString deleteCharactersInRange:NSMakeRange(0, 1)];
        [orderString deleteCharactersInRange:NSMakeRange(orderString.length - 2, 2)];
        NSString *json = [NSString stringWithFormat:@"[%@]", orderString];
      //  NSString *jsonStr = [WXCommonDataClass toJsonStr:orderArray];
        if ([addressDic objectForKey:@"contactName"] == nil || [[addressDic objectForKey:@"contactAddID"] isKindOfClass:[NSNull class]]) {
            [WXCommonViewClass showHudInView:self.view title:@"请选择收货地址"];
            [loadingView removeFromSuperview];
            return;
        }else if([[singletonClass.sendDic
                   objectForKey:@"consumeTypeId"] isEqualToString:@""] || [singletonClass.sendDic objectForKey:@"consumeTypeId"] == nil)
        {
            [WXCommonViewClass showHudInView:self.view title:@"请选择配送方式"];
            [loadingView removeFromSuperview];
            return;
        }else if (payment == 0)
        {
            [WXCommonViewClass showHudInView:self.view title:@"请选择支付方式"];
            [loadingView removeFromSuperview];
            return;
        }
        else{
        [[FY_Cloud_Pay share] setDelegate:(id)self];
        [[FY_Cloud_Pay share] getOrderWithGoods:json andGroupBuys:@"" andRemark:remark andPayment:payment andContactld:[singletonClass.addressDic objectForKey:@"contactAddID"] andConsumeType:[singletonClass.sendDic objectForKey:@"consumeTypeId"]];
        }
    }
}
//获得订单号成功回调
-(void)syncGetPayOrderSuccess:(NSDictionary *)data
{
     [loadingView removeFromSuperview];
    XYOrderPayViewController * orderPayVC = [[XYOrderPayViewController alloc]initWithNibName:@"XYOrderPayViewController" bundle:nil];
    if (self.isTuan == NO) {
        for (int i = 0; i < self.productArray.count; i++) {
            ShopCart *shop = (ShopCart *)[self.productArray objectAtIndex:i];
            [WXShoppingCartDatabaseClass deleteOneProductCart:shop];
        }
    }
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if ([[[singletonClass.sendDic objectForKey:@"consumeTypeId"] substringWithRange:NSMakeRange(0, 1)]isEqualToString:@"3"]) {
        orderPayVC.selfTakeFlag = YES;
    }else
    {
        orderPayVC.selfTakeFlag = NO;
    }
    orderPayVC.orderID = [data objectForKey:@"orderID"];
    orderPayVC.money =  [NSString stringWithFormat:@"￥%.2f",self.money + [[sendDic objectForKey:@"price"] floatValue]];
    orderPayVC.payStyle = [NSString stringWithFormat:@"%i",payment];
    orderPayVC.isTuan = self.isTuan;
    orderPayVC.payStyleText = payStr;
    [self.navigationController pushViewController:orderPayVC animated:YES];
    
}

-(void)syncGetPayOrderFailed:(NSString*)errMsg
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errMsg];
}


@end
