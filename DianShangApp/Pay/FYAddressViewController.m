//
//  FYAddressViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYAddressViewController.h"
#import "WXCommonViewClass.h"
#import "XYManageAddressViewController.h"
#import "FY_Cloud_getAddress.h"
@interface FYAddressViewController ()
{
    NSString *addID;
    NSArray *addressArray;
}
@end

@implementation FYAddressViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"选择收货地址";
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    [self initButton];
    addressArray = [[NSArray alloc] init];
}

- (void)initButton
{
    UIButton *moreBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:NSLocalizedString(@"管理", @"选择收货地址/管理")];
    [moreBtn addTarget:self action:@selector(addressBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:moreBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
}

- (void)viewWillAppear:(BOOL)animated
{
    
    [[FY_Cloud_getAddress share] setDelegate:(id)self];
    [[FY_Cloud_getAddress share] getAddressWithIsOnlyDefault:0];

}

-(void)viewWillDisappear:(BOOL)animated
{
    [[FY_Cloud_getAddress share] setDelegate:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)addressBtnPressed
{
    XYManageAddressViewController *manageVC = [[XYManageAddressViewController alloc] initWithNibName:@"XYManageAddressViewController" bundle:nil];
    [self.navigationController pushViewController:manageVC animated:YES];
    
}

- (void)backBtnPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark 网络回调
-(void)syncGetAddressSuccess:(NSArray *)data
{
    addressArray = [NSArray arrayWithArray:data];
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    for (int i = 0; i < data.count; i++) {
        if ([[[data objectAtIndex:i] objectForKey:@"isDefault"] intValue] == 1) {
            if ([singletonClass.addressDic objectForKey:@"contactName"] == nil || [[singletonClass.addressDic objectForKey:@"contactAddID"] isKindOfClass:[NSNull class]]) {
                singletonClass.addressDic = [data objectAtIndex:i];
                break;
            }
        }
    }
    addID = [singletonClass.addressDic objectForKey:@"contactAddID"];
    [self.tableView reloadData];
}

-(void)syncGetAddressFailed:(NSString*)errMsg
{
    [WXCommonViewClass showHudInView:self.view title:errMsg];
}

- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [WXCommonViewClass showHudInView:[UIApplication sharedApplication].keyWindow title:errorCode];
}

#pragma mark tableView DataSourse
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return addressArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d%d", [indexPath section], [indexPath row]];//以indexPath来唯一确定cell
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault
                                     reuseIdentifier:CellIdentifier];
    }
    

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dic = [[NSDictionary alloc] initWithDictionary:[addressArray objectAtIndex:indexPath.row]];
    cell.contentView.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0];
    UIImageView * backgroundImg = [[UIImageView alloc]init];
    [backgroundImg setFrame:CGRectMake(0, 13, 298, 100)];
    [backgroundImg setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"address_bg" ofType:@"png"]]];
    //选中标记
    UIImageView *yesImage = [[UIImageView alloc] initWithFrame:CGRectMake(250, 56, 14, 11)];
    yesImage.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"address_yes" ofType:@"png"]];
    
    
    UILabel * userTec = [[UILabel alloc]initWithFrame:CGRectMake(12, 36, 120, 20)];
    userTec.text = [dic objectForKey:@"contactName"];
    userTec.backgroundColor = [UIColor clearColor];
    userTec.font = [UIFont systemFontOfSize:12];
    
    UILabel * phone = [[UILabel alloc]initWithFrame:CGRectMake(135, 36, 120, 20)];
    phone.text = [dic objectForKey:@"contactPhone"];
    phone.backgroundColor = [UIColor clearColor];
    phone.font = [UIFont systemFontOfSize:12];
    
    UILabel * cityTec = [[UILabel alloc]initWithFrame:CGRectMake(12, 53, 200, 20)];
    cityTec.text = [NSString stringWithFormat:@"%@%@%@", [dic objectForKey:@"province"],[dic objectForKey:@"city"],[dic objectForKey:@"district"]];
    cityTec.backgroundColor = [UIColor clearColor];
    cityTec.font = [UIFont systemFontOfSize:12];
    
    UILabel * streetTec = [[UILabel alloc]initWithFrame:CGRectMake(12, 60, 230, 50)];
    streetTec.numberOfLines = 2;
    streetTec.text = [dic objectForKey:@"addressName"];
    streetTec.backgroundColor = [UIColor clearColor];
    streetTec.font = [UIFont systemFontOfSize:12];
    
    cell.backgroundColor = [UIColor clearColor];
    [cell.contentView addSubview:backgroundImg];
    [cell.contentView addSubview:userTec];
    [cell.contentView addSubview:cityTec];
    [cell.contentView addSubview:streetTec];
    [cell.contentView addSubview:phone];
    if (addID != nil && ![addID isKindOfClass:[NSNull class]]) {
    if ([addID isEqualToString:[dic objectForKey:@"contactAddID"]])
    {
       [cell.contentView addSubview:yesImage];
       return cell;
    }
    }
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 115;
}

#pragma mark tableView delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
       //     detailVC.style =((FYGifeTableViewCell *)[tableView cellForRowAtIndexPath:indexPath]).lbl_orderStyle.text;
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.addressDic = [addressArray objectAtIndex:indexPath.row];
    singletonClass.isPayFirst = NO;
    singletonClass.isPayBack = NO;
    addID = [singletonClass.addressDic objectForKey:@"contactAddID"];
   // [self.tableView reloadData];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
