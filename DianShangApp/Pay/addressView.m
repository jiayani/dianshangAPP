//
//  addressView.m
//  DianShangApp
//
//  Created by Fuy on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "addressView.h"

@implementation addressView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        addressView *addView = [[[NSBundle mainBundle] loadNibNamed:@"addressView" owner:nil options:
                                     nil] objectAtIndex:0];
        self = addView;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
