//
//  FYSendViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-6-20.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYSendViewController.h"
#import "FYSendCell.h"
#import "WXCommonViewClass.h"
#import "WXCommonSingletonClass.h"
@interface FYSendViewController ()
{
    NSMutableArray *heightArray;
    FYSendCell *cell1;
    NSString *sendID;
}
@end

@implementation FYSendViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"选择配送方式";
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    [self initData];

}

- (void)initData
{
    heightArray = [[NSMutableArray alloc] initWithCapacity:10];
    for (int i = 0; i < self.sendArray.count; i++) {
        float txtheight = 0.0;
        if ([[[self.sendArray objectAtIndex:i] objectForKey:@"ownDeliveryAddr"]isEqualToString:@""]) {
            [heightArray addObject:[NSNumber numberWithFloat:txtheight]];
        }
        else
        {
            txtheight = [self heightForTextView:cell1.textView andText:[NSString stringWithFormat:@"自提地址: %@", [[self.sendArray objectAtIndex:i] objectForKey:@"ownDeliveryAddr"]]];
            [heightArray addObject:[NSNumber numberWithFloat:txtheight]];
        }
    }
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    sendID = [singletonClass.sendDic objectForKey:@"consumeTypeId"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UITableView dataSourse
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.sendArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    FYSendCell *cell = (FYSendCell *)[[[NSBundle  mainBundle]  loadNibNamed:@"FYSendCell" owner:self options:nil]  lastObject];
    if (cell == nil){
        cell = [[FYSendCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textView.frame = CGRectMake(17, 26, cell.textView.frame.size.width, [[heightArray objectAtIndex:indexPath.row] floatValue]);
    cell.textView.text = [NSString stringWithFormat:@"自提地址: %@", [[self.sendArray objectAtIndex:indexPath.row] objectForKey:@"ownDeliveryAddr"]];
    cell.lbl_style.text = [[self.sendArray objectAtIndex:indexPath.row] objectForKey:@"consumeType"];
    
    if ([[[self.sendArray objectAtIndex:indexPath.row] objectForKey:@"consumeType"] isEqualToString:@"用户自提"]) {
        cell.lbl_money.text = @"";
    }
    else{
        cell.lbl_money.text = [NSString stringWithFormat:@"运费:%@", [[self.sendArray objectAtIndex:indexPath.row] objectForKey:@"price"]];
    }
    
    if ([[[self.sendArray objectAtIndex:indexPath.row] objectForKey:@"consumeTypeId"] isEqualToString:sendID]) {
        UIImageView *yesImage = [[UIImageView alloc] initWithFrame:CGRectMake(290, 10, 14, 11)];
        yesImage.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"address_yes" ofType:@"png"]];
        [cell.contentView addSubview:yesImage];

    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35 + [[heightArray objectAtIndex:indexPath.row] floatValue];
}

- (float)heightForTextView:(UITextView *)textView andText: (NSString *)strText{
    float fPadding = 16.0;
    CGSize constraint = CGSizeMake(276 - fPadding, CGFLOAT_MAX);
    
    CGSize size = [strText sizeWithFont: [UIFont systemFontOfSize:14] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    float fHeight = size.height + 23.0;
    
    return fHeight;
}
#pragma mark UITableView delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    singletonClass.sendDic = [self.sendArray objectAtIndex:indexPath.row];
    singletonClass.isPayFirst = YES;
    [self.navigationController popViewControllerAnimated:YES];
}

@end
