//
//  FYDetailTableViewCell.h
//  DianShangApp
//
//  Created by Fuy on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AsynImageView;
@interface FYDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet AsynImageView *imageView_product;
@property (weak, nonatomic) IBOutlet UILabel *lbl_color;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_price;
@property (weak, nonatomic) IBOutlet UILabel *lbl_count;
@property (weak, nonatomic) IBOutlet UILabel *lbl_size;

@end
