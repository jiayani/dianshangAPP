//
//  FYGiftSuccessViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-6-25.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYGiftSuccessViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lbl_order;
@property (weak, nonatomic) IBOutlet UILabel *lbl_money;

@property (strong, nonatomic) NSString *orderID;
@property (strong, nonatomic) NSString *money;
- (IBAction)btn_continueClick:(id)sender;
@end
