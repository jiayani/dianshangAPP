//
//  FYPayViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYPayViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lbl_money;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_count;
@property (nonatomic, retain) NSArray *productArray;
@property (nonatomic) bool isTuan;
@property (nonatomic) int count;
- (IBAction)btn_orderClick:(id)sender;
@property (nonatomic) double money;
@property (nonatomic, strong) NSString *payStr;
@end
