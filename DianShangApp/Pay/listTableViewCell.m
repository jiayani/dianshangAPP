//
//  listTableViewCell.m
//  DianShangApp
//
//  Created by Fuy on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "listTableViewCell.h"

@implementation listTableViewCell

- (id)init{
    listTableViewCell *firstMenuCell = [[[NSBundle mainBundle] loadNibNamed:@"listTableViewCell" owner:nil options:
                                       nil] objectAtIndex:0];

    return firstMenuCell;
}


- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
