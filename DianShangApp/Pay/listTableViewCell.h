//
//  listTableViewCell.h
//  DianShangApp
//
//  Created by Fuy on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface listTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_count;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_money;
@property (weak, nonatomic) IBOutlet UILabel *lbl_zongji;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_1;


@end
