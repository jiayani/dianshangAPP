//
//  FYLeaveView.m
//  DianShangApp
//
//  Created by Fuy on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYLeaveView.h"

@implementation FYLeaveView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        FYLeaveView *leaveView = [[[NSBundle mainBundle] loadNibNamed:@"FYLeaveView" owner:nil options:
                                     nil] objectAtIndex:0];
        self = leaveView;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
