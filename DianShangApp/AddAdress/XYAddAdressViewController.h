//
//  XYAddAdressViewController.h
//  DianShangApp
//
//  Created by wa on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XYLoadingView.h"
@interface XYAddAdressViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,UIPickerViewDataSource, UIPickerViewDelegate,UIAlertViewDelegate>
{
    NSString * province;
    NSString * city;
    NSString * district;
    int isDefault;
    XYLoadingView * loadingView;
    NSString * isFirstFlag;
}
@property (weak, nonatomic) IBOutlet UISwitch *swithButton;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UITextField *contactTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *postalTextField;
@property (weak, nonatomic) IBOutlet UITextField *areaTextField;
@property (weak, nonatomic) IBOutlet UITextView *streetTextView;
@property (retain, nonatomic) UITextField *myEditingTextField;
@property (retain, nonatomic) UITextView *myEditingTextView;
@property (retain, nonatomic) UIToolbar *toolbar;
@property (retain, nonatomic) UIPickerView *areaPicker;
@property (strong, nonatomic) NSMutableArray *pickRowOne;
@property (strong, nonatomic) NSMutableArray *pickRowTwo;
@property (strong, nonatomic) NSMutableArray *pickRowThree;
@property (retain, nonatomic) NSString *filePath;
- (IBAction)switchAction:(id)sender;
@end
