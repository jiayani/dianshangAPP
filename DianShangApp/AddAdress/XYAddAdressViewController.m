//
//  XYAddAdressViewController.m
//  DianShangApp
//
//  Created by wa on 14-5-26.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XYAddAdressViewController.h"
#import "WXCommonViewClass.h"
#import "sqlite3.h"
#import "XY_Cloud_AddContactAddress.h"
@interface XYAddAdressViewController ()

@end

@implementation XYAddAdressViewController
@synthesize filePath;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"收货地址管理", @"收货地址管理");
        [WXCommonViewClass hideTabBar:self isHiden:YES];
        /*设置导航返回按钮*/
        UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
        [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
        self.navigationItem.leftBarButtonItem = leftBarButtonItem;
        
        UIButton * cancleBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:NSLocalizedString(@"确认添加", @"收货地址管理/确认添加")];
        [cancleBtn addTarget:self action:@selector(confirmAddBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:cancleBtn];
        self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    isFirstFlag = @"isFirstFlag";
    [self initController];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [[XY_Cloud_AddContactAddress share] setDelegate:nil];
}
- (void)viewDidUnload {
    [super viewDidUnload];
    self.toolbar = nil;
    self.areaTextField = nil;
    self.streetTextView = nil;
    self.postalTextField = nil;
    self.contactTextField = nil;
    self.phoneTextField = nil;
    self.myEditingTextField = nil;
    self.myEditingTextView = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initController {
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0) {
        [self.swithButton setFrame:CGRectMake(233, 269, 31, 31)];
    }
    
    self.pickRowOne = [[NSMutableArray alloc]init];
    self.pickRowTwo = [[NSMutableArray alloc]init];
    self.pickRowThree = [[NSMutableArray alloc]init];
    
    [self copyFileDatabaseAndGetDbFilePath];
    [self seleceProNameFromDB];
    [self seleceCityNameFromDB:@"1"];
    [self seleceZoneNameFromDB:@"1"];
    
    [self initToolBar];
    
    self.areaPicker = [[UIPickerView alloc]init];
    self.areaPicker.delegate = self;
    self.areaPicker.dataSource = self;
    self.areaPicker.showsSelectionIndicator = YES;
    self.areaPicker.tag = 11;
    self.areaTextField.inputView = self.areaPicker;
    
    //添加ToolBar
    self.areaTextField.inputAccessoryView = self.toolbar;
    self.streetTextView.inputAccessoryView = self.toolbar;
    self.postalTextField.inputAccessoryView = self.toolbar;
    self.contactTextField.inputAccessoryView = self.toolbar;
    self.phoneTextField.inputAccessoryView = self.toolbar;
}

//单击完成时，隐藏键盘
-(IBAction)resignKeyboard:(id)sender
{
    
    [self.myEditingTextField resignFirstResponder];
    [self.streetTextView resignFirstResponder];
    [self.mainScrollView setContentOffset:CGPointMake(0, 0)];
    //    self.srollView.scrollEnabled = NO;
    
}
//单击前一项按钮执行的操作
-(IBAction)previousField:(id)sender
{
    id firstResponder = self.myEditingTextField;
    if(firstResponder == nil){
        firstResponder = self.myEditingTextView;
    }
    if ([firstResponder isKindOfClass:[UITextField class]]) {
        NSUInteger tag = [(UITextField *)firstResponder tag];
        NSUInteger previousTag = tag ==1 ? 1 : tag-1;
        [self checkBarButton:previousTag];
        
        UITextField *previousField = (UITextField *)[self.view viewWithTag:previousTag];
        [previousField becomeFirstResponder];
        
    }
    if ([firstResponder isKindOfClass:[UITextView class]]) {
        NSUInteger tag = [(UITextView *)firstResponder tag];
        NSUInteger previousTag = tag ==1 ? 1 : tag-1;
        [self checkBarButton:previousTag];
        
        UITextField *previousField = (UITextField *)[self.view viewWithTag:previousTag];
        [previousField becomeFirstResponder];
        
    }
    
}
//单击下一项按钮执行的操作
-(IBAction)nextField:(id)sender
{
    id firstResponder = self.myEditingTextField;
    if(firstResponder == nil){
        firstResponder = self.myEditingTextView;
    }
    if ([firstResponder isKindOfClass:[UITextField class]]) {
        NSUInteger tag = [(UITextField *)firstResponder tag];
        NSUInteger nextTag=tag == 5 ? 5 : tag+1;
        [self checkBarButton:nextTag];
        
        UITextField *previousField = (UITextField *)[self.view viewWithTag:nextTag];
        [previousField becomeFirstResponder];
        
    }
    if ([firstResponder isKindOfClass:[UITextView class]]) {
        NSUInteger tag = [(UITextView *)firstResponder tag];
        NSUInteger nextTag = tag ==5 ? 5 : tag+1;
        [self checkBarButton:nextTag];
        
        UITextField *previousField = (UITextField *)[self.view viewWithTag:nextTag];
        [previousField becomeFirstResponder];
        
    }
    
}

/*初始化toolbar*/
- (void)initToolBar{
    
    if (self.toolbar == nil) {
        self.toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 38.0f)];
        self.toolbar.barStyle = UIBarStyleBlackTranslucent;
        
        UIBarButtonItem *preBarItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"上一项", @"上一项")
                                                                       style:UIBarButtonItemStyleBordered
                                                                      target:self
                                                                      action:@selector(previousField:)];
        UIBarButtonItem *nextBarItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"下一项", @"下一项")
                                                                        style:UIBarButtonItemStyleBordered
                                                                       target:self
                                                                       action:@selector(nextField:)];
        
        UIBarButtonItem *spaceBarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                      target:nil
                                                                                      action:nil];
        
        UIBarButtonItem *doneBarItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"完成", @"完成")
                                                                        style:UIBarButtonItemStyleDone
                                                                       target:self
                                                                       action:@selector(resignKeyboard:)];
        
        [self.toolbar setItems:[NSArray arrayWithObjects:preBarItem,nextBarItem,spaceBarItem,doneBarItem,nil]];
        
    }
    
}
//检查previous和nextbutton 可不可用
-(void)checkBarButton:(NSUInteger)tag
{
    UIBarButtonItem *previousBarItem=(UIBarButtonItem *)[[self.toolbar items] objectAtIndex:0];
    UIBarButtonItem *nextBarItem=(UIBarButtonItem *)[[self.toolbar items] objectAtIndex:1];
    
    [previousBarItem setEnabled:tag == 1 ? NO: YES];
    [nextBarItem setEnabled:tag == 5 ? NO : YES];
}


-(void)copyFileDatabaseAndGetDbFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory,NSUserDomainMask, YES);
    self.filePath  = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"china_province_city_zone.db"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:self.filePath]) {
//        NSLog(@"文件已经存在了");
    }
    else {
        NSString *resourceSampleDBFolderPath =[[NSBundle mainBundle]pathForResource:@"china_province_city_zone" ofType:@"db"];
        NSData *mainBundleFile = [NSData dataWithContentsOfFile:resourceSampleDBFolderPath];
        [[NSFileManager defaultManager] createFileAtPath:self.filePath
                                                contents:mainBundleFile
                                              attributes:nil];
    }
    
}

- (void)seleceProNameFromDB{
    
    sqlite3 *selectDateBase = nil;
    if(sqlite3_open([self.filePath UTF8String], &selectDateBase) == SQLITE_OK) {
        NSMutableString *sqlStr = [[NSMutableString alloc] init];
        
        [sqlStr appendFormat:@"select * from T_Province;"];
        
        const char *sqlChar = [sqlStr UTF8String];
        
        sqlite3_stmt *sqlStmt = nil;
        if(sqlite3_prepare_v2(selectDateBase, sqlChar, -1, &sqlStmt, nil)
           == SQLITE_OK) {
            while(sqlite3_step(sqlStmt) == SQLITE_ROW) {
                
                char *name = (char *)sqlite3_column_text(sqlStmt, 0);
                NSString *nameString = [[NSString alloc] initWithUTF8String:name];
                
                char *proID = (char *)sqlite3_column_text(sqlStmt, 1);
                NSString *proIDString = [[NSString alloc] initWithUTF8String:proID];
                
                NSMutableDictionary * commonDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:nameString,@"nameString",proIDString,@"proIDString",nil];
                [_pickRowOne addObject:commonDic];
                
            }
            
        }
        sqlite3_finalize(sqlStmt);
    }
    sqlite3_close(selectDateBase);
    
}
- (void)seleceCityNameFromDB:(NSString *)index{
    
    sqlite3 *selectDateBase = nil;
    if(sqlite3_open([self.filePath UTF8String], &selectDateBase) == SQLITE_OK) {
        NSMutableString *sqlStr = [[NSMutableString alloc] init];
        
        [sqlStr appendFormat:@"select * from T_City where ProID = '%@';",index];
        
        const char *sqlChar = [sqlStr UTF8String];
        
        sqlite3_stmt *sqlStmt = nil;
        if(sqlite3_prepare_v2(selectDateBase, sqlChar, -1, &sqlStmt, nil)
           == SQLITE_OK) {
            while(sqlite3_step(sqlStmt) == SQLITE_ROW) {
                
                char *name = (char *)sqlite3_column_text(sqlStmt, 0);
                NSString *nameString = [[NSString alloc] initWithUTF8String:name];
                
                char *citySort = (char *)sqlite3_column_text(sqlStmt, 2);
                NSString *citySortString = [[NSString alloc] initWithUTF8String:citySort];
                
                NSMutableDictionary * commonDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:nameString,@"nameString",citySortString,@"citySortString",nil];
                
                [_pickRowTwo addObject:commonDic];
            }
            
        }
        sqlite3_finalize(sqlStmt);
    }
    sqlite3_close(selectDateBase);
    
}
- (void)seleceZoneNameFromDB:(NSString *)index{
    
    sqlite3 *selectDateBase = nil;
    if(sqlite3_open([self.filePath UTF8String], &selectDateBase) == SQLITE_OK) {
        NSMutableString *sqlStr = [[NSMutableString alloc] init];
        
        [sqlStr appendFormat:@"select ZoneName from T_Zone where CityID = '%@';",index];
        
        const char *sqlChar = [sqlStr UTF8String];
        
        sqlite3_stmt *sqlStmt = nil;
        if(sqlite3_prepare_v2(selectDateBase, sqlChar, -1, &sqlStmt, nil)
           == SQLITE_OK) {
            while(sqlite3_step(sqlStmt) == SQLITE_ROW) {
                
                char *name = (char *)sqlite3_column_text(sqlStmt, 0);
                NSString *nameString = [[NSString alloc] initWithUTF8String:name];
                [_pickRowThree addObject:nameString];
            }
            
        }
        sqlite3_finalize(sqlStmt);
    }
    sqlite3_close(selectDateBase);
    
}
//返回按钮事件
- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)confirmAddBtnPressed:(id)sender {
    
     if ([[self checkOrderInfor] isEqualToString:@""]) {
         
         NSString * postfixUrl = [NSString stringWithFormat:@"isDefault=%i&contactName=%@&contactPhone=%@&addressName=%@&zipCode=%@&province=%@&city=%@&district=%@",isDefault,self.contactTextField.text,self.phoneTextField.text,self.streetTextView.text,self.postalTextField.text,province,city,district];
         loadingView = [[XYLoadingView alloc] initWithType:2];
         [self.view addSubview:loadingView];
         [[XY_Cloud_AddContactAddress share] setDelegate:(id)self];
         [[XY_Cloud_AddContactAddress share] addContactAndAddress:postfixUrl];
         
     }
     else {
          [self myAlertView:[self checkOrderInfor]];
     }
    
}

-(void)addContactAndAdressSuccess:(NSDictionary *)data
{
    [loadingView removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)addContactAndAdressFailed:(NSString*)errMsg
{
    
}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView{
    self.myEditingTextView = textView;
    NSUInteger tag=[textView tag];
    [self checkBarButton:tag];
    self.mainScrollView.scrollEnabled = YES;
    if ([self.streetTextView.text isEqualToString:@""])
    {
        self.streetTextView.text = nil;
        self.streetTextView.font = [UIFont systemFontOfSize:15];
        self.streetTextView.textColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1];
    }
    CGPoint pt;
    CGRect rc = [textView bounds];
    rc = [textView convertRect:rc toView:self.mainScrollView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 60;
    [self.mainScrollView setContentOffset:pt animated:YES];
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    self.myEditingTextView = nil;
    if ([self.streetTextView.text isEqualToString:@""])
    {
        self.streetTextView.text = @"";
        self.streetTextView.font = [UIFont systemFontOfSize:15];
        self.streetTextView.textColor = [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1];
    }
}
#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == self.areaTextField && [isFirstFlag isEqualToString:@"isFirstFlag"]) {
        isFirstFlag = @"";
        self.areaTextField.text = @"北京市北京市东城区";
        province = @"北京市";
        city = @"北京市";
        district = @"东城区";
    }
    self.myEditingTextField = textField;
    NSUInteger tag=[textField tag];
    [self checkBarButton:tag];
    self.mainScrollView.scrollEnabled = YES;
    if ( textField.tag != 1 ) {

    CGPoint pt;
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:self.mainScrollView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 60;
    [self.mainScrollView setContentOffset:pt animated:YES];
        
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.myEditingTextField = nil;
}

#pragma mark -UIPickerView dataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 3;
}
// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0) {
        return _pickRowOne.count;
    }
    else if (component == 1) {
        return _pickRowTwo.count;
    }
    else {
        if (_pickRowThree.count == 0) {
            return 1;
        }
        else {
            return _pickRowThree.count;
        }
    }
}
-(NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (component == 0) {
        return [[_pickRowOne objectAtIndex:row]objectForKey:@"nameString"];
    }
    else if (component == 1) {
        return [[_pickRowTwo objectAtIndex:row]objectForKey:@"nameString"];
    }
    else {
        if (_pickRowThree.count == 0) {
            if (_pickRowTwo.count != 0) {
                NSInteger rowTwo = [pickerView selectedRowInComponent:1];
                return [[_pickRowTwo objectAtIndex:rowTwo]objectForKey:@"nameString"];
            }
            else {
                return nil;
            }
        }
        else {
            return [_pickRowThree objectAtIndex:row];
        }
    }
}
#pragma mark -UIPickerView delegate
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if (component == 0) {
        NSInteger rowOne = [pickerView selectedRowInComponent:0];
        NSString * index = [[_pickRowOne objectAtIndex:rowOne]objectForKey:@"proIDString"];
        _pickRowTwo = [[NSMutableArray alloc]init];
        _pickRowThree = [[NSMutableArray alloc]init];
        [self seleceCityNameFromDB:index];
        [self.areaPicker reloadComponent:1];
        [self.areaPicker selectRow:0 inComponent:1 animated:NO];
        NSInteger rowTwo = [pickerView selectedRowInComponent:1];
        NSString * indexo = [[_pickRowTwo objectAtIndex:rowTwo]objectForKey:@"citySortString"];
        [self seleceZoneNameFromDB:indexo];
        [self.areaPicker reloadComponent:2];
        [self.areaPicker selectRow:0 inComponent:2 animated:NO];
    }
    else if (component == 1) {
        NSInteger rowTwo = [pickerView selectedRowInComponent:1];
        NSString * index = [[_pickRowTwo objectAtIndex:rowTwo]objectForKey:@"citySortString"];
        _pickRowThree = [[NSMutableArray alloc]init];
        [self seleceZoneNameFromDB:index];
        [self.areaPicker reloadComponent:2];
        [self.areaPicker selectRow:0 inComponent:2 animated:NO];
    }
    
    NSInteger rowOne = [pickerView selectedRowInComponent:0];
    NSInteger rowTwo = [pickerView selectedRowInComponent:1];
    NSInteger rowThree = [pickerView selectedRowInComponent:2];
    NSString *selectedOne = [[_pickRowOne objectAtIndex:rowOne]objectForKey:@"nameString"];
    NSString *selectedTwo = [[_pickRowTwo objectAtIndex:rowTwo]objectForKey:@"nameString"];
    
    if (_pickRowThree.count == 0) {
        province = selectedOne;
        city = selectedTwo;
        district = selectedTwo;
        self.areaTextField.text = [[NSString alloc] initWithFormat:@"%@%@%@",
                                   selectedOne,selectedTwo,selectedTwo];
    }
    else {
        NSString *selectedThree = [_pickRowThree objectAtIndex:rowThree];
        province = selectedOne;
        city = selectedTwo;
        district = selectedThree;
        self.areaTextField.text = [[NSString alloc] initWithFormat:@"%@%@%@",
                                   selectedOne,selectedTwo,selectedThree];
    }
}
- (NSString *)checkOrderInfor{
    
        if (self.contactTextField.text.length == 0) {
            return @"收货人姓名不能为空";
        }
        else if (self.phoneTextField.text.length == 0) {
            return @"手机号码不能为空";
        }
        else if (self.phoneTextField.text.length != 11 || ![self isValidatePhone:self.phoneTextField.text]) {
            return @"手机号码格式不正确";
        }
//        else if (self.postalTextField.text.length == 0) {
//            return @"邮政编码不能为空";
//        }
//        else if (![self isValidatePostal:self.postalTextField.text]) {
//            return @"邮政编码格式不正确";
//        }
        else if (self.areaTextField.text.length == 0) {
            return @"所在地区不能为空";
        }
        else if ([self.streetTextView.text isEqualToString:@""]) {
            return @"详细地址不能为空";
        }
    
    
    return @"";
}

- (BOOL)isValidatePostal:(NSString *)postal {
    NSString *postalRegex = @"^[1-9][0-9]{5}$";
    NSPredicate *postalTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", postalRegex];
    return [postalTest evaluateWithObject:postal];
}

- (BOOL)isValidatePhone:(NSString *)phone {
    NSString *phoneRegex = @"^(13|14|15|18)\\d{9}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:phone];
}
- (void)myAlertView:(NSString *)message{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"确定"
                                          otherButtonTitles:nil];
    
    [alert show];
}


- (IBAction)switchAction:(id)sender {
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    if (isButtonOn) {
        isDefault = 1;
    }else {
        isDefault =0;
    }
}
//无网络连接和意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [[XY_Cloud_AddContactAddress share] setDelegate:nil];
    //小圈停止运动
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
    
}
@end
