//
//  XY_Advertising.h
//  DianShangApp
//
//  Created by wa on 14-5-16.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WX_Cloud_Delegate.h"
@interface XY_Cloud_Advertising : NSObject
{
   id <WX_Cloud_Delegate> delegate;
   NSMutableData * recivedData;
}
@property (strong,nonatomic) id <WX_Cloud_Delegate> delegate;
@property (strong,nonatomic) NSMutableData * recivedData;

+(id)share;
-(void)loadingImageAndTitle;

@end
