//
//  XYSelectedGoodsViewController.h
//  DianShangApp
//
//  Created by hxy on 14-5-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomPageControl.h"
#import "MKNumberBadgeView.h"
#import "XYLoadingView.h"
#import "AsynImageView.h"
#import "WX_Cloud_Delegate.h"
@interface XYSelectedGoodsViewController : UIViewController<UIScrollViewDelegate,WX_Cloud_Delegate>
{
    NSArray * adArray;
    NSDate * startDate;
    NSDate * endDate;
    BOOL timeStart;
    NSString * groupbuyID;
    XYLoadingView * loadingView;
    AsynImageView * groupImgView;
}
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak,nonatomic)IBOutlet UIScrollView *adScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *neProductScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *hotProductScrollView;
@property(weak,nonatomic)IBOutlet CustomPageControl *customPageControl;
@property (weak, nonatomic) IBOutlet UILabel *remainTimeLabel;
@property(strong,nonatomic)NSArray *adArray;
@property(retain,nonatomic)UIImageView *firstAdImageView;
@property(retain,nonatomic)UIImageView *lastAdImageView;
@property (weak, nonatomic) IBOutlet UIButton *leaveMsgBtn;
@property (weak, nonatomic) IBOutlet UIButton *preferInforBtn;
@property (weak, nonatomic) IBOutlet UIButton *groupBuyBtn;
@property (weak, nonatomic) IBOutlet UIView *firstView;
@property (weak, nonatomic) IBOutlet UIView *secondView;
@property (weak, nonatomic) IBOutlet UILabel *groupbuyTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *groupbuyPriceLab;
@property (weak, nonatomic) IBOutlet UILabel *buyerCountLab;
@property(strong,nonatomic)NSArray * neProductArr;
@property(strong,nonatomic)NSArray * hotProductArr;
@property (weak, nonatomic) IBOutlet MKNumberBadgeView *groupBuyMKNum;
@property (weak, nonatomic) IBOutlet MKNumberBadgeView *preferInfoMKNum;
@property (weak, nonatomic) IBOutlet MKNumberBadgeView *leaveMsgMKNum;
- (IBAction)leaveMsgBtnAct:(id)sender;
- (IBAction)preferInforBtnAct:(id)sender;
- (IBAction)groupBuyBtnAct:(id)sender;
- (IBAction)groomGroupBtnAct:(id)sender;
- (IBAction)neProductBtnAct:(id)sender;
- (IBAction)hotProductBtnAct:(id)sender;
@end
