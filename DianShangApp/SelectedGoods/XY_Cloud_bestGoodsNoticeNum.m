//
//  XY_bestGoodsNoticeNum.m
//  DianShangApp
//
//  Created by wa on 14-6-5.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XY_Cloud_bestGoodsNoticeNum.h"
#import "SBJson.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"

static XY_Cloud_bestGoodsNoticeNum * bestGoodsNoticeNum = nil;
@implementation XY_Cloud_bestGoodsNoticeNum
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!bestGoodsNoticeNum)
    {
        bestGoodsNoticeNum = [[XY_Cloud_bestGoodsNoticeNum alloc] init];
    }
    return bestGoodsNoticeNum;
}
-(void)getBestGoodsNoticeNum:(NSString *)groupbuyDate :(NSString *)privilegeDate :(NSString *)suggestDate :(NSString *)pushMsgDate :(NSString *)discussDate :(NSString *)consultDate
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        NSString * urlStr;
        WXCommonSingletonClass *  singletonClass  = [WXCommonSingletonClass share];
        urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/Ecom_bestGoodsNoticeNum?shopid=%@&uid=%@&groupbuyDate=%@&privilegeDate=%@&suggestDate=%@&pushMsgDate=%@&discussDate=%@&consultDate=%@",singletonClass.shopid,singletonClass.currentUserId,groupbuyDate,privilegeDate,suggestDate,pushMsgDate,discussDate,consultDate]];
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:TIMEOUT_INTERVAL];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        NSDictionary * dataDic = [dic objectForKey:@"data"];
        [delegate getBestGoodsNoticeNumSuccess:dataDic];
    }
    else
    {
        if ([[dic objectForKey:@"message"] length] <= 0)
        {
            [delegate getBestGoodsNoticeNumFailed:@"请求失败"];
        }
        else
        {
            NSString * message = [dic objectForKey:@"message"];
            [delegate getBestGoodsNoticeNumFailed:message];
        }
    }
}

@end
