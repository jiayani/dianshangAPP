//
//  XY_Advertising.m
//  DianShangApp
//
//  Created by wa on 14-5-16.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XY_Cloud_Advertising.h"
#import "SBJson.h"
#import "AppDelegate.h"
#import "WXCommonDateClass.h"
#import "WX_Cloud_IsConnection.h"
#import "ProductAds.h"

static XY_Cloud_Advertising * adver = nil;
@implementation XY_Cloud_Advertising
@synthesize delegate;
@synthesize recivedData;

+(id)share
{
    if (!adver)
    {
        adver = [[XY_Cloud_Advertising alloc] init];
    }
    return adver;
}

-(void)loadingImageAndTitle
{
    if ([[WX_Cloud_IsConnection share] isConnectionAvailable])
    {
        WXCommonSingletonClass * singletonClass  = [WXCommonSingletonClass share];
        NSString * urlStr = [singletonClass.serviceURL stringByAppendingString:[NSString stringWithFormat:@"Iphone/ShopIndexAd?shopid=%@",singletonClass.shopid]];
        NSURL * url = [NSURL URLWithString:urlStr];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:15];
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (connection)
        {
            recivedData = [[NSMutableData alloc] init];
        }
    }
    else
    {
        [delegate accidentError:NoHaveNetwork errorCode:@"1"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [recivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [recivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate accidentError:MyNetworkError errorCode:@"2"];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString * str = [[NSString alloc] initWithData:recivedData encoding:NSUTF8StringEncoding];
    NSDictionary * dic = [str JSONValue];
    /*收到成功代码后的操作*/
    if ([[dic objectForKey:@"code"] intValue] == 100)
    {
        AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
        NSManagedObjectContext *context=[appDelegate managedObjectContext];
        
        NSArray * array = [dic objectForKey:@"data"];
        
        NSError *error;
        NSEntityDescription *entityDescription=[NSEntityDescription entityForName:@"ProductAds" inManagedObjectContext:context];
        [context lock];
        for (int i=0; i<[array count]; i++) {
            ProductAds *theObject;
            NSDictionary * object = [array objectAtIndex:i];
            NSString * adid = [object objectForKey:@"advertisingID"];
            int ad_type = [[object objectForKey:@"type"]intValue];
            NSString * ad_url = [object objectForKey:@"linkInfo"];
            NSString * title = [object objectForKey:@"title"];
            NSDictionary * imageDic = [object objectForKey:@"image"];
            NSString * pic_url = [imageDic objectForKey:@"img"];
            //查询本地数据库中是否有该数据 有：进行修改  无：添加一条数据
            NSFetchRequest *request=[[NSFetchRequest alloc]init];
            NSPredicate *pred=[NSPredicate predicateWithFormat:@"(adid=%@)",adid];
            [request setPredicate:pred];
            [request setEntity:entityDescription];
            NSArray *objects=[context executeFetchRequest:request error:&error];
            if (objects!=nil && objects.count>0) {
                
                theObject=(ProductAds *)[objects objectAtIndex:0];
                
            }else{
                theObject=(ProductAds *)[[NSManagedObject alloc]initWithEntity:entityDescription insertIntoManagedObjectContext:context];
            }
            theObject.adid = [NSNumber numberWithInt:[adid intValue]];
            theObject.pic_url = [NSString stringWithFormat:@"%@", pic_url];
            theObject.ad_type = [NSNumber numberWithInt:ad_type];
            theObject.title = [NSString stringWithFormat:@"%@", title];
            theObject.ad_url = [NSString stringWithFormat:@"%@", ad_url];
            theObject.ad_orderBy = [NSNumber numberWithInt:i];
           
        }
        if ([context save:nil]) {
            [context unlock];
            [delegate syncSuccess:nil];
        }
        else {
            [context unlock];
        }
        
    }
        else
        {
            if ([[dic objectForKey:@"message"] length] <= 0)
            {
                [delegate syncFailed:@"请求失败"];
            }
            else
            {
            [delegate syncFailed:[dic objectForKey:@"message"]];
            }
        }
   
}






@end
