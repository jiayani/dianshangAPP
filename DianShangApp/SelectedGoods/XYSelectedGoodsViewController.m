//
//  XYSelectedGoodsViewController.m
//  DianShangApp
//
//  Created by hxy on 14-5-15.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XYSelectedGoodsViewController.h"
#import "AsynImageView.h"
#import "XY_Cloud_Advertising.h"
#import "AppDelegate.h"
#import "ProductAds.h"
#import "XY_Cloud_bestGoodsNoticeNum.h"
#import "XY_Cloud_bestGoodsIndexSimple.h"
#import "XYGoodsInforViewController.h"
#import "FYLeaveMessageViewController.h"
#import "WXCommonViewClass.h"
#import "WXGroupByingListViewController.h"
#import "XYGoodsDisplayViewController.h"
#import "WXGroupBuyingDetailViewController.h"
#import "WXFavorableActivityDetailViewController.h"
#import "WXCommonDateClass.h"
#import "WXUnionShopListViewController.h"
#import "WXCommonDataClass.h"
static NSTimer *adTimer = nil;
/*当前显示广告索引*/
static NSInteger pageControllerIndex = 0;
static NSInteger maxPageControllerIndex = 0;
static CustomPageControl *customPageControl = nil;


@interface XYSelectedGoodsViewController ()

@end

@implementation XYSelectedGoodsViewController
@synthesize adArray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"首页", @"首页");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    timeStart = YES;
    groupImgView = [[AsynImageView alloc] init];
    
    if (iPhone5) {
        self.mainScrollView.contentSize = CGSizeMake(320, self.mainScrollView.frame.size.height + 256.0f);
        
    }else{
        self.mainScrollView.contentSize = CGSizeMake(320, self.mainScrollView.frame.size.height + 345.0f);
    }
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
    [self initController];
    [[XY_Cloud_Advertising share] setDelegate:(id)self];
    [[XY_Cloud_Advertising share] loadingImageAndTitle];
    
    [[XY_Cloud_bestGoodsNoticeNum share] setDelegate:(id)self];
    [[XY_Cloud_bestGoodsNoticeNum share] getBestGoodsNoticeNum:[self getLocalStoData:@"groupBuy"] :[self getLocalStoData:@"preferInfor"] :[self getLocalStoData:@"opinionTime"] :[self getLocalStoData:@"pushTime"] :[self getLocalStoData:@"reviewTime"] :[self getLocalStoData:@"infomationTime"]];
    
    [[XY_Cloud_bestGoodsIndexSimple share] setDelegate:(id)self];
    [[XY_Cloud_bestGoodsIndexSimple share] bestGoodsIndexSimple];
    
    [self setScrollViewProperty:self.neProductScrollView];
    [self setScrollViewProperty:self.hotProductScrollView];
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    
    [[XY_Cloud_Advertising share] setDelegate:nil];
    [[XY_Cloud_bestGoodsNoticeNum share] setDelegate:nil];
    [[XY_Cloud_bestGoodsIndexSimple share] setDelegate:nil];
}
- (void)viewWillAppear:(BOOL)animated
{
    [[XY_Cloud_Advertising share] setDelegate:(id)self];
    [[XY_Cloud_Advertising share] loadingImageAndTitle];
    
    [[XY_Cloud_bestGoodsNoticeNum share] setDelegate:(id)self];
    [[XY_Cloud_bestGoodsNoticeNum share] getBestGoodsNoticeNum:[self getLocalStoData:@"groupBuy"] :[self getLocalStoData:@"preferInfor"] :[self getLocalStoData:@"opinionTime"] :[self getLocalStoData:@"pushTime"] :[self getLocalStoData:@"reviewTime"] :[self getLocalStoData:@"infomationTime"]];
    
    [[XY_Cloud_bestGoodsIndexSimple share] setDelegate:(id)self];
    [[XY_Cloud_bestGoodsIndexSimple share] bestGoodsIndexSimple];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    //wangxia 2014-10-9 积分联盟alert提示信息
    [self integralUnionAlert];
}

- (void)initController
{
    UIButton *rightBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:@"呼叫客服"];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [rightBtn addTarget:self action:@selector(rightButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}

- (void)rightButtonPressed
{
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    [WXCommonDataClass callPhone:singletonClass.phoneNumber];
}

-(void)getBestGoodsIndexSimpleSuccess:(NSDictionary *)data
{
    [loadingView removeFromSuperview];
    NSArray * groupBuyDic = [data objectForKey:@"groupbuy"];
    
    if (groupBuyDic.count == 0) {
        [self.firstView setHidden:YES];
        [self.secondView setFrame:CGRectMake(0, 228, 320, 348)];
        if (iPhone5) {
            self.mainScrollView.contentSize = CGSizeMake(320, self.mainScrollView.frame.size.height + 256.0f-133.0f);
            
        }else{
            self.mainScrollView.contentSize = CGSizeMake(320, self.mainScrollView.frame.size.height + 345.0f-133.0f);
        }
    }
    else
    {
        groupbuyID = [[groupBuyDic objectAtIndex:0]  objectForKey:@"groupbuyID"];
        
        self.groupbuyPriceLab.text = [NSString stringWithFormat:@"￥%.2f",[[[groupBuyDic objectAtIndex:0] objectForKey:@"groupbuyPrice"] floatValue]];
        self.groupbuyTitleLab.text = [[groupBuyDic objectAtIndex:0]  objectForKey:@"groupbuyTitle"];
        self.buyerCountLab.text = [NSString stringWithFormat:@"%@人",[[groupBuyDic objectAtIndex:0]  objectForKey:@"buyerCount"]];
        
        
        groupImgView.frame = CGRectMake(10, 51, 70, 67);
        groupImgView.tag = 1;
        
        groupImgView.type = 1;
        groupImgView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
        groupImgView.imageURL = [NSString stringWithFormat:@"%@",[[groupBuyDic objectAtIndex:0] objectForKey:@"img"]];
        [self.firstView addSubview:groupImgView];
        
        startDate = [NSDate dateWithTimeIntervalSince1970:(long)[[[groupBuyDic objectAtIndex:0]  objectForKey:@"startDate"]longLongValue]];
        NSTimeZone *zone = [NSTimeZone  timeZoneWithName:@"Asia/Shanghai"];
        NSInteger interval = [zone secondsFromGMTForDate:startDate];
        startDate = [startDate  dateByAddingTimeInterval: interval];
        
        endDate = [NSDate dateWithTimeIntervalSince1970:(long)[[[groupBuyDic objectAtIndex:0]  objectForKey:@"endDate"]longLongValue]];
        NSTimeZone *endZone = [NSTimeZone  timeZoneWithName:@"Asia/Shanghai"];
        NSInteger endInterval = [endZone secondsFromGMTForDate:endDate];
        endDate = [endDate  dateByAddingTimeInterval: endInterval];

        if ((long)[[[groupBuyDic objectAtIndex:0]  objectForKey:@"endDate"]longLongValue] - [self date] > 0 ) {
            [self.firstView setHidden:NO];
            [self.secondView setFrame:CGRectMake(0, 364, 320, 348)];
            if (iPhone5) {
                self.mainScrollView.contentSize = CGSizeMake(320, self.mainScrollView.frame.size.height + 256.0f);
                
            }else{
                self.mainScrollView.contentSize = CGSizeMake(320, self.mainScrollView.frame.size.height + 345.0f);
            }
            [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerFireMethod:) userInfo:nil repeats:YES];
        }
        else {
            [self.firstView setHidden:YES];
            [self.secondView setFrame:CGRectMake(0, 228, 320, 348)];
            if (iPhone5) {
                self.mainScrollView.contentSize = CGSizeMake(320, self.mainScrollView.frame.size.height + 256.0f-133.0f);
                
            }else{
                self.mainScrollView.contentSize = CGSizeMake(320, self.mainScrollView.frame.size.height + 345.0f-133.0f);
            }
        }
        
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(groupBuySingleTap:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [self.firstView addGestureRecognizer:tapGestureRecognizer];
        self.firstView.userInteractionEnabled = YES;
        
    }
    //********以上是团购的信息********
    self.neProductArr = [[NSMutableArray alloc]init];
    self.hotProductArr = [[NSMutableArray alloc]init];
    for (UILabel * titleLab in self.neProductScrollView.subviews) {
        [titleLab removeFromSuperview];
    }
    for (AsynImageView * imageView in self.neProductScrollView.subviews) {
        [imageView removeFromSuperview];
    }
    for (UIControl *clickView in self.neProductScrollView.subviews) {
        [clickView removeFromSuperview];
    }
    for (UILabel * titleLab in self.hotProductScrollView.subviews) {
        [titleLab removeFromSuperview];
    }
    for (AsynImageView * imageView in self.hotProductScrollView.subviews) {
        [imageView removeFromSuperview];
    }
    for (UIControl *clickView in self.hotProductScrollView.subviews) {
        [clickView removeFromSuperview];
    }
    self.neProductArr = [data objectForKey:@"newproduct"];
    self.hotProductArr = [data objectForKey:@"hotproduct"];
    [self product:self.neProductArr :self.neProductScrollView :@"newprodcut"];
    [self product:self.hotProductArr :self.hotProductScrollView :@"hotproduct"];
    
}

-(void)groupBuySingleTap:(UITapGestureRecognizer*)recognizer{
    WXGroupBuyingDetailViewController * groupBuyingDetailVC = [[WXGroupBuyingDetailViewController alloc]initWithNibName:@"WXGroupBuyingDetailViewController" bundle:nil];
    groupBuyingDetailVC.keyStr = groupbuyID;
    [self.navigationController pushViewController:groupBuyingDetailVC animated:YES];
}

-(void)product:(NSArray *)productArr :(UIScrollView *)scrollView :(NSString *)clickFlag{
    for (int k = 0; k < productArr.count; k++)
	{
        NSDictionary * productAdDic = [productArr objectAtIndex:k];
        AsynImageView * imageView = [[AsynImageView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
        imageView.type = 1;
        imageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
        imageView.imageURL = [NSString stringWithFormat:@"%@",[productAdDic objectForKey:@"img"]];
        imageView.userInteractionEnabled = NO;
        imageView.exclusiveTouch         = YES;
		imageView.tag = k;
        
        UILabel * titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 80, 80, 20)];
        titleLab.font = [UIFont systemFontOfSize:12];
        titleLab.textColor = [UIColor blackColor];
        titleLab.backgroundColor = [UIColor clearColor];
        titleLab.text = [productAdDic objectForKey:@"title"];
        titleLab.textAlignment = NSTextAlignmentCenter;
        
        UIControl *clickView;
        if ([clickFlag isEqualToString:@"newprodcut"]) {
            clickView = [[UIControl alloc] init];
            [clickView addTarget:self
                          action:@selector(newSendto:)
                forControlEvents:UIControlEventTouchUpInside];
            [clickView setExclusiveTouch:YES];
        }
        
        else if ([clickFlag isEqualToString:@"hotproduct"]) {
            clickView = [[UIControl alloc] init];
            [clickView addTarget:self
                             action:@selector(hotSendto:)
                   forControlEvents:UIControlEventTouchUpInside];
            [clickView setExclusiveTouch:YES];
        }
        [clickView setTag:k];
        [clickView addSubview:titleLab];
        [clickView addSubview:imageView];
        
        CGRect rect = clickView.frame;
        rect.size.height = 100;
        rect.size.width = 80;
        rect.origin.x    = k * 80 +10;
        rect.origin.y    = 10;
        clickView.frame = rect;
        
		[scrollView addSubview:clickView];
	}
    
    UIControl *view = nil;
	NSArray *subviews = [scrollView subviews];
    
	CGFloat curXLoc = 100;
	for (view in subviews)
	{
		if ([view isKindOfClass:[UIControl class]] && view.tag > 0)
		{
			CGRect frame = view.frame;
			frame.origin = CGPointMake(curXLoc, 10);
			view.frame = frame;
			
			//curXLoc += (kScrollObjWidth);
            curXLoc += 90;
		}
	}
	
	[scrollView setContentSize:CGSizeMake(((productArr.count) * 90 + 10), [scrollView bounds].size.height)];
    
}
- (void)hotSendto:(id)sender
{
    UIControl *clickView=(UIControl *)sender;
    
    XYGoodsInforViewController * xyGoodsInforVC= [[XYGoodsInforViewController alloc] initWithNibName:@"XYGoodsInforViewController" bundle:nil];
    xyGoodsInforVC.productId = [[self.hotProductArr objectAtIndex:clickView.tag] objectForKey:@"hotpro_id"];
    [self.navigationController pushViewController:xyGoodsInforVC animated:YES];
    
}

- (void)newSendto:(id)sender
{
    UIControl *clickView=(UIControl *)sender;
    
    
    XYGoodsInforViewController * xyGoodsInforVC= [[XYGoodsInforViewController alloc] initWithNibName:@"XYGoodsInforViewController" bundle:nil];
    xyGoodsInforVC.productId = [[self.neProductArr objectAtIndex:clickView.tag] objectForKey:@"hotpro_id"];
    [self.navigationController pushViewController:xyGoodsInforVC animated:YES];
}


-(void)getBestGoodsIndexSimpleFailed:(NSString*)errMsg
{
    [[XY_Cloud_bestGoodsIndexSimple share]setDelegate:nil];
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errMsg];
}

-(void)getBestGoodsNoticeNumSuccess:(NSDictionary *)data
{
    [self.groupBuyMKNum setHidden:YES];
    [self.leaveMsgMKNum setHidden:YES];
    [self.preferInfoMKNum setHidden:YES];
    [self setValueOfBadgetView:[[data objectForKey:@"groupbyProNum"]integerValue] CartNum:self.groupBuyMKNum];
    [self setValueOfBadgetView:[[data objectForKey:@"leaveMsgNum"]integerValue] CartNum:self.leaveMsgMKNum];
    [self setValueOfBadgetView:[[data objectForKey:@"privilegeNum"]integerValue] CartNum:self.preferInfoMKNum];
}
-(void)setValueOfBadgetView:(NSInteger)newNum CartNum:(MKNumberBadgeView *)cartNum{
    
    cartNum.hideWhenZero = YES;
    cartNum.value = newNum;
    cartNum.shadow = NO;
    
}
-(void)getBestGoodsNoticeNumFailed:(NSString*)errMsg
{
    [[XY_Cloud_bestGoodsNoticeNum share]setDelegate:nil];
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errMsg];
}
- (void)timerFireMethod:(NSTimer*)theTimer
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *endDat = [[NSDateComponents alloc] init];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:endDate];
    
    static int year;
    static int month;
    static int day;
    static int hour;
    static int minute;
    static int second;
    
    if(timeStart) {
        year = [[dateString substringWithRange:NSMakeRange(0, 4)] intValue];
        month = [[dateString substringWithRange:NSMakeRange(5, 2)] intValue];
        day = [[dateString substringWithRange:NSMakeRange(8, 2)] intValue];
        hour = [[dateString substringWithRange:NSMakeRange(11, 2)] intValue];
        minute = [[dateString substringWithRange:NSMakeRange(14, 2)] intValue];
        second = [[dateString substringWithRange:NSMakeRange(17, 2)] intValue];
        timeStart= NO;
    }
    
    [endDat setYear:year];
    [endDat setMonth:month];
    [endDat setDay:day];
    [endDat setHour:hour];
    [endDat setMinute:minute];
    [endDat setSecond:second];
    
    NSDate *todate =  [cal dateFromComponents:endDat];
    NSDate *star = [NSDate date];
    NSTimeZone *zone = [NSTimeZone  timeZoneWithName:@"Asia/Shanghai"];
    //    NSTimeZone *zone = [NSTimeZone  systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate:star];
    NSDate *localeDate = [star  dateByAddingTimeInterval: interval];
    
    unsigned int unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    
    NSDateComponents *d = [cal components:unitFlags fromDate:localeDate toDate:todate options:0];
    
    NSString *xiaoshi = [NSString stringWithFormat:@"%d", [d hour]];
    if([d hour] < 10 && [d hour] >=0) {
        xiaoshi = [NSString stringWithFormat:@"0%d",[d hour]];
    }
    
    NSString *fen = [NSString stringWithFormat:@"%d", [d minute]];
    if([d minute] < 10 && [d minute] >=0) {
        fen = [NSString stringWithFormat:@"0%d",[d minute]];
    }
    NSString *miao = [NSString stringWithFormat:@"%d", [d second]];
    if([d second] < 10 && [d second] >=0) {
        miao = [NSString stringWithFormat:@"0%d",[d second]];
    }
    self.remainTimeLabel.text = [NSString stringWithFormat:@"%d天 %@:%@:%@",[d day], xiaoshi, fen, miao];
}
- (void) setScrollViewProperty:(UIScrollView *)scrollView
{
    scrollView.delegate                       = self;
    scrollView.pagingEnabled                  = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator   = NO;
    scrollView.indicatorStyle                 = UIScrollViewIndicatorStyleWhite;
    scrollView.decelerationRate               = UIScrollViewDecelerationRateNormal;
    scrollView.bounces                        = NO;
    scrollView.exclusiveTouch                 = YES;
}

- (NSArray *)getAllAdvertisements{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    NSEntityDescription *entityDescription=[NSEntityDescription entityForName:@"ProductAds" inManagedObjectContext:context];
    NSFetchRequest *request=[[NSFetchRequest alloc]init];
    [request setEntity:entityDescription];
    
    NSError *error;
    NSArray *allobjects=[context executeFetchRequest:request error:&error];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:
                                        @"ad_orderBy"
                                                                   ascending:YES];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
	NSArray *objects = [allobjects sortedArrayUsingDescriptors:sortDescriptors];
    
    
    return objects;
}

-(void)syncSuccess:(NSDictionary*)adDic{
    
    
    
    //////////////*********************************////////////////////
    
    adArray = [self getAllAdvertisements];
    if(adTimer == nil){
        if(adArray.count != 0){
            adTimer = [NSTimer scheduledTimerWithTimeInterval:5 target: self selector: @selector(handleTimer:)  userInfo:nil  repeats: YES];
            
        }
    }
    maxPageControllerIndex = adArray.count + 1;
    self.adScrollView.contentSize = CGSizeMake(320*([adArray count]+2), 160);
    customPageControl.numberOfPages = [adArray count];
    customPageControl = self.customPageControl;
    for (int i=0; i< [adArray count]; i++)
    {
        ProductAds * productAds = (ProductAds *)[adArray objectAtIndex:i];
        AsynImageView * imageView = [[AsynImageView alloc] init];
        imageView.frame = CGRectMake((i+1)*320, 0, 320, 160);
        imageView.tag = i;
        
        imageView.type = 1;
        imageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
        imageView.imageURL = [NSString stringWithFormat:@"%@",productAds.pic_url];
        
        //绑定单击事件
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(adSingleTap:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [imageView addGestureRecognizer:tapGestureRecognizer];
        imageView.userInteractionEnabled = YES;
        [self.adScrollView addSubview:imageView];
        
    }
    //在第一个位置添加第一张广告图片
    [self loadAdHolderWithAdIndex:0];
    
    //在最后一个位置添加第一张广告图片
    [self loadAdHolderWithAdIndex:adArray.count-1];
    
    self.adScrollView.decelerationRate = 0;
    self.adScrollView.frame = CGRectMake(0, 0, 320, 160);
    self.adScrollView.showsHorizontalScrollIndicator = NO;
    self.adScrollView.showsVerticalScrollIndicator = NO;
    [self.adScrollView scrollRectToVisible:CGRectMake(320,0, 320, 160) animated:NO];
    self.customPageControl.currentPage = 0;
    
}
-(void)syncFailed:(NSString*)errCode{
    [[XY_Cloud_Advertising share]setDelegate:nil];
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errCode];
}


/*加载广告占位 第一张和最后一张*/
- (void)loadAdHolderWithAdIndex:(NSInteger)index{
    
    ProductAds * productAds;
    
    AsynImageView *imageView = [[AsynImageView alloc]init];
    //第一张广告过渡
    if(index == 0){
        productAds = (ProductAds*)[adArray objectAtIndex:adArray.count - 1];
        
        //第一张广告过渡添加在0的位置
        imageView.frame = CGRectMake(0, 0, 320, 160);
        self.firstAdImageView = imageView;
        //最后一张广告过渡
    }else{
        productAds = (ProductAds*)[adArray objectAtIndex:0];
        //最后一张广告过渡添加在最后一张广告过渡的后面
        imageView.frame = CGRectMake((index+2)*320, 0, 320, 160);
        self.lastAdImageView = imageView;
    }
    imageView.type = 1;
    imageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    
    imageView.imageURL = [NSString stringWithFormat:@"%@",productAds.pic_url];
    
    [self.adScrollView addSubview:imageView];
}

/*广告位自动轮播*/
- (void) handleTimer: (NSTimer *) timer
{
    pageControllerIndex = customPageControl.currentPage;
    pageControllerIndex++;
    //速度0.7 修改坐标
    [UIView animateWithDuration:0.7
                     animations:^{
                         self.adScrollView.contentOffset = CGPointMake((pageControllerIndex+1)*320,0);
                         customPageControl.currentPage = pageControllerIndex;
                         
                     }];
    
    if(pageControllerIndex == maxPageControllerIndex -1){
        [NSTimer scheduledTimerWithTimeInterval:0.8 target:self selector:@selector(scrollToBegin) userInfo:self repeats:NO];
        
    }
    
}
//从边界外的广告图滚回初始广告位
- (void)scrollToBegin{
    self.adScrollView.contentOffset = CGPointMake(320,0);
    customPageControl.currentPage = 0;
    pageControllerIndex = 0;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.adScrollView) {
        
        int cur = scrollView.contentOffset.x / scrollView.frame.size.width ;
        self.customPageControl.currentPage = cur-1;
        //当前存在广告信息
        if(adArray !=nil && adArray.count != 0){
            if (cur==0){
                [self.adScrollView scrollRectToVisible:CGRectMake(320 * [adArray count],0,320,460) animated:NO];
                self.customPageControl.currentPage = [adArray count]-1;
            }else if (cur==([adArray count]+1)){
                [self.adScrollView scrollRectToVisible:CGRectMake(320,0,320,460) animated:NO];
                self.customPageControl.currentPage = 0;

            }
            //当前不存在广告信息 默认添加4张占位图片
        }else{
            if (cur==0){
                [self.adScrollView scrollRectToVisible:CGRectMake(320 * 4,0,320,460) animated:NO];
                self.customPageControl.currentPage = 3;

            }else if (cur == 5){
                [self.adScrollView scrollRectToVisible:CGRectMake(320,0,320,460) animated:NO];
                self.customPageControl.currentPage = 0;

            }
        }
    }
}


- (void)adSingleTap:(UITapGestureRecognizer*)recognizer{
    NSInteger adTag = recognizer.view.tag;
    ProductAds *productAds = [adArray objectAtIndex:adTag];
    NSString *linkInfo = [NSString stringWithFormat:@"%@",productAds.ad_url];
    //跳至优惠活动
    if([[productAds.ad_type stringValue] isEqualToString:@"3"]){
        WXFavorableActivityDetailViewController * favorableActivityDetailVC = [[WXFavorableActivityDetailViewController alloc]initWithNibName:@"WXFavorableActivityDetailViewController" bundle:nil];
        favorableActivityDetailVC.keyStr = linkInfo;
        [self.navigationController pushViewController:favorableActivityDetailVC animated:YES];
        
    }
    //跳至商品详情
    if([[productAds.ad_type stringValue] isEqualToString:@"2"]){
        
        XYGoodsInforViewController * xyGoodsInforVC= [[XYGoodsInforViewController alloc] initWithNibName:@"XYGoodsInforViewController" bundle:nil];
        xyGoodsInforVC.productId = linkInfo;
        [self.navigationController pushViewController:xyGoodsInforVC animated:YES];
    }
    //打开网页
    if([[productAds.ad_type stringValue] isEqualToString:@"1"]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:linkInfo]];
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//留言消息
- (IBAction)leaveMsgBtnAct:(id)sender {
    [self.leaveMsgMKNum setHidden:YES];
    FYLeaveMessageViewController * leaveMessageVC= [[FYLeaveMessageViewController alloc] initWithNibName:@"FYLeaveMessageViewController" bundle:nil];
    [self.navigationController pushViewController:leaveMessageVC animated:YES];
}
//优惠信息
- (IBAction)preferInforBtnAct:(id)sender {
    [self.preferInfoMKNum setHidden:YES];
    WXGroupByingListViewController * groupByingListVC= [[WXGroupByingListViewController alloc] initWithNibName:@"WXGroupByingListViewController" bundle:nil];
    groupByingListVC.flagInt = 1;
    [self.navigationController pushViewController:groupByingListVC animated:YES];
}
//团购商品
- (IBAction)groupBuyBtnAct:(id)sender {
    NSString * dateNowStr = [NSString stringWithFormat:@"%ld",[self dateNow]];
    [self localStoData:dateNowStr key:@"groupBuy"];
    [self.groupBuyMKNum setHidden:YES];
    WXGroupByingListViewController * groupByingListVC= [[WXGroupByingListViewController alloc] initWithNibName:@"WXGroupByingListViewController" bundle:nil];
    groupByingListVC.flagInt = 2;
    [self.navigationController pushViewController:groupByingListVC animated:YES];
}
//活动推荐
- (IBAction)groomGroupBtnAct:(id)sender {
    WXGroupByingListViewController * groupByingListVC= [[WXGroupByingListViewController alloc] initWithNibName:@"WXGroupByingListViewController" bundle:nil];
    groupByingListVC.flagInt = 2;
    [self.navigationController pushViewController:groupByingListVC animated:YES];
}
//最新商品
- (IBAction)neProductBtnAct:(id)sender {
    XYGoodsDisplayViewController * xyGoodsDisplayVC= [[XYGoodsDisplayViewController alloc] initWithNibName:@"XYGoodsDisplayViewController" bundle:nil];
    xyGoodsDisplayVC.type = 2;
    xyGoodsDisplayVC.sortType = 9;
    xyGoodsDisplayVC.page = 1;
    [self.navigationController pushViewController:xyGoodsDisplayVC animated:YES];
}
//热销商品
- (IBAction)hotProductBtnAct:(id)sender {
    XYGoodsDisplayViewController * xyGoodsDisplayVC= [[XYGoodsDisplayViewController alloc] initWithNibName:@"XYGoodsDisplayViewController" bundle:nil];
    xyGoodsDisplayVC.type = 1;
    xyGoodsDisplayVC.sortType = 2;
    xyGoodsDisplayVC.page = 1;
    [self.navigationController pushViewController:xyGoodsDisplayVC animated:YES];
}
- (NSString *)getLocalStoData:(NSString *)key{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString * longStr = [defaults objectForKey:key];
    [defaults synchronize];
    if (longStr == NULL || longStr == nil) {
        longStr = @"0";
    }
    return longStr;
}
- (void)localStoData:(NSString *)object key:(NSString *)key{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:object forKey:key];
    [defaults synchronize];
}
- (long)date {
    NSDate *datenow = [NSDate date];
    NSTimeZone *zone = [NSTimeZone  timeZoneWithName:@"Asia/Shanghai"];
    //    NSTimeZone *zone = [NSTimeZone  systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate:datenow];
    NSDate *localeDate = [datenow  dateByAddingTimeInterval: interval];
    long  timSp = (long)[localeDate timeIntervalSince1970];
    return timSp;
}
- (long)dateNow {
    NSDate *datenow = [NSDate date];
    long  timSp = (long)[datenow timeIntervalSince1970];
    return timSp;
}


//无网络连接和意外错误
-(void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode{
    [loadingView removeFromSuperview];
    [[XY_Cloud_bestGoodsNoticeNum share] setDelegate:nil];
    [[XY_Cloud_bestGoodsIndexSimple share] setDelegate:nil];
    [[XY_Cloud_Advertising share] setDelegate:nil];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
    
}
#pragma mark - wangxia 积分联盟
//wangxia 新增加积分联盟，在第一次启动的时候提示
- (void)integralUnionAlert{
    BOOL isOpen;
    WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    if (singletonClass.isChange == NO) {
        isOpen = NO;
    }else{
        isOpen = YES;
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //是第一次安装App时需要把所有的配置信息保存到NSUserDefaults中
    if ([[defaults objectForKey:@"IsNotFirstUnion"]boolValue] == NO && isOpen == YES){
        [defaults setObject:[NSNumber numberWithBool:YES] forKey:@"IsNotFirstUnion"];
        NSString  *alertStr = NSLocalizedString(@"新功能“积分联盟”上线啦，以后不同商家的积分可通过哇点通用积分互相转化，合并为其中一个商家的积分，或者合并为哇点通用积分，聚少成多，兑换高价值的礼品！还不赶快爽一下!", @"积分联盟首页提示信息");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"重要提醒" message:alertStr delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:@"去看看",nil];
        if (iOS8) {
            //系统ios8的处理
            NSString *title = NSLocalizedString(@"重要提醒", nil);
            NSString *message = alertStr;
            NSString *cancelButtonTitle = NSLocalizedString(@"知道了", nil);
            NSString *otherButtonTitle = NSLocalizedString(@"去看看", nil);
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
            // Create the actions.
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                
            }];
            
            UIAlertAction *otherAction = [UIAlertAction actionWithTitle:otherButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self goToUnionShop];
            }];
            
            // Add the actions.
            [alertController addAction:cancelAction];
            [alertController addAction:otherAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
            return;
            
        }else if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
        {   //如果ios7.0的处理办法
            CGSize size = [alertStr sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(240, 400) lineBreakMode:NSLineBreakByTruncatingTail];
            UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, -20, 240, size.height)];
            textLabel.font = [UIFont systemFontOfSize:15];
            textLabel.textColor = [UIColor blackColor];
            textLabel.backgroundColor = [UIColor clearColor];
            textLabel.lineBreakMode = NSLineBreakByWordWrapping;
            textLabel.numberOfLines = 0;
            textLabel.textAlignment = NSTextAlignmentLeft;
            textLabel.text = alertStr;
            [alert setValue:textLabel forKey:@"accessoryView"];
            //这个地方别忘了把alertview的message设为空
            alert.message = @"";
        }
        alert.delegate = self;
        alert.tag = 200;
        [alert show];
    }
    
    
}
//跳转到联盟商家
- (void)goToUnionShop{
    WXUnionShopListViewController *unionShopListViewController = [[WXUnionShopListViewController alloc]initWithNibName:@"WXUnionShopListViewController" bundle:nil];
    unionShopListViewController.isFromeStarPage = YES;
    [self.navigationController pushViewController:unionShopListViewController animated:YES];
}
#pragma mark -UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 200){
        if (buttonIndex == 1) {
            [self goToUnionShop];
        }
    }
}
- (void)willPresentAlertView:(UIAlertView *)alertView{
    //设置alertView中文字居左对其
    if (alertView.tag == 200) {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0) {
            for( UIView * view in alertView.subviews )
                
            {
                if( [view isKindOfClass:[UILabel class]] )
                    
                {
                    UILabel* label = (UILabel*) view;
                    
                    [label setTextAlignment:NSTextAlignmentLeft];
                    
                }
                
            }
        }
        
    }
    
}

@end
