//
//  XYPayWebViewController.h
//  DianShangApp
//
//  Created by wa on 14-6-16.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XYPayWebViewController : UIViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *payWebView;
@property(retain,nonatomic)NSString *payUrl;
@property (nonatomic, strong) NSString *orderID;
@property (nonatomic, strong) NSString *money;
@property (nonatomic, strong) NSString *payStyleText;
@property (nonatomic, assign)  BOOL isTuan;
@property (nonatomic, assign) BOOL selfTakeFlag;
@end
