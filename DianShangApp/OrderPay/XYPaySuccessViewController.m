//
//  XYPaySuccessViewController.m
//  DianShangApp
//
//  Created by wa on 14-6-16.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XYPaySuccessViewController.h"
#import "WXCommonViewClass.h"
#import "FYOrderDetailViewController.h"

@interface XYPaySuccessViewController ()

@end

@implementation XYPaySuccessViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [WXCommonViewClass hideTabBar:self isHiden:YES];
        self.title = NSLocalizedString(@"订单已提交", @"订单已提交/订单已提交");
        /*设置导航返回按钮*/
        UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
        [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
        self.navigationItem.leftBarButtonItem = leftBarButtonItem;
        
        UIButton * cancleBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:NSLocalizedString(@"查看订单", @"查看订单/订单已提交")];
        [cancleBtn addTarget:self action:@selector(submitOrderBtnPressed) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:cancleBtn];
        self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.moneyLabel.text = self.money;
    self.orderLabel.text = self.orderID;
    self.payStyleLabel.text = self.payStyleText;
    
    if (self.isTuan) {
        self.bigTitleLabel.text = @"恭喜你团购成功！";
    }
    else
    {
        self.bigTitleLabel.text = @"您已成功购买！";
    }
    if (self.selfTakeFlag) {
        if (self.isTuan) {
            self.titleMegLabel.text = @"您已成功团购，稍后您会收到订单验证码短信，请妥善保管，您可凭订单短信进行消费。\n注：订单验证码可于订单详情的备注查询。";
        }
        else
        {
            self.titleMegLabel.text = @"您已成功购买，稍后您会收到订单验证码短信，请妥善保管，您可凭订单短信进行消费。\n注：订单验证码可于订单详情的备注查询。";
        }
    }
    else
    {
        self.titleMegLabel.text = @"您的订单已提交，我们会尽快为您安排发货，请耐心等待。";
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//返回按钮事件
- (void)backBtnPressed{
    if ([self.pushFlag isEqualToString:@"pushFlag"]) {
        self.pushFlag = @"";
         [WXCommonViewClass goToTabItem:self.navigationController tabItem:4];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
   
}

-(void)submitOrderBtnPressed{
    FYOrderDetailViewController * orderDetailVC = [[FYOrderDetailViewController alloc]initWithNibName:@"FYOrderDetailViewController" bundle:nil];
    orderDetailVC.orderID = self.orderID;
    orderDetailVC.pushView = self;
    [self.navigationController pushViewController:orderDetailVC animated:YES];
}

- (IBAction)goOnBuyBtnAct:(id)sender {
    [WXCommonViewClass goToTabItem:self.navigationController tabItem:1];
}
@end
