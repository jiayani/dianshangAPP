//
//  XYOrderPayViewController.m
//  DianShangApp
//
//  Created by wa on 14-6-12.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XYOrderPayViewController.h"
#import "WXCommonViewClass.h"
#import "XYPaySuccessViewController.h"
#import "AppDelegate.h"
#import "AlixPay.h"
#import "XY_Cloud_OrderPaymentInfo.h"
#import "XYPayWebViewController.h"
#import "FYOrderDetailViewController.h"
#import "WXCommonSingletonClass.h"
@interface XYOrderPayViewController ()

@end

@implementation XYOrderPayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
         [WXCommonViewClass hideTabBar:self isHiden:YES];
        self.title = NSLocalizedString(@"订单已提交", @"订单已提交/订单已提交");
        /*设置导航返回按钮*/
        UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
        [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
        self.navigationItem.leftBarButtonItem = leftBarButtonItem;
        
        UIButton * cancleBtn = [WXCommonViewClass getRightBtnOfNav:nil hightlightedImageName:nil  title:NSLocalizedString(@"查看订单", @"查看订单/订单已提交")];
        [cancleBtn addTarget:self action:@selector(submitOrderBtnPressed) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:cancleBtn];
        self.navigationItem.rightBarButtonItem = rightBarButtonItem;
       
    }
    return self;
}

-(void)submitOrderBtnPressed{
    FYOrderDetailViewController * orderDetailVC = [[FYOrderDetailViewController alloc]initWithNibName:@"FYOrderDetailViewController" bundle:nil];
    orderDetailVC.orderID = self.orderID;
    orderDetailVC.pushView = self;
    [self.navigationController pushViewController:orderDetailVC animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
     [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
    
    self.moneyLabel.text = self.money;
    self.orderLabel.text = self.orderID;
    self.payStyleLabel.text = self.payStyleText;
    [self.payOffBtn setHidden:NO];
    if (self.isTuan) {
        if ([self.payStyle isEqualToString:@"3"] || [self.payStyle isEqualToString:@"4"]) {
            self.bigTitleLabel.text = @"恭喜你，团购成功！";
        }
    }
    else
    {
        self.bigTitleLabel.text = @"您的订单已被提交\n我们会尽快进行处理";
    }

    if ([self.payStyle isEqualToString:@"1"] || [self.payStyle isEqualToString:@"2"] ) {
        self.titleMegLabel.text = @"请您在24小时内完成支付，否则订单会自动取消。您也可以在订单详情中继续支付该订单。";
    }
    else if ([self.payStyle isEqualToString:@"4"])
    {
        if (self.isTuan) {
            self.titleMegLabel.text = @"您已成功团购，稍后您会收到订单验证码短信，请妥善保管，您可凭订单短信进行消费。\n注：订单验证码可于订单详情的备注查询。";
        }
        else
        {
            self.titleMegLabel.text = @"您已成功购买，稍后您会收到订单验证码短信，请妥善保管，您可凭订单短信进行消费。\n注：订单验证码可于订单详情的备注查询。";
        }
        [self.payOffBtn setTitle:@"继续购买" forState:UIControlStateNormal];
        AnalysisFlag = @"goOnBuy";
    }
    else if ([self.payStyle isEqualToString:@"3"])
    {
        AnalysisFlag = @"goOnBuy";
        self.titleMegLabel.text = @"您的订单已提交，我们会尽快为您安排发货，请耐心等待。";
        [self.payOffBtn setTitle:@"继续购买" forState:UIControlStateNormal];
    }
    
   
    
    
    if ([[[singletonClass.sendDic objectForKey:@"consumeTypeId"] substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"3"]) {
        [self.selfAddressLabel setHidden:NO];
        [self.addressTitle setHidden:NO];
        self.selfAddressLabel.text = [singletonClass.sendDic objectForKey:@"ownDeliveryAddr"];
        CGSize size = [self.selfAddressLabel.text sizeWithFont:self.selfAddressLabel.font
                                             constrainedToSize:CGSizeMake(self.selfAddressLabel.frame.size.width, 10000)];
        CGRect rect = self.changeImageView.frame;
        rect.size.height += size.height+10;
        [self.changeImageView setFrame:rect];
        
        CGRect addressRect = self.selfAddressLabel.frame;
        addressRect.size.height += size.height;
        [self.selfAddressLabel setFrame:addressRect];
        
        CGRect flowerRect = self.flowerImageView.frame;
        flowerRect.origin.y += size.height + 5;
        [self.flowerImageView setFrame:flowerRect];
        
        CGRect payOffRect = self.payOffBtn.frame;
        payOffRect.origin.y += size.height + 5;
        [self.payOffBtn setFrame:payOffRect];
    }
    else
    {
        [self.selfAddressLabel setHidden:YES];
        [self.addressTitle setHidden:YES];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[XY_Cloud_OrderPaymentInfo share] setDelegate:nil];
}

//返回按钮事件
- (void)backBtnPressed{
    [WXCommonViewClass goToTabItem:self.navigationController tabItem:4];
}

- (void)gotoPaySuccess{
    XYPaySuccessViewController *paySuccessVC = [[XYPaySuccessViewController alloc]init];
    paySuccessVC.isTuan = self.isTuan;
    paySuccessVC.orderID = self.orderID;
    paySuccessVC.money = self.money;
    paySuccessVC.payStyleText = self.payStyleText;
    paySuccessVC.pushFlag = @"pushFlag";
    paySuccessVC.selfTakeFlag = self.selfTakeFlag;
    [self.navigationController pushViewController:paySuccessVC animated:YES];
    
}

- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [WXCommonViewClass showHudInView:self.view title:errorCode];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)payOffBtnAct:(id)sender {
    if ([AnalysisFlag isEqualToString:@"goOnBuy"]) {
        AnalysisFlag = @"";
        [WXCommonViewClass goToTabItem:self.navigationController tabItem:1];
    }
    else
    {
    [[XY_Cloud_OrderPaymentInfo share]setDelegate:(id)self];
    [[XY_Cloud_OrderPaymentInfo share]readyToPay:self.orderID];
    }
}

/*客户端支付*/
- (void)startAliPay:(NSString*)str{
	NSString *appScheme = url_schemes_alixPay;
    //获取快捷支付单例并调用快捷支付接口
    AlixPay * alixpay = [AlixPay shared];
    int ret = [alixpay pay:str applicationScheme:appScheme];
    
    if (ret == kSPErrorAlipayClientNotInstalled) {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"提示"
                                                             message:@"您还没有安装支付宝快捷支付，请先安装。"
                                                            delegate:self
                                                   cancelButtonTitle:@"确定"
                                                   otherButtonTitles:nil];
        [alertView setTag:777];
        [alertView show];
    }
}
//支付宝相关
-(void)ready:(NSString*)RSAString
{
    [[XY_Cloud_OrderPaymentInfo share] setDelegate:nil];
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    appDelegate.paySucessVC = self;
    [[XY_Cloud_OrderPaymentInfo share] setDelegate:nil];
    if([self.payStyle intValue] == 1){
        [self startAliPay:RSAString];
    }else if([self.payStyle intValue] == 2){
        
         XYPayWebViewController *payWebVC = [[XYPayWebViewController alloc]init];
         payWebVC.payUrl = RSAString;
         payWebVC.isTuan = self.isTuan;
         payWebVC.orderID = self.orderID;
         payWebVC.money = self.money;
         payWebVC.payStyleText = self.payStyleText;
         payWebVC.selfTakeFlag = self.selfTakeFlag;
        [self.navigationController pushViewController:payWebVC animated:YES];
        
    }
}
-(void)notReady:(NSString*)errCode
{
    [[XY_Cloud_OrderPaymentInfo share] setDelegate:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (alertView.tag == 777) {
		NSString * URLString = @"http://itunes.apple.com/cn/app/id535715926?mt=8";
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:URLString]];
	}
}
@end
