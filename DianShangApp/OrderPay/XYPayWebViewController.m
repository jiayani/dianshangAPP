//
//  XYPayWebViewController.m
//  DianShangApp
//
//  Created by wa on 14-6-16.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "XYPayWebViewController.h"
#import "WXCommonViewClass.h"
#import "XYPaySuccessViewController.h"
@interface XYPayWebViewController ()

@end

@implementation XYPayWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [WXCommonViewClass hideTabBar:self isHiden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     [WXCommonViewClass setActivityWX:YES content:NSLocalizedString(@"正在加载…", @"登录/正在加载…") nav:self.navigationController];
    NSURL *url = [NSURL URLWithString:self.payUrl];
    [_payWebView loadRequest:[NSURLRequest requestWithURL:url]];
}
- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSURL *url = [request URL];
    NSString *fullUrl = [url scheme];

    if([fullUrl isEqualToString:@"succ"]){
        
        XYPaySuccessViewController *paySuccessVC = [[XYPaySuccessViewController alloc]init];
        paySuccessVC.isTuan = self.isTuan;
        paySuccessVC.orderID = self.orderID;
        paySuccessVC.money = self.money;
        paySuccessVC.pushFlag = @"pushFlag";
        paySuccessVC.payStyleText = self.payStyleText;
        paySuccessVC.selfTakeFlag = self.selfTakeFlag;
        [self.navigationController pushViewController:paySuccessVC animated:YES];
    }else if([fullUrl isEqualToString:@"fail"]){
        [self.navigationController popViewControllerAnimated:YES];
    }else if([fullUrl isEqualToString:@"stop"]){
        [self.navigationController popViewControllerAnimated:YES];
    }
    return YES;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [WXCommonViewClass setActivityWX:NO content:nil nav:self.navigationController];
    
}
@end
