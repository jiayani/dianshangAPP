//
//  XYPaySuccessViewController.h
//  DianShangApp
//
//  Created by wa on 14-6-16.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XYPaySuccessViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *orderLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *payStyleLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleMegLabel;
@property (weak, nonatomic) IBOutlet UILabel *bigTitleLabel;
@property (nonatomic, assign)  BOOL isTuan;
@property (nonatomic, strong) NSString *orderID;
@property (nonatomic, strong) NSString *money;
@property (nonatomic, strong) NSString *payStyleText;
@property (nonatomic, strong) NSString *pushFlag;
@property (nonatomic, assign) BOOL selfTakeFlag;
- (IBAction)goOnBuyBtnAct:(id)sender;
@end
