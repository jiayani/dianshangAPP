//
//  XYOrderPayViewController.h
//  DianShangApp
//
//  Created by wa on 14-6-12.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WX_Cloud_Delegate.h"

@interface XYOrderPayViewController : UIViewController<UIAlertViewDelegate,WX_Cloud_Delegate>
{
    NSString * AnalysisFlag;
}
@property (weak, nonatomic) IBOutlet UILabel *orderLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *payStyleLabel;
@property (weak, nonatomic) IBOutlet UILabel *selfAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *bigTitleLabel;
@property (nonatomic, strong) NSString *orderID;
@property (nonatomic, strong) NSString *money;
@property (nonatomic, strong) NSString *payStyle;
@property (nonatomic, strong) NSString *payStyleText;
@property (nonatomic, strong) NSString *selfAddress;
@property (nonatomic, assign)  BOOL isTuan;
@property (weak, nonatomic) IBOutlet UIImageView *changeImageView;
@property (weak, nonatomic) IBOutlet UIImageView *flowerImageView;
@property (weak, nonatomic) IBOutlet UILabel *addressTitle;
@property (weak, nonatomic) IBOutlet UIButton *payOffBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleMegLabel;
@property (nonatomic, assign) BOOL selfTakeFlag;
- (IBAction)payOffBtnAct:(id)sender;
- (void)gotoPaySuccess;
@end
