//
//  FYGifeListTableViewCell.h
//  DianShangApp
//
//  Created by Fuy on 14-6-3.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AsynImageView;
@interface FYGifeListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbl_gifeName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_gifeScore;
@property (weak, nonatomic) IBOutlet AsynImageView *gifeImageView;

@end
