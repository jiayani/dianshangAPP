//
//  FYCreditsGiftViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-5-28.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYCreditsGiftViewController : UIViewController<UIAlertViewDelegate>{
    //wangxia 用来保存联盟礼品的数组
    NSArray *union_gift_array;
}

@property (weak, nonatomic) IBOutlet UIView *menuView;
@property(weak,nonatomic)IBOutlet UIScrollView *creditsScrollView;
@property(weak,nonatomic)IBOutlet UIView *adView;
@property(weak,nonatomic)IBOutlet UIImageView *adImageView;
@property(weak,nonatomic)IBOutlet UIView *myCreditsStateView;
@property(weak,nonatomic)IBOutlet UILabel *userInforLabel;
@property(weak,nonatomic)IBOutlet UIButton *loginButton;
@property(weak,nonatomic)IBOutlet UIButton *exchangeButton;
@property (weak, nonatomic) IBOutlet UILabel *lbl_jifen;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;

//wangxia 积分联盟
@property (weak, nonatomic) IBOutlet UIView *unionView;
@property (weak, nonatomic) IBOutlet UIScrollView *unionGiftScrollView;


/*积分兑换*/
- (IBAction)exchangeCredits:(id)sender;
/*登录*/
- (IBAction)login:(id)sender;

//wangxia 积分联盟
- (IBAction)unionMoreBtnPressed:(id)sender;
@end
