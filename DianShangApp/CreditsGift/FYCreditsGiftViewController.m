//
//  FYCreditsGiftViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-5-28.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYCreditsGiftViewController.h"
#import "WXCommonViewClass.h"
#import "WXUserDatabaseClass.h"
#import "WX_Cloud_IsConnection.h"
#import "WX_Cloud_Delegate.h"
#import "FY_Cloud_GiftMenuSync.h"
#import "FY_Cloud_MyCard.h"
#import "FYCreditsGiftListViewController.h"
#import "XYGiftSearchViewController.h"
#import "FY_Cloud_GetJifen.h"
#import "WXUserLoginViewController.h"
#import "FYjifenChangeViewController.h"
#import "XYLoadingView.h"
#import "UserInfo.h"
#import "WXBindWooViewController.h"
#import "FYHouseViewController.h"
#import "AsynImageView.h"
#import "XYGiftInforViewController.h"
//竖间距
#define kHEIGHT 15
//横间距
#define kWIDTH 12

#define kFIRST_MENU_HEIGHT 38

//button宽度
#define kBUTTON_WIDTH 90;
//button高度
#define KBUTTON_HEIGHT 36
static float totalHeight = 0.0f;
static UIFont *firstMenuFont,*secondMenuFont;
@interface FYCreditsGiftViewController ()
{
    NSArray *private_gift;
    NSArray *woo_gift;
    NSMutableArray *secondMenuArray;
    NSMutableArray *secondMenuWooArray;
    XYLoadingView *loadingView;
    NSString *jifen;
    NSDictionary * dic;
    float lastHeight;
}
@end

@implementation FYCreditsGiftViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"积分礼品",@"积分礼品");
        [WXCommonViewClass hideTabBar:self isHiden:NO];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //wangxia 联盟礼品 初始化
    union_gift_array = [[NSArray alloc]init];
    self.unionView.hidden = YES;
    //end
    firstMenuFont = [UIFont systemFontOfSize:16];
    secondMenuFont = [UIFont systemFontOfSize:14];
    private_gift = [[NSArray alloc] init];
    woo_gift = [[NSArray alloc] init];
    secondMenuArray = [[NSMutableArray alloc] initWithCapacity:10];
    secondMenuWooArray = [[NSMutableArray alloc] initWithCapacity:10];
    
    UIButton *rightButtonOfNav = [WXCommonViewClass getRightBtnOfNav:@"gift_search.png" hightlightedImageName:@"gift_search.png" title:nil];
    [rightButtonOfNav addTarget:self action:@selector(searchPush:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightButtonOfNav];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.adImageView addGestureRecognizer:tapGestureRecognizer];
    if(![[WX_Cloud_IsConnection share] isConnectionAvailable]){
        [WXCommonViewClass showHudInButtonOfView:self.view title:NSLocalizedString(@"目前网络连接不可用",@"目前网络连接不可用") closeInSecond:3];
    }else{
        
        loadingView = [[XYLoadingView alloc] initWithType:2];
        [self.view addSubview:loadingView];
        [[FY_Cloud_GiftMenuSync share] setDelegate:(id)self];
        [[FY_Cloud_GiftMenuSync share] giftMenuSync];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self initMenuView];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[FY_Cloud_MyCard share] setDelegate:nil];
    [[FY_Cloud_GiftMenuSync share] setDelegate:nil];
}

- (void)singleTap:(id)sender
{
    FYHouseViewController *houseVC = [[FYHouseViewController alloc] initWithNibName:@"FYHouseViewController" bundle:nil];
    [self.navigationController pushViewController:houseVC animated:YES];
}
#pragma mark 网络回调
-(void)syncFYGetMyCardSuccess:(NSDictionary *)dataDic
{
    self.lbl_jifen.text = [NSString stringWithFormat:@"您还有积分%@", [dataDic objectForKey:@"cardIntegral"]];
    jifen = [NSString stringWithFormat:@"%@", [dataDic objectForKey:@"cardIntegral"]];
    [[FY_Cloud_MyCard share] setDelegate:nil];
}

-(void)syncFYGetMyCardFailed:(NSString *)errCode
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errCode];
    [[FY_Cloud_MyCard share] setDelegate:nil];
}

-(void)syncGiftMenuSuccess:(id)database
{
    [loadingView removeFromSuperview];
    dic = (NSDictionary *)database;
    private_gift = [dic objectForKey:@"private_gift"];
    if (![[dic objectForKey:@"private_gift"] isEqual:@[]] ) {
        secondMenuArray = [[private_gift objectAtIndex:0] objectForKey:@"children"];
        woo_gift = [dic objectForKey:@"woo_gift"];
        [self addMenu];
    }
    
    if ([[dic objectForKey:@"woo_gift"] isEqual:@[]] ) {
        
    }else
    {
        secondMenuWooArray = [[woo_gift objectAtIndex:0] objectForKey:@"children"];
        [self addWooMenu];
    }
    //wangxia 添加联盟礼品
    union_gift_array = [dic objectForKey:@"union_gift"];
    [self setUnionGiftScrollView];
    
}

-(void)syncGiftMenuFailed:(NSString*)errCode
{
    [WXCommonViewClass showHudInView:self.view title:errCode];
}

- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [[FY_Cloud_MyCard share] setDelegate:nil];
    [[FY_Cloud_GiftMenuSync share] setDelegate:nil];
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)searchPush:(id)sender
{
    XYGiftSearchViewController * giftSearchVC= [[XYGiftSearchViewController alloc] initWithNibName:@"XYGiftSearchViewController" bundle:nil];
    //积分传值，暂时写死
    giftSearchVC.tranScore = jifen;
    [self.navigationController pushViewController:giftSearchVC animated:YES];
}

- (void)initMenuView{
    UserInfo *user = [WXUserDatabaseClass getCurrentUser];
    if (user != nil) {
       // [[FY_Cloud_jifenInfo share] setDelegate:self];
       // [[FY_Cloud_jifenInfo share] getData];
        WXCommonSingletonClass *singletonClass = [WXCommonSingletonClass share];
        [[FY_Cloud_MyCard share] setDelegate:(id)self];
        [[FY_Cloud_MyCard share] syncGetMyCard:singletonClass.currentUserId];
         if ([user.isBindWoo intValue] == 0) {
            [self.exchangeButton setTitle:@"绑定账号" forState:UIControlStateNormal];
        }else
        {
            [self.exchangeButton setTitle:@"积分互换" forState:UIControlStateNormal];
        }
        [self.loginButton setHidden:YES];
        if (singletonClass.isChange) {
            [self.exchangeButton setHidden:NO];
        }else
        {
            [self.exchangeButton setHidden:YES];
        }
        self.lbl_jifen.hidden = NO;
        self.lbl_name.hidden = NO;
        self.lbl_name.text = [NSString stringWithFormat:@"亲爱的%@", user.userEmail];
        self.userInforLabel.hidden = YES;
    }else
    {
        
        [self.loginButton setHidden:NO];
        [self.exchangeButton setHidden:YES];
        self.lbl_jifen.hidden = YES;
        self.lbl_name.hidden = YES;
        self.userInforLabel.hidden = NO;
        
    }
}

- (IBAction)login:(id)sender
{
    WXUserLoginViewController *loginVC = [[WXUserLoginViewController alloc]initWithNibName:@"WXUserLoginViewController" bundle:nil];
    [self.navigationController pushViewController:loginVC animated:YES];
}

- (IBAction)exchangeCredits:(id)sender
{
    UserInfo *user = [WXUserDatabaseClass getCurrentUser];
    if ([user.isBindWoo intValue] == 0) {
        WXBindWooViewController *bindVC = [[WXBindWooViewController alloc] initWithNibName:@"WXBindWooViewController" bundle:nil];
        [self.navigationController pushViewController:bindVC animated:YES];
    }else
    {
    FYjifenChangeViewController *changeVC = [[FYjifenChangeViewController alloc] initWithNibName:@"FYjifenChangeViewController" bundle:nil];
    [self.navigationController pushViewController:changeVC animated:YES];
    }
}

//加载页面元素
- (void)addMenu
{
    for (int i = 0; i < private_gift.count; i++) {
        if(i == 0){
            totalHeight += kHEIGHT;
        }
        //begin:添加前面的红色标识
        UIView *redLineView = [[UIView alloc]init];
        redLineView.frame = CGRectMake(10, totalHeight+3, 2, 12);
        redLineView.backgroundColor = [UIColor colorWithRed:253.0f/255.0f green:75.0f/255.0f blue:77.0f/255.0f alpha:1.0f];
        [_menuView addSubview:redLineView];
        //end
        
        //begin:一级菜单
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(15,totalHeight,150,19)];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",[[private_gift objectAtIndex:i] objectForKey:@"firstMenuName"]]];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,2)];
        
        label.attributedText = str;
        label.textColor = [UIColor colorWithRed:43.0f/255.0f green:43.0f/255.0f blue:43.0f/255.0f alpha:1.0];
        label.backgroundColor = [UIColor clearColor];
        label.font = firstMenuFont;
        [_menuView addSubview:label];
        //end
        
        //begin:一级菜单下添加横线
        UIView *lineView = [[UIView alloc]init];
        lineView.frame = CGRectMake(10, totalHeight + 23, 310, 0.5f);
        lineView.backgroundColor = [UIColor colorWithRed:219.0f/255.0f green:219.0f/255.0f blue:219.0f/255.0f alpha:1.0f];
        [_menuView addSubview:lineView];
        //end
        
        secondMenuArray = [[private_gift objectAtIndex:i] objectForKey:@"children"];
        if(secondMenuArray == nil || secondMenuArray.count == 0){
            totalHeight += kFIRST_MENU_HEIGHT;
        }else
        {
            for (int j=0;j<[secondMenuArray count];j++)
            {
                
                NSString *secondMenuName = [[secondMenuArray objectAtIndex:j] objectForKey:@"secondMenuName"];
                
                UIButton *button =[UIButton buttonWithType:UIButtonTypeCustom];
                //如果二级目录超过1000 程序将出现问题
                button.tag = i * 100 + j;
                
                float sWidth = kBUTTON_WIDTH;
                
                float sHeight = KBUTTON_HEIGHT;
                float sX = (j % 3 + 1) * kWIDTH + j % 3 * kBUTTON_WIDTH;
                if(j == 0){
                    totalHeight += kHEIGHT + 19;
                }else if((j + 1) % 3 == 1){
                    totalHeight += kHEIGHT + KBUTTON_HEIGHT;
                }
                
                float sY = totalHeight;
                
                button.frame = CGRectMake(sX,sY,sWidth,sHeight);
                lastHeight= sY;
                [button setBackgroundImage:[UIImage imageNamed:@"gift_btn_bg.png"] forState:UIControlStateNormal];
                button.titleLabel.font = secondMenuFont;
                [button setTitleColor:[UIColor colorWithRed:102.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1.0] forState:UIControlStateNormal];
                [button setTitle:secondMenuName forState:UIControlStateNormal];
                [button addTarget:self action:@selector(showGiftList:) forControlEvents:UIControlEventTouchUpInside];
                [_menuView addSubview:button];
                //判断当前是否超出menuView高度
                if(j == [secondMenuArray count] - 1){
                    totalHeight += kHEIGHT + KBUTTON_HEIGHT;
                }
            }
        }
    }
    if(totalHeight >_menuView.frame.size.height){
        CGRect frame = _menuView.frame;
        frame.size.height = totalHeight;
        _menuView.frame = frame;
        _creditsScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.menuView.frame.origin.y + self.menuView.frame.size.height );
        if (!iPhone5){
            _creditsScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.menuView.frame.origin.y + self.menuView.frame.size.height + 74);
        }
        _creditsScrollView.scrollEnabled = YES;
    }
}
//加载哇点礼品
- (void)addWooMenu
{
    for (int i = 0; i < woo_gift.count; i++) {
        if(i == 0){
            totalHeight += kHEIGHT;
        }
        //begin:添加前面的红色标识
        UIView *redLineView = [[UIView alloc]init];
        redLineView.frame = CGRectMake(10, totalHeight - 11, 2, 12);
        redLineView.backgroundColor = [UIColor colorWithRed:253.0f/255.0f green:75.0f/255.0f blue:77.0f/255.0f alpha:1.0f];
        [_menuView addSubview:redLineView];
        //end
        //begin:添加一级菜单
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(15, totalHeight - 15, 150, 19)];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",[[woo_gift objectAtIndex:i] objectForKey:@"firstMenuName"]]];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,2)];
        label.attributedText = str;
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1.0];
        label.font = firstMenuFont;
        [_menuView addSubview:label];
        //end
        
        //begin:一级菜单下添加横线
        UIView *lineView = [[UIView alloc]init];
        lineView.frame = CGRectMake(10, totalHeight + 8, 310, 0.5f);
        lineView.backgroundColor = [UIColor colorWithRed:219.0f/255.0f green:219.0f/255.0f blue:219.0f/255.0f alpha:1.0f];
        [_menuView addSubview:lineView];
        //end
        
        secondMenuWooArray = [[woo_gift objectAtIndex:i] objectForKey:@"children"];
        if(secondMenuWooArray == nil || secondMenuWooArray.count == 0){
            totalHeight += kFIRST_MENU_HEIGHT;
        }else
        {
            for (int j=0;j<[secondMenuWooArray count];j++)
            {
                
                NSString *secondMenuName = [[secondMenuWooArray objectAtIndex:j] objectForKey:@"secondMenuName"];
                
                UIButton *button =[UIButton buttonWithType:UIButtonTypeCustom];
                //如果二级目录超过1000 程序将出现问题
                button.tag = i * 100 + j + 1;
                button.tag = -button.tag;
                float sWidth = kBUTTON_WIDTH;
                float sHeight = KBUTTON_HEIGHT;
                float sX = (j % 3 + 1) * kWIDTH + j % 3 * kBUTTON_WIDTH;
                if(j == 0){
                    totalHeight += kHEIGHT + 19;
                }else if((j + 1) % 3 == 1){
                    totalHeight += kHEIGHT + KBUTTON_HEIGHT;
                }
                
                float sY = totalHeight;
                
                button.frame = CGRectMake(sX,sY - 15,sWidth,sHeight);
                lastHeight = sY - 15;
                [button setBackgroundImage:[UIImage imageNamed:@"gift_btn_bg.png"] forState:UIControlStateNormal];
                button.titleLabel.font = secondMenuFont;
                [button setTitleColor:[UIColor colorWithRed:102.0f/255.0 green:102.0f/255.0 blue:102.0f/255.0 alpha:1.0] forState:UIControlStateNormal];
                [button setTitle:secondMenuName forState:UIControlStateNormal];
                [button addTarget:self action:@selector(showGiftList:) forControlEvents:UIControlEventTouchUpInside];
                [_menuView addSubview:button];
                //判断当前是否超出menuView高度
                if(j == [secondMenuWooArray count] - 1){
                    totalHeight += kHEIGHT + KBUTTON_HEIGHT;
                }
            }
        }
    }
    if(totalHeight >_menuView.frame.size.height){
        CGRect frame = _menuView.frame;
        frame.size.height = totalHeight;
        _menuView.frame = frame;
        _creditsScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.menuView.frame.origin.y + self.menuView.frame.size.height);
        if (!iPhone5) {
            _creditsScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.menuView.frame.origin.y + self.menuView.frame.size.height + 74);
        }
        _creditsScrollView.scrollEnabled = YES;
    }
}

- (void)showGiftList:(id)sender
{
    UIButton *button = (UIButton *)sender;
    FYCreditsGiftListViewController *FYGifeVC = [[FYCreditsGiftListViewController alloc] initWithNibName:@"FYCreditsGiftListViewController" bundle:nil];
    if(button.tag >= 0){
        NSUInteger i = button.tag / 100;
        NSUInteger j = button.tag % 100;
        NSArray *array = [[private_gift objectAtIndex:i] objectForKey:@"children"];
        FYGifeVC.title = [[array objectAtIndex:j] objectForKey:@"secondMenuName"];
        FYGifeVC.firstMenuId = [[private_gift objectAtIndex:i] objectForKey:@"firstMenuID"];
        FYGifeVC.secondMenuId = [[array objectAtIndex:j] objectForKey:@"secondMenuID"];
        FYGifeVC.listType = 0;
        
    }else{
        NSUInteger i = -button.tag / 100;
        NSUInteger j = -button.tag % 100 - 1;
        NSArray *array = [[woo_gift objectAtIndex:i] objectForKey:@"children"];
        FYGifeVC.title = [[array objectAtIndex:j] objectForKey:@"secondMenuName"];
        FYGifeVC.firstMenuId = [[woo_gift objectAtIndex:i] objectForKey:@"firstMenuID"];
        FYGifeVC.secondMenuId = [[array objectAtIndex:j] objectForKey:@"secondMenuID"];
        FYGifeVC.listType = 1;
    }
    [WXCommonViewClass hideTabBar:FYGifeVC isHiden:YES];
    [self.navigationController pushViewController:FYGifeVC animated:YES];
    
}

#pragma mark - wangxia 联盟礼品

-(void)setUnionGiftScrollView{
    for (UIView *tempView in self.unionGiftScrollView.subviews) {
        [tempView removeFromSuperview];
    }
    CGFloat pointY;
    if (union_gift_array != nil && union_gift_array.count > 0) {
        pointY = self.menuView.frame.origin.y;
        
        self.unionView.hidden = NO;
        // 是否按页滚动
        self.unionGiftScrollView.pagingEnabled = NO;
        self.unionGiftScrollView.backgroundColor = [UIColor clearColor];
        // 隐藏滚动条
        self.unionGiftScrollView.showsVerticalScrollIndicator = NO;
        self.unionGiftScrollView.showsHorizontalScrollIndicator = YES;
        
        self.unionGiftScrollView.canCancelContentTouches = NO;
        
        self.unionGiftScrollView.clipsToBounds = YES;
        
        CGRect rect = CGRectMake(10, 0, 80, 106);
        
        CGFloat width = rect.size.width;
        
        int i = 0;
        for (NSDictionary *tempDic in union_gift_array)
        {
            // 创建ImageView
            UIView *tempView = [[UIView alloc]initWithFrame:rect];
            tempView.tag = i;
            //图片
            AsynImageView *imageView = [[AsynImageView alloc] initWithFrame:CGRectMake(0, 2, 80, 80)];
            imageView.type = 3;
            imageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
            imageView.imageURL = [tempDic objectForKey:@"giftPic"];
            
            //礼品名称
            UILabel *myLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 83, 80, 20)];
            myLabel.backgroundColor = [UIColor clearColor];
            myLabel.text = [tempDic objectForKey:@"giftName"];
            myLabel.font = [UIFont systemFontOfSize:12];
            myLabel.textColor = [UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1.0];
            myLabel.textAlignment = NSTextAlignmentCenter;
            [tempView addSubview:imageView];
            [tempView addSubview:myLabel];
            [self.unionGiftScrollView addSubview:tempView];
            //wangxia 联盟礼品单击事件
            UITapGestureRecognizer *tapGestureRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(unionGiftSingleTap:)];
            tapGestureRecognizer1.numberOfTapsRequired = 1;
            [tempView addGestureRecognizer:tapGestureRecognizer1];
            // 加入到ScrollView
            
            // 修改子视图位置
            CGRect frame = rect;
            frame.origin.x = frame.origin.x + frame.size.width +10;
            rect = frame;
            i++;
            
        }
        // 设置ScrollView大小
        self.unionGiftScrollView.contentSize = CGSizeMake((width + 10)*i ,self.unionGiftScrollView.frame.size.height);
        CGRect frame = _menuView.frame;
        frame.size.height += 115 ;
        _menuView.frame = frame;
    }else{
        self.unionView.hidden = YES;
        pointY = self.unionView.frame.origin.y;
        
    }
    self.menuView.frame = CGRectMake(self.menuView.frame.origin.x, pointY, self.menuView.frame.size.width, self.menuView.frame.size.height);
    
}

//跳转到礼品列表页面
- (IBAction)unionMoreBtnPressed:(id)sender {
    FYCreditsGiftListViewController *creditsGiftListVC = [[FYCreditsGiftListViewController alloc]initWithNibName:@"FYCreditsGiftListViewController" bundle:nil];
    creditsGiftListVC.title = @"联盟礼品";
    [WXCommonViewClass hideTabBar:creditsGiftListVC isHiden:YES];
    creditsGiftListVC.listType = 2;//联盟礼品标识
    [self.navigationController pushViewController:creditsGiftListVC animated:YES];
}

//跳转到礼品详情页面
- (void)gotoUnionGiftDetailVC:(int)row{
    XYGiftInforViewController *giftInforVC = [[XYGiftInforViewController alloc] initWithNibName:@"XYGiftInforViewController" bundle:nil];
    giftInforVC.giftType = 2;
    NSDictionary *tempDic = [union_gift_array objectAtIndex:row];

    giftInforVC.giftID = [[tempDic objectForKey:@"giftId"] intValue];
    [self.navigationController pushViewController:giftInforVC animated:YES];
}
-(void)unionGiftSingleTap:(UITapGestureRecognizer*)recognizer{
    int row = recognizer.view.tag;
    [self gotoUnionGiftDetailVC:row];
    
}
@end
