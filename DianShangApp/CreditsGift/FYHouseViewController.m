//
//  FYHouseViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-7-3.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYHouseViewController.h"
#import "WXCommonViewClass.h"
#import "WXConfigDataControll.h"
#import "AppDelegate.h"

@interface FYHouseViewController ()

@end

@implementation FYHouseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"积分管家";
    [WXCommonViewClass hideTabBar:self isHiden:YES];
    /*设置导航返回按钮*/
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    
    NSString *htmlStr = @"http://www.591woo.com";
    
    UIWebView *webView = [[UIWebView alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width - 20, self.view.frame.size.height - 60)];
    
    webView.backgroundColor = [UIColor clearColor];
    [webView setOpaque:NO];
    id scroller = [webView.subviews objectAtIndex:0];
    for (UIView *subView in [scroller subviews])
    {
        if ([[[subView class] description] isEqualToString:@"UIImageView"])
            subView.hidden = YES;
    }
    [self.view addSubview:webView];
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:htmlStr]];
    [webView loadRequest:request];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}
#pragma mark - my methods
//返回按钮事件
- (void)backBtnPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
