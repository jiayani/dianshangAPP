//
//  FYCreditsGiftListViewController.h
//  DianShangApp
//
//  Created by Fuy on 14-6-3.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableFooterView.h"
#import "WX_Cloud_Delegate.h"
@interface FYCreditsGiftListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,EGORefreshTableDelegate,WX_Cloud_Delegate>

@property (nonatomic, retain) NSMutableArray *gifeArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property(retain,nonatomic)NSString *firstMenuId;
@property(retain,nonatomic)NSString *secondMenuId;


//0为商家 1为哇点
@property(assign,nonatomic)NSInteger listType;

@end
