//
//  FYCreditsGiftListViewController.m
//  DianShangApp
//
//  Created by Fuy on 14-6-3.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYCreditsGiftListViewController.h"
#import "FYGifeListTableViewCell.h"
#import "WXCommonViewClass.h"
#import "WX_Cloud_IsConnection.h"
#import "WX_Cloud_Delegate.h"
#import "FY_Cloud_GiftListSync.h"
#import "XYGiftInforViewController.h"
#import "AsynImageView.h"
#import "XYNoDataView.h"
#import "XYLoadingView.h"
#import "WX_Cloud_GetUnionGiftList.h"

#define maxCountOfOnePage 8;
@interface FYCreditsGiftListViewController ()
{
    int giftID;
    BOOL isHaveMore;
    BOOL reloading;
    EGORefreshTableFooterView *refreshFooterView;
    int page;
    XYLoadingView *loadingView;
}
@end

@implementation FYCreditsGiftListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *leftButtonOfNav = [WXCommonViewClass getBackButton];
    [leftButtonOfNav addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftButtonOfNav];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    page = 1;
    self.gifeArray = [[NSMutableArray alloc]init];
    [self requestProductList];
    [self initRefreshFooterView];
}

- (void)initRefreshFooterView{
    
    int height = MAX(self.tableView.bounds.size.height, self.tableView.contentSize.height);
    refreshFooterView = [[EGORefreshTableFooterView alloc]  initWithFrame:CGRectZero];
    refreshFooterView.frame =CGRectMake(0.0f, height, self.view.frame.size.width, self.tableView.bounds.size.height);
    //如果contentsize的高度比表的高度小，那么就需要把刷新视图放在表的bounds的下面
    
    refreshFooterView.delegate = self;
    //下拉刷新的控件添加在tableView上
    
    [self.tableView addSubview:refreshFooterView];
    reloading = NO;
    
}

- (void)backBtnPressed{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[FY_Cloud_GiftListSync share]setDelegate:nil];
    [[WX_Cloud_GetUnionGiftList share]setDelegate:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}
#pragma mark 网络回调
-(void)syncGifeListSuccess:(id)database
{
    NSArray *array = (NSArray *)database;
    [loadingView removeFromSuperview];
    if (self.gifeArray.count == 0) {
        self.gifeArray = [NSMutableArray arrayWithArray:array];
    }else{
        [self.gifeArray addObjectsFromArray:array] ;
    }
    if (array.count == 8) {
        isHaveMore = YES;
    }else{
        isHaveMore = NO;
    }
    //刷新UI
    [self reloadUI];
    
}

-(void)syncGifeListFailed:(NSString*)errCode
{
    [loadingView removeFromSuperview];
    XYNoDataView *noData = [[XYNoDataView alloc] initWithFrame:self.view.frame];
    noData.megUpLabel.text = @"这里空空如也";
    noData.megDownLabel.text = @"现在暂无该类礼品，先去其他的礼品类别中看看吧";
    if (page == 1) {
        [self.view addSubview:noData];
    }else
    {
        isHaveMore = NO;
        [self reloadUI];
    }
    
}

- (void)accidentError:(NSString *)errorStr errorCode:(NSString *)errorCode
{
    [loadingView removeFromSuperview];
    [WXCommonViewClass showHudInView:self.view title:errorStr];
}
#pragma mark - 联盟礼品列表回掉
//得到联盟礼品的列表 wangxia
- (void)syncGetUnionGiftListSuccess:(id)data{
    [[WX_Cloud_GetUnionGiftList share]setDelegate:nil];
    NSArray *array = (NSArray *)data;
    [loadingView removeFromSuperview];
    if (self.gifeArray.count == 0) {
        self.gifeArray = [NSMutableArray arrayWithArray:array];
    }else{
        [self.gifeArray addObjectsFromArray:array] ;
    }
    if (array.count == 8) {
        isHaveMore = YES;
    }else{
        isHaveMore = NO;
    }
    //刷新UI
    [self reloadUI];
}
- (void)syncGetUnionGiftListFailed:(NSString *)errCode{
    [[WX_Cloud_GetUnionGiftList share]setDelegate:nil];
    [loadingView removeFromSuperview];
    XYNoDataView *noData = [[XYNoDataView alloc] initWithFrame:self.view.frame];
    noData.megUpLabel.text = @"这里空空如也";
    noData.megDownLabel.text = @"现在暂无该类礼品，先去其他的礼品类别中看看吧";
    if (page == 1) {
        [self.view addSubview:noData];
    }else
    {
        isHaveMore = NO;
        [self reloadUI];
    }
}

#pragma mark  tableView DataSourse

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.gifeArray == nil || self.gifeArray.count == 0){
        return 0;
    }
    return self.gifeArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *str = @"Cell";
    FYGifeListTableViewCell *cell = (FYGifeListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:str];
    if (cell == nil){
        cell = [[[NSBundle  mainBundle]  loadNibNamed:@"FYGifeListTableViewCell" owner:self options:nil]  lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lbl_gifeName.text = [NSString stringWithFormat:@"%@",[[self.gifeArray objectAtIndex:indexPath.row] objectForKey:@"giftName"]];
    cell.lbl_gifeScore.text = [NSString stringWithFormat:@"%@积分", [[self.gifeArray objectAtIndex:indexPath.row] objectForKey:@"giftIntegral"]];
    cell.gifeImageView.type = 1;
    if ([[[self.gifeArray objectAtIndex:indexPath.row] objectForKey:@"giftType"] intValue] == 1) {
        cell.gifeImageView.isFromWooGift = YES;
    }
    cell.gifeImageView.placeholderImage = [UIImage imageNamed:@"defaultImage.png"];
    if (_listType == 2) {
        cell.gifeImageView.imageURL = [[self.gifeArray objectAtIndex:indexPath.row] objectForKey:@"giftPic"] ;
    }else{
        cell.gifeImageView.imageURL = [[[[self.gifeArray objectAtIndex:indexPath.row] objectForKey:@"giftPic"] objectAtIndex:0] objectForKey:@"img"];
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    XYGiftInforViewController *XYGiftVC = [[XYGiftInforViewController alloc] initWithNibName:@"XYGiftInforViewController" bundle:nil];
    XYGiftVC.giftType = self.listType;
    XYGiftVC.giftID = [[[self.gifeArray objectAtIndex:indexPath.row] objectForKey:@"giftID"] intValue];
    [self.navigationController pushViewController:XYGiftVC animated:YES];
    
    
    
    
}
-(void)requestProductList{
    loadingView = [[XYLoadingView alloc] initWithType:2];
    [self.view addSubview:loadingView];
    if (_listType == 2) {
        [[WX_Cloud_GetUnionGiftList share]setDelegate:(id)self];
        [[WX_Cloud_GetUnionGiftList share] getUnionGiftList:page];
    }else{
        [[FY_Cloud_GiftListSync share]setDelegate:(id)self];
        [[FY_Cloud_GiftListSync share]giftSync:_firstMenuId second:_secondMenuId page:page giftType:_listType];
    }
}

#pragma mark - 下拉刷新数据方法
//请求数据
-(void)requestData
{
    reloading = YES;
    BOOL isConnection = [[[WX_Cloud_IsConnection alloc]init]isConnectionAvailable];
    if (isConnection) {//有网络情况
        
        //每次进入该页面的时候，重新从服务器端读取数据
        if(refreshFooterView.hasMoreData)
        {
            
            int myCount = self.gifeArray.count;
            int pageCount = myCount/maxCountOfOnePage;
            page += pageCount;
            //下载团购信息
            [self requestProductList];
            
        }
    }else{//无网络情况
        [WXCommonViewClass showHudInButtonOfView:self.view title:NoHaveNetwork closeInSecond:2];
    }
    
}

-(void)reloadUI
{
    reloading = NO;
    //停止下拉的动作,恢复表格的便宜
	[refreshFooterView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    //更新界面
    [self.tableView reloadData];
    
    [self setRefreshViewFrame];
    
}

-(void)setRefreshViewFrame
{
    //如果contentsize的高度比表的高度小，那么就需要把刷新视图放在表的bounds的下面
    int height = MAX(self.tableView.bounds.size.height, self.tableView.contentSize.height);
    refreshFooterView.frame =CGRectMake(0.0f, height, self.view.frame.size.width, self.tableView.bounds.size.height);
}

#pragma mark - EGORefreshTableFooterDelegate
//出发下拉刷新动作，开始拉取数据
- (void)egoRefreshTableDidTriggerRefresh:(EGORefreshPos)aRefreshPos
{
    [self requestData];
}


//返回当前刷新状态：是否在刷新
- (BOOL)egoRefreshTableDataSourceIsLoading:(UIView*)view{
	
	return reloading; // should return if data source model is reloading
	
}
//返回刷新时间
// if we don't realize this method, it won't display the refresh timestamp
- (NSDate*)egoRefreshTableDataSourceLastUpdated:(UIView*)view
{
	
	return [NSDate date]; // should return date data source was last changed
	
}

#pragma mark - UIScrollView

//此代理在scrollview滚动时就会调用
//在下拉一段距离到提示松开和松开后提示都应该有变化，变化可以在这里实现
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    refreshFooterView.hasMoreData = isHaveMore;
    [refreshFooterView egoRefreshScrollViewDidScroll:scrollView];
}
//松开后判断表格是否在刷新，若在刷新则表格位置偏移，且状态说明文字变化为loading...
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    refreshFooterView.hasMoreData = isHaveMore;
    [refreshFooterView egoRefreshScrollViewDidEndDragging:scrollView];
}

@end
