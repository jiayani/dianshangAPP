
//
//  FYGifeListTableViewCell.m
//  DianShangApp
//
//  Created by Fuy on 14-6-3.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "FYGifeListTableViewCell.h"
#import "WXConfigDataControll.h"
@implementation FYGifeListTableViewCell

- (void)awakeFromNib
{
    self.lbl_gifeScore.textColor = [WXConfigDataControll getFontColorOfIntegral];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
