//
//  WXSearchViewController.m
//  DianShangApp
//
//  Created by 霞 王 on 14-4-22.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import "WXSearchViewController.h"

@interface WXSearchViewController ()

@end

@implementation WXSearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
