//
//  AppDelegate.h
//  DianShangApp
//
//  Created by 霞 王 on 14-4-22.
//  Copyright (c) 2014年 霞 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FYOrderDetailViewController.h"
#import "XYOrderPayViewController.h"
#import "FYMyOrderViewController.h"
#import "WX_Cloud_Delegate.h"
#import <OneAPM/OneAPM.h>

@class FYShareViewController;
@class WXTabBarViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate,UINavigationControllerDelegate,WX_Cloud_Delegate>{
    UIWindow *window;

}

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, retain) WXTabBarViewController *wxTabBarViewController;

//wangxia
@property (strong,nonatomic) UserInfo *currentUser;
@property (strong,nonatomic) NSDictionary *pushNotificationKey;
@property (retain,nonatomic) NSArray *fontStyleOfMenu;
@property (retain,nonatomic) UIColor *titleFontColorOfNav;//导航上标题的字体颜色
@property (readwrite) CGFloat titleFontSizeOfNav;//导航上标题的字体大小

@property (retain,nonatomic) NSDictionary *versionDictionary;
//支付成功继续调用
@property(retain,nonatomic)XYOrderPayViewController *paySucessVC;
@property (retain,nonatomic) FYOrderDetailViewController * orderDetailVC;
@property (retain,nonatomic) FYMyOrderViewController * myOrderVC;
@property (strong, nonatomic) FYShareViewController *shareVC;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
+ (AppDelegate *)delegate;
- (UIWindow *)getCurrentWindow;
- (WXTabBarViewController *)getWXBarViewController;
- (void)updateVersion;//设置从版本更新中得到的内容

@end
